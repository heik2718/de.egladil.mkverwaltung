//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * PrivatkontoValidationTest
 */
public class PrivatkontoValidationTest {

	private static final Logger LOG = LoggerFactory.getLogger(Privatkonto.class);

	private Validator validator;

	private Privatkonto privatkontakt;

	private static final String JAHR_ALT = "2015";

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		privatkontakt = TestUtils.validRandomPrivatkontakt(JAHR_ALT);
	}

	@Test
	public void validate_passes_when_valid() {
		// Act
		final Set<ConstraintViolation<Privatkonto>> errors = validator.validate(privatkontakt);

		final Iterator<ConstraintViolation<Privatkonto>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<Privatkonto> cv = iter.next();
			LOG.info(cv.getPropertyPath().toString() + " " + cv.getMessage());
		}

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	public void nimmtTeil_gibt_valide_teilnahme_zurueck() {
		// Arrange
		final String jahr = "2017";

		// Act
		final Privatteilnahme privatteilnahme = new Privatteilnahme(jahr, new KuerzelGenerator().generateDefaultKuerzel());
		privatkontakt.addTeilnahme(privatteilnahme);

		// Assert
		final TeilnahmeIdentifierProvider teilnahme = privatkontakt.getTeilnahmeZuJahr(jahr);
		assertNotNull(teilnahme);

		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		// Act
		final Set<ConstraintViolation<Privatteilnahme>> errors = validator.validate(privatteilnahme);

		// Assert
		assertTrue(errors.isEmpty());
	}

}
