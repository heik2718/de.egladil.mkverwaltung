//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * FindAndCheckAssociatedEntitiesCommandTest
 */
public class FindAndCheckAssociatedEntitiesCommandTest {

	private static final String KUERZEL_TEILNAHME = "ZTFGR65Z";

	private static final String JAHR = "2018";

	private FindAndCheckAssociatedEntitiesCommand command;

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private PublicTeilnehmer teilnehmerPayload;

	private PublicTeilnehmer teilnehmerPayloadMitZusatz;

	private IPrivatteilnahmeDao privatteilnahmeDao;

	private ISchulteilnahmeDao schulteilnahmeDao;

	private IAuswertungsgruppeDao auswertungsgruppeDao;

	private ITeilnehmerDao teilnehmerDao;

	private Privatteilnahme teilnahme;

	@BeforeEach
	void setUp() {
		auswertungsgruppeDao = Mockito.mock(IAuswertungsgruppeDao.class);
		teilnehmerDao = Mockito.mock(ITeilnehmerDao.class);
		privatteilnahmeDao = Mockito.mock(IPrivatteilnahmeDao.class);
		schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		command = new FindAndCheckAssociatedEntitiesCommand(privatteilnahmeDao, schulteilnahmeDao, auswertungsgruppeDao,
			teilnehmerDao);

		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);

		teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Schlemmer");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setVorname("Rudi");

		teilnehmerPayloadMitZusatz = new PublicTeilnehmer();
		teilnehmerPayloadMitZusatz.setNachname("Schlemmer");
		teilnehmerPayloadMitZusatz.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayloadMitZusatz.setVorname("Rudi");
		teilnehmerPayloadMitZusatz.setZusatz("ghagsdga");

		teilnahme = new Privatteilnahme();
		teilnahme.setKuerzel(KUERZEL_TEILNAHME);
		teilnahme.setJahr(JAHR);
	}

	@Nested
	@DisplayName("Tests für Teilnehmer ohne Auswertungsgruppe")
	class SingleTeilnehmerTests {
		@Test
		@DisplayName("PreconditionFailedException wenn die Privatteilnahme nicht existiert")
		void keinePrivatteilnahme() {
			Mockito.when(privatteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.empty());

			final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
				command.withTeilnahmeIdentifier(teilnahmeIdentifier).execute();
			});
			assertEquals("Es gibt keine Teilnahme mit " + teilnahmeIdentifier.toString(), ex.getMessage());
		}

		@Test
		@DisplayName("PreconditionFailedException wenn die Schulteilnahme nicht existiert")
		void keineSchulteilnahme() {
			teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);
			Mockito.when(schulteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.empty());

			final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
				command.withTeilnahmeIdentifier(teilnahmeIdentifier).execute();
			});
			assertEquals("Es gibt keine Teilnahme mit " + teilnahmeIdentifier.toString(), ex.getMessage());
		}

		@Test
		@DisplayName("Die gefundene Privatteilnahme ist nach execute verfügbar")
		void privatteilnahmeVorhanden() {
			Mockito.when(privatteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.of(teilnahme));

			command.withTeilnahmeIdentifier(teilnahmeIdentifier).execute();
			final Privatteilnahme result = command.getPrivatteilnahme();

			assertEquals(teilnahme, result);
		}
	}
}
