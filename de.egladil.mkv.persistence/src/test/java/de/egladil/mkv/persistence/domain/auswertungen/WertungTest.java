//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Wertung;

/**
 * WertungTest
 */
public class WertungTest {

	@Test
	public void valueOfStringIgnoringCase_success() {
		assertEquals(Wertung.f, Wertung.valueOfStringIgnoringCase("f"));
		assertEquals(Wertung.f, Wertung.valueOfStringIgnoringCase("F"));

		assertEquals(Wertung.n, Wertung.valueOfStringIgnoringCase("n"));
		assertEquals(Wertung.n, Wertung.valueOfStringIgnoringCase("N"));

		assertEquals(Wertung.r, Wertung.valueOfStringIgnoringCase("r"));
		assertEquals(Wertung.r, Wertung.valueOfStringIgnoringCase("R"));
	}

	@Test
	public void valueOfStringIgnoringCase_parameter_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			Wertung.valueOfStringIgnoringCase(null);
		});
		assertEquals("str darf nicht null sein", ex.getMessage());
	}

	@Test
	public void valueOfStringIgnoringCase_parameter_leer() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			Wertung.valueOfStringIgnoringCase("");
		});
		assertEquals("No enum constant de.egladil.mkv.persistence.domain.enums.Wertung.", ex.getMessage());

	}

	@Test
	public void valueOfStringIgnoringCase_parameter_nicht_fnr() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			Wertung.valueOfStringIgnoringCase("A");
		});
		assertEquals("No enum constant de.egladil.mkv.persistence.domain.enums.Wertung.a", ex.getMessage());

	}
}
