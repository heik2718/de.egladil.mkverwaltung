//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.file.VerzeichnisAuflistenCommand;

/**
 * VerzeichnisAuflistenCommandTest
 */
public class VerzeichnisAuflistenCommandTest {

	@Test
	public void listFilesAndLogIOException_gibt_null_zurueck_wenn_verzeichnis_nicht_existiert() {
		// Arrange
		final String path = "/diesen/pfad/gibt/es/nicht";

		// Act
		final List<File> files = new VerzeichnisAuflistenCommand().listFilesAndLogIOException(path);

		// Assert
		assertNull(files);
	}

	@Test
	public void listFilesAndLogIOException_gibt_nicht_null_zurueck() {
		// Arrange
		final String path = "/home/heike/Downloads";

		// Act
		final List<File> files = new VerzeichnisAuflistenCommand().listFilesAndLogIOException(path);

		// Assert
		assertNotNull(files);
	}

	@Test
	public void listFilesAndLogIOException_gibt_null_zurueck_wenn_keine_permission() {
		// Arrange
		final String path = "/root/nicht_loeschen/";

		// Act
		final List<File> files = new VerzeichnisAuflistenCommand().listFilesAndLogIOException(path);

		// Assert
		assertNull(files);
	}
}
