//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.common.exception.ResourceNotFoundException;

/**
 * DateiartTest
 */
public class DateiartTest {

	@Test
	public void getFileName_klasse_1() {
		// Arrange
		final String jahr = "2014";
		final String expected = "2014_minikaenguru_klasse_1.zip";

		// Act
		final String actual = Dateiart.UNTERLAGEN_KLASSE_1.getFileName(jahr);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getFileName_klasse_2() {
		// Arrange
		final String jahr = "2014";
		final String expected = "2014_minikaenguru_klasse_2.zip";

		// Act
		final String actual = Dateiart.UNTERLAGEN_KLASSE_2.getFileName(jahr);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getFileName_throws_IllegalArgumenrException_when_jahr_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			Dateiart.UNTERLAGEN_KLASSE_1.getFileName(null);
		});
		assertEquals("Parameter wettbewerbsjahr darf nicht null sein", ex.getMessage());
	}

	@Test
	public void valueOfQueryParameter_klasse_1() {
		// Act + Assert
		assertEquals(Dateiart.UNTERLAGEN_KLASSE_1, Dateiart.valueOfQueryParameter("klasse1"));
	}

	@Test
	public void valueOfQueryParameter_klasse_2() {
		// Act + Assert
		assertEquals(Dateiart.UNTERLAGEN_KLASSE_2, Dateiart.valueOfQueryParameter("klasse2"));
	}

	@Test
	public void valueOfQueryParameter_throws_IllegalArgumentException_wenn_parameter_unbekannt() {
		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			Dateiart.valueOfQueryParameter("klasse11");
		});
		assertEquals("keine Dateiart mit [queryParameter=]klasse11] bekannt", ex.getMessage());

	}

	@Test
	public void valueOfQueryParameter_throws_IllegalArgumentException_wenn_parameter_null() {
		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			Dateiart.valueOfQueryParameter(null);
		});
		assertEquals("keine Dateiart mit [queryParameter=]null", ex.getMessage());

	}
}
