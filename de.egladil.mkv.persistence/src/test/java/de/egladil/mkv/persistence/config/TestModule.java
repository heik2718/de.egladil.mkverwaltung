//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenAuthenticator;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenDao;
import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.bv.aas.impl.AuthenticationServiceImpl;
import de.egladil.bv.aas.impl.BenutzerServiceImpl;
import de.egladil.bv.aas.impl.RegistrierungServiceImpl;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.kataloge.impl.KatalogService;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;
import de.egladil.mkv.persistence.service.impl.AdvFacadeImpl;
import de.egladil.mkv.persistence.service.impl.AuswertungsgruppenServiceImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerFacadeImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerServiceImpl;
import io.dropwizard.auth.Authenticator;

/**
 * MKVModule
 */
public class TestModule extends AbstractModule {

	private final String configRoot;

	/**
	 * Erzeugt eine Instanz von TestModule
	 */
	public TestModule(final String configRoot) {
		this.configRoot = configRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());
		install(new BVPersistenceModule(this.configRoot));
		install(new MKVPersistenceModule(this.configRoot));

		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class);
		bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);
		bind(IBenutzerService.class).to(BenutzerServiceImpl.class);

		bind(IRegistrierungService.class).to(RegistrierungServiceImpl.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);

		bind(IAccessTokenDAO.class).to(CachingAccessTokenDao.class);

		bind(IEgladilConfiguration.class).to(PersistenceTestConfiguration.class);
		bind(AuswertungsgruppenService.class).to(AuswertungsgruppenServiceImpl.class);
		bind(IProtokollService.class).to(ProtokollService.class);

		bind(TeilnehmerService.class).to(TeilnehmerServiceImpl.class);
		bind(AdvFacade.class).to(AdvFacadeImpl.class);
		bind(TeilnehmerFacade.class).to(TeilnehmerFacadeImpl.class);

		bind(IKatalogService.class).to(KatalogService.class);
	}

	/**
	 *
	 * @param binder
	 */
	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", this.configRoot);
		Names.bindProperties(binder, properties);
	}
}
