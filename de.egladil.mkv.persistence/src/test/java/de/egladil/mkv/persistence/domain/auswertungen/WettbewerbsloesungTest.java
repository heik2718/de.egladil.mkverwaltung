//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * WettbewerbsloesungTest
 */
public class WettbewerbsloesungTest {

	@Test
	public void same_object_is_equal() {
		final Wettbewerbsloesung l1 = new Wettbewerbsloesung();
		l1.setJahr("2017");
		l1.setKlasse(Klassenstufe.EINS);

		// Assert
		assertEquals(l1, l1);
	}

	@Test
	public void same_jahr_und_klasse_is_equal() {
		final Wettbewerbsloesung l1 = new Wettbewerbsloesung();
		l1.setJahr("2017");
		l1.setKlasse(Klassenstufe.EINS);

		final Wettbewerbsloesung l2 = new Wettbewerbsloesung();
		l2.setJahr("2017");
		l2.setKlasse(Klassenstufe.EINS);

		// Assert
		assertEquals(l2, l1);
		assertEquals(l2.hashCode(), l1.hashCode());
	}

	@Test
	public void different_jahr_is_not_equal() {
		final Wettbewerbsloesung l1 = new Wettbewerbsloesung();
		l1.setJahr("2017");
		l1.setKlasse(Klassenstufe.EINS);

		final Wettbewerbsloesung l2 = new Wettbewerbsloesung();
		l2.setJahr("2016");
		l2.setKlasse(Klassenstufe.EINS);

		// Assert
		assertFalse(l1.equals(l2));
	}
}
