//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * CreateLoesungszettelRohdatenCommandTest
 */
public class CreateLoesungszettelRohdatenCommandTest {

	@Test
	@DisplayName("IllegalArgumentException wenn teilnehmerPayload null")
	void konstruktorTeilnehmerPayloadNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new CreateLoesungszettelRohdatenCommand(null, new Wettbewerbsloesung());
		});
		assertEquals("teilnehmerPayload null", ex.getMessage());

	}

	@Test
	@DisplayName("IllegalArgumentException wenn dao null")
	void konstruktorWettbewerbsloesungNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new CreateLoesungszettelRohdatenCommand(new PublicTeilnehmer(), null);
		});
		assertEquals("wettbewerbsloesung null", ex.getMessage());

	}

	@Test
	@DisplayName("IllegalStateException wenn get before execute")
	void getBeforeExecute() {
		final Throwable ex = assertThrows(IllegalStateException.class, () -> {
			new CreateLoesungszettelRohdatenCommand(new PublicTeilnehmer(), new Wettbewerbsloesung()).getLoesungszettelRohdaten();
		});
		assertEquals("command must be executed before: please call command.execute()", ex.getMessage());
	}

	@Test
	@DisplayName("Erzeugt LoesungszettelRohdaten Klasse 1 korrekt")
	public void executeKlasse1Klappt() {
		// Arrange
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.EINS));
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("e", 5), new PublicAntwortbuchstabe("a", 1), new PublicAntwortbuchstabe("c", 3),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("d", 4), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("-", 0) };

		teilnehmerPayload.setAntworten(antwortbuchstaben);

		final String loesungscode = "CDADECCCBCDE";
		final String expectedNutzereingabe = "A-Abeac-dbA-";
		final String expectedAntwortcode = "ANABEACNDBAN";
		final String berechneterWertungscode = "fnrfrfrnfffn";

		final Wettbewerbsloesung wettbewerbsloesung = new Wettbewerbsloesung();
		wettbewerbsloesung.setLoesungscode(loesungscode);

		final CreateLoesungszettelRohdatenCommand command = new CreateLoesungszettelRohdatenCommand(teilnehmerPayload,
			wettbewerbsloesung);

		// Act
		command.execute();
		final LoesungszettelRohdaten daten = command.getLoesungszettelRohdaten();

		// Assert
		assertEquals(expectedNutzereingabe, daten.getNutzereingabe());
		assertEquals(expectedAntwortcode, daten.getAntwortcode());
		assertEquals(Auswertungsquelle.ONLINE, daten.getAuswertungsquelle());
		assertEquals(berechneterWertungscode, daten.getWertungscode());
		assertEquals(Klassenstufe.EINS, daten.getKlassenstufe());
		assertEquals(Integer.valueOf(1), daten.getKaengurusprung());
		assertEquals(Integer.valueOf(1675), daten.getPunkte());
	}

	@Test
	@DisplayName("Erzeugt LoesungszettelRohdaten Klasse 2 korrekt")
	public void executeKlasse2Klappt() {
		// Arrange
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("E", 1),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("C", 1), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("e", 5), new PublicAntwortbuchstabe("c", 1), new PublicAntwortbuchstabe("c", 3),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("d", 4), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3),
			new PublicAntwortbuchstabe("B", 3), new PublicAntwortbuchstabe("A", 3) };

		teilnehmerPayload.setAntworten(antwortbuchstaben);

		final String loesungscode = "EACDDCDCCEBACBA";
		final String expectedNutzereingabe = "EACbecc-dbA-CBA";
		final String expectedAntwortcode = "EACBECCNDBANCBA";
		final String berechneterWertungscode = "rrrffrfnfffnrrr";

		final Wettbewerbsloesung wettbewerbsloesung = new Wettbewerbsloesung();
		wettbewerbsloesung.setLoesungscode(loesungscode);

		final CreateLoesungszettelRohdatenCommand command = new CreateLoesungszettelRohdatenCommand(teilnehmerPayload,
			wettbewerbsloesung);

		// Act
		command.execute();
		final LoesungszettelRohdaten daten = command.getLoesungszettelRohdaten();

		// Assert
		assertEquals(expectedNutzereingabe, daten.getNutzereingabe());
		assertEquals(expectedAntwortcode, daten.getAntwortcode());
		assertEquals(Auswertungsquelle.ONLINE, daten.getAuswertungsquelle());
		assertEquals(berechneterWertungscode, daten.getWertungscode());
		assertEquals(Klassenstufe.ZWEI, daten.getKlassenstufe());
		assertEquals(Integer.valueOf(3), daten.getKaengurusprung());
		assertEquals(Integer.valueOf(3725), daten.getPunkte());
	}

	@Test
	@DisplayName("Erzeugt LoesungszettelRohdaten Inklusion korrekt")
	public void executeInklusionKlappt() {
		// Arrange
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.IKID));
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("C", 3),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("c", 3) };

		teilnehmerPayload.setAntworten(antwortbuchstaben);

		final String loesungscode = "CACBAB";
		final String expectedNutzereingabe = "CACb-c";
		final String expectedAntwortcode = "CACBNC";
		final String berechneterWertungscode = "rrrrnf";

		final Wettbewerbsloesung wettbewerbsloesung = new Wettbewerbsloesung();
		wettbewerbsloesung.setLoesungscode(loesungscode);

		final CreateLoesungszettelRohdatenCommand command = new CreateLoesungszettelRohdatenCommand(teilnehmerPayload,
			wettbewerbsloesung);

		// Act
		command.execute();
		final LoesungszettelRohdaten daten = command.getLoesungszettelRohdaten();

		// Assert
		assertEquals(expectedNutzereingabe, daten.getNutzereingabe());
		assertEquals(expectedAntwortcode, daten.getAntwortcode());
		assertEquals(Auswertungsquelle.ONLINE, daten.getAuswertungsquelle());
		assertEquals(berechneterWertungscode, daten.getWertungscode());
		assertEquals(Klassenstufe.IKID, daten.getKlassenstufe());
		assertEquals(Integer.valueOf(4), daten.getKaengurusprung());
		assertEquals(Integer.valueOf(2350), daten.getPunkte());
	}

	@Test
	@DisplayName("Erzeugt LoesungszettelRohdaten Inklusion alles falsch")
	public void executeInklusionAlleKreuzeFalschKlappt() {
		// Arrange
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.IKID));
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("C", 3) };

		teilnehmerPayload.setAntworten(antwortbuchstaben);

		final String loesungscode = "CACBAB";
		final String expectedNutzereingabe = "ABBACC";
		final String expectedAntwortcode = "ABBACC";
		final String berechneterWertungscode = "ffffff";

		final Wettbewerbsloesung wettbewerbsloesung = new Wettbewerbsloesung();
		wettbewerbsloesung.setLoesungscode(loesungscode);

		final CreateLoesungszettelRohdatenCommand command = new CreateLoesungszettelRohdatenCommand(teilnehmerPayload,
			wettbewerbsloesung);

		// Act
		command.execute();
		final LoesungszettelRohdaten daten = command.getLoesungszettelRohdaten();

		// Assert
		assertEquals(expectedNutzereingabe, daten.getNutzereingabe());
		assertEquals(expectedAntwortcode, daten.getAntwortcode());
		assertEquals(Auswertungsquelle.ONLINE, daten.getAuswertungsquelle());
		assertEquals(berechneterWertungscode, daten.getWertungscode());
		assertEquals(Klassenstufe.IKID, daten.getKlassenstufe());
		assertEquals(Integer.valueOf(0), daten.getKaengurusprung());
		assertEquals(Integer.valueOf(0), daten.getPunkte());
	}
}
