//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.ValidationUtils;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;

/**
 * @author heikew
 *
 */
public class MailadresseAendernPayloadTest {

	private static final Logger LOG = LoggerFactory.getLogger(MailadresseAendernPayloadTest.class);

	private Validator validator;

	private MailadresseAendernPayload payload;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		payload = TestUtils.validMailadresseAendernPayload();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_throws_when_parameter_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			validator.validate(null);
		});
		assertEquals("HV000116: The object to be validated must not be null.", ex.getMessage());
	}

	@Test
	public void validate_fails_when_benutzername_null() {
		// Arrange
		payload.setUsername(null);

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("username"));
	}

	@Test
	public void validate_fails_when_benutzername_doesnot_match_pattern() {
		// Arrange
		payload.setUsername("hallo-hallo@hallo.@hallo");

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("username"));
	}

	@Test
	public void validate_fails_when_benutzername_not_an_email() {
		// Arrange
		payload.setUsername("http://www.evil-link.com");

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Set<String> invalidProperties = new HashSet<>();
		final Iterator<ConstraintViolation<MailadresseAendernPayload>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<MailadresseAendernPayload> cv = iter.next();
			invalidProperties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, invalidProperties.size());
		assertEquals("username", invalidProperties.iterator().next());
	}

	@Test
	public void validate_fails_when_passwort_invalid() {
		// Arrange
		payload.setPassword("Grafц<KOKS>1");

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<MailadresseAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("password", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_passwort_null() {
		// Arrange
		payload.setPassword(null);

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<MailadresseAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("password", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_email_null() {
		// Arrange
		payload.setEmail(null);

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	public void validate_fails_when_email_doesnot_match_pattern() {
		// Arrange
		payload.setEmail("hallo-hallo@hallo.@hallo");

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	public void validate_fails_when_email_not_an_email() {
		// Arrange
		payload.setEmail("http://www.evil-link.com");

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Set<String> invalidProperties = new HashSet<>();
		final Iterator<ConstraintViolation<MailadresseAendernPayload>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<MailadresseAendernPayload> cv = iter.next();
			invalidProperties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, invalidProperties.size());
		assertEquals("email", invalidProperties.iterator().next());
	}

	@Test
	public void validate_fails_when_benutzername_equals_email() {
		// Arrange
		payload.setEmail(payload.getUsername().toUpperCase());

		// Act + Assert
		final Set<ConstraintViolation<MailadresseAendernPayload>> errors = validator.validate(payload);

		assertEquals(1, errors.size());

		final ConstraintViolation<MailadresseAendernPayload> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
	}
}
