//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;

/**
 * BerechnePunkteCommandValidationDecoratorTest
 */
public class BerechnePunkteCommandValidationDecoratorTest {

	private BerechnePunkteCommandValidationDecorator commandDecorator;

	@BeforeEach
	public void setUp() {
		this.commandDecorator = new BerechnePunkteCommandValidationDecorator(new BerechnePunkteKlasseZweiCommand(), 15);

	}

	@Test
	public void berechne_throws_NullPointerException_wenn_wertungscode_null() {
		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			commandDecorator.berechne(null);
		});
		assertEquals("wertungscode null", ex.getMessage());
	}

	@Test
	public void berechne_throws_MKAuswertungException_wenn_wertungscode_invalid() {
		// Act + Assert
		final Throwable ex = assertThrows(MKAuswertungenException.class, () -> {
			commandDecorator.berechne("ferrnfrrfnnrfff");
		});

		assertEquals("unerlaubtes Zeichen e im Wertungscode: index=1", ex.getMessage());
	}

	@Test
	public void berechne_throws_IndexOutOfBoundsException_wenn_wertungscode_zu_kurz() {
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			commandDecorator.berechne("frrnfrrfnnrfff");
		});

		assertEquals("wertungscode muss die Länge 15 haben.", ex.getMessage());
	}

	@Test
	public void berechne_throws_IndexOutOfBoundsException_wenn_wertungscode_zu_lang() {
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			commandDecorator.berechne("frrnfrrfnnrfffrn");
		});

		assertEquals("wertungscode muss die Länge 15 haben.", ex.getMessage());
	}
}
