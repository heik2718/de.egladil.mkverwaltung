//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerAendererTest
 */
public class TeilnehmerAendererTest {

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final String TEILNAHMEKUERZEL = "GTRF67RC";

	private static final String JAHR = "2018";

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private static final String ALTER_VORNAME = "Emilia Ccharlotta";

	private static final String ALTER_NACHNAME = "Zimmmermann";

	private String generiertesKuerzel;

	private Teilnehmer teilnehmer;

	private TeilnehmerAenderer aenderer;

	@BeforeEach
	void setUp() {
		teilnehmer = new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR), KLASSENSTUFE)
			.vorname(ALTER_VORNAME).nachname(ALTER_NACHNAME).checkAndBuild();
		generiertesKuerzel = teilnehmer.getKuerzel();
		aenderer = new TeilnehmerAenderer(teilnehmer);
	}

	@Test
	@DisplayName("konstruktor: IllegalArgumentException wenn parameter null")
	void konstructor() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerAenderer(null);
		});
		assertEquals("teilnehmer erforderlich", ex.getMessage());
	}

	@Nested
	@DisplayName("fluent methoden normalisieren die neuen attribute")
	class ChangeVornameNachname {

		@Test
		@DisplayName("normalisierter vorname bleibt unverändert")
		void vornameNormalisiert() {
			final String vorname = "Emilia Charlotta";

			final TeilnehmerAenderer result = aenderer.vorname(vorname);
			assertEquals(vorname, result.getVorname());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen in vorname werden getrimt")
		void vornameTrim() {
			final String vorname = "  Emilia Charlotta  ";
			final String expected = "Emilia Charlotta";

			final TeilnehmerAenderer result = aenderer.vorname(vorname);
			assertEquals(expected, result.getVorname());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen in vorname werden getrimt, innere leerzeichen geschrumpft")
		void vornameTrimShrink() {
			final String vorname = "  Emilia   Charlotta  ";
			final String expected = "Emilia Charlotta";

			final TeilnehmerAenderer result = aenderer.vorname(vorname);
			assertEquals(expected, result.getVorname());
		}

		@Test
		@DisplayName("keine Exception wenn Vorname null")
		void vornameNull() {
			final String vorname = null;

			final TeilnehmerAenderer result = aenderer.vorname(vorname);
			assertNull(result.getVorname());
		}

		@Test
		@DisplayName("normalisierter nachname bleibt unverändert")
		void nachnameNormalisiert() {
			final String nachname = "Zimmermann";

			final TeilnehmerAenderer result = aenderer.nachname(nachname);
			assertEquals(nachname, result.getNachname());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen in nachname werden getrimt")
		void nachnameTrim() {
			final String nachname = "  Zimmermann  ";
			final String expected = "Zimmermann";

			final TeilnehmerAenderer result = aenderer.nachname(nachname);
			assertEquals(expected, result.getNachname());
		}

		@Test
		@DisplayName("keine Exception wenn nachname null")
		void nachnameNull() {
			final String nachname = null;

			final TeilnehmerAenderer result = aenderer.nachname(nachname);
			assertNull(result.getNachname());
		}
	}

	@Nested
	@DisplayName("Tests für checkAndChange")
	class CheckAndChange {
		@Test
		@DisplayName("IllegalArgrumentException wenn vorname und nachname null sind")
		void vornameNachnameNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.checkAndChange();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname blank und nachname null")
		void vornameBlankNachnameNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.vorname(" ").checkAndChange();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname null und nachname blank")
		void vornameNullNachnameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.nachname(" ").checkAndChange();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname und nachname blank sind")
		void vornameNachnameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.vorname("").nachname(" ").checkAndChange();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname zu lang, nachname blank")
		void vornameZuLangNachnameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.vorname("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilch").nachname(" ")
					.checkAndChange();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn nachname zu lang, vorname blank")
		void nachnameZuLangVornameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.nachname("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilch").vorname(" ")
					.checkAndChange();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn zusammengesetzter Name zu lang")
		void zusammenesetzterNameZuLang() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.nachname("Universums aus denen alle ").vorname("chemischenElemente aufgebaut sind Teilchen für Teilch")
					.checkAndChange();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("nur vorname und nachname werden geändert")
		void buildInitialisiertKuerzel() {
			final String vorname = "  Emilia   Charlotta  ";
			final String expectedVorname = "Emilia Charlotta";

			final String nachname = "  Zimmermann  ";
			final String expectedNachname = "Zimmermann";

			final Teilnehmer teilnehmer = aenderer.vorname(vorname).nachname(nachname).checkAndChange();

			assertEquals(generiertesKuerzel, teilnehmer.getKuerzel());
			assertEquals(expectedVorname, teilnehmer.getVorname());
			assertEquals(expectedNachname, teilnehmer.getNachname());
			assertEquals(JAHR, teilnehmer.getJahr());
			assertEquals(TEILNAHMEART, teilnehmer.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, teilnehmer.getTeilnahmekuerzel());
			assertNull(teilnehmer.getZusatz());
		}

		@Test
		@DisplayName("nur vorname, nachname und zusatz werden geändert")
		void buildInitialisiertKuerzelUndZusatz() {
			final String vorname = "  Emilia   Charlotta  ";
			final String expectedVorname = "Emilia Charlotta";

			final String nachname = "  Zimmermann  ";
			final String expectedNachname = "Zimmermann";

			final String zusatz = " die vom    Fenster ";
			final String expectedZusatz = "die vom Fenster";

			final Teilnehmer teilnehmer = aenderer.vorname(vorname).nachname(nachname).zusatz(zusatz).checkAndChange();

			assertEquals(generiertesKuerzel, teilnehmer.getKuerzel());
			assertEquals(expectedVorname, teilnehmer.getVorname());
			assertEquals(expectedNachname, teilnehmer.getNachname());
			assertEquals(expectedZusatz, teilnehmer.getZusatz());
			assertEquals(JAHR, teilnehmer.getJahr());
			assertEquals(TEILNAHMEART, teilnehmer.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, teilnehmer.getTeilnahmekuerzel());
		}
	}
}
