//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.TestUtils;

/**
 * DateiSuchenCommandTest
 */
public class DateiSuchenCommandTest {

	private List<String> verzeichnisse = Arrays.asList(new String[] { TestUtils.getDevUploadDir(), TestUtils.getDevSandboxDir() });

	@Test
	public void constructor_throws_NullPointerException() {
		assertThrows(NullPointerException.class, () -> {
			new DateiSuchenCommand(null);
		});
	}

	@Test // (expected = NullPointerException.class)
	public void gefunden_throws_NullPointerException_wenn_dateiname_null() {
		assertThrows(NullPointerException.class, () -> {
			new DateiSuchenCommand(verzeichnisse).gefunden(null, new DateiValidationStrategie());
		});

	}

	@Test // (expected = NullPointerException.class)
	public void gefunden_throws_NullPointerException_wenn_validationStrategie_null() {
		assertThrows(NullPointerException.class, () -> {
			new DateiSuchenCommand(verzeichnisse).gefunden("hallo.ods", null);
		});

	}

	@Test
	public void gefunden_returns_false() {
		// Act
		final boolean gefunden = new DateiSuchenCommand(verzeichnisse).gefunden("hallo.ods", new DateiValidationStrategie());

		// Assert
		assertFalse(gefunden);
	}

	@Test
	public void gefunden_returns_true_mkv() {
		// Act
		final boolean gefunden = new DateiSuchenCommand(verzeichnisse).gefunden("existing.ods", new DateiValidationStrategie());

		// Assert
		assertTrue(gefunden);
	}

	@Test
	public void gefunden_returns_true_sandbox() {
		// Act
		final boolean gefunden = new DateiSuchenCommand(verzeichnisse).gefunden(
			"existing.unknown", new DateiValidationStrategie());

		// Assert
		assertTrue(gefunden);
	}
}
