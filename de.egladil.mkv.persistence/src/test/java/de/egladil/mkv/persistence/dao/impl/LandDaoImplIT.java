//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * LandDaoImplIT
 */
public class LandDaoImplIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(LandDaoImplIT.class);

	private static final List<String> letters = Arrays.asList(new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" });

	@Inject
	private ILandDao dao;

	@Inject
	private KuerzelGenerator kuerzelGenerator;

	@Test
	public void anlegen_aendern_keine_dublette_soll_klappen() {
		// Arrange 1
		final Land land = new Land();
		final String millis = "" + System.currentTimeMillis();
		land.setName("Hurz-" + millis);

		final String kuerzel = randomString(5);
		land.setKuerzel(kuerzel);

		LOG.debug("persistieren Land {}", land);

		// Act 1
		final Land persistent = dao.persist(land);

		// Assert 1
		final Long id = persistent.getId();
		assertNotNull(id);

		// Arrange 2
		// dao = new LandDaoImpl();
		final Land dublette = new Land();
		dublette.setName(land.getName());
		dublette.setKuerzel(kuerzel);

		// Act 2 + Assert 2
		try {
			dao.persist(dublette);
			fail();
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optDup = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			assertTrue(optDup.isPresent());
			final EgladilDuplicateEntryException dupEx = optDup.get();
			LOG.debug("Doppelter Einrag: {}", dupEx.getUniqueIndexName());
			assertEquals("uk_laender_1", dupEx.getUniqueIndexName());
		}

		// Arrange 3
		persistent.setName("geändert-" + millis);

		// Act 3
		dao.persist(persistent);
	}

	@Test
	public void persist_validation_laenge_kuerzel_klappt() {
		// Arrange
		final Land land = new Land();
		land.setName("zu langes kürzel");
		land.setKuerzel("ADFRED");

		// Act + Assert
		try {
			dao.persist(land);
			fail("keine ConstraintViolationException");
		} catch (final ConstraintViolationException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void persist_validation_laenge_bezeichnung_klappt() {
		// Arrange
		final Land land = new Land();
		land.setName("zu lange Bezeichnung, zuuuu lange Bezeichnung zu lange Bezeichnung, zuuuu lange Bezeichnung wirklich!");
		land.setKuerzel("ADFRE");

		// Act + Assert
		try {
			dao.persist(land);
			fail("keine ConstraintViolationException");
		} catch (final ConstraintViolationException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void findLandByBezeichnung_soll_klappen_wenn_parameter_leer() {
		// Arrange
		// Act
		final List<Land> laender = dao.findLandByName(" ");

		// Assert
		assertFalse(laender.isEmpty());
	}

	@Test
	public void findLandByBezeichnung_exception_wenn_parameter_null() {
		final Throwable exception = assertThrows(MKVException.class, () -> {
			dao.findLandByName(null);
		});
		assertEquals("bezeichnung darf nicht null sein!", exception.getMessage());
	}

	@Test
	public void findLandByBezeichnung_soll_nicht_case_sensitiv_suchen() {
		// Arrange
		// Act
		final List<Land> laender = dao.findLandByName("Ändert");

		// Assert
		assertFalse(laender.isEmpty());
		final Land erstes = laender.get(0);
		assertTrue(erstes.getOrte().isEmpty());
	}

	@Test
	public void delete_soll_klappen() {
		// Arrange
		final List<Land> laender = dao.findLandByName("geändert");

		// Act, falls was zum Löschen da ist
		if (laender.size() > 2) {
			final Land zuLoeschen = laender.get(0);
			final Long id = zuLoeschen.getId();

			dao.delete(zuLoeschen);

			final Optional<Land> nichtDa = dao.findById(Land.class, id);

			// Assert
			assertFalse(nichtDa.isPresent());
		}
	}

	@Test
	public void persist_cascading_klappt() {
		// Arrange
		final Land land = new Land();
		final String millis = "" + System.currentTimeMillis();
		land.setName("Hurz-" + millis);

		final String kuerzel = randomString(5);
		land.setKuerzel(kuerzel);

		LOG.debug("persistieren Land {}", land);

		final Ort ort = new Ort();
		ort.setName("Ort-" + millis);
		ort.setKuerzel(kuerzelGenerator.generateDefaultKuerzel());

		final Schule schule = new Schule();
		schule.setName("Schule - " + millis);
		schule.setKuerzel(kuerzelGenerator.generateDefaultKuerzel());

		ort.addSchule(schule);
		land.addOrt(ort);

		// Act
		final Land persisted = dao.persist(land);

		// Assert
		assertNotNull(persisted.getId());
		assertNotNull(ort.getId());
		assertNotNull(schule.getId());
	}

	@Test
	public void findAllFreigeschaltet_klappt() {
		// Act
		final List<Land> laender = dao.findAllFreigeschaltet();

		// Assert
		assertFalse(laender.isEmpty());
		for (final Land land : laender) {
			assertTrue("Fehler bei Land " + land.toString(), land.isFreigeschaltet());
			land.getOrte();
		}

	}

	private String randomString(final int length) {
		String bla = "";
		for (int i = 0; i < length; i++) {
			bla += "9";
		}

		String rand = "" + new Random().nextInt(Integer.valueOf(bla));
		rand = StringUtils.leftPad(rand, length, '0');

		String kuerzel = "";

		for (int i = 0; i < rand.length(); i++) {
			final int index = Integer.valueOf(rand.substring(i, i + 1));
			kuerzel += letters.get(index);
		}

		return kuerzel;
	}

	@Test
	public void findByUniqueKey_klappt() {
		// Arrange
		final String kuerzel = "de-BE";

		// Act
		final Optional<Land> land = dao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertTrue(land.isPresent());
		assertFalse(land.get().getOrte().isEmpty());
	}

	@Test
	public void findByUniqueKey_keinTreffer() {
		// Arrange
		final String kuerzel = "6X0ILV";
		// Act
		final Optional<Land> land = dao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertFalse(land.isPresent());
	}

	@Test
	void getLandZuOrt() {
		// Arrange
		final Ort ort = new Ort();
		ort.setKuerzel("PAMT7UXS");

		// Act
		final PublicLand pl = dao.getFuerOrt(ort);

		// Assert
		assertEquals("de-BB", pl.getKuerzel());
		assertEquals("Brandenburg", pl.getName());
	}
}
