//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;

/**
 * FindLehrerteilnahmeJahrFilterTest
 */
public class FindLehrerteilnahmeJahrFilterTest {

	@Test
	public void findAktuelleTeilnahme_throws_NPE_when_teilnahmen_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new FindLehrerteilnahmeJahrFilter("2017").findTeilnahme(null);
		});
		assertEquals("lehrerteilnahmen", ex.getMessage());
	}

	@Test
	public void constructor_throws_NPE_when_jahr_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new FindLehrerteilnahmeJahrFilter(null);
		});
		assertEquals("jahr", ex.getMessage());
	}

	@Test
	public void findAktuelleTeilnahme_treffer() {
		// Arrange
		final List<LehrerteilnahmeInformation> teilnahmen = new ArrayList<>();
		final String expectedKuerzel = "BBBBBBBB";
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel("AAAAAAAA");
			info.setJahr("2016");
			teilnahmen.add(info);
		}
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel(expectedKuerzel);
			info.setJahr("2017");
			teilnahmen.add(info);
		}

		// Act
		final LehrerteilnahmeInformation treffer = new FindLehrerteilnahmeJahrFilter("2017").findTeilnahme(teilnahmen);

		// Assert
		assertNotNull(treffer);
		assertEquals(expectedKuerzel, treffer.getKuerzel());
	}

	@Test
	public void findAktuelleTeilnahme_kein_treffer() {
		// Arrange
		final List<LehrerteilnahmeInformation> teilnahmen = new ArrayList<>();
		final String expectedKuerzel = "BBBBBBBB";
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel("AAAAAAAA");
			info.setJahr("2016");
			teilnahmen.add(info);
		}
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel(expectedKuerzel);
			info.setJahr("2017");
			teilnahmen.add(info);
		}

		// Act
		final LehrerteilnahmeInformation treffer = new FindLehrerteilnahmeJahrFilter("2018").findTeilnahme(teilnahmen);

		// Assert
		assertNull(treffer);
	}

}
