//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;

/**
 * AuswertungsgruppeRootBuilderTest
 */
public class AuswertungsgruppeRootBuilderTest {

	private static final String TEILNAHMEKUERZEL = "GTFKL987";

	private static final String JAHR = "2018";

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private AuswertungsgruppeBuilder rootBuilder;

	@BeforeEach
	void setUp() {
		rootBuilder = new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR);
	}

	@Nested
	@DisplayName("Tests zur Initialisierung des builders")
	class InitTests {

		@Test
		@DisplayName("IllegalArgumentException wenn teilnahmeart null")
		void teilnahmeartNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeBuilder(null, TEILNAHMEKUERZEL, JAHR);
			});

			assertEquals("teilnahmeart ist erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn teilnahmekuerzel null")
		void teilnahmekuerzelNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeBuilder(TEILNAHMEART, null, JAHR);
			});

			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn teilnahmekuerzel blank")
		void teilnahmekuerzelBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeBuilder(TEILNAHMEART, " ", JAHR);
			});

			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn jahr null")
		void jahrNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, null);
			});

			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn jahr blank")
		void jahrBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, " ");
			});

			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("normalisierter Schulname bleibt unverändert")
		void initNameNormal() {
			rootBuilder.name("Krautgartenschule");
			assertEquals("Krautgartenschule", rootBuilder.getNeuerName());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen werden entfernt")
		void initNameFuehrendeUndEndendeLeerzeichen() {
			rootBuilder.name(" Krautgartenschule  ");
			assertEquals("Krautgartenschule", rootBuilder.getNeuerName());
		}

		@Test
		@DisplayName("innere Leerzeichen werden geschrumpft")
		void initInnereLeerzeichen() {
			rootBuilder.name("Krautgartenschule  Mainz-Kostheim");
			assertEquals("Krautgartenschule Mainz-Kostheim", rootBuilder.getNeuerName());
		}
	}

	@Nested
	@DisplayName("Tests für checkAndBuild")
	class BuildTests {
		@Test
		@DisplayName("IllegalArgrumentException wenn zusammengesetzter Name zu lang")
		void zusammenesetzterNameZuLang() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				rootBuilder.name("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilchen zu Kette")
					.checkAndBuild();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("erzeugte Instanz hat alle Attribute")
		void buildInnereLeerzeichen() {
			final Auswertungsgruppe result = rootBuilder.name("Krautgartenschule  Mainz-Kostheim").checkAndBuild();
			assertEquals("Krautgartenschule Mainz-Kostheim", result.getName());
			assertEquals(TEILNAHMEART, result.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, result.getTeilnahmekuerzel());
			assertEquals(JAHR, result.getJahr());
			assertEquals(22, result.getKuerzel().length());
			assertNull(result.getParent());
		}

		@Test
		@DisplayName("name darf null bleiben")
		void buildOhneName() {
			final Auswertungsgruppe result = rootBuilder.checkAndBuild();
			assertNull(result.getName());
			assertEquals(TEILNAHMEART, result.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, result.getTeilnahmekuerzel());
			assertEquals(JAHR, result.getJahr());
			assertEquals(22, result.getKuerzel().length());
			assertNull(result.getParent());
		}
	}
}
