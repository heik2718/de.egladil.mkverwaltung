//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;

/**
 * AuswertungsgruppeChildBuilderTest
 */
public class AuswertungsgruppeChildBuilderTest {

	private static final String TEILNAHMEKUERZEL = "GTFKL987";

	private static final String JAHR = "2018";

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private Auswertungsgruppe parent;

	private AuswertungsgruppeChildBuilder builder;

	@BeforeEach
	void setUp() {
		parent = new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR).name("Blumenwiesenschule").checkAndBuild();
		builder = new AuswertungsgruppeChildBuilder(parent, KLASSENSTUFE);
	}

	@Nested
	@DisplayName("Tests zur Initialisierung des builders")
	class InitTests {

		@Test
		@DisplayName("IllegalArgumentException wenn parent null")
		void teilnahmeartNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeChildBuilder(null, KLASSENSTUFE);
			});

			assertEquals("parent ist erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn klassenstufe null")
		void teilnahmekuerzelNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AuswertungsgruppeChildBuilder(parent, null);
			});

			assertEquals("klassenstufe ist erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("normalisierter Schulname bleibt unverändert")
		void initNameNormal() {
			builder.name("Krautgartenschule");
			assertEquals("Krautgartenschule", builder.getNeuerName());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen werden entfernt")
		void initNameFuehrendeUndEndendeLeerzeichen() {
			builder.name(" Krautgartenschule  ");
			assertEquals("Krautgartenschule", builder.getNeuerName());
		}

		@Test
		@DisplayName("innere Leerzeichen werden geschrumpft")
		void initInnereLeerzeichen() {
			builder.name("Krautgartenschule  Mainz-Kostheim");
			assertEquals("Krautgartenschule Mainz-Kostheim", builder.getNeuerName());
		}
	}

	@Nested
	@DisplayName("Tests für checkAndBuild")
	class BuildTests {
		@Test
		@DisplayName("IllegalArgrumentException wenn zusammengesetzter Name zu lang")
		void zusammenesetzterNameZuLang() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				builder.name("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilchen zu Kette")
					.checkAndBuild();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("erzeugte Instanz hat alle Attribute")
		void buildInnereLeerzeichen() {
			final Auswertungsgruppe result = builder.name("Krautgartenschule  Mainz-Kostheim").checkAndBuild();
			assertEquals("Krautgartenschule Mainz-Kostheim", result.getName());
			assertEquals(TEILNAHMEART, result.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, result.getTeilnahmekuerzel());
			assertEquals(JAHR, result.getJahr());
			assertEquals(22, result.getKuerzel().length());
			assertEquals(KLASSENSTUFE, result.getKlassenstufe());
			assertEquals(parent, result.getParent());
			assertEquals(1, parent.getAuswertungsgruppen().size());
			assertEquals(result, parent.getAuswertungsgruppen().get(0));
		}

		@Test
		@DisplayName("name darf null bleiben")
		void buildOhneName() {
			final Auswertungsgruppe result = builder.checkAndBuild();
			assertEquals("Klassenstufe 2", result.getName());
			assertEquals(TEILNAHMEART, result.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, result.getTeilnahmekuerzel());
			assertEquals(JAHR, result.getJahr());
			assertEquals(22, result.getKuerzel().length());
			assertEquals(parent, result.getParent());
			assertEquals(1, parent.getAuswertungsgruppen().size());
			assertEquals(result, parent.getAuswertungsgruppen().get(0));
		}
	}
}
