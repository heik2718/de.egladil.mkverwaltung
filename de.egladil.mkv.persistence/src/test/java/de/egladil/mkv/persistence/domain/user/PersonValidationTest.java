//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.TestUtils;

/**
 * KontaktdatenValidationTest
 */
public class PersonValidationTest {

	private static final Logger LOG = LoggerFactory.getLogger(PersonValidationTest.class);

	private Validator validator;

	private Person kontaktdaten;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		kontaktdaten = TestUtils.validKontaktdaten();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act
		final Set<ConstraintViolation<Person>> errors = validator.validate(kontaktdaten);

		final Iterator<ConstraintViolation<Person>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<Person> cv = iter.next();
			LOG.info(cv.getPropertyPath().toString() + " " + cv.getMessage());
		}

		// Assert
		assertTrue(errors.isEmpty());
	}
}
