//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.io.InputStream;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * KontextTest
 */
public class KontextTest {

	public static void main(String[] args) {
		try {
			new KontextTest().testDeserialize();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	void testDeserialize() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/mkvapiconf.json")) {
			Kontext kontext = new ObjectMapper().readValue(in, Kontext.class);

			System.out.println(kontext.toString());
		}
	}

}
