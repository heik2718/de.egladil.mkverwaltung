//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;

/**
 * TeilnehmerFacadeIT
 */
public class TeilnehmerFacadeImplTest {

	private TeilnehmerFacade teilnehmerFacade;

	@BeforeEach
	public void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());

		teilnehmerFacade = new TeilnehmerFacadeImpl(Mockito.mock(ILoesungszettelDao.class),
			Mockito.mock(IAuswertungsgruppeDao.class), Mockito.mock(ITeilnehmerDao.class), Mockito.mock(IUploadInfoDao.class),
			Mockito.mock(TeilnehmerService.class));
	}

	@Nested
	@DisplayName("Tests für Loesungszettel")
	class LoesungszettelTests {

		@Test
		@DisplayName("getLoesungszettel throws IllegalArgumentException wenn TeilnahmeIdentifierProvider null")
		void alleLoesungszettelParameterNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnahmeIdentifierProvider provider = null;
				teilnehmerFacade.getLoesungszettel(provider);
			});
			assertEquals("teilnahmeIdentifierProvider null", ex.getMessage());
		}

		@Test
		@DisplayName("getUniqueLoesungszettel throws IllegalArgumentException wenn kuerzel null")
		void einLoesungszettelParameterNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final String kuerzel = null;
				teilnehmerFacade.getUniqueLoesungszettel(kuerzel);
			});
			assertEquals("kuerzel blank", ex.getMessage());
		}

		@Test
		@DisplayName("getUniqueLoesungszettel throws IllegalArgumentException wenn kuerzel null")
		void einLoesungszettelParameterBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final String kuerzel = " ";
				teilnehmerFacade.getUniqueLoesungszettel(kuerzel);
			});
			assertEquals("kuerzel blank", ex.getMessage());
		}

	}

	@Nested
	@DisplayName("Tests für Teilnehmer")
	class TeilnehmerTests {

		@Test
		@DisplayName("getTeilnehmer throws IllegalArgumentException wenn TeilnahmeIdentifierProvider null")
		void parameterNull1() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnahmeIdentifierProvider provider = null;
				teilnehmerFacade.getTeilnehmer(provider);
			});
			assertEquals("teilnahmeIdentifierProvider null", ex.getMessage());
		}

		@Test
		@DisplayName("getTeilnehmer throws IllegalArgumentException wenn TeilnahmeIdentifier null")
		void parameterNull2() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnahmeIdentifier ident = null;
				teilnehmerFacade.getTeilnehmer(ident);
			});
			assertEquals("teilnahmeIdentifier null", ex.getMessage());
		}

		@Test
		@DisplayName("getAnzahlTeilnehmer throws IllegalArgumentException wenn TeilnahmeIdentifierProvider null")
		void anzahlTeilnehmerAktuellParameterNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnahmeIdentifierProvider provider = null;
				teilnehmerFacade.getAnzahlTeilnehmer(provider);
			});
			assertEquals("teilnahmeIdentifierProvider null", ex.getMessage());
		}

		@Test
		@DisplayName("delete: IllegalArgumentExeption wenn benutzerUuid null")
		void deleteUuidNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteTeilnehmer(null, "shadaf");
			});
			assertEquals("benutzerUuid blank", ex.getMessage());
		}

		@Test
		@DisplayName("delete: IllegalArgumentExeption wenn benutzerUuid blank")
		void deleteUuidLeer() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteTeilnehmer(" ", "vashjdf");
			});
			assertEquals("benutzerUuid blank", ex.getMessage());
		}

		@Test
		@DisplayName("delete: IllegalArgumentExeption wenn kuerzel null")
		void deleteKuerzelNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteTeilnehmer("shadaf", null);
			});
			assertEquals("kuerzel blank", ex.getMessage());
		}

		@Test
		@DisplayName("delete: IllegalArgumentExeption wenn kuerzel blank")
		void deleteKuerzelLeer() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteTeilnehmer("vashjdf", " ");
			});
			assertEquals("kuerzel blank", ex.getMessage());
		}

		@Test
		@DisplayName("deleteAll: IllegalArgumentExeption wenn benutzerUuid null")
		void deleteAllUuidNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteAllTeilnehmer(null, TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("afsfa", "2018"));
			});
			assertEquals("benutzerUuid blank", ex.getMessage());
		}

		@Test
		@DisplayName("deleteAll: IllegalArgumentExeption wenn benutzerUuid blank")
		void deleteAllUuidLeer() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteAllTeilnehmer(" ", TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("afsfa", "2018"));
			});
			assertEquals("benutzerUuid blank", ex.getMessage());
		}

		@Test
		@DisplayName("deleteAll: IllegalArgumentExeption wenn teilnahmeIdentifier null")
		void deleteAllTeilnahmeIdentifierNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.deleteAllTeilnehmer("hdai", null);
			});
			assertEquals("teilnahmeIdentifier null", ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für UploadInfos")
	class UploadInfoTests {

		@Test
		@DisplayName("getUploadInfos throws IllegalArgumentException wenn parameter null")
		void parameterNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.getUploadInfos(null);
			});
			assertEquals("teilnahmeIdentifierProvider null", ex.getMessage());
		}
	}
}
