//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.google.inject.Inject;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * TeilnehmerFacadeIT
 */
public class TeilnehmerFacadeIT extends AbstractGuiceIT {

	private static final String KUERZEL_TEILNAHME = "PAMT7UXS";

	private static final String JAHR = "2018";

	private static final String BENUTZER_UUID = "45b7abb6-086e-47b4-93d5-32eae491ea0f";

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private PublicTeilnehmer teilnehmerPayload;

	@Inject
	private TeilnehmerFacade teilnehmerFacade;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);
		teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Schlemmer");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
		teilnehmerPayload.setVorname("Rudi");
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
	}

	@Nested
	@DisplayName("Tests für Loesungszettel")
	class LoesungszettelTests {

		@Test
		@DisplayName("Lösungszettel vergangenes Jahr")
		void loesungszettelAbeschlossenesJahr() {
			// Arrange
			final String jahr = "2017";
			final String teilnahmekuerzel = "4AKICN7Y";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final List<Loesungszettel> alleLoesungszettel = teilnehmerFacade.getLoesungszettel(tiProvider);

			// Assert
			assertFalse(alleLoesungszettel.isEmpty());
		}

		@Test
		@DisplayName("getUniqueLoesungszettel mit kuerzel ein Treffer")
		void getLoesungszettelMitKuerzelTreffer() {
			final String kuerzel = "MA1FB06S20171115181123";

			final Optional<Loesungszettel> opt = teilnehmerFacade.getUniqueLoesungszettel(kuerzel);

			assertTrue(opt.isPresent());
		}

		@Test
		@DisplayName("Anzahl Lösungszettel aktuelles Jahr")
		void anzahlLoesungszettelAktuell() {
			// Arrange CSVQ5NGY
			final String jahr = "2018";
			final String teilnahmekuerzel = "BPN390UP";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final long anzahl = teilnehmerFacade.getAnzahlLoesungszettel(tiProvider);

			// Assert
			assertTrue(anzahl > 0);

		}

	}

	@Nested
	@DisplayName("Tests für Teilnehmer")
	class TeilnehmerTests {

		@Test
		@DisplayName("happy hour für nichtleere Teilnehmermenge")
		void trefferlisteNichtLeer() {
			final String jahr = "2018";
			final String teilnahmekuerzel = "M94P3IH9";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final List<Teilnehmer> trefferliste = teilnehmerFacade.getTeilnehmer(tiProvider);

			// Assert
			assertFalse(trefferliste.isEmpty());
		}

		@Test
		@DisplayName("ganz neuer Teilnehmer")
		void createTeilnehmerNeu() {
			final Teilnehmer teilnehmer = teilnehmerFacade.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

			assertNotNull(teilnehmer.getId());

		}

		@Test
		@DisplayName("Anzahl Teilnehmer Privatteilnahme aktuelles Jahr")
		void anzahlTeilnehmerAktuell() {
			// Arrange CSVQ5NGY
			final String jahr = "2018";
			final String teilnahmekuerzel = "BPN390UP";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final long anzahl = teilnehmerFacade.getAnzahlTeilnehmer(tiProvider);

			// Assert
			assertTrue(anzahl > 0l);
		}
	}

	@Nested
	@DisplayName("Tests für UploadInfos")
	class UploadInfoTests {

		@Test
		@DisplayName("happy hour für nichtleere UploadInfos")
		void trefferlisteNichtLeer() {
			final String jahr = "2017";
			final String teilnahmekuerzel = "4AKICN7Y";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final List<UploadInfo> trefferliste = teilnehmerFacade.getUploadInfos(tiProvider);

			// Assert
			assertFalse(trefferliste.isEmpty());
		}

	}
}
