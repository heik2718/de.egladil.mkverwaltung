//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.plausi;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;

/**
 * ConvertToTeilnahmeartCommandTest
 */
public class ConvertToTeilnahmeartCommandTest {

	@Test
	@DisplayName("keine Exception wenn P")
	void happyHourP() {
		try {
			new ConvertToTeilnahmeartCommand("P").execute();
		} catch (final PreconditionFailedException e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("keine Exception wenn S")
	void happyHourS() {
		try {
			new ConvertToTeilnahmeartCommand("S").execute();
		} catch (final PreconditionFailedException e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("IllegalArgumentException wenn parameter null")
	void constructorParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ConvertToTeilnahmeartCommand(null);
		});
		assertEquals("teilnahmeartName null", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn teilnahmeartName irgendwas")
	void jahrIrgendwas() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ConvertToTeilnahmeartCommand(UUID.randomUUID().toString().substring(0, 4)).execute();
		});
		assertTrue(ex.getMessage().startsWith("No enum constant de.egladil.mkv.persistence.domain.enums.Teilnahmeart"));
	}

	@Test
	void getKlassenstufeVorExcecute() {
		final Throwable ex = assertThrows(IllegalStateException.class, () -> {
			new ConvertToTeilnahmeartCommand("P").getTeilnahmeart();
		});
		assertEquals("IllegalState: please invoke execute() first", ex.getMessage());
	}

	@Test
	@DisplayName("happy hour")
	void nachExecuteGibtsKlassenstufe() {
		final ConvertToTeilnahmeartCommand cmd = new ConvertToTeilnahmeartCommand("P");
		cmd.execute();
		assertEquals(Teilnahmeart.P, cmd.getTeilnahmeart());
	}

}
