//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.berechnungen.AbstractBerechnePunkteCommand;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * BerechnePunkteKlasseZweiCommandTest
 */
public class BerechnePunkteKlasseZweiCommandTest {

	@Test
	public void berechne_rechnet_richtig() {
		// Arrange
		final String wertungscode = "rrffrrffrrnnnnf";
		final int expected = 3125;

		// Act
		final int actual = AbstractBerechnePunkteCommand.createCommand(Klassenstufe.ZWEI).berechne(wertungscode);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void berechne_throws_IndexOutOfBoundsException_wenn_zu_kurz() {
		// Arrange
		final String wertungscode = "rrffrrffrrnnnf";

		// Act + Assert
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			AbstractBerechnePunkteCommand.createCommand(Klassenstufe.ZWEI).berechne(wertungscode);
		});
		assertEquals("wertungscode muss die Länge 15 haben.", ex.getMessage());
	}

	@Test
	public void berechne_throws_IndexOutOfBoundsException_wenn_zu_lang() {
		// Arrange
		final String wertungscode = "rrffrrffrrnnnfrr";

		// Act + Assert
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			AbstractBerechnePunkteCommand.createCommand(Klassenstufe.ZWEI).berechne(wertungscode);
		});
		assertEquals("wertungscode muss die Länge 15 haben.", ex.getMessage());

	}
}
