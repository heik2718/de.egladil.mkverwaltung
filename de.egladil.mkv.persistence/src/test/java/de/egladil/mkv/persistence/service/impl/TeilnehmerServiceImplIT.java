//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.auswertungen.Antworteingabemodus;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.TeilnehmerService;

/**
 * TeilnehmerServiceImplIT
 */
public class TeilnehmerServiceImplIT extends AbstractGuiceIT {

	private static final String KUERZEL_TEILNAHME = "UQWNIRV7";

	private static final String BENUTZER_UUID = "2b47a859-e6c7-4402-b1fd-0ac02984ee79";

	private TeilnahmeIdentifier teilnahmeIdentifier;

	@Inject
	private TeilnehmerService teilnehmerService;

	@Inject
	private ILoesungszettelDao loesunszettelDao;

	@Inject
	private ITeilnehmerDao teilnehmerDao;

	@Inject
	private IProtokollService protokollService;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		final String jahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
		teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(KUERZEL_TEILNAHME, jahr);
	}

	@Test
	void createKlappt() {
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Biene");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
		teilnehmerPayload.setVorname("Maja");
		teilnehmerPayload.setEingabemodus(Antworteingabemodus.MATRIX.toString());
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("E", 5),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("e", 5), new PublicAntwortbuchstabe("c", 3), new PublicAntwortbuchstabe("c", 3),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("d", 4), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3),
			new PublicAntwortbuchstabe("B", 3), new PublicAntwortbuchstabe("A", 3) };

		teilnehmerPayload.setAntworten(antwortbuchstaben);

		final Teilnehmer teilnehmer = teilnehmerService.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

		assertNotNull(teilnehmer.getKuerzel());
		assertEquals(Klassenstufe.ZWEI, teilnehmer.getKlassenstufe());
		assertEquals("Maja", teilnehmer.getVorname());
		assertEquals("Biene", teilnehmer.getNachname());
		assertNotNull(teilnehmer.getLoesungszettelkuerzel());

		assertEquals("ENCBECCNDBANCBA", teilnehmer.getAntwortcode());
		assertEquals(Integer.valueOf(3), teilnehmer.getKaengurusprung());
		assertEquals("34,25", teilnehmer.getPunkte());

		final Optional<Loesungszettel> opt = loesunszettelDao.findByUniqueKey(Loesungszettel.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmer.getLoesungszettelkuerzel());
		assertTrue(opt.isPresent());
		final Loesungszettel loesungszettel = opt.get();
		assertEquals("ENCBECCNDBANCBA", loesungszettel.getLoesungszettelRohdaten().getAntwortcode());
	}

	@Test
	void createOhneAntwortenKlappt() {
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("von Trontheim");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.EINS));
		teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
		teilnehmerPayload.setVorname("Marlisa");

		final Teilnehmer teilnehmer = teilnehmerService.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

		assertNotNull(teilnehmer.getKuerzel());
		assertEquals(Klassenstufe.EINS, teilnehmer.getKlassenstufe());
		assertEquals("Marlisa", teilnehmer.getVorname());
		assertEquals("von Trontheim", teilnehmer.getNachname());
		assertNull(teilnehmer.getLoesungszettelkuerzel());
		assertNull(teilnehmer.getEingabemodus());
	}

	@Test
	void updateVorhandenMitAntwortenKlappt() {
		final Optional<Teilnehmer> optTeilnehmer = teilnehmerDao.findByUniqueKey(Teilnehmer.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, "G9SN5UX220171231075039");
		if (optTeilnehmer.isPresent()) {
			final PublicTeilnehmer teilnehmerPayload = PublicTeilnehmer.fromTeilnehmer(optTeilnehmer.get());

			final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("E", 5),
				new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("B", 2),
				new PublicAntwortbuchstabe("E", 5), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("C", 3),
				new PublicAntwortbuchstabe("N", 0), new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("B", 2),
				new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("N", 0) };

			teilnehmerPayload.setAntworten(antwortbuchstaben);
			teilnehmerPayload.setEingabemodus(Antworteingabemodus.MATRIX.toString());

			final Teilnehmer teilnehmer = teilnehmerService.updateTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

			assertNotNull(teilnehmer.getKuerzel());
			assertEquals(Klassenstufe.EINS, teilnehmer.getKlassenstufe());
			assertEquals("Marlisa", teilnehmer.getVorname());
			assertEquals("von Trontheim", teilnehmer.getNachname());
			assertTrue(teilnehmer.getLoesungszettelkuerzel().length() == 22);
		}
	}

	@Test
	void updateKlappt() {
		final String kuerzel = "SVICJGE220171231080455";
		final Optional<Teilnehmer> optTeilnehmer = teilnehmerDao.findByUniqueKey(Teilnehmer.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		if (optTeilnehmer.isPresent()) {
			final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
			teilnehmerPayload.setNachname("Biene");
			teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
			teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
			teilnehmerPayload.setVorname("Maja");
			teilnehmerPayload.setKuerzel(kuerzel);
			teilnehmerPayload.setEingabemodus(Antworteingabemodus.MATRIX.toString());

			final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("E", 5),
				new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("D", 4),
				new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("D", 4),
				new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("B", 2),
				new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3),
				new PublicAntwortbuchstabe("B", 3), new PublicAntwortbuchstabe("A", 3) };

			teilnehmerPayload.setAntworten(antwortbuchstaben);

			final Teilnehmer teilnehmer = teilnehmerService.updateTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

			assertEquals(4, teilnehmer.getNummer());
			assertEquals(kuerzel, teilnehmer.getKuerzel());
			assertEquals("ENCDDCDNDBANCBA", teilnehmer.getAntwortcode());
			assertEquals(Integer.valueOf(5), teilnehmer.getKaengurusprung());
			assertEquals("46,75", teilnehmer.getPunkte());
			assertNotNull(teilnehmer.getLoesungszettelkuerzel());

			final Optional<Loesungszettel> opt = loesunszettelDao.findByUniqueKey(Loesungszettel.class,
				MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmer.getLoesungszettelkuerzel());
			assertTrue(opt.isPresent());
			final Loesungszettel loesungszettel = opt.get();
			assertEquals("ENCDDCDNDBANCBA", loesungszettel.getLoesungszettelRohdaten().getAntwortcode());
		}
	}

	@Test
	void updateOhneAntwortenKlappt() {
		final String kuerzel = "EDWBT2UD20171231080454";
		final Optional<Teilnehmer> optTeilnehmer = teilnehmerDao.findByUniqueKey(Teilnehmer.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		if (optTeilnehmer.isPresent()) {
			final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
			teilnehmerPayload.setNachname("Wandi");
			teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
			teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
			teilnehmerPayload.setVorname("Sonne");
			teilnehmerPayload.setKuerzel(kuerzel);

			final Teilnehmer teilnehmer = teilnehmerService.updateTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

			assertEquals(5, teilnehmer.getNummer());
			assertEquals(kuerzel, teilnehmer.getKuerzel());
			assertNull(teilnehmer.getLoesungszettelkuerzel());
			assertNull(teilnehmer.getEingabemodus());
		}
	}

	@Test
	@DisplayName("delete Teilnehmer mit Lösungszettel")
	void createUndDeleteMitLoesungszettel() {
		// Arrange
		final PublicTeilnehmer teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("zu Löschen");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
		teilnehmerPayload.setVorname("Olaf");
		teilnehmerPayload.setEingabemodus(Antworteingabemodus.MATRIX.toString());
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("E", 5),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("e", 5), new PublicAntwortbuchstabe("c", 3), new PublicAntwortbuchstabe("c", 3),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("d", 4), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3),
			new PublicAntwortbuchstabe("B", 3), new PublicAntwortbuchstabe("A", 3) };

		teilnehmerPayload.setAntworten(antwortbuchstaben);

		final Teilnehmer teilnehmer = teilnehmerService.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);

		assertNotNull(teilnehmer.getKuerzel());
		assertEquals(Klassenstufe.ZWEI, teilnehmer.getKlassenstufe());
		assertEquals("Olaf", teilnehmer.getVorname());
		assertEquals("zu Löschen", teilnehmer.getNachname());
		assertNotNull(teilnehmer.getLoesungszettelkuerzel());

		assertEquals("ENCBECCNDBANCBA", teilnehmer.getAntwortcode());
		assertEquals(Integer.valueOf(3), teilnehmer.getKaengurusprung());
		assertEquals("34,25", teilnehmer.getPunkte());

		final Optional<Loesungszettel> opt = loesunszettelDao.findByUniqueKey(Loesungszettel.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmer.getLoesungszettelkuerzel());
		assertTrue(opt.isPresent());
		final Loesungszettel loesungszettel = opt.get();
		assertEquals("ENCBECCNDBANCBA", loesungszettel.getLoesungszettelRohdaten().getAntwortcode());

		final String teilnehmerKuerzel = teilnehmer.getKuerzel();
		final List<Ereignis> ereinisseVorher = protokollService.findByEreignisart(Ereignisart.TEILNEHMER_GELOESCHT_EINZELN.getKuerzel());
		final int expectedAnzahlEreignisse = ereinisseVorher.size() + 1;

		// Act
		teilnehmerService.deleteTeilnehmer("2b47a859-e6c7-4402-b1fd-0ac02984ee79", teilnehmerKuerzel);

		// Assert
		final Optional<Teilnehmer> optTeilnehmer = teilnehmerDao.findByUniqueKey(Teilnehmer.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmerKuerzel);

		assertFalse(optTeilnehmer.isPresent());

		final Optional<Loesungszettel> optLoes = loesunszettelDao.findByUniqueKey(Loesungszettel.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, loesungszettel.getKuerzel());
		assertTrue(optLoes.isPresent());

		final List<Ereignis> ereignisseNachher = protokollService
			.findByEreignisart(Ereignisart.TEILNEHMER_GELOESCHT_EINZELN.getKuerzel());
		assertEquals(expectedAnzahlEreignisse, ereignisseNachher.size());
	}
}
