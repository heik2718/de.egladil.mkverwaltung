//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.domain.adv.AdvText;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.payload.request.AdvVereinbarungAntrag;
import de.egladil.mkv.persistence.service.AdvFacade;

/**
 * AdvFacadeIT
 */
public class AdvFacadeIT extends AbstractGuiceIT {

	@Inject
	private AdvFacade advFacade;

	@Test
	void getAktuellstenTextKlappt() {
		// Act
		final AdvText advText = advFacade.getAktuellstenAdvText();

		// Assert
		assertNotNull(advText);
	}

	@Test
	void createAdvVereinbarungKlappt() {
		// Arrange
		final String benutzerUuid = "2b47a859-e6c7-4402-b1fd-0ac02984ee79";
		final AdvVereinbarungAntrag antrag = new AdvVereinbarungAntrag();
		antrag.setSchulkuerzel("UQWNIRV7");
		antrag.setStrasse("Schulweg");
		antrag.setHausnummer("2");
		antrag.setPlz("98765");
		antrag.setLaendercode("de");
		antrag.setSchulname("Grundschule Adelschlag");
		antrag.setOrt("Adelschlag");

		// Act
		final AdvVereinbarung vereinbarung = advFacade.createAdvVereinbarung(benutzerUuid, antrag);

		// Assert
		assertNotNull(vereinbarung);

	}

}
