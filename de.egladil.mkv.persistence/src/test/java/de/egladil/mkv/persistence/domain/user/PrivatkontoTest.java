//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * PrivatkontoTest
 */
public class PrivatkontoTest {

	private Validator validator;

	private Person person;

	private String benutzerUuid;

	private String jahr = "2015";

	private Privatteilnahme teilnahme;

	private boolean benachrichtigen = true;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();

		person = new Person("Builder", "Testberson");

		benutzerUuid = UUID.randomUUID().toString();

		teilnahme = new Privatteilnahme(jahr, "9HZTF6F4");
	}

	@Test
	public void constructor_returns_complete_valid_inactive_privatkonto() {
		// Act
		final Privatkonto privatkonto = new Privatkonto(benutzerUuid, person, teilnahme, benachrichtigen);

		// Assert
		final Set<ConstraintViolation<Privatkonto>> privatkontoErrors = validator.validate(privatkonto);
		assertTrue(privatkontoErrors.isEmpty());

		assertEquals(benutzerUuid, privatkonto.getUuid());
		assertEquals(person.getVorname(), privatkonto.getPerson().getVorname());
		assertEquals(person.getNachname(), privatkonto.getPerson().getNachname());

		final List<Privatteilnahme> teilnahmen = privatkonto.getTeilnahmen();
		assertEquals(1, teilnahmen.size());

		final Privatteilnahme teilnahme = teilnahmen.get(0);

		final Set<ConstraintViolation<Privatteilnahme>> teilnahmedatenErrors = validator.validate(teilnahme);
		assertTrue(teilnahmedatenErrors.isEmpty());

		assertEquals(jahr, teilnahme.getJahr());
		assertEquals(benachrichtigen, privatkonto.isAutomatischBenachrichtigen());
	}

	@Test
	public void constructor_throws_when_teilnahme_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Privatkonto(benutzerUuid, person, null, false);
		});
		assertEquals("brauchen eine teilnahme fuer das Privatkonto", ex.getMessage());

	}

	@Test
	public void constructor_throws_when_person_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Privatkonto(benutzerUuid, null, teilnahme, true);
		});
		assertEquals("brauchen eine person fuer das Privatkonto", ex.getMessage());

	}

	@Test
	public void constructor_throws_when_uuid_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Privatkonto(null, person, teilnahme, true);
		});
		assertEquals("brauchen eine uuid fuer das Privatkonto", ex.getMessage());

	}
}
