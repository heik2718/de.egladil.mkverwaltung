//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * AufgabeFalschGeloestRechnerStrategieTest
 */
public class AufgabeFalschGeloestRechnerStrategieTest {

	@Test
	public void changeSummeDrei_klappt() {
		// Arrange
		final AufgabeFalschGeloestRechnerStrategie strategie = new AufgabeFalschGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 5));
		final int summe = 1900;
		final int expected = 1825;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeVier_klappt() {
		// Arrange
		final AufgabeFalschGeloestRechnerStrategie strategie = new AufgabeFalschGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.VIER, 5));
		final int summe = 1900;
		final int expected = 1800;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeFuenf_klappt() {
		// Arrange
		final AufgabeFalschGeloestRechnerStrategie strategie = new AufgabeFalschGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.FUENF, 5));
		final int summe = 1900;
		final int expected = 1775;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeInklusionDrei_klappt() {
		// Arrange
		final AufgabeFalschGeloestRechnerStrategie strategie = new AufgabeFalschGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 3));
		final int summe = 1900;
		final int expected = 1750;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeInklusionVier_klappt() {
		// Arrange
		final AufgabeFalschGeloestRechnerStrategie strategie = new AufgabeFalschGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.VIER, 3));
		final int summe = 1900;
		final int expected = 1700;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeInklusionFuenf_klappt() {
		// Arrange
		final AufgabeFalschGeloestRechnerStrategie strategie = new AufgabeFalschGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.FUENF, 3));
		final int summe = 1900;
		final int expected = 1650;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}
}
