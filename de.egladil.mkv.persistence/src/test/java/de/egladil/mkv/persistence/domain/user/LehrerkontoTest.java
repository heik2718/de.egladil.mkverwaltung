//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * LehrerkontoTest
 */
public class LehrerkontoTest {

	private Schule schule;

	private Person person;

	private String benutzerUuid;

	private String jahr = "2015";

	private Schulteilnahme schulteilnahme;

	@BeforeEach
	public void setUp() {
		schule = new Schule();
		schule.setId(4l);
		schule.setKuerzel(new KuerzelGenerator().generateDefaultKuerzel());

		person = new Person("Builder", "Testberson");

		benutzerUuid = UUID.randomUUID().toString();

		schulteilnahme = new Schulteilnahme();
		schulteilnahme.setJahr(jahr);
		schulteilnahme.setId(626l);
		schulteilnahme.setKuerzel(schule.getKuerzel());
	}

	@Test
	public void constructor_throws_when_uuid_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Lehrerkonto(null, person, schule, false);
		});
		assertEquals("brauchen eine benutzerUuid fuer das Lehrerkonto", ex.getMessage());

	}

	@Test
	public void constructor_throws_when_uuid_blank() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Lehrerkonto(" ", person, schule, true);
		});
		assertEquals("brauchen eine benutzerUuid fuer das Lehrerkonto", ex.getMessage());

	}

	@Test
	public void constructor_throws_when_person_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Lehrerkonto(benutzerUuid, null, schule, false);
		});
		assertEquals("brauchen eine person fuer das Lehrerkonto", ex.getMessage());

	}

	@Test
	public void constructor_throws_when_zuordung_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new Lehrerkonto(benutzerUuid, person, null, true);
		});
		assertEquals("brauchen eine Schule fuer das Lehrerkonto", ex.getMessage());

	}

	public void constructor_returns_valid_lehrerkonto() {
		// Act
		final Lehrerkonto lehrerkonto = new Lehrerkonto(benutzerUuid, person, schule, true);

		// Assert
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		final Validator validator = validatorFactory.getValidator();

		final Set<ConstraintViolation<Lehrerkonto>> lehrerkontoErrors = validator.validate(lehrerkonto);
		assertTrue(lehrerkontoErrors.isEmpty());
	}
}
