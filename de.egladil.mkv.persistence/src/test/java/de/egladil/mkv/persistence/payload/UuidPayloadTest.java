//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.common.validation.ValidationUtils;
import de.egladil.mkv.persistence.payload.request.UuidPayload;

/**
 * UuidPayloadTest
 */
public class UuidPayloadTest {

	private Validator validator;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@Test
	public void validate_passes_when_valid_uuid() {
		// Arrange
		final UuidPayload payload = new UuidPayload("630dcfa8-e1af-43aa-b8e7-16326df71152"); // VAw15sdLRYQ
		// Act + Assert
		final Set<ConstraintViolation<UuidPayload>> errors = validator.validate(payload);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_passes_when_valid_kurzzeitpasswort() {
		// Arrange
		final UuidPayload payload = new UuidPayload("VAw15sdLRYQ"); //
		// Act + Assert
		final Set<ConstraintViolation<UuidPayload>> errors = validator.validate(payload);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_fails_when_code_null() {
		// Arrange
		final UuidPayload payload = new UuidPayload(null); //

		// Act + Assert
		final Set<ConstraintViolation<UuidPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("code"));
	}

	@Test
	public void validate_fails_when_code_blank() {
		// Arrange
		final UuidPayload payload = new UuidPayload(" "); //

		// Act + Assert
		final Set<ConstraintViolation<UuidPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("code"));
	}

	@Test
	public void validate_fails_when_code_zu_lang() {
		// Arrange
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 41; i++) {
			sb.append("a");
		}
		final String code = sb.toString();
		assertEquals("Testsettin: brauchen genau 41 Zeichen", 41, code.length());
		final UuidPayload payload = new UuidPayload(code); //

		// Act + Assert
		final Set<ConstraintViolation<UuidPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("code"));
	}
}
