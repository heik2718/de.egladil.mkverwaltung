//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.utils.FontCalulator;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * FontCalulatorTest
 */
public class FontCalulatorTest {

	@Nested
	@DisplayName("Heuristiken für Font-Size für 1 Zeile nach Zeichenzahl")
	class MaximaleFontSizeEinzeilig {

		@Test
		@DisplayName("Name 21 Zeichen")
		void nameKurzObereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_LARGE;
			final String name = getName(21);
			assertEquals(21, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 23 Zeichen")
		void nameMittelGenau() {
			final int expected = UrkundeConstants.SIZE_NAME_MEDIUM;
			final String name = getName(23);
			assertEquals(23, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 24 Zeichen")
		void nameMittelObereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_MEDIUM;
			final String name = getName(24);
			assertEquals(24, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 25 Zeichen")
		void nameLangUntereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(25);
			assertEquals(25, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 27 Zeichen")
		void nameLangObereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(27);
			assertEquals(27, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}
	}

	@Nested
	@DisplayName("Heuristiken für Font-Size für 2 Zeilen nach Zeichenzahl")
	class MaximaleFontSizeZweizeilig {

		@Test
		@DisplayName("Name 28 Zeichen")
		void nameKurzUntereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(28);
			assertEquals(28, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 44 Zeichen")
		void nameKurzObereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(44);
			assertEquals(44, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 45 Zeichen")
		void nameMittelUntereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(45);
			assertEquals(45, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 48 Zeichen")
		void nameMittelObereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(48);
			assertEquals(48, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 49 Zeichen")
		void nameLangUntereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(49);
			assertEquals(49, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Name 53 Zeichen")
		void nameLangObereSchranke() {
			final int expected = UrkundeConstants.SIZE_NAME_SMALL;
			final String name = getName(53);
			assertEquals(53, name.length());

			final int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);

			assertEquals(expected, actual);
		}

		@Test
		@DisplayName("Spiele mit einer Testwortgruppe")
		void testwortgruppe() {
			String name = "Universums aus denen alle chemisch";
			int actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);
			assertEquals(UrkundeConstants.SIZE_NAME_SMALL, actual);

			name = "Universums aus denen alle chemisc";
			actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);
			assertEquals(UrkundeConstants.SIZE_NAME_MEDIUM, actual);

			name = "Universums aus denen alle che";
			actual = new FontCalulator().maximaleFontSizeTeilnehmername(name);
			assertEquals(UrkundeConstants.SIZE_NAME_LARGE, actual);
		}
	}

	@Nested
	@DisplayName("Tests für unerlaubte Parameter")
	class MaximaleFontSizeIllegal {
		@Test
		@DisplayName("Name null")
		void nameNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new FontCalulator().maximaleFontSizeTeilnehmername(null);
			});
			assertEquals("name null", ex.getMessage());
		}

		@Test
		@DisplayName("Name zu lang")
		void nameZuLang() {
			final String name = getName(55);
			assertEquals(55, name.length());
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new FontCalulator().maximaleFontSizeTeilnehmername(name);
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für Höhe")
	class Hoehe {

		@Test
		@DisplayName("Höhe des Titels")
		void hoeheTitel() throws Exception {
			final float hoehe = new FontCalulator().berechneHoehe("Minikänguru", UrkundeConstants.SIZE_TITLE);
			System.out.println("hoehe=" + hoehe);
		}
	}

	private String getName(final int anzahlZeichen) {
		String str = "";
		for (int i = 0; i < anzahlZeichen; i++) {
			if (i % 2 == 0) {
				str = str + "m";
			} else {
				str = str + "x";
			}
		}
		return str;
	}
}
