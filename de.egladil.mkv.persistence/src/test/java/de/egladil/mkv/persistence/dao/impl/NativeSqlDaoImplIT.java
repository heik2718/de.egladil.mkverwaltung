//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.INativeSqlDao;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;

/**
 * NativeSqlDaoImplIT
 */
public class NativeSqlDaoImplIT extends AbstractGuiceIT {

	private static final String JAHR = "2018";

	private static final List<String> LAND = Arrays.asList(new String[] { "de-HE" });

	private static final List<String> LAENDER = Arrays.asList(new String[] { "de-HE", "de-BY" });

	@Inject
	private INativeSqlDao dao;

	@Nested
	@DisplayName("Tests für getAnzahlSchulteilnahmenLand mit verschiedenen Parameterkombinationen")
	class AnzahlSchulteilnahmenTests {
		@Test
		void getAnzahlSchulteilnahmenLandKlappt() {
			// Act
			final int anzahl = dao.getAnzahlSchulteilnahmenLand(LAND, JAHR);

			// Assert
			assertEquals(2, anzahl);
		}

		@Test
		void getAnzahlSchulteilnahmenLaenderKlappt() {
			// Act
			final int anzahl = dao.getAnzahlSchulteilnahmenLand(LAENDER, JAHR);

			// Assert
			assertTrue(anzahl > 0);
		}
	}

	@Nested
	@DisplayName("Tests für getAnzahlLoesungszettel mit verschiedenen Parameterkombinationen")
	class AnzahlLoesungszettelTests {

		@Test
		void nurLaender() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(null, null, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void mehrereLaender() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(null, null, LAENDER);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void nurKlassenstufe() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(null, Klassenstufe.ZWEI, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void nurTeilnahmeart() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(null, null, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void nurJahr() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(JAHR, null, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void jahrTeilnahmeart() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(JAHR, null, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void jahrKlassenstufe() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(JAHR, Klassenstufe.ZWEI, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void teilnahmeartKlassenstufe() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(null, Klassenstufe.ZWEI, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void jahrTeilnahmeartKlassenstufe() {
			// Act
			final int anzahl = dao.getAnzahlLoesungszettelSchulteilnahmen(JAHR, Klassenstufe.ZWEI, LAND);

			// Assret
			assertTrue(anzahl > 0);
		}
	}

	@Nested
	@DisplayName("Gemischte Tests")
	class GemischteTests {

		@Test
		void getAuswertbareJahreKlappt() {
			// Act
			final List<String> result = dao.getAuswertbareJahre();

			// Assert
			assertFalse(result.isEmpty());
		}

		@Test
		void getLoesungszettelSchulenKlappt() {
			// Arrange
			final List<String> kuerzelliste = Arrays.asList(new String[] {"de-BB", "de-MV"});

			// Act
			final List<Loesungszettel> trefferliste = dao.getLoesungszettelSchulen("2018", Klassenstufe.ZWEI, kuerzelliste);

			// Assert
			assertEquals(4, trefferliste.size());
		}

		@Test
		void getSchulteilnahmenZuLehrerKlappt() {
			// Arrange
			Long id = 47l;

			// Act
			List<Schulteilnahme> teilnahmen = dao.getSchulteilnahmenForLehrer(id);

			// Assert
			assertTrue(teilnahmen.size() >= 8);

		}

		@Test
		void getAnzahlSchulteilnahmenZuLehrerKlappt() {
			// Arrange
			Long id = 47l;

			// Act
			int teilnahmen = dao.getAnzahlSchulteilnahmenForLehrer(id);

			// Assert
			assertTrue(teilnahmen >= 8);

		}
	}
}
