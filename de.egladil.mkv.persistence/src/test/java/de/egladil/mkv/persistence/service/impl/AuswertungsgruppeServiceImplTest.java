//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.INativeSqlDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeBuilder;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.AuswertungsgruppePayload;

/**
 * AuswertungsgruppeServiceImplTest
 */
public class AuswertungsgruppeServiceImplTest {

	private static final String TEILNAHMEKUERZEL = "9PDDS51Q";

	private static final String JAHR = "2018";

	private static final String BENUTZER_UUID = "gsag";

	private static final String AUSWERTUNGSGRUPPE_KUERZEL = "GGIFfftu";

	private AuswertungsgruppenServiceImpl service;

	private IAuswertungsgruppeDao auswertungsgruppeDao;

	private ISchuleDao schuleDao;

	private IProtokollService protokollService;

	private INativeSqlDao nativeSqlDao;

	private Auswertungsgruppe root;

	private TeilnahmeIdentifier teilnahmeIdentifier;

	@BeforeEach
	void setUp() {
		auswertungsgruppeDao = Mockito.mock(IAuswertungsgruppeDao.class);
		schuleDao = Mockito.mock(ISchuleDao.class);
		protokollService = Mockito.mock(IProtokollService.class);
		nativeSqlDao = Mockito.mock(INativeSqlDao.class);

		service = new AuswertungsgruppenServiceImpl(auswertungsgruppeDao, schuleDao, protokollService, nativeSqlDao);
		root = new AuswertungsgruppeBuilder(Teilnahmeart.S, TEILNAHMEKUERZEL, JAHR).name("Schule").checkAndBuild();
		teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(TEILNAHMEKUERZEL, JAHR);
	}

	@Nested
	class RootGruppeTests {

		@Test
		void findMitTeilnahmeidentifierNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.getRootAuswertungsgruppe(null);
			});

			assertEquals("teilnahmeIdentifier null", ex.getMessage());
		}

		@Test
		void createRootMitTeilnahmeidentifierNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.findOrCreateRootAuswertungsgruppe(null);
			});

			assertEquals("teilnahmeIdentifier null", ex.getMessage());
		}

		@Test
		void createRootKeineSchule() {
			// Arrange
			Mockito.when(auswertungsgruppeDao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.empty());
			Mockito.when(schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, TEILNAHMEKUERZEL))
				.thenReturn(Optional.empty());

			// Act + Assert
			final Throwable ex = assertThrows(MKVException.class, () -> {
				service.findOrCreateRootAuswertungsgruppe(teilnahmeIdentifier);
			});

			assertEquals(
				"Datenfehler: keine Schule zur Schulteilnahme TeilnahmeIdentifier [teilnahmeart=S, jahr=2018, kuerzel=9PDDS51Q] bekannt",
				ex.getMessage());
		}

		@Test
		@DisplayName("getRootAuswertungsgruppen throws IllegalArgumentException wenn jahr null")
		void getRootAuswertungsgruppenThrows() {

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.getRootAuswertungsgruppen(null);
			});

			assertEquals("jahr null", ex.getMessage());
		}
	}

	@Nested
	class CreateTests {

		@Test
		void createBenutzerUuidNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.createAuswertungsgruppe(null, root, new AuswertungsgruppePayload());
			});

			assertEquals("benutzerUuid null", ex.getMessage());
		}

		@Test
		void createParentNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.createAuswertungsgruppe(BENUTZER_UUID, null, new AuswertungsgruppePayload());
			});

			assertEquals("parent null", ex.getMessage());
		}

		@Test
		void createPayloadNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.createAuswertungsgruppe(BENUTZER_UUID, root, null);
			});

			assertEquals("payload null", ex.getMessage());
		}

		@Test
		void createKlappt() {
			final IAuswertungsgruppeDao dao = new AuswertungsgruppeDaoAdapter() {

				@Override
				public Auswertungsgruppe persist(final Auswertungsgruppe entity) throws PersistenceException {
					return entity;
				}
			};

			final AuswertungsgruppenServiceImpl serv = new AuswertungsgruppenServiceImpl(dao, schuleDao, protokollService, nativeSqlDao);

			final String name = "Eichhörnchenklasse";
			final AuswertungsgruppePayload payload = new AuswertungsgruppePayload();
			payload.setKlassenstufe(Klassenstufe.EINS);
			payload.setName(name);

			final Auswertungsgruppe gruppe = serv.createAuswertungsgruppe(BENUTZER_UUID, root, payload);
			assertNotNull(gruppe.getKuerzel());
			assertTrue(root.getAuswertungsgruppen().contains(gruppe));
			assertEquals(name, gruppe.getName());
			assertEquals(TEILNAHMEKUERZEL, gruppe.getTeilnahmekuerzel());
			assertEquals(Teilnahmeart.S, gruppe.getTeilnahmeart());
			assertEquals(JAHR, gruppe.getJahr());
			assertEquals(Klassenstufe.EINS, gruppe.getKlassenstufe());
			assertEquals(root, gruppe.getParent());
			assertEquals(BENUTZER_UUID, gruppe.getGeaendertDurch());
		}
	}

	@Nested
	class UpdateTests {

		@Test
		void updateBenutzerUuidNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.updateAuswertungsgruppe(null, AUSWERTUNGSGRUPPE_KUERZEL, new AuswertungsgruppePayload());
			});

			assertEquals("benutzerUuid null", ex.getMessage());
		}

		@Test
		void updateParentNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.updateAuswertungsgruppe(BENUTZER_UUID, null, new AuswertungsgruppePayload());
			});

			assertEquals("kuerzel null", ex.getMessage());
		}

		@Test
		void updatePayloadNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.updateAuswertungsgruppe(BENUTZER_UUID, AUSWERTUNGSGRUPPE_KUERZEL, null);
			});

			assertEquals("payload null", ex.getMessage());
		}

		@Test
		void updateKlappt() {

			final String name = "Fuchsklasse";
			final AuswertungsgruppePayload payload = new AuswertungsgruppePayload();
			payload.setKlassenstufe(Klassenstufe.EINS);
			payload.setName(name);

			final Auswertungsgruppe zuAendern = new AuswertungsgruppeBuilder(Teilnahmeart.S, TEILNAHMEKUERZEL, JAHR)
				.name("Eichhörnchenklasse").klassenstufe(Klassenstufe.ZWEI).kuerzel(AUSWERTUNGSGRUPPE_KUERZEL).checkAndBuild();
			root.addAuswertungsgruppe(zuAendern);

			final IAuswertungsgruppeDao dao = new AuswertungsgruppeDaoAdapter() {

				@Override
				public Auswertungsgruppe persist(final Auswertungsgruppe entity) throws PersistenceException {
					return entity;
				}

				@Override
				public Optional<Auswertungsgruppe> findByUniqueKey(final Class<Auswertungsgruppe> clazz, final String attributeName,
					final String attributeValue) throws MKVException, EgladilStorageException {
					return Optional.of(zuAendern);
				}
			};

			final AuswertungsgruppenServiceImpl serv = new AuswertungsgruppenServiceImpl(dao, schuleDao, protokollService, nativeSqlDao);

			final Auswertungsgruppe gruppe = serv.updateAuswertungsgruppe(BENUTZER_UUID, AUSWERTUNGSGRUPPE_KUERZEL, payload);
			assertNotNull(gruppe.getKuerzel());
			assertTrue(root.getAuswertungsgruppen().contains(gruppe));
			assertEquals(name, gruppe.getName());
			assertEquals(TEILNAHMEKUERZEL, gruppe.getTeilnahmekuerzel());
			assertEquals(Teilnahmeart.S, gruppe.getTeilnahmeart());
			assertEquals(JAHR, gruppe.getJahr());
			assertEquals(Klassenstufe.ZWEI, gruppe.getKlassenstufe());
			assertEquals(root, gruppe.getParent());
			assertEquals(BENUTZER_UUID, gruppe.getGeaendertDurch());
		}
	}

	@Nested
	class DeleteTests {
		@Test
		void deleteBenutzerUuidNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.checkAndDeleteAuswertungsgruppe(null, AUSWERTUNGSGRUPPE_KUERZEL, teilnahmeIdentifier);
			});

			assertEquals("benutzerUuid null", ex.getMessage());
		}

		@Test
		void deleteKuerzelNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.checkAndDeleteAuswertungsgruppe(BENUTZER_UUID, null, teilnahmeIdentifier);
			});

			assertEquals("kuerzel null", ex.getMessage());
		}

		@Test
		void deleteTeilnahmeIdentifierNull() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.checkAndDeleteAuswertungsgruppe(BENUTZER_UUID, AUSWERTUNGSGRUPPE_KUERZEL, null);
			});

			assertEquals("teilnahmeIdentifier null", ex.getMessage());
		}

		@Test
		void deleteRootgruppeNull() {
			final IAuswertungsgruppeDao dao = new AuswertungsgruppeDaoAdapter() {
				@Override
				public Optional<Auswertungsgruppe> findRootByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
					return Optional.empty();
				}

			};

			final String expectedMessage = "Keine Rootgruppe zu " + teilnahmeIdentifier.toString() + " gefunden";

			final AuswertungsgruppenServiceImpl serv = new AuswertungsgruppenServiceImpl(dao, schuleDao, protokollService, nativeSqlDao);

			final Throwable ex = assertThrows(MKVException.class, () -> {
				serv.checkAndDeleteAuswertungsgruppe(BENUTZER_UUID, AUSWERTUNGSGRUPPE_KUERZEL, teilnahmeIdentifier);
			});

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		void attemptToDeleteRootgruppe() {
			final IAuswertungsgruppeDao dao = new AuswertungsgruppeDaoAdapter() {
				@Override
				public Optional<Auswertungsgruppe> findRootByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
					final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(teilnahmeIdentifier.getTeilnahmeart(),
						teilnahmeIdentifier.getKuerzel(), teilnahmeIdentifier.getJahr()).kuerzel(AUSWERTUNGSGRUPPE_KUERZEL)
							.name("Irgendwas").checkAndBuild();
					return Optional.of(auswertungsgruppe);
				}

			};

			final String expectedMessage = "Die Rootgruppe kann nicht gelöscht werden!";

			final AuswertungsgruppenServiceImpl serv = new AuswertungsgruppenServiceImpl(dao, schuleDao, protokollService, nativeSqlDao);

			final Throwable ex = assertThrows(MKVException.class, () -> {
				serv.checkAndDeleteAuswertungsgruppe(BENUTZER_UUID, AUSWERTUNGSGRUPPE_KUERZEL, teilnahmeIdentifier);
			});

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		void deleteGruppeMitTeilnehmern() {
			final IAuswertungsgruppeDao dao = new AuswertungsgruppeDaoAdapter() {
				@Override
				public Optional<Auswertungsgruppe> findRootByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {

					final Auswertungsgruppe root = new AuswertungsgruppeBuilder(teilnahmeIdentifier.getTeilnahmeart(),
						teilnahmeIdentifier.getKuerzel(), teilnahmeIdentifier.getJahr()).kuerzel("FIFIFFUFJ").name("Irgendwas")
							.checkAndBuild();

					final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(teilnahmeIdentifier.getTeilnahmeart(),
						teilnahmeIdentifier.getKuerzel(), teilnahmeIdentifier.getJahr()).kuerzel(AUSWERTUNGSGRUPPE_KUERZEL)
							.name("Irgendwas").klassenstufe(Klassenstufe.EINS).checkAndBuild();

					final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.EINS).vorname("Rudi")
						.checkAndBuild();
					auswertungsgruppe.addTeilnehmer(teilnehmer);
					root.addAuswertungsgruppe(auswertungsgruppe);
					return Optional.of(root);
				}

			};

			final String expectedMessage = "Die Auswertungsgruppe kann nicht gelöscht werden, da sie Teilnehmer hat";

			final AuswertungsgruppenServiceImpl serv = new AuswertungsgruppenServiceImpl(dao, schuleDao, protokollService, nativeSqlDao);

			final Throwable ex = assertThrows(MKVException.class, () -> {
				serv.checkAndDeleteAuswertungsgruppe(BENUTZER_UUID, AUSWERTUNGSGRUPPE_KUERZEL, teilnahmeIdentifier);
			});

			assertEquals(expectedMessage, ex.getMessage());
		}
	}
}
