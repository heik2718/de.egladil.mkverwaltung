//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;

/**
 * PrivatteilnahmeDaoImplIT
 */
public class PrivatteilnahmeDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IPrivatteilnahmeDao dao;

	@Test
	public void findByKuerzelKlappt() {
		// Arrange
		final String kuerzel = "4AKICN7Y";

		// Act
		final Optional<Privatteilnahme> optTeilnahme = dao.findByUniqueKey(Privatteilnahme.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertTrue(optTeilnahme.isPresent());
	}

}
