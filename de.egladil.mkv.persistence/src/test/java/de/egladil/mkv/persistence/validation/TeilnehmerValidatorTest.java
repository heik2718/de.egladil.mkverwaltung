//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerValidatorTest
 */
public class TeilnehmerValidatorTest {

	private Teilnehmer teilnehmer;

	private Validator validator;

	@BeforeEach
	void setUp() {
		this.teilnehmer = new TeilnehmerBuilder(TeilnahmeIdentifier.create(Teilnahmeart.P, "H6343GD4", "2018"), Klassenstufe.EINS)
			.vorname("Heinz").nachname("Strunz").zusatz("vom Fenster").checkAndBuild();
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@Test
	@DisplayName("besteht wenn valid")
	void validateValid() {
		// Act
		final Set<ConstraintViolation<Teilnehmer>> errors = validator.validate(teilnehmer);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	@DisplayName("fällt durch wenn zu validierendes Objekt ist null")
	public void validateNull() {

		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			validator.validate(null);
		});
		assertEquals("HV000116: The object to be validated must not be null.", ex.getMessage());
	}

}
