package de.egladil.mkv.persistence.kataloge.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

public class KatalogServiceIT extends AbstractGuiceIT {

	private static final String ADMIN_UUID = "67289f2c-85a3-4cec-8b97-f42460f0679b";

	@Inject
	private KatalogService katalogService;

	@Inject
	private ILandDao landDao;

	@Inject
	private IOrtDao ortDao;

	@Inject
	private ISchuleDao schuleDao;

	@Test
	public void landAnlegen_klappt() {
		// Arrange
		final String kuerzel = new KuerzelGenerator().generateKuerzel(5, "ABCDEFGHIJKLMNOPQRSTUVWXYZ-".toCharArray());
		final String name = "Land " + kuerzel;
		final boolean freischalten = false;

		// Act
		final Land land = katalogService.landAnlegen(kuerzel, name, ADMIN_UUID, freischalten);

		// Assert
		assertNotNull(land);
		assertEquals(freischalten, land.isFreigeschaltet());
	}

	@Test
	public void ortAnlegen_klappt() {
		// Arrange
		final Optional<Land> optLand = landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, "ABCDE");
		assertTrue("Test kann nicht starten: kein Land gefunden", optLand.isPresent());

		// Act
		final Ort ort = katalogService.ortAnlegen(optLand.get().getKuerzel(), "Testort " + System.currentTimeMillis(), ADMIN_UUID);

		// Assert
		assertNotNull(ort);
		assertTrue(ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, ort.getKuerzel()).isPresent());
	}

	@Test
	public void schuleAnlegen_klappt() {
		// Act
		final NeueSchulePayload payload = new NeueSchulePayload("ABCDE", "GTR4GK8B", "Testschule " + System.currentTimeMillis());
		final Schule schule = katalogService.schuleAnlegen(payload, ADMIN_UUID);

		// Assert
		assertNotNull(schule);
		assertTrue(schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, schule.getKuerzel()).isPresent());
	}
}
