//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;

/**
* WettbewerbsloesungDaoImplIT
*/
public class WettbewerbsloesungDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IWettbewerbsloesungDao dao;

	@Test
	public void findByJahr_klappt(){
		// Arrange
		final String wettbewerbsjahr = "2017";

		// Act
		final List<Wettbewerbsloesung> liste = dao.findByJahr(wettbewerbsjahr);

		// Assert
		assertEquals(2, liste.size());
	}
}
