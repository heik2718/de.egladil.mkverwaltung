//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.service.TeilnehmerService;

/**
 * TeilnehmerServiceImplTest
 */
public class TeilnehmerServiceImplTest {

	private static final String BENUTZER_UUID = "bKJSDK";

	private static final String KUERZEL_TEILNAHME = "ZTFGR65Z";

	private static final String JAHR = "2018";

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private PublicTeilnehmer teilnehmerPayload;

	private IPrivatteilnahmeDao privatteilnahmeDao;

	private Kontext mockKontext;

	private TeilnehmerService service;

	@BeforeEach
	void setUp() {
		mockKontext = new Kontext();
		mockKontext.setWettbewerbsjahr("2018");
		KontextReader.getInstance().setMockKontextForTest(mockKontext);

		final ILoesungszettelDao loesungszettelDao = Mockito.mock(ILoesungszettelDao.class);
		final IAuswertungsgruppeDao auswertungsgruppeDao = Mockito.mock(IAuswertungsgruppeDao.class);
		final ITeilnehmerDao teilnehmerDao = Mockito.mock(ITeilnehmerDao.class);
		privatteilnahmeDao = Mockito.mock(IPrivatteilnahmeDao.class);
		final ISchulteilnahmeDao schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		final IWettbewerbsloesungDao wettbewerbsloesungDao = Mockito.mock(IWettbewerbsloesungDao.class);
		service = new TeilnehmerServiceImpl(teilnehmerDao, auswertungsgruppeDao, privatteilnahmeDao, schulteilnahmeDao,
			loesungszettelDao, wettbewerbsloesungDao, Mockito.mock(IProtokollService.class),
			Mockito.mock(AuthorizationService.class));

		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);
		teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Schlemmer");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setVorname("Rudi");

	}

	@Test
	@DisplayName("create IllegalArgumentException wenn benutzerUuid blank")
	void createTeilnahmeBenutzerUuidBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.createTeilnehmer("  ", teilnahmeIdentifier, teilnehmerPayload);
		});
		assertEquals("benutzerUuid blank", ex.getMessage());

	}

	@Test
	@DisplayName("create IllegalArgumentException wenn teilnahmeIdentifier null")
	void createTeilnahmeIdentifierNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.createTeilnehmer(BENUTZER_UUID, null, teilnehmerPayload);
		});
		assertEquals("teilnahmeIdentifier null", ex.getMessage());

	}

	@Test
	@DisplayName("create IllegalArgumentException wenn teilnehmerPayload null")
	void createTeilnehmerPayloadNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, null);
		});
		assertEquals("teilnehmerPayload null", ex.getMessage());
	}

	@Test
	@DisplayName("PreconditionFailedException wenn teilnahme nicht im aktuellen wettbewerbsjahr")
	void createTeilnehmerFalschesJahr() {
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, "2017");
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			service.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);
		});
		assertEquals("jahr ist nicht das aktuelle Wettbewerbsjahr", ex.getMessage());

	}

	@Test
	@DisplayName("PreconditionFailedException wenn keine privatteilnahme vorhanden")
	void createTeilnehmerKeinePrivatteilnahme() {
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);
		Mockito.when(privatteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.empty());
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			service.createTeilnehmer(BENUTZER_UUID, teilnahmeIdentifier, teilnehmerPayload);
		});
		assertEquals("Es gibt keine Teilnahme mit TeilnahmeIdentifier [teilnahmeart=P, jahr=2018, kuerzel=ZTFGR65Z]",
			ex.getMessage());
	}

	@Test
	@DisplayName("delete not authorited")
	void deleteNotAuthorized() {
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("HEHEHEHE", JAHR);
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.EINS).vorname("Heinz")
			.checkAndBuild();

		final AuthorizationService authorizationService = Mockito.mock(AuthorizationService.class);
		Mockito.when(authorizationService.authorizeForTeilnahme(BENUTZER_UUID, teilnahmeIdentifier))
			.thenThrow(new EgladilAuthorizationException("nicht Deine Teilnahme!"));
		final ITeilnehmerDao teilnehmerDao = Mockito.mock(ITeilnehmerDao.class);
		Mockito.when(teilnehmerDao.findByUniqueKey(Teilnehmer.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, "shadaf"))
			.thenReturn(Optional.of(teilnehmer));

		service = new TeilnehmerServiceImpl(teilnehmerDao, Mockito.mock(IAuswertungsgruppeDao.class),
			Mockito.mock(IPrivatteilnahmeDao.class), Mockito.mock(ISchulteilnahmeDao.class), Mockito.mock(ILoesungszettelDao.class),
			Mockito.mock(IWettbewerbsloesungDao.class), Mockito.mock(IProtokollService.class), authorizationService);

		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			service.deleteTeilnehmer(BENUTZER_UUID, "shadaf");
		});
		assertEquals("nicht Deine Teilnahme!", ex.getMessage());
	}

	@Test
	@DisplayName("delete IllegalArgumentExeption wenn benutzerUuid null")
	void deleteUuidNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteTeilnehmer(null, "shadaf");
		});
		assertEquals("benutzerUuid blank", ex.getMessage());
	}

	@Test
	@DisplayName("delete IllegalArgumentExeption wenn benutzerUuid blank")
	void deleteUuidLeer() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteTeilnehmer(" ", "vashjdf");
		});
		assertEquals("benutzerUuid blank", ex.getMessage());
	}

	@Test
	@DisplayName("delete IllegalArgumentExeption wenn kuerzel null")
	void deleteKuerzelNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteTeilnehmer("shadaf", null);
		});
		assertEquals("kuerzel blank", ex.getMessage());
	}

	@Test
	@DisplayName("delete IllegalArgumentExeption wenn kuerzel blank")
	void deleteKuerzelLeer() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteTeilnehmer("vashjdf", " ");
		});
		assertEquals("kuerzel blank", ex.getMessage());
	}

	@Test
	@DisplayName("deleteAll IllegalArgumentExeption wenn benutzerUuid null")
	void deleteAllUuidNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteAllTeilnehmer(null, TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("afsfa", "2018"),
				new ArrayList<>());
		});
		assertEquals("benutzerUuid blank", ex.getMessage());
	}

	@Test
	@DisplayName("deleteAll IllegalArgumentExeption wenn benutzerUuid blank")
	void deleteAllUuidLeer() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteAllTeilnehmer(" ", TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("afsfa", "2018"),
				new ArrayList<>());
		});
		assertEquals("benutzerUuid blank", ex.getMessage());
	}

	@Test
	@DisplayName("deleteAll IllegalArgumentExeption wenn teilnahmeIdentifier null")
	void deleteAllTeilnahmeIdentifierNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteAllTeilnehmer("hdai", null, new ArrayList<>());
		});
		assertEquals("teilnahmeIdentifier null", ex.getMessage());
	}

	@Test
	@DisplayName("deleteAll IllegalArgumentExeption wenn teilnehmer null")
	void deleteAllTeilnehmerNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.deleteAllTeilnehmer("sagi", TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("afsfa", "2018"), null);
		});
		assertEquals("teilnehmer null", ex.getMessage());
	}

	@Test
	@DisplayName("update IllegalArgumentException wenn benutzerUuid blank")
	void updateTeilnahmeBenutzerUuidBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.updateTeilnehmer("  ", teilnahmeIdentifier, teilnehmerPayload);
		});
		assertEquals("benutzerUuid blank", ex.getMessage());

	}

}
