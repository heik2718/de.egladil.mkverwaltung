//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * SchuleDaoImplIT
 */
public class SchuleDaoImplIT extends AbstractGuiceIT {

	@Inject
	private ISchuleDao schuleDao;

	@Test
	void findSchulenInOrtUnscharfOrtNull() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			schuleDao.findSchulenInOrtUnscharf(null, "Marienschule");
		});
		assertEquals("ortkuerzel darf nicht null sein", ex.getMessage());

	}

	@Test
	void findSchulenInOrtUnscharfNameNull() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			schuleDao.findSchulenInOrtUnscharf("HTFTGRDC", null);
		});
		assertEquals("schulnameteil darf nicht null sein", ex.getMessage());

	}

	@Test
	void findByUniqueKey_klappt() {
		// Arrange
		final String kuerzel = "9PDDS51Q";
		// Act
		final Optional<Schule> schule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertTrue(schule.isPresent());
		final Schule theSchule = schule.get();

		final Ort ort = theSchule.getOrt();

		assertNotNull(ort);
		assertEquals("Am Mellensee", ort.getName());
	}

	@Test
	void findByKuerzelMitLand() {
		// Arrange
		final String kuerzel = "6X0ISXLV";
		// Act
		final Optional<Schule> schule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertTrue(schule.isPresent());
		final Schule theSchule = schule.get();

		final Ort ort = theSchule.getOrt();

		assertNotNull(ort);
		assertEquals("Berlin", ort.getName());
	}

	@Test
	void findByUniqueKey_keinTreffer() {
		// Arrange
		final String kuerzel = "6X0ILV";
		// Act
		final Optional<Schule> schule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertFalse(schule.isPresent());
	}

	@Test
	void findSchulenInOrtUnscharf_klappt() {
		// Arrange
		final String ortkuerzel = "27CM5KFF"; // Halle (Saale)
		final String schulname = "grundschule";

		// Act
		final List<Schule> schulen = schuleDao.findSchulenInOrtUnscharf(ortkuerzel, schulname);

		// Assert
		assertEquals(10, schulen.size());
	}
}


