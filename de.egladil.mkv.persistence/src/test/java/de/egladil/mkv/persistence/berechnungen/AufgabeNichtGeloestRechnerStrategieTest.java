//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * AufgabeNichtGeloestRechnerStrategieTest
 */
public class AufgabeNichtGeloestRechnerStrategieTest {

	@Test
	public void changeSumme_klappt() {
		// Arrange
		final AufgabeNichtGeloestRechnerStrategie strategie = new AufgabeNichtGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 5));
		final int summe = 1900;
		final int expected = 1900;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeInklusion_klappt() {
		// Arrange
		final AufgabeNichtGeloestRechnerStrategie strategie = new AufgabeNichtGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 3));
		final int summe = 1900;
		final int expected = 1900;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}
}
