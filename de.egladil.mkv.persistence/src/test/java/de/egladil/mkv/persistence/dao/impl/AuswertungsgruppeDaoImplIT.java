//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeBuilder;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeChildBuilder;
import de.egladil.mkv.persistence.domain.auswertungen.IndividuellesUrkundenmotiv;
import de.egladil.mkv.persistence.domain.auswertungen.Ueberschriftfarbe;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * AuswertungsgruppeDaoImplIT
 */
public class AuswertungsgruppeDaoImplIT extends AbstractGuiceIT {

	private static final String TEILNAHMEKUERZEL = "L0LD4AXU";

	private static final String JAHR = "2018";

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private TeilnahmeIdentifier teilnahmeIdentifier;

	@Inject
	private IAuswertungsgruppeDao dao;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		teilnahmeIdentifier = TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR);
	}

	@Test
	@DisplayName("root Anlegen klappt")
	void insertRoot() throws IOException {
		// Arrange
		final Optional<Auswertungsgruppe> optRoot = dao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier);
		if (!optRoot.isPresent()) {
			final List<Auswertungsgruppe> vorhandene = dao.findAll(Auswertungsgruppe.class);
			final int expectedSize = vorhandene.size() + 1;

			final Auswertungsgruppe ausbildungsgruppe = new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR)
				.name("Blumenwiesenschule").checkAndBuild();

			dao.persist(ausbildungsgruppe);

			final List<Auswertungsgruppe> jetzige = dao.findAll(Auswertungsgruppe.class);
			assertEquals(expectedSize, jetzige.size());
		}
	}

	@Test
	@DisplayName("child Anlegen klappt")
	void insertChild() throws IOException {
		final Optional<Auswertungsgruppe> optRoot = dao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier);
		if (optRoot.isPresent()) {
			final Auswertungsgruppe root = optRoot.get();
			final List<Auswertungsgruppe> vorhandene = root.getAuswertungsgruppen();
			final int expectedSize = vorhandene.size() + 1;

			final Auswertungsgruppe klasse = new AuswertungsgruppeChildBuilder(root, KLASSENSTUFE)
				.name(UUID.randomUUID().toString().substring(0, 8)).checkAndBuild();

			root.addAuswertungsgruppe(klasse);

			final Auswertungsgruppe persisted = dao.persist(root);

			final List<Auswertungsgruppe> jetzige = persisted.getAuswertungsgruppen();
			assertEquals(expectedSize, jetzige.size());
		}
	}

	@Test
	@DisplayName("neues pdf hinzufügen")
	void changeRoot() throws IOException {
		final Optional<Auswertungsgruppe> optRoot = dao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier);
		if (optRoot.isPresent()) {
			final Auswertungsgruppe root = optRoot.get();

			if (root.getIndividuellesUrkundenmotiv() == null) {
				try (InputStream in = getClass().getResourceAsStream("/overlay_blau.pdf")) {
					final byte[] data = IOUtils.toByteArray(in);
					final IndividuellesUrkundenmotiv individuellesUrkundenmotiv = new IndividuellesUrkundenmotiv(
						Ueberschriftfarbe.BLACK, data);
					root.setIndividuellesUrkundenmotiv(individuellesUrkundenmotiv);
				}
			}

			dao.persist(root);
		}
	}

	@Test
	@DisplayName("alle Auswertungsgruppen zu einem TeilnahmeIdentifier")
	void findAlle() {

		final List<Auswertungsgruppe> trefferliste = dao.findByTeilnahmeIdentifier(teilnahmeIdentifier);

		if (!trefferliste.isEmpty()) {
			assertTrue(trefferliste.size() > 1);
		}

	}

	@Test
	@DisplayName("alle Rootgruppen zu einem Jahr")
	void findRootgruppenMitJahr() {

		final List<Auswertungsgruppe> trefferliste = dao.findRootgruppenByJahr(JAHR);

		if (!trefferliste.isEmpty()) {
			assertTrue(trefferliste.size() > 1);
		}
	}
}
