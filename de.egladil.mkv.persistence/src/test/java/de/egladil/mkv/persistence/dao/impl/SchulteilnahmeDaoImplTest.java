//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * SchulteilnahmeDaoImplTest
 */
public class SchulteilnahmeDaoImplTest extends AbstractGuiceIT {

	private static final String KUERZEL = "U7B0OYKZ";

	@Inject
	private ISchulteilnahmeDao dao;

	@Inject
	private ISchuleDao schuleDao;

	@Test
	public void findByKuerzel_klappt() {
		// Act
		final List<Schulteilnahme> alle = dao.findAllBySchulkuerzel(KUERZEL);

		// Assert
		assertFalse(alle.isEmpty());
	}

	@Test
	public void findBySchulkuerzelUndJahr_klappt() {
		// Act
		final Optional<Schulteilnahme> optTeilnahme = dao
			.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(KUERZEL, "2010"));

		// Assert
		assertTrue(optTeilnahme.isPresent());
	}

	@Test
	public void findByKuerzel_kein_treffer() {
		// Act
		final List<Schulteilnahme> alle = dao.findAllBySchulkuerzel("HÖHÖHÖHÖ");

		// Assert
		assertTrue(alle.isEmpty());
	}

	@Test
	public void persist_klappt() {
		// Arrange
		final Optional<Schule> optSchule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, "YFUTLIC3");
		assertTrue("Testdaten fehlen", optSchule.isPresent());

		final List<Schulteilnahme> alleZuSchule = dao.findAllBySchulkuerzel(optSchule.get().getKuerzel());
		int maxJahr = 2008;

		for (final Schulteilnahme t : alleZuSchule) {
			final int aktJahr = Integer.valueOf(t.getJahr());
			if (aktJahr > maxJahr) {
				maxJahr = aktJahr;
			}
		}

		final int jahr = maxJahr + 1;

		final Schulteilnahme neue = new Schulteilnahme();
		neue.setJahr("" + jahr);
		neue.setKuerzel(optSchule.get().getKuerzel());

		// Act
		final Schulteilnahme persisted = dao.persist(neue);

		// Assert
		assertNotNull(persisted);
	}
}
