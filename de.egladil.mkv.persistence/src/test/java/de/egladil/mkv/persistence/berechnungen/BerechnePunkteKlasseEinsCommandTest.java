//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.berechnungen.AbstractBerechnePunkteCommand;
import de.egladil.mkv.persistence.berechnungen.IBerechnePunkteCommand;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * BerechnePunkteKlasseEinsCommandTest
 */
public class BerechnePunkteKlasseEinsCommandTest {

	private static final Logger LOG = LoggerFactory.getLogger(BerechnePunkteKlasseEinsCommandTest.class);

	private static final List<String> LETTERS = Arrays.asList(new String[] { "f", "r", "n" });

	@Test
	public void berechne_rechnet_richtig() {
		// Arrange
		final String wertungscode = "rfffrfrrfrrf";
		final int expected = 3125;

		// Act
		final int actual = AbstractBerechnePunkteCommand.createCommand(Klassenstufe.EINS).berechne(wertungscode);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void alle_punkte() {
		// Arrange
		final IBerechnePunkteCommand command = AbstractBerechnePunkteCommand.createCommand(Klassenstufe.EINS);
		final Set<String> worte = new HashSet<>();
		addWort(worte, "", 12);
		assertEquals(531441, worte.size());

		final List<Integer> punkte = new ArrayList<>();
		for (final String wort : worte) {
			final Integer p = command.berechne(wort);
			if (!punkte.contains(p)) {
				punkte.add(Integer.valueOf(p));
			}
		}
		assertEquals(215, punkte.size());
		Collections.sort(punkte);
		final StringBuffer sb = new StringBuffer();
		for (final Integer i : punkte) {
			sb.append(i.toString());
			sb.append("\n");
		}
		LOG.debug(sb.toString());
	}

	@Test
	public void berechne_throws_IndexOutOfBoundsException_wenn_zu_kurz() {
		// Arrange
		final String wertungscode = "rrrffrrffrr";

		// Act + Assert
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			AbstractBerechnePunkteCommand.createCommand(Klassenstufe.EINS).berechne(wertungscode);
		});
		assertEquals("wertungscode muss die Länge 12 haben.", ex.getMessage());

	}

	@Test
	public void berechne_throws_IndexOutOfBoundsException_wenn_zu_lang() {
		// Arrange
		final String wertungscode = "rrrffrrffrrnf";

		// Act + Assert
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			AbstractBerechnePunkteCommand.createCommand(Klassenstufe.EINS).berechne(wertungscode);
		});
		assertEquals("wertungscode muss die Länge 12 haben.", ex.getMessage());

	}

	/**
	 * Fügt zu worte alle Worte der Länge laenge aus den 3 Buchstaben f,r und n hinzu.<br>
	 * <br>
	 * Nach Aufruf <code>addWort(worte, "",12);</code> enthält worte alle möglichen Wertungscode für Klassenstufe 1.
	 *
	 *
	 * @param worte
	 * @param prefix
	 * @param laenge
	 */
	private void addWort(final Set<String> worte, final String prefix, final int laenge) {
		if (laenge == 0) {
			worte.add(prefix);
			return;
		}
		for (final String letter : LETTERS) {
			addWort(worte, prefix + letter, laenge - 1);
		}
	}
}
