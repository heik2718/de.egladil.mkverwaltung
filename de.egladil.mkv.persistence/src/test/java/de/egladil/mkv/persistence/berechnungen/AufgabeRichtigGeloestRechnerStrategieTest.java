//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * AufgabeRichtigGeloestRechnerStrategieTest
 */
public class AufgabeRichtigGeloestRechnerStrategieTest {

	@Test
	public void changeSumme_klappt() {
		// Arrange
		final AufgabeRichtigGeloestRechnerStrategie strategie = new AufgabeRichtigGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 5));
		final int summe = 1900;
		final int expected = 2200;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void changeSummeInklusion_klappt() {
		// Arrange
		final AufgabeRichtigGeloestRechnerStrategie strategie = new AufgabeRichtigGeloestRechnerStrategie(
			new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 3));
		final int summe = 1900;
		final int expected = 2200;

		// Act
		final int actual = strategie.applyToSumme(summe);

		// Assert
		assertEquals(expected, actual);
	}
}
