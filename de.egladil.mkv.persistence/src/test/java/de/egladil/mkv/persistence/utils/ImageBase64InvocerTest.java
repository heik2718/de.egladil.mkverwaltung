//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * ImageBase64InvocerTest
 */
public class ImageBase64InvocerTest {

	@Test
	@DisplayName("happy hour")
	void convert() throws IOException {

		final ClasspathResourceByteArray resource = new ClasspathResourceByteArray("/overlay_green.png");
		new ClasspathBinaryResourceReader(resource).execute();
		final ImageSerialisierung image = new ImageSerialisierung(resource.getData());
		final ImageBase64Converter converter = new ImageBase64Converter(image);

		converter.execute();

		assertTrue(image.getBase64Image().startsWith("iVBORw0KGgoAAAANSUhEUgAAADUAAABLCAYAAADQzs/BA"));
	}

	@Test
	@DisplayName("IllegalArgumentException wenn parameter null")
	void constructorParameterNull() throws IOException {

		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ImageBase64Converter(null);
		});
		assertEquals("image null", ex.getMessage());
	}
}
