//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * TextTest
 */
public class TextTest {

	@Test
	@DisplayName("Initialisierung mit List")
	void initTest1() {
		final Text text = new Text(Arrays.asList(new String[] { "das", "ist", "ein", "satz" }));
		final List<String> transformed = text.getTransformedText();
		assertThat(transformed, is(text.getText()));
	}

	@Test
	@DisplayName("Initialisierung mit Array")
	void initTest2() {
		final Text text = new Text(new String[] { "das", "ist", "ein", "satz" });
		final List<String> transformed = text.getTransformedText();
		assertThat(transformed, is(text.getText()));
	}

	@Test
	@DisplayName("IllegalArgumentException wenn list null")
	void parameterListNull() {
		final List<String> list = null;
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new Text(list);
		});
		assertEquals("text null", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn list null")
	void parameterArrayNull() {
		final String[] array = null;
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new Text(array);
		});
		assertEquals("text null", ex.getMessage());
	}

}
