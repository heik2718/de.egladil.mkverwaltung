//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.google.inject.Inject;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * TeilnehmerFacadeIT
 */
public class TeilnehmerFacadeAuswertungsgruppenIT extends AbstractGuiceIT {

	private PublicTeilnehmer teilnehmerPayload;

	@Inject
	private TeilnehmerFacade teilnehmerFacade;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Schlemmer");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setZusatz(UUID.randomUUID().toString().substring(0, 8));
		teilnehmerPayload.setVorname("Rudi");
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
	}

	@Nested
	@DisplayName("Tests für Auswertungsgruppen")
	class AuswertungsgruppenTests {
		@Test
		@DisplayName("")
		void auswertungsgruppenMitTreffer() {
			// Arrange
			final String jahr = "2018";
			final String teilnahmekuerzel = "M94P3IH9";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final List<Auswertungsgruppe> trefferliste = teilnehmerFacade.getAuswertungsgruppen(tiProvider);

			// Assert
			assertTrue(trefferliste.size() > 1);
		}

		@Test
		@DisplayName("getAuswertungsgruppen throws IllegalArgumentException wenn parameter null")
		void listeParameterNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.getAuswertungsgruppen(null);
			});
			assertEquals("teilnahmeIdentifierProvider null", ex.getMessage());
		}

		@Test
		@DisplayName("wurzel einer Hierarchie finden")
		void root() {
			final String jahr = "2018";
			final String teilnahmekuerzel = "M94P3IH9";
			final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(teilnahmekuerzel,
				jahr);
			final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
			Mockito.when(tiProvider.provideTeilnahmeIdentifier()).thenReturn(teilnahmeIdentifier);

			// Act
			final Optional<Auswertungsgruppe> optGruppe = teilnehmerFacade.getRootAuswertungsruppe(tiProvider);

			// Assert
			assertTrue(optGruppe.isPresent());
		}

		@Test
		@DisplayName("getRootAuswertungsruppe throws IllegalArgumentException wenn parameter null")
		void rootParameterNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerFacade.getRootAuswertungsruppe(null);
			});
			assertEquals("teilnahmeIdentifierProvider null", ex.getMessage());
		}
	}
}
