//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;

/**
 * SchuleReadOnlyDaoIT
 */
public class SchuleReadOnlyDaoIT extends AbstractGuiceIT {

	@Inject
	private ISchuleReadOnlyDao schuleDao;

	@Test
	void countSchulenOhneOrt() {
		// Arrange
		final String name = "SteIn";

		// Act
		final BigInteger anzahl = schuleDao.anzahlTreffer(name, null);

		// Assert
		assertEquals(BigInteger.valueOf(311l), anzahl);
	}

	@Test
	void countSchulenMitOrt() {
		// Arrange
		final String name = "SteIn";

		// Act
		final BigInteger anzahl = schuleDao.anzahlTreffer(name, name);

		// Assert
		assertEquals(BigInteger.valueOf(110l), anzahl);
	}

	@Test
	void countSchulenInOrt() {
		// Arrange
		final String name = "Münster";

		// Act
		final BigInteger anzahl = schuleDao.anzahlTreffer(null, name);

		// Assert
		assertEquals(BigInteger.valueOf(88l), anzahl);
	}

	@Test
	void findSchulenOhneOrt() {
		// Arrange
		final String name = "SteIn";

		// Act
		final List<SchuleReadOnly> trefferliste = schuleDao.findSchulen(name, null);

		// Assert
		assertEquals(311, trefferliste.size());
	}

	@Test
	void findSchulenMitOrt() {
		// Arrange
		final String name = "SteIn";

		// Act
		final List<SchuleReadOnly> trefferliste = schuleDao.findSchulen(name, name);

		// Assert
		assertEquals(110, trefferliste.size());
	}

	@Test
	void findSchulenInOrt() {
		// Arrange
		final String name = "Münster";

		// Act
		final List<SchuleReadOnly> trefferliste = schuleDao.findSchulen(null, name);

		// Assert
		assertEquals(88, trefferliste.size());
	}

	@Test
	void findUnbekannteSchuleImLandKlappt() {
		// Arrange
		final String landkuerzel = "de-RP";

		// Act
		final Optional<SchuleReadOnly> optSchule = schuleDao.findUnbekannteSchuleImLand(landkuerzel);

		// Assert
		assertTrue(optSchule.isPresent());
	}

	@Test
	void findUnbekannteSchuleBRDKlappt() {
		// Arrange
		final String landkuerzel = "de";

		// Act
		final Optional<SchuleReadOnly> optSchule = schuleDao.findUnbekannteSchuleImLand(landkuerzel);

		// Assert
		assertTrue(optSchule.isPresent());
	}

	@Test
	void findUnbekannteSchuleSchweizKlappt() {
		// Arrange
		final String landkuerzel = "CH";

		// Act
		final Optional<SchuleReadOnly> optSchule = schuleDao.findUnbekannteSchuleImLand(landkuerzel);

		// Assert
		assertTrue(optSchule.isPresent());
	}
}

