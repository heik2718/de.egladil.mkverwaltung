//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * LoesungszettelDaoImplIT
 */
public class LoesungszettelDaoImplIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(LoesungszettelDaoImplIT.class);

	@Inject
	private ILoesungszettelDao loesungszettelDao;

	@Inject
	private ISchulteilnahmeDao schulteilnahmeDao;

	@Test
	public void anlegen_klappt() {
		// Arrange
		final List<Schulteilnahme> schulteilnahmen = schulteilnahmeDao.findAllByJahr("2008");
		assertFalse("Test kann nicht beginnen: keine Schulteilnahmen vorhanden", schulteilnahmen.isEmpty());
		final int index = new Random().nextInt(schulteilnahmen.size());
		final Schulteilnahme schulteilnahme = schulteilnahmen.get(index);
		LOG.info("wählen Schulteilnahme index {} - {}", index, schulteilnahme);

		final LoesungszettelRohdaten daten = new LoesungszettelRohdaten.Builder(Auswertungsquelle.UPLOAD, Klassenstufe.EINS, 2,
			2725, "rrffnrffrrfn", "r,e,f,f,n,r,f,f,r,r,f,n").typo(true).build();

		final Loesungszettel loesungszettel = new Loesungszettel.Builder(schulteilnahme.getJahr(), Teilnahmeart.S,
			schulteilnahme.getKuerzel(), new Random().nextInt(1000), daten).withSprache(Sprache.de).build();

		// Act
		final Loesungszettel persisted = loesungszettelDao.persist(loesungszettel);

		// Assert
		assertEquals(22, loesungszettel.getKuerzel().length());
		assertNotNull(persisted);

	}

	@Test
	public void getMaxNummer_treffermenge_nicht_leer() {
		// Arrange
		final Teilnahmeart teilnahmeart = Teilnahmeart.S;
		final Klassenstufe klassenstufe = Klassenstufe.EINS;
		final String jahr = "2017";
		final String teilnahmekuerzel = "YG9D32CD";

		// Act
		final int maxNummer = loesungszettelDao.getMaxNummer(teilnahmeart, jahr, teilnahmekuerzel, klassenstufe);
		LOG.debug("max nummer = {}", maxNummer);

		// Assert
		assertTrue(maxNummer > 0);
	}

	@Test
	public void getMaxNummer_treffermenge_leer() {
		// Arrange
		final Teilnahmeart teilnahmeart = Teilnahmeart.S;
		final Klassenstufe klassenstufe = Klassenstufe.EINS;
		final String jahr = "2017";
		final String teilnahmekuerzel = "ABCDEFGH";

		// Act
		final int maxNummer = loesungszettelDao.getMaxNummer(teilnahmeart, jahr, teilnahmekuerzel, klassenstufe);

		// Assert
		assertEquals(0, maxNummer);
	}

	@Test
	public void findByTeilnahmeSchuleKlappt() {
		// Arrange
		final TeilnahmeIdentifierProvider teilnahme = new TeilnahmeIdentifierProvider() {

			@Override
			public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
				return TeilnahmeIdentifier.createSchulteilnahmeIdentifier("YG9D32CD", "2017");
			}
		};

		// Act
		final List<Loesungszettel> trefferliste = loesungszettelDao
			.findByTeilnahmeIdentifier(teilnahme.provideTeilnahmeIdentifier());

		// Assert
		assertEquals(834, trefferliste.size());
	}

	@Test
	public void findByTeilnahmePrivatKlappt() {
		// Arrange
		final TeilnahmeIdentifierProvider teilnahme = new TeilnahmeIdentifierProvider() {

			@Override
			public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
				return TeilnahmeIdentifier.createSchulteilnahmeIdentifier("4AKICN7Y", "2017");
			}
		};

		// Act
		final List<Loesungszettel> trefferliste = loesungszettelDao
			.findByTeilnahmeIdentifier(teilnahme.provideTeilnahmeIdentifier());

		// Assert
		assertEquals(112, trefferliste.size());
	}

	@Nested
	@DisplayName("Tests für getAnzahlLoesungszettel mit verschiedenen Parameterkombinationen")
	class AnzahlLoesungszettelTests {

		@Test
		void keinParameter() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel(null, null, null);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void nurKlassenstufe() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel(null, null, Klassenstufe.ZWEI);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void nurTeilnahmeart() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel(null, Teilnahmeart.P, null);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void nurJahr() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel("2018", null, null);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void jahrTeilnahmeart() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel("2018", Teilnahmeart.S, null);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void jahrKlassenstufe() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel("2018", null, Klassenstufe.ZWEI);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void teilnahmeartKlassenstufe() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel(null, Teilnahmeart.S, Klassenstufe.ZWEI);

			// Assret
			assertTrue(anzahl > 0);
		}

		@Test
		void jahrTeilnahmeartKlassenstufe() {
			// Act
			final int anzahl = loesungszettelDao.getAnzahlLoesungszettel("2018", Teilnahmeart.S, Klassenstufe.ZWEI);

			// Assret
			assertTrue(anzahl > 0);
		}
	}
}
