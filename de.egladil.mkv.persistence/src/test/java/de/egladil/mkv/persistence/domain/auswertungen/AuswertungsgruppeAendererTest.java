//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * AuswertungsgruppeAendererTest
 */
public class AuswertungsgruppeAendererTest {

	private static final String TEILNAHMEKUERZEL = "GTFKL987";

	private static final String JAHR = "2018";

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private Auswertungsgruppe auswertungsgruppe;

	private Auswertungsgruppe parent;

	private AuswertungsgruppeAenderer aenderer;

	@BeforeEach
	void setUp() {
		parent = new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR).name("Krautgartenschule").checkAndBuild();
		auswertungsgruppe = new AuswertungsgruppeChildBuilder(parent, KLASSENSTUFE).name("Fuchsklasse").checkAndBuild();
		aenderer = new AuswertungsgruppeAenderer(auswertungsgruppe);
	}

	@Test
	@DisplayName("konstruktor: IllegalArgumentException wenn parameter null")
	void konstructor() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new AuswertungsgruppeAenderer(null);
		});
		assertEquals("auswertungsgruppe erforderlich", ex.getMessage());
	}

	@Nested
	@DisplayName("Tests für checkAndChange")
	class BuildTests {
		@Test
		@DisplayName("IllegalArgrumentException wenn zusammengesetzter Name zu lang")
		void zusammenesetzterNameZuLang() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				aenderer.name("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilchen zu Kette")
					.checkAndChange();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("nur der Name wird geändert")
		void buildInnereLeerzeichen() {
			final Auswertungsgruppe result = aenderer.name("Schneckenklasse").checkAndChange();
			assertEquals("Schneckenklasse", result.getName());
			assertEquals(parent, result.getParent());
			assertEquals("Krautgartenschule", parent.getName());
			assertEquals(TEILNAHMEART, result.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, result.getTeilnahmekuerzel());
			assertEquals(JAHR, result.getJahr());
		}

		@Test
		@DisplayName("name darf null bleiben")
		void buildOhneName() {
			final Auswertungsgruppe result = aenderer.checkAndChange();
			assertEquals("Klassenstufe 2", result.getName());
			assertEquals(parent, result.getParent());
			assertEquals("Krautgartenschule", parent.getName());
			assertEquals(TEILNAHMEART, result.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, result.getTeilnahmekuerzel());
			assertEquals(JAHR, result.getJahr());
		}
	}

	@Test
	void schulkatalogNameTest() {
		final String neuerName = "Comenius-Grundschule-Oranienburg";
		new LengthTester().checkTooLongAndThrow(neuerName, UrkundeConstants.SIZE_TEXT_NORMAL);
	}

}
