//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.berechnungen.FindKaengurusprungCommand;

/**
 * FindKaengurusprungCommandTest
 */
public class FindKaengurusprungCommandTest {

	private final FindKaengurusprungCommand command = new FindKaengurusprungCommand();

	@Test
	public void findKaengurusprung_klappt_1() {
		// Arrange
		final String wertungscode = "frrfnrrrffrnnf";
		final int expected = 3;

		// Act
		final int actual = command.findKaengurusprung(wertungscode);

		// Assert
		assertEquals(expected, actual);

	}

	@Test
	public void findKaengurusprung_klappt_2() {
		// Arrange
		final String wertungscode = "rfrfrfr";
		final int expected = 1;

		// Act
		final int actual = command.findKaengurusprung(wertungscode);

		// Assert
		assertEquals(expected, actual);

	}

	@Test
	public void findKaengurusprung_klappt_3() {
		// Arrange
		final String wertungscode = "rrrrfrr";
		final int expected = 4;

		// Act
		final int actual = command.findKaengurusprung(wertungscode);

		// Assert
		assertEquals(expected, actual);

	}

}
