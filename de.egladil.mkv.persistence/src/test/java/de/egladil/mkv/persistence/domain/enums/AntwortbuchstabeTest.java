//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * AntwortbuchstabeTest
 */
public class AntwortbuchstabeTest {

	@Test
	@DisplayName("Tests für korrekte Eingaben")
	public void valueOfSingleLetterKorrekteEingaben() {
		final String[] eingaben = new String[] { "A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "N", "n", "-" };
		final Antwortbuchstabe[] expected = new Antwortbuchstabe[] { Antwortbuchstabe.A, Antwortbuchstabe.A, Antwortbuchstabe.B,
			Antwortbuchstabe.B, Antwortbuchstabe.C, Antwortbuchstabe.C, Antwortbuchstabe.D, Antwortbuchstabe.D, Antwortbuchstabe.E,
			Antwortbuchstabe.E, Antwortbuchstabe.N, Antwortbuchstabe.N, Antwortbuchstabe.N };
		final List<Antwortbuchstabe> result = new ArrayList<>();
		for (int i = 0; i < eingaben.length; i++) {
			result.add(Antwortbuchstabe.valueOfSingleLetter(eingaben[i]));
		}

		final Antwortbuchstabe[] actual = result.toArray(new Antwortbuchstabe[] {});
		assertArrayEquals(expected, actual);
	}

	@Test
	@DisplayName("Testen Zeichen, die nicht erlaubt sind")
	public void valueOfSingleLetterInvalidParameter() {
		final char[] nichtErlaubteZeichen = "fFgGhHiIjJkKlLmMoOpPqQrRsStTuUvVwWxXyYzZäÄöÖüÜ1234567890".toCharArray();
		for (final char c : nichtErlaubteZeichen) {
			assertThrows(IllegalArgumentException.class, () -> {
				Antwortbuchstabe.valueOfSingleLetter(String.valueOf(c));
			});
		}
	}

	@Test
	@DisplayName("String länger als 1")
	public void valueOfSingleLetterZweiBuchstaben() {
		assertThrows(IllegalArgumentException.class, () -> {
			Antwortbuchstabe.valueOfSingleLetter("AV");
		});
	}

	@Test
	@DisplayName("String null")
	public void valueOfSingleLetterNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			Antwortbuchstabe.valueOfSingleLetter(null);
		});
	}

	@Test
	@DisplayName("Testen valueOfNummer mit gültigen Parametern")
	void valueOfNummerKorrekt() {
		final Antwortbuchstabe[] expected = new Antwortbuchstabe[] { Antwortbuchstabe.A, Antwortbuchstabe.B, Antwortbuchstabe.C,
			Antwortbuchstabe.D, Antwortbuchstabe.E, Antwortbuchstabe.N };

		final int[] eingaben = new int[] { 1, 2, 3, 4, 5, 0 };
		final List<Antwortbuchstabe> result = new ArrayList<>();
		for (final int eingabe : eingaben) {
			result.add(Antwortbuchstabe.valueOfNummer(eingabe));
		}
		final Antwortbuchstabe[] actual = result.toArray(new Antwortbuchstabe[] {});
		assertArrayEquals(expected, actual);
	}

	@Test
	@DisplayName("Testen Zeichen, die nicht erlaubt sind")
	public void valueOfNumberInvalidParameter() {
		final int[] eingaben = new int[] { -1, 6, 7, 8, 9, 10 };
		for (final int eingabe : eingaben) {
			assertThrows(IllegalArgumentException.class, () -> {
				Antwortbuchstabe.valueOfNummer(eingabe);
			});
		}
	}
}
