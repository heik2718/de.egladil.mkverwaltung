//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * TrimShrinkStringCommandTest
 */
public class TrimShrinkStringCommandTest {

	@Test
	@DisplayName("keine einschließenden Leerzeichen")
	void keineLeerzeichen() {
		final String expected = "hallo";
		final String actual = new TrimShrinkStringCommand("hallo").execute();

		assertEquals(expected, actual);
	}

	@Test
	@DisplayName("keine einschließenden Leerzeichen")
	void einschließendeLeerzeichen() {
		final String expected = "hallo";
		final String actual = new TrimShrinkStringCommand(" hallo  ").execute();

		assertEquals(expected, actual);
	}

	@Test
	@DisplayName("überflüssige innere Leerzeichen")
	void einschließendeUndEinebundeneLeerzeichen() {
		final String expected = "ha llo";
		final String actual = new TrimShrinkStringCommand(" ha  llo  ").execute();

		assertEquals(expected, actual);
	}

	@Test
	@DisplayName("IllegalArgumentException wenn parameter null")
	void stringNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TrimShrinkStringCommand(null);
		});
		assertEquals("string null", ex.getMessage());
	}

}
