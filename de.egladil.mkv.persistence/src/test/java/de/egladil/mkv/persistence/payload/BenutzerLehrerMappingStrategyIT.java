//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * BenutzerLehrerMappingStrategyIT
 */
public class BenutzerLehrerMappingStrategyIT extends AbstractGuiceIT {

	private static final String BENUTZER_UUID = "03e23b1f-0cba-4079-8ab1-ac7d7c4de9b6";

	@Inject
	private IBenutzerService benutzerService;

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

	@Inject
	private ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	@Inject
	private AdvFacade advFacade;

	private ObjectMapper objectMapper;

	@Inject
	private IKatalogService katalogService;

	private BenutzerLehrerMappingStrategy strategy;

	@Inject
	private TeilnahmenFacade teilnahmenFacade;

	@Inject
	private TeilnehmerFacade teilnehmerFacade;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		TestUtils.initMKVApiKontextReader("2019", true);
		objectMapper = new ObjectMapper();
		strategy = new BenutzerLehrerMappingStrategy(lehrerteilnahmeInfoDao, advFacade, katalogService, teilnahmenFacade,
			teilnehmerFacade);
	}

	@Test
	void testLoginVader() {

	}

	@Test
	public void createMKVBenutzer() throws JsonProcessingException {
		// Arrange
		final Lehrerkonto lehrerkonto = lehrerkontoDao.findByUUID(BENUTZER_UUID).get();
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID);

		// Act
		final MKVBenutzer mkvUser = strategy.createMKVBenutzer(benutzerkonto, lehrerkonto);

		final String json = objectMapper.writeValueAsString(mkvUser);
		System.out.println(json);

		// Assert
		assertNotNull(mkvUser);
		assertNotNull(mkvUser.getBasisdaten());
		final PersonBasisdaten basisdaten = mkvUser.getBasisdaten();
		assertEquals("MKV_LEHRER", basisdaten.getRolle());
		assertEquals("Hannelore", basisdaten.getVorname());
		assertEquals("Random--1413122879", basisdaten.getNachname());
		assertEquals(benutzerkonto.getEmail(), basisdaten.getEmail());

		final ErweiterteKontodaten erweiterteDaten = mkvUser.getErweiterteKontodaten();
		assertNotNull(erweiterteDaten);
		final PublicTeilnahme aktuelleTeilnahme = erweiterteDaten.getAktuelleTeilnahme();
		assertFalse(StringUtils.isBlank(aktuelleTeilnahme.getKollegen()));
		assertTrue(aktuelleTeilnahme.isAktuelle());
		assertEquals("2017", aktuelleTeilnahme.getTeilnahmeIdentifier().getJahr());
		assertEquals(Teilnahmeart.S, aktuelleTeilnahme.getTeilnahmeIdentifier().getTeilnahmeart());
		assertEquals("MXVCSK7J", aktuelleTeilnahme.getTeilnahmeIdentifier().getKuerzel());

		assertNotNull(aktuelleTeilnahme);
		assertNotNull(erweiterteDaten.getSchule());
		assertEquals(13, erweiterteDaten.getAnzahlTeilnahmen());
		assertFalse(erweiterteDaten.isAdvVereinbarungVorhanden());
		assertTrue(erweiterteDaten.isMailbenachrichtigung());

		assertNull(mkvUser.getHateoasPayload());
	}
}
