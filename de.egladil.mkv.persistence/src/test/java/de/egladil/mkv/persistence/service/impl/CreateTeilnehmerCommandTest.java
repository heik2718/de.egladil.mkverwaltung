//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * CreateTeilnehmerCommandTest
 */
public class CreateTeilnehmerCommandTest {

	private static final String KUERZEL_TEILNAHME = "ZTFGR65Z";

	private PublicTeilnehmer teilnehmerPayload;

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private static final String JAHR = "2018";

	@BeforeEach
	void setUp() {
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);

		teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Schlemmer");
		teilnehmerPayload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI));
		teilnehmerPayload.setVorname("Rudi");
	}

	@Test
	@DisplayName("Konstruktor: IllegalArgumentException wenn teilnahmeIdentifier null")
	void constructorIdentifierNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new CreateTeilnehmerCommand(null, teilnehmerPayload);
		});
		assertEquals("teilnahmeIdentifier", ex.getMessage());
	}

	@Test
	@DisplayName("Konstruktor: IllegalArgumentException wenn teilnehmerPayload null")
	void constructorPayloadNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new CreateTeilnehmerCommand(teilnahmeIdentifier, null);
		});
		assertEquals("teilnehmerPayload null", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalStateException wenn getter ohne execute")
	void getTeilnehmerWithoutExecute() {
		final Throwable ex = assertThrows(IllegalStateException.class, () -> {
			new CreateTeilnehmerCommand(teilnahmeIdentifier, teilnehmerPayload).getTeilnehmer();
		});
		assertEquals("IllegalState: please invoke execute() first", ex.getMessage());
	}

	@Test
	@DisplayName("happy hour")
	void executeCreatesTeilnehmer() {
		final CreateTeilnehmerCommand cmd = new CreateTeilnehmerCommand(teilnahmeIdentifier, teilnehmerPayload);
		cmd.execute();

		final Teilnehmer teilnehmer = cmd.getTeilnehmer();

		assertNull(teilnehmer.getId());
		assertNull(teilnehmer.getAuswertungsgruppe());
		assertNotNull(teilnehmer.getKuerzel());
		assertNull(teilnehmer.getLoesungszettelkuerzel());
		assertEquals(teilnahmeIdentifier.getTeilnahmeart(), teilnehmer.getTeilnahmeart());
		assertEquals(KUERZEL_TEILNAHME, teilnehmer.getTeilnahmekuerzel());
		assertEquals(Klassenstufe.ZWEI, teilnehmer.getKlassenstufe());
		assertEquals(JAHR, teilnehmer.getJahr());
		assertEquals("Schlemmer", teilnehmer.getNachname());
		assertEquals("Rudi", teilnehmer.getVorname());
		assertNull(teilnehmer.getZusatz());
	}

}
