//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.file.VerzeichnisLoeschenCommand;

/**
 * VerzeichnisLoeschenCommandTest
 */
public class VerzeichnisLoeschenCommandTest {

	@Test
	public void deleteAndLogIOException_gibt_false_zurueck() {
		// Arrange
		final String path = "/root/nicht_loeschen";

		// Act
		final boolean geloescht = new VerzeichnisLoeschenCommand().deleteAndLogIOException(new File(path));

		// Assert
		assertFalse(geloescht);
	}

	@Test // (expected = NullPointerException.class)
	public void deleteAndLogIOException_wirft_NullPointerException() {
		assertThrows(NullPointerException.class, () -> {
			new VerzeichnisLoeschenCommand().deleteAndLogIOException(null);
		});
	}

	@Test
	public void deleteAndLogIOException_gibt_true_zurueck() throws IOException {
		// Arrange
		final File dir = new File("/home/heike/upload/muss_weg");
		FileUtils.forceMkdir(dir);
		final File datei = new File(dir.getAbsolutePath() + "/datei");
		FileUtils.touch(datei);

		assertTrue(dir.isDirectory());
		assertTrue(datei.isFile());

		// Act
		final boolean deleted = new VerzeichnisLoeschenCommand().deleteAndLogIOException(dir);

		// Assert
		assertTrue(deleted);
	}
}
