//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * KeyTeilnehmernummerTest
 */
public class KeyTeilnehmernummerTest {

	private String jahr1 = "2017";

	private String jahr2 = "208";

	private String kuerzel1 = "gdagi";

	private String kuerzel2 = "sajdg";

	private TeilnahmeIdentifierProvider prov1 = Mockito.mock(TeilnahmeIdentifierProvider.class);

	private TeilnahmeIdentifierProvider prov2 = Mockito.mock(TeilnahmeIdentifierProvider.class);

	@Test
	void gleich() {
		final TeilnahmeIdentifier ti1 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov1.provideTeilnahmeIdentifier()).thenReturn(ti1);

		final TeilnahmeIdentifier ti2 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov2.provideTeilnahmeIdentifier()).thenReturn(ti2);

		final KeyTeilnehmernummer key1 = new KeyTeilnehmernummer(prov1, Klassenstufe.EINS);
		final KeyTeilnehmernummer key2 = new KeyTeilnehmernummer(prov2, Klassenstufe.EINS);

		assertEquals(key1, key2);
		assertEquals(key1.hashCode(), key2.hashCode());
	}

	@Test
	void verschiedeneJahre() {
		final TeilnahmeIdentifier ti1 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov1.provideTeilnahmeIdentifier()).thenReturn(ti1);

		final TeilnahmeIdentifier ti2 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr2);
		Mockito.when(prov2.provideTeilnahmeIdentifier()).thenReturn(ti2);

		final KeyTeilnehmernummer key1 = new KeyTeilnehmernummer(prov1, Klassenstufe.EINS);
		final KeyTeilnehmernummer key2 = new KeyTeilnehmernummer(prov2, Klassenstufe.EINS);

		assertFalse(key1.equals(key2));
		assertFalse(key1.hashCode() == key2.hashCode());
	}

	@Test
	void verschiedeneKuerzel() {
		final TeilnahmeIdentifier ti1 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov1.provideTeilnahmeIdentifier()).thenReturn(ti1);

		final TeilnahmeIdentifier ti2 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel2, jahr1);
		Mockito.when(prov2.provideTeilnahmeIdentifier()).thenReturn(ti2);

		final KeyTeilnehmernummer key1 = new KeyTeilnehmernummer(prov1, Klassenstufe.EINS);
		final KeyTeilnehmernummer key2 = new KeyTeilnehmernummer(prov2, Klassenstufe.EINS);

		assertFalse(key1.equals(key2));
		assertFalse(key1.hashCode() == key2.hashCode());
	}

	@Test
	void verschiedeneTeilnahmearten() {
		final TeilnahmeIdentifier ti1 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov1.provideTeilnahmeIdentifier()).thenReturn(ti1);

		final TeilnahmeIdentifier ti2 = TeilnahmeIdentifier.create(Teilnahmeart.P, kuerzel1, jahr1);
		Mockito.when(prov2.provideTeilnahmeIdentifier()).thenReturn(ti2);

		final KeyTeilnehmernummer key1 = new KeyTeilnehmernummer(prov1, Klassenstufe.EINS);
		final KeyTeilnehmernummer key2 = new KeyTeilnehmernummer(prov2, Klassenstufe.EINS);

		assertFalse(key1.equals(key2));
		assertFalse(key1.hashCode() == key2.hashCode());
	}

	@Test
	void verschiedeneKlassenstufen() {
		final TeilnahmeIdentifier ti1 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov1.provideTeilnahmeIdentifier()).thenReturn(ti1);

		final TeilnahmeIdentifier ti2 = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel1, jahr1);
		Mockito.when(prov2.provideTeilnahmeIdentifier()).thenReturn(ti2);

		final KeyTeilnehmernummer key1 = new KeyTeilnehmernummer(prov1, Klassenstufe.EINS);
		final KeyTeilnehmernummer key2 = new KeyTeilnehmernummer(prov2, Klassenstufe.ZWEI);

		assertFalse(key1.equals(key2));
		assertFalse(key1.hashCode() == key2.hashCode());
	}
}
