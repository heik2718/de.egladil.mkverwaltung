//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.plausi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.payload.response.Kontext;

/**
 * CheckWettbewerbsjahrCommandTest
 */
public class CheckWettbewerbsjahrCommandTest {
	private Kontext mockKontext;

	@BeforeEach
	void setUp() {
		mockKontext = new Kontext();
		mockKontext.setWettbewerbsjahr("2018");
		KontextReader.getInstance().setMockKontextForTest(mockKontext);
	}

	@Test
	@DisplayName("keine Exception wenn jahr stimmt")
	void jahrStimmt() {
		try {
			new CheckWettbewerbsjahrCommand("2018").execute();
		} catch (final PreconditionFailedException e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("IllegalArgumentException wenn parameter jahr null")
	void constructorJahrNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new CheckWettbewerbsjahrCommand(null);
		});
		assertEquals("jahr null", ex.getMessage());
	}

	@Test
	@DisplayName("PreconditionFailedException wenn jahr in Zukunft")
	void jahrZuGross() {
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			new CheckWettbewerbsjahrCommand("2019").execute();
		});
		assertEquals("jahr ist nicht das aktuelle Wettbewerbsjahr", ex.getMessage());
	}

	@Test
	@DisplayName("PreconditionFailedException wenn jahr in Vergangenheit")
	void jahrZuKlein() {
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			new CheckWettbewerbsjahrCommand("2017").execute();
		});
		assertEquals("jahr ist nicht das aktuelle Wettbewerbsjahr", ex.getMessage());
	}

	@Test
	@DisplayName("PreconditionFailedException wenn jahr irgendwas")
	void jahrIrgendwas() {
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			new CheckWettbewerbsjahrCommand(UUID.randomUUID().toString().substring(0, 8)).execute();
			;
		});
		assertEquals("jahr ist nicht das aktuelle Wettbewerbsjahr", ex.getMessage());
	}
}
