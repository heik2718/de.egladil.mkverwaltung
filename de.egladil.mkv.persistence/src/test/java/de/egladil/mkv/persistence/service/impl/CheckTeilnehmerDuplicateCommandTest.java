//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * CheckTeilnehmerDuplicateCommandTest
 */
public class CheckTeilnehmerDuplicateCommandTest {

	private static final String KUERZEL_TEILNAHME = "ZTFGR65Z";

	private static final String KUERZEL_VORHANDENER_TEILNEHMER = "AEDF65FR201711241034";

	private static final String JAHR = "2018";

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private PublicTeilnehmer teilnehmerPayload;

	private PublicTeilnehmer teilnehmerPayloadMitZusatz;

	private Privatteilnahme teilnahme;

	private Teilnehmer duplicate;

	private List<Teilnehmer> trefferliste;

	private CheckTeilnehmerDuplicateCommand command;

	@BeforeEach
	void setUp() {
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(KUERZEL_TEILNAHME, JAHR);

		teilnehmerPayload = new PublicTeilnehmer();
		teilnehmerPayload.setNachname("Schlemmer");
		final PublicKlassenstufe klassenstufe = PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI);
		teilnehmerPayload.setKlassenstufe(klassenstufe);
		teilnehmerPayload.setVorname("Rudi");

		teilnehmerPayloadMitZusatz = new PublicTeilnehmer();
		teilnehmerPayloadMitZusatz.setNachname("Schlemmer");
		teilnehmerPayloadMitZusatz.setKlassenstufe(klassenstufe);
		teilnehmerPayloadMitZusatz.setVorname("Rudi");
		teilnehmerPayloadMitZusatz.setZusatz("ghagsdga");

		teilnahme = new Privatteilnahme();
		teilnahme.setKuerzel(KUERZEL_TEILNAHME);
		teilnahme.setJahr(JAHR);

		duplicate = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.valueOf(teilnehmerPayload.getKlassenstufe().getName()))
			.vorname(teilnehmerPayload.getVorname()).nachname(teilnehmerPayload.getNachname()).checkAndBuild();
		duplicate.setKuerzel(KUERZEL_VORHANDENER_TEILNEHMER);

		trefferliste = Arrays.asList(new Teilnehmer[] { duplicate });

		command = new CheckTeilnehmerDuplicateCommand(teilnahmeIdentifier, teilnehmerPayload, trefferliste);
	}

	@Test
	@DisplayName("EgladilDuplicateEntryException wenn attribute gleich, kuerzel verschieden")
	void duplicates() {
		final Throwable ex = assertThrows(EgladilDuplicateEntryException.class, () -> {
			// Arrange
			assertTrue(command.hasEqualAttributes(duplicate));
			assertFalse(duplicate.getKuerzel().equals(teilnehmerPayload.getKuerzel()));

			// Act
			command.execute();
		});

		final EgladilDuplicateEntryException dupEx = (EgladilDuplicateEntryException) ex;
		assertEquals("uk_teilnehmer_2", dupEx.getUniqueIndexName());
	}

	@Test
	@DisplayName("keine EgladilDuplicateEntryException wenn attribute und kuerzel gleich")
	void keineDuplicates() {
		// Arrange
		teilnehmerPayload.setKuerzel(duplicate.getKuerzel());
		assertTrue(command.hasEqualAttributes(duplicate));

		// Act
		command.execute();
	}

	@Test
	@DisplayName("nicht equal, wenn vorname, nachname, klasse gleich, aber ein zusatz null")
	void hasEqualAttributesZusatzNullUndNichtNull() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.ZWEI)
			.vorname(teilnehmerPayloadMitZusatz.getVorname()).nachname(teilnehmerPayloadMitZusatz.getNachname()).checkAndBuild();
		assertNull(teilnehmer.getZusatz());
		assertNotNull(teilnehmerPayloadMitZusatz.getZusatz());
		assertFalse(new CheckTeilnehmerDuplicateCommand(teilnahmeIdentifier, teilnehmerPayloadMitZusatz, trefferliste)
			.hasEqualAttributes(teilnehmer));
	}

	@Test
	@DisplayName("nicht equal,wenn vorname, nachname, klasse gleich, beide zusatz nicht null, verschieden")
	void hasEqualAttributesZusatzNichtNullVerschieden() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.ZWEI)
			.vorname(teilnehmerPayloadMitZusatz.getVorname()).nachname(teilnehmerPayloadMitZusatz.getNachname()).zusatz("agsigi")
			.checkAndBuild();

		assertFalse(command.hasEqualAttributes(teilnehmer));
	}

	@Test
	@DisplayName("gleich, wenn alle Attribute gleich")
	void equalAttributes() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.ZWEI)
			.vorname(teilnehmerPayloadMitZusatz.getVorname()).nachname(teilnehmerPayloadMitZusatz.getNachname())
			.zusatz(teilnehmerPayloadMitZusatz.getZusatz()).checkAndBuild();

		assertFalse(command.hasEqualAttributes(teilnehmer));
	}

	@Test
	@DisplayName("gleich, nur vornamen verschieden")
	void hasEqualAttributesZusatzNurVornamenVerschieden() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.ZWEI).vorname("Walter")
			.nachname(teilnehmerPayloadMitZusatz.getNachname()).zusatz(teilnehmerPayloadMitZusatz.getZusatz()).checkAndBuild();

		assertFalse(command.hasEqualAttributes(teilnehmer));
	}

	@Test
	@DisplayName("gleich, nur nachnamen verschieden")
	void hasEqualAttributesZusatzNurNachnamenVerschieden() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.ZWEI)
			.vorname(teilnehmerPayloadMitZusatz.getVorname()).nachname("Müller").zusatz(teilnehmerPayloadMitZusatz.getZusatz())
			.checkAndBuild();

		assertFalse(command.hasEqualAttributes(teilnehmer));
	}

	@Test
	@DisplayName("alle gleich")
	void hasEqualAttributesWirklicheDublette() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.EINS)
			.vorname("Mike").nachname("Müller").zusatz(teilnehmerPayloadMitZusatz.getZusatz())
			.checkAndBuild();

		assertFalse(command.hasEqualAttributes(teilnehmer));
	}

}
