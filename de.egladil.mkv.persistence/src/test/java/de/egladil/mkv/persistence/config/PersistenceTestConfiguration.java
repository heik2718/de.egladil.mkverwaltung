//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.config;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.OsUtils;

/**
 * PersistenceTestConfiguration
 */
public class PersistenceTestConfiguration extends AbstractEgladilConfiguration {
	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von PersistenceTestConfiguration
	 */
	public PersistenceTestConfiguration() {
		this(OsUtils.getDevConfigRoot());

	}

	/**
	 * Erzeugt eine Instanz von PersistenceTestConfiguration
	 */
	public PersistenceTestConfiguration(String pathConfigRoot) {
		super(OsUtils.getDevConfigRoot());
	}

	@Override
	protected String getConfigFileName() {
		return "mkv_service.properties";
	}

};