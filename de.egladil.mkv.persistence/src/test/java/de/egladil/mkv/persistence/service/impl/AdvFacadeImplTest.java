//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAdvTextDao;
import de.egladil.mkv.persistence.dao.IAdvVereinbarungDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.adv.AdvText;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.payload.request.AdvVereinbarungAntrag;

/**
 * AdvFacadeImplTest
 */
public class AdvFacadeImplTest {

	private static final String BENUTZER_UUID = "baasjkbd";

	private static final String SCHULKUERZEL = "H5363637";

	private IAdvTextDao advTextDao;

	private IAdvVereinbarungDao advVereinbarungDao;

	private ISchuleDao schuleDao;

	private AdvVereinbarungAntrag antrag;

	@BeforeEach
	void setUp() {
		advTextDao = Mockito.mock(IAdvTextDao.class);
		advVereinbarungDao = Mockito.mock(IAdvVereinbarungDao.class);
		schuleDao = Mockito.mock(ISchuleDao.class);
		final AdvText aktuellerText = new AdvText();
		aktuellerText.setChecksumme("sahlshqh");
		aktuellerText.setDateiname("shagsdqg");
		aktuellerText.setVersionsnummer("1.0");

		final Schule schule = new Schule();
		schule.setKuerzel(SCHULKUERZEL);

		Mockito.when(advTextDao.findAll(AdvText.class)).thenReturn(Arrays.asList(new AdvText[] { aktuellerText }));
		Mockito.when(advVereinbarungDao.findBySchulkuerzel(SCHULKUERZEL)).thenReturn(Optional.empty());
		Mockito.when(schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, SCHULKUERZEL))
			.thenReturn(Optional.of(schule));

		antrag = new AdvVereinbarungAntrag();
		antrag.setLaendercode("de");
		antrag.setHausnummer("35");
		antrag.setPlz("23456");
		antrag.setSchulkuerzel(SCHULKUERZEL);
		antrag.setStrasse("Schulstraße");
		antrag.setSchulname("Neißekinder");
	}

	@Test
	void findOrCreateMitBenutzerUuidNull() {
		// Arrange
		final AdvFacadeImpl facade = new AdvFacadeImpl(advTextDao, advVereinbarungDao, schuleDao);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			facade.createAdvVereinbarung(null, new AdvVereinbarungAntrag());
		});
		assertEquals("benutzerUuid blank", ex.getMessage());
	}

	@Test
	void findOrCreateMitBenutzerUuidBlank() {
		// Arrange
		final AdvFacadeImpl facade = new AdvFacadeImpl(advTextDao, advVereinbarungDao, schuleDao);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			facade.createAdvVereinbarung("  ", antrag);
		});
		assertEquals("benutzerUuid blank", ex.getMessage());
	}

	@Test
	void findOrCreateMitAntragNull() {
		// Arrange
		final AdvFacadeImpl facade = new AdvFacadeImpl(advTextDao, advVereinbarungDao, schuleDao);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			facade.createAdvVereinbarung(BENUTZER_UUID, null);
		});
		assertEquals("antrag null", ex.getMessage());
	}

	@Test
	void findOrCreateSchuleNichtGefunden() {
		// Arrange
		Mockito.when(schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, SCHULKUERZEL))
			.thenReturn(Optional.empty());
		final AdvFacadeImpl facade = new AdvFacadeImpl(advTextDao, advVereinbarungDao, schuleDao);

		// Act + Assert
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			facade.createAdvVereinbarung(BENUTZER_UUID, antrag);
		});
		assertEquals("Keine Schule mit kuerzel H5363637 vorhanden", ex.getMessage());
	}

	@Test
	void findOrCreateKeinAktuellsterAdvText() {
		// Arrange
		Mockito.when(advTextDao.findAll(AdvText.class)).thenReturn(new ArrayList<>());
		final AdvFacadeImpl facade = new AdvFacadeImpl(advTextDao, advVereinbarungDao, schuleDao);

		// Act + Assert
		final Throwable ex = assertThrows(PreconditionFailedException.class, () -> {
			facade.createAdvVereinbarung(BENUTZER_UUID, antrag);
		});
		assertEquals("Kein AdvText vorhanden", ex.getMessage());
	}
}
