//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence;

import java.util.Date;
import java.util.UUID;

import de.egladil.bv.aas.payload.EmailPayload;
import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.bv.aas.payload.TempPwdAendernPayload;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * TestUtils
 */
public class TestUtils {

	public static Person validKontaktdaten() {
		final Person kontaktdaten = new Person();
		kontaktdaten.setLastLogin(new Date());
		kontaktdaten.setVorname("Olle, IT");
		kontaktdaten.setNachname("Bolle " + System.currentTimeMillis());
		return kontaktdaten;
	}

	public static final Privatkonto validRandomPrivatkontakt(final String jahr) {
		final Person person = validKontaktdaten();
		person.setLastLogin(new Date());
		final Privatkonto result = new Privatkonto(UUID.randomUUID().toString(), person,
			new Privatteilnahme(jahr, new KuerzelGenerator().generateDefaultKuerzel()), false);
		return result;
	}

	public static final Privatkonto validPrivatkontakt(final String jahr) {
		final Person person = new Person("Olle, IT", "Bolle");
		person.setLastLogin(new Date());
		final Privatkonto result = new Privatkonto(UUID.randomUUID().toString(), person,
			new Privatteilnahme(jahr, new KuerzelGenerator().generateDefaultKuerzel()), false);
		return result;
	}

	public static Privatteilnahme validPrivatteilnahme() {
		final Privatteilnahme result = new Privatteilnahme("2013", new KuerzelGenerator().generateDefaultKuerzel());
		return result;
	}

	public static String getDownloadPath() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "D:\\projekte\\priv\\download";
		}
		return "/home/heike/Downloads/mkv";
	}

	public static Lehrerkonto validActiveLehrerkonto(final String uuid, final String jahr) {
		final Schule schule = new Schule();
		schule.setKuerzel("ZD43EMM6");
		schule.setId(4356l);

		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setId(645l);
		schulteilnahme.setJahr(jahr);
		schulteilnahme.setKuerzel(schule.getKuerzel());

		final Lehrerkonto lehrerkonto = new Lehrerkonto(uuid, new Person("A", "B"), schule, true);
		// final Lehrerteilnahme lehrerteilnahme = new Lehrerteilnahme(schulteilnahme);
		// lehrerkonto.addTeilnahme(lehrerteilnahme);
		lehrerkonto.addSchulteilnahme(schulteilnahme);
		;
		return lehrerkonto;
	}

	public static PasswortAendernPayload validPasswortAendernPayload() {
		final PasswortAendernPayload result = new PasswortAendernPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		result.setPasswort("start123");
		result.setPasswortNeu("start987");
		result.setPasswortNeuWdh("start987");
		return result;
	}

	public static TempPwdAendernPayload validTempPwdAendernPayload() {
		final TempPwdAendernPayload result = new TempPwdAendernPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		result.setPasswortNeu("start987");
		result.setPasswortNeuWdh("start987");
		result.setPasswort("Juhet8Ztrd");
		return result;
	}

	public static EmailPayload validEmailPayload() {
		final EmailPayload result = new EmailPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		return result;
	}

	public static PersonAendernPayload validPersonAendernPayload() {
		final PersonAendernPayload result = new PersonAendernPayload("horst-schlemmer@web.de", "Horst", "Schlemmer", null,
			"MKV_LEHRER");
		return result;
	}

	public static MailadresseAendernPayload validMailadresseAendernPayload() {
		final MailadresseAendernPayload result = new MailadresseAendernPayload("horst-schlemmer@web.de", "hurz323!",
			"horsti@provider.com");
		return result;
	}

	public static void initMKVApiKontextReader(final String wettbewerbsjahr, final boolean neuanmeldungFreigeschaltet) {
		KontextReader.destroy();
		final Kontext mockKontext = new Kontext();
		mockKontext.setWettbewerbsjahr(wettbewerbsjahr);
		mockKontext.setNeuanmeldungFreigegeben(neuanmeldungFreigeschaltet);
		KontextReader.getInstance().setMockKontextForTest(mockKontext);
	}

	/**
	 * Gibt dem Pfad zum Dev-Upload-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevUploadDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/upload/mkv";
		}
		return "/home/heike/upload/mkv";
	}

	/**
	 * Gibt dem Pfad zum Dev-Upload-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevSandboxDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/upload/mkv";
		}
		return "/home/heike/upload/mkv";
	}
}
