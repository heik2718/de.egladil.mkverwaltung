//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;

/**
 * AuswertungDownloadDaoImplIT
 */
public class AuswertungDownloadDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IAuswertungDownloadDao dao;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	@Test
	@DisplayName("findAll")
	void findAllKLappt() {
		final List<AuswertungDownload> downloads = dao.findAll(AuswertungDownload.class);

		assertNotNull(downloads);
	}

}
