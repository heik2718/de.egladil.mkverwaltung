//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.file.VerzeichnisKinderProvider;

/**
 * VerzeichnisKinderProviderTest
 */
public class VerzeichnisKinderProviderTest {

	private static final Logger LOG = LoggerFactory.getLogger(VerzeichnisKinderProviderTest.class);

	@Test
	public void constructor_1_klappt() throws Exception {
		// Act
		final VerzeichnisKinderProvider provider = new VerzeichnisKinderProvider(TestUtils.getDevUploadDir() + "/mail");

		// Assert
		final List<File> files = provider.getFiles();
		assertEquals(4, files.size());
		for (final File f : files) {
			LOG.debug("File {}", f.getName());
		}
	}

	@Test
	public void constructor_2_klappt() throws Exception {
		// Act
		final VerzeichnisKinderProvider provider = new VerzeichnisKinderProvider(TestUtils.getDevUploadDir() + "/mail/0920183b-f3a1-4815-8ed2-ecad908a2167");

		// Assert
		final List<File> files = provider.getFiles();
		assertEquals(2, files.size());
		for (final File f : files) {
			LOG.debug("File {}", f.getName());
		}
	}
}
