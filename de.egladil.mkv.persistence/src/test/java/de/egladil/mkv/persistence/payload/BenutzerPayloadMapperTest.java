//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * BenutzerPayloadMapperTest
 */
public class BenutzerPayloadMapperTest {

	private static final String AKTJAHR = "2017";

	private BenutzerPayloadMapper benuztzerPayloadMapper;

	private IBenutzerService benutzerService;

	private ILehrerkontoDao lehrerkontoDao;

	private IPrivatkontoDao privatkontoDao;

	private ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private TeilnehmerFacade teilnehmerFacade;

	private TeilnahmenFacade teilnahmenFacade;


	private IKatalogService katalogService;

	private AdvFacade advFacade;

	@BeforeEach
	public void setUp() {
		TestUtils.initMKVApiKontextReader(AKTJAHR, true);
		benutzerService = Mockito.mock(IBenutzerService.class);
		lehrerteilnahmeInfoDao = Mockito.mock(ILehrerteilnahmeInfoDao.class);
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		teilnehmerFacade = Mockito.mock(TeilnehmerFacade.class);
		teilnahmenFacade = Mockito.mock(TeilnahmenFacade.class);
		advFacade = Mockito.mock(AdvFacade.class);
		katalogService = Mockito.mock(IKatalogService.class);
		benuztzerPayloadMapper = new BenutzerPayloadMapper(benutzerService, lehrerkontoDao, privatkontoDao, lehrerteilnahmeInfoDao,
			teilnehmerFacade, advFacade, katalogService, teilnahmenFacade);
		// benuztzerPayloadMapper.setDownloadPath(TestUtils.getDownloadPath());
	}

	@Test
	public void createPublicMKVUserUuidNull() {
		// Arrange
		final String benutzerUUID = UUID.randomUUID().toString();
		final String accessTokenId = UUID.randomUUID().toString();
		final String csrfToken = UUID.randomUUID().toString();

		final AccessToken accessToken = new AccessToken(accessTokenId);
		accessToken.setCsrfToken(csrfToken);
		accessToken.setPrimaryPrincipal(benutzerUUID);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			final String uuid = null;
			benuztzerPayloadMapper.createBenutzer(uuid, false);
		});
		assertEquals("uuid blank", ex.getMessage());

	}

	@Test
	public void createPublicMKVUserUuidLeer() {
		// Arrange
		final String benutzerUUID = UUID.randomUUID().toString();
		final String accessTokenId = UUID.randomUUID().toString();
		final String csrfToken = UUID.randomUUID().toString();

		final AccessToken accessToken = new AccessToken(accessTokenId);
		accessToken.setCsrfToken(csrfToken);
		accessToken.setPrimaryPrincipal(benutzerUUID);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			final String uuid = " ";
			benuztzerPayloadMapper.createBenutzer(uuid, false);
		});
		assertEquals("uuid blank", ex.getMessage());

	}

	@Test
	public void createPublicMKVUserBenutzerNull() {
		// Arrange
		final String benutzerUUID = UUID.randomUUID().toString();
		final String accessTokenId = UUID.randomUUID().toString();
		final String csrfToken = UUID.randomUUID().toString();

		final AccessToken accessToken = new AccessToken(accessTokenId);
		accessToken.setCsrfToken(csrfToken);
		accessToken.setPrimaryPrincipal(benutzerUUID);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			final Benutzerkonto benutzer = null;
			benuztzerPayloadMapper.createBenutzer(benutzer, false);
		});
		assertEquals("benutzer null", ex.getMessage());

	}

}
