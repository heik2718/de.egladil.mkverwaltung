//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
* AuswertungsgruppeDaoAdapter
*/
public class AuswertungsgruppeDaoAdapter implements IAuswertungsgruppeDao {

	@Override
	public List<Auswertungsgruppe> findAll(final Class<Auswertungsgruppe> clazz) throws PersistenceException {
		return null;
	}

	@Override
	public Optional<Auswertungsgruppe> findById(final Class<Auswertungsgruppe> clazz, final long id) {
		return Optional.empty();
	}

	@Override
	public Auswertungsgruppe persist(final Auswertungsgruppe entity) throws PersistenceException {
		return null;
	}

	@Override
	public List<Auswertungsgruppe> persist(final List<Auswertungsgruppe> entities) throws PersistenceException {
		return null;
	}

	@Override
	public String delete(final Auswertungsgruppe entity) throws PersistenceException {
		return "";
	}

	@Override
	public void delete(final List<Auswertungsgruppe> entities) throws PersistenceException {
	}

	@Override
	public Optional<Auswertungsgruppe> findByUniqueKey(final Class<Auswertungsgruppe> clazz, final String attributeName, final String attributeValue)
		throws MKVException, EgladilStorageException {
		return Optional.empty();
	}

	@Override
	public Optional<Auswertungsgruppe> findRootByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		return Optional.empty();
	}

	@Override
	public List<Auswertungsgruppe> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		return null;
	}

	@Override
	public List<Auswertungsgruppe> findRootgruppenByJahr(final String jahr) {
		return null;
	}

}
