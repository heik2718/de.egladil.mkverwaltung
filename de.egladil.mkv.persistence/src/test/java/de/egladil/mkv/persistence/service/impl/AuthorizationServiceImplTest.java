//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeInfoDao;
import de.egladil.mkv.persistence.domain.BenutzerstatusProvider;

/**
 * AuthorizationServiceImplTest
 */
public class AuthorizationServiceImplTest {

	private AuthorizationServiceImpl authService;

	private BenutzerstatusProvider benutzerstusProvider;

	@BeforeEach
	void setUp() {
		authService = new AuthorizationServiceImpl(Mockito.mock(ILehrerteilnahmeInfoDao.class),
			Mockito.mock(IPrivatteilnahmeInfoDao.class), Mockito.mock(ILehrerkontoDao.class));
		benutzerstusProvider = Mockito.mock(BenutzerstatusProvider.class);
	}

	@Test
	void nullIsNotAuthorized() {
		assertFalse(authService.isAuthorized(null));
	}

	@Test
	void aktiviertNichtAnonymGesperrtIsNotAuthorized() {
		Mockito.when(benutzerstusProvider.isAktiviert()).thenReturn(true);
		Mockito.when(benutzerstusProvider.isAnonym()).thenReturn(false);
		Mockito.when(benutzerstusProvider.isGesperrt()).thenReturn(true);

		assertFalse(authService.isAuthorized(benutzerstusProvider));
	}

	@Test
	void anonymNichtGesperrtIsNotAuthorized() {
		Mockito.when(benutzerstusProvider.isAktiviert()).thenReturn(true);
		Mockito.when(benutzerstusProvider.isAnonym()).thenReturn(true);
		Mockito.when(benutzerstusProvider.isGesperrt()).thenReturn(false);

		assertFalse(authService.isAuthorized(benutzerstusProvider));
	}

	@Test
	void nichtAnonymNichtAktiviertIsNotAuthorized() {

		Mockito.when(benutzerstusProvider.isAktiviert()).thenReturn(false);
		Mockito.when(benutzerstusProvider.isAnonym()).thenReturn(false);
		Mockito.when(benutzerstusProvider.isGesperrt()).thenReturn(false);

		assertFalse(authService.isAuthorized(benutzerstusProvider));
	}

	@Test
	void aktiviertNichtAnonymNichtGesperrtIstAuthorized() {

		Mockito.when(benutzerstusProvider.isAktiviert()).thenReturn(true);
		Mockito.when(benutzerstusProvider.isAnonym()).thenReturn(false);
		Mockito.when(benutzerstusProvider.isGesperrt()).thenReturn(false);

		assertTrue(authService.isAuthorized(benutzerstusProvider));
	}

}
