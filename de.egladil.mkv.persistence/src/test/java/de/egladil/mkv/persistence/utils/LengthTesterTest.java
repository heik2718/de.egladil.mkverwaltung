//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * LengthTesterTest
 */
public class LengthTesterTest {

	@Test
	@DisplayName("testen wrapping für umbruch exakt")
	void needsWrappingUmbruchExakt() {
		final boolean actual = new LengthTester().needsWrapping("Universums aus denen alle chemisch en",
			new Integer[] { UrkundeConstants.SIZE_NAME_SMALL });
		assertFalse(actual);
	}

	@Test
	@DisplayName("testen wrapping für umbruch bei schriftgröße normal")
	void needsWrappingSizeNormal() {
		final boolean actual = new LengthTester().needsWrapping("Universums aus denen alle chemischen Eleme",
			new Integer[] { UrkundeConstants.SIZE_TEXT_NORMAL });
		assertFalse(actual);
	}

	@Test
	@DisplayName("obere Längengrenze")
	void checkLengtPasstGerade() {
		new LengthTester().checkTooLongAndThrow("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teil",
			UrkundeConstants.SIZE_NAME_SMALL);

	}

	@Test
	@DisplayName("Teilnehmername ein Zeichen zu lang")
	void teilnehmernameZuLang() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new LengthTester().checkTooLongAndThrow("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilch",
				UrkundeConstants.SIZE_NAME_SMALL);
		});
		assertEquals("name passt nicht in drei Zeilen", ex.getMessage());

	}

	@Test
	@DisplayName("Schulname ein Zeichen zu lang")
	void schulnameZuLang() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new LengthTester().checkTooLongAndThrow(
				"Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilchen zu Kette",
				UrkundeConstants.SIZE_TEXT_NORMAL);
		});
		assertEquals("name passt nicht in drei Zeilen", ex.getMessage());

	}

	@Test
	@DisplayName("testen wrapping für umbruch exakt")
	void needsWrappingLangerSchulname() {
		final boolean actual = new LengthTester().needsWrapping("Neue Grundschule Potsdam anerkannte Ersatzschule",
			new Integer[] { UrkundeConstants.SIZE_NAME_SMALL });
		assertTrue(actual);
	}

}
