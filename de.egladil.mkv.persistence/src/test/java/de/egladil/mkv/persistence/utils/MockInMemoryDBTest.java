//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.mkv.persistence.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;

/**
 * MockInMemoryDBTest
 */
public class MockInMemoryDBTest {
	private MockInMemoryDB db;

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() {

		db = new MockInMemoryDB();
	}

	// FIXME: hier kann noch das Wechseln der Schule gespielt werden!!!
	@Test
	public void findLehrerkontoByUuid_klappt() {
		Lehrerkonto lehrerkonto1 = null;
		Lehrerkonto lehrerkonto2 = null;
		{
			final Optional<Lehrerkonto> opt1 = db.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_1);
			assertTrue(opt1.isPresent());
			lehrerkonto1 = opt1.get();
			final Schule schule = lehrerkonto1.getSchule();
			assertNotNull(schule);
			assertEquals(schule.getKuerzel(), MockInMemoryDB.SCHULKUERZEL_1);
			final List<Schulteilnahme> schulteilnahmen = db.findSchulteilnahmen(schule);
			assertEquals(2, schulteilnahmen.size());
			// List<Lehrerteilnahme> lehrerteilnahmen = lehrerkonto1.getTeilnahmen();
			final List<Schulteilnahme> lehrerteilnahmen = lehrerkonto1.getSchulteilnahmen();
			assertEquals(2, lehrerteilnahmen.size());
			final List<String> jahreTeilnahmen = new ArrayList<>();
			for (final Schulteilnahme t : lehrerteilnahmen) {
				final String jahr = t.getJahr();
				if (!jahreTeilnahmen.contains(jahr)) {
					jahreTeilnahmen.add(jahr);
				}
			}
			assertTrue(jahreTeilnahmen.contains("2016"));
			assertTrue(jahreTeilnahmen.contains("2017"));
			final TeilnahmeIdentifierProvider aktuelleTeilnahmen = lehrerkonto1.getTeilnahmeZuJahr("2017");
			assertNotNull(aktuelleTeilnahmen);
		}
		{
			final Optional<Lehrerkonto> opt2 = db.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_2);
			assertTrue(opt2.isPresent());
			lehrerkonto2 = opt2.get();
			final Schule schule = lehrerkonto2.getSchule();
			assertNotNull(schule);
			assertEquals(schule.getKuerzel(), MockInMemoryDB.SCHULKUERZEL_1);
			final List<Schulteilnahme> schulteilnahmen = db.findSchulteilnahmen(schule);
			assertEquals(2, schulteilnahmen.size());
			final List<Schulteilnahme> lehrerteilnahmen = lehrerkonto2.getSchulteilnahmen();
			assertEquals(1, lehrerteilnahmen.size());
			final List<String> jahreTeilnahmen = new ArrayList<>();
			for (final Schulteilnahme t : lehrerteilnahmen) {
				final String jahr = t.getJahr();
				if (!jahreTeilnahmen.contains(jahr)) {
					jahreTeilnahmen.add(jahr);
				}
			}
			assertTrue(jahreTeilnahmen.contains("2017"));
			final TeilnahmeIdentifierProvider aktuelleTeilnahmen = lehrerkonto2.getTeilnahmeZuJahr("2017");
			assertNotNull(aktuelleTeilnahmen);
		}
	}
}
