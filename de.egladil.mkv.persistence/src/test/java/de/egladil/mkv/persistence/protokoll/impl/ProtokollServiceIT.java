//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.protokoll.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;

/**
 * ProtokollServiceIT
 */
public class ProtokollServiceIT extends AbstractGuiceIT {

	@Inject
	private ProtokollService protokollService;

	@Inject
	private IEreignisDao ereignisDao;

	@Test
	public void protokollieren_klappt() {
		// Arrange
		List<Ereignis> ereignisse = ereignisDao.findAll(Ereignis.class);
		final int expectedAnzahl = ereignisse.size() + 1;
		final Ereignisart ereignisart = Ereignisart.DOWNLOAD_OHNE_RECHT;
		final String wer = "eine-testende-person";
		final String was = "hat eine Datei heruntergeladen";

		// Act
		protokollService.protokollieren(ereignisart.getKuerzel(), wer, was);

		// Assert
		ereignisse = ereignisDao.findAll(Ereignis.class);
		final int actualAnzahl = ereignisse.size();

		assertEquals(expectedAnzahl, actualAnzahl);

	}

}
