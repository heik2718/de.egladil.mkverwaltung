//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.protokoll.impl;

import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;

/**
 * ProtokollServiceTest
 */
public class ProtokollServiceTest {

	private ProtokollService protokollService;

	@BeforeEach
	public void setUp() {
		final IEreignisDao ereignisDao = Mockito.mock(IEreignisDao.class);
		final IEgladilConfiguration configuration = Mockito.mock(IEgladilConfiguration.class);
		Mockito.when(configuration.getProperty("protokoll.modus")).thenReturn("LOG");
		protokollService = new ProtokollService(ereignisDao, configuration);
	}

	@Test
	public void protokollieren_throws_when_ereignisart_null() {
		final Ereignis ereignis = protokollService.protokollieren(null, "ICH", null);
		assertNull(ereignis);
	}

	@Test
	public void protokollieren_throws_when_wer_null() {
		final Ereignis ereignis = protokollService.protokollieren(Ereignisart.DOWNLOAD_OHNE_RECHT.getKuerzel(), null, null);
		assertNull(ereignis);
	}

	@Test
	public void protokollieren_throws_when_wer_blank() {
		final Ereignis ereignis = protokollService.protokollieren(Ereignisart.DOWNLOAD_OHNE_RECHT.getKuerzel(), " ", null);
		assertNull(ereignis);

	}

	@Test
	public void protokollieren_ins_log_klappt() {
		protokollService.protokollieren(Ereignisart.KURZZEITPASSWORT_ANGEFORDERT.getKuerzel(), "boesewicht@atnt.us", null);
	}
}
