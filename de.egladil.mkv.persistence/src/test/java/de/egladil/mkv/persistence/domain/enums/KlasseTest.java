package de.egladil.mkv.persistence.domain.enums;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class KlasseTest {

	@Test
	public void getLabel_klasse1_klappt() {
		assertEquals("Klasse 1", Klassenstufe.EINS.getLabel());
	}

	@Test
	public void getLabel_klasse2_klappt() {
		assertEquals("Klasse 2", Klassenstufe.ZWEI.getLabel());
	}

}
