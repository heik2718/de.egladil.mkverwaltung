//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;

/**
 * TeilnahmenFacadeImplIT
 */
public class TeilnahmenFacadeImplIT extends AbstractGuiceIT {

	private static final String LEHRER_UUID_VADER = "fd77d23c-198d-4554-877d-91931c43a309";

	private static final String PRIVAT_UUID = "bf01e395-7d12-4e53-8a7c-2d595ae7c263";

	private static final String LEHRER_UUID_KOLLEGE = "f0ff76c0-81d5-43ad-94a5-09c2beb42787";

	@Inject
	private TeilnahmenFacade teilnahmenFacade;

	@Inject
	private IBenutzerService benutzerService;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		TestUtils.initMKVApiKontextReader("2019", true);

	}

	@Test
	void testeLehrerteilnahmen() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(LEHRER_UUID_VADER);

		// Act
		final List<PublicTeilnahme> teilnahmen = teilnahmenFacade.getAllTeilnahmen(benutzerkonto);

		// Assert
		assertEquals(8, teilnahmen.size());

		final PublicTeilnahme aktuelleTeilnahme = teilnahmen.get(0);
		assertTrue(aktuelleTeilnahme.isAktuelle());
		assertNotNull(aktuelleTeilnahme.getSchule());
		assertNotNull(aktuelleTeilnahme.getKollegen());
		assertTrue(aktuelleTeilnahme.getAnzahlLoesungszettel() >= 0l);
		assertTrue(aktuelleTeilnahme.getAnzahlTeilnehmer() >= 0);
		assertEquals(0, aktuelleTeilnahme.getAnzahlUploads());
		{
			final HateoasPayload hateoasPayload = aktuelleTeilnahme.getHateoasPayload();
			assertEquals(2, hateoasPayload.getLinks().size());
			assertEquals("9PDDS51Q", hateoasPayload.getId());
			assertEquals("/teilnahmen/2019/S/9PDDS51Q", hateoasPayload.getUrl());
			for (final HateoasLink link : hateoasPayload.getLinks()) {
				assertEquals("GET", link.getMethod());
				assertEquals("application/json", link.getMediatype());
			}
		}

		for (int i = 1; i < teilnahmen.size(); i++) {
			final PublicTeilnahme pt = teilnahmen.get(i);
			final String jahr = pt.getTeilnahmeIdentifier().getJahr();

			assertFalse(pt.isAktuelle());
			assertNull(pt.getKollegen());
			assertNotNull(pt.getSchule());

			final HateoasPayload hateoasPayload = pt.getHateoasPayload();
			assertEquals(1, hateoasPayload.getLinks().size());

			String kuerzel = null;
			final Integer jahrAsInt = Integer.valueOf(pt.getTeilnahmeIdentifier().getJahr());
			switch (jahrAsInt) {
			case 2017:
				kuerzel = "YG9D32CD";
				assertTrue(pt.getAnzahlLoesungszettel() > 0l);
				assertTrue(pt.getAnzahlTeilnehmer() > 0);
				assertTrue(pt.getAnzahlUploads() > 0);
				break;
			case 2018:
				kuerzel = "9PDDS51Q";
				assertTrue(pt.getAnzahlLoesungszettel() > 0l);
				assertTrue(pt.getAnzahlTeilnehmer() > 0);
				assertEquals(0, pt.getAnzahlUploads());
				break;
			default:
				kuerzel = "YG9D32CD";
				assertEquals(0, pt.getAnzahlLoesungszettel(), "Fehler bei Jahr" + jahr);
				assertEquals(0, pt.getAnzahlTeilnehmer(), "Fehler bei Jahr" + jahr);
				assertEquals(0, pt.getAnzahlUploads(), "Fehler bei Jahr" + jahr);
				break;
			}

			assertEquals(kuerzel, hateoasPayload.getId(), "Fehler bei Jahr " + pt.getTeilnahmeIdentifier().getJahr());
			assertEquals("/teilnahmen/" + jahr + "/S/" + kuerzel, hateoasPayload.getUrl());

			final HateoasLink link = hateoasPayload.getLinks().get(0);
			assertEquals("GET", link.getMethod());
			assertEquals("application/json", link.getMediatype());
			assertEquals("schulstatistik", link.getRel());
			assertEquals("/teilnahmen/" + jahr + "/S/" + kuerzel + "/schulstatistik", link.getUrl());
		}
	}

	@Test
	void testeLehrerteilnahmenNeuerKollege() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(LEHRER_UUID_KOLLEGE);

		// Act
		final List<PublicTeilnahme> teilnahmen = teilnahmenFacade.getAllTeilnahmen(benutzerkonto);

		// Assert
		assertEquals(2, teilnahmen.size());

		final PublicTeilnahme aktuelleTeilnahme = teilnahmen.get(0);
		assertTrue(aktuelleTeilnahme.isAktuelle());
		assertNotNull(aktuelleTeilnahme.getSchule());
		assertNotNull(aktuelleTeilnahme.getKollegen());
		assertTrue(aktuelleTeilnahme.getAnzahlLoesungszettel() >= 0l);
		assertTrue(aktuelleTeilnahme.getAnzahlTeilnehmer() >= 0);
		assertEquals(0, aktuelleTeilnahme.getAnzahlUploads());
		{
			final HateoasPayload hateoasPayload = aktuelleTeilnahme.getHateoasPayload();
			assertEquals(2, hateoasPayload.getLinks().size());
			assertEquals("9PDDS51Q", hateoasPayload.getId());
			assertEquals("/teilnahmen/2019/S/9PDDS51Q", hateoasPayload.getUrl());
			for (final HateoasLink link : hateoasPayload.getLinks()) {
				assertEquals("GET", link.getMethod());
				assertEquals("application/json", link.getMediatype());
			}
		}

		for (int i = 1; i < teilnahmen.size(); i++) {
			final PublicTeilnahme pt = teilnahmen.get(i);
			final String jahr = pt.getTeilnahmeIdentifier().getJahr();

			assertFalse(pt.isAktuelle());
			assertNull(pt.getKollegen());
			assertNotNull(pt.getSchule());

			final HateoasPayload hateoasPayload = pt.getHateoasPayload();
			assertEquals(1, hateoasPayload.getLinks().size());

			String kuerzel = null;
			final Integer jahrAsInt = Integer.valueOf(pt.getTeilnahmeIdentifier().getJahr());
			switch (jahrAsInt) {
			case 2018:
				kuerzel = "9PDDS51Q";
				assertTrue(pt.getAnzahlLoesungszettel() > 0l);
				assertTrue(pt.getAnzahlTeilnehmer() > 0);
				assertEquals(0, pt.getAnzahlUploads());
				break;
			default:
				kuerzel = "YG9D32CD";
				assertEquals(0, pt.getAnzahlLoesungszettel(), "Fehler bei Jahr" + jahr);
				assertEquals(0, pt.getAnzahlTeilnehmer(), "Fehler bei Jahr" + jahr);
				assertEquals(0, pt.getAnzahlUploads(), "Fehler bei Jahr" + jahr);
				break;
			}

			assertEquals(kuerzel, hateoasPayload.getId(), "Fehler bei Jahr " + pt.getTeilnahmeIdentifier().getJahr());
			assertEquals("/teilnahmen/" + jahr + "/S/" + kuerzel, hateoasPayload.getUrl());

			final HateoasLink link = hateoasPayload.getLinks().get(0);
			assertEquals("GET", link.getMethod());
			assertEquals("application/json", link.getMediatype());
			assertEquals("schulstatistik", link.getRel());
			assertEquals("/teilnahmen/" + jahr + "/S/" + kuerzel + "/schulstatistik", link.getUrl());
		}
	}

	@Test
	void testePrivatteilnahmen() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(PRIVAT_UUID);

		// Act
		final List<PublicTeilnahme> teilnahmen = teilnahmenFacade.getAllTeilnahmen(benutzerkonto);
		assertEquals(2, teilnahmen.size());

		final PublicTeilnahme aktuelleTeilnahme = teilnahmen.get(0);
		assertTrue(aktuelleTeilnahme.isAktuelle());
		assertNull(aktuelleTeilnahme.getSchule());
		assertNull(aktuelleTeilnahme.getKollegen());
		assertTrue(aktuelleTeilnahme.getAnzahlLoesungszettel() > 0l);
		assertTrue(aktuelleTeilnahme.getAnzahlTeilnehmer() > 0);
		assertEquals(0, aktuelleTeilnahme.getAnzahlUploads());

		{
			final HateoasPayload hateoasPayload = aktuelleTeilnahme.getHateoasPayload();
			assertEquals(0, hateoasPayload.getLinks().size());
			assertEquals("BPN390UP", hateoasPayload.getId());
			assertEquals("/teilnahmen/2018/P/BPN390UP", hateoasPayload.getUrl());
		}

		final PublicTeilnahme pt = teilnahmen.get(1);

		assertFalse(pt.isAktuelle());
		assertNull(pt.getKollegen());
		assertNull(pt.getSchule());

		{
			final HateoasPayload hateoasPayload = pt.getHateoasPayload();
			assertEquals(0, hateoasPayload.getLinks().size());
			assertEquals("4AKICN7Y", hateoasPayload.getId());
			assertEquals("/teilnahmen/2017/P/4AKICN7Y", hateoasPayload.getUrl());
		}

		assertTrue(pt.getAnzahlLoesungszettel() > 0l);
		assertTrue(pt.getAnzahlUploads() > 0l);
		assertTrue(pt.getAnzahlTeilnehmer() > 0);
	}
}
