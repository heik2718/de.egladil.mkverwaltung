//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.berechnungen.WertungscodeCommand;

/**
* WertungscodeCommandTest
*/
public class WertungscodeCommandTest {

	@Test
	public void berechneWertungscode_klappt(){
		// Arrange
		final String loesungscode = "CDADECCCBCDE";
		final String korrigierteNutzereingaben = "C,N,A,D,E,C,C,C,B,A,D,E";
		final String expected = "rnrrrrrrrfrr";
		final WertungscodeCommand command = new WertungscodeCommand(loesungscode);

		// Act
		final String actual = command.berechneWertungscode(korrigierteNutzereingaben);

		// Assert
		assertEquals(expected, actual);
	}
}
