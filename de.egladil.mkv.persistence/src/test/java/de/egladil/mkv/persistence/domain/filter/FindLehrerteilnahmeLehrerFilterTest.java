//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;

/**
 * FindLehrerteilnahmeJahrFilterTest
 */
public class FindLehrerteilnahmeLehrerFilterTest {

	@Test
	public void findAktuelleTeilnahme_throws_NPE_when_teilnahmen_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new FindLehrerteilnahmeLehrerFilter(235).findTeilnahme(null);
		});
		assertEquals("lehrerteilnahmen", ex.getMessage());

	}

	@Test
	public void constructor_throws_NPE_when_jahr_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new FindLehrerteilnahmeJahrFilter(null);
		});
		assertEquals("jahr", ex.getMessage());

	}

	@Test
	public void findAktuelleTeilnahme_treffer() {
		// Arrange
		final List<LehrerteilnahmeInformation> teilnahmen = new ArrayList<>();
		final String expectedKuerzel = "BBBBBBBB";
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel("AAAAAAAA");
			info.setBenutzerId(114l);
			teilnahmen.add(info);
		}
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel(expectedKuerzel);
			info.setBenutzerId(235l);
			teilnahmen.add(info);
		}

		// Act
		final List<LehrerteilnahmeInformation> trefferliste = new FindLehrerteilnahmeLehrerFilter(235).findTeilnahme(teilnahmen);

		// Assert
		assertEquals(1, trefferliste.size());
		final LehrerteilnahmeInformation treffer = trefferliste.get(0);
		assertEquals(expectedKuerzel, treffer.getKuerzel());
	}

	@Test
	public void findAktuelleTeilnahme_kein_treffer() {
		// Arrange
		final List<LehrerteilnahmeInformation> teilnahmen = new ArrayList<>();
		final String expectedKuerzel = "BBBBBBBB";
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel("AAAAAAAA");
			info.setBenutzerId(114l);
			teilnahmen.add(info);
		}
		{
			final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation();
			info.setKuerzel(expectedKuerzel);
			info.setBenutzerId(235l);
			teilnahmen.add(info);
		}

		// Act
		final List<LehrerteilnahmeInformation> trefferliste = new FindLehrerteilnahmeLehrerFilter(42).findTeilnahme(teilnahmen);

		// Assert
		assertTrue(trefferliste.isEmpty());
	}

}
