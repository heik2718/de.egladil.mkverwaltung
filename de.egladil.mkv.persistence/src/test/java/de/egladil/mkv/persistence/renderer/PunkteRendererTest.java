//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.renderer;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;


/**
 * PunkteRendererTest
 */
public class PunkteRendererTest {

	@Test
	public void render_einstellig_klappt() {
		// Arrange
		final String expected = "0";

		// Act
		final String actual = new PunkteRenderer(0).render();

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void render_dreistellig_eine_nachkommastelle_klappt() {
		// Arrange
		final String expected = "5,00";

		// Act
		final String actual = new PunkteRenderer(500).render();

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void render_dreistellig_zwei_nachkommastellen_klappt() {
		// Arrange
		final String expected = "4,75";

		// Act
		final String actual = new PunkteRenderer(475).render();

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void render_vierstellig_eine_nachkommastelle_klappt() {
		// Arrange
		final String expected = "55,50";

		// Act
		final String actual = new PunkteRenderer(5550).render();

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void render_vierstellig_zwei_nachkommastellen_klappt() {
		// Arrange
		final String expected = "47,25";

		// Act
		final String actual = new PunkteRenderer(4725).render();

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void render_vierstellig_keine_nachkommastelle_klappt() {
		// Arrange
		final String expected = "60,00";

		// Act
		final String actual = new PunkteRenderer(6000).render();

		// Assert
		assertEquals(expected, actual);
	}
}
