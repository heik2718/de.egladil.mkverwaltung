//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;

/**
 * BenutzerPrivatMappingStrategyIT
 */
public class BenutzerPrivatMappingStrategyIT extends AbstractGuiceIT {

	private static final String BENUTZER_UUID = "7357f906-f30e-4ca6-b060-21dfa61ff0fe";

	@Inject
	private IPrivatkontoDao privatkontoDao;

	@Inject
	private IBenutzerService benutzerService;

	@Test
	void createPublicMKVUser() throws Exception {
		// Arrange
		TestUtils.initMKVApiKontextReader("2017", true);
		final Benutzerkonto benutzer = benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID);
		final Privatkonto privatkonto = privatkontoDao.findByUUID(BENUTZER_UUID).get();


		// Act
		final MKVBenutzer mkvUser = new BenutzerPrivatMappingStrategy().createMKVBenutzer(benutzer, privatkonto);

		final String json = new ObjectMapper().writeValueAsString(mkvUser);
		System.out.println(json);

		// Assert
		assertNotNull(mkvUser);
		final PersonBasisdaten basisdaten = mkvUser.getBasisdaten();
		assertNotNull(basisdaten);
		assertEquals("MKV_PRIVAT", basisdaten.getRolle());
		assertEquals("Privat", basisdaten.getVorname());
		assertEquals("Kontakt-1475865819362", basisdaten.getNachname());
		assertEquals(benutzer.getEmail(), basisdaten.getEmail());

		final ErweiterteKontodaten erweiterteDaten = mkvUser.getErweiterteKontodaten();
		assertNotNull(erweiterteDaten);
		assertFalse(erweiterteDaten.isAdvVereinbarungVorhanden());
		assertTrue(erweiterteDaten.isMailbenachrichtigung());
		assertNotNull(erweiterteDaten.getAktuelleTeilnahme());
		assertEquals(2, erweiterteDaten.getAnzahlTeilnahmen());
		assertNull(erweiterteDaten.getSchule());

		assertNull(mkvUser.getHateoasPayload());
	}

}
