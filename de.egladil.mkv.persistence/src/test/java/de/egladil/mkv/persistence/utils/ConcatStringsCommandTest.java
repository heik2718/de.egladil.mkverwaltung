//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * ConcatStringsCommandTest
 */
public class ConcatStringsCommandTest {

	@Test
	@DisplayName("ein Wort ohne einschließende Leerzeichen bleibt wie es ist")
	void wortOhneEinschliessendeLeerzeichen() {
		final Text text = new Text(Arrays.asList(new String[] { "wort" }));
		final String expected = "wort";
		new ConcatStringsCommand(text).execute();
		final List<String> output = text.getTransformedText();
		assertEquals(1, output.size());
		assertEquals(expected, output.get(0));
	}

	@Test
	@DisplayName("ein Wort mit einschließenden Leerzeichen wird getrimmt")
	void wortEinschliessendeLeerzeichen() {
		final Text text = new Text(Arrays.asList(new String[] { "  wort   " }));
		final String expected = "wort";
		new ConcatStringsCommand(text).execute();
		final List<String> output = text.getTransformedText();
		assertEquals(1, output.size());
		assertEquals(expected, output.get(0));
	}

	@Test
	@DisplayName("kein Wort ergibt leere Liste")
	void leereListe() {
		final Text text = new Text(Arrays.asList(new String[] {}));
		new ConcatStringsCommand(text).execute();
		final List<String> output = text.getTransformedText();
		assertEquals(0, output.size());
	}

	@Test
	@DisplayName("mehr als ein Wort ohne einchließende Leerzeichen ergibt normale Wortgruppe")
	void listeMitMehrAlsEinemItemKeineLeerzeichen() {
		final Text text = new Text(Arrays.asList(new String[] { "eins", "zwei" }));
		final String expected = "eins zwei";
		new ConcatStringsCommand(text).execute();
		final List<String> output = text.getTransformedText();
		assertEquals(1, output.size());
		assertEquals(expected, output.get(0));
	}

	@Test
	@DisplayName("mehr als ein Wort ohne einchließende Leerzeichen ergibt großzügig getrennte Wortgruppe")
	void listeMitMehrAlsEinemItemLeerzeichen() {
		final Text text = new Text(Arrays.asList(new String[] { "eins ", "  zwei " }));
		final String expected = "eins    zwei";
		new ConcatStringsCommand(text).execute();
		final List<String> output = text.getTransformedText();
		assertEquals(1, output.size());
		assertEquals(expected, output.get(0));
	}
}
