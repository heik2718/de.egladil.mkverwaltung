//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IAdminBenutzerDao;
import de.egladil.mkv.persistence.domain.user.AdminBenutzerinfo;

/**
 * AdminBenutzerDaoImplIT
 */
public class AdminBenutzerDaoImplIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(AdminBenutzerDaoImplIT.class);

	@Inject
	private IAdminBenutzerDao dao;

	@Test
	@DisplayName("find with name like klappt Treffer vorname")
	void findByNameLikeVorname() {
		// Arrange
		final String name = "Heik";

		// Act
		final List<AdminBenutzerinfo> trefferliste = dao.findWithName(name);

		// Assert
		assertTrue(trefferliste.size() >= 11);
		for (final AdminBenutzerinfo benutzerinfo : trefferliste) {
			LOG.info(benutzerinfo.toString());
		}
	}

	@Test
	@DisplayName("find with name like klappt Treffer nachname")
	void findByNameLikeNachname() {
		// Arrange
		final String name = "Tes";

		// Act
		final List<AdminBenutzerinfo> trefferliste = dao.findWithName(name);
		System.err.println("" + trefferliste.size());

		// Assert
		assertTrue(trefferliste.size() >= 31);
		for (final AdminBenutzerinfo benutzerinfo : trefferliste) {
			LOG.info(benutzerinfo.toString());
		}
	}

	@Test
	@DisplayName("getAnzahl with name like klappt Treffer nachname")
	void getAnzahlNameLikeNachname() {
		// Arrange
		final String name = "Tes";

		// Act
		final BigInteger anzahl = dao.getAnzahlWithNameLike(name);

		// Assert
		assertTrue(BigInteger.valueOf(31l).compareTo(anzahl) >= 0);
	}

	@Test
	@DisplayName("find with name null IllegalArgumentException")
	void findWithNameParameterNull() {
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			dao.findWithName(null);
		});
		assertEquals("name blank", exception.getMessage());
	}

	@Test
	@DisplayName("find with name blank IllegalArgumentException")
	void findWithNameParameterBlank() {
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			dao.findWithName(" ");
		});
		assertEquals("name blank", exception.getMessage());
	}

	@Test
	@DisplayName("getAnzahl with name null IllegalArgumentException")
	void getAnzahlWithNameParameterNull() {
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			dao.getAnzahlWithNameLike(null);
		});
		assertEquals("name blank", exception.getMessage());
	}

	@Test
	@DisplayName("getAnzahl with name blank IllegalArgumentException")
	void getAnzahlWithNameParameterBlank() {
		final Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			dao.getAnzahlWithNameLike(" ");
		});
		assertEquals("name blank", exception.getMessage());
	}
}
