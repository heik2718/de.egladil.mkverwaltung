//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * ClasspathResourceByteArrayTest
 */
public class ClasspathResourceByteArrayTest {

	@Test
	@DisplayName("constructor IllegalArgumentException wenn parameter null")
	void contructorParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ClasspathResourceByteArray(null);
		});
		assertEquals("resourcePath blank", ex.getMessage());
	}

	@Test
	@DisplayName("constructor IllegalArgumentException wenn parameter null")
	void contructorParameterBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ClasspathResourceByteArray(" ");
		});
		assertEquals("resourcePath blank", ex.getMessage());
	}

}
