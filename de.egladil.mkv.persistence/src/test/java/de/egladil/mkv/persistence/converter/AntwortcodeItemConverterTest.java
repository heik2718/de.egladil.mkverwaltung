//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;

/**
 * AntwortcodeItemConverterTest
 */
public class AntwortcodeItemConverterTest {

	@Nested
	@DisplayName("Tests zur Konvertoerung von korrekten Großbuchstaben.")
	class ConvertGrossbuchstabenTests {
		@Test
		@DisplayName("konvertieren A in PublicAntwortbuchstabe")
		void convertA() {
			// Arrange
			final String buchstabe = "A";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("A", 1);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren B in PublicAntwortbuchstabe")
		void convertB() {
			// Arrange
			final String buchstabe = "B";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("B", 2);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren C in PublicAntwortbuchstabe")
		void convertC() {
			// Arrange
			final String buchstabe = "C";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("C", 3);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren D in PublicAntwortbuchstabe")
		void convertD() {
			// Arrange
			final String buchstabe = "D";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("D", 4);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren E in PublicAntwortbuchstabe")
		void convertE() {
			// Arrange
			final String buchstabe = "E";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("E", 5);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren N in PublicAntwortbuchstabe")
		void convertN() {
			// Arrange
			final String buchstabe = "N";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("N", 0);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}
	}

	@Nested
	@DisplayName("Tests zur Konvertierung von korrekten Kleinbuchstaben und Minus.")
	class ConvertKleinbuchstabenTests {
		@Test
		@DisplayName("konvertieren a in PublicAntwortbuchstabe")
		void convertA() {
			// Arrange
			final String buchstabe = "a";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("A", 1);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren b in PublicAntwortbuchstabe")
		void convertB() {
			// Arrange
			final String buchstabe = "b";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("B", 2);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren c in PublicAntwortbuchstabe")
		void convertC() {
			// Arrange
			final String buchstabe = "c";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("C", 3);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren d in PublicAntwortbuchstabe")
		void convertD() {
			// Arrange
			final String buchstabe = "d";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("D", 4);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren e in PublicAntwortbuchstabe")
		void convertE() {
			// Arrange
			final String buchstabe = "e";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("E", 5);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren n in PublicAntwortbuchstabe")
		void convertN() {
			// Arrange
			final String buchstabe = "n";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("N", 0);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}

		@Test
		@DisplayName("konvertieren - in PublicAntwortbuchstabe")
		void convertMinus() {
			// Arrange
			final String buchstabe = "-";
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			final PublicAntwortbuchstabe expectedAntwortbuchstabe = new PublicAntwortbuchstabe("-", 0);

			// Act
			final PublicAntwortbuchstabe publicAntwortbuchstabe = converter.fromString(buchstabe);

			//
			assertEquals(expectedAntwortbuchstabe, publicAntwortbuchstabe);
		}
	}

	@Nested
	@DisplayName("Tests zur Rückkonversion")
	class ConvertFromAntworbuchstabe {

		@Test
		void parametrNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new AntwortcodeItemConverter().fromAntwortbuchstabe(null);
			});

			assertEquals("antwortbuchstabe null", ex.getMessage());
		}

		@Test
		@DisplayName("nur 0-5")
		void alleLegalenParameter() {
			final AntwortcodeItemConverter converter = new AntwortcodeItemConverter();
			for (int i = 0; i < 6; i++) {
				final PublicAntwortbuchstabe pa = PublicAntwortbuchstabe.fromCode(i);

				switch (i) {
				case 0:
					assertEquals("N", converter.fromAntwortbuchstabe(pa));
					break;
				case 1:
					assertEquals("A", converter.fromAntwortbuchstabe(pa));
					break;
				case 2:
					assertEquals("B", converter.fromAntwortbuchstabe(pa));
					break;
				case 3:
					assertEquals("C", converter.fromAntwortbuchstabe(pa));
					break;
				case 4:
					assertEquals("D", converter.fromAntwortbuchstabe(pa));
					break;
				case 5:
					assertEquals("E", converter.fromAntwortbuchstabe(pa));
					break;
				default:
					break;
				}
			}
		}
	}
}
