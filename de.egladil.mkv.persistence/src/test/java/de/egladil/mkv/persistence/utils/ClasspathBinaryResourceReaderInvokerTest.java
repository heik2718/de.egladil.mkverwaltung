//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * ClasspathBinaryResourceReaderInvokerTest
 */
public class ClasspathBinaryResourceReaderInvokerTest {

	@Test
	@DisplayName("constructor IllegalArgumentException wenn parameter null")
	void contructorParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ClasspathBinaryResourceReader(null);
		});
		assertEquals("byteArray null", ex.getMessage());
	}

}
