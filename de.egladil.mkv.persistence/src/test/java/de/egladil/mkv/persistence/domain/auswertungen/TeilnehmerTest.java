//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerTest
 */
public class TeilnehmerTest {

	private static final String JAHR = "2018";

	private static final String TEILNAHMEKUERZEL = "HGTR54D3";

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private Teilnehmer teilnehmer;

	@BeforeEach
	void setUp() {
		this.teilnehmer = new TeilnehmerBuilder(TeilnahmeIdentifier.create(Teilnahmeart.S, TEILNAHMEKUERZEL, JAHR), KLASSENSTUFE)
			.vorname("Emilia").nachname("Meier").checkAndBuild();
	}

	@Test
	@DisplayName("provide klappt")
	void provideTeilnahmeIdentifier() {
		final TeilnahmeIdentifier identifier = teilnehmer.provideTeilnahmeIdentifier();
		assertEquals(JAHR, identifier.getJahr());
		assertEquals(Teilnahmeart.S, identifier.getTeilnahmeart());
		assertEquals(TEILNAHMEKUERZEL, identifier.getKuerzel());
	}
}
