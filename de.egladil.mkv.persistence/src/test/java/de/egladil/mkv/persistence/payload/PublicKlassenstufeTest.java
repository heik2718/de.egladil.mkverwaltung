//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;

/**
 * PublicKlassenstufeTest
 */
public class PublicKlassenstufeTest {

	@Test
	@DisplayName("IllegalArgumentException wenn parameter null")
	void creatorMethodWithParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			PublicKlassenstufe.fromKlassenstufe(null);
		});
		assertEquals("klassenstufe null", ex.getMessage());

	}

	@Test
	@DisplayName("happy hour")
	void happyHour() {
		final PublicKlassenstufe result = PublicKlassenstufe.fromKlassenstufe(Klassenstufe.EINS);
		assertEquals("EINS", result.getName());
		assertEquals("Klasse 1", result.getLabel());
	}

}
