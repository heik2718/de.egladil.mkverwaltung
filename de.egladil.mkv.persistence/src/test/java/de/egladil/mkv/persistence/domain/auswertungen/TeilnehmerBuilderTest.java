//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerBuilderTest
 */
public class TeilnehmerBuilderTest {

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final String TEILNAHMEKUERZEL = "GTRF67RC";

	private static final String JAHR = "2018";

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private TeilnehmerBuilder teilnehmerBuilder;

	@BeforeEach
	void setUp() {
		this.teilnehmerBuilder = new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR),
			KLASSENSTUFE);
	}

	@Nested
	@DisplayName("Tests für constructor")
	class ConsructorTests {

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn teilnahmeart null")
		void teilnahmeartNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(TeilnahmeIdentifier.create(null, TEILNAHMEKUERZEL, JAHR), KLASSENSTUFE);
			});
			assertEquals("teilahmeart ist erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn teilnahmekuerzel null")
		void teilnahmekuerzelNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, null, JAHR), KLASSENSTUFE);
			});
			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn teilnahmekuerzel blank")
		void teilnahmekuerzelBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, " ", JAHR), KLASSENSTUFE);
			});
			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn jahr null")
		void jahrNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, null), KLASSENSTUFE);
			});
			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn jahr blank")
		void jahrBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, " "), KLASSENSTUFE);
			});
			assertEquals("teilnahmekuerzel und jahr sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn klassenstufe null")
		void klassenstufeNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR), null);
			});
			assertEquals("klassenstufe ist erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("constructor throws IllegalArgumentException wenn teilnahmeIdentifier null")
		void teilnahmeIdentifierNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				new TeilnehmerBuilder(null, KLASSENSTUFE);
			});
			assertEquals("teilnahmeIdentifier", ex.getMessage());
		}

	}

	@Nested
	@DisplayName("Tests für vorname und nachname")
	class InitVornameNachname {

		@Test
		@DisplayName("normalisierter vorname bleibt unverändert")
		void vornameNormalisiert() {
			final String vorname = "Emilia Charlotta";

			final TeilnehmerManipulator result = teilnehmerBuilder.vorname(vorname);
			assertEquals(vorname, result.getVorname());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen in vorname werden getrimt")
		void vornameTrim() {
			final String vorname = "  Emilia Charlotta  ";
			final String expected = "Emilia Charlotta";

			final TeilnehmerManipulator result = teilnehmerBuilder.vorname(vorname);
			assertEquals(expected, result.getVorname());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen in vorname werden getrimt, innere leerzeichen geschrumpft")
		void vornameTrimShrink() {
			final String vorname = "  Emilia   Charlotta  ";
			final String expected = "Emilia Charlotta";

			final TeilnehmerManipulator result = teilnehmerBuilder.vorname(vorname);
			assertEquals(expected, result.getVorname());
		}

		@Test
		@DisplayName("keine Exception wenn Vorname null")
		void vornameNull() {
			final String vorname = null;

			final TeilnehmerManipulator result = teilnehmerBuilder.vorname(vorname);
			assertNull(result.getVorname());
		}

		@Test
		@DisplayName("normalisierter nachname bleibt unverändert")
		void nachnameNormalisiert() {
			final String nachname = "Zimmermann";

			final TeilnehmerManipulator result = teilnehmerBuilder.nachname(nachname);
			assertEquals(nachname, result.getNachname());
		}

		@Test
		@DisplayName("führende und endende Leerzeichen in nachname werden getrimt")
		void nachnameTrim() {
			final String nachname = "  Zimmermann  ";
			final String expected = "Zimmermann";

			final TeilnehmerManipulator result = teilnehmerBuilder.nachname(nachname);
			assertEquals(expected, result.getNachname());
		}

		@Test
		@DisplayName("keine Exception wenn nachname null")
		void nachnameNull() {
			final String nachname = null;

			final TeilnehmerManipulator result = teilnehmerBuilder.nachname(nachname);
			assertNull(result.getNachname());
		}
	}

	@Nested
	@DisplayName("Tests für checkAndBuild")
	class CheckAndBuild {

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname und nachname null sind")
		void vornameNachnameNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.checkAndBuild();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname blank und nachname null")
		void vornameBlankNachnameNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.vorname(" ").checkAndBuild();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname null und nachname blank")
		void vornameNullNachnameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.nachname(" ").checkAndBuild();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname und nachname blank sind")
		void vornameNachnameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.vorname("").nachname(" ").checkAndBuild();
			});
			assertEquals("vorname oder nachname sind erforderlich", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn vorname zu lang, nachname blank")
		void vornameZuLangNachnameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.vorname("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilch")
					.nachname(" ").checkAndBuild();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn nachname zu lang, vorname blank")
		void nachnameZuLangVornameBlank() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.nachname("Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilch")
					.vorname(" ").checkAndBuild();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgrumentException wenn zusammengesetzter Name zu lang")
		void zusammenesetzterNameZuLang() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				teilnehmerBuilder.nachname("Universums aus denen alle ")
					.vorname("chemischenElemente aufgebaut sind Teilchen für Teilch").checkAndBuild();
			});
			assertEquals("name passt nicht in drei Zeilen", ex.getMessage());
		}

		@Test
		@DisplayName("das Kürzel wird initialisiert")
		void buildInitialisiertKuerzel() {
			final String vorname = "  Emilia   Charlotta  ";
			final String expectedVorname = "Emilia Charlotta";

			final String nachname = "  Zimmermann  ";
			final String expectedNachname = "Zimmermann";

			final Teilnehmer teilnehmer = teilnehmerBuilder.vorname(vorname).nachname(nachname).checkAndBuild();

			assertEquals(22, teilnehmer.getKuerzel().length());
			assertEquals(expectedVorname, teilnehmer.getVorname());
			assertEquals(expectedNachname, teilnehmer.getNachname());
			assertEquals(JAHR, teilnehmer.getJahr());
			assertEquals(TEILNAHMEART, teilnehmer.getTeilnahmeart());
			assertEquals(TEILNAHMEKUERZEL, teilnehmer.getTeilnahmekuerzel());
			assertEquals(KLASSENSTUFE, teilnehmer.getKlassenstufe());
			assertNull(teilnehmer.getZusatz());
		}

		@Test
		@DisplayName("das Kürzel und der Zusatz werden initialisiert")
		void buildInitialisiertKuerzelUndZusatz() {
			final String vorname = "  Emilia   Charlotta  ";
			final String expectedVorname = "Emilia Charlotta";

			final String nachname = "  Zimmermann  ";
			final String expectedNachname = "Zimmermann";

			final String zusatz = " die vom    Fenster ";
			final String expectedZusatz = "die vom Fenster";

			final Teilnehmer teilnehmer = teilnehmerBuilder.vorname(vorname).nachname(nachname).zusatz(zusatz).checkAndBuild();

			assertNotNull(teilnehmer.getKuerzel());
			assertEquals(expectedVorname, teilnehmer.getVorname());
			assertEquals(expectedNachname, teilnehmer.getNachname());
			assertEquals(expectedZusatz, teilnehmer.getZusatz());
			assertEquals(JAHR, teilnehmer.getJahr());
			assertEquals(TEILNAHMEART, teilnehmer.getTeilnahmeart());
			assertEquals(KLASSENSTUFE, teilnehmer.getKlassenstufe());
			assertEquals(TEILNAHMEKUERZEL, teilnehmer.getTeilnahmekuerzel());
		}
	}

}
