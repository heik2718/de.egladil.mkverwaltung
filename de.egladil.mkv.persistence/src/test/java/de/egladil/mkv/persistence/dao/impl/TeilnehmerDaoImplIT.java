//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerDaoImplIT
 */
public class TeilnehmerDaoImplIT extends AbstractGuiceIT {

	private static final String TEILNAHMEKUERZEL = "L0LD4AXU";

	private static final String JAHR = "2018";

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private TeilnahmeIdentifier teilnahmeIdentifier;

	@Inject
	private IAuswertungsgruppeDao auswertungsgruppeDao;

	@Inject
	private ITeilnehmerDao dao;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		teilnahmeIdentifier = Mockito.mock(TeilnahmeIdentifier.class);
		Mockito.when(teilnahmeIdentifier.getTeilnahmeart()).thenReturn(TEILNAHMEART);
		Mockito.when(teilnahmeIdentifier.getKuerzel()).thenReturn(TEILNAHMEKUERZEL);
		Mockito.when(teilnahmeIdentifier.getJahr()).thenReturn(JAHR);
	}

	@Test
	@DisplayName("teilnehmer hinzufügen")
	void addTeilnehmer() throws IOException {
		final Optional<Auswertungsgruppe> optRoot = auswertungsgruppeDao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier);
		if (optRoot.isPresent()) {
			final Auswertungsgruppe root = optRoot.get();
			if (!root.getAuswertungsgruppen().isEmpty()) {
				final Auswertungsgruppe klasse = root.getAuswertungsgruppen().get(0);

				final Teilnehmer teilnehmer = new TeilnehmerBuilder(
					TeilnahmeIdentifier.create(Teilnahmeart.S, TEILNAHMEKUERZEL, JAHR), KLASSENSTUFE)
						.nachname(UUID.randomUUID().toString().substring(0, 8))
						.vorname(UUID.randomUUID().toString().substring(0, 8)).checkAndBuild();
				klasse.addTeilnehmer(teilnehmer);
				dao.persist(teilnehmer);
			}
		}
	}

	@Test
	@DisplayName("Teilnehmer ohne auswertungsgruppe anlegen")
	void alleinstehendenTeilnehmerAnlegen() {
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.create(Teilnahmeart.P, "BPN390UP", JAHR);
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, KLASSENSTUFE)
			.nachname(UUID.randomUUID().toString().substring(0, 8)).vorname(UUID.randomUUID().toString().substring(0, 8))
			.checkAndBuild();
		final Teilnehmer persisted = dao.persist(teilnehmer);

		assertNotNull(persisted.getId());
	}

	@Test
	@DisplayName("suche mit teilnahmeIdentifier")
	void findByIdentifier() {
		// Arrange
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier("L0LD4AXU", "2018");

		// Act
		final List<Teilnehmer> trefferliste = dao.findByTeilnahmeIdentifier(teilnahmeIdentifier);

		// Assert
		assertFalse(trefferliste.isEmpty());

	}

	void findByIdentifierParamNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			dao.findByTeilnahmeIdentifier(null);
		});

		assertEquals("teilnahmeIdentifier null", ex.getMessage());
	}
}
