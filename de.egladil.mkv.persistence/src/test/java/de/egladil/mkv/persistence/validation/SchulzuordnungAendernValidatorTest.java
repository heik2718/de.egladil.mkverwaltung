//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.payload.request.SchulzuordnungAendernPayload;

/**
 * SchulzuordnungAendernValidatorTest
 */
public class SchulzuordnungAendernValidatorTest {

	private static final Logger LOG = LoggerFactory.getLogger(SchulzuordnungAendernPayload.class);

	private Validator validator;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@Test
	public void validation_fails_with_IllegalArgumentException_when_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			validator.validate(null);
		});
		assertEquals("HV000116: The object to be validated must not be null.", ex.getMessage());
	}

	@Test
	public void validation_passes_when_valid() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	public void validation_passes_when_anmelden_false() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setAnmelden(false);

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertTrue(errors.isEmpty());
	}

	@Test
	public void validation_fails_when_kuerzelAlt_null() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setKuerzelAlt(null);

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertEquals(1, errors.size());
		final ConstraintViolation<SchulzuordnungAendernPayload> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("kuerzelAlt", cv.getPropertyPath().toString());
	}

	@Test
	public void validation_fails_when_kuerzelAlt_kein_kuerzel() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setKuerzelAlt("Z&u");

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertEquals(1, errors.size());
		final ConstraintViolation<SchulzuordnungAendernPayload> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("kuerzelAlt", cv.getPropertyPath().toString());
	}

	@Test
	public void validation_fails_when_kuerzelNeu_null() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setKuerzelNeu(null);

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertEquals(1, errors.size());
		final ConstraintViolation<SchulzuordnungAendernPayload> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("kuerzelNeu", cv.getPropertyPath().toString());
	}

	@Test
	public void validation_fails_when_kuerzelNeu_kein_kuerzel() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setKuerzelNeu("Z&u");

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertEquals(1, errors.size());
		final ConstraintViolation<SchulzuordnungAendernPayload> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
		assertEquals("kuerzelNeu", cv.getPropertyPath().toString());
	}

	@Test
	public void validation_fails_when_kuerzelNeu_gleich_kuerzelAlt() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setKuerzelNeu(payload.getKuerzelAlt());

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertEquals(1, errors.size());
		final ConstraintViolation<SchulzuordnungAendernPayload> cv = errors.iterator().next();
		LOG.info(cv.getMessage());
	}

	@Test
	public void validation_fails_when_kuerzelNeu_gleich_kuerzelAlt_und_anmelden_false() {
		// Arrange
		final SchulzuordnungAendernPayload payload = getValidSchulzuordnung();
		payload.setKuerzelNeu(payload.getKuerzelAlt());
		payload.setAnmelden(false);

		// Act
		final Set<ConstraintViolation<SchulzuordnungAendernPayload>> errors = validator.validate(payload);

		// Assert
		assertEquals(1, errors.size());
		final Iterator<ConstraintViolation<SchulzuordnungAendernPayload>> iter = errors.iterator();
		while (iter.hasNext()) {
			final ConstraintViolation<SchulzuordnungAendernPayload> cv = iter.next();
			LOG.info(cv.getMessage());
		}
	}

	private SchulzuordnungAendernPayload getValidSchulzuordnung() {
		final SchulzuordnungAendernPayload payload = new SchulzuordnungAendernPayload();
		payload.setAnmelden(true);
		payload.setKuerzelAlt("ASDE5RT7");
		payload.setKuerzelNeu("LJH6545F");
		return payload;
	}

}
