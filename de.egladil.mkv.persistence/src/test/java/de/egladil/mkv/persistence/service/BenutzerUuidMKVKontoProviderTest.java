//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * BenutzerUuidMKVKontoProviderTest
 */
public class BenutzerUuidMKVKontoProviderTest {

	private static final String AKTJAHR = "2017";

	private static final String BENUTZER_UUID = "hsakhf-hafwoo412g1";

	private ILehrerkontoDao lehrerkontoDao;

	private IPrivatkontoDao privatkontoDao;

	private IdentifierMKVKontoProvider provider;

	@BeforeEach
	public void setUp() {
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		provider = new IdentifierMKVKontoProvider();
	}

	@Test
	public void findMKVBenutzer_sucht_beide_kontoarten() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt(AKTJAHR);
		privatkonto.setUuid(BENUTZER_UUID);

		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());
		Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(privatkonto));

		// Act
		final Optional<IMKVKonto> mkvKonto = provider.findMKVBenutzerByBenutzerUuid(BENUTZER_UUID, lehrerkontoDao, privatkontoDao);

		// Assert
		assertTrue(mkvKonto.isPresent());
		assertTrue(mkvKonto.get() instanceof Privatkonto);
	}

}
