//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * PrivatkontaktDaoImplIT
 */
public class PrivatkontoDaoImplIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(PrivatkontoDaoImplIT.class);

	@Inject
	private IPrivatkontoDao dao;

	// @Test
	void anlegen_mit_teilnahme_klappt() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validRandomPrivatkontakt("2017");
		final int expectedAnzahl = privatkonto.getTeilnahmen().size();

		// Act
		final Privatkonto persisted = dao.persist(privatkonto);

		// Assert
		assertNotNull(persisted.getId());
		final List<Privatteilnahme> persistedTeilnahmen = persisted.getTeilnahmen();
		assertEquals(expectedAnzahl, persistedTeilnahmen.size());
		assertNotNull(persistedTeilnahmen.get(0).getId());
	}

	@Test
	void findByUUID_klappt() {
		// Act
		final Optional<Privatkonto> opt = dao.findByUUID("bf01e395-7d12-4e53-8a7c-2d595ae7c263");

		// Assert
		assertTrue(opt.isPresent());
		final Privatkonto privatkonto = opt.get();
		final List<Privatteilnahme> teilnahmen = privatkonto.getTeilnahmen();
		assertFalse(teilnahmen.isEmpty());
		for (final Privatteilnahme t : teilnahmen) {
			LOG.info("{}", t);
		}
	}
}
