//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.cache;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;

/**
 * LandCacheTest
 */
public class LandCacheTest extends AbstractGuiceIT {

	@Inject
	private ILandDao landDao;

	@Inject
	private LandCache landCache;

	@Test
	public void refresh() {
		final String kuerzel = "de-BY";

		Land land = (landCache.getLand(kuerzel)).get();
		land.getOrte();

		System.err.println("1: " + land.getName());

		try {
			Thread.sleep(700);
		} catch (final InterruptedException e) {
			//
		}

		land = (landCache.getLand(kuerzel)).get();
		land.getOrte();

		System.err.println("2: " + land.getName());

		try {
			Thread.sleep(1500);
		} catch (final InterruptedException e) {
			//
		}

		land = (landCache.getLand(kuerzel)).get();
		land.getOrte();

		System.err.println("3: " + land.getName());

		try {
			Thread.sleep(3000);
		} catch (final InterruptedException e) {
			//
		}

		land = (landCache.getLand(kuerzel)).get();
		land.getOrte();

		System.err.println("4:" + land.getName());

	}
}
