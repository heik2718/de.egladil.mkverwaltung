//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;

/**
 * SchulteilnahmeReadOnlyDaoIT
 */
public class SchulteilnahmeReadOnlyDaoIT extends AbstractGuiceIT {

	@Inject
	private ISchulteilnahmeReadOnlyDao dao;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	@Test
	void loadKlappt() {
		final List<SchulteilnahmeReadOnly> trefferliste = dao.findAll(SchulteilnahmeReadOnly.class);

		assertFalse(trefferliste.isEmpty());
	}

	@Test
	void findKlappt() {
		// Arrange
		final String jahr = "2018";
		final String kuerzel = "ZW9Z0FIT";

		// Act
		final Optional<SchulteilnahmeReadOnly> opt = dao.findBySchulkuerzelUndJahr(kuerzel, jahr);

		// Assert#
		assertTrue(opt.isPresent());
	}
}
