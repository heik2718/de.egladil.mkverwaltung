//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * UploadInfoImplIT
 */
public class UploadInfoImplIT extends AbstractGuiceIT {

	@Inject
	private IUploadInfoDao uploadInfoDao;

	@Test
	void findValidUploadsByStatus_klappt() {
		// Act
		final List<UploadInfo> uploads = uploadInfoDao.findValidUploadsByStatus(UploadStatus.NEU);

		// Assert
		assertNotNull(uploads);

	}

	@Test
	void findByTeilnahmeIdentifierLehrer() {
		// Arrange
		final String teilnahmekuerzel = "YG9D32CD";
		final String jahr = "2017";

		// Act
		final List<UploadInfo> uploads = uploadInfoDao
			.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(teilnahmekuerzel, jahr));
		// Assert
		assertFalse(uploads.isEmpty());
	}

	@Test
	void getAnzahlLehrer() {
		// Arrange
		final String teilnahmekuerzel = "YG9D32CD";
		final String jahr = "2017";

		// Act
		final long anzahl = uploadInfoDao
			.anzahlUploadInfos(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(teilnahmekuerzel, jahr));
		// Assert
		assertEquals(66, anzahl);
	}

	@Test
	void findByTeilnahmeIdentifierPrivat() {
		// Arrange
		final String teilnahmekuerzel = "4AKICN7Y";
		final String jahr = "2017";

		// Act
		final List<UploadInfo> uploads = uploadInfoDao
			.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(teilnahmekuerzel, jahr));

		// Assert
		assertNotNull(uploads);
	}

	@Test
	void getAnzahlPrivat() {
		// Arrange
		final String teilnahmekuerzel = "4AKICN7Y";
		final String jahr = "2017";

		// Act
		final long anzahl = uploadInfoDao
			.anzahlUploadInfos(TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(teilnahmekuerzel, jahr));

		// Assert
		assertEquals(11, anzahl);
	}

	@Test
	void findByDateinameMitTreffer() {
		// Arrange
		String dateiname = "S_093O1J71_bc7a4930df994e8872061fa789200a9e08c7f67aaced80fae744103d44cb9508.ods";

		// Act
		UploadInfo uploadInfo = uploadInfoDao.findByDateiname(dateiname);

		// Assert
		assertNotNull(uploadInfo);
		assertEquals(UploadMimeType.ODS, uploadInfo.getMimetype());
		assertEquals("093O1J71", uploadInfo.getTeilnahmekuerzel());
		assertEquals("2018", uploadInfo.getJahr());
		assertEquals(Teilnahmeart.S, uploadInfo.getTeilnahmeart());
		assertEquals("DE-HE", uploadInfo.getLandkuerzel());
		assertEquals(UploadStatus.FERTIG, uploadInfo.getUploadStatus());
	}

	@Test
	void findByDateinameOhneTreffer() {
		// Arrange
		String dateiname = "S_093O1J71_bc7a1930df994e8872061fa789200a9e08c7f67aaced80fae744103d44cb9508.ods";

		// Act
		UploadInfo uploadInfo = uploadInfoDao.findByDateiname(dateiname);

		// Assert
		assertNull(uploadInfo);
	}

}
