//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * AnonymsierungsserviceTest
 */
public class AnonymsierungsserviceTest {

	private static final String UUID = "gajda-83bsk";

	private static final String CONTEXT_MSG = "Sediena Esad";

	private Anonymsierungsservice service;

	private IBenutzerService benutzerService;

	private ILehrerkontoDao lehrerkontoDao;

	private IPrivatkontoDao privatkontoDao;

	private IProtokollService protokollService;

	@BeforeEach
	public void setUp() {
		benutzerService = Mockito.mock(IBenutzerService.class);
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		protokollService = Mockito.mock(IProtokollService.class);

		service = new Anonymsierungsservice(benutzerService, lehrerkontoDao, privatkontoDao, protokollService);
	}

	@Test
	public void kontoAnonymsieren_wirft_IllegalArgumentException_wenn_benutzerUuid_null() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.kontoAnonymsieren(null, optKont, "blubb");
		});

		assertEquals("benutzerUuid", ex.getMessage());
	}

	@Test
	public void kontoAnonymsieren_wirft_IllegalArgumentException_wenn_konto_null() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.kontoAnonymsieren(UUID, null, "blubb");
		});

		assertEquals("konto", ex.getMessage());

	}

	@Test
	public void kontoAnonymsieren_wirft_IllegalArgumentException_wenn_contextMessageBlank() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			service.kontoAnonymsieren(UUID, optKont, "");
		});

		assertEquals("contextMessage", ex.getMessage());

	}

	@Test
	public void kontoAnonymisieren_ruft_persist_und_protokollieren_auf() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilDuplicateEntryException_bei_benutzer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenThrow(new EgladilDuplicateEntryException("dup"));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: Benutzer EgladilDuplicateEntryException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilStorageException_bei_benutzer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenThrow(new EgladilStorageException("dup"));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: Benutzer EgladilStorageException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilConcurrentModificationException_bei_benutzer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenThrow(new EgladilConcurrentModificationException("dup"));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: Benutzer EgladilConcurrentModificationException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_UnexpectedException_bei_benutzer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenThrow(new RuntimeException("dup"));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: Benutzer RuntimeException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilDuplicateEntryException_bei_lehrer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenThrow(new EgladilDuplicateEntryException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_LEHRER EgladilDuplicateEntryException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilStorageException_bei_lehrer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenThrow(new EgladilStorageException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_LEHRER EgladilStorageException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilConcurrentModificationException_bei_lehrer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenThrow(new EgladilConcurrentModificationException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_LEHRER EgladilConcurrentModificationException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_UnexpectedException_bei_lehrer() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenThrow(new RuntimeException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_LEHRER RuntimeException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_benutzer_null() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(null);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(0)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_benutzer_anonym() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);
		benutzer.setAnonym(true);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(0)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(1)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_lehrer_fehlt() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Optional<IMKVKonto> optKont = Optional.empty();

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(0)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_lehrer_anonym() {
		// Arrange
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(UUID, "2017");
		final Person person = lehrerkonto.getPerson();
		person.setAnonym(true);
		final Optional<IMKVKonto> optKont = Optional.of(lehrerkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(lehrerkontoDao, times(0)).persist(lehrerkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilDuplicateEntryException_bei_privat() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt("2017");
		privatkonto.setUuid(UUID);
		final Optional<IMKVKonto> optKont = Optional.of(privatkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(privatkontoDao.persist(privatkonto)).thenThrow(new EgladilDuplicateEntryException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_PRIVAT EgladilDuplicateEntryException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(privatkontoDao, times(1)).persist(privatkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilStorageException_bei_privat() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt("2017");
		privatkonto.setUuid(UUID);
		final Optional<IMKVKonto> optKont = Optional.of(privatkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(privatkontoDao.persist(privatkonto)).thenThrow(new EgladilStorageException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_PRIVAT EgladilStorageException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(privatkontoDao, times(1)).persist(privatkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_EgladilConcurrentModificationException_bei_privat() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt("2017");
		privatkonto.setUuid(UUID);
		final Optional<IMKVKonto> optKont = Optional.of(privatkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(privatkontoDao.persist(privatkonto)).thenThrow(new EgladilConcurrentModificationException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_PRIVAT EgladilConcurrentModificationException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(privatkontoDao, times(1)).persist(privatkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_UnexpectedException_bei_privat() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt("2017");
		privatkonto.setUuid(UUID);
		final Optional<IMKVKonto> optKont = Optional.of(privatkonto);

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		Mockito.when(privatkontoDao.persist(privatkonto)).thenThrow(new RuntimeException("dup"));
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), UUID,
			"Sediena Esad: MKV_PRIVAT RuntimeException");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(privatkontoDao, times(1)).persist(privatkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_privat_fehlt() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt("2017");
		privatkonto.setUuid(UUID);
		final Optional<IMKVKonto> optKont = Optional.empty();

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(privatkontoDao, times(0)).persist(privatkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void kontoAnonymisieren_privat_anonym() {
		// Arrange
		final Privatkonto privatkonto = TestUtils.validPrivatkontakt("2017");
		privatkonto.setUuid(UUID);
		privatkonto.getPerson().setAnonym(true);
		final Optional<IMKVKonto> optKont = Optional.empty();

		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(UUID);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzer);
		Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
		final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), UUID, CONTEXT_MSG);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		service.kontoAnonymsieren(UUID, optKont, CONTEXT_MSG);

		// Assert
		Mockito.verify(benutzerService, times(1)).persistBenutzerkonto(benutzer);
		Mockito.verify(privatkontoDao, times(0)).persist(privatkonto);
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}
}
