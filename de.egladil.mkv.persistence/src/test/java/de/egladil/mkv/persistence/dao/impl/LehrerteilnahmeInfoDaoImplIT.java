//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;

/**
 * LehrerteilnahmeInfoDaoImplIT. 235 MXVCSK7J
 */
public class LehrerteilnahmeInfoDaoImplIT extends AbstractGuiceIT {

	@Inject
	private ILehrerteilnahmeInfoDao dao;

	@Test
	public void findAlleMitBenutzerId_klappt() {
		// Arrange
		// final long benutzerId = 235;
		final long benutzerId = 142;

		// Act
		final List<LehrerteilnahmeInformation> trefferliste = dao.findAlleMitBenutzerId(benutzerId);

		// Assert
		assertTrue(trefferliste.size() > 2);
		final Set<String> schulkuerzel = new HashSet<>();
		for (final LehrerteilnahmeInformation info : trefferliste) {
			schulkuerzel.add(info.getSchulkuerzel());
		}

		for (final String k : schulkuerzel) {
			System.out.println(k);
		}
	}

	@Test
	public void findAlleMitSchulkuerzel_klappt() {
		// Arrange
		final String schulkuerzel = "MXVCSK7J";

		// Act
		final List<LehrerteilnahmeInformation> trefferliste = dao.findAlleMitSchulkuerzel(schulkuerzel);

		// Assert
		assertTrue(trefferliste.size() > 2);
		final Set<Long> benutzerIds = new HashSet<>();
		for (final LehrerteilnahmeInformation info : trefferliste) {
			benutzerIds.add(info.getBenutzerId());
		}
		for (final Long l : benutzerIds) {
			System.out.println(l.toString());
		}

	}

	@Test
	public void findAlleMitBenutzerIdOderSchulkuerzel_klappt() {
		// Arrange
		final long benutzerId = 235;
		final String schulkuerzel = "MXVCSK7J";
		final List<LehrerteilnahmeInformation> lehrertreffer = dao.findAlleMitBenutzerId(benutzerId);
		final List<LehrerteilnahmeInformation> schultreffer = dao.findAlleMitSchulkuerzel(schulkuerzel);

		final Set<LehrerteilnahmeInformation> union = new HashSet<>();
		union.addAll(lehrertreffer);
		union.addAll(schultreffer);

		// Act
		final List<LehrerteilnahmeInformation> trefferliste = dao.findAlleMitBenutzerIdOderSchulkuerzel(benutzerId, schulkuerzel);

		// Assert
		for (final LehrerteilnahmeInformation info : trefferliste) {
			assertTrue("Fehler bei " + info.toString(), union.contains(info));
		}

		for (final LehrerteilnahmeInformation info : union) {
			assertTrue("Fehler bei " + info.toString(), trefferliste.contains(info));
		}

		System.out.println(
			"#union=" + union.size() + ", #lehrertreffer=" + lehrertreffer.size() + ", #schultreffer=" + schultreffer.size());
	}

	@Test
	public void findAlleMitJahrUndSchulkuerzel_klappt() {
		// Arrange
		final String jahr = "2008";
		final String schulkuerzel = "MXVCSK7J";

		// Act
		final List<LehrerteilnahmeInformation> trefferliste = dao.findAlleMitJahrUndSchulkuerzel(jahr, schulkuerzel);

		// Assert
		assertTrue(trefferliste.size() > 0);

		for (final LehrerteilnahmeInformation info : trefferliste) {
			System.out.println(info.toString());
		}

	}

}
