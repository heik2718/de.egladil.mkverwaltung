//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;

/**
 * SchulteilnahmeDaoImplIT
 */
public class SchulteilnahmeDaoImplIT extends AbstractGuiceIT {

	@Inject
	private ISchulteilnahmeDao dao;

	@Test
	void findOrphanKlappt() {
		// Arrange
		final String schulkuerzel = "5DNKY6AU";
		final String jahr = "2017";

		// Act
		Optional<Schulteilnahme> opt = dao.findOrphan(schulkuerzel, jahr);

		// Assert
		if (opt.isPresent()) {
			final Schulteilnahme t = opt.get();
			System.out.println(t);
			dao.delete(t);

			opt = dao.findOrphan(schulkuerzel, jahr);
			assertFalse(opt.isPresent());
		}
	}

	@Test
	@DisplayName("Schulteilnahmen existieren")
	void findBySchulkuerzelKlappt() {
		final String schulkuerzel = "YG9D32CD";
		final List<Schulteilnahme> teilnahmen = dao.findAllBySchulkuerzel(schulkuerzel);

		assertFalse(teilnahmen.isEmpty());

	}
}
