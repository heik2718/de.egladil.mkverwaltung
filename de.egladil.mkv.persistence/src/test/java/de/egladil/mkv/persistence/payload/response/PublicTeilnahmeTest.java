//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;

/**
* PublicTeilnahmeTest
*/
public class PublicTeilnahmeTest {

	@Test
	@DisplayName("teilnahme in json")
	void serialize() throws Exception {
		final PublicTeilnahme pt = PublicTeilnahme.createFromTeilnahmeIdentifier(TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("ABCDEFGH", "2019"));
		System.out.println(new ObjectMapper().writeValueAsString(pt));
	}

}
