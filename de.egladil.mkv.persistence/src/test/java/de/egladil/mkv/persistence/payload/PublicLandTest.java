//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.payload.response.PublicOrt;

/**
 * PublicLandTest
 */
public class PublicLandTest {

	@Test
	public void create_throws_when_parameter_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			PublicLand.createMitOrten(null);
		});
		assertEquals("land darf nicht null sein", ex.getMessage());

	}

	@Test
	public void create_keine_exception_when_orte_null() {
		// Arrange
		final Land land = new Land();
		land.setName("Gigerad");
		land.setKuerzel("GI");
		land.setOrte(null);

		// Act
		final PublicLand entity = PublicLand.createMitOrten(land);

		// Assert
		assertTrue(entity.getKinder().isEmpty());
	}

	@Test
	public void create_mit_orten_klappt() {
		// Arrange
		final Land land = new Land();
		final String bezeichnung = "Gigerad";
		final String landkuerzel = "GI";

		final String name = "Haligau";
		final String ortkuerzel = "J56FGTRE";

		land.setName(bezeichnung);
		land.setKuerzel(landkuerzel);

		final Ort ort = new Ort();
		ort.setName(name);
		ort.setKuerzel(ortkuerzel);

		land.addOrt(ort);

		// Act
		final PublicLand entity = PublicLand.createMitOrten(land);

		// Assert
		assertEquals(bezeichnung, entity.getName());
		assertEquals(landkuerzel, entity.getKuerzel());
		assertEquals(1, entity.getKinder().size());

		final PublicOrt po = entity.getKinder().get(0);
		assertEquals(name, po.getName());
		assertEquals(ortkuerzel, po.getKuerzel());
		assertTrue(po.getKinder().isEmpty());
	}

}
