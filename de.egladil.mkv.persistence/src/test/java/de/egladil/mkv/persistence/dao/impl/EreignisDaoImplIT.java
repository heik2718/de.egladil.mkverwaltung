//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;

/**
 * EreignisDaoImplIT
 */
public class EreignisDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IEreignisDao ereignisDao;

	@Test
	@DisplayName("happy hour für find by ereignisart")
	void findByEreinisartHappy() {
		final List<Ereignis> ereignisse = ereignisDao.findByEreignisart(Ereignisart.LOGIN.getKuerzel());

		assertFalse(ereignisse.isEmpty());

	}

	@Test
	@DisplayName("IllegalArgumentException wenn ereignisart null")
	void findByEreignisartParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			ereignisDao.findByEreignisart(null);
		});
		assertEquals("ereignisart null", ex.getMessage());
	}
}
