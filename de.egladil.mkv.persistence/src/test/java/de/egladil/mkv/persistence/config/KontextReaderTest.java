//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.mkv.persistence.payload.response.Kontext;

/**
 * KontextReaderTest
 */
public class KontextReaderTest {

	@BeforeEach
	public void setUp() {
		KontextReader.destroy();
	}

	@Test
	public void getKontextKlappt() throws Exception {
		// Arrange
		final String validPath = "/home/heike/git/konfigurationen/mkverwaltung/configAll";

		// Act
		final Kontext conf = KontextReader.getInstance().init(validPath).getKontext();

		// Assert
		assertNotNull(conf.getDatumFreigabeLehrer());
		assertTrue(conf.getGueltigkeitEinmalpasswort() > 0);
		assertNotNull(conf.getDatumFreigabePrivat());
		assertNotNull(conf.getStartAnmeldungText());
		assertNull(conf.getAuthToken());
		assertNull(conf.getXsrfToken());

		final String json = new ObjectMapper().writeValueAsString(conf);
		System.out.println(json);
	}

	@Test
	public void initInvalidPath() throws Exception {
		// Arrange
		final String invalidPath = "/home/heike/git/konfigurationen/mkverwaltung/configAll/mkvapiconf1";

		// Act
		try {
			KontextReader.getInstance().init(invalidPath);
			fail("keine EgladilConfigurationException");
		} catch (final EgladilConfigurationException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void getKontextIllegalState() throws Exception {
		// Act
		try {
			KontextReader.getInstance().getKontext();
			fail("keine IllegalStateException");
		} catch (final IllegalStateException e) {
			System.out.println(e.getMessage());
		}
	}

}
