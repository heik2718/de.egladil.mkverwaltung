//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * BenutzerPrivatMappingStrategyTest
 */
public class BenutzerPrivatMappingStrategyTest {

	private static final String EMAIL = "privatmensch@egladil.de";

	private static final String AKTJAHR = "2017";

	private ObjectMapper objectMapper;

	private IBenutzerMappingStrategy strategy;

	@Before
	public void setUp() {
		TestUtils.initMKVApiKontextReader("2017", true);
		strategy = new BenutzerPrivatMappingStrategy();
		objectMapper = new ObjectMapper();
	}

	@Test
	public void createPublicMKVUser_klappt() throws JsonProcessingException {
		// Arrange

		final String benutzerUUID = UUID.randomUUID().toString();

		final Privatkonto privatkonto = TestUtils.validPrivatkontakt(AKTJAHR);
		privatkonto.setUuid(benutzerUUID);
		privatkonto.addTeilnahme(new Privatteilnahme("2015", new KuerzelGenerator().generateDefaultKuerzel()));
		privatkonto.addTeilnahme(new Privatteilnahme("2017", new KuerzelGenerator().generateDefaultKuerzel()));
		privatkonto.setAutomatischBenachrichtigen(true);

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(EMAIL);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(benutzerkonto.getEmail());
		benutzerkonto.setUuid(benutzerUUID);
		benutzerkonto.addRolle(new Rolle(Role.MKV_PRIVAT));

		// Act
		final MKVBenutzer mkvUser = strategy.createMKVBenutzer(benutzerkonto, privatkonto);

		final String json = objectMapper.writeValueAsString(mkvUser);
		System.out.println(json);

		// Assert
		assertNotNull(mkvUser);
		final PersonBasisdaten basisdaten = mkvUser.getBasisdaten();
		assertNotNull(basisdaten);

		assertEquals("MKV_PRIVAT", basisdaten.getRolle());
		assertEquals("Olle, IT", basisdaten.getVorname());
		assertEquals("Bolle", basisdaten.getNachname());
		assertEquals(benutzerkonto.getEmail(), basisdaten.getEmail());

		final ErweiterteKontodaten erweiterteDaten = mkvUser.getErweiterteKontodaten();
		assertNotNull(erweiterteDaten);
		assertEquals(2, erweiterteDaten.getAnzahlTeilnahmen());
		assertNotNull(erweiterteDaten.getAktuelleTeilnahme());
		assertNull(erweiterteDaten.getSchule());
		assertTrue(erweiterteDaten.isMailbenachrichtigung());
		assertFalse(erweiterteDaten.isAdvVereinbarungVorhanden());
	}
}
