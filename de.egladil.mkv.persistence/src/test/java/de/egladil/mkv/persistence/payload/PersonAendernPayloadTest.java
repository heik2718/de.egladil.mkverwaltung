//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.ValidationUtils;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;

/**
 * @author heikew
 *
 */
public class PersonAendernPayloadTest {

	private static final Logger LOG = LoggerFactory.getLogger(PersonAendernPayloadTest.class);

	private Validator validator;

	private PersonAendernPayload payload;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		payload = TestUtils.validPersonAendernPayload();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_fails_when_benutzername_doesnot_match_pattern() {
		// Arrange
		payload.setUsername("hallo-hallo@hallo.@hallo");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("username"));
	}

	@Test
	public void validate_fails_when_benutzername_null() {
		// Arrange
		payload.setUsername(null);

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("username"));
	}

	@Test
	public void validate_fails_when_benutzername_not_an_email() {
		// Arrange
		payload.setUsername("http://www.evil-link.com");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Set<String> invalidProperties = new HashSet<>();
		final Iterator<ConstraintViolation<PersonAendernPayload>> iter = errors.iterator();

		while (iter.hasNext()) {
			final ConstraintViolation<PersonAendernPayload> cv = iter.next();
			invalidProperties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, invalidProperties.size());
		assertEquals("username", invalidProperties.iterator().next());
	}

	@Test
	public void validate_fails_when_vorname_doesnot_match_pattern() {
		// Arrange
		payload.setVorname("Hallиhallo");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("vorname"));
	}

	@Test
	public void validate_fails_when_vorname_blank() {
		// Arrange
		payload.setVorname(" ");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("vorname"));
	}

	@Test
	public void validate_fails_when_vorname_zu_lang() {
		// Arrange
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 101; i++) {
			sb.append("M");
		}
		final String name = sb.toString();
		assertEquals("Fehler im Testsetting: brauchen String der Länge 101", 101, name.length());
		payload.setVorname(name);

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("vorname"));
	}

	@Test
	public void validate_fails_when_nachname_doesnot_match_pattern() {
		// Arrange
		payload.setNachname("Hallиhallo");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("nachname"));
	}

	@Test
	public void validate_fails_when_nachname_blank() {
		// Arrange
		payload.setNachname(" ");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("nachname"));
	}

	@Test
	public void validate_fails_when_nachname_zu_lang() {
		// Arrange
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 101; i++) {
			sb.append("M");
		}
		final String name = sb.toString();
		assertEquals("Fehler im Testsetting: brauchen String der Länge 101", 101, name.length());
		payload.setNachname(name);

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("nachname"));
	}

	@Test
	public void validate_fails_when_rolle_blank() {
		// Arrange
		payload.setRolle(" ");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("rolle"));
	}

	@Test
	public void validate_fails_when_rolle_unknown() {
		// Arrange
		payload.setRolle("X");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("rolle"));
	}

	@Test
	public void validate_fails_when_rolle_not_allowed() {
		// Arrange
		payload.setRolle("MKV_ADMIN");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("rolle"));
	}

	@Test
	public void validate_fails_when_kleber_blank() {
		// Arrange
		payload.setKleber(" ");

		// Act + Assert
		final Set<ConstraintViolation<PersonAendernPayload>> errors = validator.validate(payload);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("kleber"));
	}
}
