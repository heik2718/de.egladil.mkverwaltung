//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;

/**
 * LehrerkontoDaoImplIT
 */
public class LehrerkontoDaoImplIT extends AbstractGuiceIT {

	@Inject
	private ISchuleDao schuleDao;

	@Inject
	private ISchulteilnahmeDao schultenahmeDao;

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

//	@Test
	public void anlegen_mit_teilnahme_klappt() {
		// Arrange
		final Optional<Schule> opt1 = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, "WMDFNOKJ");
		assertTrue("Test kann nichht beginnen. Haben keine Schule 1", opt1.isPresent());

		final Schule schule1 = opt1.get();

		final String benutzerUuid = UUID.randomUUID().toString();

		final Person person = new Person();
		person.setVorname("Mit, Lehrerkonto");
		person.setNachname("Builder " + System.currentTimeMillis());

		final String jahr = "2010";
		final Optional<Schulteilnahme> optSchulteilnahme = schultenahmeDao
			.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(schule1.getKuerzel(), jahr));
		Schulteilnahme persistenteSchulteilnahme = null;
		if (!optSchulteilnahme.isPresent()) {
			final Schulteilnahme schulteilnahme = new Schulteilnahme();
			schulteilnahme.setJahr(jahr);
			schulteilnahme.setKuerzel(schule1.getKuerzel());
			persistenteSchulteilnahme = schultenahmeDao.persist(schulteilnahme);
		} else {
			persistenteSchulteilnahme = optSchulteilnahme.get();
		}

		final Lehrerkonto lehrerkonto = new Lehrerkonto(benutzerUuid, person, schule1, false);
		// Lehrerteilnahme lehrerteilnahme = new Lehrerteilnahme(persistenteSchulteilnahme);
		// lehrerkonto.addTeilnahme(lehrerteilnahme);
		lehrerkonto.addSchulteilnahme(persistenteSchulteilnahme);

		// Act
		final Lehrerkonto result = lehrerkontoDao.persist(lehrerkonto);

		// Assert
		assertNotNull(result.getId());
	}

	@Test
	void findByKuerzel_klappt() {
		// Arrange
		final String uuid = "eb5945dd-0ba7-4b45-9c0e-354d8338d393";

		// Act
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(uuid);

		// Assert
		assertTrue(opt.isPresent());
	}

	@Test
	void persist_klappt() {
		// Arrange
		final String uuid = "eb5945dd-0ba7-4b45-9c0e-354d8338d393";
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(uuid);
		assertTrue("Test kann nicht starten: kein passendes Lehrerkonto", opt.isPresent());

		// Act
		final Lehrerkonto lehrerkonto = opt.get();
		lehrerkonto.getPerson().setNachname("Lüdenscheidt");
		final Lehrerkonto result = lehrerkontoDao.persist(lehrerkonto);

		// Assert
		assertNotNull(result);
	}

	@Test
	public void findByPerson_treffer() {
		// Arrange
		final Person person = new Person("Horstl", "Borstl");

		// Act
		final List<Lehrerkonto> trefferliste = lehrerkontoDao.findByPerson(person);

		// Assert
		assertFalse(trefferliste.isEmpty());
		// assertEquals("PFFI12UI", trefferliste.get(0).getKuerzel());
	}
}
