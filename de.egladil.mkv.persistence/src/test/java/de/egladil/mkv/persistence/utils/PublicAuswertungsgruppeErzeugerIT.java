//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicAuswertungsgruppe;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * PublicAuswertungsgruppeErzeugerIT
 */
public class PublicAuswertungsgruppeErzeugerIT extends AbstractGuiceIT {

	@Inject
	private TeilnehmerFacade teilnehmerFacade;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	@Test
	@DisplayName("Auswertungsgruppe aus der Datenbank, lazy = true")
	void rekursionMitEchterHierarchieLazy() throws Exception {

		final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
		Mockito.when(tiProvider.provideTeilnahmeIdentifier())
			.thenReturn(TeilnahmeIdentifier.createSchulteilnahmeIdentifier("M94P3IH9", "2018"));
		final Optional<Auswertungsgruppe> opt = teilnehmerFacade.getRootAuswertungsruppe(tiProvider);
		if (opt.isPresent()) {
			final Auswertungsgruppe auswertungsgruppe = opt.get();
			final PublicAuswertungsgruppe resultRoot = new PublicAuswertungsgruppeErzeuger().execute(auswertungsgruppe, true)
				.getResult();
			assertNotNull(resultRoot);
			System.out.println(new ObjectMapper().writeValueAsString(resultRoot));
			assertFalse(resultRoot.getAuswertungsgruppen().isEmpty());
			assertTrue(resultRoot.getTeilnehmer().isEmpty());
			assertNull(resultRoot.getKlassenstufe());
			assertNull(resultRoot.getParent());
			assertEquals("Grundschule in den Kappesgärten", resultRoot.getName());
			assertTrue(resultRoot.isLazy());

			final PublicAuswertungsgruppe child = resultRoot.getAuswertungsgruppen().get(0);
			assertTrue(child.getAuswertungsgruppen().isEmpty());
			assertTrue(child.getTeilnehmer().isEmpty());
			assertNotNull(child.getKlassenstufe());
			assertEquals(resultRoot, child.getParent());
			assertTrue(child.isLazy());
		} else {
			fail("Testdaten anpassen: keine passende Auswertungsgruppe gefunden");
		}
	}

	@Test
	@DisplayName("Kindgruppe aus der Datenbank, lazy = true")
	void kindgruppeLazy() throws Exception {

		final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
		Mockito.when(tiProvider.provideTeilnahmeIdentifier())
			.thenReturn(TeilnahmeIdentifier.createSchulteilnahmeIdentifier("M94P3IH9", "2018"));
		final String kuerzel = "SEQ75RGG20180121073802";
		final Optional<Auswertungsgruppe> opt = teilnehmerFacade.getAuswertungsgruppe(kuerzel);
		if (opt.isPresent()) {
			final Auswertungsgruppe auswertungsgruppe = opt.get();
			final PublicAuswertungsgruppe result = new PublicAuswertungsgruppeErzeuger().execute(auswertungsgruppe, true)
				.getResult();
			assertNotNull(result);
			System.out.println(new ObjectMapper().writeValueAsString(result));
			assertEquals(0, result.getAuswertungsgruppen().size());
			assertTrue(result.getTeilnehmer().isEmpty());
			final PublicKlassenstufe pks = result.getKlassenstufe();
			assertEquals(Klassenstufe.EINS.toString(), pks.getName());
			assertEquals(Klassenstufe.EINS.getLabel(), pks.getLabel());
			assertNotNull(result.getParent());
			assertEquals("Klasse 1a", result.getName());
			assertTrue(result.isLazy());
		} else {
			fail("Testdaten anpassen: keine passende Auswertungsgruppe gefunden");
		}
	}

	@Test
	@DisplayName("Auswertungsgruppe aus der Datenbank, lazy = false")
	void rekursionMitEchterHierarchieEager() throws Exception {

		final TeilnahmeIdentifierProvider tiProvider = Mockito.mock(TeilnahmeIdentifierProvider.class);
		Mockito.when(tiProvider.provideTeilnahmeIdentifier())
			.thenReturn(TeilnahmeIdentifier.createSchulteilnahmeIdentifier("M94P3IH9", "2018"));
		final Optional<Auswertungsgruppe> opt = teilnehmerFacade.getRootAuswertungsruppe(tiProvider);
		if (opt.isPresent()) {
			final Auswertungsgruppe auswertungsgruppe = opt.get();
			final PublicAuswertungsgruppe resultRoot = new PublicAuswertungsgruppeErzeuger().execute(auswertungsgruppe, false)
				.getResult();
			assertNotNull(resultRoot);
			System.out.println(new ObjectMapper().writeValueAsString(resultRoot));
			assertEquals(3, resultRoot.getAuswertungsgruppen().size());
			assertTrue(resultRoot.getTeilnehmer().isEmpty());
			assertNull(resultRoot.getKlassenstufe());
			assertNull(resultRoot.getParent());
			assertEquals("Grundschule in den Kappesgärten", resultRoot.getName());
			assertFalse(resultRoot.isLazy());

			final PublicAuswertungsgruppe child = resultRoot.getAuswertungsgruppen().get(0);
			assertTrue(child.getAuswertungsgruppen().isEmpty());
			assertTrue(child.getTeilnehmer().isEmpty());
			assertNotNull(child.getKlassenstufe());
			assertEquals(resultRoot, child.getParent());
			assertFalse(child.isLazy());
		} else {
			fail("Testdaten anpassen: keine passende Auswertungsgruppe gefunden");
		}
	}

	@Test
	@DisplayName("IllegalStateException wenn vergessen execute aufzurufen")
	void illegalState() {
		final Throwable ex = assertThrows(IllegalStateException.class, () -> {
			new PublicAuswertungsgruppeErzeuger().getResult();
		});
		assertEquals("zuerst execute aufrufen, damit das Ergebnis erzeugt wird", ex.getMessage());

	}

	@Test
	@DisplayName("IllegalArgumentException wenn parameter null")
	void executeParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new PublicAuswertungsgruppeErzeuger().execute(null, false);
		});
		assertEquals("auswertungsgruppe null", ex.getMessage());

	}
}
