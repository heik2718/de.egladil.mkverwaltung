//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * PublicTeilnehmerTest
 */
public class PublicTeilnehmerTest {

	@Test
	void createFromTeilnehmerKlappt() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("AAAAAAAA", "2018"),
			Klassenstufe.ZWEI).vorname("Hans").nachname("Wurst").nummer(1).checkAndBuild();
		teilnehmer.setAntwortcode("EACDDCDNDBANCBA");
		teilnehmer.setLoesungszettelkuerzel("EH7T2PCN20171217075222");
		teilnehmer.setKaengurusprung(Integer.valueOf(7));
		teilnehmer.setPunkte("49,75");

		final PublicTeilnehmer pt = PublicTeilnehmer.fromTeilnehmer(teilnehmer);

		assertEquals("Hans", pt.getVorname());
		assertEquals("Wurst", pt.getNachname());

		final PublicKlassenstufe pk = PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI);

		assertEquals(pk, pt.getKlassenstufe());
		assertEquals(Integer.valueOf(7), pt.getKaengurusprung());
		assertEquals("49,75", pt.getPunkte());

		final PublicAntwortbuchstabe[] antwortbuchstaben = pt.getAntworten();

		assertEquals("E", antwortbuchstaben[0].getBuchstabe());
		assertEquals(5, antwortbuchstaben[0].getCode());

		assertEquals("A", antwortbuchstaben[1].getBuchstabe());
		assertEquals(1, antwortbuchstaben[1].getCode());

		assertEquals("C", antwortbuchstaben[2].getBuchstabe());
		assertEquals(3, antwortbuchstaben[2].getCode());

		assertEquals("D", antwortbuchstaben[3].getBuchstabe());
		assertEquals(4, antwortbuchstaben[3].getCode());

		assertEquals("D", antwortbuchstaben[4].getBuchstabe());
		assertEquals(4, antwortbuchstaben[4].getCode());

		assertEquals("C", antwortbuchstaben[5].getBuchstabe());
		assertEquals(3, antwortbuchstaben[5].getCode());

		assertEquals("D", antwortbuchstaben[6].getBuchstabe());
		assertEquals(4, antwortbuchstaben[6].getCode());

		assertEquals("N", antwortbuchstaben[7].getBuchstabe());
		assertEquals(0, antwortbuchstaben[7].getCode());

		assertEquals("D", antwortbuchstaben[8].getBuchstabe());
		assertEquals(4, antwortbuchstaben[8].getCode());

		assertEquals("B", antwortbuchstaben[9].getBuchstabe());
		assertEquals(2, antwortbuchstaben[9].getCode());

		assertEquals("A", antwortbuchstaben[10].getBuchstabe());
		assertEquals(1, antwortbuchstaben[10].getCode());

		assertEquals("N", antwortbuchstaben[11].getBuchstabe());
		assertEquals(0, antwortbuchstaben[11].getCode());

		assertEquals("C", antwortbuchstaben[12].getBuchstabe());
		assertEquals(3, antwortbuchstaben[12].getCode());

		assertEquals("B", antwortbuchstaben[13].getBuchstabe());
		assertEquals(2, antwortbuchstaben[13].getCode());

		assertEquals("A", antwortbuchstaben[14].getBuchstabe());
		assertEquals(1, antwortbuchstaben[14].getCode());
	}

	@Test
	void createFromTeilnehmerMitMinusKlappt() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("AAAAAAAA", "2018"),
			Klassenstufe.ZWEI).vorname("Hans").nachname("Wurst").nummer(1).checkAndBuild();
		teilnehmer.setAntwortcode("--CDDCDNDBANCBA");
		teilnehmer.setLoesungszettelkuerzel("EH7T2PCN20171217075222");
		teilnehmer.setKaengurusprung(Integer.valueOf(7));
		teilnehmer.setPunkte("49,75");

		final PublicTeilnehmer pt = PublicTeilnehmer.fromTeilnehmer(teilnehmer);

		assertEquals("Hans", pt.getVorname());
		assertEquals("Wurst", pt.getNachname());

		final PublicKlassenstufe pk = PublicKlassenstufe.fromKlassenstufe(Klassenstufe.ZWEI);

		assertEquals(pk, pt.getKlassenstufe());
		assertEquals(Integer.valueOf(7), pt.getKaengurusprung());
		assertEquals("49,75", pt.getPunkte());

		final PublicAntwortbuchstabe[] antwortbuchstaben = pt.getAntworten();

		assertEquals("-", antwortbuchstaben[0].getBuchstabe());
		assertEquals(0, antwortbuchstaben[0].getCode());

		assertEquals("-", antwortbuchstaben[1].getBuchstabe());
		assertEquals(0, antwortbuchstaben[1].getCode());

		assertEquals("C", antwortbuchstaben[2].getBuchstabe());
		assertEquals(3, antwortbuchstaben[2].getCode());

		assertEquals("D", antwortbuchstaben[3].getBuchstabe());
		assertEquals(4, antwortbuchstaben[3].getCode());

		assertEquals("D", antwortbuchstaben[4].getBuchstabe());
		assertEquals(4, antwortbuchstaben[4].getCode());

		assertEquals("C", antwortbuchstaben[5].getBuchstabe());
		assertEquals(3, antwortbuchstaben[5].getCode());

		assertEquals("D", antwortbuchstaben[6].getBuchstabe());
		assertEquals(4, antwortbuchstaben[6].getCode());

		assertEquals("N", antwortbuchstaben[7].getBuchstabe());
		assertEquals(0, antwortbuchstaben[7].getCode());

		assertEquals("D", antwortbuchstaben[8].getBuchstabe());
		assertEquals(4, antwortbuchstaben[8].getCode());

		assertEquals("B", antwortbuchstaben[9].getBuchstabe());
		assertEquals(2, antwortbuchstaben[9].getCode());

		assertEquals("A", antwortbuchstaben[10].getBuchstabe());
		assertEquals(1, antwortbuchstaben[10].getCode());

		assertEquals("N", antwortbuchstaben[11].getBuchstabe());
		assertEquals(0, antwortbuchstaben[11].getCode());

		assertEquals("C", antwortbuchstaben[12].getBuchstabe());
		assertEquals(3, antwortbuchstaben[12].getCode());

		assertEquals("B", antwortbuchstaben[13].getBuchstabe());
		assertEquals(2, antwortbuchstaben[13].getCode());

		assertEquals("A", antwortbuchstaben[14].getBuchstabe());
		assertEquals(1, antwortbuchstaben[14].getCode());
	}
}
