//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.TestUtils;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * BenutzerPayloadMapperIT
 */
public class BenutzerPayloadMapperIT extends AbstractGuiceIT {

	private static final String LEHRER_UUID = "fd77d23c-198d-4554-877d-91931c43a309";

	private static final String PRIVAT_UUID = "bf01e395-7d12-4e53-8a7c-2d595ae7c263";

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

	@Inject
	private IPrivatkontoDao privatkontoDao;

	@Inject
	private ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	@Inject
	private IBenutzerService benutzerService;

	@Inject
	private TeilnehmerFacade teilnehmerFacade;

	@Inject
	private AdvFacade advFacade;

	@Inject
	private IKatalogService katalogService;

	@Inject
	private TeilnahmenFacade teilnahmenFacade;


	private BenutzerPayloadMapper mapper;

	private AccessToken accessToken;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		accessToken = new AccessToken("sabu");
		accessToken.setCsrfToken("guidgciuaguig");

		TestUtils.initMKVApiKontextReader("2019", true);

		mapper = new BenutzerPayloadMapper(benutzerService, lehrerkontoDao, privatkontoDao, lehrerteilnahmeInfoDao,
			teilnehmerFacade, advFacade, katalogService, teilnahmenFacade);
		// mapper.setDownloadPath(TestUtils.getDownloadPath());

	}

	@Test
	void createLehrer() {
		// Arrange
		final Benutzerkonto benutzer = benutzerService.findBenutzerkontoByUUID(LEHRER_UUID);

		// Act
		final MKVBenutzer mkvBenutzer = mapper.createBenutzer(benutzer, false);

		// Assert
		ErweiterteKontodaten erweiterteKontodaten = mkvBenutzer.getErweiterteKontodaten();
		assertTrue(erweiterteKontodaten.getAnzahlTeilnahmen() >= 8);
		assertTrue(erweiterteKontodaten.getAktuelleTeilnahme().isAktuelle());
		assertNotNull(erweiterteKontodaten.getSchule());
		assertNull(mkvBenutzer.getGeschuetzteKontodaten());

		// Act 2


	}

	@Test
	void createPrivat() {
		// Arrange
		final Benutzerkonto benutzer = benutzerService.findBenutzerkontoByUUID(PRIVAT_UUID);

		// Act
		final MKVBenutzer mkvBenutzer = mapper.createBenutzer(benutzer, false);

		// Assert
		assertTrue(mkvBenutzer.getErweiterteKontodaten().getAnzahlTeilnahmen() > 0);
	}
}
