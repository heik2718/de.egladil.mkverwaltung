//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;

/**
 * UploadDaoImplIT
 */
public class UploadDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IUploadDao uploadDao;

	@Test
	void findByTeilnahmeIdentifierAndChecksummeKlappt() {

		// Arrange
		TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier("EEF8FOYK", "2019");
		String checksumme = "a557cabc6c55a72a445de6f813c8aaba27aadd5b8b7139264037512f226f5505";

		// Act
		Optional<Upload> optUpload = uploadDao.findUpload(teilnahmeIdentifier, checksumme);

		// Assert
		assertTrue(optUpload.isPresent());

	}

}
