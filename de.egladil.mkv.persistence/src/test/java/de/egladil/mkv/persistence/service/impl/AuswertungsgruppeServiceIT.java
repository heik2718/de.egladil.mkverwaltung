//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;

/**
 * AuswertungsgruppeServiceIT
 */
public class AuswertungsgruppeServiceIT extends AbstractGuiceIT {

	private static final String TEILNAHMEKUERZEL = "9PDDS51Q";

	private static final String JAHR = "2018";

	private static final String BENUTZER_UUID = "fd77d23c-198d-4554-877d-91931c43a309";



	@Inject
	private AuswertungsgruppenService service;

	@Test
	void findRootKlappt() {
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(TEILNAHMEKUERZEL, JAHR);

		final Optional<Auswertungsgruppe> optRoot = service.getRootAuswertungsgruppe(teilnahmeIdentifier);

		assertNotNull(optRoot);

		if (optRoot.isPresent()) {
			final Auswertungsgruppe root = optRoot.get();
			assertEquals("Anne Frank-Grundschule", root.getName());
		}
	}


	@Test
	void createRootKlappt() {
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier(TEILNAHMEKUERZEL, JAHR);

		final Auswertungsgruppe root = service.findOrCreateRootAuswertungsgruppe(teilnahmeIdentifier);

		assertEquals("Anne Frank-Grundschule", root.getName());
	}
}
