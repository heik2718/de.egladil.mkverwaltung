//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.kataloge.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.mkv.persistence.cache.LandCache;
import de.egladil.mkv.persistence.cache.LandCacheLoader;
import de.egladil.mkv.persistence.cache.OrtCache;
import de.egladil.mkv.persistence.cache.OrtCacheLoader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;

/**
 * KatalogDelegateTest
 */
public class KatalogServiceTest {

	private static final String LANDKUERZEL_FEHLENDES = "EDCBA";

	private static final String LANDKUERZEL_VORHANDENES = "ABCDE";

	private static final String ORTKUERZEL_FEHLENDES = "HZTFGTRE";

	private static final String ORTKUERZEL_VORHANDENES = "OHZTGFRE";

	private static final String SCHULEKUERZEL_FEHLENDES = "GFGTRTED";

	private static final String SCHULEKUERZEL_VORHANDENES = "VFRGTESR";

	private static final String UUID_ADMIN = "sdhfgsagdiuagauo";

	private static final String UUID_KEIN_ADMIN = "akjsgashio";

	private ILandDao landDao;

	private IOrtDao ortDao;

	private ISchuleDao schuleDao;

	private KatalogService katalogService;

	private IBenutzerService benutzerService;

	private Land land;

	private Ort ort;

	private Schule schule;

	@BeforeEach
	public void setUp() {
		land = new Land();
		land.setKuerzel(LANDKUERZEL_VORHANDENES);
		land.setName("Testland");

		ort = new Ort();
		ort.setKuerzel(ORTKUERZEL_VORHANDENES);
		ort.setName("Testort");
		land.addOrt(ort);

		schule = new Schule();
		schule.setKuerzel(SCHULEKUERZEL_VORHANDENES);
		schule.setName("Testgrundschule");
		ort.addSchule(schule);

		landDao = Mockito.mock(ILandDao.class);
		Mockito.when(landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, LANDKUERZEL_VORHANDENES))
			.thenReturn(Optional.of(land));
		Mockito.when(landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, LANDKUERZEL_FEHLENDES))
			.thenReturn(Optional.empty());

		ortDao = Mockito.mock(IOrtDao.class);
		Mockito.when(ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, ORTKUERZEL_VORHANDENES))
			.thenReturn(Optional.of(ort));
		Mockito.when(ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, ORTKUERZEL_FEHLENDES))
			.thenReturn(Optional.empty());

		schuleDao = Mockito.mock(ISchuleDao.class);
		Mockito.when(schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, SCHULEKUERZEL_VORHANDENES))
			.thenReturn(Optional.of(schule));
		Mockito.when(schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, SCHULEKUERZEL_FEHLENDES))
			.thenReturn(Optional.empty());

		benutzerService = Mockito.mock(IBenutzerService.class);
		Mockito.when(benutzerService.isBenutzerInRole(Role.MKV_ADMIN, UUID_ADMIN)).thenReturn(true);
		Mockito.when(benutzerService.isBenutzerInRole(Role.MKV_ADMIN, UUID_KEIN_ADMIN)).thenReturn(false);
		Mockito.when(benutzerService.isBenutzerInRole(Role.MKV_ADMIN, null)).thenThrow(new IllegalArgumentException());

		katalogService = new KatalogService(landDao, new LandCache(new LandCacheLoader(landDao)), ortDao,
			new OrtCache(new OrtCacheLoader(ortDao)), schuleDao, benutzerService);
	}

	@Test
	public void getLand_fehlendes_returns_empty() {
		final Optional<Land> opt = katalogService.getLand(LANDKUERZEL_FEHLENDES);
		assertFalse(opt.isPresent());
	}

	@Test
	public void findOrt_fehlendes_land_returns_empty() {
		final Optional<Ort> opt = katalogService.getOrt(LANDKUERZEL_FEHLENDES, ORTKUERZEL_VORHANDENES);
		assertFalse(opt.isPresent());
	}

	@Test
	public void landAnlegen_throws_unauthorized_when_not_admin() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.landAnlegen("de-HH", "Hamburg", UUID_KEIN_ADMIN, false);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void landAnlegen_throws_when_benutzerUuid_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			katalogService.landAnlegen("de-HH", "Hamburg", null, false);
		});
		assertEquals("benutzerUuid null", ex.getMessage());

	}

	@Test
	public void landNameAendern_throws_unauthorized_when_not_admin() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.landNameAendern("de-HH", "Hamburg", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void landNameAendern_throws_when_benutzerUuid_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			katalogService.landNameAendern("de-HH", "Hamburg", null);
		});
		assertEquals("benuzerrUuid null", ex.getMessage());

	}

	@Test
	public void ortAnlegen_throws_unauthorized_when_not_admin() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.ortAnlegen("de-HH", "Hamburg", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void ortAnlegen_throws_when_benutzerUuid_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			katalogService.ortAnlegen("de-TH", "Hamburg", null);
		});
		assertEquals("benuzerrUuid null", ex.getMessage());

	}

	@Test
	public void ortNameAendern_throws_unauthorized_when_not_admin() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.ortNameAendern(ORTKUERZEL_VORHANDENES, "Hamburg", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void ortNameAendern_throws_when_benutzerUuid_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			katalogService.ortNameAendern(ORTKUERZEL_VORHANDENES, "Hamburg", null);
		});
		assertEquals("benuzerrUuid null", ex.getMessage());

	}

	@Test
	public void schuleAnlegen_throws_unauthorized_when_not_admin() {
		final NeueSchulePayload payload = new NeueSchulePayload(LANDKUERZEL_VORHANDENES, ORTKUERZEL_VORHANDENES, "Gretchenschule");
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.schuleAnlegen(payload, UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void schuleAnlegen_throws_when_benutzerUuid_null() {
		final NeueSchulePayload payload = new NeueSchulePayload(LANDKUERZEL_VORHANDENES, ORTKUERZEL_VORHANDENES, "Gretchenschule");
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			katalogService.schuleAnlegen(payload, null);
		});
		assertEquals("benuzerrUuid null", ex.getMessage());

	}

	@Test
	public void schuleNameAendern_throws_unauthorized_when_not_admin() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.schuleNameAendern(SCHULEKUERZEL_VORHANDENES, "Hamburg", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void schuleNameAendern_when_benutzerUuid_null() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			katalogService.schuleNameAendern(SCHULEKUERZEL_VORHANDENES, "Hamburg", null);
		});
		assertEquals("benuzerrUuid null", ex.getMessage());

	}

	@Test
	public void ortAnlegen_throws_when_land_not_found() {
		Mockito.when(landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, LANDKUERZEL_FEHLENDES))
			.thenReturn(Optional.empty());
		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			katalogService.ortAnlegen(LANDKUERZEL_FEHLENDES, "Plusch", UUID_ADMIN);
		});
		assertEquals("Kein Land mit [kuerzel=EDCBA] gefunden", ex.getMessage());

	}

	@Test
	public void schuleAnlegen_throws_when_land_not_found() {
		Mockito.when(landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, LANDKUERZEL_FEHLENDES))
			.thenReturn(Optional.empty());
		final NeueSchulePayload payload = new NeueSchulePayload(LANDKUERZEL_FEHLENDES, ORTKUERZEL_FEHLENDES, "Gretchenschule");

		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			katalogService.schuleAnlegen(payload, UUID_ADMIN);
		});
		assertEquals("landkuerzel=EDCBA", ex.getMessage());
	}

	@Test
	public void schuleAnlegen_throws_when_ort_not_found() {
		final Land land = new Land();
		land.setKuerzel(LANDKUERZEL_VORHANDENES);
		Mockito.when(landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, LANDKUERZEL_VORHANDENES))
			.thenReturn(Optional.of(land));
		final NeueSchulePayload payload = new NeueSchulePayload(LANDKUERZEL_VORHANDENES, ORTKUERZEL_FEHLENDES, "Gretchenschule");

		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			katalogService.schuleAnlegen(payload, UUID_ADMIN);
		});
		assertEquals("ortkuerzel=HZTFGTRE", ex.getMessage());


	}

	@Test
	public void findOrte_throws_when_not_authorized() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.findOrte("sach", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void findOrte_land_throws_when_not_authorized() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.findOrte(LANDKUERZEL_VORHANDENES, "sach", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void findSchulen_throws_when_not_authorized() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.findSchulen("sach", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}

	@Test
	public void findSchulen_ort_throws_when_not_authorized() {
		final Throwable ex = assertThrows(EgladilAuthorizationException.class, () -> {
			katalogService.findSchulen(ORTKUERZEL_VORHANDENES, "sach", UUID_KEIN_ADMIN);
		});
		assertEquals("Keine Berechtigung.", ex.getMessage());

	}
}
