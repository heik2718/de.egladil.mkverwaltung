//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.PublicOrt;
import de.egladil.mkv.persistence.payload.response.PublicSchule;

/**
 * PublicOrtTest
 */
public class PublicOrtTest {

	@Test
	public void create_throws_when_parameter_null() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			PublicOrt.createMitSchulen(null);
		});
		assertEquals("ort darf nicht null sein", ex.getMessage());

	}

	@Test
	public void create_keine_exception_when_schulen_null() {
		// Arrange
		final Ort ort = new Ort();
		ort.setName("Gigerad");
		ort.setKuerzel("GI");
		ort.setSchulen(null);

		// Act
		final PublicOrt entity = PublicOrt.createMitSchulen(ort);

		// Assert
		assertTrue(entity.getKinder().isEmpty());
	}

	@Test
	public void create_mit_schule_klappt() {
		// Arrange
		final String ortname = "Gigerad";
		final String ortkuerzel = "GI";

		final String schulname = "Haligau";
		final String schulkuerzel = "J56FGTRE";

		final Ort ort = new Ort();
		ort.setName(ortname);
		ort.setKuerzel(ortkuerzel);

		final Schule schule = new Schule();
		schule.setName(schulname);
		schule.setKuerzel(schulkuerzel);

		ort.addSchule(schule);

		// Act
		final PublicOrt po = PublicOrt.createMitSchulen(ort);

		// Assert
		assertEquals(ortname, po.getName());
		assertEquals(ortkuerzel, po.getKuerzel());
		assertEquals(1, po.getKinder().size());

		final PublicSchule ps = po.getKinder().get(0);
		assertEquals(schulname, ps.getName());
		assertEquals(schulkuerzel, ps.getKuerzel());

	}

}
