//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * NummerGeneratorTest
 */
public class NummerGeneratorTest {

	private final TeilnahmeIdentifierProvider ident = new TeilnahmeIdentifierProvider() {

		@Override
		public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
			return TeilnahmeIdentifier.create(Teilnahmeart.P, "vdfdfd", "2018");
		}

	};

	private final Klassenstufe klassenstufe = Klassenstufe.EINS;

	int count = 0;

	void increment() {
		count = count + 1;
	}

	@Test
	void countConcurrently() throws Exception {
		final ExecutorService executor = Executors.newFixedThreadPool(2);

		IntStream.range(0, 10000).forEach(i -> executor.submit(this::increment));

		stop(executor);
		System.out.println("countConcurrently: " + count);

	}

	@Test
	void generateNummer() throws Exception {
		final ITeilnehmerDao teilnehmerDao = Mockito.mock(ITeilnehmerDao.class);
		Mockito.when(teilnehmerDao.getMaxNummer(ident, klassenstufe)).thenReturn(4);
		final NummerGenerator nummerGenerator = new NummerGenerator(teilnehmerDao);
		final ExecutorService executor = Executors.newFixedThreadPool(4);
		final List<Callable<Integer>> callables = new ArrayList<>();
		for (int i = 0; i < 13; i++) {
			callables.add(callable(nummerGenerator));
		}

		final List<Future<Integer>> futures = executor.invokeAll(callables);
		final List<Integer> result = new ArrayList<>();
		for (final Future<Integer> f : futures) {
			final Integer i = f.get();
			result.add(i);
			System.out.println("generateNummer: " + i);
		}
		for (int i = 5; i < 18; i++) {
			assertTrue(result.contains(Integer.valueOf(i)));
		}

		stop(executor);
	}

	Callable<Integer> callable(final NummerGenerator nummerGenerator) {
		return () -> {
			return nummerGenerator.nextNummer(ident, klassenstufe);
		};
	}

	void stop(final ExecutorService executor) {
		try {
			executor.shutdown();
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (final InterruptedException e) {
			System.err.println("termination interrupted");
		} finally {
			if (!executor.isTerminated()) {
				System.err.println("killing non-finished tasks");
			}
			executor.shutdownNow();
		}
	}
}
