//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.persistence.AbstractGuiceIT;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * OrtDaoImplIT
 */
public class OrtDaoImplIT extends AbstractGuiceIT {

	@Inject
	private IOrtDao ortDao;

	@Test
	public void findByUniqueKeyClazzNull() {
		// Arrange + Act + Assert
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findByUniqueKey(null, MKVConstants.KUERZEL_ATTRIBUTE_NAME, "tTT");
		});
		assertEquals("clazz darf nicht null sein", ex.getMessage());

	}

	@Test
	public void findByUniqueKeyNameNull() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findByUniqueKey(Ort.class, null, MKVConstants.KUERZEL_ATTRIBUTE_NAME);
		});
		assertEquals("attributeGetterName darf nicht null sein", ex.getMessage());

	}

	@Test
	public void findByUniqueKeyNameInvalid() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findByUniqueKey(Ort.class, "blubb", MKVConstants.KUERZEL_ATTRIBUTE_NAME);
		});
		assertEquals("Class Ort hat kein lesbares Attribut [blubb]: blubb", ex.getMessage());

	}

	@Test
	public void findByUniqueKeyKuerzelNull() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, null);
		});
		assertEquals("attributeValue darf nicht blank sein", ex.getMessage());

	}

	@Test
	public void findByUniqueKeyKuerzelBlank() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, " ");
		});
		assertEquals("attributeValue darf nicht blank sein", ex.getMessage());

	}

	@Test
	public void findByUniqueKey_klappt() {
		// Arrange
		final String kuerzel = "UKCJDEJ7";

		// Act
		final Optional<Ort> ort = ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertTrue(ort.isPresent());
	}

	@Test
	public void findByUniqueKey_kein_treffer() {
		// Arrange
		final String kuerzel = "U4901";

		// Act
		final Optional<Ort> ort = ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		// Assert
		assertFalse(ort.isPresent());
	}

	@Test
	public void findOrteInLandUnscharf_klappt() {
		// Arrange
		final String landkuerzel = "de-BB"; // Brandenburg
		final String ortname = "wald";

		// Act
		final List<Ort> orte = ortDao.findOrteInLandUnscharf(landkuerzel, ortname);

		// Assert
		assertEquals(17, orte.size());
	}

	@Test
	public void findOrteInLandUnscharfLandkuerzelNull() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findOrteInLandUnscharf(null, "ghgd");
		});
		assertEquals("landkuerzel darf nicht null sein", ex.getMessage());

	}

	@Test
	public void findOrteInLandUnscharfNameNull() {
		final Throwable ex = assertThrows(MKVException.class, () -> {
			ortDao.findOrteInLandUnscharf("de-BE", null);
		});
		assertEquals("ortname darf nicht null sein", ex.getMessage());

	}
}
