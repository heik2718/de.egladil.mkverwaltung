//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.converter;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;

/**
 * AntwortcodeConverterTest
 */
public class AntwortcodeConverterTest {

	@Test
	@DisplayName("Konvertierung eines Antwortcode-Strings in PublicAntowrt[] happy hour")
	void convertFromAntwortcodeKlappt() {
		final String antwortcode = "ABENDNECADDB";
		final AntwortcodeConverter converter = new AntwortcodeConverter();
		final PublicAntwortbuchstabe[] antwortbuchstaben = converter.fromAntwortcode(antwortcode);
		final PublicAntwortbuchstabe[] expectedAntwortbuchstaben = new PublicAntwortbuchstabe[] {
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("E", 5),
			new PublicAntwortbuchstabe("N", 0), new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("N", 0),
			new PublicAntwortbuchstabe("E", 5), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("B", 2) };

		assertArrayEquals(expectedAntwortbuchstaben, antwortbuchstaben);
	}

	@Test
	@DisplayName("IllegalArgumentException wenn convertFromString parameter null")
	void convertFromAntwortcodeNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			new AntwortcodeConverter().fromAntwortcode(null);
		});
	}

	@Test
	@DisplayName("IllegalArgumentException wenn convertFromString parameter invalid")
	void convertFromAntwortcodeInvalid() {
		assertThrows(IllegalArgumentException.class, () -> {
			new AntwortcodeConverter().fromAntwortcode("ABENDNECADDrB");
		});
	}

	@Test
	@DisplayName("IllegalArgumentException wenn fromAntwortbuchstaben parameter null")
	void convertFromPublicAntworen() {
		assertThrows(IllegalArgumentException.class, () -> {
			new AntwortcodeConverter().fromAntwortbuchstaben(null);
		});
	}

	@Test
	@DisplayName("Konvertierung in String klappt")
	void convertFromAntwortbuchstabenKlappt() {
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("E", 5), new PublicAntwortbuchstabe("-", 0),
			new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("E", 5),
			new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("D", 4),
			new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("B", 2) };

		final String expected = "ABENDNECADDB";

		assertEquals(expected, new AntwortcodeConverter().fromAntwortbuchstaben(antwortbuchstaben));
	}

	@Test
	void convertFromAntwortbuchstabenAusAnderemTestKlappt() {
		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("E", 1),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("C", 1), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("e", 5), new PublicAntwortbuchstabe("c", 1), new PublicAntwortbuchstabe("c", 3),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("d", 4), new PublicAntwortbuchstabe("b", 2),
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("C", 3),
			new PublicAntwortbuchstabe("B", 3), new PublicAntwortbuchstabe("A", 3) };

		assertEquals("EACBECCNDBANCBA", new AntwortcodeConverter().fromAntwortbuchstaben(antwortbuchstaben));
	}
}
