//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.adv;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.jupiter.api.Test;

/**
 * AdvVereinbarungTest
 */
public class AdvVereinbarungTest {

	@Test
	void getZugestimmtAmDruck() throws ParseException {
		// Arrange
		final String str = "10.05.2018 15:32:02";
		final String expected = "10.05.2018";
		final AdvVereinbarung vereinbarung = new AdvVereinbarung();
		final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		sdf.parse(str);
		vereinbarung.setZugestimmtAm(str);

		// Act
		final String actual = vereinbarung.getZugestimmtAmDruck();

		// Assert
		assertEquals(expected, actual);

	}

}
