//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * AuswertungsgruppeTest
 */
public class AuswertungsgruppeTest {

	private static final String TEILNAHMEKUERZEL = "GTFKL987";

	private static final String JAHR = "2018";

	private static final Teilnahmeart TEILNAHMEART = Teilnahmeart.S;

	private static final Klassenstufe KLASSENSTUFE = Klassenstufe.ZWEI;

	private Auswertungsgruppe parent;

	private AuswertungsgruppeChildBuilder builder;

	@BeforeEach
	void setUp() {
		parent = new AuswertungsgruppeBuilder(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR).name("Blumenwiesenschule").checkAndBuild();
		builder = new AuswertungsgruppeChildBuilder(parent, KLASSENSTUFE);
	}

	@Test
	@DisplayName("wenn name null, dann name aus klassenstufe gebildet")
	void getNameWennNameNull() {
		final Auswertungsgruppe auswertungsgruppe = builder.checkAndBuild();
		assertEquals("Klassenstufe 2", auswertungsgruppe.getName());
	}

	@Test
	@DisplayName("wenn name blank, dann name aus klassenstufe gebildet")
	void getNameWennNameBlank() {
		final Auswertungsgruppe auswertungsgruppe = builder.name(" ").checkAndBuild();
		assertEquals("Klassenstufe 2", auswertungsgruppe.getName());
	}

	@Test
	@DisplayName("name zieht vor klassenstufe")
	void getNameNameNichtNull() {
		final String expectedName = "Fuchsklasse";
		final Auswertungsgruppe auswertungsgruppe = builder.name(expectedName).checkAndBuild();
		assertEquals(expectedName, auswertungsgruppe.getName());

	}

	@Test
	@DisplayName("addTeilnehmer baut bidirektionale Beziehung auf")
	void addTeilnehmer() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(TeilnahmeIdentifier.create(TEILNAHMEART, TEILNAHMEKUERZEL, JAHR),
			KLASSENSTUFE).vorname("Hugo").nachname("Schmidt").checkAndBuild();
		final Auswertungsgruppe auswertungsgruppe = builder.name("Schneckenklasse").checkAndBuild();
		auswertungsgruppe.addTeilnehmer(teilnehmer);
		assertEquals(KLASSENSTUFE, teilnehmer.getKlassenstufe());
		assertEquals(auswertungsgruppe, teilnehmer.getAuswertungsgruppe());

	}

	@Test
	@DisplayName("Hinweis auf auskommentierte Tests. Können in anderem Kontext verwendet werden")
	@Disabled
	void fehlendeTests() {
		fail("Tests sind auskommentiert");
	}
}
