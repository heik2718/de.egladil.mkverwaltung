//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.plausi;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * ConvertToKlassenstufeCommandTest
 */
public class ConvertToKlassenstufeCommandTest {

	@Test
	@DisplayName("keine Exception wenn EINS")
	void jahrEINSStimmt() {
		try {
			new ConvertToKlassenstufeCommand("EINS").execute();
		} catch (final PreconditionFailedException e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("keine Exception wenn ZWEI")
	void jahrZWEIStimmt() {
		try {
			new ConvertToKlassenstufeCommand("ZWEI").execute();
		} catch (final PreconditionFailedException e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("IllegalArgumentException wenn parameter null")
	void constructorParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ConvertToKlassenstufeCommand(null);
		});
		assertEquals("klassenstufeName null", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn klassenstufeName irgendwas")
	void jahrIrgendwas() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new ConvertToKlassenstufeCommand(UUID.randomUUID().toString().substring(0, 4)).execute();
		});
		assertTrue(ex.getMessage().startsWith("No enum constant de.egladil.mkv.persistence.domain.enums.Klassenstufe"));
	}

	@Test
	void getKlassenstufeVorExcecute() {
		final Throwable ex = assertThrows(IllegalStateException.class, () -> {
			new ConvertToKlassenstufeCommand("EINS").getKlassenstufe();
		});
		assertEquals("IllegalState: please invoke execute() first", ex.getMessage());
	}

	@Test
	@DisplayName("happy hour")
	void nachExecuteGibtsKlassenstufe() {
		final ConvertToKlassenstufeCommand cmd = new ConvertToKlassenstufeCommand("EINS");
		cmd.execute();
		assertEquals(Klassenstufe.EINS, cmd.getKlassenstufe());
	}
}
