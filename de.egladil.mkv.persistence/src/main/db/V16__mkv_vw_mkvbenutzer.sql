-- view erweitern um date_modified
create or replace view vw_mkvbenutzer as
select k.vorname, k.nachname, b.id as ben_id, b.email, b.aktiviert, b.gesperrt, b.anonym, b.date_modified, r.name as rolle, k.last_login, b.uuid
from mkverwaltung.lehrerkonten k, blqzbv.benutzer b, blqzbv.benutzerrollen br, blqzbv.kat_rollen r
where k.uuid = b.uuid and br.benutzer_id = b.id and br.rolle_id = r.id
union all
select k.vorname, k.nachname, b.id as ben_id, b.email, b.aktiviert, b.gesperrt, b.anonym, b.date_modified, r.name as rolle, k.last_login, b.uuid
from mkverwaltung.privatkonten k, blqzbv.benutzer b, blqzbv.benutzerrollen br, blqzbv.kat_rollen r
where k.uuid = b.uuid and br.benutzer_id = b.id and br.rolle_id = r.id;