-- privatuploads werden gelöscht
drop table if exists privatuploads;

rename table schuluploads to uploads;

-- schuluploads erhalten eine Teilnahmeart und werden umbenannt
alter table uploads drop index uk_schuluploads_1;
alter table uploads drop column teilnahme;
alter table uploads add TEILNAHMEART varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S' COMMENT 'S(chule) oder P(rivat)';
alter table uploads change column SCHULKUERZEL TEILNAHMEKUERZEL varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'semantische Referenz auf das kuerzel der zugehörigen Teilnahme';
alter table uploads add JAHR varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2017' COMMENT 'Wettbewerbsjahr';
alter table uploads add VERARBEITET int(1) NOT NULL DEFAULT 0 COMMENT 'Flag, ob die Datei bereits nach loesungszettel überführt ist';
alter table uploads add unique index uk_uploads_1 (`TEILNAHMEART`, `CHECKSUMME`,`TEILNAHMEKUERZEL`,`JAHR`);

-- loesungszettel
alter table loesungszettel drop column ART;
alter table loesungszettel add TEILNAHMEART varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'S(schule) oder P(rivat)';
alter table loesungszettel change column TEILNAHMEKUERZEL TEILNAHMEKUERZEL varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'semantische Referenz auf das kuerzel der zugehörigen Teilnahme';
alter table loesungszettel add JAHR varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wettbewerbsjahr';
alter table loesungszettel add LANDKUERZEL varchar(5) COLLATE utf8_unicode_ci COMMENT 'Kürzel des (Bundes-)Landes';
alter table loesungszettel drop index uk_klassenzettel_1;
alter table loesungszettel drop index uk_klassenzettel_2;
alter table loesungszettel add unique index uk_loesungszettel_1 (`KUERZEL`);
alter table loesungszettel add unique index uk_loesungszettel_2 (`TEILNAHMEART`, `JAHR`, `TEILNAHMEKUERZEL`, `KLASSE`, `NUMMER`);
