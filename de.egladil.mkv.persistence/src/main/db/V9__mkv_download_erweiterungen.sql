ALTER TABLE `privatkonten` add column `DOWNLOADCODE` varchar(40) COLLATE utf8_unicode_ci;

ALTER TABLE `lehrerkonten` add column `DOWNLOADCODE` varchar(40) COLLATE utf8_unicode_ci;

DROP TABLE IF EXISTS `lehrerdownloads`;

CREATE TABLE `lehrerdownloads` (
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `KONTO` int(10) unsigned NOT NULL,
  `WAS` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ANZAHL` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_lehrerdownloads_1` (`KONTO`,`JAHR`,`WAS`),
    KEY `fk_lehrerdownloads_privat` (`KONTO`),
  CONSTRAINT `fk_lehrerdownloads_privat` FOREIGN KEY (`KONTO`) REFERENCES `lehrerkonten` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `privatdownloads` (
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `KONTO` int(10) unsigned NOT NULL,
  `WAS` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ANZAHL` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_privatdownloads_1` (`KONTO`,`JAHR`,`WAS`),
    KEY `fk_privatdownloads_privat` (`KONTO`),
  CONSTRAINT `fk_privatdownloads_privat` FOREIGN KEY (`KONTO`) REFERENCES `privatkonten` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- View für Länderstatistik
create view mkverwaltung.vw_teilnahmen_laender as select l.name, t.jahr from kat_laender l, kat_orte o, kat_schulen s, lehrer_schulen ls, klassenteilnahmen t where t.lehrer_schule = ls.id and ls.schule = s.id and s.ort = o.id and o.land = l.id and l.freigeschaltet = 1 and t.storniert = 0;

-- View für Mailverteiler Lehrer
create view mkverwaltung.vw_emails_lehrer as select l.vorname, l.nachname, b.email, t.jahr, t.storniert, t.mailnachricht, b.aktiviert, b.gesperrt from klassenteilnahmen t, lehrer_schulen ls, lehrerkonten l, blqzbv.benutzer b where b.uuid = l.uuid and ls.lehrer = l.id and t.lehrer_schule = ls.id;

-- View für Mailverteiler Privat
create view mkverwaltung.vw_emails_privat as select p.vorname, p.nachname, b.email, t.jahr, t.storniert, t.mailnachricht, b.aktiviert, b.gesperrt from privatteilnahmen t, privatkonten p, blqzbv.benutzer b where b.uuid = p.uuid and t.kontakt = p.id;

-- gekürzte Ereignisbeschreibungen
update kat_ereignisarten set beschreibung = 'eingeloggt' where kuerzel = 'SEC-001';
update kat_ereignisarten set beschreibung = 'ausgeloggt' where kuerzel = 'SEC-002';
update kat_ereignisarten set beschreibung = 'Passwort geändert' where kuerzel = 'SEC-003';
update kat_ereignisarten set beschreibung = 'Email geändert' where kuerzel = 'SEC-004';
update kat_ereignisarten set beschreibung = 'Datei download' where kuerzel = 'DAT-001';
update kat_ereignisarten set beschreibung = 'Datei upload' where kuerzel = 'DAT-002';
update kat_ereignisarten set beschreibung = 'Konto anonymisiert' where kuerzel = 'DAT-003';
update kat_ereignisarten set beschreibung = 'Kurzzeitpwd angefordert' where kuerzel = 'SEC-005';
update kat_ereignisarten set beschreibung = 'Land anlegen kein Admin' where kuerzel = 'SEC-006';
update kat_ereignisarten set beschreibung = 'Land ändern kein Admin' where kuerzel = 'SEC-007';
update kat_ereignisarten set beschreibung = 'Ort anlegen kein Admin' where kuerzel = 'SEC-008';
update kat_ereignisarten set beschreibung = 'Ort ändern kein Admin' where kuerzel = 'SEC-009';
update kat_ereignisarten set beschreibung = 'Schule anlegen kein Admin' where kuerzel = 'SEC-010';
update kat_ereignisarten set beschreibung = 'Schule ändern kein Admin' where kuerzel = 'SEC-011';
update kat_ereignisarten set beschreibung = 'Ort suchen kein Admin' where kuerzel = 'SEC-012';
update kat_ereignisarten set beschreibung = 'Schule suchen kein Admin' where kuerzel = 'SEC-013';
update kat_ereignisarten set kuerzel = 'SEC-015', beschreibung = 'Download ohne Berechtigung'  where kuerzel='DAT-001';

-- neue Ereignisse
insert into kat_ereignisarten (kuerzel, beschreibung) values ('DAT-004','unvollständig anonymisiert');