CREATE TABLE `auswertungdownloads` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DOWNLOADCODE` varchar(22) COLLATE utf8_unicode_ci NOT NULL COMMENT 'eindeutiger Schlüssel für den Link',
  `DATEN` LONGBLOB COMMENT 'generierte Auswertungsergebnisse, kann zip aus mehreren Dateien oder einzelnes pdf sein',
  `MODIFIED_BY` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID des angemeldeten Benutzers, der die Erstellung getriggert hat',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VERSION` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_auswertungdownloads_1` (`DOWNLOADCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=6744 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table mkverwaltung.auswertungsgruppen drop column urkunden;
