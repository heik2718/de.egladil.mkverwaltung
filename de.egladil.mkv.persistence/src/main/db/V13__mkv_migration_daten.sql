delimiter //

-- Flag Mailnachricht wandert zum konto
DROP PROCEDURE IF EXISTS migrate_mail_privat //
create procedure migrate_mail_privat()
begin
        declare done INT DEFAULT FALSE;
        declare v_mailnachricht, v_konto_id int;

        declare cur_teilnahmen cursor for select t.kontakt, t.mailnachricht from privatteilnahmen t;

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

        open cur_teilnahmen;
        read_loop: loop
                fetch cur_teilnahmen into v_konto_id, v_mailnachricht;

                IF done
                THEN
                  LEAVE read_loop;
                END IF;

                UPDATE privatkonten set version = version + 1, mailnachricht = v_mailnachricht where id = v_konto_id;
    end loop read_loop;
    close cur_teilnahmen;
end //
-- END migrate_mail_privat

-- Flag Mailnachricht wandert zum konto
DROP PROCEDURE IF EXISTS migrate_lehrer //
create procedure migrate_lehrer()
begin
        declare done INT DEFAULT FALSE;
        declare v_mailnachricht, v_konto_id, v_schule_id int;

        declare cur_teilnahmen cursor for select ls.lehrer, t.mailnachricht, ls.schule from bak_klassenteilnahmen t, bak_lehrer_schulen ls where t.lehrer_schule = ls.id;

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

        open cur_teilnahmen;
        read_loop: loop
                fetch cur_teilnahmen into v_konto_id, v_mailnachricht, v_schule_id;

                IF done
                THEN
                  LEAVE read_loop;
                END IF;

                UPDATE lehrerkonten set version = version + 1, mailnachricht = v_mailnachricht, schule = v_schule_id where id = v_konto_id;
    end loop read_loop;
    close cur_teilnahmen;
end //
-- END migrate_lehrer

-- nur für den ersten Schultreffer wird ein Eintrag erzeugt.
drop procedure if exists insert_schulteilnahmen //
create procedure insert_schulteilnahmen(in pi_schule_id int, in pi_jahr int)
begin

	declare done int default false;
	declare v_schulkuerzel varchar(8);
	declare v_anz int;

	select kuerzel into v_schulkuerzel from kat_schulen where id = pi_schule_id;
	select count(*) into v_anz from schulteilnahmen where kuerzel = v_schulkuerzel and jahr = concat('', pi_jahr) ;

	if (v_anz = 0) then
	   insert into schulteilnahmen (kuerzel, jahr) values (v_schulkuerzel, pi_jahr);
	end if;
END //
-- END insert_schulteilnahmen

-- verschiedene schulen lesen, für jede die erste teilnahme suchen und nur dafür einen Eintrag in schulteilnahmen erzeugen
drop procedure if exists read_lehrer_schulen //
create procedure read_lehrer_schulen(in pi_jahr int)
begin
	declare done int default false;
	declare v_ls_id, v_schule_id int;
	declare cur_ls cursor for select id, min(schule) as schule from bak_lehrer_schulen group by schule;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	open cur_ls;
	read_loop: loop
	   fetch from cur_ls into v_ls_id, v_schule_id;

	   IF done
       THEN
          LEAVE read_loop;
       END IF;

       call insert_schulteilnahmen(v_schule_id, pi_jahr);
    end loop read_loop;
    close cur_ls;
END //
-- END read_lehrer_schulen

--
DROP PROCEDURE IF EXISTS insert_teilnahmen_lehrer //
create procedure insert_teilnahmen_lehrer(IN pi_teilnahme_id int, IN pi_schule int)
begin
	declare done INT DEFAULT FALSE;
	declare v_lehrer_id int;
	declare cur_zuordn cursor for select lehrer from bak_lehrer_schulen where schule = pi_schule;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open cur_zuordn;
    read_loop: loop

    	fetch from cur_zuordn into v_lehrer_id;

    	IF done THEN
        	LEAVE read_loop;
	    END IF;

    	INSERT into schulteilnahmen_lehrer (teilnahme, lehrer) values (pi_teilnahme_id, v_lehrer_id);

    end loop read_loop;

    close cur_zuordn;
end //
-- END insert_teilnahmen_lehrer

-- für jeden Eintrag in schulteilnahmen werden n Einträge in schulteilnahmen_lehrer generiert
DROP PROCEDURE IF EXISTS read_schulteilnahmen //
CREATE PROCEDURE read_schulteilnahmen()
BEGIN

    declare done INT DEFAULT FALSE;

    declare v_teilnahme_id, v_schule_id int;
    declare v_schulkuerzel varchar(8);

	declare cur_schulteilnahmen cursor for select id, kuerzel from schulteilnahmen order by id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open cur_schulteilnahmen;
    read_loop: LOOP

    	fetch from cur_schulteilnahmen into v_teilnahme_id, v_schulkuerzel;

    	IF done THEN
        	LEAVE read_loop;
    	END IF;

    	select k.id into v_schule_id from kat_schulen k where k.kuerzel = v_schulkuerzel;

    	call insert_teilnahmen_lehrer(v_teilnahme_id, v_schule_id);

    end loop read_loop;
    close cur_schulteilnahmen;
END //
-- END read_schulteilnahmen

-- Für jedes Jahr werden Einträge in schulteilnahmen erzeugt
DROP PROCEDURE IF EXISTS read_jahre //
CREATE PROCEDURE read_jahre()
BEGIN

    declare done INT DEFAULT FALSE;

    declare v_jahr int;

	declare cur_jahre cursor for select distinct CAST(jahr AS int) as jahr from bak_klassenteilnahmen;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open cur_jahre;
    read_loop: LOOP

    	fetch from cur_jahre into v_jahr;

    	IF done THEN
        	LEAVE read_loop;
    	END IF;

    	call read_lehrer_schulen(v_jahr);

    end loop read_loop;
    close cur_jahre;
END //
-- END read_jahre

delimiter ;

 -- verschiebt Attribut nach privatkonten
call migrate_mail_privat;
 -- verschiebt Attribut nach lehrerkonten
call migrate_lehrer;
 -- generiert Einträge in schulteilnahmen für jedes Jahr
call read_jahre;
 -- generiert Einträge in schulteilnahmen_lehrer
call read_schulteilnahmen;


