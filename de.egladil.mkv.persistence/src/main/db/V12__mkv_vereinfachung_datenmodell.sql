-- Änderungen klassenteilnahmen
alter table mkverwaltung.klassenteilnahmen drop foreign key fk_klassenteilnahmen_lehrer;
alter table mkverwaltung.klassenteilnahmen drop column storniert;
alter table mkverwaltung.klassenteilnahmen drop column aktivierungsdatum;
rename table mkverwaltung.klassenteilnahmen to mkverwaltung.bak_klassenteilnahmen;

-- Spalten verschieben und neu setzen
alter table mkverwaltung.privatteilnahmen drop column downloads;
alter table mkverwaltung.privatteilnahmen drop column storniert;
alter table mkverwaltung.privatteilnahmen drop column aktivierungsdatum;
alter table mkverwaltung.lehrerkonten add MAILNACHRICHT int(1) NOT NULL DEFAULT 0 COMMENT 'flag, ob per Mail benachrichtigt werden soll';
alter table mkverwaltung.lehrerkonten add SCHULE int(10) unsigned COMMENT 'Referenz auf die Schule, zu der der Lehrer aktuell gehört';
alter table mkverwaltung.privatkonten add MAILNACHRICHT int(1) NOT NULL DEFAULT 0 COMMENT 'flag, ob per Mail benachrichtigt werden soll';

alter table mkverwaltung.klassendateien drop foreign key fk_klassendateien_teilnahme;
alter table mkverwaltung.klassendateien drop key fk_klassendateien_teilnahme;

alter table mkverwaltung.privatdateien drop foreign key fk_privatdateien_teilnahme;
alter table mkverwaltung.privatdateien drop key fk_privatdateien_teilnahme;

-- aenderungen in lehrer_schulen
alter table mkverwaltung.lehrer_schulen drop foreign key fk_lehrer_schulen_lehrer;
alter table mkverwaltung.lehrer_schulen drop foreign key fk_lehrerschulen_schule;
rename table mkverwaltung.lehrer_schulen to mkverwaltung.bak_lehrer_schulen;

-- neue Tabelle schulteilnahmen
drop table if exists mkverwaltung.schulteilnahmen;
CREATE TABLE mkverwaltung.schulteilnahmen (
  `KUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'semantische Referenz auf die Schule',
  `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_schulteilnahmen_1` (`JAHR`,`KUERZEL`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- neue Tabelle schulteilnahmen_lehrer
drop table if exists mkverwaltung.schulteilnahmen_lehrer;
CREATE TABLE mkverwaltung.schulteilnahmen_lehrer (
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TEILNAHME` int(10) unsigned NOT NULL,
  `LEHRER` int(10) unsigned NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_schulteilnahmen_1` (`LEHRER`,`TEILNAHME`),
  CONSTRAINT `fk_schulteilnahmen_teilnahme` FOREIGN KEY (`TEILNAHME`) REFERENCES `schulteilnahmen` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_schulteilnahmen_lehrer` FOREIGN KEY (`LEHRER`) REFERENCES `lehrerkonten` (`ID`)  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Tabelle privatzettel löschen
drop table if exists mkverwaltung.privatzettel;

-- Tabelle klassenzettel: fk zu teilnahme löschen
alter table mkverwaltung.klassenzettel drop foreign key fk_klassenzettel_teilnahme;

-- Tabelle klassenzettel Spalten umbenennen
alter table mkverwaltung.klassenzettel change `A1` `BA1` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `A2` `BA2` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `A3` `BA3` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `A4` `BA4` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `A5` `BA5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `B1` `BB1` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `B2` `BB2` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `B3` `BB3` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `B4` `BB4` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `B5` `BB5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `C1` `BC1` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `C2` `BC2` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `C3` `BC3` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `C4` `BC4` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';
alter table mkverwaltung.klassenzettel change `C5` `BC5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL  COMMENT 'Lösungsbuchstabe A-E oder N';

alter table mkverwaltung.klassenzettel change `WA1` `WA1` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WA2` `WA2` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WA3` `WA3` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WA4` `WA4` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WA5` `WA5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wertung r oder f oder n, kann wegen Klasse 1 auch NULL sein';
alter table mkverwaltung.klassenzettel change `WB1` `WB1` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WB2` `WB2` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WB3` `WB3` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WB4` `WB4` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WB5` `WB5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wertung r oder f oder n, kann wegen Klasse 1 auch NULL sein';
alter table mkverwaltung.klassenzettel change `WC1` `WC1` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WC2` `WC2` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WC3` `WC3` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WC4` `WC4` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Wertung r oder f oder n';
alter table mkverwaltung.klassenzettel change `WC5` `WC5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wertung r oder f oder n, kann wegen Klasse 1 auch NULL sein';

alter table mkverwaltung.klassenzettel change `QUELLE` `QUELLE`  varchar(6) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Quelle der Daten aus enum UPLOAD oder ONLINE';
alter table mkverwaltung.klassenzettel change `TEILNAHME` `TEILNAHMEKUERZEL`  varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Referenz auf die Schulteilnahme oder Privatteilnahme';

-- Tabelle Klassenzettel neue Spalte ART (werte P oder S)
alter table mkverwaltung.klassenzettel add ART varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'P(rivat) oder (S)chule für single table inheritance';
alter table mkverwaltung.klassenzettel add KONTOKUERZEL varchar(8) COLLATE utf8_unicode_ci COMMENT 'Referenz auf das Privat- oder Lehrerkonto zwecks temporärer Personalisierung. Kann beim Anonymisieren genullt werden';
alter table mkverwaltung.klassenzettel add ANONYM int(1)NOT NULL DEFAULT 0 COMMENT 'Flag, ob anonymisiert ist';
alter table mkverwaltung.klassenzettel add KIND varchar(100) COLLATE utf8_unicode_ci COMMENT 'Name des Kindes zum temporären Personalisieren zwecks Urkunden';
alter table mkverwaltung.klassenzettel add KLASSENNAME varchar(30) COLLATE utf8_unicode_ci COMMENT 'Name der Klasse des Kindes zum temporären Personalisieren zwecks Urkunden';


-- Tabelle klassenzettel umbenennen
rename table mkverwaltung.klassenzettel to mkverwaltung.loesungszettel;

-- neue Tabelle wettbewerbe
DROP TABLE if exists mkverwaltung.loesungsbuchstaben;
CREATE TABLE mkverwaltung.loesungsbuchstaben (
  `BA1` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BA2` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BA3` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BA4` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BA5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Lösungsbuchstabe A-E, kann wegen Klasse 1 NULL sein',
  `BB1` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BB2` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BB3` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BB4` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BB5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Lösungsbuchstabe A-E, kann wegen Klasse 1 NULL sein',
  `BC1` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BC2` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BC3` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BC4` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Lösungsbuchstabe A-E',
  `BC5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Lösungsbuchstabe A-E, kann wegen Klasse 1 NULL sein',
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'das Wettbewerbsjahr',
  `KLASSE` varchar(4) COLLATE utf8_unicode_ci NOT NULL comment 'aus enum: EINS, ZWEI',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_wettbewerbe_1` (`JAHR`, `KLASSE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Tabelle klassendateien umbenennen
rename table mkverwaltung.klassendateien to mkverwaltung.schuluploads;

-- Tabelle privatdateien umbenennen
rename table mkverwaltung.privatdateien to mkverwaltung.privatuploads;

-- kaputte views dropen
drop view if exists mkverwaltung.vw_klassenteilnahmen;
drop view if exists mkverwaltung.vw_teilnahmen_laender;
drop view if exists mkverwaltung.vw_emails_lehrer;
drop view if exists mkverwaltung.vw_emails_privat;
