insert into mkverwaltung.kat_ereignisarten (KUERZEL, BESCHREIBUNG) values ('SEC-014', 'fremde Daten ändern');

create view mkverwaltung.ereignisprotokoll as select a.kuerzel, a.beschreibung, e.wann, e.wer, e.was from kat_ereignisarten a, ereignisse e where a.kuerzel = e.ereignisart order by e.id