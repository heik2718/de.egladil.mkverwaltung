
alter table mkverwaltung.auswertungsgruppen add `MODIFIED_BY` varchar(40) COLLATE utf8_unicode_ci COMMENT 'UUID des angemeldeten Benutzers, der die Erstellung getriggert hat';
