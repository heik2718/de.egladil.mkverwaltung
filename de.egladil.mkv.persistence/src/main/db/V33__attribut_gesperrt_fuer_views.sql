
create or replace view vw_privatteilnahmen as
select t.id as id, t.kuerzel as kuerzel, k.vorname, k.nachname, b.id as ben_id, b.email, b.aktiviert, b.gesperrt, k.anonym, k.mailnachricht, k.last_login, t.jahr, b.uuid
from mkverwaltung.privatkonten k, mkverwaltung.privatteilnahmen t, blqzbv.benutzer b
where t.kontakt = k.id and k.uuid = b.uuid;

create or replace view vw_lehrerteilnahmen as
select concat_ws('', tl.teilnahme,tl.lehrer,t.jahr)  as id
, t.kuerzel as kuerzel
, k.vorname
, k.nachname
, b.id as ben_id
, b.email
, b.aktiviert
, b.gesperrt
, k.anonym
, k.mailnachricht
, t.jahr
, s.name as schule
, o.name as ort
, l.name as land
, k.last_login
, b.uuid
, s.kuerzel as schulkuerzel
, o.kuerzel as ortkuerzel
, l.kuerzel as landkuerzel
from mkverwaltung.lehrerkonten k
, schulteilnahmen t
, schulteilnahmen_lehrer tl
, blqzbv.benutzer b
, kat_schulen s
, kat_orte o
, kat_laender l
where tl.lehrer = k.id
and tl.teilnahme = t.id
and t.kuerzel = s.kuerzel
and s.ort = o.id
and o.land = l.id
and k.uuid = b.uuid;