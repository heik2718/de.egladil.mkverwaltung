-- Aufräumen nach erfolgreicher Migration
alter table privatteilnahmen drop column mailnachricht;

-- indexe renamen
alter table schuluploads drop index uk_klassendateien_1;
alter table schuluploads add unique index uk_schuluploads_1 (`CHECKSUMME`,`SCHULKUERZEL`);

alter table privatuploads drop index uk_privatdateien_1;
alter table privatuploads add unique index uk_privatuploads_1 (`CHECKSUMME`,`TEILNAHME`);

-- foreign keys
ALTER TABLE schulteilnahmen_lehrer ADD CONSTRAINT fk_stl_teilnahme FOREIGN KEY (teilnahme) REFERENCES schulteilnahmen (id) ON DELETE cascade ON UPDATE cascade;
ALTER TABLE schulteilnahmen_lehrer ADD CONSTRAINT fk_stl_lehrer FOREIGN KEY (lehrer) REFERENCES lehrerkonten (id) ON DELETE cascade ON UPDATE cascade;

ALTER TABLE lehrerkonten ADD CONSTRAINT fk_lehrer_schule FOREIGN KEY (schule) REFERENCES kat_schulen (id) ON DELETE cascade ON UPDATE cascade;

alter table lehrerdownloads drop foreign key fk_lehrerdownloads_privat;
ALTER TABLE lehrerdownloads ADD CONSTRAINT fk_lehrerdownloads_lehrer FOREIGN KEY (KONTO) REFERENCES lehrerkonten (id) ON DELETE cascade ON UPDATE cascade;

create or replace view mkverwaltung.vw_schulteilnahmen as
select t.id as id, l.kuerzel as land_kuerzel, l.name as land,  o.name as ort,  s.name as schule, t.jahr
from schulteilnahmen t, kat_schulen s, kat_orte o, kat_laender l where t.kuerzel = s.kuerzel and s.ort = o.id and o.land = l.id;

create or replace view mkverwaltung.vw_teilnahmen_laender as
select t.id as id, l.name, t.jahr
from kat_laender l, kat_orte o, kat_schulen s, schulteilnahmen t
where t.kuerzel = s.kuerzel and s.ort = o.id and o.land = l.id and l.freigeschaltet = 1;

-- View für Mailverteiler Lehrer
create or replace view mkverwaltung.vw_emails_lehrer as
select tl.id as id, l.vorname, l.nachname, b.email, t.jahr, l.mailnachricht, b.aktiviert, b.gesperrt
from schulteilnahmen t, schulteilnahmen_lehrer tl, lehrerkonten l, blqzbv.benutzer b
where b.uuid = l.uuid and tl.teilnahme = t.id and  tl.lehrer = l.id;

-- View für Mailverteiler Privat
create or replace view mkverwaltung.vw_emails_privat as
select t.id as id, p.vorname, p.nachname, b.email, t.jahr, p.mailnachricht, b.aktiviert, b.gesperrt
from privatteilnahmen t, privatkonten p, blqzbv.benutzer b
where b.uuid = p.uuid and t.kontakt = p.id;

create or replace view vw_lehrerteilnahmen as
select tl.id as id, t.kuerzel as kuerzel, k.vorname, k.nachname, b.id as ben_id, b.email, b.aktiviert, k.anonym, k.mailnachricht, t.jahr, s.name as schule, o.name as ort, l.name as land, k.last_login, b.uuid, s.kuerzel as schulkuerzel, o.kuerzel as ortkuerzel, l.kuerzel as landkuerzel
from mkverwaltung.lehrerkonten k, schulteilnahmen t, schulteilnahmen_lehrer tl, blqzbv.benutzer b, kat_schulen s, kat_orte o, kat_laender l
where tl.lehrer = k.id and tl.teilnahme = t.id and t.kuerzel = s.kuerzel and s.ort = o.id and o.land = l.id and k.uuid = b.uuid;

create or replace view vw_privatteilnahmen as
select t.id as id, t.kuerzel as kuerzel, k.vorname, k.nachname, b.id as ben_id, b.email, b.aktiviert, k.anonym, k.mailnachricht, k.last_login, t.jahr, b.uuid
from mkverwaltung.privatkonten k, mkverwaltung.privatteilnahmen t, blqzbv.benutzer b
where t.kontakt = k.id and k.uuid = b.uuid;


DROP PROCEDURE IF EXISTS read_jahre;
DROP PROCEDURE IF EXISTS read_schulteilnahmen;
DROP PROCEDURE IF EXISTS insert_teilnahmen_lehrer;
drop procedure if exists read_lehrer_schulen;
drop procedure if exists insert_schulteilnahmen;
DROP PROCEDURE IF EXISTS migrate_lehrer;
DROP PROCEDURE IF EXISTS migrate_mail_privat;


