-- View für Mailverteiler Lehrer
create or replace view mkverwaltung.vw_emails_lehrer as select b.ID, l.VORNAME, l.NACHNAME, b.EMAIL, t.JAHR, t.STORNIERT, t.MAILNACHRICHT, b.AKTIVIERT, b.GESPERRT from klassenteilnahmen t, lehrer_schulen ls, lehrerkonten l, blqzbv.benutzer b where b.uuid = l.uuid and ls.lehrer = l.id and t.lehrer_schule = ls.id and l.anonym = 0;

-- View für Mailverteiler Privat
create or replace view mkverwaltung.vw_emails_privat as select b.ID, p.VORNAME, p.NACHNAME, b.EMAIL, t.JAHR, t.STORNIERT, t.MAILNACHRICHT, b.AKTIVIERT, b.GESPERRT from privatteilnahmen t, privatkonten p, blqzbv.benutzer b where b.uuid = p.uuid and t.kontakt = p.id and p.anonym = 0;

-- checksumme mit sha256
alter table klassendateien change column `CHECKSUMME` `CHECKSUMME` varchar(128) COLLATE utf8_unicode_ci NOT NULL;
alter table privatdateien change column `CHECKSUMME` `CHECKSUMME` varchar(128) COLLATE utf8_unicode_ci NOT NULL;
