create or replace view vw_auswertungsgruppen as
select id, parent_id, kuerzel, teilnahmeart, teilnahmekuerzel, jahr, klassenstufe, name, farbschema, ueberschriftfarbe from auswertungsgruppen;

create or replace view vw_teilnehmer as
select id, kuerzel, auswertungsgruppe, teilnahmeart, teilnahmekuerzel, jahr, klassenstufe, vorname, nachname, zusatz, anonym, loesungszettelkuerzel as loesungszettel from teilnehmer;
