ALTER TABLE mkverwaltung.klassenteilnahmen
   DROP COLUMN `AKTIVIERT`;

ALTER TABLE mkverwaltung.privatteilnahmen
   DROP COLUMN `AKTIVIERT`;

DROP INDEX `uk_lehrerkonten_1` ON mkverwaltung.lehrerkonten;

ALTER TABLE mkverwaltung.lehrerkonten
   DROP COLUMN `KUERZEL`;

DROP INDEX `uk_privatkontakte_1` ON mkverwaltung.privatkonten;

ALTER TABLE mkverwaltung.privatkonten
   DROP COLUMN `KUERZEL`;

ALTER TABLE mkverwaltung.privatteilnahmen
   ADD `AKTIVIERUNGSDATUM` timestamp NOT NULL;

ALTER TABLE mkverwaltung.klassenteilnahmen
   ADD `AKTIVIERUNGSDATUM` timestamp NOT NULL;