ALTER TABLE mkverwaltung.schulteilnahmen_lehrer
   ADD CONSTRAINT fk_teiln_lehrer FOREIGN KEY(lehrer)
       REFERENCES mkverwaltung.lehrerkonten(`ID`) ON UPDATE CASCADE;

ALTER TABLE mkverwaltung.schulteilnahmen_lehrer
   ADD CONSTRAINT fk_teiln_schule FOREIGN KEY(teilnahme)
       REFERENCES mkverwaltung.schulteilnahmen(`ID`) ON UPDATE CASCADE;


create or replace view vw_lehrerteilnahmen as
select concat_ws('', tl.teilnahme,tl.lehrer,t.jahr)  as id
, t.kuerzel as kuerzel
, k.vorname
, k.nachname
, b.id as ben_id
, b.email
, b.aktiviert
, k.anonym
, k.mailnachricht
, t.jahr
, s.name as schule
, o.name as ort
, l.name as land
, k.last_login
, b.uuid
, s.kuerzel as schulkuerzel
, o.kuerzel as ortkuerzel
, l.kuerzel as landkuerzel
from mkverwaltung.lehrerkonten k
, schulteilnahmen t
, schulteilnahmen_lehrer tl
, blqzbv.benutzer b
, kat_schulen s
, kat_orte o
, kat_laender l
where tl.lehrer = k.id
and tl.teilnahme = t.id
and t.kuerzel = s.kuerzel
and s.ort = o.id
and o.land = l.id
and k.uuid = b.uuid;

create or replace VIEW `vw_emails_lehrer`
AS select concat_ws('',`t`.`ID`,`l`.`ID`,`b`.`ID`) AS `id`,`l`.`VORNAME` AS `vorname`,`l`.`NACHNAME` AS `nachname`,`b`.`EMAIL` AS `email`,`t`.`JAHR` AS `jahr`,`l`.`MAILNACHRICHT` AS `mailnachricht`,`b`.`AKTIVIERT` AS `aktiviert`,`b`.`GESPERRT` AS `gesperrt`
from (((`mkverwaltung`.`schulteilnahmen` `t` join `mkverwaltung`.`schulteilnahmen_lehrer` `tl`) join `mkverwaltung`.`lehrerkonten` `l`) join `blqzbv`.`benutzer` `b`)
where ((`b`.`UUID` = `l`.`UUID`) and (`tl`.`teilnahme` = `t`.`ID`) and (`tl`.`lehrer` = `l`.`ID`));





