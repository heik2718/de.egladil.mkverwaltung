-- ist jetzt finalisiert!!!! Nicht mehr ändern!

-- neue Felder in loesungszettel
alter table mkverwaltung.loesungszettel add KAENGURUSPRUNG int(2) NOT NULL COMMENT 'Länge des Kängurusprungs';
alter table mkverwaltung.loesungszettel add TYPO int(1) NOT NULL DEFAULT 0 COMMENT 'Flag, ob dieser Lösungszettel einen Typo enthielt';
alter table mkverwaltung.loesungszettel change KIND ANREDE varchar(101) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Name des Kindes zum temporären Personalisieren zwecks Urkunden';
alter table mkverwaltung.loesungszettel add VORNAME varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Vorname des Kindes zum temporären Personalisieren zwecks Urkunden';
alter table mkverwaltung.loesungszettel add NACHNAME varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nachname des Kindes zum temporären Personalisieren zwecks Urkunden';
alter table mkverwaltung.loesungszettel add ORIGINALWERTUNG varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Die Wertungen kommasepariert aus der Excel-Tabelle';
alter table mkverwaltung.loesungszettel add PUNKTE integer(4) DEFAULT NULL COMMENT 'Die erreichten Punkte als ganze Zahl (also mit 100 multipliziert)';
alter table mkverwaltung.loesungszettel drop column BA1;
alter table mkverwaltung.loesungszettel drop column BA2;
alter table mkverwaltung.loesungszettel drop column BA3;
alter table mkverwaltung.loesungszettel drop column BA4;
alter table mkverwaltung.loesungszettel drop column BA5;
alter table mkverwaltung.loesungszettel drop column BB1;
alter table mkverwaltung.loesungszettel drop column BB2;
alter table mkverwaltung.loesungszettel drop column BB3;
alter table mkverwaltung.loesungszettel drop column BB4;
alter table mkverwaltung.loesungszettel drop column BB5;
alter table mkverwaltung.loesungszettel drop column BC1;
alter table mkverwaltung.loesungszettel drop column BC2;
alter table mkverwaltung.loesungszettel drop column BC3;
alter table mkverwaltung.loesungszettel drop column BC4;
alter table mkverwaltung.loesungszettel drop column BC5;
alter table mkverwaltung.loesungszettel drop column WA1;
alter table mkverwaltung.loesungszettel drop column WA2;
alter table mkverwaltung.loesungszettel drop column WA3;
alter table mkverwaltung.loesungszettel drop column WA4;
alter table mkverwaltung.loesungszettel drop column WA5;
alter table mkverwaltung.loesungszettel drop column WB1;
alter table mkverwaltung.loesungszettel drop column WB2;
alter table mkverwaltung.loesungszettel drop column WB3;
alter table mkverwaltung.loesungszettel drop column WB4;
alter table mkverwaltung.loesungszettel drop column WB5;
alter table mkverwaltung.loesungszettel drop column WC1;
alter table mkverwaltung.loesungszettel drop column WC2;
alter table mkverwaltung.loesungszettel drop column WC3;
alter table mkverwaltung.loesungszettel drop column WC4;
alter table mkverwaltung.loesungszettel drop column WC5;
alter table mkverwaltung.loesungszettel add ANTWORTCODE varchar(15) COLLATE utf8_unicode_ci COMMENT 'lückenlose Aneinanderreihung aller vom Kind gesetzten Antwortbuchstaben A-E oder N. 15 für Klasse 2, 12 für Klasse 1';
alter table mkverwaltung.loesungszettel add WERTUNGSCODE varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'lückenlose Aneinanderreihung der Bewertung des Antwortcodes f,n oder r. 15 für Klasse 2, 12 für Klasse 1';

alter table mkverwaltung.loesungsbuchstaben drop column BA1;
alter table mkverwaltung.loesungsbuchstaben drop column BA2;
alter table mkverwaltung.loesungsbuchstaben drop column BA3;
alter table mkverwaltung.loesungsbuchstaben drop column BA4;
alter table mkverwaltung.loesungsbuchstaben drop column BA5;
alter table mkverwaltung.loesungsbuchstaben drop column BB1;
alter table mkverwaltung.loesungsbuchstaben drop column BB2;
alter table mkverwaltung.loesungsbuchstaben drop column BB3;
alter table mkverwaltung.loesungsbuchstaben drop column BB4;
alter table mkverwaltung.loesungsbuchstaben drop column BB5;
alter table mkverwaltung.loesungsbuchstaben drop column BC1;
alter table mkverwaltung.loesungsbuchstaben drop column BC2;
alter table mkverwaltung.loesungsbuchstaben drop column BC3;
alter table mkverwaltung.loesungsbuchstaben drop column BC4;
alter table mkverwaltung.loesungsbuchstaben drop column BC5;
alter table mkverwaltung.loesungsbuchstaben add LOESUNGSCODE varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'lückenlose Aneinanderreihung aller Lösunsbuchstaben A-E. 15 für Klasse 2, 12 für Klasse 1';

-- insert loesungsbuchstaben 2017
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2017', 'EINS', 'CDADECCCBCDE');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2017', 'ZWEI', 'EACDDCDCCEBACBA');

SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE mkverwaltung.schulteilnahmen_lehrer DROP INDEX uk_schulteilnahmen_1;
ALTER TABLE mkverwaltung.schulteilnahmen_lehrer ADD UNIQUE KEY uk_lehrerteilnahmen_1 (`LEHRER`,`TEILNAHME`);
SET FOREIGN_KEY_CHECKS = 1;

alter table mkverwaltung.uploads drop column verarbeitet;
alter table mkverwaltung.uploads add STATUS varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NEU' COMMENT 'Verarbeitungsstatus der Datei: NEU, FERTIG, LEER, FEHLER';

alter table mkverwaltung.uploads change dateiname DATEINAME varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name der Datei im upload-Verzeichnis';

-- view für uploadinfo
create or replace view vw_uploadinfos as select u.ID, u.TEILNAHMEKUERZEL, u.TEILNAHMEART, u.MIMETYPE, u.DATEINAME, u.JAHR, u.STATUS, null as LANDKUERZEL from uploads u, privatteilnahmen p where u.TEILNAHMEKUERZEL = p.KUERZEL
UNION ALL
select u.ID, u.TEILNAHMEKUERZEL, u.TEILNAHMEART, u.MIMETYPE, u.DATEINAME, u.JAHR, u.STATUS, l.KUERZEL as LANDKUERZEL from uploads u, kat_schulen s, kat_orte o, kat_laender l where u.TEILNAHMEKUERZEL = s.KUERZEL and s.ORT = o.ID and o.LAND = l.ID;

alter table mkverwaltung.ereignisse change WAS WAS varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL;
insert into kat_ereignisarten (kuerzel, beschreibung) values ('DAT-004','upload verarbeitet');

-- ist jetzt finalisiert!!!! Nicht mehr ändern!

