
CREATE TABLE `adv_texte` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `VERSIONSNUMMER` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Versionsnumer als eindeutiger Schlüssel des Vereinbarungstextes',
  `DATEINAME` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name der Datei im download-Verzeichnis',
  `CHECKSUMME` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Checksumme der Datei im download-Verzeichnis',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_adv_texte_1` (`VERSIONSNUMMER`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `adv_vereinbarungen` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AVD_TEXTE_ID` int(10) unsigned NOT NULL,
  `SCHULKUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Referenz auf kat_schulen und damit Ort und Land',
  `SCHULNAME`  varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `STRASSE`  varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `HAUSNR`  varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ORT`  varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PLZ`  varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `LAENDERCODE`  varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `ZUGESTIMMT_AM` varchar(19) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Zeitstempel im vollständigen Datum-Uhrzeit-Format dd.MM.yyyy hh:mm:ss',
  `ZUGESTIMMT_DURCH` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID des angemeldeten Benutzers, der zugestimmt hat.',
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_adv_vereinbarungen_1` (`SCHULKUERZEL`),
  CONSTRAINT `fk_vereinbarung_texte` FOREIGN KEY (`AVD_TEXTE_ID`) REFERENCES `adv_texte` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into adv_texte (versionsnummer, dateiname, checksumme) values ('1.0', 'adv-vereinbarung-1.0.pdf', '1fdd891c8d109fd3ddc3494ad8dbe03c0c4b2670b2505152f6f7bed82c380c50');




