drop table if exists `bak_lehrer_schulen`;
drop table if exists `bak_klassenteilnahmen`;

ALTER TABLE `loesungszettel` DROP COLUMN `ANREDE`;
ALTER TABLE `loesungszettel` DROP COLUMN `KONTOKUERZEL`;
ALTER TABLE `loesungszettel` DROP COLUMN `KLASSENNAME`;
ALTER TABLE `loesungszettel` DROP COLUMN `VORNAME`;
ALTER TABLE `loesungszettel` DROP COLUMN `NACHNAME`;
ALTER TABLE `loesungszettel` DROP COLUMN `ANONYM`;
ALTER TABLE `loesungszettel` CHANGE COLUMN `KLASSE` `KLASSENSTUFE` varchar(4) COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `loesungszettel` CHANGE COLUMN `KUERZEL` `KUERZEL` varchar(22) COLLATE utf8_unicode_ci NOT NULL;

update loesungszettel set kuerzel = concat(kuerzel,replace(replace(concat(date(date_modified),time(date_modified)),'-',''),':',''));

CREATE TABLE `auswertungsgruppen` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PARENT_ID` int(10) unsigned ,
  `KUERZEL` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `TEILNAHMEART` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'P = Privatteilnahme, S = Schulteilnahme',
  `TEILNAHMEKUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'semantische Referenz auf schulteilnahmen oder privatteilnahmen',
  `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'identifiziert zusammen mit SCHULKUERZEL eine schulteilnahme',
  `KLASSENSTUFE` varchar(4) COLLATE utf8_unicode_ci COMMENT 'Klassenstufe als fallback, falls NAME null',
  `NAME` varchar(110) COLLATE utf8_unicode_ci COMMENT 'Name zum Druck auf die Urkunde. Kann null sein, wenn dieser z.B. schon im Overlay enthalten ist',
  `FARBSCHEMA` varchar(15) COLLATE utf8_unicode_ci COMMENT 'identifiziert das Hintergrundbild der durch mich bereitgestellten Urkundenvorlagen',
  `OVERLAY` LONGBLOB COMMENT 'optionales Hintergrundbild für Urkunden',
  `UEBERSCHRIFTFARBE` varchar(15) COLLATE utf8_unicode_ci COMMENT 'Farbe der Überschrift bei Verwendung eines eigenen Hintergrundbildes',
  `URKUNDEN` LONGBLOB COMMENT 'generiertes Urkunden-PDF',
  `TEILNEHMERUEBERSICHT` MEDIUMTEXT COMMENT 'Teilnehmerübersicht (Namen, Punkte, Kängurusprung, Platzierung,...) als xml',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VERSION` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_auswertungsgruppen_1` (`KUERZEL`),
  UNIQUE KEY `uk_auswertungsgruppen_2` (`PARENT_ID`,`TEILNAHMEART`, `TEILNAHMEKUERZEL`, `JAHR`, `KLASSENSTUFE`, `NAME`),
  CONSTRAINT `fk_gruppe_parent` FOREIGN KEY (`PARENT_ID`) REFERENCES `auswertungsgruppen` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6744 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE mkverwaltung.teilnehmer (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KUERZEL` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `AUSWERTUNGSGRUPPE` int(10) unsigned COMMENT 'referenz auf auswertungsgruppen',
  `TEILNAHMEART` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'P = Privatteilnahme, S = Schulteilnahme',
  `TEILNAHMEKUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'semantische Referenz auf schulteilnahmen oder privatteilnahmen',
  `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'identifiziert zusammen mit SCHULKUERZEL eine schulteilnahme',
  `KLASSENSTUFE` varchar(4) COLLATE utf8_unicode_ci COMMENT 'Klassenstufe als fallback, falls NAME null',
  `LOESUNGSZETTELKUERZEL` varchar(22) COLLATE utf8_unicode_ci COMMENT 'semantische referenz auf loesungszettel',
  `VORNAME` varchar(55) COLLATE utf8_unicode_ci COMMENT 'Vorname zum Druck auf die Urkunde',
  `NACHNAME` varchar(55) COLLATE utf8_unicode_ci COMMENT 'Nachname zum Druck auf die Urkunde',
  `ZUSATZ` varchar(55) COLLATE utf8_unicode_ci COMMENT 'kann verwendet werden, wenn es in dieser Klasse mehr als ein Kind mit diesem vornamen und namen gibt. nur zur Anzeige, wird nicht gedruckt',
  `ANONYM` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag anonym 0/1',
  `URKUNDE` LONGBLOB COMMENT 'generiertes Urkunden-PDF',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VERSION` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_teilnehmer_1` (`KUERZEL`),
  UNIQUE KEY `uk_teilnehmer_2` (`AUSWERTUNGSGRUPPE`, `TEILNAHMEART`, `TEILNAHMEKUERZEL`, `JAHR`, `KLASSENSTUFE`, `VORNAME`, `NACHNAME`, `ZUSATZ`),
  CONSTRAINT `fk_teilnehmer_gruppe` FOREIGN KEY (`AUSWERTUNGSGRUPPE`) REFERENCES `auswertungsgruppen` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
