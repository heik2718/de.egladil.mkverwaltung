
alter table teilnehmer add column `SPRACHE` varchar(3) COLLATE utf8_unicode_ci COMMENT 'Kürzel für die Sprache der Aufgaben und Urkunden';

alter table loesungszettel add column `SPRACHE` varchar(3) COLLATE utf8_unicode_ci COMMENT 'Kürzel für die Sprache der Aufgaben';

update teilnehmer set sprache = 'de';

update loesungszettel set sprache = 'de';

