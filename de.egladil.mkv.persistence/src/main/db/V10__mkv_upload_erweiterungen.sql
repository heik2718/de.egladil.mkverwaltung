-- Klassen- und Privatzettel müssen auf Position 5 auch NULL zulassen.
ALTER TABLE `klassenzettel` modify column `A5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `klassenzettel` modify column `B5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `klassenzettel` modify column `C5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `klassenzettel` modify column `WA5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `klassenzettel` modify column `WB5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `klassenzettel` modify column `WC5` varchar(1) COLLATE utf8_unicode_ci;

ALTER TABLE `privatzettel` modify column `A5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `privatzettel` modify column `B5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `privatzettel` modify column `C5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `privatzettel` modify column `WA5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `privatzettel` modify column `WB5` varchar(1) COLLATE utf8_unicode_ci;
ALTER TABLE `privatzettel` modify column `WC5` varchar(1) COLLATE utf8_unicode_ci;

CREATE TABLE `klassendateien` (
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_UPLOAD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MIMETYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `SCHULKUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `CHECKSUMME` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `DATEINAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEILNAHME` int(10) unsigned NOT NULL,
  `OHNE_DOWNLOADS` int(1) NOT NULL default 0,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_klassendateien_1` (`CHECKSUMME`,`TEILNAHME`),
    KEY `fk_klassendateien_teilnahme` (`TEILNAHME`),
  CONSTRAINT `fk_klassendateien_teilnahme` FOREIGN KEY (`TEILNAHME`) REFERENCES `klassenteilnahmen` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `privatdateien` (
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_UPLOAD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MIMETYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `TEILNAHMEKUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `DATEINAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CHECKSUMME` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEILNAHME` int(10) unsigned NOT NULL,
  `OHNE_DOWNLOADS` int(1) NOT NULL default 0,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_privatdateien_1` (`CHECKSUMME`,`TEILNAHME`),
    KEY `fk_privatdateien_teilnahme` (`TEILNAHME`),
  CONSTRAINT `fk_privatdateien_teilnahme` FOREIGN KEY (`TEILNAHME`) REFERENCES `privatteilnahmen` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- neue EReignisarten
insert into kat_ereignisarten (kuerzel, beschreibung) values ('SEC-016','Upload zu groß');
insert into kat_ereignisarten (kuerzel, beschreibung) values ('SEC-017','Upload falscher MIME-Type');

-- entfernte Ereignisarten
delete from kat_ereignisarten where KUERZEL in ('DAT-001', 'DAT-002');
update kat_ereignisarten set KUERZEL = 'DAT-001' where KUERZEL = 'DAT-003';
update kat_ereignisarten set KUERZEL = 'DAT-002' where KUERZEL = 'DAT-004';
