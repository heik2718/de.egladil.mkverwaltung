-- dateiname verlängern
alter table uploads change dateiname dateiname varchar(150) COLLATE utf8_unicode_ci NOT NULL;

-- Daten korrigieren
update uploads set version = version + 1, dateiname = concat('S_', TEILNAHMEKUERZEL, '_', checksumme, '.ods') where mimetype = 'ODS';
update uploads set version = version + 1, dateiname = concat('S_', TEILNAHMEKUERZEL, '_', checksumme, '.xlsx') where mimetype = 'XLSX';
update uploads set version = version + 1, dateiname = concat('S_', TEILNAHMEKUERZEL, '_', checksumme, '.unknown') where mimetype = 'UNKNOWN';
update uploads set version = version + 1, dateiname = concat('S_', TEILNAHMEKUERZEL, '_', checksumme, '.xls') where mimetype in ('XLS', 'EXCEL', 'XLA_X_EXCEL', 'XLA_X_MSEXCEL', 'VDN_MSEXCEL');
