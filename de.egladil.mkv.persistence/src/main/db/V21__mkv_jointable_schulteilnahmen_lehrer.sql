-- Many to many relationship between schulteilnahmen und lehrer redefinieren

-- neue Tabelle schulteilnahmen
drop table if exists mkverwaltung.neu_schulteilnahmen_lehrer;
create table neu_schulteilnahmen_lehrer as select teilnahme, lehrer from schulteilnahmen_lehrer;

drop table if exists mkverwaltung.schulteilnahmen_lehrer;

alter table mkverwaltung.neu_schulteilnahmen_lehrer rename to mkverwaltung.schulteilnahmen_lehrer;



