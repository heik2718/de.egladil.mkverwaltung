
create or replace view vw_schulen as SELECT l.ID AS LAND_ID, l.KUERZEL AS LAND_KUERZEL, l.NAME AS LAND_NAME
	, o.ID AS ORT_ID, o.KUERZEL AS ORT_KUERZEL, o.NAME AS ORT_NAME, s.* from kat_laender l, kat_orte o, kat_schulen s where s.ort = o.id and o.land = l.id;



