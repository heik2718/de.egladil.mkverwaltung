
alter table auswertungsgruppen CHANGE `MODIFIED_BY` `GEAENDERT_DURCH` varchar(40) COLLATE utf8_unicode_ci COMMENT 'UUID des angemeldeten Benutzers, der Daten erfasst/geändert hat (Protokollierung DSGVO).';

alter table teilnehmer add column `GEAENDERT_DURCH` varchar(40) COLLATE utf8_unicode_ci COMMENT 'UUID des angemeldeten Benutzers, der die Daten erfasst/geändert hat (Protokollierung DSGVO).';

alter table kat_ereignisarten add column `AUFBEWAHREN` int(1) NOT NULL DEFAULT 0 COMMENT 'Flag, ob Ereignisse mit diesem Typ Zwecks Nachweispflich DSGVO länger als 2 Wochen aufbewahrt werden.';


insert into kat_ereignisarten (kuerzel, beschreibung, aufbewahren) values ('DAT-010', 'Teilnehmer geändert', 1);
update kat_ereignisarten set beschreibung = 'Auswertungsgruppe geändert' where kuerzel = 'DAT-009';
update kat_ereignisarten set aufbewahren = 1 where kuerzel in ('DAT-001', 'DAT-005', 'DAT-006', 'DAT-008');

