
create or replace view mkverwaltung.ereignisprotokoll as select e.id as eid, a.id as aid, e.ereignisart, a.kuerzel, a.beschreibung, e.was, substr(e.wer,1,8) as wer, e.wann from kat_ereignisarten a, ereignisse e where a.kuerzel = e.ereignisart order by e.id;

create or replace view mkverwaltung.vw_schulteilnahmen as
select t.id as id, l.kuerzel as landkuerzel, l.name as land, o.kuerzel as ortkuerzel, o.name as ort, s.kuerzel as schulkuerzel,  s.name as schule, t.jahr
from schulteilnahmen t, kat_schulen s, kat_orte o, kat_laender l where t.kuerzel = s.kuerzel and s.ort = o.id and o.land = l.id;

