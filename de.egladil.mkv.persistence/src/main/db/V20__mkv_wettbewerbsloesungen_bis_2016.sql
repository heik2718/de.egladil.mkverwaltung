delete from mkverwaltung.loesungsbuchstaben;

insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2010', 'ZWEI','EDBEBCCCABBADED');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2011', 'ZWEI','CDECEDDABDBBAAD');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2012', 'ZWEI','ECDBDCBCBBCABAD');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2013', 'ZWEI','DBCCBEBEEBDCBAA');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2014', 'EINS','BABCAAADBACCEDB');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2014', 'ZWEI','BABCAAADBACCEDB');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2015', 'EINS','CDDEBDCAAECBCAE');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2015', 'ZWEI','CDDEBDCAAECBCAE');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2016', 'EINS','EDDADECCCCABABD');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2016', 'ZWEI','EDDADECCCCABABD');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2017', 'EINS', 'CDADECCCBCDE');
insert into mkverwaltung.loesungsbuchstaben (JAHR, KLASSE, LOESUNGSCODE) values ('2017', 'ZWEI', 'EACDDCDCCEBACBA');
