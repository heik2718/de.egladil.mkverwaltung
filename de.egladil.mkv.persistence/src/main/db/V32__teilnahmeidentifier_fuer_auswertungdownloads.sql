
delete from mkverwaltung.auswertungdownloads;

alter table mkverwaltung.auswertungdownloads add `TEILNAHMEART` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'P = Privatteilnahme, S = Schulteilnahme';
alter table mkverwaltung.auswertungdownloads add `TEILNAHMEKUERZEL` varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'semantische Referenz auf schulteilnahmen oder privatteilnahmen';
alter table mkverwaltung.auswertungdownloads add `JAHR` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'identifiziert zusammen mit SCHULKUERZEL eine schulteilnahme';
