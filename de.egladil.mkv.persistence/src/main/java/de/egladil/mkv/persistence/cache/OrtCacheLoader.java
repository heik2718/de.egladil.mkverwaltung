//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.cache;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheLoader;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.domain.kataloge.Ort;

/**
 * OrtCacheLoader
 */
@Singleton
public class OrtCacheLoader extends CacheLoader<String, Ort> {

	private static final Logger LOG = LoggerFactory.getLogger(OrtCacheLoader.class);

	private final IOrtDao ortDao;

	private ExecutorService executor;

	/**
	 * Erzeugt eine Instanz von OrtCacheLoader
	 */
	@Inject
	public OrtCacheLoader(IOrtDao ortDao) {
		this.ortDao = ortDao;
		executor = Executors.newSingleThreadExecutor();
	}

	/**
	 * @see com.google.common.cache.CacheLoader#load(java.lang.Object)
	 */
	@Override
	public Ort load(String key) throws Exception {
		try {
			return readOrtFromDB(key);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * @see com.google.common.cache.CacheLoader#reload(java.lang.Object, java.lang.Object)
	 */
	@Override
	public ListenableFuture<Ort> reload(String kuerzel, Ort oldValue) throws Exception {
		ListenableFutureTask<Ort> task = ListenableFutureTask.create(new Callable<Ort>() {
			@Override
			public Ort call() throws Exception {
				try {
					Ort ort = readOrtFromDB(kuerzel);
					LOG.info("Ort {} reloaded", kuerzel);
					return ort == null ? oldValue : ort;
				} catch (Throwable e) {
					LOG.warn("Error on reload Ort {}", kuerzel);
					return oldValue;
				}
			}
		});
		executor.execute(task);
		return task;
	}

	/**
	 * TODO
	 *
	 * @param key
	 * @return
	 */
	public Ort readOrtFromDB(String key) {
		Optional<Ort> opt = ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, key);
		return opt.isPresent() ? opt.get() : null;
	}
}
