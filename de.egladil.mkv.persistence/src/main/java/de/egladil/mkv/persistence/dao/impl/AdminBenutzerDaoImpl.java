//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IAdminBenutzerDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.user.AdminBenutzerinfo;

/**
 * AdminBenutzerDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class AdminBenutzerDaoImpl extends BaseDaoImpl<AdminBenutzerinfo> implements IAdminBenutzerDao {

	private static final Logger LOG = LoggerFactory.getLogger(AdminBenutzerDaoImpl.class);

	@Inject
	public AdminBenutzerDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public List<AdminBenutzerinfo> findWithName(final String name) {

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("name blank");
		}

		final String stmt = "select a from AdminBenutzerinfo a where lower(a.vorname) like :name or lower(a.nachname) like :name";

		final TypedQuery<AdminBenutzerinfo> query = getEntityManager().createQuery(stmt, AdminBenutzerinfo.class);
		query.setParameter("name", "%" + name.toLowerCase() + "%");

		final List<AdminBenutzerinfo> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());
		return trefferliste;
	}

	@Override
	public BigInteger getAnzahlWithNameLike(final String name) {

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("name blank");
		}

		final String stmt = "select count(*) from vw_mkvbenutzer a where lower(a.vorname) like :name or lower(a.nachname) like :name";

		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("name", "%" + name.toLowerCase() + "%");
		return SqlUtils.getCount(query);
	}



	@Override
	public BigInteger getAnzahlWhereUuidStartsWith(final String uuidPart) {

		if (StringUtils.isBlank(uuidPart)) {
			throw new IllegalArgumentException("uuidPart blank");
		}

		final String stmt = "select count(*) from vw_mkvbenutzer a where a.uuid like :uuidPart";

		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("uuidPart", uuidPart + "%");
		return SqlUtils.getCount(query);

	}

	@Override
	public List<AdminBenutzerinfo> findWhereUuidStartsWith(final String uuidPart) {

		if (StringUtils.isBlank(uuidPart)) {
			throw new IllegalArgumentException("uuidPart blank");
		}

		final String stmt = "select a from AdminBenutzerinfo a where a.uuid like :uuidPart";

		final TypedQuery<AdminBenutzerinfo> query = getEntityManager().createQuery(stmt, AdminBenutzerinfo.class);
		query.setParameter("uuidPart", uuidPart + "%");

		final List<AdminBenutzerinfo> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());
		return trefferliste;
	}

	@Override
	public AdminBenutzerinfo persist(final AdminBenutzerinfo entity) throws PersistenceException {
		throw new UnsupportedOperationException("AdminBenutzerinfo kann nicht gespeichert werden");
	}

	@Override
	public List<AdminBenutzerinfo> persist(final List<AdminBenutzerinfo> entities) throws PersistenceException {
		throw new UnsupportedOperationException("AdminBenutzerinfos können nicht gespeichert werden");
	}

	@Override
	public String delete(final AdminBenutzerinfo entity) throws PersistenceException {
		throw new UnsupportedOperationException("AdminBenutzerinfo kann nicht gelöscht werden");
	}

	@Override
	public void delete(final List<AdminBenutzerinfo> entities) throws PersistenceException {
		throw new UnsupportedOperationException("AdminBenutzerinfos können nicht gelöscht werden");
	}
}
