//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * ILehrerteilnahmeInfoDao
 */
public interface ILehrerteilnahmeInfoDao extends IBaseDao<LehrerteilnahmeInformation> {

	/**
	 * Sucht alle Teilnahmeinformationen zu gegebenem schulkuerzel.
	 *
	 * @param schulkuerzel
	 * @return
	 * @throws MKVException
	 */
	List<LehrerteilnahmeInformation> findAlleMitSchulkuerzel(String schulkuerzel);

	/**
	 * Sucht alle Lehrerteilnahmeinformationen zu dem gegebenen Lehrer.
	 *
	 * @param benutzerId long benutzerId des Lehrers.
	 * @return List
	 * @throws MKVException
	 */
	List<LehrerteilnahmeInformation> findAlleMitBenutzerId(long benutzerId);

	/**
	 * Findet alle Lehrerteilnahmeinfos die zu dem gegebenen Lehrer oder der gegebenen Schule gehören.
	 *
	 * @param benutzerId long die AAS-Benutzer-ID des Lehrers.
	 * @param schulkuerzel String das schulkuerzel darf nicht null sein.
	 * @return List
	 * @throws MKVException
	 */
	List<LehrerteilnahmeInformation> findAlleMitBenutzerIdOderSchulkuerzel(long benutzerId, String schulkuerzel);

	/**
	 * Sucht alle LehrerteilnahmeInformation-Instanzen mit dem gegebenen Jahr und dem gegebenen schulkuerzel.
	 *
	 * @param jahr String das Jahr darf nicht null sein
	 * @param schulkuerzel Strung das Schuulkuerzel darf nicht null sein
	 * @return List
	 * @throws MKVException
	 */
	List<LehrerteilnahmeInformation> findAlleMitJahrUndSchulkuerzel(String jahr, String schulkuerzel);
}
