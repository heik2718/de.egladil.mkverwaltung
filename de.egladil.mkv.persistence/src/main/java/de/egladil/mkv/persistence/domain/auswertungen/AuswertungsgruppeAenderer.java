//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

/**
 * AuswertungsgruppeAenderer
 */
public class AuswertungsgruppeAenderer extends AuswertungsgruppeManipulator {

	private final Auswertungsgruppe auswertungsgruppe;

	/**
	 * AuswertungsgruppeAenderer
	 */
	public AuswertungsgruppeAenderer(final Auswertungsgruppe auswertungsgruppe) {
		super();
		if (auswertungsgruppe == null) {
			throw new IllegalArgumentException("auswertungsgruppe erforderlich");
		}
		this.auswertungsgruppe = auswertungsgruppe;
	}

	public AuswertungsgruppeAenderer name(final String neuerName) {
		super.normalizeAndSetName(neuerName);
		return this;
	}

	public Auswertungsgruppe checkAndChange() {
		pruefeNameValid();
		auswertungsgruppe.setName(getNeuerName());
		return auswertungsgruppe;
	}
}
