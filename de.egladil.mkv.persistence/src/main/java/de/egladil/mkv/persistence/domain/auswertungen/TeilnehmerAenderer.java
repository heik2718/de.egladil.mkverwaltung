//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * TeilnehmerAenderer
 */
public class TeilnehmerAenderer extends TeilnehmerManipulator {

	private final Teilnehmer teilnehmer;

	/**
	 * TeilnehmerAenderer
	 */
	public TeilnehmerAenderer(final Teilnehmer teilnehmer) {
		super();
		if (teilnehmer == null) {
			throw new IllegalArgumentException("teilnehmer erforderlich");
		}
		this.teilnehmer = teilnehmer;
	}

	public TeilnehmerAenderer vorname(final String neuerVorname) {
		this.normalizeAndSetVorname(neuerVorname);
		return this;
	}

	public TeilnehmerAenderer nachname(final String neuerNachname) {
		this.normalizeAndSetNachname(neuerNachname);
		return this;
	}

	public TeilnehmerAenderer zusatz(final String neuerZusatz) {
		this.normalizeAndSetZusatz(neuerZusatz);
		return this;
	}

	public TeilnehmerAenderer eingabemodus(final Antworteingabemodus eingabemodus) {
		this.setEingabemodus(eingabemodus);
		return this;
	}

	public TeilnehmerAenderer sprache(final Sprache sprache) {
		this.setSprache(sprache);
		return this;
	}

	public Teilnehmer checkAndChange() {
		pruefeVornameNachnameValid();
		teilnehmer.setVorname(getVorname());
		teilnehmer.setNachname(getNachname());
		teilnehmer.setZusatz(getZusatz());
		teilnehmer.setEingabemodus(getEingabemodus());
		teilnehmer.setSprache(getSprache());
		return teilnehmer;
	}
}
