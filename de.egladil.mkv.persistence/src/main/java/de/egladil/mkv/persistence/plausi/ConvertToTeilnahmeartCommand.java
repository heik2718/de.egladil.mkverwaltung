//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.plausi;

import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * ConvertToTeilnahmeartCommand
 */
public class ConvertToTeilnahmeartCommand implements ICommand {

	private final String teilnahmeartName;

	private Teilnahmeart teilnahmeart;

	/**
	 * ConvertToTeilnahmeartCommand
	 */
	public ConvertToTeilnahmeartCommand(final String teilnahmeartName) {
		if (teilnahmeartName == null) {
			throw new IllegalArgumentException("teilnahmeartName null");
		}
		this.teilnahmeartName = teilnahmeartName;
	}

	@Override
	public void execute() {
		teilnahmeart = Teilnahmeart.valueOf(teilnahmeartName);
	}

	public final Teilnahmeart getTeilnahmeart() {
		if (teilnahmeart == null) {
			throw new IllegalStateException("IllegalState: please invoke execute() first");
		}
		return teilnahmeart;
	}

}
