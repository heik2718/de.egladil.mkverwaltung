//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.protokoll;

/**
 * Ereignisart
 */
public enum Ereignisart {

	LOGIN("SEC-001"),
	LOGOUT("SEC-002"),
	PASSWORT_GEAENDERT("SEC-003"),
	EMAIL_GEAENDERT("SEC-004"),
	KURZZEITPASSWORT_ANGEFORDERT("SEC-005"),
	DOWNLOAD_OHNE_RECHT("SEC-015"),
	UPLOAD_SIZE("SEC-016"),
	UPLOAD_MIMETYPE("SEC-017"),
	ANONYMISIERT("DAT-001"),
	LAND_ANLEGEN("SEC-006"),
	LAND_AENDERN("SEC-007"),
	ORT_ANLEGEN("SEC-008"),
	ORT_AENDERN("SEC-009"),
	SCHULE_ANLEGEN("SEC-010"),
	SCHULE_AENDERN("SEC-011"),
	ORT_SUCHEN("SEC-012"),
	SCHULE_SUCHEN("SEC-013"),
	FREMDE_DATEN_AENDERN("SEC-014"),
	ANONYMISIERUNG_UNVOLLST("DAT-002"),
	SCHULTEILNAHME_GELOESCHT("DAT-003"),
	UPLOAD_VERARBEITET("DAT-004"),
	TEILNEHMER_GELOESCHT_EINZELN("DAT-005"),
	TEILNEHMER_GELOESCHT_MEHRERE("DAT-006"),
	AUSWERTUNG_GENERIERT_MKV("DAT-007"),
	KLASSE_GELOESCHT("DAT-008"),
	AUSWERTUNGSGRUPPE_GEAENDERT("DAT-009"),
	TEILNEHMER_GEAENDERT("DAT-010");

	private final String kuerzel;

	/**
	 * Erzeugt eine Instanz von EreignisartMapping
	 */
	private Ereignisart(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	/**
	 * Liefert die Membervariable kuerzel
	 *
	 * @return die Membervariable kuerzel
	 */
	public String getKuerzel() {
		return kuerzel;
	}

}
