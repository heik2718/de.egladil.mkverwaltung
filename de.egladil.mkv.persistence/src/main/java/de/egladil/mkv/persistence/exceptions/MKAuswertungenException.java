//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.exceptions;

/**
 * MKAuswertungenException ist eine RuntimeException, die auf Implementierungsfehler hinweist.
 */
public class MKAuswertungenException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von MKAuswertungenException
	 */
	public MKAuswertungenException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von MKAuswertungenException
	 */
	public MKAuswertungenException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von MKAuswertungenException
	 */
	public MKAuswertungenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
