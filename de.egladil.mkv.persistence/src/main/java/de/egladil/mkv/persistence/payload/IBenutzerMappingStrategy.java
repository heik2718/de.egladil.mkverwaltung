package de.egladil.mkv.persistence.payload;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;

public interface IBenutzerMappingStrategy {



	/**
	 * Erzeugt ein Objekt, das alle relevanten Daten des angemeldeten MKV-Benutzers enthält.
	 *
	 * @param benutzer Benutzerkonto der Benutzer in AAS.
	 * @param kontakt IMKVKonto der Benutzer als MKV-User.
	 * @return MKVBenutzer
	 **/
	MKVBenutzer createMKVBenutzer(Benutzerkonto benutzer, IMKVKonto kontakt);
}