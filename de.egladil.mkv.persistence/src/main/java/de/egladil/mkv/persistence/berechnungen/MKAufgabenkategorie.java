/**
 *
 */
package de.egladil.mkv.persistence.berechnungen;

/**
 * @author heike
 */
public enum MKAufgabenkategorie {

	DREI(300),

	VIER(400),

	FUENF(500);

	private final int punkte;

	private MKAufgabenkategorie(final int punkte) {
		this.punkte = punkte;
	}

	public int getPunkte() {
		return punkte;
	}
}