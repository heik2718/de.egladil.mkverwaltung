//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * IPrivatkontaktDao
 */
public interface IPrivatkontoDao extends IMKVKontoDao<Privatkonto> {

}
