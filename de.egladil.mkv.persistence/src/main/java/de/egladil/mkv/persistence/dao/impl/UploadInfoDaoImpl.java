//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * UploadInfoDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class UploadInfoDaoImpl extends BaseDaoImpl<UploadInfo> implements IUploadInfoDao {

	private static final Logger LOG = LoggerFactory.getLogger(UploadInfoDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von UploadInfoDaoImpl
	 */
	@Inject
	public UploadInfoDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IUploadInfoDao#findValidUploadsByStatus(de.egladil.mkv.persistence.domain.enums.UploadStatus)
	 */
	@Override
	public List<UploadInfo> findValidUploadsByStatus(final UploadStatus status) {
		if (status == null) {
			throw new MKVException("status darf nicht null sein");
		}
		final String stmt = "SELECT u from UploadInfo u where u.uploadStatus = :status and u.mimetype <> :mimetype";
		final TypedQuery<UploadInfo> query = getEntityManager().createQuery(stmt, UploadInfo.class);
		query.setParameter("status", status);
		query.setParameter("mimetype", UploadMimeType.UNKNOWN);

		final List<UploadInfo> treffer = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", treffer.size());

		return treffer;
	}

	@Override
	public List<UploadInfo> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		final String stmt = "SELECT u from UploadInfo u where u.teilnahmekuerzel = :teilnahmekuerzel and u.teilnahmeart = :teilnahmeart and u.jahr = :jahr";
		final TypedQuery<UploadInfo> query = getEntityManager().createQuery(stmt, UploadInfo.class);
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());

		final List<UploadInfo> treffer = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", treffer.size());

		return treffer;
	}

	@Override
	public long anzahlUploadInfos(final TeilnahmeIdentifier teilnahmeIdentifier) {
		final String stmt = "select count(l) from UploadInfo l where l.teilnahmeart = :teilnahmeart and l.jahr = :jahr and l.teilnahmekuerzel = :teilnahmekuerzel";
		final TypedQuery<Long> query = getEntityManager().createQuery(stmt, Long.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());

		final Long result = query.getSingleResult();
		LOG.debug("Result: {}", result);
		return result == null ? 0 : result.longValue();

	}

	@Override
	public UploadInfo findByDateiname(final String dateiname) {
		if (dateiname == null) {
			throw new IllegalArgumentException("dateiname null");
		}
		final String stmt = "SELECT u from UploadInfo u where u.dateiname = :dateiname";
		final TypedQuery<UploadInfo> query = getEntityManager().createQuery(stmt, UploadInfo.class);
		query.setParameter("dateiname", dateiname);

		final List<UploadInfo> treffer = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", treffer.size());

		if (treffer.size() > 1) {
			throw new EgladilStorageException("Mehr als ein Upload mit dateiname='" + dateiname + "' vorhanden");
		}

		return treffer.isEmpty() ? null : treffer.get(0);
	}

}
