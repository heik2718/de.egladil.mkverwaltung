//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IAdvTextDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.adv.AdvText;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AdvTextDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class AdvTextDaoImpl extends BaseDaoImpl<AdvText> implements IAdvTextDao {

	/**
	 * AdvTextDaoImpl
	 */
	@Inject
	public AdvTextDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public String delete(final AdvText entity) throws PersistenceException {
		throw new MKVException("AdvTexte duerfen nicht geloescht werden!!!");
	}

	@Override
	public void delete(final List<AdvText> entities) throws PersistenceException {
		throw new MKVException("AdvTexte duerfen nicht geloescht werden!!!");
	}
}
