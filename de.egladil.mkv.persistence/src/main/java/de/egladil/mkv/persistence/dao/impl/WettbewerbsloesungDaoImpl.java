//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;

/**
 * WettbewerbsloesungDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class WettbewerbsloesungDaoImpl extends BaseDaoImpl<Wettbewerbsloesung> implements IWettbewerbsloesungDao {

	private static final Logger LOG = LoggerFactory.getLogger(WettbewerbsloesungDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von WettbewerbsloesungDaoImpl
	 */
	@Inject
	public WettbewerbsloesungDaoImpl(Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao#findByJahr(java.lang.String)
	 */
	@Override
	public List<Wettbewerbsloesung> findByJahr(String wettbewerbsjahr) {
		if (wettbewerbsjahr == null) {
			throw new NullPointerException("wettbewerbsloesung");
		}
		String stmt = "select w from Wettbewerbsloesung w where w.jahr = :jahr";
		TypedQuery<Wettbewerbsloesung> query = getEntityManager().createQuery(stmt, Wettbewerbsloesung.class);
		query.setParameter("jahr", wettbewerbsjahr);

		List<Wettbewerbsloesung> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());
		return trefferliste;
	}

}
