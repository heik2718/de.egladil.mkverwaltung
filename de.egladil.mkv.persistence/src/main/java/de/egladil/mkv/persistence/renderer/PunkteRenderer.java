//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.renderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PunkteRenderer erzeugt die Ausgabe für Punkte im Minikänguru-Wettbewerb. Das ist eine Darstellung einer Dezimalzahl
 * mit 1 oder 2 Nachkommastellen mit Punkt.
 */
public class PunkteRenderer {

	private static final Logger LOG = LoggerFactory.getLogger(PunkteRenderer.class);

	private final int punkte;

	/**
	 * Erzeugt eine Instanz von PunkteRenderer
	 */
	public PunkteRenderer(final int punkte) {
		this.punkte = punkte;
	}

	public String render() {
		if (punkte == 0) {
			return "0";
		}
		final String punkteString = String.valueOf(punkte);
		final int indexKomma = punkteString.length() - 2;
		final String nachkommastellen = punkteString.substring(indexKomma, punkteString.length());
		final String vorkommastellen = punkteString.substring(0, indexKomma);

		final String text = vorkommastellen + "," + nachkommastellen;

		LOG.debug("rein={}, raus={}", punkte, text);
		return text;
	}
}
