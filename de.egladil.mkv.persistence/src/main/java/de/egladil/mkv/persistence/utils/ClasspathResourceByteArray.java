//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * ClasspathResourceByteArray
 */
public class ClasspathResourceByteArray {

	private final String resourcePath;

	private byte[] data;

	/**
	 * ClasspathResourceByteArray
	 */
	public ClasspathResourceByteArray(final String resourcePath) {
		if (StringUtils.isBlank(resourcePath)) {
			throw new IllegalArgumentException("resourcePath blank");
		}
		this.resourcePath = resourcePath;
	}

	public final byte[] getData() {
		return data;
	}

	public final void setData(final byte[] data) {
		this.data = data;
	}

	public final String getResourcePath() {
		return resourcePath;
	}

}
