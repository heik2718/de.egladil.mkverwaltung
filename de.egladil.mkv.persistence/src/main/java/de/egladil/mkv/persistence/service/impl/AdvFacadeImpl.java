//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAdvTextDao;
import de.egladil.mkv.persistence.dao.IAdvVereinbarungDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.adv.AdvText;
import de.egladil.mkv.persistence.domain.adv.AdvTextVersionsnummerSorter;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.payload.request.AdvVereinbarungAntrag;
import de.egladil.mkv.persistence.service.AdvFacade;

/**
 * AdvFacadeImpl
 */
@Singleton
public class AdvFacadeImpl implements AdvFacade {

	private static final String FORMAT_DEFAULT_DATETIME = "dd.MM.yyyy HH:mm:ss";

	private final IAdvTextDao advTextDao;

	private final IAdvVereinbarungDao advVereinbarungDao;

	private final ISchuleDao schuleDao;

	/**
	 * AdvFacadeImpl
	 */
	@Inject
	public AdvFacadeImpl(final IAdvTextDao advTextDao, final IAdvVereinbarungDao advVereinbarungDao, final ISchuleDao schuleDao) {
		this.advTextDao = advTextDao;
		this.advVereinbarungDao = advVereinbarungDao;
		this.schuleDao = schuleDao;
	}

	@Override
	public AdvText getAktuellstenAdvText() {

		final List<AdvText> alleTexte = advTextDao.findAll(AdvText.class);
		if (alleTexte.isEmpty()) {
			return null;
		}
		Collections.sort(alleTexte, new AdvTextVersionsnummerSorter());
		return alleTexte.get(alleTexte.size() - 1);
	}

	@Override
	@Transactional
	public AdvVereinbarung createAdvVereinbarung(final String benutzerUuid, final AdvVereinbarungAntrag antrag) {

		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}
		if (antrag == null) {
			throw new IllegalArgumentException("antrag null");
		}

		final Optional<AdvVereinbarung> optVereinbarung = findAdvVereinbarungFuerSchule(antrag.getSchulkuerzel());
		if (optVereinbarung.isPresent()) {
			return optVereinbarung.get();
		}

		final Optional<Schule> optSchule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			antrag.getSchulkuerzel());

		if (!optSchule.isPresent()) {
			throw new PreconditionFailedException("Keine Schule mit kuerzel " + antrag.getSchulkuerzel() + " vorhanden");
		}

		final AdvText advText = this.getAktuellstenAdvText();

		if (advText == null) {
			throw new PreconditionFailedException("Kein AdvText vorhanden");
		}

		final AdvVereinbarung vereinbarung = new AdvVereinbarung();
		vereinbarung.setSchulkuerzel(antrag.getSchulkuerzel());
		vereinbarung.setSchulname(antrag.getSchulname());
		vereinbarung.setLaendercode(antrag.getLaendercode());
		vereinbarung.setPlz(antrag.getPlz());
		vereinbarung.setOrt(antrag.getOrt());
		vereinbarung.setStrasse(antrag.getStrasse());
		vereinbarung.setHausnummer(antrag.getHausnummer());
		vereinbarung.setAdvText(advText);
		vereinbarung.setZugestimmtDurch(benutzerUuid);

		final SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DEFAULT_DATETIME);
		vereinbarung.setZugestimmtAm(sdf.format(new Date()));

		final AdvVereinbarung persisted = advVereinbarungDao.persist(vereinbarung);

		return persisted;
	}

	@Override
	public Optional<AdvVereinbarung> findAdvVereinbarungFuerSchule(final String schulkuerzel) {

		final Optional<AdvVereinbarung> optVereinbarung = advVereinbarungDao.findBySchulkuerzel(schulkuerzel);
		return optVereinbarung;
	}
}
