//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.UuidString;

/**
 * UuidPayload wrapped einen String, der eine UUID sein muss.
 */
public class UuidPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@UuidString
	@NotNull
	@Size(max = 40)
	private final String code;

	/**
	 * Erzeugt eine Instanz von UuidPayload
	 */
	public UuidPayload(final String code) {
		this.code = code;
	}

	@Override
	public String toBotLog() {
		return code;
	}

	@Override
	public String toString() {
		return code;
	}
}
