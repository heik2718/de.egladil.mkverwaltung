//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.HttpUrl;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * SchulePayload zum Ändern der Schule.
 */
public class SchulePayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@NotNull
	@Size(min = 8, max = 8)
	@Kuerzel
	private String schulkuerzel;

	@HttpUrl
	private String url;

	@StringLatin
	@Size(min = 0, max = 100)
	private String strasse;

	@Honeypot
	private String kleber;

	/**
	 * Erzeugt eine Instanz von SchulePayload
	 */
	public SchulePayload() {
	}

	public SchulePayload(final String schulkuerzel, final String name) {
		this.name = name;
		this.schulkuerzel = schulkuerzel;
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	@Override
	public String toString() {
		return "SchulePayload [schulkuerzel=" + schulkuerzel + ", name=" + name + ", url=" + url + ", strasse=" + strasse
			+ ", kleber=" + kleber + "]";
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Membervariable ortkuerzel
	 *
	 * @return die Membervariable ortkuerzel
	 */
	public String getSchulkuerzel() {
		return schulkuerzel;
	}

	/**
	 * Liefert die Membervariable url
	 *
	 * @return die Membervariable url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param url neuer Wert der Membervariablen url
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Liefert die Membervariable strasse
	 *
	 * @return die Membervariable strasse
	 */
	public String getStrasse() {
		return strasse;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param strasse neuer Wert der Membervariablen strasse
	 */
	public void setStrasse(final String strasse) {
		this.strasse = strasse;
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}
}
