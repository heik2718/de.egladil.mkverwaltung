//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * LehrerteilnahmeInfoDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class LehrerteilnahmeInfoDaoImpl extends BaseDaoImpl<LehrerteilnahmeInformation> implements ILehrerteilnahmeInfoDao {

	private static final Logger LOG = LoggerFactory.getLogger(LehrerteilnahmeInfoDaoImpl.class);

	@Inject
	public LehrerteilnahmeInfoDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao#findByUuid(java.lang.String)
	 */
	@Override
	public List<LehrerteilnahmeInformation> findAlleMitSchulkuerzel(final String schulkuerzel) {
		if (schulkuerzel == null) {
			throw new MKVException("schulkuerzel darf nicht null sein");
		}
		final String stmt = "SELECT i from LehrerteilnahmeInformation i where schulkuerzel = :schulkuerzel";
		final TypedQuery<LehrerteilnahmeInformation> query = getEntityManager().createQuery(stmt, LehrerteilnahmeInformation.class);
		query.setParameter("schulkuerzel", schulkuerzel);

		final List<LehrerteilnahmeInformation> treffer = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", treffer.size());

		return treffer;

	}

	/**
	 * @see de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao#findAlleMitBenutzerId(long)
	 */
	@Override
	public List<LehrerteilnahmeInformation> findAlleMitBenutzerId(final long benutzerId) {
		final String stmt = "SELECT i from LehrerteilnahmeInformation i where benutzerId = :benutzerId";
//		final String stmt = "select id, kuerzel, vorname, nachname, ben_id, email, aktiviert, anonym, mailnachricht, jahr, schule, " +
//"ort, land, last_login, uuid, schulkuerzel, ortkuerzel, landkuerzel from vw_lehrerteilnahmen where ben_id = :benutzerId";
		final TypedQuery<LehrerteilnahmeInformation> query = getEntityManager().createQuery(stmt, LehrerteilnahmeInformation.class);
//		final Query query = getEntityManager().createNativeQuery(stmt, LehrerteilnahmeInformation.class);
		query.setParameter("benutzerId", benutzerId);

		final List<LehrerteilnahmeInformation> treffer = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", treffer.size());

		return treffer;
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao#findAlleMitBenutzerIdOderSchulkuerzel(long,
	 * java.lang.String)
	 */
	@Override
	public List<LehrerteilnahmeInformation> findAlleMitBenutzerIdOderSchulkuerzel(final long benutzerId,
		final String schulkuerzel) {
		if (schulkuerzel == null) {
			throw new MKVException("schulkuerzel darf nicht null sein");
		}
		final String stmt = "SELECT i from LehrerteilnahmeInformation i where benutzerId = :benutzerId or schulkuerzel = :schulkuerzel";
		final TypedQuery<LehrerteilnahmeInformation> query = getEntityManager().createQuery(stmt, LehrerteilnahmeInformation.class);
		query.setParameter("benutzerId", benutzerId);
		query.setParameter("schulkuerzel", schulkuerzel);

		final List<LehrerteilnahmeInformation> treffer = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", treffer.size());

		return treffer;
	}

	/**
	* @see de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao#findAlleMitJahrUndSchulkuerzel(java.lang.String, java.lang.String)
	*/
	@Override
	public List<LehrerteilnahmeInformation> findAlleMitJahrUndSchulkuerzel(final String jahr, final String schulkuerzel) {
		if (jahr == null) {
			throw new MKVException("jahr darf nicht null sein");
		}
		if (schulkuerzel == null) {
			throw new MKVException("schulkuerzel darf nicht null sein");
		}

		final String stmt = "SELECT i from LehrerteilnahmeInformation i where jahr = :jahr and schulkuerzel = :schulkuerzel";
		final TypedQuery<LehrerteilnahmeInformation> query = getEntityManager().createQuery(stmt, LehrerteilnahmeInformation.class);
		query.setParameter("jahr", jahr);
		query.setParameter("schulkuerzel", schulkuerzel);

		final List<LehrerteilnahmeInformation> treffer = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", treffer.size());

		return treffer;
	}

	@Override
	public LehrerteilnahmeInformation persist(final LehrerteilnahmeInformation entity) throws PersistenceException {
		throw new UnsupportedOperationException("LehrerteilnahmeInformation ist readonly");
	}

	@Override
	public List<LehrerteilnahmeInformation> persist(final List<LehrerteilnahmeInformation> entities) throws PersistenceException {
		throw new UnsupportedOperationException("LehrerteilnahmeInformation ist readonly");
	}

	@Override
	public String delete(final LehrerteilnahmeInformation entity) throws PersistenceException {
		throw new UnsupportedOperationException("LehrerteilnahmeInformation ist readonly");
	}

	@Override
	public void delete(final List<LehrerteilnahmeInformation> entities) throws PersistenceException {
		throw new UnsupportedOperationException("LehrerteilnahmeInformation ist readonly");
	}

}
