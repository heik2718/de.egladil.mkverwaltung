package de.egladil.mkv.persistence.berechnungen;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotNull;

import de.egladil.mkv.persistence.annotations.Wertungscode;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Wertung;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;

public abstract class AbstractBerechnePunkteCommand implements IBerechnePunkteCommand {

	public static IBerechnePunkteCommand createCommand(final Klassenstufe klassenstufe) {
		switch (klassenstufe) {
		case EINS:
			return new BerechnePunkteCommandValidationDecorator(new BerechnePunkteKlasseEinsCommand(),
				klassenstufe.getAnzahlAufgaben());
		case ZWEI:
			return new BerechnePunkteCommandValidationDecorator(new BerechnePunkteKlasseZweiCommand(),
				klassenstufe.getAnzahlAufgaben());
		case IKID:
			return new BerechnePunkteCommandValidationDecorator(new BerechnePunkteInklusionCommand(),
				klassenstufe.getAnzahlAufgaben());
		default:
			throw new MKAuswertungenException(
				"keine Implementierung von IBerechnePunkteCommand für Klassenstufe " + klassenstufe + " vorhanden");
		}
	}

	private final Map<Integer, AufgabenkategorieDekorator> indexAufgabenkategorien;

	private int guthaben;

	protected AbstractBerechnePunkteCommand() {
		indexAufgabenkategorien = new HashMap<>();
		init(indexAufgabenkategorien);
	}

	/**
	 *
	 * Initialisiert die Instanz.
	 *
	 * @param indexAufgabenkategorien Map darf nicht null sein!
	 * @throws NullPointerException wenn parameter null
	 */
	protected abstract void init(Map<Integer, AufgabenkategorieDekorator> indexAufgabenkategorien);

	/**
	 * @see de.egladil.mkv.persistence.berechnungen.IBerechnePunkteCommand#berechne(java.lang.String)
	 */
	@Override
	public int berechne(@Wertungscode @NotNull final String wertungscode) {
		final char[] chars = wertungscode.toCharArray();
		int summe = guthaben;
		for (int index = 0; index < wertungscode.length(); index++) {
			final AufgabenkategorieDekorator kategorie = indexAufgabenkategorien.get(Integer.valueOf(index));
			if (kategorie == null) {
				throw new MKAuswertungenException("keine AufgabenkategorieDekorator zu index " + index + " vorhanden");
			}
			final Wertung wertung = Wertung.valueOfStringIgnoringCase(String.valueOf(chars[index]));
			final IWertungRechnerStrategie wertungRechnerStrategie = AbstractWertungRechnerStrategie
				.createWertungRechnerStrategie(wertung, kategorie);
			summe = wertungRechnerStrategie.applyToSumme(summe);
		}
		return summe;
	}

	protected void setGuthaben(final int guthaben) {
		this.guthaben = guthaben;
	}
}