//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import de.egladil.mkv.persistence.domain.BooleanToIntegerConverter;

/**
 * AbstractMailempfaenger
 */
@Entity
@Table(name = "vw_emails_privat")
public class PrivatmailEmpfaenger implements IMkvUser {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "VORNAME")
	private String vorname;

	@Column(name = "NACHNAME")
	private String nachname;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "JAHR")
	private String jahr;

	@Column(name = "AKTIVIERT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean aktiviert;

	@Column(name = "GESPERRT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean gesperrt;

	@Column(name = "MAILNACHRICHT")
	@Convert(converter = BooleanToIntegerConverter.class)
	private boolean mailnachricht;

	@Override
	public String toString() {
		return "Privatmailempfaenger [vorname=" + vorname + ", nachname=" + nachname + ", email=" + email + ", jahr=" + jahr
			+ ", aktiviert=" + aktiviert + ", gesperrt=" + gesperrt + ", mailnachricht=" + mailnachricht + "]";
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public String getVorname() {
		return vorname;
	}

	@Override
	public String getNachname() {
		return nachname;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public String getJahr() {
		return jahr;
	}

	@Override
	public boolean isAktiviert() {
		return aktiviert;
	}

	@Override
	public boolean isGesperrt() {
		return gesperrt;
	}

	@Override
	public boolean isMailnachricht() {
		return mailnachricht;
	}
}
