//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import de.egladil.mkv.persistence.berechnungen.AbstractBerechnePunkteCommand;
import de.egladil.mkv.persistence.berechnungen.FindKaengurusprungCommand;
import de.egladil.mkv.persistence.berechnungen.IBerechnePunkteCommand;
import de.egladil.mkv.persistence.berechnungen.WertungscodeCommand;
import de.egladil.mkv.persistence.converter.AntwortcodeConverter;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * CreateLoesungszettelRohdatenCommand
 */
public class CreateLoesungszettelRohdatenCommand implements ICommand {

	private final PublicTeilnehmer teilnehmerPayload;

	private final Wettbewerbsloesung wettbewerbsloesung;

	private LoesungszettelRohdaten loesungszettelRohdaten;

	/**
	 * CreateLoesungszettelRohdatenCommand
	 *
	 * @param teilnehmerPayload PublicTeilnehmer darf nicht null sein
	 * @param wettbewerbsloesung Wettbewerbsloesung darf nicht null sein
	 */
	public CreateLoesungszettelRohdatenCommand(final PublicTeilnehmer teilnehmerPayload,
		final Wettbewerbsloesung wettbewerbsloesung) {
		if (teilnehmerPayload == null) {
			throw new IllegalArgumentException("teilnehmerPayload null");
		}
		if (wettbewerbsloesung == null) {
			throw new IllegalArgumentException("wettbewerbsloesung null");
		}
		this.teilnehmerPayload = teilnehmerPayload;
		this.wettbewerbsloesung = wettbewerbsloesung;
	}

	@Override
	public void execute() {
		final String nutzereingabe = getNutzereingabe();
		final String antwortcode = new AntwortcodeConverter().fromAntwortbuchstaben(teilnehmerPayload.getAntworten());
		final String berechneterWertungscode = new WertungscodeCommand(wettbewerbsloesung.getLoesungscode())
			.berechneWertungscode(antwortcode);
		final int kaengurusprung = new FindKaengurusprungCommand().findKaengurusprung(berechneterWertungscode);
		final Klassenstufe klassenstufe = Klassenstufe.valueOf(teilnehmerPayload.getKlassenstufe().getName());
		final IBerechnePunkteCommand berechnePunkteCommand = AbstractBerechnePunkteCommand.createCommand(klassenstufe);
		final int punkte = berechnePunkteCommand.berechne(berechneterWertungscode);
		loesungszettelRohdaten = new LoesungszettelRohdaten.Builder(Auswertungsquelle.ONLINE, klassenstufe, kaengurusprung, punkte,
			berechneterWertungscode, nutzereingabe).antwortcode(antwortcode).build();
	}

	String getNutzereingabe() {
		final StringBuffer sb = new StringBuffer();
		for (final PublicAntwortbuchstabe antwortPayload : teilnehmerPayload.getAntworten()) {
			sb.append(antwortPayload.getBuchstabe());
		}
		return sb.toString();
	}

	public final LoesungszettelRohdaten getLoesungszettelRohdaten() {
		if (loesungszettelRohdaten == null) {
			throw new IllegalStateException("command must be executed before: please call command.execute()");
		}
		return loesungszettelRohdaten;
	}

}
