//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

/**
 * CheckDownloadPrivatFreigabeCommand
 */
public class CheckDownloadPrivatFreigabeCommand extends AbstractCheckDownloadFreigabeCommand {

	private static final String FREISCHALTUNG_MARKER = "privat.txt";

	/**
	 * Erzeugt eine Instanz von CheckDownloadPrivatFreigabeCommand
	 */
	public CheckDownloadPrivatFreigabeCommand(String downloadPath) {
		super(downloadPath);
	}

	@Override
	protected String getMarkerFileName() {
		return FREISCHALTUNG_MARKER;
	}

}
