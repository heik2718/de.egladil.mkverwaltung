//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * SchulteilnahmeReadOnlyDao
 */
@Singleton
@MKVPersistenceUnit
public class SchulteilnahmeReadOnlyDao extends BaseDaoImpl<SchulteilnahmeReadOnly> implements ISchulteilnahmeReadOnlyDao {

	private static final Logger LOG = LoggerFactory.getLogger(SchulteilnahmeReadOnlyDao.class);

	/**
	 * SchulteilnahmeReadOnlyDao
	 */
	@Inject
	public SchulteilnahmeReadOnlyDao(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<SchulteilnahmeReadOnly> findBySchulkuerzelUndJahr(final String schulkuerzel, final String jahr) {

		if (schulkuerzel == null) {
			throw new IllegalArgumentException("schulkuerzel null");
		}
		if (jahr == null) {
			throw new IllegalArgumentException("jahr null");
		}
		final String stmt = "select t from SchulteilnahmeReadOnly t where t.schulkuerzel = :schulkuerzel and t.jahr = :jahr";
		final TypedQuery<SchulteilnahmeReadOnly> query = getEntityManager().createQuery(stmt, SchulteilnahmeReadOnly.class);
		query.setParameter("schulkuerzel", schulkuerzel);
		query.setParameter("jahr", jahr);

		final List<SchulteilnahmeReadOnly> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		if (trefferliste.isEmpty()) {
			return Optional.empty();
		}

		if (trefferliste.size() > 1) {
			throw new MKVException(
				"Mehr als eine Schulteilnahme mit schulkuerzel=" + schulkuerzel + " und jahr=" + jahr + " gefunden");

		}
		return Optional.of(trefferliste.get(0));

	}

	@Override
	public List<SchulteilnahmeReadOnly> findBySchulkuerzel(final String schulkuerzel) {
		if (schulkuerzel == null) {
			throw new IllegalArgumentException("schulkuerzel null");
		}
		final String stmt = "select t from SchulteilnahmeReadOnly t where t.schulkuerzel = :schulkuerzel";
		final TypedQuery<SchulteilnahmeReadOnly> query = getEntityManager().createQuery(stmt, SchulteilnahmeReadOnly.class);
		query.setParameter("schulkuerzel", schulkuerzel);

		final List<SchulteilnahmeReadOnly> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	@Override
	public SchulteilnahmeReadOnly persist(final SchulteilnahmeReadOnly entity) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

	@Override
	public List<SchulteilnahmeReadOnly> persist(final List<SchulteilnahmeReadOnly> entities) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

	@Override
	public String delete(final SchulteilnahmeReadOnly entity) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

	@Override
	public void delete(final List<SchulteilnahmeReadOnly> entities) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}
}
