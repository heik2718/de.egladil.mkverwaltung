//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

/**
 * DownloadArt
 */
public enum DownloadArt {

	STATISTIK_LOESUNGSZETTEL,
	STATISTIK_SCHULE,
	URKUNDE

}
