//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import javax.validation.constraints.NotNull;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.mkv.persistence.validation.ValidSchulzuordnungAendernAnfrage;

/**
 * SchulzuordnungAendernPayload
 */
@ValidSchulzuordnungAendernAnfrage
public class SchulzuordnungAendernPayload implements ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Kuerzel
	private String kuerzelAlt;

	@NotNull
	@Kuerzel
	private String kuerzelNeu;

	private boolean anmelden = false;

	/**
	 * Erzeugt eine Instanz von SchulzuordnungAendernPayload
	 */
	public SchulzuordnungAendernPayload() {
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("SchulzuordnungAendernPayload [kuerzelAlt=");
		builder.append(kuerzelAlt);
		builder.append(", kuerzelNeu=");
		builder.append(kuerzelNeu);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	public boolean isAnmelden() {
		return anmelden;
	}

	public void setAnmelden(final boolean anmelden) {
		this.anmelden = anmelden;
	}

	public String getKuerzelAlt() {
		return kuerzelAlt;
	}

	public void setKuerzelAlt(final String kuerzelAlt) {
		this.kuerzelAlt = kuerzelAlt;
	}

	public String getKuerzelNeu() {
		return kuerzelNeu;
	}

	public void setKuerzelNeu(final String kuerzelNeu) {
		this.kuerzelNeu = kuerzelNeu;
	}

}
