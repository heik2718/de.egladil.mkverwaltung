package de.egladil.mkv.persistence.service;

import java.util.List;
import java.util.Optional;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

public interface TeilnehmerService {

	/**
	 * Erzeugt einen neuen Teilnehmer mit oder ohne Lösungszettel.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param teilnehmerPayload TeilnehmerPayload
	 * @return Teilnehmer
	 */
	Teilnehmer createTeilnehmer(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier, PublicTeilnehmer teilnehmerPayload);

	/**
	 * Erzeugt einen neuen Teilnehmer mit oder ohne Lösungszettel.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param teilnehmerPayload TeilnehmerPayload
	 * @return Teilnehmer
	 */
	Teilnehmer updateTeilnehmer(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier, PublicTeilnehmer teilnehmerPayload)
		throws IllegalArgumentException, PreconditionFailedException, EgladilDuplicateEntryException, EgladilStorageException;

	/**
	 * Der durch das kuerzel spezifizierte Teilnehmer wird gelöscht.
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers.
	 * @param kuerzel String das Teilnehmerkürzel.
	 * @return Optional
	 */
	Optional<Teilnehmer> deleteTeilnehmer(String benutzerUuid, String kuerzel);

	/**
	 * Alle zur durch teilnahmeIdentifier identifizierte Teilnahme gehörenden Teilnehmer werden gelöscht.
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers.
	 * @param teilnahmeIdentifier TeilnahmeIdentifier zur Protokollierung
	 * @param teilnehmer List
	 */
	void deleteAllTeilnehmer(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier, List<Teilnehmer> teilnehmer);

}