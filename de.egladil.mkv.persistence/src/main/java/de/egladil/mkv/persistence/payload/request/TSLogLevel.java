// =====================================================
// Project: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
// =====================================================
package de.egladil.mkv.persistence.payload.request;

/**
 * TSLogLevel sind die Loglevels, die aus meinem Angular-LogService kommen.
 */
public enum TSLogLevel {

	All, Debug, Info, Warn, Error, Fatal, Off;

	public static TSLogLevel valueOfInt(String levelStr) {

		int level = 6;
		try {
			level = Integer.parseInt(levelStr);

		} catch (NumberFormatException e) {
			System.err.println("nicht numerisches LogLevel vom Client bekommen: " + levelStr);
		}

		switch (level) {
		case 0:
			return All;
		case 1:
			return Debug;
		case 2:
			return Info;
		case 3:
			return Warn;
		case 4:
			return Error;
		case 5:
			return Fatal;
		default:
			return Off;
		}
	}

}
