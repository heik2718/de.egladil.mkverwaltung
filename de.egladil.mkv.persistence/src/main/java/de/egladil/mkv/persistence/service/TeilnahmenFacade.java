//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import java.util.List;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.PrivatteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;

/**
 * TeilnahmenFacade
 */
public interface TeilnahmenFacade {

	/**
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers.
	 * @return List
	 */
	List<PublicTeilnahme> getAllTeilnahmen(Benutzerkonto benutzerkonto);

	/**
	 *
	 * @param schulkuerzel
	 * @return List
	 */
	List<SchulteilnahmeReadOnly> getSchulteilnahmen(String schulkuerzel);

	/**
	 * Ermittelt die Anzahl der Lösungszettel zu gegebener Teilnahme.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return int
	 */
	int getAnzahlLoesungszettel(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Ermittelt die Anzahl der Kinder zu gegebener Teilnahme. Diese kann nur im aktuellen Wettbewerbsjahr größer 0
	 * sein.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return int
	 */
	int getAnzahlKinder(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Ermittelt die Anzahl der Uploads zu gegebener Teilnahme.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return int
	 */
	int getAnzahlUploads(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 *
	 * @param schulkuerzel
	 * @return
	 */
	int getAnzahlSchulteilnahmen(String schulkuerzel);

	/**
	 * Ermittelt die Anzahl der Schulteilnahmen mit gegebenen Landkuerzeln.
	 *
	 * @param landkuerzel List von landkuerzeln. Das ermöglicht, die Anzhal von Schulteilnahmen zu Ländergruppen zu
	 * ermitteln.
	 * @param jahr String das Jahr
	 * @return int
	 */
	int getAnzahlSchulteilnahmenLand(List<String> landkuerzel, String jahr);

	/**
	 * Ermittelt die Anzahl der Lösungszettel von Schulteilnehmern.
	 *
	 * @param jahr String wenn null, dann alle Jahre.
	 * @param klassenstufe Klassenstufe wenn null, dann alle Klassenstufen
	 * @param landkuerzel List von Länderkürzeln wenn leer, dann alle Länder.
	 * @return int
	 */
	int getAnzahlLoesungszettelSchulteilnahmen(String jahr, Klassenstufe klassenstufe, List<String> landkuerzel);

	/**
	 * Ermittelt die Anzahl der Lösungszettel von Privatteilnehmern.
	 *
	 * @param jahr String wenn null, dann alle Jahre.
	 * @param klassenstufe Klassenstufe wenn null, dann alle Klassenstufen
	 * @return int
	 */
	int getAnzahlLoesungszettelPrivatteilnahmen(String jahr, Klassenstufe klassenstufe);

	/**
	 * Gibt alle Lösungszettel der gegebenen Klassenstufe im gegebenen Jahr zurück.
	 *
	 * @param jahr String
	 * @param klassenstufe
	 * @return List
	 */
	List<Loesungszettel> findLoesungszettelByJahrUndKlassenstufe(String jahr, Klassenstufe klassenstufe);

	/**
	 * Ermittelt alle Lösungszettel von Schulteilnahmen, die zu Ländern in der kuerzelliste gehören.
	 *
	 * @param jahr String das Wettbewerbsjahr
	 * @param klassenstufe Klassenstufe die Klassenstufe
	 * @param kuerzelliste List
	 * @return List
	 */
	List<Loesungszettel> findLoesungszettelSchulen(String jahr, Klassenstufe klassenstufe, List<String> kuerzelliste);

	/**
	 * Ermittelt alle Lösungszettel zu Privatteilnahmen.
	 *
	 * @param jahr String das Wettbewerbsjahr
	 * @param klassenstufe Klassenstufe die Klassenstufe
	 * @return List
	 */
	List<Loesungszettel> findLoesungszettelPrivat(String jahr, Klassenstufe klassenstufe);

	/**
	 * @return List
	 */
	List<String> getAuswertbareJahre();

	/**
	 * Sucht alle Teilnahmejahre für das durch das kuerzel gegebene Land.
	 *
	 * @param kuerzel
	 * @return
	 */
	List<String> findTeilnahmejahreLand(String kuerzel);

	/**
	 * Gibt eine Liste aller Privatteilnahmen in einem gegebenen Jahr zurück.
	 *
	 * @param jahr String darf nicht blank sein.
	 * @return List
	 */
	List<PrivatteilnahmeInformation> getAllPrivatteilnahmeInfosZuJahr(String jahr);

	/**
	 *
	 * @param lehrerkontoId
	 * @return
	 */
	int getAnzahlSchulteilnahmenForLehrer(final Long lehrerkontoId);

	/**
	 * Diese Methode wird sowohl beim Login als auch beim Laden der Lehrerteilnahmen benötigt. Daher ist die Signatur
	 * etwas sperrig.
	 *
	 * @param lehrerkonto Lehrerkonto
	 * @param aktuelleSchule PublicSchule
	 * @param alleAktuellenTeilnahmeinfos List<LehrerteilnahmeInformation> nur zum aktuellen Wettbewerbsjahr.
	 * @return {@link PublicTeilnahme} oder null
	 */
	PublicTeilnahme sammleDatenAktuelleLehrerteilnahme(final Lehrerkonto lehrerkonto, final PublicSchule aktuelleSchule,
		final List<LehrerteilnahmeInformation> alleAktuellenTeilnahmeinfos);
}
