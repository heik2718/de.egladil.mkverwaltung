//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

/**
 * IBerechnePunkteCommand ist ein Command, das für einen Wertungscode die Punkte berechnet. Eine Implementierung erhält
 * man über die factory method AbstractBerechnePunkteCommand.create.
 */
public interface IBerechnePunkteCommand {

	/**
	 * Berechnet die Punkte für den gegebenen Wertungscode.
	 *
	 * @param wertungscode String aus f,r und n ohne Kommas. Darf nicht null sein.
	 * @return int die kommafreie Variante der Punkte (also 100 x Punkte).
	 * @throws NullPointerException wenn wertungscode null
	 * @throws IndexOutOfBounds wenn wertungscode nicht die korrekte Länge hat.
	 */
	int berechne(String wertungscode);
}
