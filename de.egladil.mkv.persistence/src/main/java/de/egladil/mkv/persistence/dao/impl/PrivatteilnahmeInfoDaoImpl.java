//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.teilnahmen.PrivatteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * PrivatteilnahmeInfoDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class PrivatteilnahmeInfoDaoImpl extends BaseDaoImpl<PrivatteilnahmeInformation> implements IPrivatteilnahmeInfoDao {

	private static final Logger LOG = LoggerFactory.getLogger(PrivatteilnahmeInfoDaoImpl.class);

	/**
	 * PrivatteilnahmeInfoDaoImpl
	 */
	@Inject
	public PrivatteilnahmeInfoDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<PrivatteilnahmeInformation> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier)
		throws EgladilStorageException {

		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		try {
			final String stmt = "select t from PrivatteilnahmeInformation t where t.kuerzel = :kuerzel and t.jahr = :jahr";
			final TypedQuery<PrivatteilnahmeInformation> query = getEntityManager().createQuery(stmt,
				PrivatteilnahmeInformation.class);
			query.setParameter("kuerzel", teilnahmeIdentifier.getKuerzel());
			query.setParameter("jahr", teilnahmeIdentifier.getJahr());

			final List<PrivatteilnahmeInformation> trefferliste = query.getResultList();
			LOG.debug("Anzahl Treffer: {}", trefferliste.size());

			if (trefferliste.size() > 1) {
				throw new EgladilStorageException(
					"Datenfehler: mehr als eine Privatteilnahme mit " + teilnahmeIdentifier.toString() + " vorhanden");
			}
			return trefferliste.isEmpty() ? Optional.empty() : Optional.of(trefferliste.get(0));
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwarteter Fehler beim Suchen der Privatteilnahme mit  "
				+ teilnahmeIdentifier.toString() + ": " + e.getMessage(), e);
		}
	}

	@Override
	public List<PrivatteilnahmeInformation> findByJahr(final String jahr) {

		if (StringUtils.isBlank(jahr)) {
			throw new IllegalArgumentException("jahr blank");
		}

		try {
			final String stmt = "select t from PrivatteilnahmeInformation t where t.jahr = :jahr";
			final TypedQuery<PrivatteilnahmeInformation> query = getEntityManager().createQuery(stmt,
				PrivatteilnahmeInformation.class);
			query.setParameter("jahr", jahr);

			final List<PrivatteilnahmeInformation> trefferliste = query.getResultList();
			LOG.debug("Anzahl Treffer: {}", trefferliste.size());

			return trefferliste;
		} catch (final Exception e) {
			throw new EgladilStorageException(
				"Unerwarteter Fehler beim Suchen der Privatteilnahmen im Jahr  " + jahr + ": " + e.getMessage(), e);
		}
	}

	@Override
	public PrivatteilnahmeInformation persist(final PrivatteilnahmeInformation entity) throws PersistenceException {

		throw new EgladilStorageException("PrivatteilnahmeInformation ist ein view");
	}

	@Override
	public List<PrivatteilnahmeInformation> persist(final List<PrivatteilnahmeInformation> entities) throws PersistenceException {

		throw new EgladilStorageException("PrivatteilnahmeInformation ist ein view");
	}

	@Override
	public String delete(final PrivatteilnahmeInformation entity) throws PersistenceException {
		throw new EgladilStorageException("PrivatteilnahmeInformation ist ein view");
	}

	@Override
	public void delete(final List<PrivatteilnahmeInformation> entities) throws PersistenceException {

		throw new EgladilStorageException("PrivatteilnahmeInformation ist ein view");
	}

}
