//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * PublicSchule enthält die Attribute einer Schule, die für nicht autorisierte Nutzer zugänglich sind.
 */
public class PublicSchule {

	@JsonProperty
	private int level = 2;

	@JsonProperty
	private String name;

	@JsonProperty
	private String kuerzel;

	@JsonProperty
	private SchuleLage lage;

	@JsonProperty
	private String schultyp;

	@JsonProperty
	private String strasse;

	@JsonProperty
	private String url;

	@JsonProperty
	private int anzahlTeilnahmen;

	@JsonProperty
	private HateoasPayload hateoasPayload;

	@Deprecated
	public static PublicSchule create(final Schule schule) {
		if (schule == null) {
			throw new MKVException("schule darf nicht null sein");
		}
		return new PublicSchule(schule.getName(), schule.getKuerzel());
	}

	/**
	 *
	 * @param schule
	 * @param theLage
	 * @return
	 */
	public static PublicSchule create(final Schule schule, final SchuleLage theLage) {

		if (schule == null) {
			throw new IllegalArgumentException("schule null");
		}
		if (theLage == null) {
			throw new IllegalArgumentException("theLage null");
		}

		final PublicSchule ps = new PublicSchule();
		ps.setUrl(schule.getUrl());
		final String schulkuerzel = schule.getKuerzel();
		ps.setKuerzel(schulkuerzel);
		ps.setName(schule.getName());
		ps.setSchultyp(schule.getSchultyp());
		ps.setStrasse(schule.getStrasse());
		ps.setLage(theLage);
		ps.setAnzahlTeilnahmen(0);
		final HateoasPayload hateoasPayload = new HateoasPayload(schulkuerzel, "/schulen/" + schulkuerzel);
		{
			final HateoasLink link = new HateoasLink("/orte/" + theLage.getOrtkuerzel(), "Ort", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/laender/" + theLage.getLandkuerzel(), "Land", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/schulen/" + schulkuerzel, "Schule ändern", HttpMethod.PUT,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/schulen/" + schulkuerzel + "/adv", "ADV", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/schulen/" + schulkuerzel + "/teilnahmen", "Teilnahmen", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		ps.setHateoasPayload(hateoasPayload);
		return ps;
	}

	/**
	 * Mapped SchuleReadOnly zu PublicSchule
	 *
	 * @param schule SchuleReadOnly darf nicht null sein
	 * @return PublicSchule
	 */
	public static PublicSchule create(final SchuleReadOnly schule) {
		if (schule == null) {
			throw new IllegalArgumentException("schule null");
		}
		final PublicSchule ps = new PublicSchule();
		ps.setUrl(schule.getUrl());
		final String schulkuerzel = schule.getKuerzel();
		ps.setKuerzel(schulkuerzel);
		ps.setName(schule.getName());
		ps.setSchultyp(schule.getSchultyp());
		ps.setStrasse(schule.getStrasse());
		final SchuleLage theLage = new SchuleLage(schule.getLandKuerzel(), schule.getLandName(), schule.getOrtKuerzel(),
			schule.getOrtName());
		ps.setLage(theLage);
		final HateoasPayload hateoasPayload = new HateoasPayload(schulkuerzel, "/schulen/" + schulkuerzel);
		{
			final HateoasLink link = new HateoasLink("/orte/" + theLage.getOrtkuerzel(), "Ort", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/laender/" + theLage.getLandkuerzel(), "Land", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/schulen/" + schulkuerzel, "Schule ändern", HttpMethod.PUT,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		ps.setHateoasPayload(hateoasPayload);
		return ps;
	}

	/**
	 * Erzeugt eine Instanz von PublicSchule
	 */
	public PublicSchule() {
	}

	/**
	 * Erzeugt eine Instanz von PublicSchule
	 */
	public PublicSchule(final String name, final String kuerzel) {
		this.name = name;
		this.kuerzel = kuerzel;
	}

	public void addHateoasLink(final HateoasLink link) {
		if (link != null && this.hateoasPayload != null) {
			this.hateoasPayload.addLink(link);
		}
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PublicSchule other = (PublicSchule) obj;
		if (kuerzel == null) {
			if (other.kuerzel != null)
				return false;
		} else if (!kuerzel.equals(other.kuerzel))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param name neuer Wert der Membervariablen name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Liefert die Membervariable kuerzel
	 *
	 * @return die Membervariable kuerzel
	 */
	public String getKuerzel() {
		return kuerzel;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kuerzel neuer Wert der Membervariablen kuerzel
	 */
	public void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicSchule [name=");
		builder.append(name);
		builder.append(", kuerzel=");
		builder.append(kuerzel);
		builder.append("]");
		return builder.toString();
	}

	public final SchuleLage getLage() {
		return lage;
	}

	public final void setLage(final SchuleLage lage) {
		this.lage = lage;
	}

	public final void setSchultyp(final String schultyp) {
		this.schultyp = schultyp;
	}

	public final void setStrasse(final String strasse) {
		this.strasse = strasse;
	}

	public final void setUrl(final String externeUrl) {
		this.url = externeUrl;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}

	public final HateoasPayload getHateoasPayload() {
		return hateoasPayload;
	}

	public final int getAnzahlTeilnahmen() {
		return anzahlTeilnahmen;
	}

	public final void setAnzahlTeilnahmen(final int anzahlTeilnahmen) {
		this.anzahlTeilnahmen = anzahlTeilnahmen;
	}
}
