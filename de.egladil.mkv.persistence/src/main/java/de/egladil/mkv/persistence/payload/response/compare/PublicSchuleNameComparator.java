//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;

/**
 * PublicSchuleNameComparator
 */
public class PublicSchuleNameComparator implements Comparator<PublicSchule> {

	private final SchuleLageLandOrtComparator lageComparator = new SchuleLageLandOrtComparator();

	@Override
	public int compare(final PublicSchule arg0, final PublicSchule arg1) {
		final String name0 = arg0.getName().toLowerCase();
		final String name1 = arg1.getName().toLowerCase();

		if (name0.equals(name1)) {
			final SchuleLage lage0 = arg0.getLage();
			final SchuleLage lage1 = arg1.getLage();

			if (lage0 == null && lage1 == null) {
				return 0;
			}
			if (lage0 == null && lage1 != null) {
				return -1;
			}
			if (lage0 != null && lage1 == null) {
				return 1;
			}

			return lageComparator.compare(lage0, lage1);
		}

		return name0.compareTo(name1);
	}

}
