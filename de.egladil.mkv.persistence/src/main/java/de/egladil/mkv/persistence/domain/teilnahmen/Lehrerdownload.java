//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import de.egladil.mkv.persistence.domain.user.Lehrerkonto;

/**
 * Download
 */
@Entity
@Table(name = "lehrerdownloads")
public class Lehrerdownload implements Serializable, IDownload {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@NotNull
	@Embedded
	private Downloaddaten downloaddaten;

	@ManyToOne()
	@JoinColumn(name = "KONTO")
	private Lehrerkonto lehrerkonto;

	@Version
	@Column(name = "VERSION")
	private int version;

	@Override
	public String toString() {
		return "Lehrerdownload [downloaddaten=" + downloaddaten + "]";
	}

	/**
	 * @see de.egladil.mkv.persistence.domain.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Downloaddaten getDownloaddaten() {
		return downloaddaten;
	}

	@Override
	public void setDownloaddaten(final Downloaddaten downloaddaten) {
		this.downloaddaten = downloaddaten;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public Lehrerkonto getLehrerkonto() {
		return lehrerkonto;
	}

	public void setLehrerkonto(final Lehrerkonto lehrerkonto) {
		this.lehrerkonto = lehrerkonto;
	}
}
