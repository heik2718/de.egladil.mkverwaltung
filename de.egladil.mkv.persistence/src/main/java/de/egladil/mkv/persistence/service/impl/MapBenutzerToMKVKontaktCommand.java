//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.Optional;

import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IdentifierMKVKontoProvider;

/**
 * MapBenutzerToMKVKontaktCommand
 */
public class MapBenutzerToMKVKontaktCommand {

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final String uuid;

	/**
	 * MapBenutzerToMKVKontaktCommand
	 */
	public MapBenutzerToMKVKontaktCommand(final ILehrerkontoDao lehrerkontoDao, final IPrivatkontoDao privatkontoDao,
		final String uuid) {

		if (uuid == null) {
			throw new IllegalArgumentException("uuid null");
		}

		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.uuid = uuid;
	}

	/**
	 * @return IMKVKonto
	 * @throws MKVException falls nicht gefunden.
	 */
	public IMKVKonto findMKVKonto() throws MKVException {
		final Optional<IMKVKonto> opt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(uuid, lehrerkontoDao,
			privatkontoDao);
		if (!opt.isPresent()) {
			throw new MKVException("kein Privat- oder Lehrerkonto mit UUID [" + uuid + "] bekannt");
		}
		final IMKVKonto kontakt = opt.get();
		return kontakt;
	}
}
