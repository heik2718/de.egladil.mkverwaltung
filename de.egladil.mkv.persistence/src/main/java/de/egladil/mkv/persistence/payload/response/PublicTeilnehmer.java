//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.mkv.persistence.converter.AntwortcodeConverter;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;

/**
 * PublicTeilnehmer
 */
public class PublicTeilnehmer implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	@Kuerzel
	@Size(max = 22)
	private String kuerzel;

	@JsonProperty
	private String teilnahmeart;

	@JsonProperty
	private String teilnahmekuerzel;

	@JsonProperty
	private String jahr;

	@JsonProperty
	private PublicKlassenstufe klassenstufe;

	@NotNull
	@JsonProperty
	private PublicSprache sprache;

	@JsonProperty
	@StringLatin
	@NotBlank
	@Size(max = 55)
	private String vorname;

	@JsonProperty
	@StringLatin
	@Size(max = 55)
	private String nachname;

	@StringLatin
	@Size(max = 55)
	@JsonProperty
	private String zusatz;

	@JsonProperty
	private String punkte;

	@JsonProperty
	private Integer kaengurusprung;

	@JsonProperty
	private PublicAntwortbuchstabe[] antworten;

	@JsonProperty
	@Kuerzel
	@Size(max = 22)
	private String loesungszettelKuerzel;

	@JsonProperty
	private PublicAuswertungsgruppe auswertungsgruppe;

	@JsonProperty
	private String eingabemodus;

	/**
	 * Erzeugt Teilnehmer ohne Lösungszettel, aber mit den Daten für einen Löungszettel.
	 *
	 * @param teilnehmer Teilnehmer
	 * @return PublicTeilnehmer
	 */
	public static PublicTeilnehmer fromTeilnehmer(final Teilnehmer teilnehmer) {
		final PublicTeilnehmer result = new PublicTeilnehmer();
		result.jahr = teilnehmer.getJahr();
		result.klassenstufe = PublicKlassenstufe.fromKlassenstufe(teilnehmer.getKlassenstufe());
		result.kuerzel = teilnehmer.getKuerzel();
		result.loesungszettelKuerzel = teilnehmer.getLoesungszettelkuerzel();
		result.nachname = teilnehmer.getNachname();
		result.vorname = teilnehmer.getVorname();
		result.zusatz = teilnehmer.getZusatz();
		result.teilnahmeart = teilnehmer.getTeilnahmeart().name();
		result.teilnahmekuerzel = teilnehmer.getTeilnahmekuerzel();
		result.setKaengurusprung(teilnehmer.getKaengurusprung());
		result.setPunkte(teilnehmer.getPunkte());
		result.setSprache(PublicSprache.fromSprache(teilnehmer.getSprache()));
		if (teilnehmer.getAuswertungsgruppe() != null) {
			result.setAuswertungsgruppe(PublicAuswertungsgruppe.fromAuswertungsgruppe(teilnehmer.getAuswertungsgruppe(), true));
		}
		if (teilnehmer.getAntwortcode() != null) {
			result.setAntworten(new AntwortcodeConverter().fromAntwortcode(teilnehmer.getAntwortcode()));
		}
		if (teilnehmer.getEingabemodus() != null) {
			result.setEingabemodus(teilnehmer.getEingabemodus().toString());
		}

		return result;
	}

	public final String getKuerzel() {
		return kuerzel;
	}

	public final void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public final String getTeilnahmeart() {
		return teilnahmeart;
	}

	final void setTeilnahmeart(final String teilnahmeart) {
		this.teilnahmeart = teilnahmeart;
	}

	public final String getTeilnahmekuerzel() {
		return teilnahmekuerzel;
	}

	final void setTeilnahmekuerzel(final String teilnahmekuerzel) {
		this.teilnahmekuerzel = teilnahmekuerzel;
	}

	public final String getJahr() {
		return jahr;
	}

	final void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public final String getVorname() {
		return vorname;
	}

	public final void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	public final String getNachname() {
		return nachname;
	}

	public final void setNachname(final String nachname) {
		this.nachname = nachname;
	}

	public final String getZusatz() {
		return zusatz;
	}

	public final void setZusatz(final String zusatz) {
		this.zusatz = zusatz;
	}

	public final String getLoesungszettelKuerzel() {
		return loesungszettelKuerzel;
	}

	public final void setLoesungszettelKuerzel(final String loesungszettelKuerzel) {
		this.loesungszettelKuerzel = loesungszettelKuerzel;
	}

	public final String getPunkte() {
		return punkte;
	}

	public final void setPunkte(final String punkte) {
		this.punkte = punkte;
	}

	public final Integer getKaengurusprung() {
		return kaengurusprung;
	}

	public final void setKaengurusprung(final Integer kaengurusprung) {
		this.kaengurusprung = kaengurusprung;
	}

	public final PublicAntwortbuchstabe[] getAntworten() {
		return antworten;
	}

	public final void setAntworten(final PublicAntwortbuchstabe[] antworten) {
		this.antworten = antworten;
	}

	public final PublicKlassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	public final void setKlassenstufe(final PublicKlassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicTeilnehmer [kuerzel=");
		builder.append(kuerzel);
		builder.append(", vorname=");
		builder.append(vorname);
		builder.append(", nachname=");
		builder.append(nachname);
		builder.append(", zusatz=");
		builder.append(zusatz);
		builder.append(", loesungszettelKuerzel=");
		builder.append(loesungszettelKuerzel);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	public final PublicAuswertungsgruppe getAuswertungsgruppe() {
		return auswertungsgruppe;
	}

	final void setAuswertungsgruppe(final PublicAuswertungsgruppe auswertungsgruppe) {
		this.auswertungsgruppe = auswertungsgruppe;
	}

	public final String getEingabemodus() {
		return eingabemodus;
	}

	public void setEingabemodus(final String eingabemodus) {
		this.eingabemodus = eingabemodus;
	}

	public final PublicSprache getSprache() {
		return sprache;
	}

	public final void setSprache(final PublicSprache sprache) {
		this.sprache = sprache;
	}
}
