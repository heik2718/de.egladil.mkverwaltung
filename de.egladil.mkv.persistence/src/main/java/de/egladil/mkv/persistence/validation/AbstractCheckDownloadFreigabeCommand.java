//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import java.io.File;

/**
 * AbstractCheckDownloadFreigabeCommand
 */
public abstract class AbstractCheckDownloadFreigabeCommand implements ICheckDownloadFreigabe {

	private final String downloadPath;

	/**
	 * Erzeugt eine Instanz von AbstractCheckDownloadFreigabeCommand
	 */
	public AbstractCheckDownloadFreigabeCommand(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	/**
	 * @see de.egladil.mkv.persistence.validation.ICheckDownloadFreigabe#isFreigegeben()
	 */
	@Override
	public boolean isFreigegeben() {
		File file = new File(downloadPath + File.separator + getMarkerFileName());
		final boolean markerfilePresent = file.isFile();
		return markerfilePresent;
	}

	/**
	 * Gibt den Namen des Markerfiles zurück, dessen Vorhandensein die Freigabe markiert.
	 *
	 * @return
	 */
	protected abstract String getMarkerFileName();

}
