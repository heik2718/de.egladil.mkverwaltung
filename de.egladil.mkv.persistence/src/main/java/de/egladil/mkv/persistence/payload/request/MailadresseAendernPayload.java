//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.Passwort;
import de.egladil.mkv.persistence.annotations.ValidMailadresseAendernPayload;

/**
 * MailadresseAendernPayload<br>
 * <br>
 * {"username":"alte-mailadresse@egladil.de","password":"start123","email":"neue-mailadresse@egladil.de","kleber":null}
 */
@ValidMailadresseAendernPayload
public class MailadresseAendernPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	@JsonProperty
	private String username;

	@Passwort
	@NotBlank
	@JsonProperty
	private String password;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	@JsonProperty
	private String email;

	@Honeypot
	@JsonProperty
	private String kleber;

	/**
	 * Erzeugt eine Instanz von MailadresseAendernPayload
	 */
	public MailadresseAendernPayload() {
	}

	/**
	 * Erzeugt eine Instanz von MailadresseAendernPayload
	 */
	public MailadresseAendernPayload(final String username, final String password, final String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}

	@Override
	public String toBotLog() {
		return "MailadresseAendernPayload [username=" + username + ", password=" + password + ", email=" + email + ", kleber="
			+ kleber + "]";
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("MailadresseAendernPayload [username=");
		builder.append(username);
		builder.append(", password=XXX, email=");
		builder.append(email);
		builder.append(", kleber=");
		builder.append(kleber);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Liefert die Membervariable username
	 *
	 * @return die Membervariable username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param username neuer Wert der Membervariablen username
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * Liefert die Membervariable password
	 *
	 * @return die Membervariable password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param password neuer Wert der Membervariablen password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Liefert die Membervariable email
	 *
	 * @return die Membervariable email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param email neuer Wert der Membervariablen email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

}
