//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.exceptions;

/**
 * MKVException
 */
public class MKVException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von MKVException
	 */
	public MKVException(String message) {

		super(message);
	}

	/**
	 * Erzeugt eine Instanz von MKVException
	 */
	public MKVException(Throwable cause) {

		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von MKVException
	 */
	public MKVException(String message, Throwable cause) {

		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von MKVException
	 */
	public MKVException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {

		super(message, cause, enableSuppression, writableStackTrace);
	}

}
