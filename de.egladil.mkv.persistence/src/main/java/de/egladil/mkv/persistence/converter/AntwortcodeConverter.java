//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.converter;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;

/**
 * AntwortcodeConverter konvertiert ein Array von PublicAntwortbuchstabe in einen Antwortcode- String und umgekehrt.
 */
public class AntwortcodeConverter {

	private final AntwortcodeItemConverter itemConverter = new AntwortcodeItemConverter();

	/**
	 * Konvertiert die einzelnen Zeichen eines Strings in ein PublicAntwortbuchstabe[]
	 *
	 * @param antwortcode String muss aus gültigen Zeichen bestehen.
	 * @return
	 */
	public PublicAntwortbuchstabe[] fromAntwortcode(final String antwortcode) {
		if (antwortcode == null) {
			throw new IllegalArgumentException("antwortcode null");
		}
		final List<PublicAntwortbuchstabe> result = new ArrayList<>(antwortcode.length());
		int index = 0;
		for (final char c : antwortcode.toCharArray()) {
			final PublicAntwortbuchstabe antwortbuchstabe = itemConverter.fromString(String.valueOf(c));
			antwortbuchstabe.setIndex(index);
			index++;
			result.add(antwortbuchstabe);
		}
		return result.toArray(new PublicAntwortbuchstabe[] {});
	}

	/**
	 * Umwandlung in einen Strin aus A-E und N ohne Komma.
	 *
	 * @param antwortbuchstaben PublicAntwortbuchstaben[]
	 * @return String
	 */
	public String fromAntwortbuchstaben(final PublicAntwortbuchstabe[] antwortbuchstaben) {
		if (antwortbuchstaben == null) {
			throw new IllegalArgumentException("antwortbuchstaben null");
		}
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < antwortbuchstaben.length; i++) {
			sb.append(itemConverter.fromAntwortbuchstabe(antwortbuchstaben[i]));
		}
		return sb.toString();
	}
}
