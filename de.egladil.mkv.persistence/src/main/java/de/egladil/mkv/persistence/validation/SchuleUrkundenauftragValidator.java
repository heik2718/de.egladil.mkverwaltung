//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.validators.KuerzelValidator;
import de.egladil.mkv.persistence.annotations.ValidSchuleUrkundenauftrag;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.payload.request.SchuleUrkundenauftrag;

/**
 * SchuleUrkundenauftragValidator
 */
public class SchuleUrkundenauftragValidator implements ConstraintValidator<ValidSchuleUrkundenauftrag, SchuleUrkundenauftrag> {

	private static final Logger LOG = LoggerFactory.getLogger(SchuleUrkundenauftragValidator.class);

	private KuerzelValidator kuerzelValidator = new KuerzelValidator();

	@Override
	public void initialize(final ValidSchuleUrkundenauftrag constraintAnnotation) {
		// nix zu initialisieren
	}

	@Override
	public boolean isValid(final SchuleUrkundenauftrag value, final ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		try {
			Farbschema.valueOf(value.getFarbschemaName());
		} catch (final IllegalArgumentException e) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.schuleurkundenauftrag}").addBeanNode()
				.addConstraintViolation();
			LOG.error("ungültiger farbschemaName {}", value.getFarbschemaName());
			return false;
		}
		final String kuerzel = value.getKuerzelRootgruppe();
		if (!kuerzelValidator.isValid(kuerzel, context)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.schuleurkundenauftrag}").addBeanNode()
				.addConstraintViolation();
			LOG.error("SchuleUrkundenauftrag.kuerzelRootgruppe enthält ungültige Zeichen: {}", kuerzel);
			return false;
		}
		if (StringUtils.isBlank(kuerzel)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.teilnehmerurkundenauftrag}").addBeanNode()
				.addConstraintViolation();
			LOG.error("SchuleUrkundenauftrag.kuerzelRootgruppe enthält leere kuerzel");
			return false;
		}
		if (kuerzel.length() > 22) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.teilnehmerurkundenauftrag}").addBeanNode()
				.addConstraintViolation();
			LOG.error("SchuleUrkundenauftrag.kuerzelRootgruppe enthält zu langes kuerzel {}", kuerzel);
			return false;
		}
		return true;
	}

}
