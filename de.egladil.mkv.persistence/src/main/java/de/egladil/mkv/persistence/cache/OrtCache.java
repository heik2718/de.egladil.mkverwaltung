//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.cache;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.LoadingCache;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * OrtCache
 */
@Singleton
public class OrtCache {

	private LoadingCache<String, Ort> cache;

	private final OrtCacheLoader cacheLoader;

	/**
	 * Erzeugt eine Instanz von OrtCache
	 */
	@Inject
	public OrtCache(OrtCacheLoader cacheLoader) {
		this.cacheLoader = cacheLoader;
		init();
	}

	public void init() {
		cache = CacheBuilder.newBuilder().maximumSize(2000).expireAfterAccess(10, TimeUnit.MINUTES)
			.refreshAfterWrite(4, TimeUnit.MINUTES).build(cacheLoader);
	}

	/**
	 * Läd den Ort mit dem gegebenen kuerzel.
	 *
	 * @param kuerzel
	 * @return Optional
	 */
	public Optional<Ort> getOrt(String kuerzel) throws MKVException {
		try {
			Ort ort = cache.get(kuerzel);
			return Optional.of(ort);
		} catch (InvalidCacheLoadException e) {
			return Optional.empty();
		} catch (ExecutionException e) {
			String msg = "Exception beim Lesen von Ort [" + kuerzel + "]: " + e.getMessage();
			throw new MKVException(msg, e);
		}
	}

}
