//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;

/**
 * AuswertungDownloadDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class AuswertungDownloadDaoImpl extends BaseDaoImpl<AuswertungDownload> implements IAuswertungDownloadDao {

	/**
	 * AuswertungDownloadDaoImpl
	 */
	@Inject
	public AuswertungDownloadDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

}
