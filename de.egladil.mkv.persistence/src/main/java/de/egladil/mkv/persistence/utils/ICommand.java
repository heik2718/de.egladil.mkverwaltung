//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

/**
 * ICommand
 */
public interface ICommand {

	/**
	 * Methode, die vom Command invocer aufgerufen wird und auf den dem Command bekannten Receiver operiert.
	 */
	void execute();
}
