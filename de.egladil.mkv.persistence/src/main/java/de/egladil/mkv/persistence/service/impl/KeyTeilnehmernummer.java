//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * KeyTeilnehmernummer
 */
public class KeyTeilnehmernummer {

	private final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider;

	private final Klassenstufe klassenstufe;

	/**
	 * KeyTeilnehmernummer
	 */
	public KeyTeilnehmernummer(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider, final Klassenstufe klassenstufe) {
		this.teilnahmeIdentifierProvider = teilnahmeIdentifierProvider;
		this.klassenstufe = klassenstufe;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("KeyTeilnehmernummer [teilnahmeIdentifier=");
		builder.append(teilnahmeIdentifierProvider);
		builder.append(", klassenstufe=");
		builder.append(klassenstufe);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((klassenstufe == null) ? 0 : klassenstufe.hashCode());
		final TeilnahmeIdentifier ti = teilnahmeIdentifierProvider.provideTeilnahmeIdentifier();
		result = prime * result + ((ti.getJahr() == null) ? 0 : ti.getJahr().hashCode());
		result = prime * result + ((ti.getKuerzel() == null) ? 0 : ti.getKuerzel().hashCode());
		result = prime * result + ((ti.getTeilnahmeart() == null) ? 0 : ti.getTeilnahmeart().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final KeyTeilnehmernummer other = (KeyTeilnehmernummer) obj;
		if (klassenstufe != other.klassenstufe) {
			return false;
		}
		final TeilnahmeIdentifier tiSelf = teilnahmeIdentifierProvider.provideTeilnahmeIdentifier();
		final TeilnahmeIdentifier tiOther = other.teilnahmeIdentifierProvider.provideTeilnahmeIdentifier();
		if (tiSelf == null) {
			if (tiOther != null) {
				return false;
			}
		} else if (!tiSelf.getJahr().equals(tiOther.getJahr())) {
			return false;
		} else if (!tiSelf.getKuerzel().equals(tiOther.getKuerzel())) {
			return false;
		} else if (!tiSelf.getTeilnahmeart().equals(tiOther.getTeilnahmeart())) {
			return false;
		}
		return true;
	}
}
