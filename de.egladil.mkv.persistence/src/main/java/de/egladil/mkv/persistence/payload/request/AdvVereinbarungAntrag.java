//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Hausnummer;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.LandKuerzel;
import de.egladil.common.validation.annotations.Plz;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * AdvVereinbarungAntrag
 */
public class AdvVereinbarungAntrag implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 8, max = 8)
	@Kuerzel
	@JsonProperty
	private String schulkuerzel;

	@NotNull
	@StringLatin
	@Size(min = 1, max = 100)
	@JsonProperty
	private String schulname;

	@NotNull
	@LandKuerzel
	@Size(min = 2, max = 2)
	@JsonProperty
	private String laendercode;

	@NotBlank
	@Size(min = 1, max = 10)
	@Plz
	@JsonProperty
	private String plz;

	@NotNull
	@StringLatin
	@Size(min = 1, max = 100)
	@JsonProperty
	private String ort;

	@NotBlank
	@Size(min = 1, max = 100)
	@StringLatin
	@JsonProperty
	private String strasse;

	@NotBlank
	@Size(min = 1, max = 10)
	@Hausnummer
	@JsonProperty
	private String hausnummer;

	@Override
	public String toBotLog() {
		return toString();
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("AdvVereinbarungAntrag [schulkuerzel=");
		builder.append(schulkuerzel);
		builder.append(", schulname=");
		builder.append(schulname);
		builder.append(", laendercode=");
		builder.append(laendercode);
		builder.append(", plz=");
		builder.append(plz);
		builder.append(", ort=");
		builder.append(ort);
		builder.append(", strasse=");
		builder.append(strasse);
		builder.append(", hausnummer=");
		builder.append(hausnummer);
		builder.append("]");
		return builder.toString();
	}

	public final String getSchulkuerzel() {
		return schulkuerzel;
	}

	public final void setSchulkuerzel(final String schulkuerzel) {
		this.schulkuerzel = schulkuerzel;
	}

	public final String getPlz() {
		return plz;
	}

	public final void setPlz(final String plz) {
		this.plz = plz;
	}

	public final String getStrasse() {
		return strasse;
	}

	public final void setStrasse(final String strasse) {
		this.strasse = strasse;
	}

	public final String getHausnummer() {
		return hausnummer;
	}

	public final void setHausnummer(final String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public final String getSchulname() {
		return schulname;
	}

	public final void setSchulname(final String schulname) {
		this.schulname = schulname;
	}

	public final String getLaendercode() {
		return laendercode;
	}

	public final void setLaendercode(final String laendercode) {
		this.laendercode = laendercode;
	}

	public final String getOrt() {
		return ort;
	}

	public final void setOrt(final String ort) {
		this.ort = ort;
	}

}
