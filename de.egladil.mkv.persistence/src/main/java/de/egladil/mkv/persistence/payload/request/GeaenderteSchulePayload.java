//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.HttpUrl;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * GeaenderteSchulePayload
 */
public class GeaenderteSchulePayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@HttpUrl
	private String url;

	@StringLatin
	@Size(min = 0, max = 200)
	private String strasse;

	@Override
	public String toBotLog() {
		return toString();
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("GeaenderteSchulePayload [name=");
		builder.append(name);
		builder.append(", url=");
		builder.append(url);
		builder.append(", strasse=");
		builder.append(strasse);
		builder.append("]");
		return builder.toString();
	}

	public final String getName() {
		return name;
	}

	public final String getUrl() {
		return url;
	}

	public final String getStrasse() {
		return strasse;
	}
}
