//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;
import java.util.Optional;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;

/**
 * ISchulteilnahmeReadOnlyDao
 */
public interface ISchulteilnahmeReadOnlyDao extends IBaseDao<SchulteilnahmeReadOnly> {

	/**
	 *
	 * @param schulkuerzel
	 * @param jahr
	 * @return Optional
	 */
	Optional<SchulteilnahmeReadOnly> findBySchulkuerzelUndJahr(String schulkuerzel, String jahr);

	/**
	 *
	 * @param schulkuerzel
	 * @return List
	 */
	List<SchulteilnahmeReadOnly> findBySchulkuerzel(String schulkuerzel);



}
