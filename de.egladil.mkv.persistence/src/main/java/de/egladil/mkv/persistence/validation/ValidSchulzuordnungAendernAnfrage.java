//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 */
@Target({ ElementType.TYPE, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SchulzuordnungAendernValidator.class)
@Documented
public @interface ValidSchulzuordnungAendernAnfrage {

	String message() default "{de.egladil.constraints.changeSchule}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
