//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.teilnahmen;

/**
 * Filterart beschreibt die Semantik für die kuerzelliste eines Teilnahmefilters.
 */
public enum Filterart {

	LAND,
	PRIVAT;
}
