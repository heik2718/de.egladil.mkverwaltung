//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * AuswertungsgruppeChildBuilder
 */
public class AuswertungsgruppeChildBuilder extends AuswertungsgruppeBuilder {

	private final Auswertungsgruppe parent;

	private final Klassenstufe klassenstufe;

	/**
	 * AuswertungsgruppeChildBuilder
	 */
	public AuswertungsgruppeChildBuilder(final Auswertungsgruppe parent, final Klassenstufe klassenstufe) {
		super();
		if (parent == null) {
			throw new IllegalArgumentException("parent ist erforderlich");
		}
		if (klassenstufe == null) {
			throw new IllegalArgumentException("klassenstufe ist erforderlich");
		}
		checkAndInit(parent.getTeilnahmeart(), parent.getTeilnahmekuerzel(), parent.getJahr());
		this.parent = parent;
		this.klassenstufe = klassenstufe;
	}

	@Override
	public AuswertungsgruppeChildBuilder name(final String name) {
		super.normalizeAndSetName(name);
		return this;
	}

	@Override
	public Auswertungsgruppe checkAndBuild() {
		final Auswertungsgruppe gruppe = super.checkAndBuild();
		parent.addAuswertungsgruppe(gruppe);
		gruppe.setKlassenstufe(klassenstufe);
		return gruppe;
	}
}
