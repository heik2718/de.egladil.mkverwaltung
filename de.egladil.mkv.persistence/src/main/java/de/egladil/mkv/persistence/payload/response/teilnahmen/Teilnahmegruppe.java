//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.teilnahmen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasPayload;
import de.egladil.common.persistence.utils.PrettyStringUtils;

/**
 * Teilnahmegruppe ist eine Zusammenfassung von Ländern, Orten, Schulen, Privatteilnahmen zu Gruppen mit einer
 * gemeinsamen Statistik.
 */
public class Teilnahmegruppe implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private HateoasPayload hateoasPayload;

	@JsonProperty
	private TeilnahmenFilter filter;

	@JsonProperty
	private String jahr;

	/**
	 * Das kann ein einzelnes Kürzel sein (ein Land) oder eine kommaseparierte Liste von Kürzeln.
	 */
	@JsonProperty
	private String kuerzel;

	/**
	 * Bei Ländern und Orten ist das die Anzahl Schulen, bei Privatteilnahmen die gesamtzahl.
	 */
	@JsonProperty
	private long anzahlAnmeldungen;

	@JsonProperty
	private long anzahlKinder;

	/**
	 * Nur bei persistenten Filterkriterien
	 */
	@JsonProperty
	private List<String> teilnahmejahre = new ArrayList<>();

	public final HateoasPayload getHateoasPayload() {
		return hateoasPayload;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}

	public final String getJahr() {
		return jahr;
	}

	public final void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public final String getKuerzel() {
		return kuerzel;
	}

	public final long getAnzahlAnmeldungen() {
		return anzahlAnmeldungen;
	}

	public final void setAnzahlAnmeldungen(final long anzahlSchulen) {
		this.anzahlAnmeldungen = anzahlSchulen;
	}

	public final long getAnzahlKinder() {
		return anzahlKinder;
	}

	public final void setAnzahlKinder(final long anzahlTeilnehmer) {
		this.anzahlKinder = anzahlTeilnehmer;
	}

	public final List<String> getTeilnahmejahre() {
		return teilnahmejahre;
	}

	public final void setTeilnahmejahre(final List<String> teilnahmejahre) {
		this.teilnahmejahre = teilnahmejahre;
	}

	public final TeilnahmenFilter getFilter() {
		return filter;
	}

	/**
	 * Setzt auch das kuerzel
	 *
	 * @param filter
	 */
	public final void setFilter(final TeilnahmenFilter filter) {
		this.filter = filter;
		this.kuerzel = PrettyStringUtils.collectionToString(this.filter.getKuerzelliste(), ",");
	}

	public final void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Teilnahmegruppe [jahr=");
		builder.append(jahr);
		builder.append(", kuerzel=");
		builder.append(kuerzel);
		builder.append(", anzahlAnmeldungen=");
		builder.append(anzahlAnmeldungen);
		builder.append(", anzahlKinder=");
		builder.append(anzahlKinder);
		builder.append(", filterart=");
		builder.append(filter.getFilterart());
		builder.append("]");
		return builder.toString();
	}
}
