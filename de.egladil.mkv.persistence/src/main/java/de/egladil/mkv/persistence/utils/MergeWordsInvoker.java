//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.List;

/**
 * MergeWordsInvoker setzt eine List von Strings zu einer Zeile zusammen.
 */
public class MergeWordsInvoker {

	public String mergeToLine(final List<String> worte) {
		final Text text = new Text(worte);
		new ConcatStringsCommand(text).execute();
		return text.getTransformedText().get(0);
	}
}
