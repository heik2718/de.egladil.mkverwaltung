//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.cache;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.LoadingCache;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * LandCache
 */
@Singleton
public class LandCache {

	private LoadingCache<String, Land> cache;

	private LandCacheLoader cacheLoader;

	/**
	 * Erzeugt eine Instanz von LandCache
	 */
	@Inject
	public LandCache(LandCacheLoader cacheLoader) {
		this.cacheLoader = cacheLoader;
		init();
	}

	public void init() {
		cache = CacheBuilder.newBuilder().maximumSize(10).expireAfterAccess(10, TimeUnit.MINUTES)
			.refreshAfterWrite(6, TimeUnit.MINUTES).build(cacheLoader);
		// cache = CacheBuilder.newBuilder().maximumSize(10).expireAfterAccess(10,
		// TimeUnit.SECONDS).refreshAfterWrite(1, TimeUnit.SECONDS).build(cacheLoader);
	}

	/**
	 * Läd das Land mit dem gegebenen kuerzel, sofern vorhanden.<br>
	 *
	 * @param kuerzel
	 * @return Land oder EgladilWebappExcetion oder MKVException
	 */
	public Optional<Land> getLand(String kuerzel) throws MKVException {
		try {
			Land land = cache.get(kuerzel);
			return Optional.of(land);
		} catch (InvalidCacheLoadException e) {
			return Optional.empty();
		} catch (ExecutionException e) {
			String msg = "Exception beim Lesen von Land [" + kuerzel + "]: " + e.getMessage();
			throw new MKVException(msg, e);
		}
	}
}
