//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.MergeWordsInvoker;
import de.egladil.mkv.persistence.utils.NormalizeWortgruppeInvoker;
import de.egladil.mkv.persistence.utils.UrkundeConstants;


/**
 * TeilnehmerManipulator
 */
public abstract class TeilnehmerManipulator {

	private String vorname;

	private String nachname;

	private String zusatz;

	private Sprache sprache;

	private Antworteingabemodus eingabemodus;

	private final NormalizeWortgruppeInvoker normalizeWortgruppeInvoker;

	private final MergeWordsInvoker mergeWordsInvoker;

	/**
	 * TeilnehmerManipulator
	 */
	public TeilnehmerManipulator() {
		this.normalizeWortgruppeInvoker = new NormalizeWortgruppeInvoker();
		this.mergeWordsInvoker = new MergeWordsInvoker();
	}

	protected final NormalizeWortgruppeInvoker getNormalizeWortgruppeInvoker() {
		return normalizeWortgruppeInvoker;
	}

	protected final String getVorname() {
		return vorname;
	}

	protected final void normalizeAndSetVorname(final String vorname) {
		if (vorname != null) {
			this.vorname = normalizeWortgruppeInvoker.normalize(vorname);
		} else {
			this.vorname = null;
		}
	}

	protected final String getNachname() {
		return nachname;
	}

	protected final void normalizeAndSetNachname(final String nachname) {
		if (nachname != null) {
			this.nachname = normalizeWortgruppeInvoker.normalize(nachname);
		} else {
			this.nachname = null;
		}
	}

	protected final void setEingabemodus(final Antworteingabemodus eingabemodus) {
		this.eingabemodus = eingabemodus;
	}

	protected final String getZusatz() {
		return zusatz;
	}

	protected final void normalizeAndSetZusatz(final String zusatz) {
		if (zusatz != null) {
			this.zusatz = normalizeWortgruppeInvoker.normalize(zusatz);
		} else {
			this.zusatz = null;
		}
	}

	protected void pruefeVornameNachnameValid() {
		if (StringUtils.isAllBlank(new String[] { vorname, nachname })) {
			throw new IllegalArgumentException("vorname oder nachname sind erforderlich");
		}
		final String zusammengesetzterName = mergeWordsInvoker.mergeToLine(Arrays.asList(new String[] { vorname, nachname }));
		new LengthTester().checkTooLongAndThrow(zusammengesetzterName, UrkundeConstants.SIZE_NAME_SMALL);
	}

	protected final Antworteingabemodus getEingabemodus() {
		return eingabemodus;
	}

	protected final Sprache getSprache() {
		return sprache;
	}

	protected final void setSprache(final Sprache sprache) {
		this.sprache = sprache;
	}
}
