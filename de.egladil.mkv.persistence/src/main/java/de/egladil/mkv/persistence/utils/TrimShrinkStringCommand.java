//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Splitter;

/**
 * TrimShrinkStringCommand entfernt einschließende Leerzeichen und schrumpft innere Leerzeichen auf eins zusammen.
 */
@Deprecated
public class TrimShrinkStringCommand implements IProduceStringCommand {

	private static final Splitter SPLITTER = Splitter.on(" ").trimResults();

	private final String string;

	/**
	 * TrimShrinkStringCommand
	 */
	public TrimShrinkStringCommand(final String string) {
		if (string == null) {
			throw new IllegalArgumentException("string null");
		}
		this.string = string;
	}

	@Override
	public String execute() {
		final String trimmed = string.trim();
		final List<String> worte = SPLITTER.splitToList(trimmed);
		final StringBuffer sb = new StringBuffer();
		for (final String wort : worte) {
			if (!StringUtils.isBlank(wort)) {
				sb.append(wort);
				sb.append(" ");
			}
		}
		return sb.toString().trim();
	}
}
