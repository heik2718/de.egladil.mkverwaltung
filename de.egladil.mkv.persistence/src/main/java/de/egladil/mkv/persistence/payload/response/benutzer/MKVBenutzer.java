//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.benutzer;

import de.egladil.common.persistence.HateoasPayload;

/**
 * MKVBenutzer ist das DTO für MKV und Admin.
 */
public class MKVBenutzer {

	private HateoasPayload hateoasPayload;

	private PersonBasisdaten basisdaten;

	private ErweiterteKontodaten erweiterteKontodaten;

	private GeschuetzteKontodaten geschuetzteKontodaten;

	public final PersonBasisdaten getBasisdaten() {
		return basisdaten;
	}

	public final void setBasisdaten(final PersonBasisdaten basisdaten) {
		this.basisdaten = basisdaten;
	}

	public final ErweiterteKontodaten getErweiterteKontodaten() {
		return erweiterteKontodaten;
	}

	public final void setErweiterteKontodaten(final ErweiterteKontodaten erweiterteKontodaten) {
		this.erweiterteKontodaten = erweiterteKontodaten;
	}

	public final GeschuetzteKontodaten getGeschuetzteKontodaten() {
		return geschuetzteKontodaten;
	}

	public final void setGeschuetzteKontodaten(final GeschuetzteKontodaten geschuetzteKontodaten) {
		this.geschuetzteKontodaten = geschuetzteKontodaten;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}

	public final HateoasPayload getHateoasPayload() {
		return hateoasPayload;
	}
}
