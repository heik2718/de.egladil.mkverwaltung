//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import java.util.Optional;

import de.egladil.mkv.persistence.domain.adv.AdvText;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.payload.request.AdvVereinbarungAntrag;

/**
 * AdvFacade
 */
public interface AdvFacade {

	/**
	 * Gibt den aktuellsten (also mit der höchsten Versionsnummer) zurück.
	 *
	 * @return AdvText oder null
	 */
	AdvText getAktuellstenAdvText();

	/**
	 * Erzeugt eine Vereinbarung mit der Schule und speichert diese.
	 *
	 * @param benutzerUuid String darf nicht null sein.
	 * @param antrag AdvVereinbarungAntrag darf nicht null sein.
	 * @return AdvVereinbarung
	 */
	AdvVereinbarung createAdvVereinbarung(String benutzerUuid, AdvVereinbarungAntrag antrag);

	/**
	 * Sucht die AdvVereinbarung zu gegebenem Schulkuerzel.
	 *
	 * @param schulkuerzel String
	 * @return Optional
	 */
	Optional<AdvVereinbarung> findAdvVereinbarungFuerSchule(String schulkuerzel);
}
