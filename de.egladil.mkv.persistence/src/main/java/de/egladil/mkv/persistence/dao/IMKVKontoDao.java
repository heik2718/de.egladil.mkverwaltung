//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.user.Person;

/**
 * IKontaktDao
 */
public interface IMKVKontoDao<T extends IMKVKonto> {

	Optional<T> findByUUID(String uuid);

	/**
	 * Sucht die Konten, bei denen der Name und der Vorname caseinsensitive gleich denen der gegebenen Person sind.
	 *
	 * @param person
	 * @return LIst
	 */
	List<T> findByPerson(Person person);

	/**
	 * C oder U.<br>
	 * <br>
	 * <b>Achtung:</b> aus Gründen, die ich noch nicht verstehe, kann man PersistenceExceptions nicht fangen und
	 * umwandeln. Das ist so lange dem Aufrufer vorbehalten.
	 *
	 * @param kontakt
	 * @return
	 * @throws PersistenceException
	 */
	T persist(T kontakt) throws PersistenceException;

	/**
	 *
	 * @return
	 */
	List<T> findAll(Class<T> clazz) throws PersistenceException;
}
