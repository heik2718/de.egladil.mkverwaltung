//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.SchuleLage;

/**
 * ISchuleDao
 */
public interface ISchuleDao extends IBaseDao<Schule> {

	/**
	 * Sucht im Ort mit dem gegebenen KUERZEL alle Schulen, deren Name like schulnameteil ist. TODO
	 *
	 * @param ortkuerzel
	 * @param schulnameteil
	 * @return List
	 */
	List<Schule> findSchulenInOrtUnscharf(String ortkuerzel, String schulnameteil) throws MKVException, PersistenceException;

	/**
	 *
	 * @param schulkuerzel String
	 * @return SchuleLage oder null
	 */
	SchuleLage getSchuleLage(String schulkuerzel);
}
