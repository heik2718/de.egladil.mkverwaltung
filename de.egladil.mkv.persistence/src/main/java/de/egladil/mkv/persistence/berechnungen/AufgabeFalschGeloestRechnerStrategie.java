//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

/**
 * AufgabeFalschGeloestRechnerStrategie
 */
public class AufgabeFalschGeloestRechnerStrategie extends AbstractWertungRechnerStrategie {

	/**
	 * @param aufgabenkategorie MKAufgabenkategorie darf nicht null sein
	 * @throws NullPointerException wenn Parameter null
	 */
	protected AufgabeFalschGeloestRechnerStrategie(final AufgabenkategorieDekorator aufgabenkategorie) {
		super(aufgabenkategorie);
	}

	/**
	 * @see de.egladil.mkv.persistence.berechnungen.IWertungRechnerStrategie#applyToSumme(int)
	 */
	@Override
	public int applyToSumme(final int summe) {
		final int result = summe - getAufgabenkategorie().berechneStrafpunkte();
		return result;
	}
}
