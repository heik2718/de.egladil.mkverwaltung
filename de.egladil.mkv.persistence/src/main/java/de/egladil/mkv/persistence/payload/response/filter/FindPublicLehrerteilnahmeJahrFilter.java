//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.filter;

import java.util.List;

import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;

/**
 * FindPublicLehrerteilnahmeJahrFilter
 */
public class FindPublicLehrerteilnahmeJahrFilter {

	private final String jahr;

	/**
	 * Erzeugt eine Instanz von FindPublicLehrerteilnahmeJahrFilter
	 */
	public FindPublicLehrerteilnahmeJahrFilter(final String jahr) throws NullPointerException {
		if (jahr == null) {
			throw new NullPointerException("jahr");
		}
		this.jahr = jahr;
	}

	/**
	 * Sucht aus der Liste der PublicLehrerteilnamen diejenige zum gegebenen Jahr heraus.
	 *
	 * @param teilnahmen List darf nicht null sein
	 * @return PublicLehrerteilnahme oder null.
	 * @throws NullPointerException wenn Parameter null.
	 */
	public PublicTeilnahme find(final List<PublicTeilnahme> teilnahmen) {
		if (teilnahmen == null) {
			throw new NullPointerException("teilnahmen");
		}
		for (final PublicTeilnahme teilnahme : teilnahmen) {
			if (jahr.equals(teilnahme.getTeilnahmeIdentifier().getJahr())) {
				return teilnahme;
			}
		}
		return null;
	}
}
