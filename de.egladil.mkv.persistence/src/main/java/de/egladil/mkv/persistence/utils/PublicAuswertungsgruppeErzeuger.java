//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.List;

import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.payload.response.EnhancePublicAuswertungsgruppeCommand;
import de.egladil.mkv.persistence.payload.response.PublicAuswertungsgruppe;
import de.egladil.mkv.persistence.payload.response.PublicFarbschema;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * PublicAuswertungsgruppeErzeuger
 */
public class PublicAuswertungsgruppeErzeuger {

	private PublicAuswertungsgruppe result;

	/**
	 *
	 * @param auswertungsgruppe Auswertungsgruppe
	 * @param lazy boolean wenn es teilnehmer gibt, dann werden diese nicht geladen.
	 * @return PublicAuswertungsgruppeErzeuger
	 */
	public PublicAuswertungsgruppeErzeuger execute(final Auswertungsgruppe auswertungsgruppe, final boolean lazy) {
		if (auswertungsgruppe == null) {
			throw new IllegalArgumentException("auswertungsgruppe null");
		}
		if (auswertungsgruppe.isRoot()) {
			result = createAuswertungsgruppe(auswertungsgruppe, lazy);
		} else {
			final PublicAuswertungsgruppe root = createAuswertungsgruppe(auswertungsgruppe.getParent(), lazy);
			for (final PublicAuswertungsgruppe g : root.getAuswertungsgruppen()) {
				if (g.getKuerzel().equals(auswertungsgruppe.getKuerzel())) {
					result = g;
					break;
				}
			}
		}
		return this;
	}

	private PublicAuswertungsgruppe createAuswertungsgruppe(final Auswertungsgruppe auswertungsgruppe, final boolean lazy) {
		final PublicAuswertungsgruppe result = new PublicAuswertungsgruppe();
		result.setLazy(lazy);
		if (auswertungsgruppe.getFarbschema() != null) {
			result.setFarbschema(PublicFarbschema.fromFarbschema(auswertungsgruppe.getFarbschema()));
		}
		if (auswertungsgruppe.getKlassenstufe() != null) {
			final PublicKlassenstufe pks = PublicKlassenstufe.fromKlassenstufe(auswertungsgruppe.getKlassenstufe());
			result.setKlassenstufe(pks);
		}
		result.setJahr(auswertungsgruppe.getJahr());
		result.setKuerzel(auswertungsgruppe.getKuerzel());
		result.setName(auswertungsgruppe.getName());
		result.setTeilnahmeart(auswertungsgruppe.getTeilnahmeart().name());
		result.setTeilnahmekuerzel(auswertungsgruppe.getTeilnahmekuerzel());
		if (!lazy) {
			final List<Teilnehmer> alleTeilnehmer = auswertungsgruppe.getAlleTeilnehmer();
			for (final Teilnehmer t : alleTeilnehmer) {
				result.addTeilnehmer(PublicTeilnehmer.fromTeilnehmer(t));
			}
		}
		enhancePublicAuswertungsgruppe(auswertungsgruppe, result);
		for (final Auswertungsgruppe child : auswertungsgruppe.getAuswertungsgruppen()) {
			final PublicAuswertungsgruppe resultChild = createAuswertungsgruppe(child, lazy);
			enhancePublicAuswertungsgruppe(child, resultChild);
			result.addAuswertungsgruppe(resultChild);
			resultChild.setParent(result);
		}
		return result;
	}

	/**
	 *
	 * @return PublicAuswertungsgruppe das Erebnis der Erzeugung
	 * @throws IllegalStateException
	 */
	public final PublicAuswertungsgruppe getResult() {
		if (result == null) {
			throw new IllegalStateException("zuerst execute aufrufen, damit das Ergebnis erzeugt wird");
		}
		return result;
	}

	private void enhancePublicAuswertungsgruppe(final Auswertungsgruppe auswertungsgruppe, final PublicAuswertungsgruppe pa) {
		new EnhancePublicAuswertungsgruppeCommand(auswertungsgruppe, pa).execute();
	}
}
