//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;

/**
 * TeilnehmerFacadeImpl
 */
@Singleton
public class TeilnehmerFacadeImpl implements TeilnehmerFacade {

	private final ILoesungszettelDao loesungszettelDao;

	private final IAuswertungsgruppeDao auswertungsgruppeDao;

	private final ITeilnehmerDao teilnehmerDao;

	private final IUploadInfoDao uploadInfoDao;

	private final TeilnehmerService teilnehmerService;

	/**
	 * TeilnehmerFacadeImpl
	 */
	@Inject
	public TeilnehmerFacadeImpl(final ILoesungszettelDao loesungszettelDao, final IAuswertungsgruppeDao auswertungsgruppeDao,
		final ITeilnehmerDao teilnehmerDao, final IUploadInfoDao uploadInfoDao, final TeilnehmerService teilnehmerService) {
		this.loesungszettelDao = loesungszettelDao;
		this.auswertungsgruppeDao = auswertungsgruppeDao;
		this.teilnehmerDao = teilnehmerDao;
		this.uploadInfoDao = uploadInfoDao;
		this.teilnehmerService = teilnehmerService;
	}

	@Override
	public List<Loesungszettel> getLoesungszettel(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		final List<Loesungszettel> result = loesungszettelDao
			.findByTeilnahmeIdentifier(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
		return result;
	}

	@Override
	public Optional<Loesungszettel> getLoesungszettel(final Teilnehmer teilnehmer) {
		if (teilnehmer.getLoesungszettelkuerzel() == null) {
			return Optional.empty();
		}
		return loesungszettelDao.findByUniqueKey(Loesungszettel.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			teilnehmer.getLoesungszettelkuerzel());
	}

	@Override
	public Optional<Loesungszettel> getUniqueLoesungszettel(final String kuerzel) {
		if (StringUtils.isBlank(kuerzel)) {
			throw new IllegalArgumentException("kuerzel blank");
		}
		return loesungszettelDao.findByUniqueKey(Loesungszettel.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
	}

	@Override
	public List<Auswertungsgruppe> getAuswertungsgruppen(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		final List<Auswertungsgruppe> trefferliste = auswertungsgruppeDao
			.findByTeilnahmeIdentifier(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
		return trefferliste;
	}

	@Override
	public Optional<Auswertungsgruppe> getRootAuswertungsruppe(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		final Optional<Auswertungsgruppe> result = auswertungsgruppeDao
			.findRootByTeilnahmeIdentifier(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
		return result;
	}

	@Override
	public Optional<Auswertungsgruppe> getAuswertungsgruppe(final String kuerzel) {
		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}
		return auswertungsgruppeDao.findByUniqueKey(Auswertungsgruppe.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
	}

	@Override
	public List<Teilnehmer> getTeilnehmer(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		final List<Teilnehmer> trefferliste = teilnehmerDao
			.findByTeilnahmeIdentifier(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
		return trefferliste;
	}

	@Override
	public List<Teilnehmer> getTeilnehmer(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		return this.getTeilnehmer(this.createTeilnahmeIdentifierProvider(teilnahmeIdentifier));
	}

	@Override
	public long getAnzahlTeilnehmer(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		return teilnehmerDao.anzahlTeilnehmer(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
	}

	@Override
	public List<UploadInfo> getUploadInfos(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		final List<UploadInfo> trefferliste = uploadInfoDao
			.findByTeilnahmeIdentifier(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
		return trefferliste;
	}

	@Override
	public long getAnzahlLoesungszettel(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		return loesungszettelDao.anzahlLoesungszettel(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
	}

	@Override
	public int getAnzahlLoesungszettel(final String jahr, final Teilnahmeart teilnahmeart, final Klassenstufe klassenstufe) {
		return loesungszettelDao.getAnzahlLoesungszettel(jahr, teilnahmeart, klassenstufe);
	}

	@Override
	public long getAnzahlUploads(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		return uploadInfoDao.anzahlUploadInfos(teilnahmeIdentifierProvider.provideTeilnahmeIdentifier());
	}

	@Override
	public Optional<Teilnehmer> getTeilnehmer(final String kuerzel) {
		return this.teilnehmerDao.findByUniqueKey(Teilnehmer.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
	}

	@Override
	public Teilnehmer createTeilnehmer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier,
		final PublicTeilnehmer teilnehmerPayload)
		throws IllegalArgumentException, PreconditionFailedException, EgladilDuplicateEntryException, EgladilStorageException {
		return teilnehmerService.createTeilnehmer(benutzerUuid, teilnahmeIdentifier, teilnehmerPayload);
	}

	@Override
	public Teilnehmer updateTeilnehmer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier,
		final PublicTeilnehmer teilnehmerPayload)
		throws IllegalArgumentException, PreconditionFailedException, EgladilDuplicateEntryException, EgladilStorageException {
		return teilnehmerService.updateTeilnehmer(benutzerUuid, teilnahmeIdentifier, teilnehmerPayload);
	}

	private TeilnahmeIdentifierProvider createTeilnahmeIdentifierProvider(final TeilnahmeIdentifier teilnahmeIdentifier) {
		return new TeilnahmeIdentifierProvider() {

			@Override
			public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
				return teilnahmeIdentifier;
			}
		};
	}

	@Override
	public Optional<Teilnehmer> deleteTeilnehmer(final String benutzerUuid, final String kuerzel) {
		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}
		if (StringUtils.isBlank(kuerzel)) {
			throw new IllegalArgumentException("kuerzel blank");
		}
		return teilnehmerService.deleteTeilnehmer(benutzerUuid, kuerzel);
	}

	@Override
	public void deleteAllTeilnehmer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		final List<Teilnehmer> teilnehmer = this.getTeilnehmer(teilnahmeIdentifier);
		if (!teilnehmer.isEmpty()) {
			teilnehmerService.deleteAllTeilnehmer(benutzerUuid, teilnahmeIdentifier, teilnehmer);
		}
	}
}
