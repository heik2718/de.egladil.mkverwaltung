//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * SchulteilnahmeDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class SchulteilnahmeDaoImpl extends BaseDaoImpl<Schulteilnahme> implements ISchulteilnahmeDao {

	private static final Logger LOG = LoggerFactory.getLogger(SchulteilnahmeDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von SchulteilnahmeDaoImpl
	 */
	@Inject
	public SchulteilnahmeDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<Schulteilnahme> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier)
		throws EgladilStorageException {
		try {
			final String stmt = "SELECT t FROM Schulteilnahme t where t.kuerzel = :kuerzel and t.jahr = :jahr";
			LOG.debug(stmt);

			final EntityManager entityManager = getEntityManager();
			final TypedQuery<Schulteilnahme> query = entityManager.createQuery(stmt, Schulteilnahme.class);
			query.setParameter("kuerzel", teilnahmeIdentifier.getKuerzel());
			query.setParameter("jahr", teilnahmeIdentifier.getJahr());

			final List<Schulteilnahme> treffer = query.getResultList();
			LOG.debug("Anzahl Treffer={}", treffer.size());
			if (treffer.size() == 1) {
				return Optional.of(treffer.get(0));
			}
			if (treffer.isEmpty()) {
				return Optional.empty();
			}
			throw new EgladilStorageException(
				"Datenfehler: mehr als eine Schulteilnahme mit " + teilnahmeIdentifier.toString() + " vorhanden");
		} catch (final Exception e) {
			throw new EgladilStorageException(
				"Unerwarteter Fehler beim Suchen der Schulteilnahme mit  " + teilnahmeIdentifier.toString() + ": " + e.getMessage(),
				e);
		}
	}

	@Override
	public List<Schulteilnahme> findAllBySchulkuerzel(final String schulkuerzel) throws MKVException, EgladilStorageException {
		if (schulkuerzel == null) {
			throw new MKVException("schulkuerzel darf nicht null sein");
		}
		try {
			final String stmt = "SELECT t FROM Schulteilnahme t where t.kuerzel = :kuerzel";
			LOG.debug(stmt);

			final EntityManager entityManager = getEntityManager();
			final TypedQuery<Schulteilnahme> query = entityManager.createQuery(stmt, Schulteilnahme.class);
			query.setParameter("kuerzel", schulkuerzel);

			final List<Schulteilnahme> treffer = query.getResultList();
			LOG.debug("Anzahl Treffer={}", treffer.size());
			return treffer;
		} catch (final Exception e) {
			throw new EgladilStorageException(
				"Unerwarteter Fehler beim Suchen der Schulteilnahmen mit [schulkuerzel=" + schulkuerzel + "]: " + e.getMessage(),
				e);
		}
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.ISchulteilnahmeDao#findAllByJahr(java.lang.String)
	 */
	@Override
	public List<Schulteilnahme> findAllByJahr(final String jahr) throws MKVException, EgladilStorageException {
		if (jahr == null) {
			throw new MKVException("jahr darf nicht null sein");
		}
		try {
			final String stmt = "SELECT t FROM Schulteilnahme t where t.jahr = :jahr";
			LOG.debug(stmt);

			final EntityManager entityManager = getEntityManager();
			final TypedQuery<Schulteilnahme> query = entityManager.createQuery(stmt, Schulteilnahme.class);
			query.setParameter("jahr", jahr);

			final List<Schulteilnahme> treffer = query.getResultList();
			LOG.debug("Anzahl Treffer={}", treffer.size());
			return treffer;
		} catch (final Exception e) {
			throw new EgladilStorageException(
				"Unerwarteter Fehler beim Suchen der Schulteilnahmen mit [jahr=" + jahr + "]: " + e.getMessage(), e);
		}
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.ISchulteilnahmeDao#findOrphan(java.lang.String, java.lang.String)
	 */
	@Override
	public Optional<Schulteilnahme> findOrphan(final String kuerzel, final String jahr)
		throws MKVException, EgladilStorageException {
		if (kuerzel == null) {
			throw new MKVException("kuerzel darf nicht null sein");
		}
		if (jahr == null) {
			throw new MKVException("jahr darf nicht null sein");
		}
		try {
			final String stmt = "select id from schulteilnahmen where jahr = :jahr and kuerzel = :kuerzel and id not in (select teilnahme from schulteilnahmen_lehrer)";
			final Query query = getEntityManager().createNativeQuery(stmt);
			query.setParameter("jahr", jahr);
			query.setParameter("kuerzel", kuerzel);

			@SuppressWarnings("rawtypes")
			final List trefferliste = query.getResultList();
			LOG.debug("Anzahl Treffer: {}", trefferliste.size());
			if (trefferliste.isEmpty()) {
				return Optional.empty();
			}
			if (trefferliste.size() > 1) {
				throw new EgladilStorageException(
					"Mehr als eine Schulteilnahme mit [kuerzel=" + kuerzel + ", jahr=" + jahr + "] gefunden");
			}
			final Integer id = (Integer) trefferliste.get(0);
			return this.findById(Schulteilnahme.class, id);
		} catch (final Exception e) {
			throw new EgladilStorageException(
				"Unerwarteter Fehler beim Suchen der Schulteilnahmen mit [jahr=" + jahr + "]: " + e.getMessage(), e);
		}
	}
}
