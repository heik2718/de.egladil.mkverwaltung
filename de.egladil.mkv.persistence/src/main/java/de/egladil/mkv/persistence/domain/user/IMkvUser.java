//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

/**
 * IMkvUser ist ein MKV-Benutzer mit bestimmten Attributen.
 */
public interface IMkvUser extends IMailempfaenger {

	boolean isMailnachricht();

	boolean isGesperrt();

	boolean isAktiviert();

	String getJahr();

	String getNachname();

	String getVorname();

}
