//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;

/**
 * BenutzerPrivatMappingStrategy
 */
public class BenutzerPrivatMappingStrategy implements IBenutzerMappingStrategy {

	private final String wettbewerbsjahr;

	/**
	 * Erzeugt eine Instanz von BenutzerPrivatMappingStrategy
	 */
	public BenutzerPrivatMappingStrategy() {
		wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	@Override
	public MKVBenutzer createMKVBenutzer(final Benutzerkonto benutzer, final IMKVKonto kontakt) {

		final Privatkonto privatkonto = (Privatkonto) kontakt;
		final Person person = privatkonto.getPerson();

		final MKVBenutzer result = new MKVBenutzer();
		final PersonBasisdaten basisdaten = new PersonBasisdaten();
		basisdaten.setEmail(benutzer.getEmail());
		basisdaten.setRolle(Role.MKV_PRIVAT.toString());
		basisdaten.setNachname(person.getNachname());
		basisdaten.setVorname(person.getVorname());
		result.setBasisdaten(basisdaten);

		final ErweiterteKontodaten erweiterteDaten = new ErweiterteKontodaten();
		erweiterteDaten.setMailbenachrichtigung(privatkonto.isAutomatischBenachrichtigen());

		final TeilnahmeIdentifierProvider aktuelleTeilnahme = privatkonto.getTeilnahmeZuJahr(wettbewerbsjahr);
		if (aktuelleTeilnahme != null) {
			final PublicTeilnahme pt = PublicTeilnahme
				.createFromTeilnahmeIdentifier(aktuelleTeilnahme.provideTeilnahmeIdentifier());
			pt.setAktuelle(true);
			erweiterteDaten.setAktuelleTeilnahme(pt);
		}

		erweiterteDaten.setAnzahlTeilnahmen(privatkonto.getTeilnahmen().size());

		result.setErweiterteKontodaten(erweiterteDaten);

		return result;
	}
}
