//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.IPathValidationStrategie;

/**
 * VerzeichnisValidationStrategy
 */
public class VerzeichnisValidationStrategy implements IPathValidationStrategie {

	private static final Logger LOG = LoggerFactory.getLogger(VerzeichnisValidationStrategy.class);

	/**
	 * @see de.egladil.common.validation.IPathValidationStrategie#checkIsRegular(java.nio.file.Path)
	 */
	@Override
	public void checkIsRegular(Path path) throws IOException {
		if (!Files.isDirectory(path, new LinkOption[] { LinkOption.NOFOLLOW_LINKS })) {
			LOG.info("{} existiert nicht oder und ist kein Verzeichnis", path.toString());
			throw new IOException(path.toString() + " existiert nicht oder ist kein reguläres Verzeichnis");
		}
		LOG.debug("{} existiert und ist ein Verzeichnis", path.toString());
	}
}
