package de.egladil.mkv.persistence.config;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.JpaPersistPrivateModule;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.mkv.persistence.dao.IAdminBenutzerDao;
import de.egladil.mkv.persistence.dao.IAdvTextDao;
import de.egladil.mkv.persistence.dao.IAdvVereinbarungDao;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.INativeSqlDao;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.dao.KatalogDaoProvider;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.dao.impl.AdminBenutzerDaoImpl;
import de.egladil.mkv.persistence.dao.impl.AdvTextDaoImpl;
import de.egladil.mkv.persistence.dao.impl.AdvVereinbarungDaoImpl;
import de.egladil.mkv.persistence.dao.impl.AuswertungDownloadDaoImpl;
import de.egladil.mkv.persistence.dao.impl.AuswertungsgruppeDaoImpl;
import de.egladil.mkv.persistence.dao.impl.EreignisDaoImpl;
import de.egladil.mkv.persistence.dao.impl.KatalogDaoProviderImpl;
import de.egladil.mkv.persistence.dao.impl.LandDaoImpl;
import de.egladil.mkv.persistence.dao.impl.LehrerkontoDaoImpl;
import de.egladil.mkv.persistence.dao.impl.LehrerteilnahmeInfoDaoImpl;
import de.egladil.mkv.persistence.dao.impl.LoesungszettelDaoImpl;
import de.egladil.mkv.persistence.dao.impl.NativeSqlDaoImpl;
import de.egladil.mkv.persistence.dao.impl.OrtDaoImpl;
import de.egladil.mkv.persistence.dao.impl.PrivatkontoDaoImpl;
import de.egladil.mkv.persistence.dao.impl.PrivatteilnahmeDaoImpl;
import de.egladil.mkv.persistence.dao.impl.PrivatteilnahmeInfoDaoImpl;
import de.egladil.mkv.persistence.dao.impl.SchuleDaoImpl;
import de.egladil.mkv.persistence.dao.impl.SchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.impl.SchulteilnahmeDaoImpl;
import de.egladil.mkv.persistence.dao.impl.SchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.dao.impl.TeilnehmerDaoImpl;
import de.egladil.mkv.persistence.dao.impl.UploadDaoImpl;
import de.egladil.mkv.persistence.dao.impl.UploadInfoDaoImpl;
import de.egladil.mkv.persistence.dao.impl.WettbewerbsloesungDaoImpl;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.impl.AuthorizationServiceImpl;
import de.egladil.mkv.persistence.service.impl.TeilnahmenFacadeImpl;

public class MKVPersistenceModule extends JpaPersistPrivateModule {

	public MKVPersistenceModule(final String pathConfigRoot) {
		super(MKVPersistenceUnit.class, pathConfigRoot);
	}

	@Override
	protected String getPersistenceUnitName() {
		return "mkvServicePU";
	}

	@Override
	protected IEgladilConfiguration getConfigurations() {
		return new AbstractEgladilConfiguration(getPathConfigRoot()) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "mkv_persistence.properties";
			}
		};
	}

	@Override
	protected void doConfigure() {
		bind(ILandDao.class).to(LandDaoImpl.class);
		expose(ILandDao.class);

		bind(IOrtDao.class).to(OrtDaoImpl.class);
		expose(IOrtDao.class);

		bind(ISchuleDao.class).to(SchuleDaoImpl.class);
		expose(ISchuleDao.class);

		bind(ISchuleReadOnlyDao.class).to(SchuleReadOnlyDao.class);
		expose(ISchuleReadOnlyDao.class);

		bind(ILehrerkontoDao.class).to(LehrerkontoDaoImpl.class);
		expose(ILehrerkontoDao.class);

		bind(IPrivatkontoDao.class).to(PrivatkontoDaoImpl.class);
		expose(IPrivatkontoDao.class);

		bind(IPrivatteilnahmeDao.class).to(PrivatteilnahmeDaoImpl.class);
		expose(IPrivatteilnahmeDao.class);

		bind(ISchulteilnahmeDao.class).to(SchulteilnahmeDaoImpl.class);
		expose(ISchulteilnahmeDao.class);

		bind(IEreignisDao.class).to(EreignisDaoImpl.class);
		expose(IEreignisDao.class);

		bind(ILehrerteilnahmeInfoDao.class).to(LehrerteilnahmeInfoDaoImpl.class);
		expose(ILehrerteilnahmeInfoDao.class);

		bind(IUploadDao.class).to(UploadDaoImpl.class);
		expose(IUploadDao.class);

		bind(ILoesungszettelDao.class).to(LoesungszettelDaoImpl.class);
		expose(ILoesungszettelDao.class);

		bind(IUploadInfoDao.class).to(UploadInfoDaoImpl.class);
		expose(IUploadInfoDao.class);

		bind(IAdminBenutzerDao.class).to(AdminBenutzerDaoImpl.class);
		expose(IAdminBenutzerDao.class);

		bind(IWettbewerbsloesungDao.class).to(WettbewerbsloesungDaoImpl.class);
		expose(IWettbewerbsloesungDao.class);

		bind(IAuswertungsgruppeDao.class).to(AuswertungsgruppeDaoImpl.class);
		expose(IAuswertungsgruppeDao.class);

		bind(ITeilnehmerDao.class).to(TeilnehmerDaoImpl.class);
		expose(ITeilnehmerDao.class);

		bind(IAuswertungDownloadDao.class).to(AuswertungDownloadDaoImpl.class);
		expose(IAuswertungDownloadDao.class);

		bind(AuthorizationService.class).to(AuthorizationServiceImpl.class);
		expose(AuthorizationService.class);

		bind(IPrivatteilnahmeInfoDao.class).to(PrivatteilnahmeInfoDaoImpl.class);
		expose(IPrivatteilnahmeInfoDao.class);

		bind(ISchulteilnahmeReadOnlyDao.class).to(SchulteilnahmeReadOnlyDao.class);
		expose(ISchulteilnahmeReadOnlyDao.class);

		bind(IAdvTextDao.class).to(AdvTextDaoImpl.class);
		expose(IAdvTextDao.class);

		bind(IAdvVereinbarungDao.class).to(AdvVereinbarungDaoImpl.class);
		expose(IAdvVereinbarungDao.class);

		bind(TeilnahmenFacade.class).to(TeilnahmenFacadeImpl.class);
		expose(TeilnahmenFacade.class);

		bind(INativeSqlDao.class).to(NativeSqlDaoImpl.class);
		expose(INativeSqlDao.class);

		bind(KatalogDaoProvider.class).to(KatalogDaoProviderImpl.class);
		expose(KatalogDaoProvider.class);
	}
}
