//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * VerzeichnisAuflistenCommand
 */
public class VerzeichnisAuflistenCommand {

	private static final Logger LOG = LoggerFactory.getLogger(VerzeichnisAuflistenCommand.class);

	/**
	 * Gibt eine Liste von Verzeichnissen oder Dateien unter absPath zurück. IOException wird geloggt.
	 *
	 * @param absPath String absoluter Pfad zu einem Verzeichnis. Darf nicht null sein.
	 * @return List von File-Objekten oder null
	 * @throws NullPointerException falls absPath null
	 */
	public List<File> listFilesAndLogIOException(final String absPath) {
		if (absPath == null) {
			throw new NullPointerException("Parameter absPath");
		}
		try {
			final VerzeichnisKinderProvider verzeichnisKindProvider = new VerzeichnisKinderProvider(absPath);
			return verzeichnisKindProvider.getFiles();
		} catch (final IOException e) {
			LOG.error(e.getMessage());
			return null;
		}
	}
}
