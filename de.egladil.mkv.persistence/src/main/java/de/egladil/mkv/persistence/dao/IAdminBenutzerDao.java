//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.user.AdminBenutzerinfo;

/**
 * IAdminBenutzerDao
 */
public interface IAdminBenutzerDao extends IBaseDao<AdminBenutzerinfo> {

	/**
	 * Unscharfe Suche in Privatkonto. Es wird nach vorname or nachname unscharf gesucht.
	 *
	 * @param name String darf nicht blank sein.
	 * @return List
	 */
	List<AdminBenutzerinfo> findWithName(String name);

	/**
	 * Unscharfe Suche in Privatkonto. Es wird nach vorname or nachname unscharf gesucht.
	 *
	 * @param name String darf nicht blank sein.
	 * @return int
	 */
	BigInteger getAnzahlWithNameLike(String name);

	/**
	 * UUID startsWith.
	 *
	 * @param uuidPart String
	 * @return BigInteger
	 */
	BigInteger getAnzahlWhereUuidStartsWith(String uuidPart);

	/**
	 * Sucht alle Benutzer, deren UUID mit uuidPart beginnt.
	 *
	 * @param uuidPart String Anfang der UUID
	 * @return List
	 */
	List<AdminBenutzerinfo> findWhereUuidStartsWith(String uuidPart);
}
