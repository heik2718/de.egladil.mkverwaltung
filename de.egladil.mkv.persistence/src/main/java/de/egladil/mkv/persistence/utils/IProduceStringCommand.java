//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

/**
 * IProduceStringCommand
 */
public interface IProduceStringCommand {

	/**
	 * Erzeuge einen resultierenden String
	 *
	 * @return String
	 */
	String execute();
}
