//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SchuleLage
 */
public class OrtLage {

	@JsonProperty
	private String landkuerzel;

	@JsonProperty
	private String land;

	/**
	 * SchuleLage
	 */
	public OrtLage() {
	}

	/**
	 * SchuleLage
	 */
	public OrtLage(final String landkuerzel, final String land) {
		this.landkuerzel = landkuerzel;
		this.land = land;
	}

	public final String getLandkuerzel() {
		return landkuerzel;
	}

	public final String getLand() {
		return land;
	}
}
