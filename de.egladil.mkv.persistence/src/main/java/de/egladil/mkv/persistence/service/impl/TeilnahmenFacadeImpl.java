//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.INativeSqlDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.impl.SchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.PrivatteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifierJahrDescendingComparator;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * TeilnahmenFacadeImpl
 */
@Singleton
public class TeilnahmenFacadeImpl implements TeilnahmenFacade {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnahmenFacade.class);

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private final TeilnehmerFacade teilnehmerFacade;

	private final SchulteilnahmeReadOnlyDao schulteilnahmenDao;

	private final ISchuleReadOnlyDao schuleReadOnlyDao;

	private final IPrivatteilnahmeInfoDao privatteilnahmeInfoDao;

	private final INativeSqlDao nativeSqlDao;

	/**
	 * TeilnahmenFacadeImpl
	 */
	@Inject
	public TeilnahmenFacadeImpl(final ILehrerkontoDao lehrerkontoDao, final IPrivatkontoDao privatkontoDao,
		final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao, final TeilnehmerFacade teilnehmerFacade,
		final SchulteilnahmeReadOnlyDao schulteilnahmenDao, final IPrivatteilnahmeInfoDao privatteilnahmeInfoDao,
		final INativeSqlDao nativeSqlDao, final ISchuleReadOnlyDao schuleReadOnlyDao) {
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.lehrerteilnahmeInfoDao = lehrerteilnahmeInfoDao;
		this.teilnehmerFacade = teilnehmerFacade;
		this.schulteilnahmenDao = schulteilnahmenDao;
		this.privatteilnahmeInfoDao = privatteilnahmeInfoDao;
		this.nativeSqlDao = nativeSqlDao;
		this.schuleReadOnlyDao = schuleReadOnlyDao;
	}

	@Override
	public List<PublicTeilnahme> getAllTeilnahmen(final Benutzerkonto benutzer) {

		final IMKVKonto kontakt = new MapBenutzerToMKVKontaktCommand(lehrerkontoDao, privatkontoDao, benutzer.getUuid())
			.findMKVKonto();

		if (kontakt.getRole() == Role.MKV_LEHRER) {

			Lehrerkonto lehrerkonto = (Lehrerkonto) kontakt;

			List<Schulteilnahme> alleDirektenTeilnahmen = nativeSqlDao.getSchulteilnahmenForLehrer(kontakt.getId());
			String wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();

			final List<PublicTeilnahme> result = new ArrayList<>(alleDirektenTeilnahmen.size());
			Map<String, SchuleReadOnly> schulenMap = new HashMap<>();

			for (Schulteilnahme teilnahme : alleDirektenTeilnahmen) {
				SchuleReadOnly schule = schulenMap.get(teilnahme.getKuerzel());

				if (schule == null) {
					Optional<SchuleReadOnly> optSchule = schuleReadOnlyDao.findByKuerzel(teilnahme.getKuerzel());
					if (optSchule.isPresent()) {
						schule = optSchule.get();
						schulenMap.put(teilnahme.getKuerzel(), schule);
					}
				}

				PublicTeilnahme pt = null;
				final PublicSchule ps = PublicSchule.create(schule);

				if (wettbewerbsjahr.equals(teilnahme.getJahr())) {
					List<LehrerteilnahmeInformation> alleAktuellenTeilnahmeinfos = lehrerteilnahmeInfoDao
						.findAlleMitJahrUndSchulkuerzel(wettbewerbsjahr, teilnahme.getKuerzel());
					pt = this.sammleDatenAktuelleLehrerteilnahme((Lehrerkonto) kontakt, ps, alleAktuellenTeilnahmeinfos);

				} else {
					pt = new PublicTeilnahme();
					pt.setTeilnahmeIdentifier(
						TeilnahmeIdentifier.createSchulteilnahmeIdentifier(teilnahme.getKuerzel(), teilnahme.getJahr()));
					pt.setSchule(ps);
				}
				result.add(pt);

			}

			List<SchulteilnahmeReadOnly> alleTeilnahmenDerAktuellenSchule = schulteilnahmenDao
				.findBySchulkuerzel(lehrerkonto.getSchule().getKuerzel());
			for (SchulteilnahmeReadOnly st : alleTeilnahmenDerAktuellenSchule) {
				if (!wettbewerbsjahr.equals(st.getJahr())) {

					Optional<PublicTeilnahme> optVorhandene = result.stream()
						.filter(t -> t.getTeilnahmeIdentifier().equals(st.provideTeilnahmeIdentifier())).findFirst();

					if (!optVorhandene.isPresent()) {

						SchuleLage lage = SchuleLage.create(st);
						PublicSchule ps = new PublicSchule(st.getSchule(), st.getSchulkuerzel());
						ps.setLage(lage);

						PublicTeilnahme pt = new PublicTeilnahme();
						pt.setAktuelle(false);
						pt.setAnzahlLoesungszettel(this.getAnzahlLoesungszettel(st));
						pt.setAnzahlTeilnehmer(this.getAnzahlKinder(st));
						pt.setAnzahlUploads(this.getAnzahlUploads(st));
						pt.setSchule(ps);
						pt.setTeilnahmeIdentifier(st.provideTeilnahmeIdentifier());

						result.add(pt);
					}
				}
			}

			for (final PublicTeilnahme teilnahme : result) {
				new EnhanceTeilnahmeCommand(teilnehmerFacade, teilnahme).execute();
			}

			Collections.sort(result, new TeilnahmeIdentifierJahrDescendingComparator());
			return result;
		}

		final Privatkonto privatkonto = (Privatkonto) kontakt;
		final List<Privatteilnahme> alleTeilnahmen = privatkonto.getTeilnahmen();
		final List<PublicTeilnahme> result = new ArrayList<>();
		for (final Privatteilnahme t : alleTeilnahmen) {
			final PublicTeilnahme pt = PublicTeilnahme.createFromTeilnahmeIdentifier(t.provideTeilnahmeIdentifier());
			new EnhanceTeilnahmeCommand(teilnehmerFacade, pt).execute();
			result.add(pt);
		}
		Collections.sort(result, new TeilnahmeIdentifierJahrDescendingComparator());
		return result;
	}

	@Override
	public List<SchulteilnahmeReadOnly> getSchulteilnahmen(final String schulkuerzel) {
		return schulteilnahmenDao.findBySchulkuerzel(schulkuerzel);
	}

	@Override
	public int getAnzahlSchulteilnahmen(final String schulkuerzel) {
		return nativeSqlDao.getAnzahlSchulteilnahmen(schulkuerzel);
	}

	@Override
	public int getAnzahlSchulteilnahmenLand(final List<String> landkuerzel, final String jahr) {

		if (landkuerzel.isEmpty()) {
			LOG.warn("leere Kürzelliste erhalten.");
			return 0;
		}

		final int anzahl = nativeSqlDao.getAnzahlSchulteilnahmenLand(landkuerzel, jahr);
		return anzahl;
	}

	@Override
	public int getAnzahlLoesungszettelSchulteilnahmen(final String jahr, final Klassenstufe klassenstufe,
		final List<String> landkuerzel) {

		if (landkuerzel == null || landkuerzel.isEmpty()) {
			return teilnehmerFacade.getAnzahlLoesungszettel(jahr, Teilnahmeart.S, klassenstufe);
		}

		return nativeSqlDao.getAnzahlLoesungszettelSchulteilnahmen(jahr, klassenstufe, landkuerzel);
	}

	@Override
	public List<Loesungszettel> findLoesungszettelByJahrUndKlassenstufe(final String jahr, final Klassenstufe klassenstufe) {
		return this.nativeSqlDao.getLoesungszettelByJahrUndKlassenstufe(jahr, klassenstufe);
	}

	@Override
	public List<Loesungszettel> findLoesungszettelSchulen(final String jahr, final Klassenstufe klassenstufe,
		final List<String> kuerzelliste) {
		return nativeSqlDao.getLoesungszettelSchulen(jahr, klassenstufe, kuerzelliste);
	}

	@Override
	public List<Loesungszettel> findLoesungszettelPrivat(final String jahr, final Klassenstufe klassenstufe) {
		return nativeSqlDao.getLoesungszettelPrivat(jahr, klassenstufe);
	}

	@Override
	public int getAnzahlLoesungszettelPrivatteilnahmen(final String jahr, final Klassenstufe klassenstufe) {
		return teilnehmerFacade.getAnzahlLoesungszettel(jahr, Teilnahmeart.P, klassenstufe);
	}

	@Override
	public List<String> getAuswertbareJahre() {
		return nativeSqlDao.getAuswertbareJahre();
	}

	@Override
	public List<String> findTeilnahmejahreLand(final String kuerzel) {
		if (StringUtils.isBlank(kuerzel)) {
			throw new IllegalArgumentException("kuerzel blank");
		}

		return nativeSqlDao.findTeilnahmejahreLand(kuerzel);
	}

	@Override
	public List<PrivatteilnahmeInformation> getAllPrivatteilnahmeInfosZuJahr(final String jahr) {

		if (StringUtils.isBlank(jahr)) {
			throw new IllegalArgumentException("jahr blank");
		}

		final List<PrivatteilnahmeInformation> teilnahmeInfos = privatteilnahmeInfoDao.findByJahr(jahr);

		return teilnahmeInfos;
	}

	@Override
	public int getAnzahlLoesungszettel(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		return nativeSqlDao.getAnzahlLoesungszettel(teilnahmeIdentifierProvider);

	}

	@Override
	public int getAnzahlKinder(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		return Integer.valueOf("" + teilnehmerFacade.getAnzahlTeilnehmer(teilnahmeIdentifierProvider));
	}

	@Override
	public int getAnzahlUploads(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {
		return Integer.valueOf("" + teilnehmerFacade.getAnzahlUploads(teilnahmeIdentifierProvider));
	}

	@Override
	public int getAnzahlSchulteilnahmenForLehrer(final Long lehrerkontoId) {
		return nativeSqlDao.getAnzahlSchulteilnahmenForLehrer(lehrerkontoId);
	}

	@Override
	public PublicTeilnahme sammleDatenAktuelleLehrerteilnahme(final Lehrerkonto lehrerkonto, final PublicSchule aktuelleSchule,
		final List<LehrerteilnahmeInformation> alleAktuellenTeilnahmeinfos) {

		Optional<LehrerteilnahmeInformation> optEigene = alleAktuellenTeilnahmeinfos.stream()
			.filter(i -> lehrerkonto.getUuid().equals(i.getBenutzerUuid())).findFirst();

		if (optEigene.isPresent()) {
			PublicTeilnahme aktuelleTeilnahme = PublicTeilnahme
				.createFromTeilnahmeIdentifier(optEigene.get().provideTeilnahmeIdentifier());
			new EnhanceTeilnahmeCommand(teilnehmerFacade, aktuelleTeilnahme).execute();

			if (alleAktuellenTeilnahmeinfos.size() > 1) {
				List<LehrerteilnahmeInformation> alleAnderen = alleAktuellenTeilnahmeinfos.stream()
					.filter(i -> !lehrerkonto.getUuid().equals(i.getBenutzerUuid())).collect(Collectors.toList());

				List<String> kollegen = new ArrayList<>(alleAnderen.size());
				for (LehrerteilnahmeInformation t : alleAnderen) {
					kollegen.add(t.getVollenNamen());
				}
				aktuelleTeilnahme.setKollegen(PrettyStringUtils.collectionToDefaultString(kollegen));

			}
			aktuelleTeilnahme.setSchule(aktuelleSchule);
			return aktuelleTeilnahme;

		}
		return null;
	}
}
