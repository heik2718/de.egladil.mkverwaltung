//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import org.slf4j.LoggerFactory;

import de.egladil.common.validation.IPathValidationStrategie;

import org.slf4j.Logger;

/**
 * DateiValidationStrategie
 */
public class DateiValidationStrategie implements IPathValidationStrategie {

	private static final Logger LOG = LoggerFactory.getLogger(DateiValidationStrategie.class);

	/**
	 * @see de.egladil.common.validation.IPathValidationStrategie#checkIsRegular(java.nio.file.Path)
	 */
	@Override
	public void checkIsRegular(Path path) throws IOException {
		if (!Files.isRegularFile(path, new LinkOption[] { LinkOption.NOFOLLOW_LINKS })) {
			LOG.info("{} existiert nicht oder und ist keine Datei", path.toString());
			throw new IOException(path.toString() + " existiert nicht oder ist keine reguläre Datei");
		}
		LOG.debug("{} existiert und ist eine Datei", path.toString());
	}
}
