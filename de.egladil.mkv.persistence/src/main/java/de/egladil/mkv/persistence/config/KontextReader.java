//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.config;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.mkv.persistence.payload.response.Kontext;

/**
 * KontextReader greift auf die Konfigurationsdatei mkvapiconf.json zu und liest sie bei jeder Anfrage neu aus. Dadurch
 * muss bei Änderung der Datei die Anwendung nicht durchgestartet werden.
 */
public class KontextReader {

	private static final String NAME_EXTERNAL_KONTEXT_FILE = "mkvapiconf.json";

	private static final Logger LOG = LoggerFactory.getLogger(KontextReader.class);

	private String pathConfigFile;

	private static KontextReader instance;

	private Kontext mockKontext;

	/**
	 * KontextReader
	 */
	KontextReader() {
	}

	public static KontextReader getInstance() {
		if (instance == null) {
			instance = new KontextReader();
		}
		return instance;
	}

	/**
	 * @param pathConfigRoot String Pfad zum Konfigurationsverzeichnis.
	 * @return KontextReader
	 */
	public KontextReader init(final String pathConfigRoot) {
		if (this.pathConfigFile == null) {
			this.pathConfigFile = pathConfigRoot + File.separator + NAME_EXTERNAL_KONTEXT_FILE;
			// Das Ding wirft eine EgladilConfigurationException, die das hochfahren des Servers unterbricht.
			// Damit wird sichergestellt, dass alle weiteren Zugriffe klappen, solange die Konfig-Datei nicht
			// verschwindet.
			final Kontext kontext = this.getKontext();
			LOG.info("KontextReader initialisiert: {}", kontext);
		}
		return this;
	}

	/**
	 *
	 * @return Kontext jedes Mal neu ausgelesen.
	 * @throws EgladilConfigurationException
	 */
	public Kontext getKontext() throws EgladilConfigurationException {
		if (mockKontext != null) {
			return mockKontext;
		}
		if (this.pathConfigFile == null) {
			throw new IllegalStateException(
				"KontextReader ist nicht initialisiert. Beim Start der Anwendung einmal KontextReader.getInstance().init(...) aufrufen!");
		}
		final File configFile = new File(pathConfigFile);
		final ObjectMapper objectMapper = new ObjectMapper();
		Kontext kontext = null;
		try {
			kontext = objectMapper.readValue(configFile, Kontext.class);
			return kontext;
		} catch (final IOException e) {
			LOG.debug(e.getMessage(), e);
			throw new EgladilConfigurationException("Konnte Konfigurationsfile [" + pathConfigFile + "] nicht lesen:  "
				+ e.getMessage()
				+ " folgendes prüfen: KontextReader.NAME_EXTERNAL_KONTEXT_FILE und configRoot in mkvservice-deb.yml. NAME_EXTERNAL_KONTEXT_FILE="
				+ NAME_EXTERNAL_KONTEXT_FILE, e);
		}
	}



	/**
	 * Singletons sind manchmal nervig und müssen zerstört werden können.
	 */
	public static void destroy() {
		instance = null;
	}

	public void setMockKontextForTest(final Kontext kontext) {
		this.mockKontext = kontext;
	}
}
