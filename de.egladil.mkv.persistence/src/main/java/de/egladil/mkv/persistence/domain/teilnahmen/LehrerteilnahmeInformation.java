//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.bv.aas.domain.BooleanToIntegerConverter;
import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.LandKuerzel;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.mkv.persistence.domain.BenutzerstatusProvider;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;

/**
 * LehrerteilnahmeInformation liefert alle Informationen zu Schulteilnahmen, die für Clients interessant sind
 */
@Entity
@Table(name = "vw_lehrerteilnahmen")
public class LehrerteilnahmeInformation implements IDomainObject, TeilnahmeIdentifierProvider, BenutzerstatusProvider {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonIgnore
	private Long id;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	@Column(name = "kuerzel", length = 8)
	@JsonProperty
	private String kuerzel;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	@Column(name = "vorname")
	@JsonProperty
	private String vorname;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	@Column(name = "nachname")
	@JsonProperty
	private String nachname;

	@NotBlank
	@Email
	@Column(name = "email", length = 255)
	@JsonProperty
	private String email;

	@Column(name = "ben_id")
	@JsonIgnore
	private Long benutzerId;

	@Column(name = "uuid")
	@JsonProperty("uuid")
	private String benutzerUuid;

	@Column(name = "aktiviert")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean aktiviert;

	@Column(name = "anonym")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean anonym;

	@Column(name = "gesperrt")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean gesperrt;

	@Column(name = "mailnachricht")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean mailnachricht;

	@Column(name = "jahr")
	@JsonProperty
	private String jahr;

	@Column(name = "schule")
	@JsonProperty
	private String schulname;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	@Column(name = "schulkuerzel", length = 8)
	@JsonProperty
	private String schulkuerzel;

	@Column(name = "ort")
	@JsonProperty
	private String ortname;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	@Column(name = "ortkuerzel", length = 8)
	@JsonProperty
	private String ortkuerzel;

	@Column(name = "land")
	@JsonProperty
	private String landname;

	@NotNull
	@LandKuerzel
	@Size(max = 5)
	@Column(name = "landkuerzel", length = 8)
	@JsonProperty
	private String landkuerzel;

	public LehrerteilnahmeInformation() {
	}

	public LehrerteilnahmeInformation(final Long id, final String kuerzel, final String vorname, final String nachname,
		final String email, final Long benutzerId, final boolean aktiviert, final boolean anonym, final boolean mailnachricht,
		final String jahr, final String schulname, final String schulkuerzel) {
		this.id = id;
		this.kuerzel = kuerzel;
		this.vorname = vorname;
		this.nachname = nachname;
		this.email = email;
		this.benutzerId = benutzerId;
		this.aktiviert = aktiviert;
		this.anonym = anonym;
		this.mailnachricht = mailnachricht;
		this.jahr = jahr;
		this.schulname = schulname;
		this.schulkuerzel = schulkuerzel;
	}

	@Override
	public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
		return TeilnahmeIdentifier.createSchulteilnahmeIdentifier(schulkuerzel, jahr);
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getVollenNamen() {
		return vorname + " " + nachname;
	}

	/**
	 * Liefert die Membervariable vorname
	 *
	 * @return die Membervariable vorname
	 */
	public String getVorname() {
		return vorname;
	}

	/**
	 * Liefert die Membervariable nachname
	 *
	 * @return die Membervariable nachname
	 */
	public String getNachname() {
		return nachname;
	}

	/**
	 * Liefert die Membervariable email
	 *
	 * @return die Membervariable email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Liefert die Membervariable benutzerId
	 *
	 * @return die Membervariable benutzerId
	 */
	public Long getBenutzerId() {
		return benutzerId;
	}

	/**
	 * Liefert die Membervariable aktiviert
	 *
	 * @return die Membervariable aktiviert
	 */
	@Override
	public boolean isAktiviert() {
		return aktiviert;
	}

	/**
	 * Liefert die Membervariable anonym
	 *
	 * @return die Membervariable anonym
	 */
	@Override
	public boolean isAnonym() {
		return anonym;
	}

	/**
	 * Liefert die Membervariable mailnachricht
	 *
	 * @return die Membervariable mailnachricht
	 */
	public boolean isMailnachricht() {
		return mailnachricht;
	}

	/**
	 * Liefert die Membervariable jahr
	 *
	 * @return die Membervariable jahr
	 */
	public String getJahr() {
		return jahr;
	}

	/**
	 * Liefert die Membervariable schulname
	 *
	 * @return die Membervariable schulname
	 */
	public String getSchulname() {
		return schulname;
	}

	/**
	 * Liefert die Membervariable schulkuerzel
	 *
	 * @return die Membervariable schulkuerzel
	 */
	public String getSchulkuerzel() {
		return schulkuerzel;
	}

	/**
	 * Liefert die Membervariable ortname
	 *
	 * @return die Membervariable ortname
	 */
	public String getOrtname() {
		return ortname;
	}

	/**
	 * Liefert die Membervariable ortkuerzel
	 *
	 * @return die Membervariable ortkuerzel
	 */
	public String getOrtkuerzel() {
		return ortkuerzel;
	}

	/**
	 * Liefert die Membervariable landname
	 *
	 * @return die Membervariable landname
	 */
	public String getLandname() {
		return landname;
	}

	/**
	 * Liefert die Membervariable landkuerzel
	 *
	 * @return die Membervariable landkuerzel
	 */
	public String getLandkuerzel() {
		return landkuerzel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public void setSchulkuerzel(final String schulkuerzel) {
		this.schulkuerzel = schulkuerzel;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final LehrerteilnahmeInformation other = (LehrerteilnahmeInformation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("LehrerteilnahmeInformation [id=");
		builder.append(id);
		builder.append(", benutzerId=");
		builder.append(benutzerId);
		builder.append(", jahr=");
		builder.append(jahr);
		builder.append(", email=");
		builder.append(email);
		builder.append(", schulkuerzel=");
		builder.append(schulkuerzel);
		builder.append(", ortkuerzel=");
		builder.append(ortkuerzel);
		builder.append(", landkuerzel=");
		builder.append(landkuerzel);
		builder.append("]");
		return builder.toString();
	}

	public void setBenutzerId(final Long benutzerId) {
		this.benutzerId = benutzerId;
	}

	public String getBenutzerUuid() {
		return benutzerUuid;
	}

	@Override
	public boolean isGesperrt() {
		return gesperrt;
	}
}
