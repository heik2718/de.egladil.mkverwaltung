//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.common.validation.annotations.UuidString;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * Teilnehmer kapselt die Attribute eines Teilnehmers, die für die AuswertungDownload relevant sind. Eine Referenz auf eine
 * Klassenstufe ist nicht erforderlich
 */
@Entity
@Table(name = "teilnehmer")
public class Teilnehmer implements IDomainObject, TeilnahmeIdentifierProvider {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Version
	@Column(name = "VERSION")
	private int version;

	@NotNull
	@Kuerzel
	@Size(min = 22, max = 22)
	@Column(name = "KUERZEL")
	private String kuerzel;

	@NotNull
	@Column(name = "TEILNAHMEART")
	@Enumerated(EnumType.STRING)
	private Teilnahmeart teilnahmeart;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	@Column(name = "TEILNAHMEKUERZEL")
	private String teilnahmekuerzel;

	@NotNull
	@Size(min = 4, max = 4)
	@Column(name = "JAHR")
	private String jahr;

	@NotNull
	@Column(name = "KLASSENSTUFE")
	@Enumerated(EnumType.STRING)
	private Klassenstufe klassenstufe;

	@Column(name = "NUMMER")
	private int nummer;

	@StringLatin
	@NotBlank
	@Size(max = 55)
	@Column(name = "VORNAME")
	private String vorname;

	@StringLatin
	@Size(max = 55)
	@Column(name = "NACHNAME")
	private String nachname;

	@StringLatin
	@Size(max = 55)
	@Column(name = "ZUSATZ")
	private String zusatz;

	@NotNull
	@Column(name = "SPRACHE")
	@Enumerated(EnumType.STRING)
	private Sprache sprache = Sprache.de;

	@Kuerzel
	@Size(max = 22)
	@Column(name = "LOESUNGSZETTELKUERZEL")
	private String loesungszettelkuerzel;

	@Transient
	private String antwortcode;

	@Transient
	private Integer kaengurusprung;

	@Transient
	private String punkte;

	@ManyToOne()
	@JoinColumn(name = "AUSWERTUNGSGRUPPE")
	private Auswertungsgruppe auswertungsgruppe;

	@Column(name = "EINGABEMODUS")
	@Enumerated(EnumType.STRING)
	private Antworteingabemodus eingabemodus;

	@UuidString
	@Size(max = 40)
	@Column(name = "GEAENDERT_DURCH")
	@JsonProperty
	private String geaendertDurch;


	/**
	 * Teilnehmer
	 */
	Teilnehmer() {
	}

	/**
	 * Teilnehmer
	 */
	Teilnehmer(final TeilnahmeIdentifier teilnahmeIdentifier, final Klassenstufe klassenstufe, final String vorname,
		final String nachname, final String zusatz) {
		this.teilnahmeart = teilnahmeIdentifier.getTeilnahmeart();
		this.teilnahmekuerzel = teilnahmeIdentifier.getKuerzel();
		this.jahr = teilnahmeIdentifier.getJahr();
		this.klassenstufe = klassenstufe;
		this.vorname = vorname;
		this.nachname = nachname;
		this.zusatz = zusatz;
	}

	@Override
	public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
		return TeilnahmeIdentifier.create(teilnahmeart, teilnahmekuerzel, jahr);
	}

	public String getVorname() {
		return vorname;
	}

	public String getNachname() {
		return nachname;
	}

	void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	void setNachname(final String nachname) {
		this.nachname = nachname;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public Teilnahmeart getTeilnahmeart() {
		return teilnahmeart;
	}

	void setTeilnahmeart(final Teilnahmeart teilnahmeart) {
		this.teilnahmeart = teilnahmeart;
	}

	public String getTeilnahmekuerzel() {
		return teilnahmekuerzel;
	}

	void setTeilnahmekuerzel(final String teilnahmekuerzel) {
		this.teilnahmekuerzel = teilnahmekuerzel;
	}

	public String getJahr() {
		return jahr;
	}

	void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public String getLoesungszettelkuerzel() {
		return loesungszettelkuerzel;
	}

	public void setLoesungszettelkuerzel(final String loesungszettelKuerzel) {
		this.loesungszettelkuerzel = loesungszettelKuerzel;
	}

	public String getZusatz() {
		return zusatz;
	}

	void setZusatz(final String bemerkung) {
		this.zusatz = bemerkung;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Teilnehmer [kuerzel=");
		builder.append(kuerzel);
		builder.append(", ");
		builder.append(provideTeilnahmeIdentifier().toString());
		builder.append(", vorname=");
		builder.append(vorname);
		builder.append(", nachname=");
		builder.append(nachname);
		builder.append(", loesungszettelkuerzel=");
		builder.append(loesungszettelkuerzel);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Teilnehmer other = (Teilnehmer) obj;
		if (kuerzel == null) {
			if (other.kuerzel != null) {
				return false;
			}
		} else if (!kuerzel.equals(other.kuerzel)) {
			return false;
		}
		return true;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	void setAuswertungsgruppe(final Auswertungsgruppe auswertungsgruppe) {
		this.auswertungsgruppe = auswertungsgruppe;
	}

	public Auswertungsgruppe getAuswertungsgruppe() {
		return auswertungsgruppe;
	}

	public Klassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	void setKlassenstufe(final Klassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(final int nummer) {
		this.nummer = nummer;
	}

	public String getAntwortcode() {
		return antwortcode;
	}

	public void setAntwortcode(final String antwortcode) {
		this.antwortcode = antwortcode;
	}

	public Integer getKaengurusprung() {
		return kaengurusprung;
	}

	public void setKaengurusprung(final Integer kaengurusprung) {
		this.kaengurusprung = kaengurusprung;
	}

	public String getPunkte() {
		return punkte;
	}

	public void setPunkte(final String punkte) {
		this.punkte = punkte;
	}

	public Antworteingabemodus getEingabemodus() {
		return eingabemodus;
	}

	public void setEingabemodus(final Antworteingabemodus eingabemodus) {
		this.eingabemodus = eingabemodus;
	}

	public String getGeaendertDurch() {
		return geaendertDurch;
	}

	public void setGeaendertDurch(final String geaendertDurch) {
		this.geaendertDurch = geaendertDurch;
	}

	public Sprache getSprache() {
		return sprache;
	}

	public void setSprache(final Sprache sprache) {
		this.sprache = sprache;
	}
}
