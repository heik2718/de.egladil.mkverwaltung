//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.Optional;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAdvVereinbarungDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;

/**
 * AdvVereinbarungDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class AdvVereinbarungDaoImpl extends BaseDaoImpl<AdvVereinbarung> implements IAdvVereinbarungDao {

	/**
	 * AdvVereinbarungDaoImpl
	 */
	@Inject
	public AdvVereinbarungDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<AdvVereinbarung> findBySchulkuerzel(final String schulkuerzel) {

		return this.findByUniqueKey(AdvVereinbarung.class, MKVConstants.SCHULKUERZEL_ATTRIBUTE_NAME, schulkuerzel);
	}
}
