//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Kuerzel;

/**
 * AuswertungsgruppeKuerzel
 */
public class AuswertungsgruppeKuerzel implements ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Kuerzel
	@NotBlank
	@Size(min = 22, max = 22)
	private final String kuerzel;

	/**
	 * AuswertungsgruppeKuerzel
	 */
	public AuswertungsgruppeKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	@Override
	public String toBotLog() {
		return null;
	}

}
