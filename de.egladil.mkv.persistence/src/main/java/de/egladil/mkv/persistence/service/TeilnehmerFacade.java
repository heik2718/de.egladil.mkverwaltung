//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import java.util.List;
import java.util.Optional;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;

/**
 * TeilnehmerFacade stellt die API zum Zugriff auf Teilnehmerinformationen zur Verfügung.
 */
public interface TeilnehmerFacade {

	/**
	 * Gibt die Lösungszettel zu einer Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return List
	 * @throws IllegalArgumentException
	 */
	List<Loesungszettel> getLoesungszettel(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Gibt den Lösungszettel mit dem gegebenen kuerzel zurück, sofern er existiert.
	 *
	 * @param kuerzel String darf nicht blank sein
	 * @return Optional
	 * @throws IllegalArgumentException falls kuerzel blank
	 */
	Optional<Loesungszettel> getUniqueLoesungszettel(String kuerzel) throws IllegalArgumentException;

	/**
	 * Gibt nur die Anzahl der Lösungszettel zurück, ohne die Lösungszettel zu laden
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return long
	 * @throws IllegalArgumentException
	 */
	long getAnzahlLoesungszettel(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Ermittelt die Anzahl der Lösungszettel mit verschiedenen Parametern.
	 *
	 * @param jahr String wenn null, dann alle Jahre.
	 * @param teilnahmeart Teilnahmeart wenn null, dann alle Teilnahmearten
	 * @param klassenstufe Klassenstufe wenn null, dann alle Klassenstufen
	 * @return int
	 */
	int getAnzahlLoesungszettel(String jahr, Teilnahmeart teilnahmeart, Klassenstufe klassenstufe);

	/**
	 * Gibt die Auswertungsgruppen zu einer Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return List
	 * @throws IllegalArgumentException
	 */
	List<Auswertungsgruppe> getAuswertungsgruppen(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Gibt die root eines Auswertungsruppenteilbaums zurück, sofern er existiert.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return Optional
	 * @throws IllegalArgumentException
	 */
	Optional<Auswertungsgruppe> getRootAuswertungsruppe(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Läd die Auswertungsgruppe mit dem gegebenen Kuerzel
	 *
	 * @param kuerzel String
	 * @return Optional
	 */
	Optional<Auswertungsgruppe> getAuswertungsgruppe(String kuerzel);

	/**
	 * Gibt die Teilnehmer zu einer Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return List
	 * @throws IllegalArgumentException
	 */
	List<Teilnehmer> getTeilnehmer(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Gibt die Teilnehmer zu einer Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return List
	 * @throws IllegalArgumentException
	 */
	List<Teilnehmer> getTeilnehmer(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 *
	 * @param teilnehmer
	 * @return Optional
	 */
	Optional<Loesungszettel> getLoesungszettel(Teilnehmer teilnehmer);

	/**
	 * Teilnehmer mit gegebenem Kürzel.
	 *
	 * @param kuerzel String
	 * @return Optional
	 */
	Optional<Teilnehmer> getTeilnehmer(String kuerzel);

	/**
	 * Fragt nur die Anzahl der Teilnehmer ab, ohne diese extra zu laden.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return long
	 */
	long getAnzahlTeilnehmer(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Gibt die UploadInfos zu einer Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return
	 * @return List
	 * @throws IllegalArgumentException
	 */
	List<UploadInfo> getUploadInfos(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Gibt die Anzahl der Uploads zurück, ohne diese tatsächlich zu laden
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return long
	 * @throws IllegalArgumentException
	 */
	long getAnzahlUploads(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Erzeugt und persistiert einen neuen Teilnehmer.
	 *
	 * @param benutzerUuid String
	 * @param teilnahmeIdentifier TeilnahmeIdentifier identifiziert die Teilnahme, zu der dieser gehört
	 * @param teilnehmerPayload PublicTeilnehmer die Daten des Teilnehmers
	 * @return Teilnehmer
	 * @throws IllegalArgumentException falls die Parameter null sind oder nameKlassenstufe nicht korrekt.
	 * @throws PreconditionFailedException falls Teilnahme nicht im aktuellen Wettbewerbsjahr liegt.
	 * @throws EgladilDuplicateEntryException falls es einen gleichen Teilnehmer schon gibt.
	 * @throws EgladilStorageException wenn es zu sonstigen Fehlern beim Persistieren kommt.
	 */
	Teilnehmer createTeilnehmer(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier, PublicTeilnehmer teilnehmerPayload)
		throws IllegalArgumentException, PreconditionFailedException, EgladilDuplicateEntryException, EgladilStorageException;

	/**
	 * Der vorhandenen Teilnehmer wird gespeichert.
	 *
	 * @param benutzerUuid String
	 * @param teilnahmeIdentifier TeilnahmeIdentifier identifiziert die Teilnahme, zu der dieser gehört
	 * @param teilnehmerPayload PublicTeilnehmer die Daten des Teilnehmers
	 * @return Teilnehmer
	 * @throws IllegalArgumentException falls die Parameter null sind.
	 * @throws EgladilDuplicateEntryException falls es einen gleichen Teilnehmer schon gibt.
	 * @throws EgladilStorageException EgladilStorageException wenn es zu sonstigen Fehlern beim Persistieren kommt.
	 */
	Teilnehmer updateTeilnehmer(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier, PublicTeilnehmer teilnehmerPayload)
		throws IllegalArgumentException, PreconditionFailedException, EgladilDuplicateEntryException, EgladilStorageException;

	/**
	 * Der durch das kuerzel spezifizierte Teilnehmer wird gelöscht.
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers.
	 * @param kuerzel String das Teilnehmerkürzel.
	 * @param Optional
	 */
	Optional<Teilnehmer> deleteTeilnehmer(String benutzerUuid, String kuerzel);

	/**
	 * Alle zur durch teilnahmeIdentifier identifizierte Teilnahme gehörenden Teilnehmer werden gelöscht.
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers.
	 * @param teilnahmeIdentifier identifiziert die Teilnahme
	 */
	void deleteAllTeilnehmer(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier);

//	/**
//	 * Gibt alle Lösungszettel der gegebenen Klassenstufe im gegebenen Jahr zurück.
//	 *
//	 * @param jahr String
//	 * @param klassenstufe
//	 * @return List
//	 */
//	List<Loesungszettel> findLoesungszettelByJahrUndKlassenstufe(String jahr, Klassenstufe klassenstufe);
}
