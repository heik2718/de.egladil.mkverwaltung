//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * WettbewerbsloesungenProvider
 */
public class WettbewerbsloesungenProvider {

	private final Map<Klassenstufe, Wettbewerbsloesung> wettbewerbsloesungen = new HashMap<>();

	/**
	 * WettbewerbsloesungenProvider
	 */
	public WettbewerbsloesungenProvider(final IWettbewerbsloesungDao wettbewerbsloesungDao) {
		final String jahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
		final List<Wettbewerbsloesung> alleLoesungen = wettbewerbsloesungDao.findByJahr(jahr);
		for (final Wettbewerbsloesung l : alleLoesungen) {
			wettbewerbsloesungen.put(l.getKlasse(), l);
		}
	}

	public Wettbewerbsloesung getWettbewerbsloesung(final Klassenstufe klassenstufe) {
		return this.wettbewerbsloesungen.get(klassenstufe);
	}
}
