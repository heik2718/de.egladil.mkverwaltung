//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import de.egladil.mkv.persistence.domain.enums.Aufgabenkategorie;

/**
 * AufgabenkategorieAngemessenheitsintervall. Gerechnet wird ganzzahlig mit 2 Nachkomastellen, also alles mit 100 multiplizieren und
 * runden, analog zu Punktklassen.
 */
public class AufgabenkategorieAngemessenheitsintervall {

	private Aufgabenkategorie aufgabenkategorie;

	private int obereSchranke;

	private int untereSchranke;

	/**
	 *
	 * @return
	 */
	public Aufgabenkategorie getAufgabenkategorie() {
		return aufgabenkategorie;
	}

	/**
	 * @param aufgabenkategorie
	 */
	public void setAufgabenkategorie(Aufgabenkategorie aufgabenkategorie) {
		this.aufgabenkategorie = aufgabenkategorie;
	}

	/**
	 *
	 * @return
	 */
	public int getObereSchranke() {
		return obereSchranke;
	}

	/**
	 * @param obereSchranke
	 */
	public void setObereSchranke(int obereSchranke) {
		this.obereSchranke = obereSchranke;
	}

	/**
	 *
	 * @return
	 */
	public int getUntereSchranke() {
		return untereSchranke;
	}

	/**
	 * @param untereSchranke
	 */
	public void setUntereSchranke(int untereSchranke) {
		this.untereSchranke = untereSchranke;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AufgabenkategorieAngemessenheitsintervall [aufgabenkategorie=" + aufgabenkategorie + ", obereSchranke="
			+ obereSchranke + ", untereSchranke=" + untereSchranke + "]";
	}

}
