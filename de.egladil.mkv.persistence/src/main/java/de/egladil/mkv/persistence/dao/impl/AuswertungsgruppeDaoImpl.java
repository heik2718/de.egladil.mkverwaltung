//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AuswertungsgruppeDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class AuswertungsgruppeDaoImpl extends BaseDaoImpl<Auswertungsgruppe> implements IAuswertungsgruppeDao {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungsgruppeDaoImpl.class);

	/**
	 * AuswertungsgruppeDaoImpl
	 */
	@Inject
	public AuswertungsgruppeDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<Auswertungsgruppe> findRootByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		final String stmt = "select a from Auswertungsgruppe a where a.parent is null and a.teilnahmeart = :teilnahmeart and a.teilnahmekuerzel = :teilnahmekuerzel and a.jahr = :jahr";
		final TypedQuery<Auswertungsgruppe> query = getEntityManager().createQuery(stmt, Auswertungsgruppe.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());

		final List<Auswertungsgruppe> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		if (trefferliste.isEmpty()) {
			return Optional.empty();
		}

		if (trefferliste.size() > 1) {
			throw new MKVException(
				"Mehr als eine Root-Auswertungsgruppe mit identifier " + teilnahmeIdentifier.toString() + " gefunden");

		}
		return Optional.of(trefferliste.get(0));
	}

	@Override
	public List<Auswertungsgruppe> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		final String stmt = "select a from Auswertungsgruppe a where a.teilnahmeart = :teilnahmeart and a.teilnahmekuerzel = :teilnahmekuerzel and a.jahr = :jahr";
		final TypedQuery<Auswertungsgruppe> query = getEntityManager().createQuery(stmt, Auswertungsgruppe.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());

		final List<Auswertungsgruppe> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	@Override
	public List<Auswertungsgruppe> findRootgruppenByJahr(final String jahr) {

		if (jahr == null) {
			throw new IllegalArgumentException("jahr null");
		}
		final String stmt = "select a from Auswertungsgruppe a where a.parent is null and a.jahr = :jahr";
		final TypedQuery<Auswertungsgruppe> query = getEntityManager().createQuery(stmt, Auswertungsgruppe.class);
		query.setParameter("jahr", jahr);

		final List<Auswertungsgruppe> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());
		return trefferliste;
	}
}
