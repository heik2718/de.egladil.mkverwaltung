//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.enums.Antwortbuchstabe;

/**
 * PublicAntwortbuchstabe => Antwortbuchstabe
 */
public class PublicAntwortbuchstabe {

	@JsonProperty
	private int code;

	@JsonProperty
	private String buchstabe;

	@JsonProperty
	private int index;

	@JsonIgnore
	private boolean valid;

	/**
	 * PublicAntwortbuchstabe
	 */
	public PublicAntwortbuchstabe() {
	}

	/**
	 * PublicAntwortbuchstabe
	 */
	public PublicAntwortbuchstabe(final String buchstabe, final int code) {
		this.buchstabe = buchstabe;
		this.code = code;
	}

	/**
	 *
	 * @param code
	 * @return
	 */
	public static PublicAntwortbuchstabe fromCode(final int code) {
		final Antwortbuchstabe ab = Antwortbuchstabe.valueOfNummer(code);
		if (Antwortbuchstabe.N == ab) {
			return new PublicAntwortbuchstabe("-", code);
		}
		return new PublicAntwortbuchstabe(ab.toString(), code);
	}

	public final int getCode() {
		return code;
	}

	public final void setCode(final int code) {
		this.code = code;
	}

	public final String getBuchstabe() {
		return buchstabe;
	}

	public final void setBuchstabe(final String buchstabe) {
		this.buchstabe = buchstabe;
	}

	public final boolean isValid() {
		return valid;
	}

	public final void setValid(final boolean valid) {
		this.valid = valid;
	}

	public final int getIndex() {
		return index;
	}

	public final void setIndex(final int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicAntwortbuchstabe [");
		builder.append(buchstabe);
		builder.append(", ");
		builder.append(code);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buchstabe == null) ? 0 : buchstabe.hashCode());
		result = prime * result + code;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PublicAntwortbuchstabe other = (PublicAntwortbuchstabe) obj;
		if (buchstabe == null) {
			if (other.buchstabe != null) {
				return false;
			}
		} else if (!buchstabe.equals(other.buchstabe)) {
			return false;
		}
		if (code != other.code) {
			return false;
		}
		return true;
	}

}
