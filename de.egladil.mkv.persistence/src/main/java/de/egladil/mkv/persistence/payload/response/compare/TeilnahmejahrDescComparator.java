//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmejahr;

/**
 * TeilnahmejahrDescComparator
 */
public class TeilnahmejahrDescComparator implements Comparator<Teilnahmejahr> {

	@Override
	public int compare(final Teilnahmejahr arg0, final Teilnahmejahr arg1) {
		return arg0.getJahr().compareTo(arg1.getJahr()) * (-1);
	}

}
