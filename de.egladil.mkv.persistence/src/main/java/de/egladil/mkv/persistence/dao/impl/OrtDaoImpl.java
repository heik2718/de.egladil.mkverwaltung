//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.OrtLage;

/**
 * OrtDao
 */
@Singleton
@MKVPersistenceUnit
public class OrtDaoImpl extends BaseDaoImpl<Ort> implements IOrtDao {

	private static final Logger LOG = LoggerFactory.getLogger(OrtDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von OrtDaoImpl
	 */
	@Inject
	public OrtDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IOrtDao#findOrteInLandUnscharf(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Ort> findOrteInLandUnscharf(final String landkuerzel, final String ortname)
		throws MKVException, PersistenceException {
		if (landkuerzel == null) {
			throw new MKVException("landkuerzel darf nicht null sein");
		}
		if (ortname == null) {
			throw new MKVException("ortname darf nicht null sein");
		}
		final String stmt = "select o.* from kat_orte o, kat_laender l where o.land = l.id and l.kuerzel = :landkuerzel and lower(o.name) like :name";

		final Query query = getEntityManager().createNativeQuery(stmt, Ort.class);

		query.setParameter("landkuerzel", landkuerzel);
		query.setParameter("name", "%" + ortname.toLowerCase() + "%");

		final List<Ort> schulen = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", schulen.size());

		return schulen;
	}

	@Override
	public List<Ort> findOrte(final String namensteilOrt, final String namensteilLand) {

		if (StringUtils.isBlank(namensteilOrt)) {
			throw new IllegalArgumentException("namensteilOrt blank");
		}

		String stmt = null;
		final boolean mitLand = StringUtils.isNotBlank(namensteilLand);
		if (mitLand) {
			stmt = "select o.id, o.kuerzel, o.name, o.land from kat_orte o, kat_laender l where o.land = l.id and lower(l.name) like :landName and lower(o.name) like :ortName";
		} else {
			stmt = "select o.id, o.kuerzel, o.name, o.land from kat_orte o where lower(o.name) like :ortName";
		}

		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("ortName", "%" + namensteilOrt.toLowerCase() + "%");

		if (mitLand) {
			query.setParameter("landName", "%" + namensteilLand.toLowerCase() + "%");
		}

		@SuppressWarnings("unchecked")
		final List<Object[]> trefferliste = query.getResultList();
		final List<Ort> result = new ArrayList<>(trefferliste.size());
		for (final Object[] o : trefferliste) {
			final Ort ort = new Ort();
			ort.setId(Long.valueOf((Integer) o[0]));
			ort.setKuerzel((String) o[1]);
			ort.setName((String) o[2]);
			ort.setLandId(Long.valueOf((Integer) o[3]));
			result.add(ort);
		}

		LOG.debug("Anzahl Treffer (ohne Land): {}", result.size());
		return result;
	}

	@Override
	public OrtLage getOrtLage(final String ortKuerzel) {

		if (StringUtils.isBlank(ortKuerzel)) {
			throw new IllegalArgumentException("ortKuerzel blank");
		}

		final String stmt = "select l.kuerzel, l.name from kat_orte o, kat_laender l where o.land = l.id and o.kuerzel = :kuerzel";
		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("kuerzel", ortKuerzel);

		@SuppressWarnings("unchecked")
		final List<Object[]> trefferliste = query.getResultList();
		if (trefferliste.isEmpty()) {
			return null;
		}
		if (trefferliste.size() > 1) {
			throw new MKVException(
				"Datenfehler: mehr als ein Ort mit kuerzel='" + ortKuerzel + "' gefunden (insgesamt " + trefferliste.size() + ").");
		}
		final Object[] treffer = trefferliste.get(0);
		final OrtLage result = new OrtLage((String) treffer[0], (String) treffer[1]);
		return result;

	}

	@Override
	public BigInteger anzahlTreffer(final String namensteilOrt, final String namensteilLand) {

		if (StringUtils.isBlank(namensteilOrt)) {
			throw new IllegalArgumentException("namensteilOrt blank");
		}
		final boolean mitLand = StringUtils.isNotBlank(namensteilLand);

		String stmt = null;
		if (StringUtils.isBlank(namensteilLand)) {
			stmt = "select count(*) from kat_orte o where lower(o.name) like :ortName";
		} else {
			stmt = "select count(*) from kat_orte o, kat_laender l where o.land = l.id and lower(l.name) like :landName and lower(o.name) like :ortName";
		}

		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("ortName", "%" + namensteilOrt.toLowerCase() + "%");
		if (mitLand) {
			query.setParameter("landName", "%" + namensteilLand.toLowerCase() + "%");
		}
		return SqlUtils.getCount(query);
	}

}
