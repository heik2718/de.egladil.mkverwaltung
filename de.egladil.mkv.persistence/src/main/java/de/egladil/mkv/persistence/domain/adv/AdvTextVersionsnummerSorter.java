//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.adv;

import java.util.Comparator;

/**
 * AdvTextVersionsnummerSorter
 */
public class AdvTextVersionsnummerSorter implements Comparator<AdvText> {

	@Override
	public int compare(final AdvText arg0, final AdvText arg1) {
		return arg0.getVersionsnummer().compareToIgnoreCase(arg1.getVersionsnummer());
	}
}
