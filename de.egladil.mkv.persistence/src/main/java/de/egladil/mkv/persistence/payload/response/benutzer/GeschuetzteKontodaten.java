//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.benutzer;

/**
 * GeschuetzteKontodaten für Admin-App.
 */
public class GeschuetzteKontodaten {

	private String uuid;

	private String lastAccess;

	private String datumGeaendert;

	private boolean aktiviert;

	private boolean gesperrt;

	private boolean anonym;

	public final String getLastAccess() {
		return lastAccess;
	}

	public final void setLastAccess(final String lastAccess) {
		this.lastAccess = lastAccess;
	}

	public final String getDatumGeaendert() {
		return datumGeaendert;
	}

	public final void setDatumGeaendert(final String datumGeaendert) {
		this.datumGeaendert = datumGeaendert;
	}

	public final String getUuid() {
		return uuid;
	}

	public final void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	public final boolean isAktiviert() {
		return aktiviert;
	}

	public final void setAktiviert(final boolean aktiviert) {
		this.aktiviert = aktiviert;
	}

	public final boolean isGesperrt() {
		return gesperrt;
	}

	public final void setGesperrt(final boolean gesperrt) {
		this.gesperrt = gesperrt;
	}

	public final boolean isAnonym() {
		return anonym;
	}

	public final void setAnonym(final boolean anonym) {
		this.anonym = anonym;
	}
}
