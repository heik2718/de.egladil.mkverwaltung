//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

/**
 * NormalizeWortgruppeInvoker normalisiert ein einzelnes Wort, indem führende und endende Leerzeichen entfernt und
 * innere Leerzeichen geschrumpft werden.
 */
public class NormalizeWortgruppeInvoker {

	public String normalize(final String wortgruppe) {
		final Text text = new Text(new String[] { wortgruppe });
		new TrimShrinkCommand(text).execute();
		return text.getTransformedText().get(0);
	}
}
