//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * ConcStringsCommandügt die Worte zu einer durch Leerzeichen getrennten Liste zusammen. Leerzeichen im Inneren werden
 * nicht geschrumpf
 */
public class ConcatStringsCommand implements ICommand {

	private final Text receiver;

	/**
	 * ConcatStringsCommand
	 *
	 * @param receiver List darf nicht null sein
	 */
	public ConcatStringsCommand(final Text receiver) {
		if (receiver == null) {
			throw new IllegalArgumentException("receiver null");
		}
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		final List<String> transformed = new ArrayList<>();
		final StringBuffer sb = new StringBuffer();
		for (final String wort : receiver.getText()) {
			if (!StringUtils.isBlank(wort)) {
				sb.append(wort);
				sb.append(" ");
			}
		}
		if (!StringUtils.isBlank(sb.toString())) {
			transformed.add(sb.toString().trim());
		}
		receiver.setTransformedText(transformed);
	}
}
