//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author heike
 *
 */
@Converter
public class BooleanToIntegerConverter implements AttributeConverter<Boolean, Integer> {

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang. Object)
	 */
	@Override
	public Integer convertToDatabaseColumn(Boolean attribute) {
		if (attribute == null) {
			return 0;
		}
		return attribute ? 1 : 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang. Object)
	 */
	@Override
	public Boolean convertToEntityAttribute(Integer dbData) {
		if (dbData == null) {
			return false;
		}
		return dbData.intValue() == 1 ? true : false;
	}
}
