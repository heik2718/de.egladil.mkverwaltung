//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.utils.PublicAuswertungsgruppeErzeuger;

/**
 * PublicAuswertungsgruppe
 */
public class PublicAuswertungsgruppe implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private String kuerzel;

	@JsonProperty
	private String teilnahmeart;

	@JsonProperty
	private String teilnahmekuerzel;

	@JsonProperty
	private String jahr;

	@JsonProperty
	private PublicKlassenstufe klassenstufe;

	@JsonProperty
	private String name;

	@JsonProperty
	private int anzahlKlassen = 0;

	@JsonProperty
	private int anzahlKinder = 0;

	@JsonProperty
	private int anzahlLoesungszettel = 0;

	@JsonProperty
	private PublicFarbschema farbschema;

	@JsonIgnore
	private PublicAuswertungsgruppe parent;

	@JsonProperty
	private List<PublicAuswertungsgruppe> auswertungsgruppen = new ArrayList<>();

	@JsonProperty
	private List<PublicTeilnehmer> teilnehmer = new ArrayList<>();

	@JsonProperty
	private boolean lazy;

	public static PublicAuswertungsgruppe fromAuswertungsgruppe(final Auswertungsgruppe auswertungsgruppe, final boolean lazy) {
		return new PublicAuswertungsgruppeErzeuger().execute(auswertungsgruppe, lazy).getResult();
	}

	/**
	 * PublicAuswertungsgruppe
	 */
	public PublicAuswertungsgruppe() {
	}

	public void addAuswertungsgruppe(final PublicAuswertungsgruppe publicAuswertungsgruppe) {
		auswertungsgruppen.add(publicAuswertungsgruppe);
	}

	public void addTeilnehmer(final PublicTeilnehmer theTeilnehmer) {
		teilnehmer.add(theTeilnehmer);
	}

	public final String getKuerzel() {
		return kuerzel;
	}

	public final void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public final String getTeilnahmeart() {
		return teilnahmeart;
	}

	public final void setTeilnahmeart(final String teilnahmeart) {
		this.teilnahmeart = teilnahmeart;
	}

	public final String getTeilnahmekuerzel() {
		return teilnahmekuerzel;
	}

	public final void setTeilnahmekuerzel(final String teilnahmekuerzel) {
		this.teilnahmekuerzel = teilnahmekuerzel;
	}

	public final String getJahr() {
		return jahr;
	}

	public final void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public final String getName() {
		return name;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final PublicFarbschema getFarbschema() {
		return farbschema;
	}

	public final void setFarbschema(final PublicFarbschema farbschema) {
		this.farbschema = farbschema;
	}

	public final List<PublicTeilnehmer> getTeilnehmer() {
		return teilnehmer;
	}

	public final PublicAuswertungsgruppe getParent() {
		return parent;
	}

	public final void setParent(final PublicAuswertungsgruppe parent) {
		this.parent = parent;
	}

	public final List<PublicAuswertungsgruppe> getAuswertungsgruppen() {
		return auswertungsgruppen;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicAuswertungsgruppe [kuerzel=");
		builder.append(kuerzel);
		builder.append(", teilnahmeart=");
		builder.append(teilnahmeart);
		builder.append(", teilnahmekuerzel=");
		builder.append(teilnahmekuerzel);
		builder.append(", jahr=");
		builder.append(jahr);
		builder.append(", klassenstufe=");
		builder.append(klassenstufe);
		builder.append(", name=");
		builder.append(name);
		builder.append(", farbschema=");
		builder.append(farbschema);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PublicAuswertungsgruppe other = (PublicAuswertungsgruppe) obj;
		if (kuerzel == null) {
			if (other.kuerzel != null) {
				return false;
			}
		} else if (!kuerzel.equals(other.kuerzel)) {
			return false;
		}
		return true;
	}

	public final boolean isLazy() {
		return lazy;
	}

	public final void setLazy(final boolean lazy) {
		this.lazy = lazy;
	}

	public final PublicKlassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	public final void setKlassenstufe(final PublicKlassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
	}

	public final int getAnzahlKinder() {
		return anzahlKinder;
	}

	public final void setAnzahlKinder(final int anzahlKinder) {
		this.anzahlKinder = anzahlKinder;
	}

	public final int getAnzahlLoesungszettel() {
		return anzahlLoesungszettel;
	}

	public final void setAnzahlLoesungszettel(final int anzahlLoesungszettel) {
		this.anzahlLoesungszettel = anzahlLoesungszettel;
	}

	public final int getAnzahlKlassen() {
		return anzahlKlassen;
	}

	public final void setAnzahlKlassen(final int anzahlKlassen) {
		this.anzahlKlassen = anzahlKlassen;
	}

}
