//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.config;

/**
 * MKVConstants
 */
public interface MKVConstants {

	String ID_ATTRIBUTE_NAME = "id";

	String KUERZEL_ATTRIBUTE_NAME = "kuerzel";

	String SCHLUESSEL_ATTRIBUTE_NAME = "schluessel";

	String NAME_ATTRIBUTE_NAME = "name";

	String SCHULKUERZEL_ATTRIBUTE_NAME = "schulkuerzel";

	String TEILNAHME_KUERZEL_ATTRIBUTE_NAME = "teilnahme.kuerzel";

	String VERSIONSNUMMER_ATTRIBUTE_NAME = "versionsnummer";

	String UUID_ATTRIBUTE_NAME = "uuid";

	String BENUTZER_UUID_ATTRIBUTE_NAME = "benutzerUuid";

	String DOWNLOADCODE_ATTRIBUT_NAME = "downloadcode";

	String CONFIRM_CODE_ATTRIBUTE_NAME = "confirm_code";

	// String CONFIG_KEY_RESET_PASSWORD_EXPIRE = "mail.resetpassword.expireinterval";

	String CONFIG_KEY_RESET_PASSWORD_DUMMYCODE = "mail.resetpassword.dummycode";

	int LAENGE_SCHULKUERZEL = 8;

	int LAENGE_ORTSKUERZEL = 10;

	char[] SCHULKURZEL_CHARS = "ABCDEFGHIJKLMNOPQRTSUVWXYZ0123456789".toCharArray();

	char[] ORT_CHARS = "ABCDEFGHIJKLMNOPQRTSUVWXYZ0123456789".toCharArray();

	int DEFAULT_LENGTH = 8;

	char[] DEFAULT_CHARS = "ABCDEFGHIJKLMNOPQRTSUVWXYZ0123456789".toCharArray();

	String GRUPPE_PRIVATTEILNAHMEN_KUERZEL = "PRIVAT";

}
