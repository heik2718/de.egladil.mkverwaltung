//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

/**
 * ImageSerialisierung
 */
public class ImageSerialisierung {

	private final byte[] data;

	private String base64Image;

	public ImageSerialisierung(final byte[] data) {
		if (data == null) {
			throw new IllegalArgumentException("data null");
		}
		this.data = data;
	}

	public final byte[] getData() {
		return data;
	}

	public final String getBase64Image() {
		return base64Image;
	}

	public final void setBase64Image(final String base64Image) {
		this.base64Image = base64Image;
	}

}
