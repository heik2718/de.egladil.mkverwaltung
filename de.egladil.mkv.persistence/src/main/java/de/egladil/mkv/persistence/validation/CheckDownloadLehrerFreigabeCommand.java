//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

/**
 * CheckDownloadLehrerFreigabeCommand
 */
public class CheckDownloadLehrerFreigabeCommand extends AbstractCheckDownloadFreigabeCommand {

	private static final String FREISCHALTUNG_MARKER = "lehrer.txt";

	/**
	 * Erzeugt eine Instanz von CheckDownloadLehrerFreigabeCommand
	 */
	public CheckDownloadLehrerFreigabeCommand(String downloadPath) {
		super(downloadPath);
	}

	/**
	 * @see de.egladil.mkv.persistence.validation.AbstractCheckDownloadFreigabeCommand#getMarkerFileName()
	 */
	@Override
	protected String getMarkerFileName() {
		return FREISCHALTUNG_MARKER;
	}
}
