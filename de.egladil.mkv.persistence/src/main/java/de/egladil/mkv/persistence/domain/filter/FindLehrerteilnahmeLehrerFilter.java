//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.filter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;

/**
 * FindLehrerteilnahmeLehrerFilter
 */
public class FindLehrerteilnahmeLehrerFilter {

	private final long lehrerBenutzerId;

	/**
	 * Erzeugt eine Instanz von FindLehrerteilnahmeLehrerFilter
	 */
	public FindLehrerteilnahmeLehrerFilter(final long lehrerBenutzerId) {
		this.lehrerBenutzerId = lehrerBenutzerId;
	}

	/**
	 * Sucht aus der gegebenen Liste alle heraus, die zur lehrerBenutzerId passen. TODO
	 *
	 * @param lehrerteilnahmen
	 * @return
	 */
	public List<LehrerteilnahmeInformation> findTeilnahme(final List<LehrerteilnahmeInformation> lehrerteilnahmen) {
		if (lehrerteilnahmen == null) {
			throw new NullPointerException("lehrerteilnahmen");
		}
		final Set<LehrerteilnahmeInformation> trefferliste = new HashSet<>();
		for (final LehrerteilnahmeInformation i : lehrerteilnahmen) {
			if (lehrerBenutzerId == i.getBenutzerId().longValue()) {
				trefferliste.add(i);
			}
		}
		return new ArrayList<>(trefferliste);
	}
}
