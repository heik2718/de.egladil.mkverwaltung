//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * PublicLand
 */
public class PublicLand {

	@JsonProperty
	private int level = 0;

	@JsonProperty
	private String name;

	@JsonProperty
	private String kuerzel;

	@JsonProperty
	private HateoasPayload hateoasPayload;

	@JsonProperty
	private int anzahlOrte;

	@JsonProperty
	private List<PublicOrt> kinder = new ArrayList<>();

	@JsonProperty
	private List<String> teilnahmejahre = new ArrayList<>();

	/**
	 * Erzeugt ein Public Land ohne Orte
	 *
	 * @param land
	 * @return
	 */
	public static PublicLand createLazy(final Land land) {
		if (land == null) {
			throw new MKVException("land darf nicht null sein");
		}
		final PublicLand entity = new PublicLand(land.getName(), land.getKuerzel());
		entity.hateoasPayload = new HateoasPayload(land.getKuerzel(), "/laender/" + land.getKuerzel());
		return entity;
	}

	/**
	 * Erzeugt eine Instanz von PublicLand aus Land.
	 *
	 * @param land
	 * @return
	 */
	public static PublicLand createMitOrten(final Land land) throws MKVException {
		if (land == null) {
			throw new MKVException("land darf nicht null sein");
		}
		final PublicLand entity = new PublicLand(land.getName(), land.getKuerzel());
		final List<PublicOrt> orte = new ArrayList<>();

		if (land.getOrte() != null) {
			for (final Ort ort : land.getOrte()) {
				orte.add(new PublicOrt(ort.getName(), ort.getKuerzel()));
			}
		}
		entity.setKinder(orte);
		return entity;
	}

	/**
	 * Erzeugt eine Instanz von PublicLand
	 */
	public PublicLand() {
	}

	/**
	 * Erzeugt eine Instanz von PublicLand
	 */
	public PublicLand(final String name, final String kuerzel) {
		this.name = name;
		this.kuerzel = kuerzel;
	}

	public void addAllTeilnahmeJahre(final List<String> jahre) {
		this.teilnahmejahre.addAll(jahre);
		Collections.sort(this.teilnahmejahre);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PublicLand other = (PublicLand) obj;
		if (kuerzel == null) {
			if (other.kuerzel != null)
				return false;
		} else if (!kuerzel.equals(other.kuerzel))
			return false;
		return true;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public void setKinder(final List<PublicOrt> orte) {
		this.kinder = orte;
	}

	public String getName() {
		return name;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public List<PublicOrt> getKinder() {
		return kinder;
	}

	public final HateoasPayload getHateoasPayload() {
		return hateoasPayload;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}

	public final int getAnzahlOrte() {
		return anzahlOrte;
	}

	public final void setAnzahlOrte(final int anzahlOrte) {
		this.anzahlOrte = anzahlOrte;
	}

	public final List<String> getTeilnahmejahre() {
		return teilnahmejahre;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicLand [name=");
		builder.append(name);
		builder.append(", kuerzel=");
		builder.append(kuerzel);
		builder.append("]");
		return builder.toString();
	}

}
