//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmegruppe;
import de.egladil.mkv.persistence.payload.response.teilnahmen.TeilnahmenFilter;

/**
 * TeilnahmegruppenNameComparator
 */
public class TeilnahmegruppenNameComparator implements Comparator<Teilnahmegruppe> {

	@Override
	public int compare(final Teilnahmegruppe arg0, final Teilnahmegruppe arg1) {
		final TeilnahmenFilter filter0 = arg0.getFilter();
		final TeilnahmenFilter filter1 = arg1.getFilter();

		return filter0.getName().compareToIgnoreCase(filter1.getName());
	}
}
