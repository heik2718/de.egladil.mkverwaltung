//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

/**
 * Auswertungsquelle ist die Quelle, aus der die Auswertung stammt.
 */
public enum Auswertungsquelle {

	UPLOAD,
	ONLINE

}
