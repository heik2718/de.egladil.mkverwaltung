//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;

import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * CheckTeilnehmerDuplicateCommand prüft, ob das update eines Teilnehmers zum Erzeugen einer Dublette führen würde. In
 * diesem Fall wirft es eine EgladilDuplicateEntryException.
 */
public class CheckTeilnehmerDuplicateCommand implements ICommand {

	private final TeilnahmeIdentifier teilnahmeIdentifier;

	private final PublicTeilnehmer teilnehmerPayload;

	private final List<Teilnehmer> alleZurTeilnahme;

	/**
	 * CheckTeilnehmerDuplicateCommand
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param teilnehmerPayload TeilnehmerPayload
	 * @param alleZurTeilnahme List
	 */
	public CheckTeilnehmerDuplicateCommand(final TeilnahmeIdentifier teilnahmeIdentifier, final PublicTeilnehmer teilnehmerPayload,
		final List<Teilnehmer> alleZurTeilnahme) {
		this.teilnahmeIdentifier = teilnahmeIdentifier;
		this.teilnehmerPayload = teilnehmerPayload;
		this.alleZurTeilnahme = alleZurTeilnahme;
	}

	@Override
	public void execute() {
		for (final Teilnehmer teilnehmer : alleZurTeilnahme) {
			if (hasEqualAttributes(teilnehmer) && !teilnehmer.getKuerzel().equals(teilnehmerPayload.getKuerzel())) {
				final EgladilDuplicateEntryException ex = new EgladilDuplicateEntryException("Teilnehmerduplicate");
				ex.setUniqueIndexName("uk_teilnehmer_2");
				throw ex;
			}
		}
	}

	boolean hasEqualAttributes(final Teilnehmer teilnehmer) {
		if (teilnehmer.getTeilnahmeart() != teilnahmeIdentifier.getTeilnahmeart()) {
			return false;
		}
		if (teilnehmer.getTeilnahmekuerzel() == null) {
			if (teilnahmeIdentifier.getKuerzel() != null) {
				return false;
			}
		} else if (!teilnehmer.getTeilnahmekuerzel().equals(teilnahmeIdentifier.getKuerzel())) {
			return false;
		}
		if (teilnehmer.getAuswertungsgruppe() == null) {
			if (teilnehmerPayload.getAuswertungsgruppe() != null) {
				return false;
			}
		} else if (!teilnehmer.getAuswertungsgruppe().getKuerzel().equals(teilnehmerPayload.getAuswertungsgruppe().getKuerzel())) {
			return false;
		}
		if (teilnehmer.getJahr() == null) {
			if (teilnahmeIdentifier.getJahr() != null) {
				return false;
			}
		} else if (!teilnehmer.getJahr().equals(teilnahmeIdentifier.getJahr())) {
			return false;
		}
		if (!teilnehmer.getKlassenstufe().name().equals(teilnehmerPayload.getKlassenstufe().getName())) {
			return false;
		}
		if (teilnehmer.getNachname() == null) {
			if (teilnehmerPayload.getNachname() != null) {
				return false;
			}
		} else if (teilnehmerPayload.getNachname() == null) {
			if (teilnehmer.getNachname() != null) {
				return false;
			}
		} else if (!teilnehmer.getNachname().trim().equalsIgnoreCase(teilnehmerPayload.getNachname().trim())) {
			return false;
		}
		if (teilnehmer.getVorname() == null) {
			if (teilnehmerPayload.getVorname() != null) {
				return false;
			}
		} else if (!teilnehmer.getVorname().trim().equalsIgnoreCase(teilnehmerPayload.getVorname().trim())) {
			return false;
		}
		if (teilnehmer.getZusatz() == null) {
			if (teilnehmerPayload.getZusatz() != null) {
				return false;
			}
		} else if (teilnehmerPayload.getZusatz() == null) {
			if (teilnehmer.getZusatz() != null) {
				return false;
			}
		} else if (!teilnehmer.getZusatz().trim().equalsIgnoreCase(teilnehmerPayload.getZusatz().trim())) {
			return false;
		}

		return true;
	}

}
