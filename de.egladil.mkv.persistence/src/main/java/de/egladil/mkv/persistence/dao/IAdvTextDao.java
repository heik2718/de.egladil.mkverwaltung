//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.adv.AdvText;

/**
 * IAdvTextDao
 */
public interface IAdvTextDao extends IBaseDao<AdvText> {

}
