//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;
import java.util.Optional;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.teilnahmen.PrivatteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * IPrivatteilnahmeInfoDao
 */
public interface IPrivatteilnahmeInfoDao extends IBaseDao<PrivatteilnahmeInformation> {

	/**
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return Optional
	 * @throws EgladilStorageException wenns mehr als eins gibt
	 */
	Optional<PrivatteilnahmeInformation> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier)
		throws EgladilStorageException;

	/**
	 * Alle Privatteilnahmen in einem Jahr.
	 *
	 * @param jahr String darf nicht blank sein
	 * @return List
	 */
	List<PrivatteilnahmeInformation> findByJahr(String jahr);

}
