//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

/**
 * Wertung der Lösung (falsch, richtig, nicht da)
 */
public enum Wertung {
	f,
	n,
	r;

	public static Wertung valueOfStringIgnoringCase(String str) {
		if (str == null) {
			throw new IllegalArgumentException("str darf nicht null sein");
		}
		return Wertung.valueOf(str.toLowerCase());
	}

}
