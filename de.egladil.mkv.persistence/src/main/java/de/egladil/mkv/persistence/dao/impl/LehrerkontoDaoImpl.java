//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * LehrerkontaktDao
 */
@Singleton
@MKVPersistenceUnit
public class LehrerkontoDaoImpl extends BaseDaoImpl<Lehrerkonto> implements ILehrerkontoDao {

	private static final Logger LOG = LoggerFactory.getLogger(LehrerkontoDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von LehrerkontoDaoImpl
	 */
	@Inject
	public LehrerkontoDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IMKVKontoDao#findByUUID(java.lang.String)
	 */
	@Override
	public Optional<Lehrerkonto> findByUUID(final String uuid) {
		return super.findByUniqueKey(Lehrerkonto.class, MKVConstants.UUID_ATTRIBUTE_NAME, uuid);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IMKVKontoDao#findByPerson(de.egladil.mkv.persistence.domain.user.Person)
	 */
	@Override
	public List<Lehrerkonto> findByPerson(final Person person) {
		if (person == null) {
			throw new MKVException("Parameter person darf nicht null sein");
		}
		final String stmt = "select l from Lehrerkonto l where lower(l.person.vorname) = :vorname and lower(l.person.nachname) = :nachname";
		final TypedQuery<Lehrerkonto> query = getEntityManager().createQuery(stmt, Lehrerkonto.class);
		query.setParameter("vorname", person.getVorname().toLowerCase());
		query.setParameter("nachname", person.getNachname().toLowerCase());
		final List<Lehrerkonto> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());
		return trefferliste;
	}
}
