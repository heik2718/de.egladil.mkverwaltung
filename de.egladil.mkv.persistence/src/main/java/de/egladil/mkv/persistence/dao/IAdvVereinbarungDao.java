//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.Optional;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;

/**
 * IAdvVereinbarungDao
 */
public interface IAdvVereinbarungDao extends IBaseDao<AdvVereinbarung> {

	/**
	 *
	 * @param schulkuerzel String
	 * @return Optional
	 */
	Optional<AdvVereinbarung> findBySchulkuerzel(String schulkuerzel);

}
