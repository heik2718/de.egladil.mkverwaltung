//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.persistence.service.IdentifierMKVKontoProvider;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * Anonymsierungsservice
 */
@Singleton
public class Anonymsierungsservice implements IAnonymisierungsservice {

	private static final Logger LOG = LoggerFactory.getLogger(Anonymsierungsservice.class);

	private final IBenutzerService benutzerService;

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final IProtokollService protokollService;

	private final KuerzelGenerator kuerzelGenerator;

	/**
	 * Erzeugt eine Instanz von Anonymsierungsservice
	 */
	@Inject
	public Anonymsierungsservice(final IBenutzerService benutzerService, final ILehrerkontoDao lehrerkontoDao,
		final IPrivatkontoDao privatkontoDao, final IProtokollService protokollService) {
		this.benutzerService = benutzerService;
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.protokollService = protokollService;
		kuerzelGenerator = new KuerzelGenerator();
	}

	@Override
	public String kontoAnonymsieren(final String benutzerUuid, final String contextMessage) throws IllegalArgumentException {
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid");
		}
		if (StringUtils.isBlank(contextMessage)) {
			throw new IllegalArgumentException("contextMessage");
		}
		final Optional<IMKVKonto> optKontakt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(benutzerUuid,
			lehrerkontoDao, privatkontoDao);
		return kontoAnonymsieren(benutzerUuid, optKontakt, contextMessage);
	}

	@Override
	public String kontoAnonymsieren(final String benutzerUuid, final Optional<IMKVKonto> konto, final String contextMessage) {
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid");
		}
		if (konto == null) {
			throw new IllegalArgumentException("konto");
		}
		if (StringUtils.isBlank(contextMessage)) {
			throw new IllegalArgumentException("contextMessage");
		}
		final Benutzerkonto benutzer = benutzerService.findBenutzerkontoByUUID(benutzerUuid);

		boolean benutzerAnonymsieren = true;
		boolean mkvAnonymsieren = true;
		if (benutzer == null || benutzer.isAnonym()) {
			benutzerAnonymsieren = false;
		}
		if (!konto.isPresent() || konto.get().getPerson().isAnonym()) {
			mkvAnonymsieren = false;
		}
		if (!benutzerAnonymsieren && !mkvAnonymsieren) {
			return "";
		}

		boolean benuztzerSuccess = true;
		boolean mkvSuccess = true;
		if (benutzerAnonymsieren) {
			benuztzerSuccess = benutzerAnonymsieren(benutzer, contextMessage);
		}
		if (mkvAnonymsieren) {
			mkvSuccess = mkvKontoAnonymsieren(konto.get(), contextMessage);
		}

		if (benuztzerSuccess && mkvSuccess) {
			final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), benutzerUuid, contextMessage);
			protokollService.protokollieren(ereignis);
		}

		return "";
	}

	private boolean benutzerAnonymsieren(final Benutzerkonto benutzer, final String contextMessage) {
		final String loginAnonymisiert = kuerzelGenerator.generateDefaultKuerzel() + "-anonymisiert-" + System.currentTimeMillis();
		benutzer.setLoginName(loginAnonymisiert);
		benutzer.setEmail(loginAnonymisiert + "@egladil.de");
		benutzer.setGesperrt(true);
		benutzer.setAnonym(true);

		String errm = null;

		try {
			benutzerService.persistBenutzerkonto(benutzer);
		} catch (EgladilConcurrentModificationException | EgladilDuplicateEntryException e) {
			LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + "Benutzerkonto konnte nicht anonymisiert werden: [UUID={}]",
				benutzer.getUuid());
			errm = e.getClass().getSimpleName();
		} catch (final EgladilStorageException e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DATENMUELL + "Benutzerkonto konnte nicht anonymisiert werden: [UUID={}]",
				benutzer.getUuid(), e);
			errm = e.getClass().getSimpleName();
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "unerwartete Exception beim Anonymsieren Benutzerkonto [UUID={}]",
				benutzer.getUuid(), e);
			errm = e.getClass().getSimpleName();
		}

		if (errm != null) {
			final String was = contextMessage + ": Benutzer " + errm;
			final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), benutzer.getUuid(), was);
			protokollService.protokollieren(ereignis);
		}

		return errm == null;
	}

	private boolean mkvKontoAnonymsieren(final IMKVKonto konto, final String contextMessage) {
		final Person person = konto.getPerson();
		person.setVorname(kuerzelGenerator.generateDefaultKuerzel());
		person.setNachname(kuerzelGenerator.generateDefaultKuerzel());
		person.setAnonym(true);

		String errm = null;

		try {
			if (konto.getRole() == Role.MKV_LEHRER) {
				lehrerkontoDao.persist((Lehrerkonto) konto);
			} else {
				privatkontoDao.persist((Privatkonto) konto);
			}
		} catch (EgladilConcurrentModificationException | EgladilStorageException e) {
			LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + "{} konnte nicht anonymisiert werden: [UUID={}]", konto.getRole(),
				konto.getUuid());
			errm = e.getClass().getSimpleName();
		} catch (final EgladilDuplicateEntryException e) {
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "duplicate entry bei {} [UUID={}]", konto.getRole(), konto.getUuid());
			errm = e.getClass().getSimpleName();
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "unerwartete Exception beim Anonymsieren {} [UUID={}]",
				konto.getRole(), konto.getUuid(), e);
			errm = e.getClass().getSimpleName();
		}

		if (errm != null) {
			final String was = contextMessage + ": " + konto.getRole() + " " + errm;
			final Ereignis ereignis = new Ereignis(Ereignisart.ANONYMISIERUNG_UNVOLLST.getKuerzel(), konto.getUuid(), was);
			protokollService.protokollieren(ereignis);
		}

		return errm == null;
	}
}
