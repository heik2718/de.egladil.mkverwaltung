//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.mkv.persistence.annotations.ValidMailadresseAendernPayload;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;

/**
 * @author heikew
 *
 */
public class MailadresseAendernPayloadValidator
	implements ConstraintValidator<ValidMailadresseAendernPayload, MailadresseAendernPayload> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(ValidMailadresseAendernPayload constraintAnnotation) {
		// nix zu initialisieren
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(MailadresseAendernPayload value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		// dies muss durch Annotationen auf den attributen abgefangen worden sein
		if (value.getUsername() == null || value.getEmail() == null) {
			return true;
		}
		if (value.getUsername().equalsIgnoreCase(value.getEmail())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.mailadresse.altNeuGleich}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		return true;
	}

}
