//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.teilnahmen;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicSchule;

/**
 * PublicTeilnahme ist das neue Teilnahme-Objekt für den angular-Client.
 */
public class PublicTeilnahme implements Serializable, TeilnahmeIdentifierProvider {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private HateoasPayload hateoasPayload;

	@JsonProperty
	private boolean aktuelle = false;

	@JsonProperty
	private TeilnahmeIdentifier teilnahmeIdentifier;

	/**
	 * Die Kollegen der gleichen Schule als kommaseparierter String. Kann null sein
	 */
	@JsonProperty
	private String kollegen;

	/**
	 * Anzahl der Einträge in UPLOADS für diese Teilnahme
	 */
	@JsonProperty
	private long anzahlUploads;

	/**
	 * Anzahl der Lösungszettel, die es zu dieser Teilnahme gibt.
	 */
	@JsonProperty
	private long anzahlLoesungszettel;

	/**
	 * Wird ermittelt aus Auswertungsgruppe/Teilnehmer oder Loesungszettel für die vergangenen Jahre bzw. bei upload.
	 * Die Erfassung von Teilnehmern ist bei anzahlUploads > 0 im aktuellen Jahr nicht möglich, um Doppelerfassungen
	 * auszuschließen.
	 */
	@JsonProperty
	private long anzahlTeilnehmer;

	/**
	 * nur gefüllt bei Lehrern.
	 */
	@JsonProperty
	private PublicSchule schule;

	public static PublicTeilnahme createFromTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		final PublicTeilnahme result = new PublicTeilnahme();
		result.teilnahmeIdentifier = teilnahmeIdentifier;
		return result;
	}

	/**
	 * PublicTeilnahme
	 */
	public PublicTeilnahme() {
	}

	@Override
	public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
		return this.teilnahmeIdentifier;
	}

	public final String getKollegen() {
		return kollegen;
	}

	public final void setKollegen(final String kollegen) {
		this.kollegen = kollegen;
	}

	public final PublicSchule getSchule() {
		return schule;
	}

	public final void setSchule(final PublicSchule aktuelleSchule) {
		this.schule = aktuelleSchule;
	}

	public final long getAnzahlUploads() {
		return anzahlUploads;
	}

	public final void setAnzahlUploads(final long anzahlUploads) {
		this.anzahlUploads = anzahlUploads;
	}

	public final long getAnzahlTeilnehmer() {
		return anzahlTeilnehmer;
	}

	public final void setAnzahlTeilnehmer(final long anzahlTeilnehmer) {
		this.anzahlTeilnehmer = anzahlTeilnehmer;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicTeilnahme [");
		builder.append(teilnahmeIdentifier);
		builder.append(", anzahlUploads=");
		builder.append(anzahlUploads);
		builder.append(", anzahlTeilnehmer=");
		builder.append(anzahlTeilnehmer);
		builder.append(", kollegen=");
		builder.append(kollegen);
		builder.append(", schule=");
		builder.append(schule);
		builder.append("]");
		return builder.toString();
	}

	public final long getAnzahlLoesungszettel() {
		return anzahlLoesungszettel;
	}

	public final void setAnzahlLoesungszettel(final long anzahlLoesungszettel) {
		this.anzahlLoesungszettel = anzahlLoesungszettel;
	}

	public final boolean isAktuelle() {
		return aktuelle;
	}

	public final void setAktuelle(final boolean aktuelle) {
		this.aktuelle = aktuelle;
	}

	public final TeilnahmeIdentifier getTeilnahmeIdentifier() {
		return teilnahmeIdentifier;
	}

	public final void setTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		this.teilnahmeIdentifier = teilnahmeIdentifier;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}

	public final HateoasPayload getHateoasPayload() {
		return hateoasPayload;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((teilnahmeIdentifier == null) ? 0 : teilnahmeIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PublicTeilnahme other = (PublicTeilnahme) obj;
		if (teilnahmeIdentifier == null) {
			if (other.teilnahmeIdentifier != null) {
				return false;
			}
		} else if (!teilnahmeIdentifier.equals(other.teilnahmeIdentifier)) {
			return false;
		}
		return true;
	}
}
