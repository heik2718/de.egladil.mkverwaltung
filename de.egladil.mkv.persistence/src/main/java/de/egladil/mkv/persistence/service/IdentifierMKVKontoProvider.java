//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import java.util.Optional;

import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * IdentifierMKVKontoProvider kapselt das Abbilden der BenutzerUUID oder des DownloadCodes auf ein Lehrerkonto oder ein
 * Privatkonto.
 */
public class IdentifierMKVKontoProvider {

	/**
	 *
	 * @param benutzerUuid
	 * @param lehrerkontoDao
	 * @param privatkontoDao
	 * @return
	 */
	public Optional<IMKVKonto> findMKVBenutzerByBenutzerUuid(final String benutzerUuid, final ILehrerkontoDao lehrerkontoDao,
		final IPrivatkontoDao privatkontoDao) {
		final Optional<Lehrerkonto> optLehrer = lehrerkontoDao.findByUUID(benutzerUuid);
		if (optLehrer.isPresent()) {
			return Optional.of(optLehrer.get());
		}
		final Optional<Privatkonto> optPrivat = privatkontoDao.findByUUID(benutzerUuid);
		if (optPrivat.isPresent()) {
			return Optional.of(optPrivat.get());
		}
		return Optional.empty();
	}
}
