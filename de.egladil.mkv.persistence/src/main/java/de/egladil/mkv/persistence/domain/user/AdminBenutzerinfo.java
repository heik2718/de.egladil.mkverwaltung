//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Email;

import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.common.validation.annotations.UuidString;
import de.egladil.mkv.persistence.domain.BooleanToIntegerConverter;

/**
 * AdminBenutzerinfo sind die Attribute eines MKV-Benutzers.
 */
@Entity
@Table(name = "vw_mkvbenutzer")
public class AdminBenutzerinfo implements IDomainObject {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ben_id")
	private Long id;

	@UuidString
	@Column
	private String uuid;

	@StringLatin
	@Column
	private String vorname;

	@StringLatin
	@Column
	private String nachname;

	@Email
	@Column
	private String email;

	@Column
	@Enumerated(EnumType.STRING)
	private Role rolle;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login")
	private Date lastLogin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modified")
	private Date datumGeaendert;

	@Convert(converter = BooleanToIntegerConverter.class)
	@Column
	private boolean anonym;

	@Convert(converter = BooleanToIntegerConverter.class)
	@Column
	private boolean aktiviert;

	@Convert(converter = BooleanToIntegerConverter.class)
	@Column
	private boolean gesperrt;

	@Override
	public String toString() {
		return rolle + ": " + vorname + " " + nachname + " - " + email + " - " + uuid;
	}

	/**
	 * @see de.egladil.mkv.persistence.domain.IDomainObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	public String getUuid() {
		return uuid;
	}

	public String getVorname() {
		return vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public Date getDatumGeaendert() {
		return datumGeaendert;
	}

	public boolean isAnonym() {
		return anonym;
	}

	public boolean isAktiviert() {
		return aktiviert;
	}

	public boolean isGesperrt() {
		return gesperrt;
	}

	public String getEmail() {
		return email;
	}

	public Role getRolle() {
		return rolle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AdminBenutzerinfo other = (AdminBenutzerinfo) obj;
		if (uuid == null) {
			if (other.uuid != null) {
				return false;
			}
		} else if (!uuid.equals(other.uuid)) {
			return false;
		}
		return true;
	}
}
