//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.teilnahmen;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasPayload;

/**
 * Teilnahmejahr
 */
public class Teilnahmejahr implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private HateoasPayload hateoasPayload;

	@JsonProperty
	private String jahr;

	/**
	 * Teilnahmejahr
	 */
	public Teilnahmejahr() {
	}

	/**
	 * Teilnahmejahr
	 */
	public Teilnahmejahr(final String jahr) {
		this.jahr = jahr;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}

	public final String getJahr() {
		return jahr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jahr == null) ? 0 : jahr.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Teilnahmejahr other = (Teilnahmejahr) obj;
		if (jahr == null) {
			if (other.jahr != null) {
				return false;
			}
		} else if (!jahr.equals(other.jahr)) {
			return false;
		}
		return true;
	}
}
