//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * DownloadCode
 */
public class DownloadCode {

	@JsonProperty
	private String downloadCode;

	@JsonProperty
	private String teilnahmeart;

	@JsonProperty
	private String jahr;

	@JsonProperty
	private String kuerzel;

	@JsonProperty
	private DownloadArt downloadArt;

	@JsonProperty
	private HateoasPayload hateoasPayload;

	/**
	 * DownloadCode
	 */
	public DownloadCode() {
	}

	/**
	 * DownloadCode
	 */
	public DownloadCode(final String downloadCode, final DownloadArt downloadArt) {
		this.downloadCode = downloadCode;
		this.downloadArt = downloadArt;
	}

	/**
	 * DownloadCode
	 */
	public DownloadCode(final String downloadCode, final TeilnahmeIdentifier teilnahmeIdentifier, final DownloadArt downloadArt) {
		this.downloadCode = downloadCode;
		this.jahr = teilnahmeIdentifier.getJahr();
		this.kuerzel = teilnahmeIdentifier.getKuerzel();
		this.teilnahmeart = teilnahmeIdentifier.getTeilnahmeart().toString();
		this.downloadArt = downloadArt;
	}

	public final void setHateoasPayload(final HateoasPayload hateoasPayload) {
		this.hateoasPayload = hateoasPayload;
	}
}
