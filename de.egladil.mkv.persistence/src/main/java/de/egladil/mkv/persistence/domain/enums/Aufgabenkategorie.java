//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * Aufgabenkategorie
 */
public enum Aufgabenkategorie {

	LEICHT("A"),
	MITTEL("B"),
	SCHWER("C");

	private final String nummerPrefix;

	/**
	 * Aufgabenkategorie
	 *
	 * @param nummerPrefix
	 */
	private Aufgabenkategorie(final String nummerPrefix) {
		this.nummerPrefix = nummerPrefix;
	}

	public static Aufgabenkategorie getByNummer(final String nummer) {
		if (nummer == null) {
			throw new IllegalArgumentException("nummer null");
		}
		for (Aufgabenkategorie kategorie : Aufgabenkategorie.values()) {
			if (nummer.startsWith(kategorie.nummerPrefix)) {
				return kategorie;
			}
		}
		throw new MKVException("fehlerhafte Nummernsyntax '" + nummer + "': nummer muss mit A, B oder C beginnen");
	}

}
