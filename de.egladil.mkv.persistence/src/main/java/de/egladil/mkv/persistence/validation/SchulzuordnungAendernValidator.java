//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.egladil.mkv.persistence.payload.request.SchulzuordnungAendernPayload;

/**
 * SchulzuordnungAendernValidator
 */
public class SchulzuordnungAendernValidator
	implements ConstraintValidator<ValidSchulzuordnungAendernAnfrage, SchulzuordnungAendernPayload> {

	/**
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(final ValidSchulzuordnungAendernAnfrage constraintAnnotation) {
		// nix erforderlich
	}

	/**
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(final SchulzuordnungAendernPayload value, final ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.getKuerzelNeu() != null && value.getKuerzelNeu().equals(value.getKuerzelAlt())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.changeSchulzuordnung.altNeuGleich}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		return true;
	}
}
