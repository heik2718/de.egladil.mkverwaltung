//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Dateiname;

/**
 * DateinamePayload
 */
public class DateinamePayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Dateiname
	@NotNull
	@Size(max = 150)
	private final String dateiname;

	/**
	 * DateinamePayload
	 */
	public DateinamePayload(final String dateiname) {
		this.dateiname = dateiname;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("DateinamePayload [dateiname=");
		builder.append(dateiname);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String toBotLog() {
		return dateiname;
	}
}
