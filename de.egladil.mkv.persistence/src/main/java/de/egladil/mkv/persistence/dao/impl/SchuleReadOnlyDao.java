//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * SchuleReadOnlyDao
 */
@Singleton
@MKVPersistenceUnit
public class SchuleReadOnlyDao extends BaseDaoImpl<SchuleReadOnly> implements ISchuleReadOnlyDao {

	private static final Logger LOG = LoggerFactory.getLogger(SchuleReadOnlyDao.class);

	/**
	 * SchuleReadOnlyDao
	 */
	@Inject
	public SchuleReadOnlyDao(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public BigInteger anzahlTreffer(final String name, final String ort) {

		if (StringUtils.isAllBlank(new String[] { name, ort })) {
			throw new IllegalArgumentException("name und ort blank");
		}
		final boolean mitSchule = StringUtils.isNotBlank(name);
		final boolean mitOrt = StringUtils.isNotBlank(ort);

		final String stmt = "select count(*) from vw_schulen s where " + getWhereConditionNative(mitSchule, mitOrt);

		final Query query = getEntityManager().createNativeQuery(stmt);
		if (mitSchule) {
			query.setParameter("name", "%" + name.toLowerCase() + "%");
		}
		if (mitOrt) {
			query.setParameter("ort", "%" + ort.toLowerCase() + "%");
		}
		return SqlUtils.getCount(query);
	}

	@Override
	public BigInteger anzahlSchulenInOrt(final Long ortId) {

		final String stmt = "select count(*) from kat_schulen s where s.ort = :ortId";

		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("ortId", ortId);
		return SqlUtils.getCount(query);
	}

	@Override
	public List<SchuleReadOnly> findSchulen(final String name, final String ort) {

		if (StringUtils.isAllBlank(new String[] { name, ort })) {
			throw new IllegalArgumentException("name und ort blank");
		}
		final boolean mitSchule = StringUtils.isNotBlank(name);
		final boolean mitOrt = StringUtils.isNotBlank(ort);

		final String stmt = "select s from SchuleReadOnly s where " + getWhereCondition(mitSchule, mitOrt);
		final TypedQuery<SchuleReadOnly> query = getEntityManager().createQuery(stmt, SchuleReadOnly.class);
		if (mitSchule) {
			query.setParameter("name", "%" + name.toLowerCase() + "%");
		}
		if (mitOrt) {
			query.setParameter("ort", "%" + ort.toLowerCase() + "%");
		}

		final List<SchuleReadOnly> trefferliste = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	private String getWhereConditionNative(final boolean mitSchule, final boolean mitOrt) {
		if (mitSchule && mitOrt) {
			return " lower(s.name) like :name and lower(s.ort_name) like :ort";
		}
		if (mitSchule) {
			return " lower(s.name) like :name";
		}
		return " lower(s.ort_name) like :ort";
	}

	private String getWhereCondition(final boolean mitSchule, final boolean mitOrt) {
		if (mitSchule && mitOrt) {
			return " lower(s.name) like :name and lower(s.ortName) like :ort";
		}
		if (mitSchule) {
			return " lower(s.name) like :name";
		}
		return " lower(s.ortName) like :ort";
	}

	@Override
	public Optional<SchuleReadOnly> findUnbekannteSchuleImLand(final String landkuerzel) {

		final String stmt = "select s from SchuleReadOnly s where s.landKuerzel = :landkuerzel and lower(s.name) = :name";
		final TypedQuery<SchuleReadOnly> query = getEntityManager().createQuery(stmt, SchuleReadOnly.class);

		query.setParameter("landkuerzel", landkuerzel);
		query.setParameter("name", "unbekannt");

		final List<SchuleReadOnly> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		if (trefferliste.isEmpty()) {
			return Optional.empty();
		}

		if (trefferliste.size() > 1) {
			throw new MKVException("mehr als eine Schule 'Unbekannt' im Land '" + landkuerzel + "' vorhanden");
		}

		return Optional.of(trefferliste.get(0));
	}



	@Override
	public Optional<SchuleReadOnly> findByKuerzel(final String kuerzel) {

		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}

		final String stmt = "select s from SchuleReadOnly s where s.kuerzel = :kuerzel";
		final TypedQuery<SchuleReadOnly> query = getEntityManager().createQuery(stmt, SchuleReadOnly.class);
		query.setParameter("kuerzel", kuerzel);


		final List<SchuleReadOnly> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		if (trefferliste.isEmpty()) {
			return Optional.empty();
		}

		if (trefferliste.size() > 1) {
			throw new MKVException("mehr als eine Schule mit kuerzel '" + kuerzel + "' vorhanden");
		}

		return Optional.of(trefferliste.get(0));
	}

	@Override
	public SchuleReadOnly persist(final SchuleReadOnly entity) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

	@Override
	public List<SchuleReadOnly> persist(final List<SchuleReadOnly> entities) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

	@Override
	public String delete(final SchuleReadOnly entity) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

	@Override
	public void delete(final List<SchuleReadOnly> entities) throws PersistenceException {
		throw new IllegalAccessError("entity ist readonly");
	}

}
