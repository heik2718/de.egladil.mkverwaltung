//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import de.egladil.common.persistence.IDomainObject;

/**
 * IDownload
 */
public interface IDownload extends IDomainObject {

	Downloaddaten getDownloaddaten();

	void setDownloaddaten(Downloaddaten downloaddaten);
}
