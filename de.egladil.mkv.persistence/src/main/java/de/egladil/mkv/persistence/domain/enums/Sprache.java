//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

/**
 * Sprache
 */
public enum Sprache {

	de("deutsch"),
	en("englisch");

	private final String label;

	/**
	 * Sprache
	 */
	private Sprache(final String label) {
		this.label = label;
	}

	public final String getLabel() {
		return label;
	}
}
