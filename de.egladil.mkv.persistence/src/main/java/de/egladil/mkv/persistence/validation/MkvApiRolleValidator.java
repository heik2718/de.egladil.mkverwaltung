//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import de.egladil.mkv.persistence.annotations.MkvApiRolle;

/**
 * @author heikew
 *
 */
public class MkvApiRolleValidator implements ConstraintValidator<MkvApiRolle, String> {

	private List<String> allowedStrings = Arrays.asList(new String[] { "MKV_LEHRER", "MKV_PRIVAT" });

	@Override
	public void initialize(MkvApiRolle constraintAnnotation) {
		// nix zu tun
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (StringUtils.isBlank(value)) {
			return true;
		}
		return allowedStrings.contains(value);
	}

}
