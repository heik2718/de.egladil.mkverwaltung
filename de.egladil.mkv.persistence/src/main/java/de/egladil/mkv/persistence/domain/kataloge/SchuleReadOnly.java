//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.kataloge;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import de.egladil.common.persistence.IDomainObject;

/**
 * SchuleReadOnly
 */
@Entity
@Table(name = "vw_schulen")
public class SchuleReadOnly implements IDomainObject {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "KUERZEL")
	private String kuerzel;

	@Column(name = "STRASSE")
	private String strasse;

	@Column(name = "URL")
	private String url;

	@Column(name = "SCHULTYP")
	private String schultyp;

	@Column(name = "ORT_ID")
	private Long ortId;

	@Column(name = "ORT_KUERZEL")
	private String ortKuerzel;

	@Column(name = "ORT_NAME")
	private String ortName;

	@Column(name = "LAND_ID")
	private Long landId;

	@Column(name = "LAND_KUERZEL")
	private String landKuerzel;

	@Column(name = "LAND_NAME")
	private String landName;

	@Override
	public Long getId() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public String getStrasse() {
		return strasse;
	}

	public String getUrl() {
		return url;
	}

	public String getSchultyp() {
		return schultyp;
	}

	public Long getOrtId() {
		return ortId;
	}

	public String getOrtKuerzel() {
		return ortKuerzel;
	}

	public String getOrtName() {
		return ortName;
	}

	public Long getLandId() {
		return landId;
	}

	public String getLandKuerzel() {
		return landKuerzel;
	}

	public String getLandName() {
		return landName;
	}

	public String getLaendercode() {
		if (this.landKuerzel.startsWith("de")) {
			return "de";
		}
		return this.landKuerzel;
	}

}
