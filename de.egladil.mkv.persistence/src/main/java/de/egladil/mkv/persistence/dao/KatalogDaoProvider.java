package de.egladil.mkv.persistence.dao;

public interface KatalogDaoProvider {

	ILandDao getLandDao();

	IOrtDao getOrtDao();

	ISchuleDao getSchuleDao();

	ISchuleReadOnlyDao getSchuleReadOnlyDao();

}