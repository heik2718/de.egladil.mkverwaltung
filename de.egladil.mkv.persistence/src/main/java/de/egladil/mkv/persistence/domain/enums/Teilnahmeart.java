//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.enums;

/**
 * Teilnahmeart
 */
public enum Teilnahmeart {

	S,
	P;
}
