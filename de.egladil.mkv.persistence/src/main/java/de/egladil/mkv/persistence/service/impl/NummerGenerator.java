//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * NummerGenerator
 */
public class NummerGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(NummerGenerator.class);

	private ConcurrentHashMap<KeyTeilnehmernummer, Integer> nummerMap = new ConcurrentHashMap<>();

	private final ITeilnehmerDao teilnehmerDao;

	/**
	 * NummerGenerator
	 */
	public NummerGenerator(final ITeilnehmerDao teilnehmerDao) {
		this.teilnehmerDao = teilnehmerDao;
	}

	/**
	 * Erzeugt synchronized die nächste Nummer für einen Teilnehmer.
	 *
	 * @param teilnahmeIdentifier
	 * @param klassenstufe
	 * @return
	 */
	public synchronized int nextNummer(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider,
		final Klassenstufe klassenstufe) {
		LOG.debug("teilnahmeIdentifier={}, klassenstufe={}", teilnahmeIdentifierProvider.provideTeilnahmeIdentifier().toString(),
			klassenstufe);
		final KeyTeilnehmernummer keyTeilnehmernummer = new KeyTeilnehmernummer(teilnahmeIdentifierProvider, klassenstufe);
		final Integer nummer = nummerMap.get(keyTeilnehmernummer);
		if (nummer != null) {
			final int next = nummer.intValue() + 1;
			nummerMap.put(keyTeilnehmernummer, Integer.valueOf(next));
			LOG.debug("aus map: {}", next);
			return next;
		} else {
			final int maxNummer = teilnehmerDao.getMaxNummer(teilnahmeIdentifierProvider, klassenstufe);
			final int next = maxNummer + 1;
			nummerMap.put(keyTeilnehmernummer, Integer.valueOf(next));
			LOG.debug("aus dao: {}", next);
			return next;
		}
	}
}
