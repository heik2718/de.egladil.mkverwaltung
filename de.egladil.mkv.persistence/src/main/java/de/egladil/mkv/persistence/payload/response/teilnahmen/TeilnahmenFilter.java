//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.teilnahmen;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * TeilnahmenFilter ist die Zusammenfassung von Kürzeln (Länder, virtuelle Kürzel, ...), mit denen man die Menge von
 * Lösungszetteln für ein Wettbewerbsjahr filtern kann.
 */
public class TeilnahmenFilter {

	@JsonProperty
	private String name;

	@JsonProperty
	private Filterart filterart;

	@JsonProperty
	private List<String> kuerzelliste = new ArrayList<>();

	/**
	 * TeilnahmenFilter
	 */
	public TeilnahmenFilter() {
	}

	/**
	 * TeilnahmenFilter
	 */
	public TeilnahmenFilter(final String name, final Filterart filterart) {
		this.name = name;
		this.filterart = filterart;
	}

	public void addKuerzel(final String kuerzel) {
		if (StringUtils.isNotBlank(kuerzel) && !kuerzelliste.contains(kuerzel)) {
			this.kuerzelliste.add(kuerzel);
		}
	}

	public final String getName() {
		return name;
	}

	public final List<String> getKuerzelliste() {
		return kuerzelliste;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final Filterart getFilterart() {
		return filterart;
	}

	public final void setFilterart(final Filterart filterart) {
		this.filterart = filterart;
	}
}
