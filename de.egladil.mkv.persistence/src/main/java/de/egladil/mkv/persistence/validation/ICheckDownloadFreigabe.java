//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import de.egladil.bv.aas.domain.Role;

/**
 * ICheckDownloadFreigabe
 */
public interface ICheckDownloadFreigabe {

	/**
	 * Prüft, ob der Download freiegeben ist.
	 *
	 * @return boolean
	 */
	boolean isFreigegeben();

	/**
	 * Gibt den zur EnsureRole passenden DownloadChecker zurueck.
	 *
	 * @param role Role falls null, gibt es eine IllegalArgumentException.
	 * @param downloadPath String falls null, gibt es eine NPE
	 * @return ICheckDownloadFreigabe
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 */
	static ICheckDownloadFreigabe getChecker(final Role role, String downloadPath) {
		if (downloadPath == null) {
			throw new NullPointerException("downloadPath");
		}
		if (Role.MKV_LEHRER == role) {
			return new CheckDownloadLehrerFreigabeCommand(downloadPath);
		}
		if (Role.MKV_PRIVAT == role) {
			return new CheckDownloadPrivatFreigabeCommand(downloadPath);
		}
		throw new IllegalArgumentException("Kein Freigabechecker für EnsureRole " + role + " vorhanden");
	}

}
