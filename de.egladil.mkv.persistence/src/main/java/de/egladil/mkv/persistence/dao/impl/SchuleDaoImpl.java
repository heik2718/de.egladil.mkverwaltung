//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.SchuleLage;

/**
 * SchuleDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class SchuleDaoImpl extends BaseDaoImpl<Schule> implements ISchuleDao {

	private static final Logger LOG = LoggerFactory.getLogger(SchuleDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von SchuleDaoImpl
	 */
	@Inject
	public SchuleDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Schule> findSchulenInOrtUnscharf(final String ortkuerzel, final String schulnameteil)
		throws MKVException, PersistenceException {
		if (ortkuerzel == null) {
			throw new MKVException("ortkuerzel darf nicht null sein");
		}
		if (schulnameteil == null) {
			throw new MKVException("schulnameteil darf nicht null sein");
		}
		final String stmt = "select s.* from kat_schulen s, kat_orte o where s.ort = o.id and o.kuerzel = :ortkuerzel and lower(s.name) like :name";

		final Query query = getEntityManager().createNativeQuery(stmt, Schule.class);

		query.setParameter("ortkuerzel", ortkuerzel);
		query.setParameter("name", "%" + schulnameteil.toLowerCase() + "%");

		final List<Schule> schulen = query.getResultList();
		LOG.debug("Anzahl schulen: {}", schulen.size());

		return schulen;
	}

	@Override
	public SchuleLage getSchuleLage(final String schulkuerzel) {

		if (schulkuerzel == null) {
			throw new IllegalArgumentException("schulkuerzel null");
		}

		final String stmt = "select l.kuerzel as landKuerzel, l.name as land, o.kuerzel as ortKuerzel, o.name as ort from kat_schulen s, kat_orte o, kat_laender l where s.ort = o.id and o.land = l.id and s.kuerzel = :kuerzel";
		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("kuerzel", schulkuerzel);

		@SuppressWarnings("unchecked")
		final List<Object[]> trefferliste = query.getResultList();
		if (trefferliste.isEmpty()) {
			return null;
		}
		if (trefferliste.size() > 1) {
			throw new MKVException("Datenfehler: mehr als eine Schule mit kuerzel='" + schulkuerzel + "' gefunden (insgesamt "
				+ trefferliste.size() + ").");
		}
		final Object[] treffer = trefferliste.get(0);
		final SchuleLage result = new SchuleLage((String) treffer[0], (String) treffer[1], (String) treffer[2],
			(String) treffer[3]);
		return result;
	}
}
