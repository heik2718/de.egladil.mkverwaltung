//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;

/**
 * IWettbewerbsloesungDao
 */
public interface IWettbewerbsloesungDao extends IBaseDao<Wettbewerbsloesung> {

	/**
	 * Findet die Wettbewerbsloesungen zu einem gegebenen Jahr.
	 *
	 * @param wettbewerbsjahr String darf nicht null sein.
	 * @return List
	 */
	List<Wettbewerbsloesung> findByJahr(String wettbewerbsjahr);

}
