//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.plausi;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * ConvertToKlassenstufeCommand
 */
public class ConvertToKlassenstufeCommand implements ICommand {

	private final String klassenstufeName;

	private Klassenstufe klassenstufe;

	/**
	 * ConvertToKlassenstufeCommand
	 */
	public ConvertToKlassenstufeCommand(final String klassenstufeName) {
		if (klassenstufeName == null) {
			throw new IllegalArgumentException("klassenstufeName null");
		}
		this.klassenstufeName = klassenstufeName;
	}

	@Override
	public void execute() {
		klassenstufe = Klassenstufe.valueOf(klassenstufeName);
	}

	public final Klassenstufe getKlassenstufe() {
		if (klassenstufe == null) {
			throw new IllegalStateException("IllegalState: please invoke execute() first");
		}
		return klassenstufe;
	}
}
