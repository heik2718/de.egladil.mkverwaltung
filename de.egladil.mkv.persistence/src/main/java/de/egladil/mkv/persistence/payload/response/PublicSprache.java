//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * PublicSprache
 */
public class PublicSprache {

	@JsonProperty
	private String kuerzel;

	@JsonProperty
	private String label;

	public static PublicSprache fromSprache(final Sprache sprache) {
		return new PublicSprache(sprache.toString(), sprache.getLabel());
	}

	/**
	 * PublicSprache
	 */
	PublicSprache() {
	}

	/**
	 * PublicSprache
	 */
	private PublicSprache(final String kuerzel, final String label) {
		this.kuerzel = kuerzel;
		this.label = label;
	}

	public final String getKuerzel() {
		return kuerzel;
	}

	public final String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicSprache [kuerzel=");
		builder.append(kuerzel);
		builder.append(", label=");
		builder.append(label);
		builder.append("]");
		return builder.toString();
	}

	public Sprache convert() {
		return Sprache.valueOf(kuerzel);
	}

}
