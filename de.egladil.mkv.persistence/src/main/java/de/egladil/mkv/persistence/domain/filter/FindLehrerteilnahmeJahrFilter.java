//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.filter;

import java.util.List;

import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;

/**
 * FindLehrerteilnahmeJahrFilter
 */
public class FindLehrerteilnahmeJahrFilter {

	private final String jahr;

	/**
	 * Erzeugt eine Instanz von FindLehrerteilnahmeJahrFilter
	 */
	public FindLehrerteilnahmeJahrFilter(final String jahr) {
		this.jahr = jahr;
		if (jahr == null) {
			throw new NullPointerException("jahr");
		}

	}

	/**
	 * Sucht in der Liste der LehrerteilnahmeInformation-Instanzen diejenige, die zum gegebenen aktuellen
	 * Wettbewerbsjahr gehört.
	 *
	 * @param lehrerteilnahmen List darf nicht null sein.
	 * @return Lehrerteilnahmeinformation oder null
	 * @throws NullPointerException
	 */
	public LehrerteilnahmeInformation findTeilnahme(final List<LehrerteilnahmeInformation> lehrerteilnahmen) {
		if (lehrerteilnahmen == null) {
			throw new NullPointerException("lehrerteilnahmen");
		}
		for (final LehrerteilnahmeInformation i : lehrerteilnahmen) {
			if (jahr.equals(i.getJahr())) {
				return i;
			}
		}
		return null;
	}

}
