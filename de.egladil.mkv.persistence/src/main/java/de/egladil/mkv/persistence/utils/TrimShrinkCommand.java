//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Splitter;

/**
 * TrimShrinkCommand entfernt einschließende Leerzeichen und schrumpft innere Leerzeichen auf eins zusammen.
 */
public class TrimShrinkCommand implements ICommand {

	private static final Splitter SPLITTER = Splitter.on(" ").trimResults();

	private Text receiver;

	/**
	 * TrimShrinkCommand
	 */
	public TrimShrinkCommand(final Text receiver) {
		if (receiver == null) {
			throw new IllegalArgumentException("receiver null");
		}
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		if (receiver.getText().isEmpty()) {
			receiver.setTransformedText(receiver.getText());
			return;
		}
		final String trimmed = receiver.getText().get(0).trim();
		final List<String> worte = SPLITTER.splitToList(trimmed);
		final StringBuffer sb = new StringBuffer();
		for (final String wort : worte) {
			if (!StringUtils.isBlank(wort)) {
				sb.append(wort);
				sb.append(" ");
			}
		}
		receiver.setTransformedText(Arrays.asList(new String[] { sb.toString().trim() }));
	}
}
