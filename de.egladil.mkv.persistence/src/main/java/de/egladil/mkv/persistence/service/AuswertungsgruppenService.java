//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import java.util.List;
import java.util.Optional;

import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.AuswertungsgruppePayload;

public interface AuswertungsgruppenService {

	/**
	 * Gibt die Auswertungsgruppen zum gegebenen teilnahmeIdentifier zurück.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 *
	 * @return Optional
	 */
	Optional<Auswertungsgruppe> getRootAuswertungsgruppe(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Erzeugt die root-Auswertungsgruppe für eine Schulteilnahme.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 *
	 * @return Auswertungsgruppe
	 *
	 * @throws IllegalArgumentException
	 * @throws MKVException
	 */
	Auswertungsgruppe findOrCreateRootAuswertungsgruppe(TeilnahmeIdentifier teilnahmeIdentifier)
		throws IllegalArgumentException, MKVException;

	/**
	 * Sucht die RootAuswertungsgruppe mit diesem TeilnahmeIdentifier.
	 *
	 * @param teilnahmeIdentifier
	 * @return Optional
	 * @throws IllegalArgumentException
	 * @throws MKVException
	 */
	Optional<Auswertungsgruppe> findRootauswertungsgruppe(TeilnahmeIdentifier teilnahmeIdentifier)
		throws IllegalArgumentException, MKVException;

	/**
	 * Sucht die Auswertungsgruppe mit dem gegeben kuerzel.
	 *
	 * @param kuerzel String
	 * @return Optional
	 */
	Optional<Auswertungsgruppe> findAuswertungsgruppe(String kuerzel);

	/**
	 * Legt eine Klasse zu gegebener Schule an.
	 *
	 * @param benutzerUuid TODO
	 * @param parent Auswertungsgruppe
	 * @param payload AuswertungsgruppePayload
	 *
	 * @return Auswertungsgruppe die neue oder null, wenn es die parent-Gruppe nicht gibt.
	 *
	 * @throws IllegalArgumentException
	 * @throws MKVException
	 * @throws EgladilDuplicateEntryException
	 */
	Auswertungsgruppe createAuswertungsgruppe(String benutzerUuid, Auswertungsgruppe parent, AuswertungsgruppePayload payload)
		throws IllegalArgumentException, MKVException, EgladilDuplicateEntryException, EgladilAuthorizationException;

	/**
	 * Ändert den Namen der gegebenen Auswertungsgruppe.
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers für geaendertDurch
	 * @param kuerzel String das kuerzel der zu ändernden Auswwertungsgruppe
	 * @param payload AuswertungsgruppePayload
	 * @return Auswertungsgrupe die geänderte Auswertungsgruppe oder null, falls es keine zum gegebenen kuerzel gibt.
	 *
	 * @throws IllegalArgumentException
	 * @throws MKVException
	 * @throws EgladilDuplicateEntryException
	 */
	Auswertungsgruppe updateAuswertungsgruppe(String benutzerUuid, String kuerzel, AuswertungsgruppePayload payload)
		throws IllegalArgumentException, MKVException, EgladilDuplicateEntryException;

	/**
	 * Wenn die Auswertungsgruppe gelöscht werden darf, wird sie gelöscht, anderenfalls wird eine MKVException geworfen.
	 *
	 * @param benutzerUuid String die UUID des angemeldeten Benutzers
	 * @param kuerzel String das kuerzel der Auswertungsgruppe.
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return Auswertungsgruppe die rootgruppe mit einer Auswertungsgruppe weniger oder null, wenn es keine rootgruppe
	 * zum gegebenen TeilnahmeIdentifier gibt.
	 * @throws IllegalArgumentException
	 * @throws MKVException falls die Gruppe nicht löschbar ist.
	 * @throws ResourceNotFoundException wenn die Gruppe nicht (mehr) existiert.
	 * @throws EgladilConcurrentModificationException
	 */
	Auswertungsgruppe checkAndDeleteAuswertungsgruppe(String benutzerUuid, String kuerzel,
		final TeilnahmeIdentifier teilnahmeIdentifier)
		throws IllegalArgumentException, MKVException, ResourceNotFoundException, EgladilConcurrentModificationException;

	/**
	 * Hold die Liste aller Auswertungsgruppen zu einem gegebenen Jahr.
	 *
	 * @param jahr String darf nicht null sein.
	 * @return List
	 */
	List<Auswertungsgruppe> getRootAuswertungsgruppen(final String jahr);

	/**
	 *
	 * @param schulkuerzel String
	 * @return Optional
	 */
	Optional<Schule> findSchule(String schulkuerzel);

}