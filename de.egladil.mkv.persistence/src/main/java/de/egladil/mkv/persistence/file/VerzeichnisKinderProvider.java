//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.egladil.common.validation.PathValidator;

/**
 * VerzeichnisKinderProvider erstellt eine Liste von direkten Kindern eines Verzeichnisses.
 */
public class VerzeichnisKinderProvider {

	private final List<File> files = new ArrayList<>();

	/**
	 *
	 * Erzeugt eine Instanz von VerzeichnisKinderProvider.
	 *
	 * @param absPath String absoluter Pfad auf der Platte. Darf nicht null sein und muss auf ein reguläres lesbares
	 * Verzeichnis zeigen. @throws
	 */
	public VerzeichnisKinderProvider(final String absPath) throws IOException {
		if (absPath == null) {
			throw new NullPointerException("Parameter absolutPathName");
		}
		new PathValidator().validate(absPath, new VerzeichnisValidationStrategy());
		final File file = new File(absPath);
		if (!file.isDirectory()) {
			throw new IllegalArgumentException(absPath + " ist kein Verzeichnis");
		}
		final File[] kinder = file.listFiles();
		for (final File f : kinder) {
			files.add(f);
		}
	}

	/**
	 * gibt eine unmodifiable List zurück!
	 */
	public List<File> getFiles() {
		return Collections.unmodifiableList(files);
	}
}
