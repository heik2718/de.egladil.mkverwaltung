//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import java.util.Map;

/**
 * BerechnePunkteKlasseEinsCommand berechnet die Punkte für Klassenstufe 1.<br>
 * <br>
 * Die Parameter der command-Methode werden NICHT validiert. Daher mit einem BerechnePunkteCommandValidationDecorator
 * verbinden!!!
 */
public final class BerechnePunkteKlasseEinsCommand extends AbstractBerechnePunkteCommand {

	protected BerechnePunkteKlasseEinsCommand() {
		super();
	}

	/**
	 * @see de.egladil.mkv.persistence.berechnungen.AbstractBerechnePunkteCommand#init(int, java.util.Map)
	 */
	@Override
	protected void init(final Map<Integer, AufgabenkategorieDekorator> indexAufgabenkategorien) {
		setGuthaben(1200);
		for (int i = 0; i < 4; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 5));
		}
		for (int i = 4; i < 8; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.VIER, 5));
		}
		for (int i = 8; i < 12; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.FUENF, 5));
		}
	}
}
