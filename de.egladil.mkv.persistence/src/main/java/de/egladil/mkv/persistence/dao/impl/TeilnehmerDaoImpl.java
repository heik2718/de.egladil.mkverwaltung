//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class TeilnehmerDaoImpl extends BaseDaoImpl<Teilnehmer> implements ITeilnehmerDao {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnehmerDaoImpl.class);

	/**
	 * TeilnehmerDaoImpl
	 */
	@Inject
	public TeilnehmerDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public List<Teilnehmer> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		final String stmt = "select t from Teilnehmer t where t.teilnahmeart = :teilnahmeart and t.teilnahmekuerzel = :teilnahmekuerzel and t.jahr = :jahr order by klassenstufe, vorname";
		final TypedQuery<Teilnehmer> query = getEntityManager().createQuery(stmt, Teilnehmer.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());

		final List<Teilnehmer> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	@Override
	public long anzahlTeilnehmer(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		final String stmt = "select count(t) from Teilnehmer t where t.teilnahmeart = :teilnahmeart and t.teilnahmekuerzel = :teilnahmekuerzel and t.jahr = :jahr";
		final TypedQuery<Long> query = getEntityManager().createQuery(stmt, Long.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());

		final Long result = query.getSingleResult();
		LOG.debug("Result: {}", result);
		return result == null ? 0 : result.longValue();
	}

	@Override
	public int getMaxNummer(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider, final Klassenstufe klassenstufe)
		throws PersistenceException {
		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}
		if (klassenstufe == null) {
			throw new IllegalArgumentException("klassenstufe null");
		}
		final TeilnahmeIdentifier teilnahmeIdentifier = teilnahmeIdentifierProvider.provideTeilnahmeIdentifier();
		final String stmt = "select max(t.nummer) from Teilnehmer t where t.teilnahmeart = :teilnahmeart and t.jahr = :jahr and t.teilnahmekuerzel = :teilnahmekuerzel and t.klassenstufe = :klasse";
		LOG.debug("stmt: {}, [teilnahmeart={}, jahr={}, teilnahmekuerzel={}, klasse={}", stmt,
			teilnahmeIdentifier.getTeilnahmeart(), teilnahmeIdentifier.getJahr(), teilnahmeIdentifier.getKuerzel(), klassenstufe);
		final TypedQuery<Integer> query = getEntityManager().createQuery(stmt, Integer.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("klasse", klassenstufe);

		final Integer result = query.getSingleResult();
		LOG.debug("Result: {}", result);
		return result == null ? 0 : result.intValue();
	}
}
