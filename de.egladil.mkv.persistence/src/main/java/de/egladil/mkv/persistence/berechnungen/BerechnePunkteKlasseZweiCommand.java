//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import java.util.Map;

/**
 * BerechnePunkteKlasseZweiCommand berechnet die Punkte für Klassenstufe 2.<br>
 * <br>
 * Die Parameter der command-Methode werden NICHT validiert. Daher mit einem BerechnePunkteCommandValidationDecorator
 * verbinden!!!
 */
public final class BerechnePunkteKlasseZweiCommand extends AbstractBerechnePunkteCommand {

	protected BerechnePunkteKlasseZweiCommand() {
		super();
	}

	@Override
	protected void init(final Map<Integer, AufgabenkategorieDekorator> indexAufgabenkategorien) {
		setGuthaben(1500);
		for (int i = 0; i < 5; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 5));
		}
		for (int i = 5; i < 10; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.VIER, 5));
		}
		for (int i = 10; i < 15; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.FUENF, 5));
		}
	}
}
