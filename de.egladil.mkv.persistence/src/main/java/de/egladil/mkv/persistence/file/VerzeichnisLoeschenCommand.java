//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VerzeichnisLoeschenCommand kapselt das rekursive Löschen eines Verzeichnisses, ohne die IOException zu propagieren.
 */
public class VerzeichnisLoeschenCommand {

	private static final Logger LOG = LoggerFactory.getLogger(VerzeichnisLoeschenCommand.class);

	/**
	 * Das gegebene Verzeichnis wird rekursiv gelöscht. Eine IOException wird geloggt, aber nicht propagiert.
	 *
	 * @param file File darf nicht null sein.
	 * @return boolean true, wenn ok, false bei IOException.
	 * @throws NullPointerException wenn file null ist.
	 */
	public boolean deleteAndLogIOException(File file) {
		if (file == null) {
			throw new NullPointerException("Parameter file");
		}
		if (!file.exists()) {
			LOG.warn(file.getAbsolutePath() + " existiert nicht.");
			return false;		}
		try {
			FileUtils.forceDelete(file);
			return true;
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}
}
