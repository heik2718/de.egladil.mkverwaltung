//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * ITeilnehmerDao
 */
public interface ITeilnehmerDao extends IBaseDao<Teilnehmer> {

	/**
	 * Sucht alle Teilnehmer zu einer durch teilnahmeIdentifier spezifizierten Teilnahme.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return List
	 */
	List<Teilnehmer> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Ermittelt die Anzahl der Teilnehmer zu der durch teilnahmeIdentifier spezifizierten Teilnahme.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return
	 */
	long anzahlTeilnehmer(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Gibt die maximale Nummer zur gegebenen Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier darf nicht null sein.
	 * @param klassenstufe Klassenstufe darf nicht null sein.
	 * @return int
	 * @throws PersistenceException
	 * @throws IllegalArgumentException falls Parameter null ist.
	 */
	int getMaxNummer(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider, Klassenstufe klassenstufe) throws PersistenceException;

}
