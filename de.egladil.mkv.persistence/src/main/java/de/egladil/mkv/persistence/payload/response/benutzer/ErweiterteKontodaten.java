//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.benutzer;

import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;

/**
 * ErweiterteKontodaten
 */
public class ErweiterteKontodaten {

	private boolean mailbenachrichtigung;

	private boolean advVereinbarungVorhanden;

	private int anzahlTeilnahmen;

	private PublicTeilnahme aktuelleTeilnahme;

	private PublicSchule schule;

	private boolean schulwechselMoeglich;

	public final boolean isMailbenachrichtigung() {
		return mailbenachrichtigung;
	}

	public final void setMailbenachrichtigung(final boolean mailbenachrichtigung) {
		this.mailbenachrichtigung = mailbenachrichtigung;
	}

	public final boolean isAdvVereinbarungVorhanden() {
		return advVereinbarungVorhanden;
	}

	public final void setAdvVereinbarungVorhanden(final boolean advVereinbarungVorhanden) {
		this.advVereinbarungVorhanden = advVereinbarungVorhanden;
	}

	public final PublicTeilnahme getAktuelleTeilnahme() {
		return aktuelleTeilnahme;
	}

	public final void setAktuelleTeilnahme(final PublicTeilnahme aktuelleTeilnahme) {
		this.aktuelleTeilnahme = aktuelleTeilnahme;
	}

	public final PublicSchule getSchule() {
		return schule;
	}

	public final void setSchule(final PublicSchule schule) {
		this.schule = schule;
	}

	public final int getAnzahlTeilnahmen() {
		return anzahlTeilnahmen;
	}

	public final void setAnzahlTeilnahmen(final int anzahlTeilnahmen) {
		this.anzahlTeilnahmen = anzahlTeilnahmen;
	}

	public final boolean isSchulwechselMoeglich() {
		return schulwechselMoeglich;
	}

	public final void setSchulwechselMoeglich(final boolean schulwechselMoeglich) {
		this.schulwechselMoeglich = schulwechselMoeglich;
	}
}
