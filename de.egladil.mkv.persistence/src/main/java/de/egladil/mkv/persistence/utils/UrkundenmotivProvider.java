//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.io.InputStream;

import com.itextpdf.text.BaseColor;

/**
 * UrkundenmotivProvider
 */
public interface UrkundenmotivProvider {

	InputStream getBackgroundImage();

	BaseColor getHeadlineColor();
}
