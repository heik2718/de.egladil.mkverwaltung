//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;

/**
 * EnhancePublicAuswertungsgruppeCommand
 */
public class EnhancePublicAuswertungsgruppeCommand {

	private final Auswertungsgruppe auswertungsgruppe;

	private final PublicAuswertungsgruppe publicAuswertungsgruppe;

	private int anzahl;

	/**
	 * EnhancePublicAuswertungsgruppeCommand
	 */
	public EnhancePublicAuswertungsgruppeCommand(final Auswertungsgruppe auswertungsgruppe,
		final PublicAuswertungsgruppe publicAuswertungsgruppe) {
		this.auswertungsgruppe = auswertungsgruppe;
		this.publicAuswertungsgruppe = publicAuswertungsgruppe;
	}

	public void execute() {
		publicAuswertungsgruppe.setAnzahlKlassen(auswertungsgruppe.getAuswertungsgruppen().size());
		anzahl = 0;
		ermittleAnzahlKinder(auswertungsgruppe);
		publicAuswertungsgruppe.setAnzahlKinder(anzahl);

		anzahl = 0;
		ermittleAnzahlLoesungszettel(auswertungsgruppe);
		publicAuswertungsgruppe.setAnzahlLoesungszettel(anzahl);
	}

	private void ermittleAnzahlLoesungszettel(final Auswertungsgruppe theAuswertungsgruppe) {
		for (final Teilnehmer t : theAuswertungsgruppe.getAlleTeilnehmer()) {
			if (t.getLoesungszettelkuerzel() != null) {
				anzahl++;
			}
		}
		for (final Auswertungsgruppe g : theAuswertungsgruppe.getAuswertungsgruppen()) {
			ermittleAnzahlLoesungszettel(g);
		}
	}

	private void ermittleAnzahlKinder(final Auswertungsgruppe theAuswertungsgruppe) {
		anzahl += theAuswertungsgruppe.getAlleTeilnehmer().size();
		for (final Auswertungsgruppe g : theAuswertungsgruppe.getAuswertungsgruppen()) {
			ermittleAnzahlKinder(g);
		}
	}
}
