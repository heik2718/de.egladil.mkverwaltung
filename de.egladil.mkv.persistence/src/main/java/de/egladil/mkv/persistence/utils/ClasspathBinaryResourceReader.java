//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * ClasspathBinaryResourceReader
 */
public class ClasspathBinaryResourceReader implements ICommand {

	private final ClasspathResourceByteArray byteArray;

	/**
	 * ClasspathBinaryResourceReader
	 */
	public ClasspathBinaryResourceReader(final ClasspathResourceByteArray byteArray) {
		if (byteArray == null) {
			throw new IllegalArgumentException("byteArray null");
		}
		this.byteArray = byteArray;
	}

	@Override
	public void execute() {
		try (final InputStream in = getClass().getResourceAsStream(byteArray.getResourcePath());
			final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			IOUtils.copy(in, out);
			byteArray.setData(out.toByteArray());
		} catch (final IOException e) {
			throw new MKVException("Konnten classpath-Resource nicht lesen: " + e.getMessage(), e);
		}
	}
}
