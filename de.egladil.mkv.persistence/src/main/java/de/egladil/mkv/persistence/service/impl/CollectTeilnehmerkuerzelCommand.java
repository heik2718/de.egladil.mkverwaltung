//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;

import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * CollectTeilnehmerkuerzelCommand
 */
public class CollectTeilnehmerkuerzelCommand implements ICommand {

	private final List<String> teilnehmerkuerzel;

	private final Auswertungsgruppe auswertungsgruppe;

	/**
	 * CollectTeilnehmerkuerzelCommand
	 */
	public CollectTeilnehmerkuerzelCommand(final Auswertungsgruppe auswertungsgruppe, final List<String> teilnehmerkuerzel) {
		this.auswertungsgruppe = auswertungsgruppe;
		this.teilnehmerkuerzel = teilnehmerkuerzel;
	}

	@Override
	public void execute() {
		collectTeilnehmer(auswertungsgruppe);
	}

	private void collectTeilnehmer(final Auswertungsgruppe gruppe) {
		for (final Teilnehmer t : gruppe.getAlleTeilnehmer()) {
			if (t.getLoesungszettelkuerzel() != null && !teilnehmerkuerzel.contains(t.getKuerzel())) {
				teilnehmerkuerzel.add(t.getKuerzel());
			}
		}
		for (final Auswertungsgruppe child : gruppe.getAuswertungsgruppen()) {
			collectTeilnehmer(child);
		}
	}

}
