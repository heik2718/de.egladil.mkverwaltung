//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * AuthorizationService autorisiert einen Benutzer für eine eine Teilnahme betreffende Aktion.
 */
public interface AuthorizationService {

	/**
	 * Prüft, ob die benutzerUuid zur gegebenen Teilnahme passt, das Benutzerkonto aktiv und nicht anonym ist.
	 *
	 * @param benutzerUuid String
	 * @param parent Auswertungsgruppe
	 * @return String für Mockito
	 */
	String authorizeForTeilnahme(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Prüft, ob die benutzerUuid zu einem Lehrerkonto gehört und dieses der durch das Schulkürzel gegebenen Schule
	 * zugeordnet ist.
	 *
	 * @param benutzerUuid String UUID des Lehrers
	 * @param schulkuerzel String Schulkürzel
	 * @return String für Mockito
	 */
	String authorizeLehrerForSchule(String benutzerUuid, String schulkuerzel);

	/**
	 * Falls ein Lehrer seine Schule gewechselt hat, soll er alle alten Statistiken sehen können, zu denen er sich
	 * registriert hatte und alle alten Statistiken der aktuellen Schule.
	 *
	 * @param benutzerUuid
	 * @param teilnahmeIdentifier
	 */
	void authorizeLehrerForSchuleOderTeilnahme(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier);

}
