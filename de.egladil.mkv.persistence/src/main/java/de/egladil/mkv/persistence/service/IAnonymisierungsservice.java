//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service;

import java.util.Optional;

import de.egladil.mkv.persistence.domain.IMKVKonto;

/**
 * IAnonymisierungsservice anonymsiert Benutzer- und MKV-Konten.
 */
public interface IAnonymisierungsservice {

	/**
	 * Das gegebene Benutzerkonto und, falls vorhanden, das MKV-Konto, werden anonymsiert. Anonymisieren heißt:
	 * Loginname, Email, Vorname und Nachname werden so geändert, dass sie nicht mehr zur Identifikation taugen. Die
	 * Anonymisiertung oder ggf. unvollständige Anonymisierung wird protokolliert.
	 *
	 * @param benutzerUuid String UUID des Benutzerkontos.
	 * @param konto Optional ein Lehrerkonto oder ein Privatkonto.
	 * @param contextMessage String eine Meldung für die Protokollierung.
	 * @return String nur für Mockito
	 */
	String kontoAnonymsieren(String benutzerUuid, Optional<IMKVKonto> konto, String contextMessage) throws IllegalArgumentException;

	/**
	 * Das gegebene Benutzerkonto wird anonymsiert. Anonymisieren heißt: Loginname, Email, Vorname und Nachname werden
	 * so geändert, dass sie nicht mehr zur Identifikation taugen. Die Anonymisiertung oder ggf. unvollständige
	 * Anonymisierung wird protokolliert.
	 *
	 * @param benutzerUuid String UUID des Benutzerkontos.
	 * @param konto Optional ein Lehrerkonto oder ein Privatkonto.
	 * @param contextMessage String eine Meldung für die Protokollierung.
	 * @return String nur für Mockito
	 */
	String kontoAnonymsieren(String benutzerUuid, String contextMessage) throws IllegalArgumentException;

}
