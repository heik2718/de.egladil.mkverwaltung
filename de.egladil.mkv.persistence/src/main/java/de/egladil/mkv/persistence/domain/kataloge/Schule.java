//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.kataloge;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.Kuerzel;

/**
 * Schule
 */
@Entity
@Table(name = "kat_schulen")
public class Schule implements IDomainObject {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Version
	@Column(name = "VERSION")
	private int version;

	@NotNull
	@Size(min = 1, max = 150)
	@Column(name = "NAME", length = 150)
	private String name;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	@Column(name = "KUERZEL", length = 8)
	private String kuerzel;

	@Column(name = "STRASSE", length = 100)
	@Size(min = 0, max = 100)
	private String strasse;

	@Column(name = "URL", length = 2000)
	@Size(min = 0, max = 2000)
	private String url;

	@Column(name = "SCHULTYP", length = 100)
	@Size(min = 0, max = 100)
	private String schultyp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORT", referencedColumnName = "ID")
	private Ort ort;

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param name neuer Wert der Membervariablen name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Liefert die Membervariable kuerzel
	 *
	 * @return die Membervariable kuerzel
	 */
	public String getKuerzel() {
		return kuerzel;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kuerzel neuer Wert der Membervariablen kuerzel
	 */
	public void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	/**
	 * Liefert die Membervariable id
	 *
	 * @return die Membervariable id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param id neuer Wert der Membervariablen id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Schule other = (Schule) obj;
		if (kuerzel == null) {
			if (other.kuerzel != null)
				return false;
		} else if (!kuerzel.equals(other.kuerzel))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable strasse
	 *
	 * @return die Membervariable strasse
	 */
	public String getStrasse() {
		return strasse;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param strasse neuer Wert der Membervariablen strasse
	 */
	public void setStrasse(final String strasse) {
		this.strasse = strasse;
	}

	/**
	 * Liefert die Membervariable url
	 *
	 * @return die Membervariable url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param url neuer Wert der Membervariablen url
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Liefert die Membervariable schultyp
	 *
	 * @return die Membervariable schultyp
	 */
	public String getSchultyp() {
		return schultyp;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param schultyp neuer Wert der Membervariablen schultyp
	 */
	public void setSchultyp(final String schultyp) {
		this.schultyp = schultyp;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Schule [kuerzel=");
		builder.append(kuerzel);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return Ort
	 */
	public Ort getOrt() {
		return ort;
	}

	/**
	 * @param ort Ort
	 */
	public void setOrt(final Ort ort) {
		this.ort = ort;
	}
}
