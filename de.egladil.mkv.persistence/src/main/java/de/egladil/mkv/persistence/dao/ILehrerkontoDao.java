//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import de.egladil.mkv.persistence.domain.user.Lehrerkonto;

/**
 * ILehrerkontoDao
 */
public interface ILehrerkontoDao extends IMKVKontoDao<Lehrerkonto> {

}
