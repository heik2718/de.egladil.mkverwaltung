//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeInfoDao;
import de.egladil.mkv.persistence.domain.BenutzerstatusProvider;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.PrivatteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.service.AuthorizationService;

/**
 * AuthorizationServiceImpl
 */
@Singleton
public class AuthorizationServiceImpl implements AuthorizationService {

	private static final Logger LOG = LoggerFactory.getLogger(AuthorizationService.class);

	private final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private final IPrivatteilnahmeInfoDao privatteilnahmeInfoDao;

	private final ILehrerkontoDao lehrerkontoDao;

	/**
	 * AuthorizationServiceImpl
	 */
	@Inject
	public AuthorizationServiceImpl(final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao,
		final IPrivatteilnahmeInfoDao privatteilnahmeInfoDao, final ILehrerkontoDao lehrerkontoDao) {
		this.lehrerteilnahmeInfoDao = lehrerteilnahmeInfoDao;
		this.privatteilnahmeInfoDao = privatteilnahmeInfoDao;
		this.lehrerkontoDao = lehrerkontoDao;
	}

	@Override
	public String authorizeForTeilnahme(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier) {

		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}

		switch (teilnahmeIdentifier.getTeilnahmeart()) {
		case P:
			authorizePrivatmensch(benutzerUuid, teilnahmeIdentifier);
			break;
		case S:
			authorizeLehrer(benutzerUuid, teilnahmeIdentifier, true);
			break;
		default:
			throw new IllegalArgumentException("unbekannte Teilnahmeart: " + teilnahmeIdentifier.getTeilnahmeart().toString());
		}

		return "";
	}

	@Override
	public String authorizeLehrerForSchule(final String benutzerUuid, final String schulkuerzel) {

		if (schulkuerzel == null) {
			throw new IllegalArgumentException("schulkuerzel null");
		}

		final Optional<Lehrerkonto> optLehrerkonto = this.lehrerkontoDao.findByUUID(benutzerUuid);
		if (!optLehrerkonto.isPresent()) {
			throw new EgladilAuthorizationException(
				"Lehrerkonto mit UUID " + benutzerUuid + " existiert nicht oder ist nicht aktiviert.");
		}

		final Lehrerkonto lehrerkonto = optLehrerkonto.get();
		final Schule schule = lehrerkonto.getSchule();

		if (!schulkuerzel.equals(schule.getKuerzel())) {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Aktionsversuch durch {} auf schulkuerzel {} ", benutzerUuid, schulkuerzel);
			throw new EgladilAuthorizationException(
				"Lehrer mit benutzerUuid " + benutzerUuid + " ist fuer Aktionen mit " + schulkuerzel + " nicht autorisiert.");
		}

		return "";
	}

	private void authorizeLehrer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier, final boolean withLog) {
		final List<LehrerteilnahmeInformation> alleLehrer = lehrerteilnahmeInfoDao
			.findAlleMitJahrUndSchulkuerzel(teilnahmeIdentifier.getJahr(), teilnahmeIdentifier.getKuerzel());

		LehrerteilnahmeInformation derLehrer = null;
		for (final LehrerteilnahmeInformation lti : alleLehrer) {
			if (lti.getBenutzerUuid().equals(benutzerUuid)) {
				derLehrer = lti;
				break;
			}
		}
		if (!isAuthorized(derLehrer)) {
			if (withLog) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Aktionsversuch durch {} auf {} ", benutzerUuid, teilnahmeIdentifier);
			}
			throw new EgladilAuthorizationException("Lehrer mit benutzerUuid " + benutzerUuid + " ist fuer Aktionen mit "
				+ teilnahmeIdentifier.toString() + " nicht autorisiert.");
		}
	}

	private void authorizePrivatmensch(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier) {

		final Optional<PrivatteilnahmeInformation> optTeilnahme = privatteilnahmeInfoDao
			.findByTeilnahmeIdentifier(teilnahmeIdentifier);

		final PrivatteilnahmeInformation teilnahmeInfo = optTeilnahme.isPresent() ? optTeilnahme.get() : null;

		if (!isAuthorized(teilnahmeInfo)) {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Aktionsversuch durch {} auf {} ", benutzerUuid, teilnahmeIdentifier);
			throw new EgladilAuthorizationException("Privatmensch mit benutzerUuid " + benutzerUuid + " ist fuer Aktionen mit "
				+ teilnahmeIdentifier.toString() + " nicht autorisiert.");
		}
	}

	boolean isAuthorized(final BenutzerstatusProvider benutzerstatusProvider) {
		if (benutzerstatusProvider == null || benutzerstatusProvider.isAnonym() || !benutzerstatusProvider.isAktiviert()
			|| benutzerstatusProvider.isGesperrt()) {
			return false;
		}
		return true;
	}

	@Override
	public void authorizeLehrerForSchuleOderTeilnahme(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier) {
		int anzahlExceptions = 0;
		try {
			this.authorizeLehrer(benutzerUuid, teilnahmeIdentifier, false);
		} catch (EgladilAuthorizationException e) {
			anzahlExceptions++;
		}
		try {
			this.authorizeLehrerForSchule(benutzerUuid, teilnahmeIdentifier.getKuerzel());
		} catch (EgladilAuthorizationException e) {
			anzahlExceptions++;
		}
		if (anzahlExceptions == 2) {
			throw new EgladilAuthorizationException("Lehrer mit benutzerUuid " + benutzerUuid + " ist fuer Aktionen mit "
				+ teilnahmeIdentifier.toString() + " nicht autorisiert.");
		}
	}
}
