//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;
import java.util.Optional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.INativeSqlDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeAenderer;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeBuilder;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.AuswertungsgruppePayload;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;

/**
 * AuswertungsgruppenServiceImpl
 */
@Singleton
public class AuswertungsgruppenServiceImpl implements AuswertungsgruppenService {

	private final IAuswertungsgruppeDao auswertungsgruppeDao;

	private final ISchuleDao schuleDao;

	private final IProtokollService protokollService;

	private final INativeSqlDao nativeSqlDao;

	/**
	 * AuswertungsgruppenServiceImpl
	 *
	 * @param auswertungsgruppeDao IAuswertungsgruppeDao
	 */
	@Inject
	public AuswertungsgruppenServiceImpl(final IAuswertungsgruppeDao auswertungsgruppeDao, final ISchuleDao schuleDao,
		final IProtokollService protokollService, final INativeSqlDao nativeSqlDao) {
		this.auswertungsgruppeDao = auswertungsgruppeDao;
		this.schuleDao = schuleDao;
		this.protokollService = protokollService;
		this.nativeSqlDao = nativeSqlDao;
	}

	@Override
	public Optional<Auswertungsgruppe> getRootAuswertungsgruppe(final TeilnahmeIdentifier teilnahmeIdentifier) {

		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		return auswertungsgruppeDao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier);
	}

	@Override
	public Auswertungsgruppe findOrCreateRootAuswertungsgruppe(final TeilnahmeIdentifier teilnahmeIdentifier) {

		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}

		final Optional<Auswertungsgruppe> optRoot = findRootauswertungsgruppe(teilnahmeIdentifier);
		if (optRoot.isPresent()) {
			return optRoot.get();
		}

		final Optional<Schule> opt = findSchule(teilnahmeIdentifier.getKuerzel());
		if (!opt.isPresent()) {
			throw new MKVException("Datenfehler: keine Schule zur Schulteilnahme " + teilnahmeIdentifier.toString() + " bekannt");
		}
		final Schule schule = opt.get();
		final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(Teilnahmeart.S, teilnahmeIdentifier.getKuerzel(),
			teilnahmeIdentifier.getJahr()).name(schule.getName()).checkAndBuild();
		final Auswertungsgruppe persisted = auswertungsgruppeDao.persist(auswertungsgruppe);

		return persisted;
	}

	@Override
	public Optional<Auswertungsgruppe> findRootauswertungsgruppe(final TeilnahmeIdentifier teilnahmeIdentifier)
		throws IllegalArgumentException, MKVException {
		return auswertungsgruppeDao.findRootByTeilnahmeIdentifier(teilnahmeIdentifier);
	}

	@Override
	public Optional<Auswertungsgruppe> findAuswertungsgruppe(final String kuerzel) {
		return auswertungsgruppeDao.findByUniqueKey(Auswertungsgruppe.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
	}

	@Override
	@Transactional
	public Auswertungsgruppe createAuswertungsgruppe(final String benutzerUuid, final Auswertungsgruppe parent,
		final AuswertungsgruppePayload payload)
		throws IllegalArgumentException, MKVException, EgladilDuplicateEntryException, EgladilAuthorizationException {

		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}
		if (parent == null) {
			throw new IllegalArgumentException("parent null");
		}
		if (payload == null) {
			throw new IllegalArgumentException("payload null");
		}

		final Auswertungsgruppe neue = new AuswertungsgruppeBuilder(parent.getTeilnahmeart(), parent.getTeilnahmekuerzel(),
			parent.getJahr()).klassenstufe(payload.getKlassenstufe()).name(payload.getName()).checkAndBuild();
		neue.setGeaendertDurch(benutzerUuid);

		parent.addAuswertungsgruppe(neue);

		final Auswertungsgruppe result = auswertungsgruppeDao.persist(neue);
		protokollService.protokollieren(Ereignisart.AUSWERTUNGSGRUPPE_GEAENDERT.getKuerzel(), benutzerUuid,
			"create " + neue.getKuerzel());
		return result;
	}

	@Override
	@Transactional
	public Auswertungsgruppe updateAuswertungsgruppe(final String benutzerUuid, final String kuerzel,
		final AuswertungsgruppePayload payload) throws IllegalArgumentException, MKVException, EgladilDuplicateEntryException {

		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}
		if (payload == null) {
			throw new IllegalArgumentException("payload null");
		}
		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}

		final Optional<Auswertungsgruppe> opt = auswertungsgruppeDao.findByUniqueKey(Auswertungsgruppe.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		if (!opt.isPresent()) {
			return null;
		}
		Auswertungsgruppe auswertungsgruppe = opt.get();
		final AuswertungsgruppeAenderer aenderer = new AuswertungsgruppeAenderer(auswertungsgruppe).name(payload.getName().trim());
		auswertungsgruppe = aenderer.checkAndChange();
		auswertungsgruppe.setGeaendertDurch(benutzerUuid);

		final Auswertungsgruppe result = auswertungsgruppeDao.persist(auswertungsgruppe);

		if (result.isRoot()) {
			final String was = "Schulname: '" + result.getTeilnahmekuerzel() + "', '" + auswertungsgruppe.getKuerzel() + "'";
			protokollService.protokollieren(Ereignisart.AUSWERTUNGSGRUPPE_GEAENDERT.getKuerzel(), benutzerUuid, was);
		} else {
			protokollService.protokollieren(Ereignisart.AUSWERTUNGSGRUPPE_GEAENDERT.getKuerzel(), benutzerUuid,
				"update " + auswertungsgruppe.getKuerzel());
		}

		return result;
	}

	@Override
	@Transactional
	public Auswertungsgruppe checkAndDeleteAuswertungsgruppe(final String benutzerUuid, final String kuerzel,
		final TeilnahmeIdentifier teilnahmeIdentifier) throws IllegalArgumentException, MKVException {
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}
		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}

		final Optional<Auswertungsgruppe> optRoot = this.getRootAuswertungsgruppe(teilnahmeIdentifier);
		if (!optRoot.isPresent()) {
			throw new MKVException("Keine Rootgruppe zu " + teilnahmeIdentifier.toString() + " gefunden");
		}

		final Auswertungsgruppe rootGruppe = optRoot.get();

		if (kuerzel.equals(rootGruppe.getKuerzel())) {
			throw new MKVException("Die Rootgruppe kann nicht gelöscht werden!");
		}
		final Auswertungsgruppe auswertungsgruppe = rootGruppe.findAuswertungsgruppe(kuerzel);
		if (auswertungsgruppe == null) {
			throw new ResourceNotFoundException(
				"Auswertungsgruppe mit kuerzel " + kuerzel + " zu " + teilnahmeIdentifier.toString());
		}

		if (!auswertungsgruppe.getAlleTeilnehmer().isEmpty()) {
			throw new MKVException("Die Auswertungsgruppe kann nicht gelöscht werden, da sie Teilnehmer hat");
		}

		rootGruppe.removeAuswertungsgruppe(auswertungsgruppe);
		rootGruppe.setGeaendertDurch(benutzerUuid);
		final Auswertungsgruppe result = this.auswertungsgruppeDao.persist(rootGruppe);

		// Hibernate kaskadiert das Löschen, indem aus auswertungsgruppe nur die parent-ID gelöscht wird. Evtl. weil das
		// constraint in der DB nicht korrekt gesetzt wurde.
		this.auswertungsgruppeDao.delete(auswertungsgruppe);

		final String bemerkung = teilnahmeIdentifier.toString() + " Klasse " + auswertungsgruppe.getName() + ", kuerzel=" + kuerzel;
		protokollService.protokollieren(Ereignisart.KLASSE_GELOESCHT.getKuerzel(), benutzerUuid, bemerkung);

		return result;
	}

	@Override
	public List<Auswertungsgruppe> getRootAuswertungsgruppen(final String jahr) {

		if (jahr == null) {
			throw new IllegalArgumentException("jahr null");
		}

		final List<Auswertungsgruppe> auswertungsgruppen = auswertungsgruppeDao.findRootgruppenByJahr(jahr);

		for (final Auswertungsgruppe gruppe : auswertungsgruppen) {
			final List<String> teilnahmejahre = nativeSqlDao.findTeilnahmejahreSchule(gruppe.getTeilnahmekuerzel());
			gruppe.setTeilnahmejahre(teilnahmejahre);
		}

		return auswertungsgruppen;
	}

	@Override
	public Optional<Schule> findSchule(final String schulkuerzel) {
		return schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, schulkuerzel);
	}
}
