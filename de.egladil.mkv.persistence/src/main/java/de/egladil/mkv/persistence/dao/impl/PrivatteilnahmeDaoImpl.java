//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * PrivatteilnahmeDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class PrivatteilnahmeDaoImpl extends BaseDaoImpl<Privatteilnahme> implements IPrivatteilnahmeDao {

	private static final Logger LOG = LoggerFactory.getLogger(PrivatteilnahmeDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von PrivatteilnahmeDaoImpl
	 */
	@Inject
	public PrivatteilnahmeDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<Privatteilnahme> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier)
		throws EgladilStorageException {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		try {
			final String stmt = "select t from Privatteilnahme t where t.kuerzel = :kuerzel and t.jahr = :jahr";
			final TypedQuery<Privatteilnahme> query = getEntityManager().createQuery(stmt, Privatteilnahme.class);
			query.setParameter("kuerzel", teilnahmeIdentifier.getKuerzel());
			query.setParameter("jahr", teilnahmeIdentifier.getJahr());

			final List<Privatteilnahme> trefferliste = query.getResultList();
			LOG.debug("Anzahl Treffer: {}", trefferliste.size());

			if (trefferliste.size() > 1) {
				throw new EgladilStorageException(
					"Datenfehler: mehr als eine Privatteilnahme mit " + teilnahmeIdentifier.toString() + " vorhanden");
			}
			return trefferliste.isEmpty() ? Optional.empty() : Optional.of(trefferliste.get(0));
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwarteter Fehler beim Suchen der Privatteilnahme mit  "
				+ teilnahmeIdentifier.toString() + ": " + e.getMessage(), e);
		}
	}
}
