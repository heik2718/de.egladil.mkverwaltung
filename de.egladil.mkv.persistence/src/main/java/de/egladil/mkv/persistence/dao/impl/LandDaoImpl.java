//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.PublicLand;

/**
 * LandDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class LandDaoImpl extends BaseDaoImpl<Land> implements ILandDao {

	private static final Logger LOG = LoggerFactory.getLogger(LandDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von LandDaoImpl
	 */
	@Inject
	public LandDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.ILandDao#findLandByName(java.lang.String)
	 */
	@Override
	public List<Land> findLandByName(final String name) {
		if (name == null) {
			throw new MKVException("bezeichnung darf nicht null sein!");
		}
		final String statement = "SELECT l from Land l where l.name like :name";
		final TypedQuery<Land> query = getEntityManager().createQuery(statement, Land.class);

		String param = "%";
		if (StringUtils.isNotBlank(name)) {
			param = "%" + name.toLowerCase() + "%";
		}

		query.setParameter("name", param);
		final List<Land> laender = query.getResultList();
		LOG.debug("Anzahl Laender={}", laender.size());
		return laender;
	}

	@Override
	public List<Land> findAllFreigeschaltet() {
		final String statement = "SELECT l from Land l where l.freigeschaltet = :freigeschaltet order by name";
		final TypedQuery<Land> query = getEntityManager().createQuery(statement, Land.class);
		query.setParameter("freigeschaltet", true);

		final List<Land> laender = query.getResultList();
		LOG.debug("Anzahl Laender={}", laender.size());
		return laender;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PublicLand getFuerOrt(final Ort ort) {

		if (ort == null) {
			throw new IllegalArgumentException("ort null");
		}
		final String stmt = "select l.kuerzel, l.name from kat_laender l, kat_orte o where o.land = l.id and o.kuerzel = :okuerzel";
		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("okuerzel", ort.getKuerzel());

		final List<Object[]> trefferliste = query.getResultList();

		if (trefferliste.size() == 1) {
			final Object[] werte = trefferliste.get(0);
			final PublicLand pl = new PublicLand();
			pl.setKuerzel(werte[0].toString());
			pl.setName(werte[1].toString());
			return pl;
		}

		throw new MKVException("Datenfehler im Ort " + ort.toString() + ": keins oder mehr als ein Land zugeordnet");
	}

	@Override
	public BigInteger anzahlOrteInLand(final Long landId) {
		final String stmt = "select count(*) from kat_orte o where o.land = :landId";

		final Query query = getEntityManager().createNativeQuery(stmt);
		query.setParameter("landId", landId);
		return SqlUtils.getCount(query);
	}
}
