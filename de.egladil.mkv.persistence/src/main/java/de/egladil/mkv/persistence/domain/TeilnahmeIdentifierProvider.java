//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain;

import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnahmeIdentifierProvider
 */
public interface TeilnahmeIdentifierProvider {

	/**
	 * @return TeilnahmeIdentifier
	 */
	TeilnahmeIdentifier provideTeilnahmeIdentifier();

}
