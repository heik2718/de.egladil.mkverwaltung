//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.Optional;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;

/**
 * IUploadDao
 */
public interface IUploadDao extends IBaseDao<Upload> {

	/**
	 * Sucht den Upload mit den identifizierenden Attributen.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier darf nicht null sein
	 * @param checksum String darf nicht null sein
	 * @return Optional
	 */
	Optional<Upload> findUpload(TeilnahmeIdentifier teilnahmeIdentifier, String checksum);
}
