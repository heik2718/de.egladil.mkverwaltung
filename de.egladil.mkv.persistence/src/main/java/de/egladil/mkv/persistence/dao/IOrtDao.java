//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.PersistenceException;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.OrtLage;

/**
 * IOrtDao
 */
public interface IOrtDao extends IBaseDao<Ort> {

	/**
	 * Sucht im Land mit dem gegebenen KUERZEL alle Orte, deren name like ortname ist.
	 *
	 * @param landkuerzel
	 * @param ortname
	 * @return List
	 */
	@Deprecated
	List<Ort> findOrteInLandUnscharf(String landkuerzel, String ortname) throws MKVException, PersistenceException;

	/**
	 * Sucht alle Orte mit dem gegebenen String im Namen und (falls nicht null) im Land mit dem namensteil im Namen.
	 *
	 * @param namensteilOrt String vollständiger Ortsname oder Teil des Ortsnamen.
	 * @param namensteilLand String vollständger Landname oder Teil des Landnamen
	 * @return List
	 */
	List<Ort> findOrte(String namensteilOrt, String namensteilLand);

	/**
	 * Natives Statement, um die OrtLage herauszufinden.
	 *
	 * @param ortKuerzel
	 * @return
	 */
	OrtLage getOrtLage(String ortKuerzel);

	/**
	 * Ermittelt die Anzahl der Treffer zu den Suchparametern.
	 *
	 * @param namensteilOrt String vollständiger Ortsname oder Teil des Ortsnamen.
	 * @param namensteilLand String vollständger Landname oder Teil des Landnamen
	 * @return BigInteger
	 */
	BigInteger anzahlTreffer(String namensteilOrt, String namensteilLand);
}
