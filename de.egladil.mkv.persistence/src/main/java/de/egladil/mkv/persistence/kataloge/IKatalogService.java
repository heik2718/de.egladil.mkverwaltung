package de.egladil.mkv.persistence.kataloge;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.NotImplementedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;
import de.egladil.mkv.persistence.payload.response.OrtLage;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;

public interface IKatalogService {

	/**
	 * Holt die gesamten Länder. Muss evtl. noch eingeschränkt werden auf die freigeschalteten?
	 *
	 * @return List
	 * @throws MKVException, wenn die Länder nicht geladen werden können.
	 */
	List<PublicLand> getPublicLaender() throws MKVException;

	/**
	 * Sucht das Land mit dem gegebenen Kürzel.
	 *
	 * @param landkuerzel
	 * @return
	 * @throws EgladilWebappException
	 * @throws MKVException
	 * @throws ConstraintViolationException
	 */
	Optional<Land> getLand(String landkuerzel) throws MKVException, ConstraintViolationException;

	/**
	 *
	 * @param landId
	 * @return
	 */
	Optional<Land> getLand(Long landId);

	/**
	 * Sucht den Ort mit gegebenem Kürzel im Land mit gegebenem Kürzel. TODO
	 *
	 * @param landkuerzel
	 * @param ortkuerzel
	 * @return
	 * @throws EgladilWebappException
	 * @throws MKVException
	 */
	Optional<Ort> getOrt(String landkuerzel, String ortkuerzel) throws MKVException, ConstraintViolationException;

	/**
	 *
	 * @param ortKuerzel String das kuerzel
	 * @return OrtLage oder null
	 */
	OrtLage getOrtLage(String ortKuerzel);

	/**
	 *
	 * @param schuleKuerzel String das kuerzel
	 * @return
	 */
	SchuleLage getSchuleLage(String schuleKuerzel);

	/**
	 *
	 * @param schulkuerzel String
	 * @return Optional
	 */
	Optional<PublicSchule> getSchule(String schulkuerzel);

	/**
	 * Legt ein neues Land mit dem gegebenen Kürzel an.
	 *
	 * @param landkuerzel
	 * @param name
	 * @param benutzerUuid
	 * @param freischalten TODO
	 * @return Land
	 */
	Land landAnlegen(String landkuerzel, String name, String benutzerUuid, boolean freischalten)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException;

	/**
	 * Ändert den Namen des Landes mit dem gegebenen Kürzel.
	 *
	 * @param landkuerzel
	 * @param name
	 * @param benutzerUuid
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilAuthorizationException
	 * @throws EgladilDuplicateEntryException
	 * @throws EgladilStorageException
	 */
	Land landNameAendern(String landkuerzel, String name, String benutzerUuid) throws IllegalArgumentException,
		EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException, NotImplementedException;

	/**
	 * Legt im Land land einen Ort mit dem gegebenen Namen an.
	 *
	 * @param landkuerzel
	 * @param ortname
	 * @param benutzerUuid
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilAuthorizationException
	 * @throws EgladilDuplicateEntryException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	Ort ortAnlegen(String landkuerzel, String ortname, String benutzerUuid) throws IllegalArgumentException,
		EgladilAuthorizationException, EgladilDuplicateEntryException, ResourceNotFoundException, EgladilStorageException;

	/**
	 * Ändert den Namen des ortes mit dem gegebenen kuerzel.
	 *
	 * @param ortkuerzel
	 * @param ortname
	 * @param benutzerUuid
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilAuthorizationException
	 * @throws EgladilDuplicateEntryException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	Ort ortNameAendern(String ortkuerzel, String ortname, String benutzerUuid) throws IllegalArgumentException,
		EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException, NotImplementedException;

	/**
	 * Legt im Land und ort eine neue Schule an.
	 *
	 * @param benutzerUuid
	 * @param NeueSchulePayload
	 *
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilAuthorizationException
	 * @throws EgladilDuplicateEntryException
	 * @throws MKVResourceNotFoundException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	Schule schuleAnlegen(NeueSchulePayload payload, String benutzerUuid) throws IllegalArgumentException,
		EgladilAuthorizationException, EgladilDuplicateEntryException, ResourceNotFoundException, EgladilStorageException;

	/**
	 * Ändert den Namen der Schule mit dem gegebenen kuerzel.
	 *
	 * @param schulkuerzel
	 * @param schulename
	 * @param benutzerUuid
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilAuthorizationException
	 * @throws EgladilDuplicateEntryException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	Schule schuleNameAendern(String schulkuerzel, String schulename, String benutzerUuid) throws IllegalArgumentException,
		EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException, NotImplementedException;

	/**
	 * Sucht orte like namensteil länderübergreifend.
	 *
	 * @param namensteil
	 * @param benutzerUuid zum autorisieren
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	List<Ort> findOrte(String namensteil, String benutzerUuid) throws IllegalArgumentException, EgladilStorageException;

	/**
	 * Sucht orte like namensteil im gegebenen Land.
	 *
	 * @param landkuerzel
	 * @param namensteil
	 * @param benutzerUuid zur Authorisierung
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	List<Ort> findOrte(String landkuerzel, String namensteil, String benutzerUuid)
		throws IllegalArgumentException, EgladilStorageException;

	/**
	 * Sucht Schulen like namensteil global.
	 *
	 * @param namensteil
	 * @param benutzerUuid zur Authorisierung
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	List<Schule> findSchulen(String namensteil, String benutzerUuid) throws IllegalArgumentException, EgladilStorageException;

	/**
	 * Sucht Schulen like namensteil im gegebenen Ort.
	 *
	 * @param ortkuerzel
	 * @param namensteil
	 * @param benutzerUuid zur Authorisierung
	 * @return
	 * @throws IllegalArgumentException
	 * @throws EgladilStorageException
	 */
	@Deprecated
	List<Schule> findSchulen(String ortkuerzel, String namensteil, String benutzerUuid)
		throws IllegalArgumentException, EgladilStorageException;

	/**
	 *
	 * @return List
	 */
	List<Schule> getAll();
}