//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.filter;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * FindKollegenteilnahmenFilter
 */
public class FindKollegenteilnahmenFilter {

	private final long lehrerBenutzerId;

	/**
	 * Erzeugt eine Instanz von FindKollegenteilnahmenFilter
	 */
	public FindKollegenteilnahmenFilter(final long lehrerBenutzerId) {
		this.lehrerBenutzerId = lehrerBenutzerId;
	}

	/**
	 * Gibt alle LehrerteilnahmeInfo-Instanzen zurück, die nicht zum durch lehrerBenutzerID passenden Lehrer gehören.
	 *
	 * @param lehrerteilnahmen
	 * @return
	 */
	public List<LehrerteilnahmeInformation> findTeilnahmenDerKollegen(final List<LehrerteilnahmeInformation> lehrerteilnahmen) {
		final List<LehrerteilnahmeInformation> result = new ArrayList<>(lehrerteilnahmen);
		final List<LehrerteilnahmeInformation> alleEigenen = new FindLehrerteilnahmeLehrerFilter(lehrerBenutzerId)
			.findTeilnahme(lehrerteilnahmen);
		if (alleEigenen.size() != 1) {
			throw new MKVException(
				"eine Gruppe enthält keine Lehrerteilnahme oder mehr als eine mit der Benutzer-ID " + lehrerBenutzerId);
		}
		final LehrerteilnahmeInformation eigene = alleEigenen.get(0);
		result.remove(eigene);
		return result;
	}
}
