//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.Optional;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * IPrivatteilnahmeDao
 */
public interface IPrivatteilnahmeDao extends IBaseDao<Privatteilnahme> {

	/**
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return Optional
	 * @throws EgladilStorageException wenns mehr als eins gibt
	 */
	Optional<Privatteilnahme> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier) throws EgladilStorageException;
}
