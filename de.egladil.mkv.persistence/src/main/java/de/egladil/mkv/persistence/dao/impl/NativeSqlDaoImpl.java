//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.INativeSqlDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * NativeSqlDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class NativeSqlDaoImpl implements INativeSqlDao {

	private static final Logger LOG = LoggerFactory.getLogger(NativeSqlDaoImpl.class);

	private Provider<EntityManager> emp;

	private final ILoesungszettelDao loesungszettelDao;

	/**
	 * NativeSqlDaoImpl
	 */
	@Inject
	public NativeSqlDaoImpl(final Provider<EntityManager> emp, final ILoesungszettelDao loesungszettelDao) {
		this.emp = emp;
		this.loesungszettelDao = loesungszettelDao;
	}

	@Override
	public int getAnzahlLoesungszettelSchulteilnahmen(final String jahr, final Klassenstufe klassenstufe,
		final List<String> landkuerzel) {

		if (landkuerzel == null || landkuerzel.isEmpty()) {
			throw new IllegalArgumentException("landkuerzel null oder leer");
		}

		String stmt = "select count(l.id) as anzahl from loesungszettel l, vw_schulteilnahmen t "
			+ " where l.TEILNAHMEKUERZEL = t.SCHULKUERZEL and l.jahr = t.jahr and l.TEILNAHMEART = 'S' ";

		if (StringUtils.isNotBlank(jahr)) {
			stmt += " and t.jahr = :jahr";
		}
		if (klassenstufe != null) {
			stmt += " and l.klassenstufe = :klassenstufe";
		}
		stmt += " and t.LANDKUERZEL in (:landkuerzel)";

		LOG.debug(stmt);

		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("landkuerzel", landkuerzel);

		if (StringUtils.isNotBlank(jahr)) {
			query.setParameter("jahr", jahr);
		}
		if (klassenstufe != null) {
			query.setParameter("klassenstufe", klassenstufe.toString());
		}

		return SqlUtils.getCount(query).intValue();
	}

	@Override
	public List<Loesungszettel> getLoesungszettelByJahrUndKlassenstufe(final String jahr, final Klassenstufe klassenstufe) {
		return this.loesungszettelDao.findByJahrAndKlassenstufe(jahr, klassenstufe);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Loesungszettel> getLoesungszettelSchulen(final String jahr, final Klassenstufe klassenstufe,
		final List<String> kuerzelliste) {

		final List<String> landkuerzel = kuerzelliste.stream().filter(k -> !MKVConstants.GRUPPE_PRIVATTEILNAHMEN_KUERZEL.equals(k))
			.collect(Collectors.toList());

		final ArrayList<Loesungszettel> result = new ArrayList<>();

		if (landkuerzel.isEmpty()) {
			return result;
		}

		final String stmt = "select l.* from loesungszettel l, vw_schulteilnahmen t where l.teilnahmeart = :teilnahmeart "
			+ "and l.teilnahmekuerzel = t.schulkuerzel and l.jahr = t.jahr and l.jahr = :jahr and l.klassenstufe = :klassenstufe "
			+ "and t.landkuerzel in :landkuerzel";

		LOG.debug(stmt);

		final Query query = emp.get().createNativeQuery(stmt, "loesungszettelResult");
		query.setParameter("landkuerzel", landkuerzel);
		query.setParameter("jahr", jahr);
		query.setParameter("teilnahmeart", Teilnahmeart.S.toString());
		query.setParameter("klassenstufe", klassenstufe.toString());

		final List trefferliste = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		for (final Object treffer : trefferliste) {
			result.add((Loesungszettel) treffer);
		}

		return result;
	}

	@Override
	public List<Loesungszettel> getLoesungszettelPrivat(final String jahr, final Klassenstufe klassenstufe) {
		return loesungszettelDao.findPrivateByJahrAndKlassenstufe(jahr, klassenstufe);
	}

	@Override
	public int getAnzahlSchulteilnahmenLand(final List<String> landkuerzel, final String jahr) {

		if (StringUtils.isBlank(jahr)) {
			throw new IllegalArgumentException("jahr blank");
		}

		final String stmt = "select count(*) from vw_schulteilnahmen t "
			+ " where t.landkuerzel in (:landkuerzel) and t.jahr = :jahr";

		LOG.debug(stmt);

		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("landkuerzel", landkuerzel);

		query.setParameter("jahr", jahr);

		return SqlUtils.getCount(query).intValue();
	}

	@Override
	public int getAnzahlSchulteilnahmen(final String schulkuerzel) {

		final String stmt = "select count(*) from schulteilnahmen t where t.kuerzel = :kuerzel";

		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("kuerzel", schulkuerzel);

		return SqlUtils.getCount(query).intValue();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<String> getAuswertbareJahre() {

		final String stmt = "select distinct(jahr) from loesungszettel";

		final Query query = emp.get().createNativeQuery(stmt);

		final List trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		final List<String> result = new ArrayList<>(trefferliste.size());
		for (final Object obj : trefferliste) {
			result.add(obj.toString());
		}

		Collections.sort(result);
		return result;

	}

	@Override
	public List<String> findTeilnahmejahreLand(final String landkuerzel) {

		if (landkuerzel == null) {
			throw new IllegalArgumentException("landkuerzel null");
		}

		final String stmt = "select distinct(jahr) from vw_schulteilnahmen where landkuerzel = :landkuerzel";
		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("landkuerzel", landkuerzel);

		@SuppressWarnings("rawtypes")
		final List trefferliste = query.getResultList();
		final List<String> results = new ArrayList<>(trefferliste.size());
		for (final Object obj : trefferliste) {
			results.add(obj.toString());
		}

		return results;
	}

	@Override
	public List<String> findTeilnahmejahreSchule(final String teilnahmekuerzel) {
		if (teilnahmekuerzel == null) {
			throw new IllegalArgumentException("landkuerzel null");
		}

		final String stmt = "select distinct(jahr) from schulteilnahmen where kuerzel = :kuerzel";
		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("kuerzel", teilnahmekuerzel);

		@SuppressWarnings("rawtypes")
		final List trefferliste = query.getResultList();
		final List<String> results = new ArrayList<>(trefferliste.size());
		for (final Object obj : trefferliste) {
			results.add(obj.toString());
		}

		return results;
	}

	@Override
	public int getAnzahlLoesungszettel(final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider) {

		if (teilnahmeIdentifierProvider == null) {
			throw new IllegalArgumentException("teilnahmeIdentifierProvider null");
		}

		final String stmt = "select count(*) from loesungszettel l "
			+ " where l.teilnahmekuerzel = :teilnahmekuerzel and l.teilnahmeart = :teilnahmeart and l.jahr= :jahr";

		LOG.debug(stmt);
		TeilnahmeIdentifier identifier = teilnahmeIdentifierProvider.provideTeilnahmeIdentifier();

		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("teilnahmekuerzel", identifier.getKuerzel());
		query.setParameter("teilnahmeart", identifier.getTeilnahmeart().toString());
		query.setParameter("jahr", identifier.getJahr());

		return SqlUtils.getCount(query).intValue();
	}

	@Override
	public List<Schulteilnahme> getSchulteilnahmenForLehrer(final Long lehrerkontoId) {

		if (lehrerkontoId == null) {
			throw new IllegalArgumentException("lehrerkontoId null");
		}

		String stmt = "select * from schulteilnahmen s where s.id in (select teilnahme from schulteilnahmen_lehrer where lehrer = :lehrerId)";

		Query query = emp.get().createNativeQuery(stmt, Schulteilnahme.class);
		query.setParameter("lehrerId", lehrerkontoId);

		@SuppressWarnings("unchecked")
		List<Schulteilnahme> trefferliste = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	@Override
	public int getAnzahlSchulteilnahmenForLehrer(final Long lehrerkontoId) {
		if (lehrerkontoId == null) {
			throw new IllegalArgumentException("lehrerkontoId null");
		}

		String stmt = "select count(*) from schulteilnahmen s where s.id in (select teilnahme from schulteilnahmen_lehrer where lehrer = :lehrerId)";
		final Query query = emp.get().createNativeQuery(stmt);
		query.setParameter("lehrerId", lehrerkontoId);

		return SqlUtils.getCount(query).intValue();
	}


}
