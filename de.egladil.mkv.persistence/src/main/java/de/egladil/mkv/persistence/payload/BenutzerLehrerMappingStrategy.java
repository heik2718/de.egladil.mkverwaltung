//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import java.util.List;
import java.util.Optional;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * BenutzerLehrerMappingStrategy
 */
public class BenutzerLehrerMappingStrategy implements IBenutzerMappingStrategy {

	private final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private final AdvFacade advFacade;

	private final String wettbewerbsjahr;

	private final IKatalogService katalogService;

	private final TeilnahmenFacade teilnahmenFacade;

	private final TeilnehmerFacade teilnehmerFacade;

	/**
	 * Erzeugt eine Instanz von BenutzerLehrerMappingStrategy
	 */
	public BenutzerLehrerMappingStrategy(final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao, final AdvFacade advFacade,
		final IKatalogService katalogService, final TeilnahmenFacade teilnahmenFacade, final TeilnehmerFacade teilnehmerFacade) {
		this.lehrerteilnahmeInfoDao = lehrerteilnahmeInfoDao;
		this.advFacade = advFacade;
		this.katalogService = katalogService;
		this.wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
		this.teilnahmenFacade = teilnahmenFacade;
		this.teilnehmerFacade = teilnehmerFacade;
	}

	@Override
	public MKVBenutzer createMKVBenutzer(final Benutzerkonto benutzer, final IMKVKonto kontakt) {

		final Lehrerkonto lehrerkonto = (Lehrerkonto) kontakt;
		final Person person = lehrerkonto.getPerson();

		final MKVBenutzer result = new MKVBenutzer();

		final PersonBasisdaten basisdaten = new PersonBasisdaten();
		basisdaten.setEmail(benutzer.getEmail());
		basisdaten.setRolle(Role.MKV_LEHRER.toString());
		basisdaten.setNachname(person.getNachname());
		basisdaten.setVorname(person.getVorname());

		result.setBasisdaten(basisdaten);

		final ErweiterteKontodaten erweiterteDaten = new ErweiterteKontodaten();

		final Schule schule = lehrerkonto.getSchule();
		final PublicSchule aktuelleSchule = new PublicSchule(schule.getName(), schule.getKuerzel());

		final SchuleLage schuleLage = katalogService.getSchuleLage(schule.getKuerzel());

		aktuelleSchule.setLage(schuleLage);

		erweiterteDaten.setSchule(aktuelleSchule);
		erweiterteDaten.setMailbenachrichtigung(lehrerkonto.isAutomatischBenachrichtigen());

		final Long lehrerBenutzerId = lehrerkonto.getId();
		List<LehrerteilnahmeInformation> alleAktuellenTeilnahmeinfos = lehrerteilnahmeInfoDao
			.findAlleMitJahrUndSchulkuerzel(wettbewerbsjahr, schule.getKuerzel());

		if (!alleAktuellenTeilnahmeinfos.isEmpty()) {
			PublicTeilnahme aktuelleTeilnahme = teilnahmenFacade.sammleDatenAktuelleLehrerteilnahme(lehrerkonto, aktuelleSchule,
				alleAktuellenTeilnahmeinfos);
			erweiterteDaten.setAktuelleTeilnahme(aktuelleTeilnahme);
		}
		final Optional<AdvVereinbarung> optAdv = advFacade.findAdvVereinbarungFuerSchule(schule.getKuerzel());

		erweiterteDaten.setAdvVereinbarungVorhanden(optAdv.isPresent());
		erweiterteDaten.setAnzahlTeilnahmen(teilnahmenFacade.getAnzahlSchulteilnahmenForLehrer(lehrerBenutzerId));
		result.setErweiterteKontodaten(erweiterteDaten);

		return result;
	}

}
