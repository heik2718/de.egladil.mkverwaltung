//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * ILoesungszettelDao
 */
public interface ILoesungszettelDao extends IBaseDao<Loesungszettel> {

	/**
	 * Gibt die maximale Nummer zu den gegebenen Lösungszetteln zurück.
	 *
	 * @param teilnahmeart Teilnahmeart darf nicht null sein.
	 * @param jahr String darf nicht null sein
	 * @param teilnahmekuerzel String darf nicht null sein.
	 * @param klassenstufe Klassenstufe darf nicht null sein.
	 * @return
	 * @throws PersistenceException
	 * @NullPointerException falls einer der Parameter null ist.
	 */
	int getMaxNummer(Teilnahmeart teilnahmeart, String jahr, String teilnahmekuerzel, Klassenstufe klassenstufe)
		throws PersistenceException;

	/**
	 * Gibt die Lösungszettel der gegebenen Klassenstufe im gegebenen Jahr zurück.
	 *
	 * @param jahr String darf nicht null sein.
	 * @param klassenstufe Klassenstufe darf nicht null sein.
	 * @return List
	 * @throws NullPointerException falls einer der Parameter null ist.
	 */
	List<Loesungszettel> findByJahrAndKlassenstufe(String jahr, Klassenstufe klassenstufe);

	/**
	 * Gibt die Lösungszettel aller Privatteilnahmen der gegebenen Klassenstufe im gegebenen Jahr zurück.
	 *
	 * @param jahr String darf nicht null sein.
	 * @param klassenstufe Klassenstufe darf nicht null sein.
	 * @return List
	 * @throws NullPointerException falls einer der Parameter null ist.
	 */
	List<Loesungszettel> findPrivateByJahrAndKlassenstufe(String jahr, Klassenstufe klassenstufe);

	/**
	 * Gibt die Liste der Lösungszettel zur gegebenen Teilnahme zurück.
	 *
	 * @param teilnahmeIdentifier ITeilnahme darf nicht null sein.
	 * @return List kann leer sein.
	 */
	List<Loesungszettel> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Ermittelt die Anzahl der loesungszettel zur spezifizierten Teilnahme
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return int
	 */
	long anzahlLoesungszettel(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Ermittelt die Anzahl der Lösungszettel mit verschiedenen Parametern.
	 *
	 * @param jahr String wenn null, dann alle Jahre.
	 * @param teilnahmeart Teilnahmeart wenn null, dann alle Teilnahmearten
	 * @param klassenstufe Klassenstufe wenn null, dann alle Klassenstufen
	 * @return int
	 */
	int getAnzahlLoesungszettel(String jahr, Teilnahmeart teilnahmeart, Klassenstufe klassenstufe);
}
