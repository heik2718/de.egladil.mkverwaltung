//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

/**
 * FindKaengurusprungCommand ermittelt im wertungscode den längsten Kängurusprung.
 */
public class FindKaengurusprungCommand {

	/**
	 * Ermittelt aus einem wertungscode die längste Kette aufeinanderfolgender r.
	 *
	 * @param wertungscode String darf nicht null sein!
	 * @return int
	 * @throws NullPointerException wenn wertungscode null
	 */
	public int findKaengurusprung(final String wertungscode) {
		if (wertungscode == null) {
			throw new NullPointerException("Parameter wertungscode");
		}
		int result = 0;
		int aktuellesMaximum = 0;
		for (final char c : wertungscode.toCharArray()) {
			if ('r' == c) {
				result++;
				if (result >= aktuellesMaximum) {
					aktuellesMaximum = result;
				}
			} else {
				result = 0;
			}
		}
		return aktuellesMaximum;
	}

}
