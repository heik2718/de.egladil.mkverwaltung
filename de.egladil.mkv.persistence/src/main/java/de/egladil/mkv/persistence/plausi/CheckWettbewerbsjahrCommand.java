//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.plausi;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * CheckWettbewerbsjahrCommand
 */
public class CheckWettbewerbsjahrCommand implements ICommand {

	private final String jahr;

	/**
	 * CheckWettbewerbsjahrCommand
	 */
	public CheckWettbewerbsjahrCommand(final String jahr) {
		if (jahr == null) {
			throw new IllegalArgumentException("jahr null");
		}
		this.jahr = jahr;
	}

	@Override
	public void execute() {
		final String wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
		if (!wettbewerbsjahr.equals(jahr)) {
			throw new PreconditionFailedException("jahr ist nicht das aktuelle Wettbewerbsjahr");
		}
	}
}
