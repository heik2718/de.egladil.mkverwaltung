//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.file;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.IPathValidationStrategie;
import de.egladil.common.validation.PathValidator;

/**
 * DateiSuchenCommand
 */
public class DateiSuchenCommand {

	private static final Logger LOG = LoggerFactory.getLogger(DateiSuchenCommand.class);

	private final List<String> verzeichnispfade;

	/**
	 *
	 * Erzeugt eine Instanz von DateiSuchenCommand
	 *
	 * @param verzeichnispfade List absolute Pfadnamen von Verzeichnissen, in denen gesucht wird.
	 */
	public DateiSuchenCommand(List<String> verzeichnispfade) {
		if (verzeichnispfade == null) {
			throw new NullPointerException("verzeichnispfade");
		}
		this.verzeichnispfade = verzeichnispfade;
	}

	/**
	 * Sucht in allen Verzeichnissen, ob sich die Datei oder das Verzeichnis dort befindet.
	 *
	 * @param filename String darf nicht null sein
	 * @param strategie IPathValidationStrategie darf nicht null sein.
	 * @return boolean
	 * @throws NullPointerException
	 */
	public boolean gefunden(String filename, IPathValidationStrategie strategie) {
		if (filename == null) {
			throw new NullPointerException("dateiname");
		}
		if (strategie == null) {
			throw new NullPointerException("strategie");
		}
		for (String dir : verzeichnispfade) {
			String absPath = dir + File.separator + filename;
			if (check(absPath, strategie)) {
				LOG.debug("File {} gefunden in {}", filename, dir);
				return true;
			}
		}
		LOG.debug("File {} nicht gefunden", filename);
		return false;
	}

	private boolean check(String absPath, IPathValidationStrategie strategie) {
		try {
			new PathValidator().validate(absPath, strategie);
			return true;
		} catch (IOException e) {
			return false;
		}
	}
}
