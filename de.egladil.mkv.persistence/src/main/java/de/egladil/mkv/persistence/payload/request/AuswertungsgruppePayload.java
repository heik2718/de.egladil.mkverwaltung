//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * AuswertungsgruppePayload
 */
public class AuswertungsgruppePayload implements ILoggable, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private Klassenstufe klassenstufe;

	@JsonProperty
	private String nameKlassenstufe;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	@JsonProperty
	private String name;

	/**
	 * AuswertungsgruppePayload
	 */
	public AuswertungsgruppePayload() {
	}

	@Override
	public String toBotLog() {
		return this.toString();
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("AuswertungsgruppePayload [klassenstufe=");
		builder.append(klassenstufe);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

	public final Klassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	public final void setKlassenstufe(final Klassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
	}

	public final String getName() {
		return name;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final String getNameKlassenstufe() {
		return nameKlassenstufe;
	}

	public final void setNameKlassenstufe(final String nameKlassenstufe) {
		this.nameKlassenstufe = nameKlassenstufe;
	}

}
