//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

@Documented
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE })
/**
 * BVPersistenceUnit
 */
public @interface MKVPersistenceUnit {
}
