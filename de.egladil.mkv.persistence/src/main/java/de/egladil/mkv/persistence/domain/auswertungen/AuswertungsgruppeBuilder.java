//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * AuswertungsgruppeBuilder
 */
public class AuswertungsgruppeBuilder extends AuswertungsgruppeManipulator {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungsgruppeBuilder.class);

	private Teilnahmeart teilnahmeart;

	private String teilnahmekuerzel;

	private String jahr;

	private Klassenstufe klassenstufe;

	private String kuerzel;

	/**
	 * AuswertungsgruppeBuilder
	 */
	protected AuswertungsgruppeBuilder() {
		super();
	}

	/**
	 * AuswertungsgruppeBuilder
	 */
	public AuswertungsgruppeBuilder(final Teilnahmeart teilnahmeart, final String teilnahmekuerzel, final String jahr) {
		super();
		checkAndInit(teilnahmeart, teilnahmekuerzel, jahr);
	}

	/**
	 * @param teilnahmeart
	 * @param teilnahmekuerzel
	 * @param jahr
	 */

	protected void checkAndInit(final Teilnahmeart teilnahmeart, final String teilnahmekuerzel, final String jahr) {
		if (teilnahmeart == null) {
			throw new IllegalArgumentException("teilnahmeart ist erforderlich");
		}
		if (StringUtils.isAnyBlank(new String[] { teilnahmekuerzel, jahr })) {
			LOG.error("teilnahmekuerzel={}, jahr={}", teilnahmekuerzel, jahr);
			throw new IllegalArgumentException("teilnahmekuerzel und jahr sind erforderlich");
		}
		this.teilnahmeart = teilnahmeart;
		this.teilnahmekuerzel = teilnahmekuerzel;
		this.jahr = jahr;
	}

	public AuswertungsgruppeBuilder name(final String name) {
		super.normalizeAndSetName(name);
		return this;
	}

	public AuswertungsgruppeBuilder klassenstufe(final Klassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
		return this;
	}

	public AuswertungsgruppeBuilder kuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
		return this;
	}

	/**
	 *
	 * @return Auswertungsgruppe mit den minimalen Pflichtattributen.
	 * @throws IllegalArgumentException falls der der Name nicht auf eine AuswertungDownload passen würde.
	 */
	public Auswertungsgruppe checkAndBuild() {
		super.pruefeNameValid();
		final Auswertungsgruppe result = new Auswertungsgruppe(teilnahmeart, teilnahmekuerzel, jahr);
		result.setName(getNeuerName());
		result.setKuerzel(this.kuerzel == null ? new KuerzelGenerator().generateDefaultKuerzelWithTimestamp() : this.kuerzel);
		result.setKlassenstufe(this.klassenstufe);
		return result;
	}

	protected final Teilnahmeart getTeilnahmeart() {
		return teilnahmeart;
	}

	protected final String getTeilnahmekuerzel() {
		return teilnahmekuerzel;
	}

	protected final String getJahr() {
		return jahr;
	}

}
