//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import org.apache.commons.lang3.StringUtils;

/**
 * WertungscodeCommand
 */
public class WertungscodeCommand {

	private final char[] loesungscode;

	/**
	 * @param loesungscode String aus A,B,C,D,E ohne Kommas.
	 */
	public WertungscodeCommand(final String loesungscode) {

		this.loesungscode = loesungscode.toCharArray();
	}

	/**
	 * Vergleicht die korrigierte Benutzereingabe mit den loesungsbuchstaben.
	 *
	 * @param korrigierteBenutzereingabe String aus A,B,C,D,E und N mit oder ohne Kommas
	 * @return String Wertungscode ist ein String aus f,r und n ohne Kommas.
	 */
	public String berechneWertungscode(final String korrigierteBenutzereingabe) {
		String[] nutzereingabeOhneKommas = null;
		if (korrigierteBenutzereingabe.indexOf(',') > 0) {
			nutzereingabeOhneKommas = korrigierteBenutzereingabe.split(",");
		} else {
			nutzereingabeOhneKommas = korrigierteBenutzereingabe.split("");
		}
		final String[] wertungen = new String[loesungscode.length];
		for (int i = 0; i < loesungscode.length; i++) {
			final String nutzerzeichen = nutzereingabeOhneKommas[i];
			if ("N".equalsIgnoreCase(nutzerzeichen)) {
				wertungen[i] = "n";
			} else {
				final String erwartetesZeichen = String.valueOf(loesungscode[i]);
				wertungen[i] = erwartetesZeichen.equalsIgnoreCase(nutzerzeichen) ? "r" : "f";
			}

		}
		return StringUtils.join(wertungen);
	}
}
