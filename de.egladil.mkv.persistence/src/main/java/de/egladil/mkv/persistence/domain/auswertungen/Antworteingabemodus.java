//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

/**
 * Antworteingabemodus ist der Eingabemodus, mit dem die Antwortbuchstaben eingegeben wurden.<br>
 * <ul>
 * <li><strong>MATRIX:</strong> Eingabe über 4x3- oder 5x3- Matrix mit 0-5 oder A-N.
 * <li><strong>CHECKBOX:</strong> Eingabe Checkboxen
 * </ul>
 */
public enum Antworteingabemodus {

	CHECKBOX,
	MATRIX;

}
