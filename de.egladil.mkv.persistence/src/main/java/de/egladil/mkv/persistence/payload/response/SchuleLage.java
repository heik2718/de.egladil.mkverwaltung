//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;

/**
 * SchuleLage
 */
public class SchuleLage {

	@JsonProperty
	private String landkuerzel;

	@JsonProperty
	private String land;

	@JsonProperty
	private String ortkuerzel;

	@JsonProperty
	private String ort;

	public static SchuleLage create(final SchulteilnahmeReadOnly st) {
		SchuleLage result = new SchuleLage();
		result.land = st.getLand();
		result.landkuerzel = st.getLandkuerzel();
		result.ort = st.getOrt();
		result.ortkuerzel = st.getOrtkuerzel();
		return result;
	}

	/**
	 * SchuleLage
	 */
	public SchuleLage() {
	}

	/**
	 * SchuleLage
	 */
	public SchuleLage(final String landkuerzel, final String land, final String ortkuerzel, final String ort) {
		this.landkuerzel = landkuerzel;
		this.land = land;
		this.ortkuerzel = ortkuerzel;
		this.ort = ort;
	}

	public final String getLandkuerzel() {
		return landkuerzel;
	}

	public final String getLand() {
		return land;
	}

	public final String getOrtkuerzel() {
		return ortkuerzel;
	}

	public final String getOrt() {
		return ort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((landkuerzel == null) ? 0 : landkuerzel.hashCode());
		result = prime * result + ((ortkuerzel == null) ? 0 : ortkuerzel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SchuleLage other = (SchuleLage) obj;
		if (landkuerzel == null) {
			if (other.landkuerzel != null) {
				return false;
			}
		} else if (!landkuerzel.equals(other.landkuerzel)) {
			return false;
		}
		if (ortkuerzel == null) {
			if (other.ortkuerzel != null) {
				return false;
			}
		} else if (!ortkuerzel.equals(other.ortkuerzel)) {
			return false;
		}
		return true;
	}
}
