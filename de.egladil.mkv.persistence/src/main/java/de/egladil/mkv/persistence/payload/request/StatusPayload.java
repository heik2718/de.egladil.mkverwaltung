//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.common.persistence.ILoggable;
import de.egladil.mkv.persistence.annotations.MkvApiRolle;

/**
 * StatusPayload
 */
public class StatusPayload implements Serializable, ILoggable {

	/* serialVersionUID		*/
	private static final long serialVersionUID = 1L;

	private boolean neuerStatus;

	@NotBlank
	@MkvApiRolle
	private String rolle;

	public boolean isNeuerStatus() {
		return neuerStatus;
	}

	public void setNeuerStatus(final boolean neuerStatus) {
		this.neuerStatus = neuerStatus;
	}

	@Override
	public String toString() {
		final StringBuffer builder = new StringBuffer();
		builder.append("StatusPayload [neuerStatus=");
		builder.append(neuerStatus);
		builder.append(", rolle=");
		builder.append(rolle);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	/**
	* @return String
	*/
	public String getRolle() {
		return rolle;
	}

	/**
	* @param rolle String
	*/
	public void setRolle(final String rolle) {
		this.rolle = rolle;
	}
}
