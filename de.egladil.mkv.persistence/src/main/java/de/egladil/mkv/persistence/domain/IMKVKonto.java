//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.ILoggable;
import de.egladil.mkv.persistence.domain.teilnahmen.Downloaddaten;
import de.egladil.mkv.persistence.domain.teilnahmen.IDownload;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.user.Person;

/**
 * IKontakt
 */
public interface IMKVKonto extends ILoggable {

	Long getId();

	String getUuid();

	Person getPerson();

	void setUuid(String uuid);

	void setPerson(Person kontaktdaten);

	Role getRole();

	/**
	 * Kuerzel der Kindobjekte: schulzuordnungen bei Lehrerkonten, privatteilnahmen bei Privatkonten.
	 *
	 * @return
	 */
	List<String> getChildKuerzel();

	/**
	 * Gibt die Teilnahme zum gegebenen Jahr zurück.
	 *
	 * @param jahr String darf nicht null sein
	 * @return ITeilnahme oder null
	 */
	TeilnahmeIdentifierProvider getTeilnahmeZuJahr(String jahr) throws IllegalArgumentException;

	/**
	 * Gibt die Daten aller Downloads dieses MKV-Kontos zurück.
	 *
	 * @return
	 */
	List<Downloaddaten> alleDownloads();

	/**
	 * Sucht einen Download mit gegebenem jahr und gegebener Dateiart.
	 *
	 * @param dateiname
	 * @param jahr
	 * @return IDownload oder null
	 */
	IDownload findDownload(String dateiname, String jahr);

	void addDownload(IDownload download);

	/**
	 * Erzeugt ein neues IDownload-Objekt.
	 *
	 * @return
	 */
	IDownload createBlankDownload();

	/**
	 * Gibt den TeilnahmeIdentifier zu diesem Jahr zurück.
	 *
	 * @param jahr String jahr
	 * @return Optional
	 */
	Optional<TeilnahmeIdentifier> getTeilnahmeIdentifier(String jahr);

	Date getLastLogin();
}
