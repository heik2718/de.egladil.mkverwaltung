//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.cache;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheLoader;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;

/**
 * LandCacheLoader
 */
@Singleton
public class LandCacheLoader extends CacheLoader<String, Land> {

	private static final Logger LOG = LoggerFactory.getLogger(LandCacheLoader.class);

	private final ILandDao landDao;

	private ExecutorService executor;

	/**
	 * Erzeugt eine Instanz von LandCacheLoader
	 */
	@Inject
	public LandCacheLoader(ILandDao landDao) {
		this.landDao = landDao;
		executor = Executors.newSingleThreadExecutor();
	}

	/**
	 * @see jersey.repackaged.com.google.common.cache.CacheLoader#load(java.lang.Object)
	 */
	@Override
	public Land load(String kuerzel) throws Exception {
		try {
			return readLandFromDB(kuerzel);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * @see com.google.common.cache.CacheLoader#reload(java.lang.Object, java.lang.Object)
	 */
	@Override
	public ListenableFuture<Land> reload(String kuerzel, Land oldValue) throws Exception {
		ListenableFutureTask<Land> task = ListenableFutureTask.create(new Callable<Land>() {
			@Override
			public Land call() throws Exception {
				try {
					Land land = readLandFromDB(kuerzel);
					LOG.info("Land {} reloaded", kuerzel);
					return land == null ? oldValue : land;
				} catch (Throwable e) {
					LOG.warn("Error on reload Land {}", kuerzel);
					return oldValue;
				}
			}
		});
		executor.execute(task);
		return task;
	}

	/**
	 *
	 * @param kuerzel
	 * @return
	 */
	public Land readLandFromDB(String kuerzel) {
		Optional<Land> opt = landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		return opt.isPresent() ? opt.get() : null;
	}
}
