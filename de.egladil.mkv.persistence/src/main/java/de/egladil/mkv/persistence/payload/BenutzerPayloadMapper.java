//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.response.benutzer.GeschuetzteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.impl.EnhanceTeilnahmeCommand;
import de.egladil.mkv.persistence.service.impl.MapBenutzerToMKVKontaktCommand;

/**
 * BenutzerPayloadMapper wandelt die Daten des Benutzers in einem MKVBenutzer um.
 */
@Singleton
public class BenutzerPayloadMapper {

	private static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

	private final IBenutzerService benutzerService;

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private final TeilnehmerFacade teilnehmerFacade;

	private final IKatalogService katalogService;

	private final AdvFacade advFacade;

	private final TeilnahmenFacade teilnahmenFacade;

	/**
	 * Erzeugt eine Instanz von BenutzerPayloadMapper
	 */
	@Inject
	public BenutzerPayloadMapper(final IBenutzerService benutzerService, final ILehrerkontoDao lehrerkontoDao,
		final IPrivatkontoDao privatkontoDao, final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao,
		final TeilnehmerFacade teilnehmerFacade, final AdvFacade advFacade, final IKatalogService katalogService,
		final TeilnahmenFacade teilnahmenFacade) {

		this.benutzerService = benutzerService;
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.lehrerteilnahmeInfoDao = lehrerteilnahmeInfoDao;
		this.teilnehmerFacade = teilnehmerFacade;
		this.advFacade = advFacade;
		this.katalogService = katalogService;
		this.teilnahmenFacade = teilnahmenFacade;
	}

	/**
	 * Mapped ein Benutzerkonto mit einer gegebenen uuid auf einen MKVBenutzer.
	 *
	 * @param uuid String
	 * @param modusAdmin boolean
	 * @return
	 * @throws MKVException
	 * @throws EgladilAuthenticationException
	 */
	public MKVBenutzer createBenutzer(final String uuid, final boolean modusAdmin) throws MKVException {

		if (StringUtils.isBlank(uuid)) {
			throw new IllegalArgumentException("uuid blank");
		}

		final Benutzerkonto benutzer = this.benutzerService.findBenutzerkontoByUUID(uuid);
		if (benutzer == null) {
			throw new ResourceNotFoundException("Tja, gibt's leider nich");
		}

		return this.createBenutzer(benutzer, modusAdmin);
	}

	/**
	 * Mapped ein Benutzerkonto auf einen MKVBenutzer.
	 *
	 * @param benutzerBenutzerkonto
	 * @param modusAdmin boolean
	 * @return
	 * @throws MKVException
	 * @throws EgladilAuthenticationException
	 */
	public MKVBenutzer createBenutzer(final Benutzerkonto benutzer, final boolean modusAdmin)
		throws MKVException, EgladilAuthenticationException {

		if (benutzer == null) {
			throw new IllegalArgumentException("benutzer null");
		}

		final IMKVKonto kontakt = new MapBenutzerToMKVKontaktCommand(lehrerkontoDao, privatkontoDao, benutzer.getUuid())
			.findMKVKonto();

		final IBenutzerMappingStrategy strategy = this.getStrategy(kontakt.getRole());

		final MKVBenutzer result = strategy.createMKVBenutzer(benutzer, kontakt);
		if (result.getErweiterteKontodaten().getAktuelleTeilnahme() != null) {
			new EnhanceTeilnahmeCommand(teilnehmerFacade, result.getErweiterteKontodaten().getAktuelleTeilnahme()).execute();
			if (!StringUtils.isBlank(result.getErweiterteKontodaten().getAktuelleTeilnahme().getKollegen())) {
				result.getErweiterteKontodaten().setSchulwechselMoeglich(true);
			}
		} else {
			if (kontakt.getRole() == Role.MKV_LEHRER) {
				result.getErweiterteKontodaten().setSchulwechselMoeglich(true);
			}
		}

		if (modusAdmin) {
			final GeschuetzteKontodaten geschuetzteDaten = new GeschuetzteKontodaten();
			geschuetzteDaten.setAktiviert(benutzer.isAktiviert());
			geschuetzteDaten.setAnonym(benutzer.isAnonym());
			geschuetzteDaten.setLastAccess(mapDate(kontakt.getLastLogin()));
			geschuetzteDaten.setGesperrt(benutzer.isGesperrt());
			geschuetzteDaten.setDatumGeaendert(mapDate(benutzer.getDatumGeaendert()));
			geschuetzteDaten.setUuid(benutzer.getUuid());

			result.setGeschuetzteKontodaten(geschuetzteDaten);
		}

		return result;
	}

	private IBenutzerMappingStrategy getStrategy(final Role role) {
		if (Role.MKV_LEHRER == role) {
			return new BenutzerLehrerMappingStrategy(lehrerteilnahmeInfoDao, advFacade, katalogService, teilnahmenFacade,
				teilnehmerFacade);
		}
		if (Role.MKV_PRIVAT == role) {
			return new BenutzerPrivatMappingStrategy();
		}
		throw new IllegalArgumentException("keine MappingStrategie für EnsureRole " + role + " vorhanden");
	}

	private String mapDate(final Date date) {
		if (date != null) {
			final SimpleDateFormat sdt = new SimpleDateFormat(DATE_TIME_FORMAT);
			return sdt.format(date);
		}
		return null;
	}
}
