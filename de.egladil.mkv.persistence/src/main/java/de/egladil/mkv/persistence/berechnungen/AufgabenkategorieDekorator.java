//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

/**
 * AufgabenkategorieDekorator kennt eine MKAufgabenkategorie und die Anzahl der Lösungsvorschläge und kann daraus die
 * Strafpunkte berechnen.
 */
public class AufgabenkategorieDekorator {

	private final MKAufgabenkategorie kategorie;

	private final int anzahlAntwortmoeglichkeiten;

	/**
	 * AufgabenkategorieDekorator
	 */
	public AufgabenkategorieDekorator(final MKAufgabenkategorie kategorie, final int anzahlAntwortmoeglichkeiten) {
		this.kategorie = kategorie;
		this.anzahlAntwortmoeglichkeiten = anzahlAntwortmoeglichkeiten;
	}

	public int getPunkte() {
		return kategorie.getPunkte();
	}

	protected final MKAufgabenkategorie getKategorie() {
		return kategorie;
	}

	public int berechneStrafpunkte() {
		return kategorie.getPunkte() / (anzahlAntwortmoeglichkeiten - 1);
	}
}
