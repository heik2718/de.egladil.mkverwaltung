//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Kuerzel;

/**
 * DateinamePayload
 */
public class DownloadcodePayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Kuerzel
	@Size(min = 22, max = 22)
	private final String downloadcode;

	/**
	 * DateinamePayload
	 */
	public DownloadcodePayload(final String downloadcode) {
		this.downloadcode = downloadcode;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Payload [downloadcode=");
		builder.append(downloadcode);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String toBotLog() {
		return downloadcode;
	}
}
