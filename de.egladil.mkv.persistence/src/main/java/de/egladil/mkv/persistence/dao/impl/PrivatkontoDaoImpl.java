//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * PrivatkontaktDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class PrivatkontoDaoImpl extends BaseDaoImpl<Privatkonto> implements IPrivatkontoDao {

	/**
	 * Erzeugt eine Instanz von PrivatkontaktDaoImpl
	 */
	@Inject
	public PrivatkontoDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IPrivatkontoDao#findByUUID(java.lang.String)
	 */
	@Override
	public Optional<Privatkonto> findByUUID(final String uuid) {
		return super.findByUniqueKey(Privatkonto.class, MKVConstants.UUID_ATTRIBUTE_NAME, uuid);
	}

	/**
	 * @see de.egladil.mkv.persistence.dao.IMKVKontoDao#findByPerson(de.egladil.mkv.persistence.domain.user.Person)
	 */
	@Override
	public List<Privatkonto> findByPerson(final Person person) {
		throw new MKVException("Methode nicht implementiert :/");
	}
}
