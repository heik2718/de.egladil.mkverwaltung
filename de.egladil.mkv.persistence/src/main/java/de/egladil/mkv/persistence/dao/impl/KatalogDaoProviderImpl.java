//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.KatalogDaoProvider;

/**
 * KatalogDaoProviderImpl bündelt alle Katalog-Daos in einem Objekt.
 */
@Singleton
public class KatalogDaoProviderImpl implements KatalogDaoProvider {

	private final ILandDao landDao;

	private final IOrtDao ortDao;

	private final ISchuleDao schuleDao;

	private final ISchuleReadOnlyDao schuleReadOnlyDao;

	/**
	 * KatalogDaoProviderImpl
	 */
	@Inject
	public KatalogDaoProviderImpl(final ILandDao landDao, final IOrtDao ortDao, final ISchuleDao schuleDao,
		final ISchuleReadOnlyDao schuleReadOnlyDao) {
		this.landDao = landDao;
		this.ortDao = ortDao;
		this.schuleDao = schuleDao;
		this.schuleReadOnlyDao = schuleReadOnlyDao;
	}

	@Override
	public final ILandDao getLandDao() {
		return landDao;
	}

	@Override
	public final IOrtDao getOrtDao() {
		return ortDao;
	}

	@Override
	public final ISchuleDao getSchuleDao() {
		return schuleDao;
	}

	@Override
	public final ISchuleReadOnlyDao getSchuleReadOnlyDao() {
		return schuleReadOnlyDao;
	}

}
