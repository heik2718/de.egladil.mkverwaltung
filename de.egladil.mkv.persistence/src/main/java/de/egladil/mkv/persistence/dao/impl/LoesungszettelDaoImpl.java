//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * LoesungszettelDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class LoesungszettelDaoImpl extends BaseDaoImpl<Loesungszettel> implements ILoesungszettelDao {

	private static final Logger LOG = LoggerFactory.getLogger(LoesungszettelDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von LoesungszettelDaoImpl
	 */
	@Inject
	public LoesungszettelDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public int getMaxNummer(final Teilnahmeart teilnahmeart, final String jahr, final String teilnahmekuerzel,
		final Klassenstufe klassenstufe) throws PersistenceException {
		final String stmt = "select max(l.nummer) from Loesungszettel l where l.teilnahmeart = :teilnahmeart and l.jahr = :jahr and l.teilnahmekuerzel = :teilnahmekuerzel and l.loesungszettelRohdaten.klassenstufe = :klasse";
		LOG.debug("stmt: {}, [teilnahmeart={}, jahr={}, teilnahmekuerzel={}, klasse={}", stmt, teilnahmeart, jahr, teilnahmekuerzel,
			klassenstufe);
		final TypedQuery<Integer> query = getEntityManager().createQuery(stmt, Integer.class);
		query.setParameter("teilnahmeart", teilnahmeart);
		query.setParameter("jahr", jahr);
		query.setParameter("teilnahmekuerzel", teilnahmekuerzel);
		query.setParameter("klasse", klassenstufe);

		final Integer result = query.getSingleResult();
		LOG.debug("Result: {}", result);
		return result == null ? 0 : result.intValue();
	}

	@Override
	public long anzahlLoesungszettel(final TeilnahmeIdentifier teilnahmeIdentifier) {

		final String stmt = "select count(l) from Loesungszettel l where l.teilnahmeart = :teilnahmeart and l.jahr = :jahr and l.teilnahmekuerzel = :teilnahmekuerzel";
		final TypedQuery<Long> query = getEntityManager().createQuery(stmt, Long.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());

		final Long result = query.getSingleResult();
		LOG.debug("Result: {}", result);
		return result == null ? 0 : result.longValue();
	}

	@Override
	public int getAnzahlLoesungszettel(final String jahr, final Teilnahmeart teilnahmeart, final Klassenstufe klassenstufe) {

		String stmt = "select count(l) from Loesungszettel l ";
		if (StringUtils.isNotBlank(jahr) || teilnahmeart != null || klassenstufe != null) {
			stmt += " where ";
		}

		boolean hasVorgaenger = false;

		if (StringUtils.isNotBlank(jahr)) {
			hasVorgaenger = true;
			stmt += " l.jahr = :jahr ";
		}
		if (teilnahmeart != null) {
			if (hasVorgaenger) {
				stmt += " and ";
			}
			stmt += " l.teilnahmeart = :teilnahmeart ";
			hasVorgaenger = true;
		}
		if (klassenstufe != null) {
			if (hasVorgaenger) {
				stmt += " and ";
			}
			stmt += " l.loesungszettelRohdaten.klassenstufe = :klassenstufe ";
		}
		final TypedQuery<Long> query = getEntityManager().createQuery(stmt, Long.class);
		if (StringUtils.isNotBlank(jahr)) {
			query.setParameter("jahr", jahr);
		}
		if (teilnahmeart != null) {
			query.setParameter("teilnahmeart", teilnahmeart);
		}
		if (klassenstufe != null) {
			query.setParameter("klassenstufe", klassenstufe);
		}
		final Long result = query.getSingleResult();
		return result == null ? 0 : result.intValue();
	}

	@Override
	public List<Loesungszettel> findByJahrAndKlassenstufe(final String jahr, final Klassenstufe klassenstufe) {
		if (jahr == null) {
			throw new NullPointerException("Parameter jahr");
		}
		if (klassenstufe == null) {
			throw new NullPointerException("Parameter klasse");
		}
		final String stmt = "select l from Loesungszettel l where l.jahr = :jahr and l.loesungszettelRohdaten.klassenstufe = :klasse";
		final TypedQuery<Loesungszettel> query = getEntityManager().createQuery(stmt, Loesungszettel.class);
		query.setParameter("jahr", jahr);
		query.setParameter("klasse", klassenstufe);

		final List<Loesungszettel> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	@Override
	public List<Loesungszettel> findPrivateByJahrAndKlassenstufe(final String jahr, final Klassenstufe klassenstufe) {
		if (jahr == null) {
			throw new NullPointerException("Parameter jahr");
		}
		if (klassenstufe == null) {
			throw new NullPointerException("Parameter klasse");
		}
		final String stmt = "select l from Loesungszettel l where l.jahr = :jahr and l.loesungszettelRohdaten.klassenstufe = :klasse and l.teilnahmeart = :teilnahmeart";
		final TypedQuery<Loesungszettel> query = getEntityManager().createQuery(stmt, Loesungszettel.class);
		query.setParameter("jahr", jahr);
		query.setParameter("klasse", klassenstufe);
		query.setParameter("teilnahmeart", Teilnahmeart.P);

		final List<Loesungszettel> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

	@Override
	public List<Loesungszettel> findByTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (teilnahmeIdentifier == null) {
			throw new NullPointerException("teilnahme");
		}
		final String stmt = "select l from Loesungszettel l where l.teilnahmekuerzel = :teilnahmekuerzel and l.jahr = :jahr and l.teilnahmeart = :teilnahmeart";
		final TypedQuery<Loesungszettel> query = getEntityManager().createQuery(stmt, Loesungszettel.class);
		query.setParameter("teilnahmekuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		final List<Loesungszettel> trefferliste = query.getResultList();
		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

}
