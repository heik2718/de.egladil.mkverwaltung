//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * PublicOrt
 */
public class PublicOrt {

	@JsonProperty
	private int level = 1;

	@JsonProperty
	private String name;

	@JsonProperty
	private String kuerzel;

	@JsonProperty
	private OrtLage lage;

	@JsonProperty
	private int anzahlSchulen;

	@JsonProperty
	private List<PublicSchule> kinder = new ArrayList<>();

	@JsonProperty
	private HateoasPayload hateoasPayload;

	/**
	 * Erzeugt eine Instanz von PublicOrt aus Ort, ohne die Schulen hinzuzufügen. Auch die Anzahl der Schulen ist 0!!!!
	 *
	 * @param ort Ort
	 * @param land Land
	 * @return PublicOrt
	 */
	public static PublicOrt createLazy(final Ort ort, final OrtLage lage) throws MKVException {
		if (ort == null) {
			throw new IllegalArgumentException("ort null");
		}
		if (lage == null) {
			throw new IllegalArgumentException("lage null");
		}
		final PublicOrt entity = new PublicOrt(ort.getName(), ort.getKuerzel());
		entity.setLage(lage);
		entity.hateoasPayload = new HateoasPayload(ort.getKuerzel(), "/orte/" + ort.getKuerzel());
		{
			final HateoasLink link = new HateoasLink("/orte/" + ort.getKuerzel() + "/schulen", "Schulen", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			entity.addHateoasLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/laender/" + lage.getLandkuerzel(), "Land", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			entity.addHateoasLink(link);
		}
		return entity;
	}

	/**
	 * Erzeugt eine vollständige Instanz von PublicOrt aus Ort und OrtLage.
	 *
	 * @param ort Ort
	 * @param land Land
	 * @return PublicOrt
	 */
	public static PublicOrt createMitSchulen(final Ort ort, final OrtLage lage) throws MKVException {
		final PublicOrt entity = PublicOrt.createLazy(ort, lage);
		{
			final HateoasLink link = new HateoasLink("/orte/" + ort.getKuerzel() + "/schulen", "Schulen", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			entity.addHateoasLink(link);
		}
		{
			final HateoasLink link = new HateoasLink("/laender/" + lage.getLandkuerzel(), "Land", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			entity.addHateoasLink(link);
		}
		final List<PublicSchule> schulen = new ArrayList<>();

		if (ort.getSchulen() != null) {
			entity.setAnzahlSchulen(ort.getSchulen().size());
			for (final Schule schule : ort.getSchulen()) {
				schulen.add(new PublicSchule(schule.getName(), schule.getKuerzel()));
			}
		}

		entity.setKinder(schulen);
		return entity;
	}

	/**
	 * Erzeugt eine Instanz von PublicOrt aus Ort.
	 *
	 * @param ort
	 * @return PublicOrt
	 */
	public static PublicOrt createMitSchulen(final Ort ort) throws MKVException {
		if (ort == null) {
			throw new MKVException("ort darf nicht null sein");
		}
		final PublicOrt entity = new PublicOrt(ort.getName(), ort.getKuerzel());
		final List<PublicSchule> schulen = new ArrayList<>();

		if (ort.getSchulen() != null) {
			for (final Schule schule : ort.getSchulen()) {
				schulen.add(new PublicSchule(schule.getName(), schule.getKuerzel()));
			}
		}

		entity.setKinder(schulen);
		return entity;
	}

	/**
	 * Erzeugt eine Instanz von PublicOrt
	 */
	public PublicOrt() {
	}

	/**
	 * Erzeugt eine Instanz von PublicOrt
	 */
	public PublicOrt(final String name, final String kuerzel) {
		this.name = name;
		this.kuerzel = kuerzel;
		if (hateoasPayload == null) {
			this.hateoasPayload = new HateoasPayload(this.kuerzel, "/orte/" + this.kuerzel);
		}
	}

	/**
	 * Tja.
	 *
	 * @param ps
	 */
	public void addKind(final PublicSchule ps) {
		if (ps != null) {
			kinder.add(ps);
		}
	}

	/**
	 * Tja.
	 *
	 * @param link
	 */
	public void addHateoasLink(final HateoasLink link) {
		if (link != null && this.hateoasPayload != null) {
			this.hateoasPayload.addLink(link);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PublicOrt other = (PublicOrt) obj;
		if (kuerzel == null) {
			if (other.kuerzel != null)
				return false;
		} else if (!kuerzel.equals(other.kuerzel))
			return false;
		return true;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
		if (hateoasPayload == null) {
			this.hateoasPayload = new HateoasPayload(this.kuerzel, "/orte/" + this.kuerzel);
		}
	}

	public void setKinder(final List<PublicSchule> schulen) {
		this.kinder = schulen;
	}

	public String getName() {
		return name;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public List<PublicSchule> getKinder() {
		return kinder;
	}

	public final int getAnzahlSchulen() {
		return anzahlSchulen;
	}

	public final void setAnzahlSchulen(final int anzahlSchulen) {
		this.anzahlSchulen = anzahlSchulen;
	}

	public final OrtLage getLage() {
		return lage;
	}

	public final void setLage(final OrtLage lage) {
		this.lage = lage;
	}

	public HateoasPayload getHateoasPayload() {
		return hateoasPayload;
	}

}
