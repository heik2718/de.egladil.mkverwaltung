//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.payload.response.PublicLand;

/**
 * ILandDao
 */
public interface ILandDao extends IBaseDao<Land> {

	/**
	 * Sucht Länder, deren Bezeichnung passen (unscharf, nicht case sensitive).
	 *
	 * @param bezeichnung String
	 * @return List
	 */
	List<Land> findLandByName(String bezeichnung);

	/**
	 *
	 * @param ort
	 * @return PublicLand
	 */
	PublicLand getFuerOrt(Ort ort);

	/**
	 * Gibt nur die freigeschalteten Länder zurück;
	 *
	 * @return List
	 */
	List<Land> findAllFreigeschaltet();

	/**
	 *
	 * @param landId
	 * @return BigInteger
	 */
	BigInteger anzahlOrteInLand(Long landId);
}
