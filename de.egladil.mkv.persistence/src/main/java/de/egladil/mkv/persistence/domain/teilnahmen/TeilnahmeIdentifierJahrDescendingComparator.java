//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import java.util.Comparator;

import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;

/**
 * TeilnahmeIdentifierJahrDescendingComparator
 */
public class TeilnahmeIdentifierJahrDescendingComparator implements Comparator<TeilnahmeIdentifierProvider> {

	@Override
	public int compare(final TeilnahmeIdentifierProvider o1, final TeilnahmeIdentifierProvider o2) {
		int jahr1 = Integer.valueOf(o1.provideTeilnahmeIdentifier().getJahr());
		int jahr2 = Integer.valueOf(o2.provideTeilnahmeIdentifier().getJahr());

		return jahr2 - jahr1;
	}

}
