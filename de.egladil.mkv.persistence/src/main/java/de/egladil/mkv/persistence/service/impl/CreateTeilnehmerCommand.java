//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import de.egladil.mkv.persistence.domain.auswertungen.Antworteingabemodus;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.plausi.ConvertToKlassenstufeCommand;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * CreateTeilnehmerCommand
 */
public class CreateTeilnehmerCommand implements ICommand {

	private final PublicTeilnehmer teilnehmerPayload;

	private final TeilnehmerBuilder teilnehmerBuilder;

	private Teilnehmer teilnehmer;

	/**
	 * CreateTeilnehmerCommand
	 */
	public CreateTeilnehmerCommand(final TeilnahmeIdentifier teilnahmeIdentifier, final PublicTeilnehmer teilnehmerPayload) {
		if (teilnehmerPayload == null) {
			throw new IllegalArgumentException("teilnehmerPayload null");
		}
		this.teilnehmerPayload = teilnehmerPayload;
		final Klassenstufe klassenstufe = checkAndGetKlassenstufe(teilnehmerPayload.getKlassenstufe().getName());
		this.teilnehmerBuilder = new TeilnehmerBuilder(teilnahmeIdentifier, klassenstufe);
	}

	@Override
	public void execute() {
		teilnehmer = teilnehmerBuilder.nachname(teilnehmerPayload.getNachname()).vorname(teilnehmerPayload.getVorname())
			.zusatz(teilnehmerPayload.getZusatz()).checkAndBuild();
		if (teilnehmerPayload.getEingabemodus() != null) {
			teilnehmer.setEingabemodus(Antworteingabemodus.valueOf(teilnehmerPayload.getEingabemodus()));
		} else {
			teilnehmer.setEingabemodus(null);
		}
		if (teilnehmerPayload.getSprache() != null) {
			teilnehmer.setSprache(teilnehmerPayload.getSprache().convert());
		} else {
			teilnehmer.setSprache(Sprache.de);
		}
	}

	private Klassenstufe checkAndGetKlassenstufe(final String nameKlassenstufe) {
		final ConvertToKlassenstufeCommand cmd = new ConvertToKlassenstufeCommand(nameKlassenstufe);
		cmd.execute();
		return cmd.getKlassenstufe();
	}

	public final Teilnehmer getTeilnehmer() {
		if (teilnehmer == null) {
			throw new IllegalStateException("IllegalState: please invoke execute() first");
		}
		return teilnehmer;
	}

}
