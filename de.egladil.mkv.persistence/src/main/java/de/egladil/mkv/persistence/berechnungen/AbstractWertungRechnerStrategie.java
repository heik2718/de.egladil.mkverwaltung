//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.domain.enums.Wertung;

/**
 * AbstractWertungRechnerStrategie
 */
public abstract class AbstractWertungRechnerStrategie implements IWertungRechnerStrategie {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractWertungRechnerStrategie.class);

	private final AufgabenkategorieDekorator aufgabenkategorie;

	/**
	 * Gibt die passende IWertungRechnerStrategie zurück.
	 *
	 * @param wertung Wertung darf nicht null sein
	 * @param kategorie MKAufgabenkategorie darf nicht null sein
	 * @return IWertungRechnerStrategie
	 * @throws NullPointerException wenn einer der Parameter null ist.
	 */
	public static IWertungRechnerStrategie createWertungRechnerStrategie(final Wertung wertung, final AufgabenkategorieDekorator kategorie) {
		if (wertung == null){
			throw new NullPointerException("Parameter wertung");
		}
		if (kategorie == null){
			throw new NullPointerException("Parameter kategorie");
		}
		switch (wertung) {
		case r:
			return new AufgabeRichtigGeloestRechnerStrategie(kategorie);
		case f:
			return new AufgabeFalschGeloestRechnerStrategie(kategorie);
		case n:
			return new AufgabeNichtGeloestRechnerStrategie(kategorie);
		default:
			LOG.warn("Wertung {} hat keine eigene Strategie. Verwenden AufgabeNichtGeloestRechnerStrategie", wertung);
			return new AufgabeNichtGeloestRechnerStrategie(kategorie);
		}
	}

	/**
	 * @param aufgabenkategorie MKAufgabenkategorie darf nicht null sein.
	 * @throws NullPointerException wenn aufgabenkategorie null
	 */
	protected AbstractWertungRechnerStrategie(final AufgabenkategorieDekorator aufgabenkategorie) {
		if (aufgabenkategorie == null) {
			throw new NullPointerException("Parameter aufgabenkategorie");
		}
		this.aufgabenkategorie = aufgabenkategorie;

	}

	protected AufgabenkategorieDekorator getAufgabenkategorie() {
		return aufgabenkategorie;
	}
}
