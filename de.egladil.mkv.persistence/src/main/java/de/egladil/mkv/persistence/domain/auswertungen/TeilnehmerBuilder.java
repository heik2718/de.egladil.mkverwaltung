//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * TeilnehmerBuilder
 */
public class TeilnehmerBuilder extends TeilnehmerManipulator {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnehmerBuilder.class);

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private Klassenstufe klassenstufe;

	private int nummer;

	private Auswertungsgruppe auswertungsgruppe;

	/**
	 * TeilnehmerBuilder
	 */
	public TeilnehmerBuilder(final TeilnahmeIdentifier teilnahmeIdentifier, final Klassenstufe klassenstufe) {
		super();
		checkAndInit(teilnahmeIdentifier, klassenstufe);
	}

	/**
	 * @param teilnahmeart
	 * @param teilnahmekuerzel
	 * @param jahr
	 * @param klassenstufe
	 */
	private void checkAndInit(final TeilnahmeIdentifier teilnahmeIdentifier, final Klassenstufe klassenstufe) {
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier");
		}

		if (teilnahmeIdentifier.getTeilnahmeart() == null) {
			throw new IllegalArgumentException("teilahmeart ist erforderlich");
		}
		if (StringUtils.isAnyBlank(new String[] { teilnahmeIdentifier.getKuerzel(), teilnahmeIdentifier.getJahr() })) {
			LOG.error("teilnahmekuerzel={}, jahr={}", teilnahmeIdentifier.getKuerzel(), teilnahmeIdentifier.getJahr());
			throw new IllegalArgumentException("teilnahmekuerzel und jahr sind erforderlich");
		}
		if (klassenstufe == null) {
			throw new IllegalArgumentException("klassenstufe ist erforderlich");
		}
		this.klassenstufe = klassenstufe;
		this.teilnahmeIdentifier = teilnahmeIdentifier;
	}

	public TeilnehmerBuilder vorname(final String vorname) {
		this.normalizeAndSetVorname(vorname);
		return this;
	}

	public TeilnehmerBuilder nachname(final String nachname) {
		this.normalizeAndSetNachname(nachname);
		return this;
	}

	public TeilnehmerBuilder zusatz(final String zusatz) {
		this.normalizeAndSetZusatz(zusatz);
		return this;
	}

	public TeilnehmerBuilder nummer(final int nummer) {
		this.nummer = nummer;
		return this;
	}

	/**
	 * Prüft vor allem vorname - nachname- Länge, generiert ein kuerzel und erzeugt eine initialisierte Instanz.
	 *
	 * @return Teilnehmer
	 * @throws IllegalArgumentException wenn zusammengesetzter Name für AuswertungDownload nicht akzeptabel ist
	 */
	public Teilnehmer checkAndBuild() {
		pruefeVornameNachnameValid();
		return bauen();
	}

	private Teilnehmer bauen() {
		final Teilnehmer teilnehmer = new Teilnehmer(teilnahmeIdentifier, klassenstufe, getVorname(), getNachname(), getZusatz());
		teilnehmer.setNummer(nummer);
		teilnehmer.setKuerzel(new KuerzelGenerator().generateDefaultKuerzelWithTimestamp());
		if (auswertungsgruppe != null) {
			auswertungsgruppe.addTeilnehmer(teilnehmer);
		}
		return teilnehmer;
	}
}
