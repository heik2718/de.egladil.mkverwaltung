//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.payload.response.SchuleLage;

/**
 * SchuleLageLandOrtComparator
 */
public class SchuleLageLandOrtComparator implements Comparator<SchuleLage> {

	@Override
	public int compare(final SchuleLage o0, final SchuleLage o1) {
		final String land0 = o0.getLand();
		final String land1 = o1.getLand();

		if (land0.equals(land1)) {
			final String ort0 = o0.getOrt();
			final String ort1 = o1.getOrt();

			return ort0.compareTo(ort1);
		}

		return land0.compareTo(land1);
	}

}
