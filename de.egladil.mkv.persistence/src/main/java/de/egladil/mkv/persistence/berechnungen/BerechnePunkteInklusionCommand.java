//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import java.util.Map;

/**
 * BerechnePunkteInklusionCommand
 */
public class BerechnePunkteInklusionCommand extends AbstractBerechnePunkteCommand {

	/**
	 * BerechnePunkteInklusionCommand
	 */
	public BerechnePunkteInklusionCommand() {
	}

	@Override
	protected void init(final Map<Integer, AufgabenkategorieDekorator> indexAufgabenkategorien) {
		setGuthaben(1200);
		for (int i = 0; i < 2; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.DREI, 3));
		}
		for (int i = 2; i < 4; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.VIER, 3));
		}
		for (int i = 4; i < 6; i++) {
			indexAufgabenkategorien.put(Integer.valueOf(i), new AufgabenkategorieDekorator(MKAufgabenkategorie.FUENF, 3));
		}
	}
}
