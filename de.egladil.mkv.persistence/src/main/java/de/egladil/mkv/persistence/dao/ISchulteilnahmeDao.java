//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;
import java.util.Optional;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * ISchulteilnahmeDao
 */
public interface ISchulteilnahmeDao extends IBaseDao<Schulteilnahme> {

	/**
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return Optional
	 * @throws EgladilStorageException wenns mehr als eins gibt
	 */
	Optional<Schulteilnahme> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier) throws EgladilStorageException;

	/**
	 * Gibt eine Liste aller Schulteilnahmen zu einer gegebenen Schule, repräsentiert durch schulkuerzel zurück.
	 *
	 * @param schulkuerzel
	 * @return
	 * @throws MKVException
	 * @throws EgladilStorageException
	 */
	List<Schulteilnahme> findAllBySchulkuerzel(String schulkuerzel) throws MKVException, EgladilStorageException;

	/**
	 * Sucht alle Schulteilnahmen mit dem gegebenen Jahr.
	 *
	 * @param jahr
	 * @return
	 * @throws MKVException
	 * @throws EgladilStorageException
	 */
	List<Schulteilnahme> findAllByJahr(String jahr) throws MKVException, EgladilStorageException;

	/**
	 *
	 * Stellt fest, ob es eine Schulteilnahme ohne Lehrerzuordnung gibt.
	 *
	 * @param jahr
	 * @return Optional
	 * @throws MKVException
	 * @throws EgladilStorageException
	 */
	Optional<Schulteilnahme> findOrphan(String kuerzel, String jahr) throws MKVException, EgladilStorageException;

}
