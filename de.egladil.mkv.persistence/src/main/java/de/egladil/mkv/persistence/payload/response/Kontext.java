//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

/**
 * Kontext sind die momentaten Konfigurationsdaten beim Zugriff auf die MKV-API
 */
public class Kontext {

	private String environment;

	private String wettbewerbsjahr;

	private int gueltigkeitEinmalpasswort;

	private String startAnmeldungText;

	private boolean downloadFreigegebenPrivat;

	private String datumFreigabePrivat;

	private boolean downloadFreigegebenLehrer;

	private String datumFreigabeLehrer;

	private boolean neuanmeldungFreigegeben;

	private boolean uploadDisabled;

	private boolean wettbewerbBeendet;

	private boolean ankuendigungWartungsmeldung;

	private String wartungsmeldungText;

	private String xsrfToken;

	private String authToken;

	private boolean dumpPayload;

	/** maximale Anzahl bytes, die in einem OpenOffice- content.xml erwartet werden, falls es kein evil content ist. */
	private int maxExtractedBytes;

	public final String getWettbewerbsjahr() {
		return wettbewerbsjahr;
	}

	public final void setWettbewerbsjahr(final String wettbewerbsjahr) {
		this.wettbewerbsjahr = wettbewerbsjahr;
	}

	public final int getGueltigkeitEinmalpasswort() {
		return gueltigkeitEinmalpasswort;
	}

	public final void setGueltigkeitEinmalpasswort(final int gueltigkeitEinmalpasswort) {
		this.gueltigkeitEinmalpasswort = gueltigkeitEinmalpasswort;
	}

	public final boolean isDownloadFreigegebenPrivat() {
		return downloadFreigegebenPrivat;
	}

	public final void setDownloadFreigegebenPrivat(final boolean downloadFreigegebenPrivat) {
		this.downloadFreigegebenPrivat = downloadFreigegebenPrivat;
	}

	public final String getDatumFreigabePrivat() {
		return datumFreigabePrivat;
	}

	public final void setDatumFreigabePrivat(final String datumFreigabePrivat) {
		this.datumFreigabePrivat = datumFreigabePrivat;
	}

	public final boolean isDownloadFreigegebenLehrer() {
		return downloadFreigegebenLehrer;
	}

	public final void setDownloadFreigegebenLehrer(final boolean downloadFreigegebenLehrer) {
		this.downloadFreigegebenLehrer = downloadFreigegebenLehrer;
	}

	public final String getDatumFreigabeLehrer() {
		return datumFreigabeLehrer;
	}

	public final void setDatumFreigabeLehrer(final String datumFreigabeLehrer) {
		this.datumFreigabeLehrer = datumFreigabeLehrer;
	}

	public final boolean isNeuanmeldungFreigegeben() {
		return neuanmeldungFreigegeben;
	}

	public final boolean isUploadDisabled() {
		return uploadDisabled;
	}

	public final void setUploadDisabled(final boolean uploadDisabled) {
		this.uploadDisabled = uploadDisabled;
	}

	public final String getXsrfToken() {
		return xsrfToken;
	}

	public final void setXsrfToken(final String csrfToken) {
		this.xsrfToken = csrfToken;
	}

	public final String getAuthToken() {
		return authToken;
	}

	public final void setAuthToken(final String authToken) {
		this.authToken = authToken;
	}

	public final String getStartAnmeldungText() {
		return startAnmeldungText;
	}

	public final void setStartAnmeldungText(final String startAnmeldungText) {
		this.startAnmeldungText = startAnmeldungText;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Kontext [wettbewerbsjahr=");
		builder.append(wettbewerbsjahr);
		builder.append(", gueltigkeitEinmalpasswort=");
		builder.append(gueltigkeitEinmalpasswort);
		builder.append(", startAnmeldungText=");
		builder.append(startAnmeldungText);
		builder.append(", downloadFreigegebenPrivat=");
		builder.append(downloadFreigegebenPrivat);
		builder.append(", datumFreigabePrivat=");
		builder.append(datumFreigabePrivat);
		builder.append(", downloadFreigegebenLehrer=");
		builder.append(downloadFreigegebenLehrer);
		builder.append(", datumFreigabeLehrer=");
		builder.append(datumFreigabeLehrer);
		builder.append(", neuanmeldungFreigegeben=");
		builder.append(neuanmeldungFreigegeben);
		builder.append(", uploadDisabled=");
		builder.append(uploadDisabled);
		builder.append(", ankuendigungWartungsmeldung=");
		builder.append(ankuendigungWartungsmeldung);
		builder.append(", wartungsmeldungText=");
		builder.append(wartungsmeldungText);
		builder.append("]");
		return builder.toString();
	}

	public final void setNeuanmeldungFreigegeben(final boolean neuanmeldungFreigegeben) {
		this.neuanmeldungFreigegeben = neuanmeldungFreigegeben;
	}

	public final boolean isWettbewerbBeendet() {
		return wettbewerbBeendet;
	}

	public final void setWettbewerbBeendet(final boolean wettbewerbBeendet) {
		this.wettbewerbBeendet = wettbewerbBeendet;
	}

	public final boolean isAnkuendigungWartungsmeldung() {
		return ankuendigungWartungsmeldung;
	}

	public final void setAnkuendigungWartungsmeldung(final boolean ankuendigungWartungsmeldung) {
		this.ankuendigungWartungsmeldung = ankuendigungWartungsmeldung;
	}

	public final String getWartungsmeldungText() {
		return wartungsmeldungText;
	}

	public final void setWartungsmeldungText(final String wartungsmeldungText) {
		this.wartungsmeldungText = wartungsmeldungText;
	}

	public final int getMaxExtractedBytes() {
		return maxExtractedBytes;
	}

	public final void setMaxExtractedBytes(final int maxExtractedBytes) {
		this.maxExtractedBytes = maxExtractedBytes;
	}

	public final String getEnvironment() {
		return environment;
	}

	public final void setEnvironment(final String environment) {
		this.environment = environment;
	}

	public boolean isDumpPayload() {
		return dumpPayload;
	}

	public void setDumpPayload(final boolean dumpPayload) {
		this.dumpPayload = dumpPayload;
	}
}
