//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

/**
 * VordefiniertesUrkundenmotiv
 */
public class VordefiniertesUrkundenmotiv {

	private final Farbschema farbschema;

	private final Ueberschriftfarbe ueberschriftfarbe;

	private final String backgroundClasspathResource;

	public static VordefiniertesUrkundenmotiv fromFarbschema(final Farbschema farbschema) {
		return new VordefiniertesUrkundenmotiv(farbschema);
	}

	private VordefiniertesUrkundenmotiv(final Farbschema farbschema) {
		this.farbschema = farbschema;
		switch (farbschema) {
		case BLUE:
			this.backgroundClasspathResource = "/overlay_blue.pdf";
			this.ueberschriftfarbe = Ueberschriftfarbe.MKV_BLUE;
			break;
		case GREEN:
			this.backgroundClasspathResource = "/overlay_green.pdf";
			this.ueberschriftfarbe = Ueberschriftfarbe.MKV_GREEN;
			break;
		case ORANGE:
			this.backgroundClasspathResource = "/overlay_orange.pdf";
			this.ueberschriftfarbe = Ueberschriftfarbe.MKV_ORANGE;
			break;
		default:
			this.backgroundClasspathResource = "/overlay_empty.pdf";
			this.ueberschriftfarbe = Ueberschriftfarbe.BLACK;
			break;
		}
	}

	public final Ueberschriftfarbe getUeberschriftfarbe() {
		return ueberschriftfarbe;
	}

	public final Farbschema getFarbschemaName() {
		return farbschema;
	}

	public final String getBackgroundClasspathResource() {
		return backgroundClasspathResource;
	}

	@Override
	public String toString() {
		return farbschema.toString();
	}
}
