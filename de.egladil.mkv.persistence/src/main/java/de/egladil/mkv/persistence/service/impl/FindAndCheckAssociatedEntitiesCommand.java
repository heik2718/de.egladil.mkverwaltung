//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.Optional;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * FindAndCheckAssociatedEntitiesCommand sucht die passende Teilnahme.
 */
public class FindAndCheckAssociatedEntitiesCommand implements ICommand {

	private TeilnahmeIdentifier teilnahmeIdentifier;

	private final IPrivatteilnahmeDao privatteilnahmeDao;

	private final ISchulteilnahmeDao schulteilnahmeDao;

	private final IAuswertungsgruppeDao auswertungsgruppeDao;

	private Privatteilnahme privatteilnahme;

	private Schulteilnahme schulteilnahme;

	private Auswertungsgruppe auswertungsgruppe;

	/**
	 * FindAndCheckAssociatedEntitiesCommand
	 */
	public FindAndCheckAssociatedEntitiesCommand(final IPrivatteilnahmeDao privatteilnahmeDao,
		final ISchulteilnahmeDao schulteilnahmeDao, final IAuswertungsgruppeDao auswertungsgruppeDao,
		final ITeilnehmerDao teilnehmerDao) {
		this.privatteilnahmeDao = privatteilnahmeDao;
		this.schulteilnahmeDao = schulteilnahmeDao;
		this.auswertungsgruppeDao = auswertungsgruppeDao;
	}

	public FindAndCheckAssociatedEntitiesCommand withTeilnahmeIdentifier(final TeilnahmeIdentifier teilnahmeIdentifier) {
		this.teilnahmeIdentifier = teilnahmeIdentifier;
		return this;
	}

	@Override
	public void execute() {
		switch (teilnahmeIdentifier.getTeilnahmeart()) {
		case P:
			findPrivatteilnahme();
			break;
		case S:
			findSchulteilnahme();
			break;
		default:
			throw new IllegalArgumentException("noch unbekannte Teilnahmeart " + teilnahmeIdentifier.getTeilnahmeart());
		}
	}

	private void findPrivatteilnahme() {
		final Optional<Privatteilnahme> optTeilnahme = privatteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier);
		if (optTeilnahme.isPresent()) {
			this.privatteilnahme = optTeilnahme.get();
		} else {
			throw new PreconditionFailedException("Es gibt keine Teilnahme mit " + teilnahmeIdentifier.toString());
		}
	}

	private void findSchulteilnahme() {
		final Optional<Schulteilnahme> optTeilnahme = schulteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier);
		if (optTeilnahme.isPresent()) {
			this.schulteilnahme = optTeilnahme.get();
		} else {
			throw new PreconditionFailedException("Es gibt keine Teilnahme mit " + teilnahmeIdentifier.toString());
		}
	}

	public final Privatteilnahme getPrivatteilnahme() {
		return privatteilnahme;
	}

	public final Schulteilnahme getSchulteilnahme() {
		return schulteilnahme;
	}

	public final Auswertungsgruppe getAuswertungsgruppe() {
		return auswertungsgruppe;
	}
}
