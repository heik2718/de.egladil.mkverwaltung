//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.benutzer;

/**
* PersonBasisdaten
*/
public class PersonBasisdaten {

	private String vorname;

	private String nachname;

	private String email;

	private String rolle;

	public final String getVorname() {
		return vorname;
	}

	public final void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	public final String getNachname() {
		return nachname;
	}

	public final void setNachname(final String nachname) {
		this.nachname = nachname;
	}

	public final String getEmail() {
		return email;
	}

	public final void setEmail(final String email) {
		this.email = email;
	}

	public final String getRolle() {
		return rolle;
	}

	public final void setRolle(final String rolle) {
		this.rolle = rolle;
	}
}
