//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.mkv.persistence.annotations.MkvApiRolle;

/**
 * NameAendernPayload
 */
public class PersonAendernPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	@JsonProperty
	private String username;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	private String vorname;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	private String nachname;

	@NotBlank
	@MkvApiRolle
	private String rolle;

	@Honeypot
	@JsonProperty
	private String kleber;

	/**
	 * Erzeugt eine Instanz von NameAendernPayload
	 */
	public PersonAendernPayload() {
	}

	/**
	 * Erzeugt eine Instanz von NameAendernPayload
	 */
	public PersonAendernPayload(final String benutzername, final String vorname, final String nachname, final String kleber) {
		this.username = benutzername;
		this.vorname = vorname;
		this.nachname = nachname;
		this.kleber = kleber;
	}

	/**
	 * Erzeugt eine Instanz von NameAendernPayload
	 */
	public PersonAendernPayload(final String benutzername, final String vorname, final String nachname, final String kleber, final String rolle) {
		this.username = benutzername;
		this.vorname = vorname;
		this.nachname = nachname;
		this.kleber = kleber;
		this.rolle = rolle;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + ((kleber == null) ? 0 : kleber.hashCode());
		result = prime * result + ((nachname == null) ? 0 : nachname.hashCode());
		result = prime * result + ((vorname == null) ? 0 : vorname.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PersonAendernPayload other = (PersonAendernPayload) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (kleber == null) {
			if (other.kleber != null)
				return false;
		} else if (!kleber.equals(other.kleber))
			return false;
		if (nachname == null) {
			if (other.nachname != null)
				return false;
		} else if (!nachname.equals(other.nachname))
			return false;
		if (vorname == null) {
			if (other.vorname != null)
				return false;
		} else if (!vorname.equals(other.vorname))
			return false;
		return true;
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	@Override
	public String toString() {
		return "NameAendernPayload [username=" + username + ", vorname=" + vorname + ", nachname=" + nachname + ", kleber="
			+ kleber + "]";
	}
	/**
	 * Liefert die Membervariable username
	 *
	 * @return die Membervariable username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param username neuer Wert der Membervariablen username
	 */
	public void setUsername(final String benutzername) {
		this.username = benutzername;
	}

	/**
	 * Liefert die Membervariable vorname
	 *
	 * @return die Membervariable vorname
	 */
	public String getVorname() {
		return vorname;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param vorname neuer Wert der Membervariablen vorname
	 */
	public void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	/**
	 * Liefert die Membervariable nachname
	 *
	 * @return die Membervariable nachname
	 */
	public String getNachname() {
		return nachname;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param nachname neuer Wert der Membervariablen nachname
	 */
	public void setNachname(final String nachname) {
		this.nachname = nachname;
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

	/**
	 * @return the rolle
	 */
	public String getRolle() {
		return rolle;
	}

	/**
	 * @param rolle the rolle to set
	 */
	public void setRolle(final String rolle) {
		this.rolle = rolle;
	}

}
