//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.HttpUrl;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * NeueSchulePayload
 */
public class NeueSchulePayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@NotNull
	@Size(min = 1, max = 5)
	private String landkuerzel;

	@NotNull
	@Size(min = 8, max = 8)
	@Kuerzel
	private String ortkuerzel;

	@HttpUrl
	private String url;

	@StringLatin
	@Size(min = 0, max = 200)
	private String strasse;

	/**
	 * Erzeugt eine Instanz von NeueSchulePayload
	 */
	public NeueSchulePayload() {
	}

	/**
	 * Erzeugt eine Instanz von NeueSchulePayload
	 */
	public NeueSchulePayload(final String landkuerzel, final String ortkuerzel, final String name) {
		this.name = name;
		this.landkuerzel = landkuerzel;
		this.ortkuerzel = ortkuerzel;
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	@Override
	public String toString() {
		return "NeueSchulePayload [landkuerzel=" + landkuerzel + ", ortkuerzel=" + ortkuerzel + ", name=" + name + ", url=" + url
			+ ", strasse=" + strasse + "]";
	}

	public String getName() {
		return name;
	}

	public String getLandkuerzel() {
		return landkuerzel;
	}

	public String getOrtkuerzel() {
		return ortkuerzel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(final String strasse) {
		this.strasse = strasse;
	}
}
