//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.bv.aas.domain.BooleanToIntegerConverter;
import de.egladil.common.persistence.IDomainObject;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.mkv.persistence.domain.BenutzerstatusProvider;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;

/**
 * PrivatteilnahmeInformation
 */
@Entity
@Table(name = "vw_privatteilnahmen")
public class PrivatteilnahmeInformation implements IDomainObject, TeilnahmeIdentifierProvider, BenutzerstatusProvider {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonIgnore
	private Long id;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	@Column(name = "kuerzel", length = 8)
	@JsonIgnore
	private String kuerzel;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	@Column(name = "vorname")
	@JsonProperty
	private String vorname;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	@Column(name = "nachname")
	@JsonProperty
	private String nachname;

	@NotBlank
	@Email
	@Column(name = "email", length = 255)
	@JsonProperty
	private String email;

	@Column(name = "ben_id")
	@JsonIgnore
	private Long benutzerId;

	@Column(name = "uuid")
	@JsonProperty("uuid")
	private String benutzerUuid;

	@Column(name = "aktiviert")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean aktiviert;

	@Column(name = "gesperrt")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean gesperrt;

	@Column(name = "anonym")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean anonym;

	@Column(name = "mailnachricht")
	@Convert(converter = BooleanToIntegerConverter.class)
	@JsonProperty
	private boolean mailnachricht;

	@Column(name = "jahr")
	@JsonProperty
	private String jahr;

	/**
	 * PrivatteilnahmeInformation
	 */
	public PrivatteilnahmeInformation() {
	}

	@Override
	public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
		return TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(kuerzel, jahr);
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public String getVorname() {
		return vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public String getEmail() {
		return email;
	}

	public Long getBenutzerId() {
		return benutzerId;
	}

	public String getBenutzerUuid() {
		return benutzerUuid;
	}

	@Override
	public boolean isAktiviert() {
		return aktiviert;
	}

	@Override
	public boolean isAnonym() {
		return anonym;
	}

	public boolean isMailnachricht() {
		return mailnachricht;
	}

	public String getJahr() {
		return jahr;
	}

	@Override
	public boolean isGesperrt() {
		return gesperrt;
	}

}
