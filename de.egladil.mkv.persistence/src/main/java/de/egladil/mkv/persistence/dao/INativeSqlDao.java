//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;

/**
 * INativeSqlDao
 */
public interface INativeSqlDao {

	/**
	 * Ermittelt die Anzahl der Schulteilnahmen mit gegebenen Landkuerzeln.
	 *
	 * @param landkuerzel List von landkuerzeln. Das ermöglicht, die Anzhal von Schulteilnahmen zu Ländergruppen zu
	 * ermitteln.
	 * @param jahr String das Jahr
	 * @return int
	 */
	int getAnzahlSchulteilnahmenLand(List<String> landkuerzel, String jahr);

	/**
	 * Ermittelt die Anzahl der Lösungszettel aus Schulteilnahmen mit verschiedenen Parametern.
	 *
	 * @param jahr String wenn null, dann alle Jahre.
	 * @param klassenstufe Klassenstufe wenn null, dann alle Klassenstufen
	 * @param landkuerzel List von Länderkürzeln. Darf nicht leer sein.
	 * @return int
	 */
	int getAnzahlLoesungszettelSchulteilnahmen(String jahr, Klassenstufe klassenstufe, List<String> landkuerzel);

	/**
	 * Gibt alle Lösungszettel der gegebenen Klassenstufe im gegebenen Jahr zurück.
	 *
	 * @param jahr String
	 * @param klassenstufe
	 * @return List
	 */
	List<Loesungszettel> getLoesungszettelByJahrUndKlassenstufe(String jahr, Klassenstufe klassenstufe);

	/**
	 * Ermittelt alle Lösungszettel, die zu Ländern in der kuerzelliste stehen.
	 *
	 * @param jahr String das Wettbewerbsjahr
	 * @param klassenstufe Klassenstufe die Klassenstufe
	 * @param kuerzelliste List
	 * @return List
	 */
	List<Loesungszettel> getLoesungszettelSchulen(String jahr, Klassenstufe klassenstufe, List<String> kuerzelliste);

	/**
	 * Ermittelt alle Lösungszettel, die zu Privatteilnehmern gehören.
	 *
	 * @param jahr String das Wettbewerbsjahr
	 * @param klassenstufe Klassenstufe die Klassenstufe
	 * @return List
	 */
	List<Loesungszettel> getLoesungszettelPrivat(String jahr, Klassenstufe klassenstufe);

	/**
	 * @return List
	 */
	List<String> getAuswertbareJahre();

	/**
	 * Sucht alle Jahre, in denen es Teilnahmen zu einem gegebenen Land gibt.
	 *
	 * @param landkuerzel String
	 * @return List
	 */
	List<String> findTeilnahmejahreLand(String landkuerzel);

	/**
	 *
	 * @param schulkuerzel
	 * @return
	 */
	int getAnzahlSchulteilnahmen(String schulkuerzel);

	/**
	 * Sucht zur gegebenen Auswertungsgruppe alle Teilnahmejahre
	 *
	 * @param teilnahmekuerzel String das Schulkürzel
	 * @return List
	 */
	List<String> findTeilnahmejahreSchule(String teilnahmekuerzel);

	/**
	 * Ermittelt die Anzahl der Lösungszettel zu gegebener Teilnahme.
	 *
	 * @param teilnahmeIdentifierProvider TeilnahmeIdentifierProvider
	 * @return int
	 */
	int getAnzahlLoesungszettel(TeilnahmeIdentifierProvider teilnahmeIdentifierProvider);

	/**
	 * Gibt die Liste aller Schulteilnahmen zu gegebenem Lehrerkonto.
	 *
	 * @param lehrerkontoId Long
	 * @return List
	 */
	List<Schulteilnahme> getSchulteilnahmenForLehrer(Long lehrerkontoId);

	/**
	 * Gibt die Anzahl aller Teilnahmen zu diesem Lehrerkonto zurück.
	 *
	 * @param lehrerkontoId Long
	 * @return int
	 */
	int getAnzahlSchulteilnahmenForLehrer(Long lehrerkontoId);
}
