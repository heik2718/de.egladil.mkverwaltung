//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.Base64;

/**
 * ImageBase64Converter
 */
public class ImageBase64Converter implements ICommand {

	private final ImageSerialisierung image;

	/**
	 * ImageBase64Converter
	 */
	public ImageBase64Converter(final ImageSerialisierung image) {
		if (image == null) {
			throw new IllegalArgumentException("image null");
		}
		this.image = image;
	}

	@Override
	public void execute() {
		final String encoded = Base64.getEncoder().encodeToString(image.getData());
		image.setBase64Image(encoded);
	}
}
