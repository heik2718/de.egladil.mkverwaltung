//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================
	
package de.egladil.mkv.persistence.dao;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;

/**
* IAuswertungDownloadDao
*/
public interface IAuswertungDownloadDao extends IBaseDao<AuswertungDownload> {

}
	