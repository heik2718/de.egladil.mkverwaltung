//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * PublicKlassenstufe
 */
public class PublicKlassenstufe implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private String name;

	@JsonProperty
	private String label;

	@JsonProperty
	private int anzahlAufgaben;

	@JsonProperty
	private int anzahlSpalten;

	public static PublicKlassenstufe fromKlassenstufe(final Klassenstufe klassenstufe) {
		if (klassenstufe == null) {
			throw new IllegalArgumentException("klassenstufe null");
		}
		final PublicKlassenstufe result = new PublicKlassenstufe();
		result.name = klassenstufe.name();
		result.label = klassenstufe.getLabel();
		result.anzahlAufgaben = klassenstufe.getAnzahlAufgaben();
		result.anzahlSpalten = klassenstufe.getAnzahlAufgaben() / 3;
		return result;
	}

	/**
	 * PublicKlassenstufe
	 */
	PublicKlassenstufe() {
	}

	public final String getName() {
		return name;
	}

	public final String getLabel() {
		return label;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PublicKlassenstufe other = (PublicKlassenstufe) obj;
		if (label == null) {
			if (other.label != null) {
				return false;
			}
		} else if (!label.equals(other.label)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicKlassenstufe [name=");
		builder.append(name);
		builder.append(", label=");
		builder.append(label);
		builder.append("]");
		return builder.toString();
	}

}
