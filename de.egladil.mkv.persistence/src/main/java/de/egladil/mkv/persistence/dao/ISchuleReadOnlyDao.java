//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;

/**
 * ISchuleReadOnlyDao
 */
public interface ISchuleReadOnlyDao extends IBaseDao<SchuleReadOnly> {

	/**
	 * Ermittelt die Anzahl der Treffer zu den Suchparametern.
	 *
	 * @param name String name oder Teile des Namen
	 * @param ort String name oder Teile des Namen
	 * @return BigInteger
	 */
	BigInteger anzahlTreffer(String name, String ort);

	/**
	 *
	 * @param ortId
	 * @return
	 */
	BigInteger anzahlSchulenInOrt(Long ortId);

	/**
	 * Sucht Schulen mit gegebenem namen unscharf.
	 *
	 * @param name String name oder Teile des Namen
	 * @param ort String name oder Teile des namen des Orts
	 * @return List
	 */
	List<SchuleReadOnly> findSchulen(String name, String ort);

	/**
	 * In jedem Land gibt es genau eine unbekannte Schule (name = "Unbekannt").
	 *
	 * @param landkuerzel String
	 * @return Optional
	 */
	Optional<SchuleReadOnly> findUnbekannteSchuleImLand(String landkuerzel);

	/**
	 *
	 * @param kuerzel String
	 * @return Optional
	 */
	Optional<SchuleReadOnly> findByKuerzel(String kuerzel);

}
