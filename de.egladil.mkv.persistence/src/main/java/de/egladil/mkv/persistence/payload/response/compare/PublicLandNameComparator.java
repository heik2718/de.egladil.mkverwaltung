//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.payload.response.PublicLand;

/**
 * PublicLandNameComparator
 */
public class PublicLandNameComparator implements Comparator<PublicLand> {

	@Override
	public int compare(final PublicLand arg0, final PublicLand arg1) {
		return arg0.getName().compareToIgnoreCase(arg1.getName());
	}
}

