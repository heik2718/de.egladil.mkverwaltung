//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import de.egladil.common.validation.validators.AbstractWhitelistValidator;
import de.egladil.mkv.persistence.annotations.Wertungscode;

/**
 * Erlaubte Zeichen sind r,f und n.
 *
 */
public class WertungscodeValidator extends AbstractWhitelistValidator<Wertungscode, String> {

	private static final String REGEXP = "[rfn]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}

}
