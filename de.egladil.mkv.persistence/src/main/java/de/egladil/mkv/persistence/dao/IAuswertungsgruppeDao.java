//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;
import java.util.Optional;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * IAuswertungsgruppeDao
 */
public interface IAuswertungsgruppeDao extends IBaseDao<Auswertungsgruppe> {

	/**
	 * Sucht eine Auswertungsgruppen, die kein parent hat mit dem gegebenen teilnahmeIdentifier.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier darf nicht null sein
	 * @return Optional
	 */
	Optional<Auswertungsgruppe> findRootByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Sucht alle Auswertungsgruppen mit dem gegebenen teilnahmeIdentifier
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return List
	 */
	List<Auswertungsgruppe> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Sucht die Rootgrupppen mit Jahr.
	 *
	 * @param jahr String darf nicht null sein.
	 * @return List
	 */
	List<Auswertungsgruppe> findRootgruppenByJahr(String jahr);
}
