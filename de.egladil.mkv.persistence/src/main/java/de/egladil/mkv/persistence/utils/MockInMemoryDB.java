//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Downloaddaten;
import de.egladil.mkv.persistence.domain.teilnahmen.Lehrerdownload;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * MockInMemoryDB
 */
public class MockInMemoryDB {

	public static final String UUID_LEHRER_1 = "gdsguf-aushd";

	public static final String UUID_LEHRER_2 = "aferdjdh-gagzaug";

	public static final String UUID_LEHRER_3 = "b645db-aushd-agfg";

	public static final String SCHULKUERZEL_1 = "HTGFRD5H";

	public static final String SCHULKUERZEL_2 = "R4GFRD5H";

	public static final String SCHULKUERZEL_3 = "A36TRD5H";

	public static final String CHECSUMME_LEHRERKONTO_1 = "asghshisaaojvsdaop";

	public static final Long ID_SCHULTEILNAHME_SCHULE_1_2016 = 1l;

	public static final Long ID_SCHULTEILNAHME_SCHULE_1_2017 = 2l;

	private Map<String, Schule> schulen;

	private Map<String, Benutzerkonto> benutzerkonten;

	private Map<String, Lehrerkonto> lehrerkonten;

	private Map<String, Privatkonto> privatkonten;

	// Achtung: es müssen verschiedene Kürzel verwendet werden für Schulen und Privatteilnahmen.
	private Map<String, Upload> uploads;

	private Map<Long, Schulteilnahme> schulteilnahmen;

	private Map<String, List<LehrerteilnahmeInformation>> lehrerteilnahmeinfos;

	/**
	 * MockInMemoryDB ist nur zu Testzwecken im mkv gedacht.
	 */
	@Deprecated
	public MockInMemoryDB() {

		schulteilnahmen = new HashMap<>();
		{
			final Schulteilnahme schulteilnahme = new Schulteilnahme();
			schulteilnahme.setJahr("2016");
			schulteilnahme.setId(ID_SCHULTEILNAHME_SCHULE_1_2016);
			schulteilnahme.setKuerzel(SCHULKUERZEL_1);
			schulteilnahmen.put(schulteilnahme.getId(), schulteilnahme);
		}
		{
			final Schulteilnahme schulteilnahme = new Schulteilnahme();
			schulteilnahme.setJahr("2017");
			schulteilnahme.setId(ID_SCHULTEILNAHME_SCHULE_1_2017);
			schulteilnahme.setKuerzel(SCHULKUERZEL_1);
			schulteilnahmen.put(schulteilnahme.getId(), schulteilnahme);
		}

		uploads = new HashMap<>();
		{
			final Upload upload = new Upload();
			upload.setTeilnahmeart(Teilnahmeart.S);
			upload.setKuerzel(SCHULKUERZEL_1);
			upload.setDateiname("UTRDFL.ods");
			upload.setChecksumme(CHECSUMME_LEHRERKONTO_1);
			upload.setMimetype(UploadMimeType.ODS);
			upload.setUploadOhneDownloads(false);
			upload.setJahr("2016");
			uploads.put(SCHULKUERZEL_1, upload);
		}

		schulen = new HashMap<>();
		{
			final Schule schule = new Schule();
			schule.setId(1l);
			schule.setKuerzel(SCHULKUERZEL_1);
			schule.setName("Erste Grundschule");
			schulen.put(SCHULKUERZEL_1, schule);
		}
		{
			final Schule schule = new Schule();
			schule.setId(2l);
			schule.setKuerzel(SCHULKUERZEL_2);
			schule.setName("Zweite Grundschule");
			schulen.put(SCHULKUERZEL_2, schule);
		}
		{
			final Schule schule = new Schule();
			schule.setId(3l);
			schule.setKuerzel(SCHULKUERZEL_3);
			schule.setName("Dritte Grundschule");
			schulen.put(SCHULKUERZEL_3, schule);
		}

		benutzerkonten = new HashMap<>();

		{
			final Benutzerkonto konto = new Benutzerkonto();
			konto.setId(1l);
			konto.setAktiviert(true);
			konto.setAnwendung(Anwendung.MKV);
			konto.setEmail("horst-schlemmer@egladil.de");
			konto.setLoginName(konto.getEmail());
			konto.setUuid(UUID_LEHRER_1);
			konto.addRolle(new Rolle(Role.MKV_LEHRER));
			benutzerkonten.put(UUID_LEHRER_1, konto);
		}

		{
			final Benutzerkonto konto = new Benutzerkonto();
			konto.setId(2l);
			konto.setAktiviert(true);
			konto.setAnwendung(Anwendung.MKV);
			konto.setEmail("barbara-rabarber@egladil.de");
			konto.setLoginName(konto.getEmail());
			konto.setUuid(UUID_LEHRER_2);
			konto.addRolle(new Rolle(Role.MKV_LEHRER));
			benutzerkonten.put(UUID_LEHRER_2, konto);
		}

		{
			final Benutzerkonto konto = new Benutzerkonto();
			konto.setId(2l);
			konto.setAktiviert(false);
			konto.setAnonym(true);
			konto.setAnwendung(Anwendung.MKV);
			konto.setEmail("XXX-YYY@egladil.de");
			konto.setLoginName(konto.getEmail());
			konto.setUuid(UUID_LEHRER_3);
			konto.addRolle(new Rolle(Role.MKV_LEHRER));
			benutzerkonten.put(UUID_LEHRER_3, konto);
		}

		lehrerkonten = new HashMap<>();
		lehrerteilnahmeinfos = new HashMap<>();
		{
			// konto mit Teilnahmen 2016 und 2017. 2017 keine Downloads, 2016 upload mit checksumme
			final Schule schule = schulen.get(SCHULKUERZEL_1);

			final Lehrerkonto lehrerkonto = new Lehrerkonto(UUID_LEHRER_1, new Person("Horst", "Schlemmer"), schule, true);

			{
				// Lehrerteilnahme lehrerteilnahme = new
				// Lehrerteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2016));
				// lehrerkonto.addTeilnahme(lehrerteilnahme);
				lehrerkonto.addSchulteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2016));
			}

			{
				// final Lehrerteilnahme lehrerteilnahme = new
				// Lehrerteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2017));
				// lehrerkonto.addTeilnahme(lehrerteilnahme);
				lehrerkonto.addSchulteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2017));
			}

			final Lehrerdownload download = new Lehrerdownload();
			download.setId(14l);
			final Downloaddaten downloaddaten = new Downloaddaten();
			downloaddaten.setDateiname("2016_minikaenguru_klasse_1.zip");
			downloaddaten.setJahr("2016");
			downloaddaten.setAnzahl(1);
			download.setDownloaddaten(downloaddaten);
			lehrerkonto.addLehrerdownload(download);

			lehrerkonten.put(UUID_LEHRER_1, lehrerkonto);

			List<LehrerteilnahmeInformation> teilnahmeinfos = lehrerteilnahmeinfos.get(SCHULKUERZEL_1);
			if (teilnahmeinfos == null) {
				teilnahmeinfos = new ArrayList<>();
			}

			{
				final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation(1l, SCHULKUERZEL_1, "Horst", "schlemmer",
					"horst-schlemmer@egladil.de", 1l, true, false, true, "2016", "Erste Grundschule", SCHULKUERZEL_1);
				teilnahmeinfos.add(info);
			}
			{
				final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation(2l, SCHULKUERZEL_1, "Horst", "schlemmer",
					"horst-schlemmer@egladil.de", 1l, true, false, true, "2017", "Erste Grundschule", SCHULKUERZEL_1);
				teilnahmeinfos.add(info);
			}
			lehrerteilnahmeinfos.put(SCHULKUERZEL_1, teilnahmeinfos);

		}
		{
			// Schule 1 mit anderem Lehrerkonto und Teilnahme im Jahr 2017 mit downloads, ohne uploads
			final Schule schule = schulen.get(SCHULKUERZEL_1);

			final Lehrerkonto lehrerkonto = new Lehrerkonto(UUID_LEHRER_2, new Person("Barbara", "Rabarber"), schule, false);
			// final Lehrerteilnahme lehrerteilnahme = new
			// Lehrerteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2017));
			// lehrerkonto.addTeilnahme(lehrerteilnahme);
			lehrerkonto.addSchulteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2017));

			final Lehrerdownload download = new Lehrerdownload();
			download.setId(14l);
			final Downloaddaten daten = new Downloaddaten();
			daten.setDateiname("2016_minikaenguru_klasse_2.zip");
			daten.setJahr("2017");
			daten.setAnzahl(1);
			download.setDownloaddaten(daten);
			lehrerkonto.addLehrerdownload(download);

			lehrerkonten.put(UUID_LEHRER_2, lehrerkonto);

			List<LehrerteilnahmeInformation> teilnahmeinfos = lehrerteilnahmeinfos.get(SCHULKUERZEL_1);
			if (teilnahmeinfos == null) {
				teilnahmeinfos = new ArrayList<>();
			}

			{
				final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation(3l, SCHULKUERZEL_1, "Barbara", "Rabarber",
					"barbara-rabarber@egladil.de", 3l, true, false, true, "2017", "Erste Grundschule", SCHULKUERZEL_1);
				teilnahmeinfos.add(info);
			}
			lehrerteilnahmeinfos.put(SCHULKUERZEL_1, teilnahmeinfos);
		}
		{
			// Schule 1 mit anderem Lehrerkonto und Teilnahme im Jahr 2017 mit downloads, ohne uploads
			final Schule schule = schulen.get(SCHULKUERZEL_1);

			// Anonymer Lehrer von 2016 Schule 1
			final Person person = new Person("XXXX", "YYYY");
			person.setAnonym(true);
			final Lehrerkonto lehrerkonto = new Lehrerkonto(UUID_LEHRER_3, person, schule, true);
			// final Lehrerteilnahme lehrerteilnahme = new
			// Lehrerteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2016));
			// lehrerkonto.addTeilnahme(lehrerteilnahme);
			lehrerkonto.addSchulteilnahme(schulteilnahmen.get(ID_SCHULTEILNAHME_SCHULE_1_2016));

			final Lehrerdownload lehrerdownload = new Lehrerdownload();
			lehrerdownload.setId(1l);
			final Downloaddaten downloaddaten = new Downloaddaten();
			downloaddaten.setAnzahl(2);
			downloaddaten.setDateiname("2016_minikaenguru_klasse_1.zip");
			downloaddaten.setJahr("2016");
			lehrerdownload.setDownloaddaten(downloaddaten);
			lehrerkonto.addLehrerdownload(lehrerdownload);
			lehrerkonten.put(UUID_LEHRER_3, lehrerkonto);

			List<LehrerteilnahmeInformation> teilnahmeinfos = lehrerteilnahmeinfos.get(SCHULKUERZEL_1);
			if (teilnahmeinfos == null) {
				teilnahmeinfos = new ArrayList<>();
			}

			{
				final LehrerteilnahmeInformation info = new LehrerteilnahmeInformation(3l, SCHULKUERZEL_1, "XXXX", "YYYY",
					"xxxx-yyyy@egladil.de", 3l, false, true, true, "2016", "Erste Grundschule", SCHULKUERZEL_1);
				teilnahmeinfos.add(info);
			}
			lehrerteilnahmeinfos.put(SCHULKUERZEL_1, teilnahmeinfos);
		}
	}

	public Optional<Lehrerkonto> findLehrerkontoByUuid(final String uuid) {
		final Lehrerkonto lehrerkonto = lehrerkonten.get(uuid);
		if (lehrerkonto != null) {
			return Optional.of(lehrerkonto);
		}
		return Optional.empty();
	}

	public Optional<Schule> findSchuleByKuerzel(final String kuerzel) {
		final Schule schule = schulen.get(kuerzel);
		if (schule == null) {
			return Optional.empty();
		}
		return Optional.of(schule);
	}

	public Schulteilnahme findSchulteilnahmeById(final Long id) {
		return schulteilnahmen.get(id);
	}

	public List<Schulteilnahme> findSchulteilnahmen(final Schule schule) {
		final List<Schulteilnahme> result = new ArrayList<>();
		for (final Schulteilnahme teilnahme : this.schulteilnahmen.values()) {
			if (teilnahme.getKuerzel().equals(schule.getKuerzel())) {
				result.add(teilnahme);
			}
		}
		return result;
	}

	public List<LehrerteilnahmeInformation> findAllLehrerteilnahmeinfosFuerSchule(final String schulkuerzel) {
		final List<LehrerteilnahmeInformation> result = lehrerteilnahmeinfos.get(schulkuerzel);
		if (result == null) {
			return new ArrayList<>();
		}
		return result;
	}

	public Benutzerkonto findBenutzerkontoByUuid(final String uuid) {
		return benutzerkonten.get(uuid);
	}
}
