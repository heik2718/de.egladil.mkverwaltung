//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain;

/**
 * BenutzerstatusProvider
 */
public interface BenutzerstatusProvider {

	boolean isAnonym();

	boolean isAktiviert();

	boolean isGesperrt();

}
