//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao;

import java.util.List;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.IBaseDao;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * IUploadInfoDao
 */
public interface IUploadInfoDao extends IBaseDao<UploadInfo> {

	/**
	 * Sucht alle uploads, deren Mimetype nicht UNKNOWN ist mit dem gegebenen Status.
	 *
	 * @param status UploadStatus darf nicht null sein.
	 * @return List
	 */
	List<UploadInfo> findValidUploadsByStatus(UploadStatus status);

	/**
	 * Sucht die uploads zu einer durch teilnahmeIdentifier spezifizierten Teilnahme.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return List
	 */
	List<UploadInfo> findByTeilnahmeIdentifier(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Ermittelt die Anzahl der uploads zur spezifizierten Teilnahme
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @return int
	 */
	long anzahlUploadInfos(TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Ermittelt die UploadInfo mit dem gegebenen Dateinamen.
	 *
	 * @param dateiname String
	 * @return UploadInfo oder null
	 * @throws EgladilStorageException falls es mehr als einen Treffer gibt.
	 */
	UploadInfo findByDateiname(String dateiname) throws EgladilStorageException;

}
