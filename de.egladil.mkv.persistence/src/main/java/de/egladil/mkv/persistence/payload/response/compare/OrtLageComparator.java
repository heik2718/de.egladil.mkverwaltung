//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.payload.response.OrtLage;

/**
 * SchuleLageComparator
 */
public class OrtLageComparator implements Comparator<OrtLage> {

	@Override
	public int compare(final OrtLage o0, final OrtLage o1) {
		final String land0 = o0.getLand();
		final String land1 = o1.getLand();

		return land0.compareTo(land1);
	}

}
