//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.converter;

import de.egladil.mkv.persistence.domain.enums.Antwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;

/**
 * AntwortcodeItemConverter konvertiert einen String der Länge 1 in einen PublicAntwortbuchstabe.
 */
public class AntwortcodeItemConverter {

	/**
	 * Wandelt buchstabe um in PublicAntwortbuchstabe.
	 *
	 * @param buchstabe String der Länge 1. Erlaubt sind nur A-E und N
	 * @return PublicAntwortbuchstabe
	 */
	public PublicAntwortbuchstabe fromString(final String buchstabe) {
		if ("-".equals(buchstabe)) {
			return new PublicAntwortbuchstabe("-", 0);
		}
		final Antwortbuchstabe antwortbuchstabe = Antwortbuchstabe.valueOfSingleLetter(buchstabe);
		return new PublicAntwortbuchstabe(antwortbuchstabe.name(), antwortbuchstabe.getNummer());
	}

	/**
	 *
	 * @param antwortbuchstabe PublicAntwortbuchstabe
	 * @return String
	 */
	public String fromAntwortbuchstabe(final PublicAntwortbuchstabe antwortbuchstabe) {
		if (antwortbuchstabe == null) {
			throw new IllegalArgumentException("antwortbuchstabe null");
		}
		if (antwortbuchstabe.getCode() == 0) {
			return "N";
		}
		return antwortbuchstabe.getBuchstabe().toUpperCase();
	}
}
