//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.filter;

import java.util.List;

import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;

/**
 * FindTeilnahmeLehrerJahrFilter
 */
public class FindTeilnahmeLehrerJahrFilter {

	private final long lehrerBenutzerId;

	private final String jahr;

	/**
	 * Erzeugt eine Instanz von FindTeilnahmeLehrerJahrFilter
	 *
	 * @throws NullPointerException wenn jahr null.
	 */
	public FindTeilnahmeLehrerJahrFilter(final long lehrerBenutzerId, final String jahr) {
		if (jahr == null) {
			throw new NullPointerException("jahr");
		}
		this.lehrerBenutzerId = lehrerBenutzerId;
		this.jahr = jahr;
	}

	public LehrerteilnahmeInformation findTeilnahme(final List<LehrerteilnahmeInformation> lehrerteilnahmen) {
		if (lehrerteilnahmen == null) {
			throw new NullPointerException("lehrerteilnahmen");
		}
		for (final LehrerteilnahmeInformation i : lehrerteilnahmen) {
			if (lehrerBenutzerId == i.getBenutzerId().longValue() && jahr.equals(i.getJahr())) {
				return i;
			}
		}
		return null;
	}

}
