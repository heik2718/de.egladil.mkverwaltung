//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import de.egladil.common.validation.validators.AbstractWhitelistValidator;
import de.egladil.mkv.persistence.annotations.Antwortcode;

/**
 * @author heikew
 *
 */
public class AntwortcodeValidator extends AbstractWhitelistValidator<Antwortcode, String> {

	private static final String REGEXP = "[ABCDENabcden-]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}

}
