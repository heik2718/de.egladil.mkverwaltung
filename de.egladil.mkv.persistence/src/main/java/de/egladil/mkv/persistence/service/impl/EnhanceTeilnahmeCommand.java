//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpMethod;

import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * EnhanceTeilnahmeCommand
 */
public class EnhanceTeilnahmeCommand {

	private final TeilnehmerFacade teilnehmerFacade;

	private final PublicTeilnahme publicTeilnahme;

	private final String url;

	private final String wettbewerbsjahr;

	/**
	 * EnhanceTeilnahmeCommand
	 */
	public EnhanceTeilnahmeCommand(final TeilnehmerFacade teilnehmerFacade, final PublicTeilnahme publicTeilnahme) {
		this.teilnehmerFacade = teilnehmerFacade;
		this.publicTeilnahme = publicTeilnahme;
		final TeilnahmeIdentifier ti = publicTeilnahme.getTeilnahmeIdentifier();
		url = "/teilnahmen/" + ti.getJahr() + "/" + ti.getTeilnahmeart().toString() + "/" + ti.getKuerzel();
		this.wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	public void execute() {
		final long anzahlUploads = teilnehmerFacade.getAnzahlUploads(publicTeilnahme);
		publicTeilnahme.setAnzahlUploads(anzahlUploads);

		final long anzahlLoesungszettel = teilnehmerFacade.getAnzahlLoesungszettel(publicTeilnahme);
		publicTeilnahme.setAnzahlLoesungszettel(anzahlLoesungszettel);

		if (anzahlUploads > 0) {
			publicTeilnahme.setAnzahlTeilnehmer(anzahlLoesungszettel);
		} else {
			final long anzahlTeilnehmer = teilnehmerFacade.getAnzahlTeilnehmer(publicTeilnahme);
			publicTeilnahme.setAnzahlTeilnehmer(anzahlTeilnehmer);
		}

		final TeilnahmeIdentifier teilnahmeIdentifier = publicTeilnahme.getTeilnahmeIdentifier();

		if (wettbewerbsjahr.equals(teilnahmeIdentifier.getJahr())) {
			publicTeilnahme.setAktuelle(true);
		}

		final HateoasPayload hateoasPayload = new HateoasPayload(teilnahmeIdentifier.getKuerzel(), url);
		if (Teilnahmeart.S == teilnahmeIdentifier.getTeilnahmeart()) {
			{
				final HateoasLink link = new HateoasLink(url + "/schulstatistik", "schulstatistik", HttpMethod.GET.toString(),
					MediaType.APPLICATION_JSON);
				hateoasPayload.addLink(link);
			}
			if (publicTeilnahme.isAktuelle()) {
				final HateoasLink link = new HateoasLink("/rootgruppen/" + teilnahmeIdentifier.getKuerzel() + "/schulstatistik",
					"klassenstatistik", HttpMethod.GET.toString(), MediaType.APPLICATION_JSON);
				hateoasPayload.addLink(link);
			}
		}
		publicTeilnahme.setHateoasPayload(hateoasPayload);
	}
}
