//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.validation;

import de.egladil.common.validation.validators.AbstractWhitelistValidator;
import de.egladil.mkv.persistence.annotations.Loesungscode;

/**
 * @author heikew
 *
 */
public class LoesungscodeValidator extends AbstractWhitelistValidator<Loesungscode, String> {

	private static final String REGEXP = "[ABCDE]*";

	/**
	 * @see de.egladil.common.validation.validators.AbstractWhitelistValidator#getWhitelist()
	 */
	@Override
	protected String getWhitelist() {
		return REGEXP;
	}

}
