//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.common.persistence.ILoggable;

/**
 * Downloaddaten
 */
@Embeddable
public class Downloaddaten implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Column(name = "WAS")
	@NotNull
	@Size(max = 40)
	private String dateiname;

	@NotBlank
	@Column(name = "JAHR", length = 4)
	private String jahr;

	@Column(name = "ANZAHL")
	private int anzahl;

	/**
	 *
	 */
	public void anzahlErhoehen() {
		anzahl++;
	}

	@Override
	public String toString() {
		return "Downloaddaten [jahr=" + jahr + ", dateiname=" + dateiname + ", anzahl=" + anzahl + "]";
	}

	/**
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return toString();
	}

	public String getDateiname() {
		return dateiname;
	}

	public void setDateiname(final String dateiart) {
		this.dateiname = dateiart;
	}

	public String getJahr() {
		return jahr;
	}

	public void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(final int anzahl) {
		this.anzahl = anzahl;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
