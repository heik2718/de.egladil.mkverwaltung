//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import com.google.inject.Singleton;

import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * KuerzelGenerator generiert Kürzel.
 */
@Singleton
public class KuerzelGenerator {

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

	private Random random = new Random();

	/**
	 * Generiert einen Zufallsstring gegeben Länge mit den Zeichen aus charPool. Basiert auf Random.
	 *
	 * @param laenge int die Länge. Muss mindestens gleich 6 sein.
	 * @param charPool die verwendeten Zeichen. Muss Mindestlänge 26 haben.
	 * @return String
	 */
	public String generateKuerzel(final int length, final char[] charPool) {
		if (charPool == null) {
			throw new MKVException("charPool darf nicht null sein");
		}
		if (charPool.length < 26) {
			throw new MKVException("charPool muss mindestlaenge 26 haben");
		}
		final StringBuilder sb = new StringBuilder();
		for (int loop = 0; loop < length; loop++) {
			final int index = random.nextInt(charPool.length);
			sb.append(charPool[index]);
		}
		final String nonce = sb.toString();
		return nonce;
	}

	/**
	 * Generiert ein Kürzel der Standardlänge 8 mit dem Standardzeichensatz A-Z0-0.
	 *
	 * @return
	 */
	public String generateDefaultKuerzel() {
		final String result = generateKuerzel(MKVConstants.DEFAULT_LENGTH, MKVConstants.DEFAULT_CHARS);
		return result;
	}

	/**
	 * Generiert ein Kürzel der Standardlänge 22 aus einem Default-Kuerzel und dem aktuellen timestamp format
	 * yyyyMMddHHmmss.
	 *
	 * @return String
	 */
	public String generateDefaultKuerzelWithTimestamp() {
		final String result = generateKuerzel(MKVConstants.DEFAULT_LENGTH, MKVConstants.DEFAULT_CHARS);
		final String formatDateTime = LocalDateTime.now().format(DATE_TIME_FORMATTER);
		return result + formatDateTime;
	}
}
