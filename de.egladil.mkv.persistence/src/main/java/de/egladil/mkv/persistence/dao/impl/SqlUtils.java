//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.math.BigInteger;

import javax.persistence.Query;

import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * SqlUtils
 */
public class SqlUtils {

	public static BigInteger getCount(final Query query) {
		final Object res = query.getSingleResult();

		if (!(res instanceof BigInteger)) {
			throw new MKVException("result ist kein BigInteger, sondern " + res.getClass());
		}

		return (BigInteger) res;
	}
}
