//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.payload.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;

/**
 * PublicFarbschema
 */
public class PublicFarbschema implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private String name;

	@JsonProperty
	private String label;

	@JsonProperty
	private String thumbnail;

	/**
	 * PublicFarbschema
	 */
	public PublicFarbschema() {
	}

	public final String getName() {
		return name;
	}

	final void setName(final String farbschema) {
		this.name = farbschema;
	}

	public final String getLabel() {
		return label;
	}

	final void setLabel(final String label) {
		this.label = label;
	}

	public final String getThumbnail() {
		return thumbnail;
	}

	public final void setThumbnail(final String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public static PublicFarbschema fromFarbschema(final Farbschema farbschema) {
		final PublicFarbschema result = new PublicFarbschema();
		result.label = farbschema.getLabel();
		result.name = farbschema.name();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("PublicFarbschema [name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

}
