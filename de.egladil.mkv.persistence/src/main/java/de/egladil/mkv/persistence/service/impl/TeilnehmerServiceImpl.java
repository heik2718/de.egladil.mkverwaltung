//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.auswertungen.Antworteingabemodus;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerAenderer;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.plausi.CheckWettbewerbsjahrCommand;
import de.egladil.mkv.persistence.renderer.PunkteRenderer;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.service.TeilnehmerService;

/**
 * TeilnehmerServiceImpl
 */
@Singleton
public class TeilnehmerServiceImpl implements TeilnehmerService {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnehmerServiceImpl.class);

	private final FindAndCheckAssociatedEntitiesCommand findAndCheckAssociatedEntitiesCommand;

	private final ITeilnehmerDao teilnehmerDao;

	private final ILoesungszettelDao loesungszettelDao;

	private final IProtokollService protokollService;

	private final IAuswertungsgruppeDao auswertungsgruppeDao;

	private final NummerGenerator nummerGenerator;

	private final WettbewerbsloesungenProvider wettbewerbsloesungProvider;

	private final AuthorizationService authorizationService;

	/**
	 * TeilnehmerServiceImpl
	 */
	@Inject
	public TeilnehmerServiceImpl(final ITeilnehmerDao teilnehmerDao, final IAuswertungsgruppeDao auswertungsgruppeDao,
		final IPrivatteilnahmeDao privatteilnahmeDao, final ISchulteilnahmeDao schulteilnahmeDao,
		final ILoesungszettelDao loesungszettelDao, final IWettbewerbsloesungDao wettbewerbsloesungDao,
		final IProtokollService protokollService, final AuthorizationService authorizationService) {
		this.teilnehmerDao = teilnehmerDao;
		this.loesungszettelDao = loesungszettelDao;
		this.wettbewerbsloesungProvider = new WettbewerbsloesungenProvider(wettbewerbsloesungDao);
		this.findAndCheckAssociatedEntitiesCommand = new FindAndCheckAssociatedEntitiesCommand(privatteilnahmeDao,
			schulteilnahmeDao, auswertungsgruppeDao, teilnehmerDao);
		this.nummerGenerator = new NummerGenerator(teilnehmerDao);
		this.protokollService = protokollService;
		this.auswertungsgruppeDao = auswertungsgruppeDao;
		this.authorizationService = authorizationService;
	}

	/**
	 * Erzeugt einen neuen Teilnehmer mit oder ohne Lösungszettel.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param teilnehmerPayload TeilnehmerPayload
	 * @return Teilnehmer
	 */
	@Override
	public Teilnehmer createTeilnehmer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier,
		final PublicTeilnehmer teilnehmerPayload) {

		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}

		checkPreconditions(teilnahmeIdentifier, teilnehmerPayload);

		checkDuplicates(teilnahmeIdentifier, teilnehmerPayload,
			getAllTeilnehmerZuTeilnahme(teilnahmeIdentifier, teilnehmerPayload));

		final Teilnehmer teilnehmer = initTeilnehmer(teilnahmeIdentifier, teilnehmerPayload);
		final int nummer = nummerGenerator.nextNummer(teilnehmer, teilnehmer.getKlassenstufe());
		teilnehmer.setNummer(nummer);

		final Loesungszettel loesungszettel = findAndUpdateOrCreateLoesungszettel(teilnehmer, teilnehmerPayload);

		if (loesungszettel != null) {
			loesungszettelDao.persist(loesungszettel);
			teilnehmer.setLoesungszettelkuerzel(loesungszettel.getKuerzel());
		} else {
			if (teilnehmer.getEingabemodus() != null) {
				throw new MKVException("Fehler im Ablauf: kein Lösungszettel, aber eingabemodus = " + teilnehmer.getEingabemodus()
					+ " [kuerzel=" + teilnehmer.getKuerzel() + "]");
			}
			teilnehmer.setLoesungszettelkuerzel(null);
		}

		if (teilnehmerPayload.getAuswertungsgruppe() != null) {
			final Optional<Auswertungsgruppe> optAuswertungsgruppe = auswertungsgruppeDao.findByUniqueKey(Auswertungsgruppe.class,
				MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmerPayload.getAuswertungsgruppe().getKuerzel());
			if (optAuswertungsgruppe.isPresent()) {
				optAuswertungsgruppe.get().addTeilnehmer(teilnehmer);
			}
		}

		teilnehmer.setGeaendertDurch(benutzerUuid);

		Teilnehmer persisted = teilnehmerDao.persist(teilnehmer);
		persisted = initTransients(persisted, loesungszettel);

		protokollService.protokollieren(Ereignisart.TEILNEHMER_GEAENDERT.getKuerzel(), benutzerUuid,
			"create " + teilnehmer.getKuerzel());

		return persisted;
	}

	/**
	 * Erzeugt einen neuen Teilnehmer mit oder ohne Lösungszettel.
	 *
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param teilnehmerPayload TeilnehmerPayload
	 * @return Teilnehmer
	 */
	@Override
	public Teilnehmer updateTeilnehmer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier,
		final PublicTeilnehmer teilnehmerPayload)
		throws IllegalArgumentException, PreconditionFailedException, EgladilDuplicateEntryException, EgladilStorageException {

		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}

		checkPreconditions(teilnahmeIdentifier, teilnehmerPayload);

		final List<Teilnehmer> alleZurTeilnahme = teilnehmerDao.findByTeilnahmeIdentifier(teilnahmeIdentifier);
		checkDuplicates(teilnahmeIdentifier, teilnehmerPayload, alleZurTeilnahme);

		Teilnehmer teilnehmer = findTeilnehmer(alleZurTeilnahme, teilnehmerPayload);

		Antworteingabemodus eingabemodus = null;
		if (teilnehmerPayload.getEingabemodus() != null) {
			eingabemodus = Antworteingabemodus.valueOf(teilnehmerPayload.getEingabemodus());
		}

		teilnehmer = new TeilnehmerAenderer(teilnehmer).nachname(teilnehmerPayload.getNachname())
			.vorname(teilnehmerPayload.getVorname()).zusatz(teilnehmerPayload.getZusatz()).eingabemodus(eingabemodus)
			.sprache(teilnehmerPayload.getSprache().convert()).checkAndChange();

		if (loesungszettelGeloescht(teilnehmerPayload, teilnehmer)) {
			final Optional<Loesungszettel> optLoes = loesungszettelDao.findByUniqueKey(Loesungszettel.class,
				MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmer.getLoesungszettelkuerzel());
			if (optLoes.isPresent()) {
				loesungszettelDao.delete(optLoes.get());
			}
			teilnehmer.setLoesungszettelkuerzel(null);
		}

		final Loesungszettel loesungszettel = findAndUpdateOrCreateLoesungszettel(teilnehmer, teilnehmerPayload);
		if (loesungszettel != null) {
			loesungszettelDao.persist(loesungszettel);
			teilnehmer.setLoesungszettelkuerzel(loesungszettel.getKuerzel());
		} else {
			if (teilnehmer.getEingabemodus() != null) {
				throw new MKVException("Fehler im Ablauf: kein Lösungszettel, aber eingabemodus = " + teilnehmer.getEingabemodus()
					+ " [kuerzel=" + teilnehmer.getKuerzel() + "]");
			}
			teilnehmer.setLoesungszettelkuerzel(null);
		}

		teilnehmer.setGeaendertDurch(benutzerUuid);

		Teilnehmer persisted = teilnehmerDao.persist(teilnehmer);

		protokollService.protokollieren(Ereignisart.TEILNEHMER_GEAENDERT.getKuerzel(), benutzerUuid,
			"update " + teilnehmer.getKuerzel());

		persisted = initTransients(persisted, loesungszettel);

		return persisted;
	}

	private boolean loesungszettelGeloescht(final PublicTeilnehmer teilnehmerPayload, final Teilnehmer teilnehmer) {
		return (teilnehmerPayload.getLoesungszettelKuerzel() == null || teilnehmerPayload.getAntworten().length == 0)
			&& teilnehmer.getLoesungszettelkuerzel() != null;
	}

	private Teilnehmer initTeilnehmer(final TeilnahmeIdentifier teilnahmeIdentifier, final PublicTeilnehmer teilnehmerPayload) {
		final CreateTeilnehmerCommand createTeilnehmerCommand = new CreateTeilnehmerCommand(teilnahmeIdentifier, teilnehmerPayload);
		createTeilnehmerCommand.execute();
		final Teilnehmer teilnehmer = createTeilnehmerCommand.getTeilnehmer();
		return teilnehmer;
	}

	private Teilnehmer initTransients(final Teilnehmer teilnehmer, final Loesungszettel loesungszettel) {
		if (loesungszettel != null) {
			teilnehmer.setAntwortcode(loesungszettel.getLoesungszettelRohdaten().getAntwortcode());
			teilnehmer.setKaengurusprung(loesungszettel.getKaengurusprung());
			teilnehmer.setPunkte(new PunkteRenderer(loesungszettel.getPunkte()).render());
			teilnehmer.setLoesungszettelkuerzel(loesungszettel.getKuerzel());
		}
		return teilnehmer;
	}

	private Loesungszettel findAndUpdateOrCreateLoesungszettel(final Teilnehmer teilnehmer,
		final PublicTeilnehmer teilnehmerPayload) {
		final PublicAntwortbuchstabe[] antworten = teilnehmerPayload.getAntworten();
		if (antworten == null || antworten.length == 0) {
			return null;
		}
		final LoesungszettelRohdaten rohdaten = createLoesungszettelRohdaten(teilnehmerPayload, teilnehmer);
		if (rohdaten == null) {
			return null;
		}
		if (teilnehmer.getId() == null || StringUtils.isBlank(teilnehmer.getLoesungszettelkuerzel())) {
			final Loesungszettel loesungszettel = new Loesungszettel.Builder(teilnehmer.getJahr(), teilnehmer.getTeilnahmeart(),
				teilnehmer.getTeilnahmekuerzel(), teilnehmer.getNummer(), rohdaten).withSprache(teilnehmer.getSprache()).build();
			return loesungszettel;
		}
		final Optional<Loesungszettel> opt = loesungszettelDao.findByUniqueKey(Loesungszettel.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmer.getLoesungszettelkuerzel());
		if (!opt.isPresent()) {
			throw new MKVException("Lösungszettel mit KUERZEL=" + teilnehmer.getLoesungszettelkuerzel() + " nicht gefunden");
		}
		final Loesungszettel result = opt.get();
		result.setSprache(teilnehmer.getSprache());
		result.setLoesungszettelRohdaten(rohdaten);


		return result;
	}

	private Teilnehmer findTeilnehmer(final List<Teilnehmer> alleZurTeilnahme, final PublicTeilnehmer teilnehmerPayload) {
		for (final Teilnehmer t : alleZurTeilnahme) {
			if (t.getKuerzel().equals(teilnehmerPayload.getKuerzel())) {
				return t;
			}
		}
		throw new ResourceNotFoundException("Teilnehmer mit TeilnehmerPayload " + teilnehmerPayload.toString() + " nicht gefunden");
	}

	private void checkPreconditions(final TeilnahmeIdentifier teilnahmeIdentifier, final PublicTeilnehmer teilnehmerPayload) {

		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		if (teilnehmerPayload == null) {
			throw new IllegalArgumentException("teilnehmerPayload null");
		}
		new CheckWettbewerbsjahrCommand(teilnahmeIdentifier.getJahr()).execute();
		findAndCheckAssociatedEntitiesCommand.withTeilnahmeIdentifier(teilnahmeIdentifier).execute();
	}

	private List<Teilnehmer> getAllTeilnehmerZuTeilnahme(final TeilnahmeIdentifier teilnahmeIdentifier,
		final PublicTeilnehmer teilnehmerPayload) {
		return teilnehmerDao.findByTeilnahmeIdentifier(teilnahmeIdentifier);
	}

	private void checkDuplicates(final TeilnahmeIdentifier teilnahmeIdentifier, final PublicTeilnehmer teilnehmerPayload,
		final List<Teilnehmer> alleZurTeilnahme) {
		new CheckTeilnehmerDuplicateCommand(teilnahmeIdentifier, teilnehmerPayload, alleZurTeilnahme).execute();
	}

	/**
	 * @param teilnehmerPayload
	 * @param teilnehmer
	 * @return
	 */
	private LoesungszettelRohdaten createLoesungszettelRohdaten(final PublicTeilnehmer teilnehmerPayload,
		final Teilnehmer teilnehmer) {
		if (teilnehmerPayload.getAntworten() == null) {
			return null;
		}

		final Wettbewerbsloesung wettbewerbsloesung = wettbewerbsloesungProvider
			.getWettbewerbsloesung(teilnehmer.getKlassenstufe());
		final CreateLoesungszettelRohdatenCommand createLoesungszettelRohdatenCommand = new CreateLoesungszettelRohdatenCommand(
			teilnehmerPayload, wettbewerbsloesung);
		createLoesungszettelRohdatenCommand.execute();
		final LoesungszettelRohdaten rohdaten = createLoesungszettelRohdatenCommand.getLoesungszettelRohdaten();
		return rohdaten;
	}

	@Override
	public Optional<Teilnehmer> deleteTeilnehmer(final String benutzerUuid, final String kuerzel) {
		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}
		if (StringUtils.isBlank(kuerzel)) {
			throw new IllegalArgumentException("kuerzel blank");
		}
		final Optional<Teilnehmer> opt = teilnehmerDao.findByUniqueKey(Teilnehmer.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			kuerzel);
		if (opt.isPresent()) {
			final Teilnehmer teilnehmer = opt.get();

			authorizationService.authorizeForTeilnahme(benutzerUuid, teilnehmer.provideTeilnahmeIdentifier());

			teilnehmerDao.delete(teilnehmer);

			final String bemerkung = teilnehmer.provideTeilnahmeIdentifier().toString() + ", loesungszettelkuerzel="
				+ teilnehmer.getLoesungszettelkuerzel();
			protokollService.protokollieren(Ereignisart.TEILNEHMER_GELOESCHT_EINZELN.getKuerzel(), benutzerUuid, bemerkung);
		} else {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Löschversuch mit unbekanntem Teilnehmerkuerzel " + kuerzel);
		}
		return opt;
	}

	@Override
	public void deleteAllTeilnehmer(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier,
		final List<Teilnehmer> teilnehmer) {
		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid blank");
		}
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}
		if (teilnehmer == null) {
			throw new IllegalArgumentException("teilnehmer null");
		}

		if (!teilnehmer.isEmpty()) {
			teilnehmerDao.delete(teilnehmer);

			protokollService.protokollieren(Ereignisart.TEILNEHMER_GELOESCHT_MEHRERE.getKuerzel(), benutzerUuid,
				teilnehmer.size() + " Teilnehmer mit " + teilnahmeIdentifier.toString());
		}
	}
}
