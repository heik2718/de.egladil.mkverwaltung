//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.user;

import de.egladil.common.persistence.IDomainObject;

/**
 * IMailempfaenger ist jemand mit einer Mailadresse
 */
public interface IMailempfaenger extends IDomainObject {

	/**
	 * Gibt die Mailadresse zurück.
	 *
	 * @return
	 */
	String getEmail();
}
