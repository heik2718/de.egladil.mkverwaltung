//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.dao.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * UploadDaoImpl
 */
@Singleton
@MKVPersistenceUnit
public class UploadDaoImpl extends BaseDaoImpl<Upload> implements IUploadDao {

	private static final Logger LOG = LoggerFactory.getLogger(UploadDaoImpl.class);

	@Inject
	public UploadDaoImpl(Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public Optional<Upload> findUpload(TeilnahmeIdentifier teilnahmeIdentifier, String checksum) {
		if (teilnahmeIdentifier == null) {
			throw new MKVException("teilnahmeschluessel darf nicht null sein");
		}
		if (checksum == null) {
			throw new MKVException("checksum darf nicht null sein");
		}
		String stmt = "SELECT u from Upload u where u.teilnahmeart = :teilnahmeart and u.kuerzel = :kuerzel and u.jahr = :jahr and u.checksumme = :checksumme";
		TypedQuery<Upload> query = getEntityManager().createQuery(stmt, Upload.class);
		query.setParameter("teilnahmeart", teilnahmeIdentifier.getTeilnahmeart());
		query.setParameter("kuerzel", teilnahmeIdentifier.getKuerzel());
		query.setParameter("jahr", teilnahmeIdentifier.getJahr());
		query.setParameter("checksumme", checksum);

		List<Upload> treffer = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", treffer.size());

		if (treffer.isEmpty()) {
			return Optional.empty();
		}
		if (treffer.size() > 1) {
			LOG.warn(
				GlobalConstants.LOG_PREFIX_IMPOSSIBLE
					+ "mehr als ein Eintrag in uploads zu TeilnahmeIdentifier und Checksumme. Gebe den ersten Treffer zurück. {}, {}",
				teilnahmeIdentifier, checksum);
		}

		return Optional.of(treffer.get(0));
	}
}
