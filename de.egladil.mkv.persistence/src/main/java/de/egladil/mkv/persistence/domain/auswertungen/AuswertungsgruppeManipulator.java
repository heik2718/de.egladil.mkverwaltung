//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.auswertungen;

import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.NormalizeWortgruppeInvoker;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * AuswertungsgruppeManipulator
 */
public abstract class AuswertungsgruppeManipulator {

	private String neuerName;

	private final NormalizeWortgruppeInvoker normalizeInvoker;

	/**
	 * AuswertungsgruppeManipulator
	 */
	public AuswertungsgruppeManipulator() {
		normalizeInvoker = new NormalizeWortgruppeInvoker();
	}

	protected void normalizeAndSetName(final String neuerName) {
		if (neuerName != null) {
			this.neuerName = normalizeInvoker.normalize(neuerName);
		}
	}

	protected void pruefeNameValid() {
		if (neuerName != null) {
			new LengthTester().checkTooLongAndThrow(neuerName, UrkundeConstants.SIZE_TEXT_NORMAL);
		}
	}

	final String getNeuerName() {
		return neuerName;
	}
}
