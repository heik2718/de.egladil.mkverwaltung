//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;

/**
 * BerechnePunkteCommandValidationDecorator
 */
public class BerechnePunkteCommandValidationDecorator implements IBerechnePunkteCommand {

	private final IBerechnePunkteCommand command;

	private final int laengeWertungscode;

	/**
	 * @param command IBerechnePunkteCommand darf nicht null sein
	 * @throws NullPointerException wenn command null
	 */
	public BerechnePunkteCommandValidationDecorator(final IBerechnePunkteCommand command, final int laengeWertungscode) {
		if (command == null) {
			throw new NullPointerException("Parameter command");
		}
		this.command = command;
		this.laengeWertungscode = laengeWertungscode;
	}

	/**
	 * @see de.egladil.mkv.persistence.berechnungen.IBerechnePunkteCommand#berechne(java.lang.String)
	 */
	@Override
	public int berechne(final String wertungscode) {
		if (wertungscode == null) {
			throw new NullPointerException("wertungscode null");
		}
		if (wertungscode.length() != laengeWertungscode) {
			throw new IndexOutOfBoundsException("wertungscode muss die Länge " + laengeWertungscode + " haben.");
		}
		final char[] chars = wertungscode.toCharArray();
		for (int index = 0; index < chars.length; index++) {
			final char c = chars[index];
			if (c != 'f' && c != 'r' && c != 'n') {
				throw new MKAuswertungenException("unerlaubtes Zeichen " + c + " im Wertungscode: index=" + index);
			}
		}
		return command.berechne(wertungscode);
	}
}
