//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.domain.teilnahmen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.common.persistence.IDomainObject;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;

/**
 * SchulteilnahmeReadOnly
 */
@Entity
@Table(name = "vw_schulteilnahmen")
public class SchulteilnahmeReadOnly implements IDomainObject, TeilnahmeIdentifierProvider {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@JsonIgnore
	private Long id;

	@Column
	@JsonProperty
	private String landkuerzel;

	@Column
	@JsonProperty
	private String land;

	@Column
	@JsonProperty
	private String ortkuerzel;

	@Column
	@JsonProperty
	private String ort;

	@Column
	@JsonProperty
	private String schulkuerzel;

	@Column
	@JsonProperty
	private String schule;

	@Column
	@JsonProperty
	private String jahr;

	@Override
	public Long getId() {
		return id;
	}

	public String getSchule() {
		return schule;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setLandkuerzel(final String landkuerzel) {
		this.landkuerzel = landkuerzel;
	}

	public void setLand(final String land) {
		this.land = land;
	}

	public void setOrtkuerzel(final String ortkuerzel) {
		this.ortkuerzel = ortkuerzel;
	}

	public void setOrt(final String ort) {
		this.ort = ort;
	}

	public void setSchulkuerzel(final String schulkuerzel) {
		this.schulkuerzel = schulkuerzel;
	}

	public void setSchule(final String schule) {
		this.schule = schule;
	}

	public void setJahr(final String jahr) {
		this.jahr = jahr;
	}

	public String getSchulkuerzel() {
		return schulkuerzel;
	}

	public String getLandkuerzel() {
		return landkuerzel;
	}

	public String getLand() {
		return land;
	}

	public String getOrtkuerzel() {
		return ortkuerzel;
	}

	public String getOrt() {
		return ort;
	}

	public String getJahr() {
		return jahr;
	}

	@Override
	public TeilnahmeIdentifier provideTeilnahmeIdentifier() {
		return TeilnahmeIdentifier.create(Teilnahmeart.S, schulkuerzel, jahr);
	}

}
