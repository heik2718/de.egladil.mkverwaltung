//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.kataloge.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.NotImplementedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.LandKuerzel;
import de.egladil.mkv.persistence.cache.LandCache;
import de.egladil.mkv.persistence.cache.OrtCache;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;
import de.egladil.mkv.persistence.payload.response.OrtLage;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;
import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * KatalogDelegate
 */
@Singleton
public class KatalogService implements IKatalogService {

	private static final Logger LOG = LoggerFactory.getLogger(KatalogService.class);

	private final ILandDao landDao;

	private final LandCache landCache;

	private final IOrtDao ortDao;

	private final OrtCache ortCache;

	private final ISchuleDao schuleDao;

	private final IBenutzerService benutzerService;

	private final KuerzelGenerator kuerzelGenerator;

	/**
	 * Erzeugt eine Instanz von KatalogDelegate
	 */
	@Inject
	public KatalogService(final ILandDao landDao, final LandCache landCache, final IOrtDao ortDao, final OrtCache ortCache,
		final ISchuleDao schuleDao, final IBenutzerService benutzerService) {
		this.landDao = landDao;
		this.landCache = landCache;
		this.ortDao = ortDao;
		this.ortCache = ortCache;
		this.schuleDao = schuleDao;
		this.benutzerService = benutzerService;
		this.kuerzelGenerator = new KuerzelGenerator();
	}

	@Override
	public List<PublicLand> getPublicLaender() throws MKVException {
		try {
			final List<Land> laender = landDao.findAllFreigeschaltet();
			final List<PublicLand> entity = new ArrayList<>();
			for (final Land land : laender) {
				entity.add(new PublicLand(land.getName(), land.getKuerzel()));
			}
			return entity;
		} catch (final PersistenceException e) {
			LOG.error("Fehler beim Landen des Länderkatalogs: " + e.getMessage(), e);
			throw new MKVException("Fehler beim Landen des Länderkatalogs");
		}
	}

	@Override
	public Optional<Land> getLand(@NotBlank @LandKuerzel @Size(min = 1, max = 5) final String landkuerzel)
		throws MKVException, ConstraintViolationException {
		return landCache.getLand(landkuerzel);
	}

	@Override
	public Optional<Land> getLand(final Long landId) {
		return landDao.findById(Land.class, landId);
	}

	@Override
	public Optional<Ort> getOrt(@NotBlank @LandKuerzel @Size(min = 1, max = 5) final String landkuerzel,
		@NotBlank @Kuerzel @Size(min = 8, max = 8) final String ortkuerzel) throws MKVException, ConstraintViolationException {
		final Optional<Land> land = landCache.getLand(landkuerzel);
		if (!land.isPresent()) {
			return Optional.empty();
		}
		// throw new EgladilRuntimeException("wollen ExceptionMapper und Logging testen");
		final Optional<Ort> optOrt = ortCache.getOrt(ortkuerzel);
		if (optOrt.isPresent()) {
			final Ort ort = optOrt.get();
			ort.setLand(land.get());
			return Optional.of(ort);
		}
		return optOrt;
	}

	@Override
	public OrtLage getOrtLage(final String ortKuerzel) {

		if (ortKuerzel == null) {
			throw new IllegalArgumentException("ortKuerzel null");
		}

		return ortDao.getOrtLage(ortKuerzel);
	}

	@Override
	public SchuleLage getSchuleLage(final String schuleKuerzel) {

		if (schuleKuerzel == null) {
			throw new IllegalArgumentException("schuleKuerzel null");
		}
		return schuleDao.getSchuleLage(schuleKuerzel);
	}

	@Override
	public Optional<PublicSchule> getSchule(final String schulkuerzel) {
		final Optional<Schule> optSchule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			schulkuerzel);
		if (optSchule.isPresent()) {
			final Schule schule = optSchule.get();

			final SchuleLage lage = schuleDao.getSchuleLage(schulkuerzel);

			final PublicSchule ps = new PublicSchule(schule.getName(), schule.getKuerzel());
			ps.setLage(lage);
			return Optional.of(ps);
		}
		return Optional.empty();
	}

	@Override
	public List<Ort> findOrte(final String namensteil, final String benutzerUuid)
		throws IllegalArgumentException, EgladilStorageException {
		authorize(benutzerUuid);
		throw new NotImplementedException("findOrte");
	}

	@Override
	public List<Ort> findOrte(final String landkuerzel, final String namensteil, final String benutzerUuid)
		throws IllegalArgumentException, EgladilStorageException {
		authorize(benutzerUuid);
		return ortDao.findOrteInLandUnscharf(landkuerzel, namensteil);
	}

	@Override
	public List<Schule> findSchulen(final String namensteil, final String benutzerUuid)
		throws IllegalArgumentException, EgladilStorageException {
		authorize(benutzerUuid);
		throw new NotImplementedException("findSchulen");
	}

	@Override
	public List<Schule> findSchulen(final String ortkuerzel, final String namensteil, final String benutzerUuid)
		throws IllegalArgumentException, EgladilStorageException {
		authorize(benutzerUuid);
		return schuleDao.findSchulenInOrtUnscharf(ortkuerzel, namensteil);
	}

	@Override
	public Land landAnlegen(final String landKuerzel, final String name, final String benutzerUuid, final boolean freischalten)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException {

		if (landKuerzel == null) {
			throw new IllegalArgumentException("landKuerzel null");
		}
		if (name == null) {
			throw new IllegalArgumentException("name null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}

		authorize(benutzerUuid);
		final Optional<Land> optLand = landCache.getLand(landKuerzel);
		if (optLand.isPresent()) {
			throw new EgladilDuplicateEntryException("Ein Land mit diesem Kürzel gibt es schon");
		}
		final Land land = new Land();
		land.setKuerzel(landKuerzel);
		land.setName(name);
		land.setFreigeschaltet(freischalten);
		try {
			return landDao.persist(land);
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optEx = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optEx.isPresent()) {
				throw optEx.get();
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	/**
	 * @see de.egladil.mkv.persistence.kataloge.IKatalogService#landNameAendern(java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public Land landNameAendern(final String landkuerzel, final String name, final String benutzerUuid)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException,
		NotImplementedException {
		if (landkuerzel == null) {
			throw new IllegalArgumentException("landkuerzel null");
		}
		if (name == null) {
			throw new IllegalArgumentException("name null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benuzerrUuid null");
		}
		authorize(benutzerUuid);
		throw new NotImplementedException("landNameAendern");
	}

	@Override
	public Ort ortAnlegen(final String landkuerzel, final String ortname, final String benutzerUuid)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException {
		if (landkuerzel == null) {
			throw new IllegalArgumentException("landkuerzel null");
		}
		if (ortname == null) {
			throw new IllegalArgumentException("ortname null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benuzerrUuid null");
		}
		authorize(benutzerUuid);
		final Optional<Land> optLand = landCache.getLand(landkuerzel);
		if (!optLand.isPresent()) {
			throw new ResourceNotFoundException("Kein Land mit [kuerzel=" + landkuerzel + "] gefunden");
		}
		final Land land = optLand.get();
		final String kuerzel = kuerzelGenerator.generateDefaultKuerzel();
		final Ort ort = new Ort();
		ort.setKuerzel(kuerzel);
		ort.setName(ortname);
		land.addOrt(ort);

		try {
			landDao.persist(land);
			return ort;
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optEx = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optEx.isPresent()) {
				throw optEx.get();
			}
			throw new EgladilStorageException("Fehler in einer Transaktion: " + e.getMessage(), e);
		}
	}

	@Override
	public Ort ortNameAendern(final String ortkuerzel, final String ortname, final String benutzerUuid)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException,
		NotImplementedException {
		if (ortkuerzel == null) {
			throw new IllegalArgumentException("ortkuerzel null");
		}
		if (ortname == null) {
			throw new IllegalArgumentException("ortname null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benuzerrUuid null");
		}
		authorize(benutzerUuid);
		throw new NotImplementedException("ortNameAendern");
	}

	@Override
	public Schule schuleAnlegen(final NeueSchulePayload payload, final String benutzerUuid)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException {
		if (payload == null) {
			throw new IllegalArgumentException("payload null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benuzerrUuid null");
		}
		pruefeNameUrkundenkompatibel(payload.getName());

		authorize(benutzerUuid);
		final Optional<Land> optLand = landCache.getLand(payload.getLandkuerzel());
		if (!optLand.isPresent()) {
			throw new ResourceNotFoundException("landkuerzel=" + payload.getLandkuerzel());
		}
		final Land land = optLand.get();
		final Optional<Ort> optOrt = land.findOrt(payload.getOrtkuerzel());
		if (!optOrt.isPresent()) {
			throw new ResourceNotFoundException("ortkuerzel=" + payload.getOrtkuerzel());
		}
		final Ort ort = optOrt.get();
		final String kuerzel = kuerzelGenerator.generateDefaultKuerzel();
		final Schule schule = new Schule();
		schule.setKuerzel(kuerzel);
		schule.setName(payload.getName());
		schule.setUrl(payload.getUrl());
		schule.setStrasse(payload.getStrasse());

		ort.addSchule(schule);

		try {
			ortDao.persist(ort);
			return schule;
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optEx = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optEx.isPresent()) {
				throw optEx.get();
			}
			throw new EgladilStorageException("Fehler in einer Transaktion: " + e.getMessage(), e);
		}
	}

	/**
	 * @see de.egladil.mkv.persistence.kataloge.IKatalogService#schuleNameAendern(de.egladil.mkv.persistence.domain.kataloge.Ort,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public Schule schuleNameAendern(final String schulkuerzel, final String schulename, final String benutzerUuid)
		throws IllegalArgumentException, EgladilAuthorizationException, EgladilDuplicateEntryException, EgladilStorageException,
		NotImplementedException {
		if (schulkuerzel == null) {
			throw new IllegalArgumentException("schulkuerzel null");
		}
		if (schulename == null) {
			throw new IllegalArgumentException("schulename null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benuzerrUuid null");
		}
		pruefeNameUrkundenkompatibel(schulename);
		authorize(benutzerUuid);
		throw new NotImplementedException("schuleNameAendern");
	}

	private void pruefeNameUrkundenkompatibel(final String neuerName) throws IllegalArgumentException {
		if (neuerName != null) {
			new LengthTester().checkTooLongAndThrow(neuerName, UrkundeConstants.SIZE_TEXT_NORMAL);
		}
	}

	/**
	 * Autorisiert die aktion oder auch nicht :).
	 *
	 * @param benutzerUuid
	 */
	void authorize(final String benutzerUuid) {
		if (!benutzerService.isBenutzerInRole(Role.MKV_ADMIN, benutzerUuid)) {
			throw new EgladilAuthorizationException("Keine Berechtigung.");
		}
	}

	@Override
	public List<Schule> getAll() {
		return schuleDao.findAll(Schule.class);
	}
}
