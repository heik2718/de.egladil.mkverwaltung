//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.persistence.berechnungen;

/**
 * IWertungRechnerStrategie ändert eine Summe anhand einer Aufgabenkategorie.
 */
public interface IWertungRechnerStrategie {

	/**
	 * Die Strategie wird auf die gegebene Summe anewendet.
	 *
	 * @param summe
	 * @return
	 */
	int applyToSumme(int summe);
}
