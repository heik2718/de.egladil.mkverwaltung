//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * PunktintervallTest
 */
public class PunktintervallTest {

	@Test
	public void compare_gleich_wenn_minval_gleich_gleiche_maxvals() {
		// Arrange + Act + Assert
		assertTrue(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build()
			.compareTo(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build()) == 0);
	}

	@Test
	public void compare_kleiner_wenn_minval_kleiner_gleiche_maxvals() {
		// Arrange + Act + Assert
		assertTrue(new Punktintervall.Builder(900).laenge(Punktintervall.DEFAULT_LAENGE).build()
			.compareTo(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build()) < 0);
	}

	@Test
	public void compare_groesser_wenn_minval_kleiner_gleiche_maxvals() {
		// Arrange + Act + Assert
		assertTrue(new Punktintervall.Builder(1200).laenge(Punktintervall.DEFAULT_LAENGE).build()
			.compareTo(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build()) > 0);
	}

	@Test
	public void equal_wenn_gleiche_intervallgrenzen() {
		assertEquals(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build(),
			new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build());

		assertEquals(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build().hashCode(),
			new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build().hashCode());
	}

	@Test
	public void not_equal_wenn_verschiedene_intervallgrenzen() {
		assertFalse(new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build()
			.equals(new Punktintervall.Builder(5000).laenge(Punktintervall.DEFAULT_LAENGE).build()));
	}

	@Test
	public void conains() {
		// Arrange
		final Punktintervall intervall = new Punktintervall.Builder(1000).laenge(Punktintervall.DEFAULT_LAENGE).build();

		// Assert
		assertTrue(intervall.contains(1000));
		assertTrue(intervall.contains(1200));
		assertTrue(intervall.contains(1475));
		assertFalse(intervall.contains(999));
		assertFalse(intervall.contains(1500));
		assertFalse(intervall.contains(1476));
	}
}
