//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;

/**
 * AuswertungStreamReaderFactoryTest
 */
public class AuswertungStreamReaderFactoryTest {

	private static final int MAX_EXTRACTED_BYTES = 2097152;

	private static final String OPEN_OFFICE = "/korrekt/klasse_2_eine_namenspalte.ods";

	private static final String XLSX = "/korrekt/klasse_2_eine_namenspalte.xlsx";

	private static final String XLS = "/korrekt/klasse_2_eine_namenspalte.xls";

	private static final String ADMIN_UPLOAD = "/korrekt/adminUpload.xlsx";

	private static final String COPIED_BYTES = "/korrekt/copiedBytes.zip";

	@Test
	public void create_wirft_IllegalArgumentException_wenn_parameter_null() throws IOException {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			AuswertungStreamReaderFactory.create(null);
		});
		assertEquals("uploadMimeType null", ex.getMessage());
	}

	@Test
	public void create_klappt_fuer_openOffice() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(OPEN_OFFICE)) {
			// Act
			final IAuswertungStreamReader streamReader = AuswertungStreamReaderFactory.create(UploadMimeType.ODS);

			// Assert
			assertTrue(streamReader instanceof AuswertungOpenOfficeStreamReader);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);

		}
	}

	@Test
	public void create_klappt_fuer_xls() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(XLS)) {
			// Act
			final IAuswertungStreamReader streamReader = AuswertungStreamReaderFactory.create(UploadMimeType.VDN_MSEXCEL);

			// Assert
			assertTrue(streamReader instanceof AbstractAuswertungExcelStreamReader);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
		}
	}

	@Test
	public void create_klappt_fuer_xlsx() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(XLSX)) {
			// Act
			final IAuswertungStreamReader streamReader = AuswertungStreamReaderFactory.create(UploadMimeType.VDN_OPEN_XML);

			// Assert
			assertTrue(streamReader instanceof AbstractAuswertungExcelStreamReader);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
		}
	}

	@Test
	public void create_klappt_fuer_admin_upload() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(ADMIN_UPLOAD)) {
			// Act
			final IAuswertungStreamReader streamReader = AuswertungStreamReaderFactory.create(UploadMimeType.VDN_OPEN_XML);

			// Assert
			assertTrue(streamReader instanceof AbstractAuswertungExcelStreamReader);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
			assertFalse(workbook.isEmpty());
		}
	}

	@Test
	public void create_klappt_fuer_copied_bytes() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(COPIED_BYTES)) {
			// Act
			final IAuswertungStreamReader streamReader = AuswertungStreamReaderFactory.create(UploadMimeType.VDN_OPEN_XML);

			// Assert
			assertTrue(streamReader instanceof AbstractAuswertungExcelStreamReader);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
			assertFalse(workbook.isEmpty());
		}
	}
}
