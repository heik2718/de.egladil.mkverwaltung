//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.statistik.gruppen.Rootgruppeuebersicht;
import de.egladil.mkv.auswertungen.statistik.gruppen.RootgruppeuebersichtCreator;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.persistence.config.KontextReader;

/**
 * SchuluebersichtAktuellesJahrPDFGeneratorTest
 */
public class SchuluebersichtAktuellesJahrPDFGeneratorTest {

	private static final String SCHULNAME = "Blümchenschule";

	private Rootgruppeuebersicht rootgruppeuebersicht;

	@BeforeEach
	void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		final List<TeilnehmerUndPunkte> rohdaten = TestHelper.getSchulrohdaten();
		rootgruppeuebersicht = new RootgruppeuebersichtCreator().createSchuluebersicht(rohdaten, SCHULNAME);
	}

	@Test
	void generiereAlles() throws Exception {

		// Arrange
		final String path = "/home/heike/temp/schuluebersicht1.pdf";
		final SchuluebersichtAktuellesJahrPDFGenerator generator = new SchuluebersichtAktuellesJahrPDFGenerator();

		// Act
		final byte[] pdf = generator.generiere(rootgruppeuebersicht, "Albert-Einstein-Schule");

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(path)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(path).canRead());

	}

	@Test
	void generiereAllesNurEinKind() throws Exception {

		// Arrange
		final List<TeilnehmerUndPunkte> rohdaten = TestHelper.getSchulrohdaten();
		final List<TeilnehmerUndPunkte> einKind = Arrays.asList(new TeilnehmerUndPunkte[] { rohdaten.get(0) });
		rootgruppeuebersicht = new RootgruppeuebersichtCreator().createSchuluebersicht(einKind, SCHULNAME);

		final String path = "/home/heike/temp/schuluebersicht2.pdf";
		final SchuluebersichtAktuellesJahrPDFGenerator generator = new SchuluebersichtAktuellesJahrPDFGenerator();

		// Act
		final byte[] pdf = generator.generiere(rootgruppeuebersicht, "Albert-Einstein-Schule");

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(path)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(path).canRead());

	}
}
