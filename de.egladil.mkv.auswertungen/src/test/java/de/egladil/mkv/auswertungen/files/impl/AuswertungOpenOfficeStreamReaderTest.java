//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.domain.MKAuswertungCell;
import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.exceptions.UnprocessableAuswertungException;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;

/**
 * AuswertungOpenOfficeStreamReaderTest
 */
@Ignore
public class AuswertungOpenOfficeStreamReaderTest {

	private static final int MAX_EXTRACTED_BYTES = 2097152;

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungOpenOfficeStreamReader.class);

	@Test
	public void readAuswertung_xls_zwei_namen_klappt() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(TestHelper.ODS_ZWEI_NAMEN)) {
			LOG.debug("Resource: {}", TestHelper.ODS_ZWEI_NAMEN);
			// Arrange
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);

			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);

			System.out.println(workbook.prettyPrint());

			final List<MKAuswertungRow> rows = workbook.getRows();
			final List<String[]> expectedValuesZweiNamen = TestHelper.getKorrektZweiNamen();
			assertEquals(expectedValuesZweiNamen.size(), rows.size());

			for (int i = 0; i < rows.size(); i++) {
				final MKAuswertungCell[] zellen = rows.get(i).showCells();
				assertEquals("Fehler bei Zeile " + (i + 1), 14, zellen.length);

				final String[] expected = expectedValuesZweiNamen.get(i);
				final String expectedString = PrettyStringUtils.collectionToString(Arrays.asList(expected), ",");
				assertEquals("Fehler bei Zeile " + (i + 1), expectedString, rows.get(i).toString());
			}
		}
	}

	@Test
	public void readAuswertung_xlsx_ein_name_klappt() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(TestHelper.ODS_EIN_NAME)) {
			LOG.debug("Resource: {}", TestHelper.ODS_EIN_NAME);
			// Arrange
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);

			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
			System.out.println(workbook.prettyPrint());

			final List<MKAuswertungRow> rows = workbook.getRows();
			final List<String[]> expectedValuesEinName = TestHelper.getKorrektEinName();
			assertEquals(expectedValuesEinName.size(), rows.size());

			for (int i = 0; i < rows.size(); i++) {
				final MKAuswertungCell[] zellen = rows.get(i).showCells();
				assertEquals("Fehler bei Zeile " + (i + 1), 16, zellen.length);

				final String[] expected = expectedValuesEinName.get(i);
				final String expectedString = PrettyStringUtils.collectionToString(Arrays.asList(expected), ",");
				assertEquals("Fehler bei Zeile " + (i + 1), expectedString, rows.get(i).toString());
			}
		}
	}

	@Test
	public void readAuswertung_xlsx_kein_name_klappt() throws IOException {
		try (InputStream in = getClass().getResourceAsStream(TestHelper.ODS_KEIN_NAME)) {
			// Arrange
			LOG.debug("Resource: {}", TestHelper.ODS_KEIN_NAME);
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);

			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
			System.out.println(workbook.prettyPrint());

			final List<MKAuswertungRow> rows = workbook.getRows();
			final List<String[]> expectedValuesKeinName = TestHelper.getKorrektKeinName();
			assertEquals(expectedValuesKeinName.size(), rows.size());

			for (int i = 0; i < rows.size(); i++) {
				final MKAuswertungCell[] zellen = rows.get(i).showCells();
				assertEquals("Fehler bei Zeile " + (i + 1), 15, zellen.length);

				final String[] expected = expectedValuesKeinName.get(i);
				final String expectedString = PrettyStringUtils.collectionToString(Arrays.asList(expected), ",");
				assertEquals("Fehler bei Zeile " + (i + 1), expectedString, rows.get(i).toString());
			}
		}
	}

	@Test
	public void readAuswertung_error3() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/error/error3.ods")) {
			LOG.debug("Resource: /error/error3.ods");
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			assertNotNull(workbook);
			System.out.println(workbook.prettyPrint());
		}
	}

	@Test
	public void readAuswertung_ziffer_statt_buchstabe() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/error/ziffer_statt_buchstabe.ods")) {
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				{
					final MKAuswertungRow row = rows.get(0);
					assertEquals("r,r,r,r,f,r,r,r,f,f,f,r", row.toNutzereingabe());
				}
				{
					final MKAuswertungRow row = rows.get(1);
					assertEquals("f,f,f,4,f,r,f,r,f,f,n,f", row.toNutzereingabe());
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage());
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertung_minus_statt_buchstabe() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/error/minus_statt_buchstabe.ods")) {
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				{
					final MKAuswertungRow row = rows.get(0);
					assertEquals("r,r,r,r,f,r,r,r,f,f,f,r", row.toNutzereingabe());
				}
				{
					final MKAuswertungRow row = rows.get(1);
					assertEquals("f,f,f,-,f,r,f,r,f,f,n,f", row.toNutzereingabe());
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage());
				fail("UnprocessableAuswertungException");
			}
		}
	}
}
