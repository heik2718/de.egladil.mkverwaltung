//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * AnwenderdatenAuswertungenOperationTest
 */
public class AnwenderdatenAuswertungenOperationTest {

	private static final Logger LOG = LoggerFactory.getLogger(AnwenderdatenAuswertungenOperationTest.class);

	private static final int MAX_EXTRACTED_BYTES = 2097152;

	private static final String PATH_UPLOAD_DIR = "/home/heike/git/konfigurationen/mkverwaltung/upload_testdateien";

	private static final String DATEINAME = "S_3QD4RXO9_6ce9e947018b77d2faeeecb90376e8cd9746bed0b9bb28c4d98e9217ed5b8cbd.xlsx";

	private static final String NAME_LEERE_DATEI = "emptySheet.xlsx";

	private AnwenderdatenAuswertenOperation operation;

	private UploadInfo uploadInfo;

	@BeforeEach
	public void setUp() {
		uploadInfo = new UploadInfo();
		uploadInfo.setId(14l);
		uploadInfo.setMimetype(UploadMimeType.VDN_OPEN_XML);

		final Map<Klassenstufe, String> loesungscodes = new HashMap<>();
		loesungscodes.put(Klassenstufe.EINS, "CDADECCCBCDE");
		loesungscodes.put(Klassenstufe.ZWEI, "EACDDCDCCEBACBA");

		operation = new AnwenderdatenAuswertenOperation(loesungscodes, MAX_EXTRACTED_BYTES);
	}

	@Test
	public void verarbeiteDaten_happy_hour() throws IOException {
		// Arrange
		uploadInfo.setDateiname(DATEINAME);

		// Act
		final Optional<VerarbeitetesWorkbook> opt = operation.verarbeiteDaten(uploadInfo, PATH_UPLOAD_DIR);

		// Assert
		assertTrue(opt.isPresent());
		final VerarbeitetesWorkbook verarbeitetesWorkbook = opt.get();
		assertEquals(6, verarbeitetesWorkbook.getRohdaten().size());
		assertEquals(Klassenstufe.EINS, verarbeitetesWorkbook.getKlasse());
		LOG.debug(verarbeitetesWorkbook.toString());
	}

	@Test
	public void verarbeiteDaten_leere_datei() throws IOException {
		// Arrange
		uploadInfo.setDateiname(NAME_LEERE_DATEI);

		// Act
		final Optional<VerarbeitetesWorkbook> opt = operation.verarbeiteDaten(uploadInfo, PATH_UPLOAD_DIR);

		// Assert
		assertTrue(opt.isPresent());
		final VerarbeitetesWorkbook verarbeitetesWorkbook = opt.get();
		assertEquals(0, verarbeitetesWorkbook.getRohdaten().size());
	}

	@Test
	public void verarbeiteDaten_throws_nullPointerException() throws IOException {
		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			operation.verarbeiteDaten(null, PATH_UPLOAD_DIR);
		});
		assertEquals("uploadInfo", ex.getMessage());
	}

	@Test
	public void verarbeiteDaten_throws() throws IOException {
		// Arrange
		uploadInfo.setDateiname("nicht_vorhanden.xlsx");

		// Act + Assert
		final Throwable ex = assertThrows(FileNotFoundException.class, () -> {
			operation.verarbeiteDaten(uploadInfo, PATH_UPLOAD_DIR);
		});
		assertEquals("/home/heike/git/konfigurationen/mkverwaltung/upload_testdateien/nicht_vorhanden.xlsx (Datei oder Verzeichnis nicht gefunden)",
			ex.getMessage());
	}
}
