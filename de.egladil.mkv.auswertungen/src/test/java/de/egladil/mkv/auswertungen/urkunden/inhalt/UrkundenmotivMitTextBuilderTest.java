//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * UrkundenmotivMitTextBuilderTest
 */
public class UrkundenmotivMitTextBuilderTest {

	final String datum = "18.03.2017";

	@Test
	@DisplayName("erfolgreiche Erzeugung ohne schulname")
	void erzeugenMinimal() {
		final UrkundenmotivProvider provider = Mockito.mock(UrkundenmotivProvider.class);
		final String wettbewerbsjahr = "2018";

		final UrkundenmotivMitText urkundenmotiv = new UrkundenmotivMitTextBuilder(wettbewerbsjahr, provider, datum).build();

		assertEquals(wettbewerbsjahr, urkundenmotiv.getWettbewerbsjahr());
	}

	@Test
	@DisplayName("Konstruktor ohne Provider")
	void builderConstructorOhneProvider() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new UrkundenmotivMitTextBuilder("2018", null, datum);
		});
		assertEquals("der UrkundenmotivProvider ist erforderlich", ex.getMessage());

	}

	@Test
	@DisplayName("Konstruktor ohne Wettbewerbsjahr")
	void builderConstructorOhneWettbewerbsjahr() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new UrkundenmotivMitTextBuilder(null, Mockito.mock(UrkundenmotivProvider.class), datum);
		});
		assertEquals("Datum und Wettbewerbsjahr sind erforderlich", ex.getMessage());

	}

	@Test
	@DisplayName("Konstruktor Wettbewerbsjahr blank")
	void builderConstructorWettbewerbsjahrBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new UrkundenmotivMitTextBuilder(" ", Mockito.mock(UrkundenmotivProvider.class), datum);
		});
		assertEquals("Datum und Wettbewerbsjahr sind erforderlich", ex.getMessage());

	}

	@Test
	@DisplayName("Konstruktor ohne Wettbewerbsjahr")
	void builderConstructorOhneDatum() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new UrkundenmotivMitTextBuilder("2018", Mockito.mock(UrkundenmotivProvider.class), null);
		});
		assertEquals("Datum und Wettbewerbsjahr sind erforderlich", ex.getMessage());

	}

	@Test
	@DisplayName("Konstruktor Wettbewerbsjahr blank")
	void builderConstructorDatumBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new UrkundenmotivMitTextBuilder("2018", Mockito.mock(UrkundenmotivProvider.class), " ");
		});
		assertEquals("Datum und Wettbewerbsjahr sind erforderlich", ex.getMessage());

	}
}
