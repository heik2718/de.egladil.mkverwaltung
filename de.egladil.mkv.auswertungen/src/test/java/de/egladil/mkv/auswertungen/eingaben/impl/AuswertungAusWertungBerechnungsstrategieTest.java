//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.files.impl.AbstractAuswertungExcelStreamReader;
import de.egladil.mkv.auswertungen.files.impl.AuswertungStreamReaderFactory;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;

/**
 * AuswertungAusWertungBerechnungsstrategieTest
 */
public class AuswertungAusWertungBerechnungsstrategieTest {

	private static final int MAX_EXTRACTED_BYTES = 2097152;

	private final AuswertungAusWertungBerechnungsstrategie auswerter = new AuswertungAusWertungBerechnungsstrategie();

	@Test
	public void createLoesunszettel_klasse_2_kein_name_erzeut_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse2KeinName();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.ZWEI, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfnnrn", loesungszettel.getWertungscode());
		assertEquals(Integer.valueOf(2), loesungszettel.getKaengurusprung());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,n,n,r,n", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertFalse(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(3325), loesungszettel.getPunkte());
	}

	@Test
	public void createLoesunszettel_klasse_2_ein_name_erzeut_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse2EinName();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.ZWEI, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfnnrn", loesungszettel.getWertungscode());
		assertEquals(Integer.valueOf(2), loesungszettel.getKaengurusprung());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,n,n,r,n", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertFalse(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(3325), loesungszettel.getPunkte());
	}

	@Test
	public void createLoesunszettel_klasse_2_zwei_namen_erzeut_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse2ZweiNamen();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.ZWEI, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfnnrn", loesungszettel.getWertungscode());
		assertEquals(Integer.valueOf(2), loesungszettel.getKaengurusprung());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,n,n,r,n", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertFalse(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(3325), loesungszettel.getPunkte());
	}

	@Test
	public void createLoesunszettel_klasse_1_kein_name_erzeut_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1KeinName();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfn", loesungszettel.getWertungscode());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,n", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertFalse(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(2725), loesungszettel.getPunkte());
	}

	@Test
	public void createLoesunszettel_klasse_1_ein_name_erzeut_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfn", loesungszettel.getWertungscode());
		assertEquals(Integer.valueOf(2), loesungszettel.getKaengurusprung());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,n", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertFalse(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(2725), loesungszettel.getPunkte());
	}

	@Test
	public void createLoesunszettel_klasse_1_zwei_namen_erzeut_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1ZweiNamen();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfn", loesungszettel.getWertungscode());
		assertEquals(Integer.valueOf(2), loesungszettel.getKaengurusprung());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,n", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertFalse(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(2725), loesungszettel.getPunkte());
	}

	@Test
	public void createLoesunszettel_klasse_1_zwei_namen_typo_vollstaendigen_loesungszettel() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1ZweiNamenUndTypo();

		// Act
		final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);

		// Assert
		assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
		assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
		assertEquals("rrffnrffrrfn", loesungszettel.getWertungscode());
		assertEquals(Integer.valueOf(2), loesungszettel.getKaengurusprung());
		assertEquals("r,r,F,f,n,r,f,f,r,r,f,m", loesungszettel.getNutzereingabe());
		assertNull(loesungszettel.getAntwortcode());
		assertTrue(loesungszettel.isTypo());
		assertEquals(Integer.valueOf(2725), loesungszettel.getPunkte());
	}

	@Test
	public void berechneRohdatenKlapptAuchBeiTypo() throws IOException {
		// Arrange
		try (InputStream in = getClass().getResourceAsStream("/error/minus_statt_buchstabe.xlsx")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_OPEN_XML);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
			{
				final MKAuswertungRow row = rows.get(0);
				final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);
				assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
				assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
				assertEquals("rrrrfrrrfffr", loesungszettel.getWertungscode());
				assertEquals(Integer.valueOf(4), loesungszettel.getKaengurusprung());
				assertEquals("r,r,r,r,f,r,r,r,f,f,f,r", loesungszettel.getNutzereingabe());
				assertNull(loesungszettel.getAntwortcode());
				assertFalse(loesungszettel.isTypo());
				assertEquals(Integer.valueOf(3625), loesungszettel.getPunkte());

			}
			{
				final MKAuswertungRow row = rows.get(1);
				final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);
				assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
				assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
				assertEquals("fffnfrfrffnf", loesungszettel.getWertungscode());
				assertEquals(Integer.valueOf(1), loesungszettel.getKaengurusprung());
				assertEquals("f,f,f,-,f,r,f,r,f,f,n,f", loesungszettel.getNutzereingabe());
				assertNull(loesungszettel.getAntwortcode());
				assertTrue(loesungszettel.isTypo());
				assertEquals(Integer.valueOf(1200), loesungszettel.getPunkte());
			}
		}
	}

	@Test
	public void berechneRohdatenKlapptAuchBeiTypo_ziffer_statt_buchstabe() throws IOException {
		// Arrange
		try (InputStream in = getClass().getResourceAsStream("/error/ziffer_statt_buchstabe.xlsx")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_OPEN_XML);
			final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
			final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
			assertEquals(2, rows.size());
			{
				final MKAuswertungRow row = rows.get(0);
				final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);
				assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
				assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
				assertEquals("rrrrfrrrfffr", loesungszettel.getWertungscode());
				assertEquals(Integer.valueOf(4), loesungszettel.getKaengurusprung());
				assertEquals("r,r,r,r,f,r,r,r,f,f,f,r", loesungszettel.getNutzereingabe());
				assertNull(loesungszettel.getAntwortcode());
				assertFalse(loesungszettel.isTypo());
				assertEquals(Integer.valueOf(3625), loesungszettel.getPunkte());

			}
			{
				final MKAuswertungRow row = rows.get(1);
				final LoesungszettelRohdaten loesungszettel = auswerter.berechneRohdaten(row);
				assertEquals(Klassenstufe.EINS, loesungszettel.getKlassenstufe());
				assertEquals(Auswertungsquelle.UPLOAD, loesungszettel.getAuswertungsquelle());
				assertEquals("fffnfrfrffnf", loesungszettel.getWertungscode());
				assertEquals(Integer.valueOf(1), loesungszettel.getKaengurusprung());
				assertEquals("f,f,f,4,f,r,f,r,f,f,n,f", loesungszettel.getNutzereingabe());
				assertNull(loesungszettel.getAntwortcode());
				assertTrue(loesungszettel.isTypo());
				assertEquals(Integer.valueOf(1200), loesungszettel.getPunkte());
			}
		}
	}

}
