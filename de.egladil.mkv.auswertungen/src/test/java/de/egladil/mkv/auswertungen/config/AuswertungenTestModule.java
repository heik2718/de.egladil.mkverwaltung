//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.email.service.IMailservice;
import de.egladil.mkv.auswertungen.persistence.AsyncUploadListener;
import de.egladil.mkv.auswertungen.persistence.IUploadListener;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.auswertungen.statistik.impl.SchulstatistikServiceImpl;
import de.egladil.mkv.auswertungen.statistik.impl.VerteilungGeneratorImpl;
import de.egladil.mkv.auswertungen.stubs.MailserviceStub;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.auswertungen.urkunden.impl.AuswertungServiceImpl;
import de.egladil.mkv.persistence.config.MKVPersistenceModule;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;
import de.egladil.mkv.persistence.service.impl.AuswertungsgruppenServiceImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerFacadeImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerServiceImpl;

/**
 * MKVModule
 */
public class AuswertungenTestModule extends AbstractModule {

	private final String configRoot;

	/**
	 * Erzeugt eine Instanz von AuswertungenTestModule
	 */
	public AuswertungenTestModule(final String configRoot) {
		this.configRoot = configRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());
		install(new MKVPersistenceModule(this.configRoot));
		bind(IEgladilConfiguration.class).to(AuswertungenTestConfiguration.class);
		bind(IUploadListener.class).to(AsyncUploadListener.class);
		bind(AuswertungService.class).to(AuswertungServiceImpl.class);
		bind(IVerteilungGenerator.class).to(VerteilungGeneratorImpl.class);
		bind(IProtokollService.class).to(ProtokollService.class);
		bind(TeilnehmerFacade.class).to(TeilnehmerFacadeImpl.class);
		bind(TeilnehmerService.class).to(TeilnehmerServiceImpl.class);
		bind(SchulstatistikService.class).to(SchulstatistikServiceImpl.class);
		bind(AuswertungsgruppenService.class).to(AuswertungsgruppenServiceImpl.class);
		bind(IMailservice.class).to(MailserviceStub.class);
	}

	/**
	 *
	 * @param binder
	 */
	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", this.configRoot);
		Names.bindProperties(binder, properties);
	}
}
