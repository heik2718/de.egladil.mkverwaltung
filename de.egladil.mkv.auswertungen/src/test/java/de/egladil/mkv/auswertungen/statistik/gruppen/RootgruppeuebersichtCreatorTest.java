//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;

/**
 * RootgruppeuebersichtCreatorTest
 */
public class RootgruppeuebersichtCreatorTest {

	@Test
	void createNormalfall() {
		// Arrange

		final List<TeilnehmerUndPunkte> alleTeilnehmer = TestHelper.getSchulrohdaten();

		// Act
		final Rootgruppeuebersicht rootgruppeuebersicht = new RootgruppeuebersichtCreator().createSchuluebersicht(alleTeilnehmer, "Blümchenschule");
		final List<Klassenschluessel> keySet = rootgruppeuebersicht.getKeysSorted();

		// Assert
		assertEquals(3, keySet.size());

		final Iterator<Klassenschluessel> iter = keySet.iterator();
		int index = 0;

		while (iter.hasNext()) {
			final Klassenschluessel key = iter.next();

			switch (index) {
			case 0:
				assertEquals(TestHelper.AMEISEN, key);
				break;
			case 1:
				assertEquals(TestHelper.LOEWEN, key);
				break;
			case 2:
				assertEquals(TestHelper.GIRAFFEN, key);
				break;
			default:
				break;
			}
			check(rootgruppeuebersicht, key);
			index++;
		}
	}

	private void check(final Rootgruppeuebersicht rootgruppeuebersicht, final Klassenschluessel key) {
		final List<TeilnehmerUndPunkteLoesungszettelAdapter> schuelerinnen = rootgruppeuebersicht.getKlassenliste(key);
		if (TestHelper.AMEISEN.equals(key)) {
			assertEquals(2, schuelerinnen.size());
			{
				final TeilnehmerUndPunkteLoesungszettelAdapter tup = schuelerinnen.get(0);
				assertEquals("Alina Franz", tup.getName());
			}
			{
				final TeilnehmerUndPunkteLoesungszettelAdapter tup = schuelerinnen.get(1);
				assertEquals("Zacharias Meier", tup.getName());
			}
		}
		if (TestHelper.GIRAFFEN.equals(key)) {
			assertEquals(1, schuelerinnen.size());
			{
				final TeilnehmerUndPunkteLoesungszettelAdapter tup = schuelerinnen.get(0);
				assertEquals("Sonja Winter", tup.getName());
			}
		}
		if (TestHelper.LOEWEN.equals(key)) {
			assertEquals(1, schuelerinnen.size());
			{
				final TeilnehmerUndPunkteLoesungszettelAdapter tup = schuelerinnen.get(0);
				assertEquals("Heiner Baum", tup.getName());
			}
		}
	}
}
