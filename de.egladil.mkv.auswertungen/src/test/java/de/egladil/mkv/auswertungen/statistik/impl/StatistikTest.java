//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.RohpunktItem;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;

/**
 * StatistikTest
 */
public class StatistikTest {

	private class LoesungszettelAdapter implements ILoesungszettel {
		private final Integer punkte;

		/**
		 * Erzeugt eine Instanz von LoesungszettelAdapter
		 */
		public LoesungszettelAdapter(final Integer punkte) {
			this.punkte = punkte;
		}

		@Override
		public Integer getPunkte() {
			return punkte;
		}

		@Override
		public Integer getKaengurusprung() {
			return 12;
		}

		@Override
		public String getWertungscode() {
			return "rrrrrrrrrrrr";
		}

	}

	private List<RohpunktItem> items;

	private List<Integer> haeufigkeiten;

	private List<ILoesungszettel> alleLoesungszettel;

	@BeforeEach
	public void setUp() {
		items = new ArrayList<>();
		haeufigkeiten = new ArrayList<>();
		alleLoesungszettel = new ArrayList<>();

		{
			final RohpunktItem item = new RohpunktItem(0, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(9));
		{
			final RohpunktItem item = new RohpunktItem(100, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(10));
		{
			final RohpunktItem item = new RohpunktItem(125, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(12));
		{
			final RohpunktItem item = new RohpunktItem(200, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(13));
		{
			final RohpunktItem item = new RohpunktItem(300, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(14));
		{
			final RohpunktItem item = new RohpunktItem(375, 24);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(38));
		{
			final RohpunktItem item = new RohpunktItem(450, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(39));
		{
			final RohpunktItem item = new RohpunktItem(475, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(40));
		{
			final RohpunktItem item = new RohpunktItem(500, 15);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(55));
		{
			final RohpunktItem item = new RohpunktItem(525, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(56));
		{
			final RohpunktItem item = new RohpunktItem(550, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(57));
		{
			final RohpunktItem item = new RohpunktItem(575, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(59));
		{
			final RohpunktItem item = new RohpunktItem(600, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(60));
		{
			final RohpunktItem item = new RohpunktItem(625, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(70));
		{
			final RohpunktItem item = new RohpunktItem(700, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(73));
		{
			final RohpunktItem item = new RohpunktItem(725, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(74));
		{
			final RohpunktItem item = new RohpunktItem(750, 24);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(98));
		{
			final RohpunktItem item = new RohpunktItem(775, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(99));
		{
			final RohpunktItem item = new RohpunktItem(800, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(101));
		{
			final RohpunktItem item = new RohpunktItem(825, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(103));
		{
			final RohpunktItem item = new RohpunktItem(850, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(110));
		{
			final RohpunktItem item = new RohpunktItem(875, 44);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(154));
		{
			final RohpunktItem item = new RohpunktItem(900, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(155));
		{
			final RohpunktItem item = new RohpunktItem(925, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(160));
		{
			final RohpunktItem item = new RohpunktItem(950, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(163));
		{
			final RohpunktItem item = new RohpunktItem(975, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(173));
		{
			final RohpunktItem item = new RohpunktItem(1000, 34);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(207));
		{
			final RohpunktItem item = new RohpunktItem(1025, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(210));
		{
			final RohpunktItem item = new RohpunktItem(1050, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(216));
		{
			final RohpunktItem item = new RohpunktItem(1075, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(223));
		{
			final RohpunktItem item = new RohpunktItem(1100, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(234));
		{
			final RohpunktItem item = new RohpunktItem(1125, 29);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(263));
		{
			final RohpunktItem item = new RohpunktItem(1150, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(265));
		{
			final RohpunktItem item = new RohpunktItem(1175, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(275));
		{
			final RohpunktItem item = new RohpunktItem(1200, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(284));
		{
			final RohpunktItem item = new RohpunktItem(1225, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(291));
		{
			final RohpunktItem item = new RohpunktItem(1250, 49);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(340));
		{
			final RohpunktItem item = new RohpunktItem(1275, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(346));
		{
			final RohpunktItem item = new RohpunktItem(1300, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(354));
		{
			final RohpunktItem item = new RohpunktItem(1325, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(363));
		{
			final RohpunktItem item = new RohpunktItem(1350, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(369));
		{
			final RohpunktItem item = new RohpunktItem(1375, 56);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(425));
		{
			final RohpunktItem item = new RohpunktItem(1400, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(430));
		{
			final RohpunktItem item = new RohpunktItem(1425, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(435));
		{
			final RohpunktItem item = new RohpunktItem(1450, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(448));
		{
			final RohpunktItem item = new RohpunktItem(1475, 15);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(463));
		{
			final RohpunktItem item = new RohpunktItem(1500, 63);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(526));
		{
			final RohpunktItem item = new RohpunktItem(1525, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(533));
		{
			final RohpunktItem item = new RohpunktItem(1550, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(539));
		{
			final RohpunktItem item = new RohpunktItem(1575, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(550));
		{
			final RohpunktItem item = new RohpunktItem(1600, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(557));
		{
			final RohpunktItem item = new RohpunktItem(1625, 68);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(625));
		{
			final RohpunktItem item = new RohpunktItem(1650, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(637));
		{
			final RohpunktItem item = new RohpunktItem(1675, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(647));
		{
			final RohpunktItem item = new RohpunktItem(1700, 18);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(665));
		{
			final RohpunktItem item = new RohpunktItem(1725, 20);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(685));
		{
			final RohpunktItem item = new RohpunktItem(1750, 77);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(762));
		{
			final RohpunktItem item = new RohpunktItem(1775, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(765));
		{
			final RohpunktItem item = new RohpunktItem(1800, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(778));
		{
			final RohpunktItem item = new RohpunktItem(1825, 21);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(799));
		{
			final RohpunktItem item = new RohpunktItem(1850, 22);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(821));
		{
			final RohpunktItem item = new RohpunktItem(1875, 86);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(907));
		{
			final RohpunktItem item = new RohpunktItem(1900, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(918));
		{
			final RohpunktItem item = new RohpunktItem(1925, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(926));
		{
			final RohpunktItem item = new RohpunktItem(1950, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(933));
		{
			final RohpunktItem item = new RohpunktItem(1975, 14);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(947));
		{
			final RohpunktItem item = new RohpunktItem(2000, 54);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1001));
		{
			final RohpunktItem item = new RohpunktItem(2025, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1009));
		{
			final RohpunktItem item = new RohpunktItem(2050, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1022));
		{
			final RohpunktItem item = new RohpunktItem(2075, 25);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1047));
		{
			final RohpunktItem item = new RohpunktItem(2100, 19);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1066));
		{
			final RohpunktItem item = new RohpunktItem(2125, 92);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1158));
		{
			final RohpunktItem item = new RohpunktItem(2150, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1165));
		{
			final RohpunktItem item = new RohpunktItem(2175, 16);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1181));
		{
			final RohpunktItem item = new RohpunktItem(2200, 15);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1196));
		{
			final RohpunktItem item = new RohpunktItem(2225, 23);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1219));
		{
			final RohpunktItem item = new RohpunktItem(2250, 67);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1286));
		{
			final RohpunktItem item = new RohpunktItem(2275, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1294));
		{
			final RohpunktItem item = new RohpunktItem(2300, 22);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1316));
		{
			final RohpunktItem item = new RohpunktItem(2325, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1328));
		{
			final RohpunktItem item = new RohpunktItem(2350, 18);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1346));
		{
			final RohpunktItem item = new RohpunktItem(2375, 102);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1448));
		{
			final RohpunktItem item = new RohpunktItem(2400, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1454));
		{
			final RohpunktItem item = new RohpunktItem(2425, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1466));
		{
			final RohpunktItem item = new RohpunktItem(2450, 21);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1487));
		{
			final RohpunktItem item = new RohpunktItem(2475, 18);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1505));
		{
			final RohpunktItem item = new RohpunktItem(2500, 80);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1585));
		{
			final RohpunktItem item = new RohpunktItem(2525, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1598));
		{
			final RohpunktItem item = new RohpunktItem(2550, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1604));
		{
			final RohpunktItem item = new RohpunktItem(2575, 17);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1621));
		{
			final RohpunktItem item = new RohpunktItem(2600, 17);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1638));
		{
			final RohpunktItem item = new RohpunktItem(2625, 66);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1704));
		{
			final RohpunktItem item = new RohpunktItem(2650, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1711));
		{
			final RohpunktItem item = new RohpunktItem(2675, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1723));
		{
			final RohpunktItem item = new RohpunktItem(2700, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1732));
		{
			final RohpunktItem item = new RohpunktItem(2725, 18);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1750));
		{
			final RohpunktItem item = new RohpunktItem(2750, 70);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1820));
		{
			final RohpunktItem item = new RohpunktItem(2775, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1826));
		{
			final RohpunktItem item = new RohpunktItem(2800, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1838));
		{
			final RohpunktItem item = new RohpunktItem(2825, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1849));
		{
			final RohpunktItem item = new RohpunktItem(2850, 17);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1866));
		{
			final RohpunktItem item = new RohpunktItem(2875, 57);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1923));
		{
			final RohpunktItem item = new RohpunktItem(2900, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1934));
		{
			final RohpunktItem item = new RohpunktItem(2925, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1943));
		{
			final RohpunktItem item = new RohpunktItem(2950, 17);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1960));
		{
			final RohpunktItem item = new RohpunktItem(2975, 23);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(1983));
		{
			final RohpunktItem item = new RohpunktItem(3000, 66);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2049));
		{
			final RohpunktItem item = new RohpunktItem(3025, 4);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2053));
		{
			final RohpunktItem item = new RohpunktItem(3050, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2059));
		{
			final RohpunktItem item = new RohpunktItem(3075, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2069));
		{
			final RohpunktItem item = new RohpunktItem(3100, 21);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2090));
		{
			final RohpunktItem item = new RohpunktItem(3125, 38);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2128));
		{
			final RohpunktItem item = new RohpunktItem(3150, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2134));
		{
			final RohpunktItem item = new RohpunktItem(3175, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2137));
		{
			final RohpunktItem item = new RohpunktItem(3200, 15);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2152));
		{
			final RohpunktItem item = new RohpunktItem(3225, 29);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2181));
		{
			final RohpunktItem item = new RohpunktItem(3250, 63);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2244));
		{
			final RohpunktItem item = new RohpunktItem(3275, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2249));
		{
			final RohpunktItem item = new RohpunktItem(3300, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2259));
		{
			final RohpunktItem item = new RohpunktItem(3325, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2272));
		{
			final RohpunktItem item = new RohpunktItem(3350, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2283));
		{
			final RohpunktItem item = new RohpunktItem(3375, 54);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2337));
		{
			final RohpunktItem item = new RohpunktItem(3400, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2342));
		{
			final RohpunktItem item = new RohpunktItem(3425, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2348));
		{
			final RohpunktItem item = new RohpunktItem(3450, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2358));
		{
			final RohpunktItem item = new RohpunktItem(3475, 16);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2374));
		{
			final RohpunktItem item = new RohpunktItem(3500, 55);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2429));
		{
			final RohpunktItem item = new RohpunktItem(3525, 4);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2433));
		{
			final RohpunktItem item = new RohpunktItem(3550, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2436));
		{
			final RohpunktItem item = new RohpunktItem(3575, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2444));
		{
			final RohpunktItem item = new RohpunktItem(3600, 17);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2461));
		{
			final RohpunktItem item = new RohpunktItem(3625, 44);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2505));
		{
			final RohpunktItem item = new RohpunktItem(3650, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2506));
		{
			final RohpunktItem item = new RohpunktItem(3675, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2517));
		{
			final RohpunktItem item = new RohpunktItem(3700, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2530));
		{
			final RohpunktItem item = new RohpunktItem(3725, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2542));
		{
			final RohpunktItem item = new RohpunktItem(3750, 37);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2579));
		{
			final RohpunktItem item = new RohpunktItem(3775, 4);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2583));
		{
			final RohpunktItem item = new RohpunktItem(3800, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2586));
		{
			final RohpunktItem item = new RohpunktItem(3825, 12);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2598));
		{
			final RohpunktItem item = new RohpunktItem(3850, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2605));
		{
			final RohpunktItem item = new RohpunktItem(3875, 41);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2646));
		{
			final RohpunktItem item = new RohpunktItem(3925, 4);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2650));
		{
			final RohpunktItem item = new RohpunktItem(3950, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2655));
		{
			final RohpunktItem item = new RohpunktItem(3975, 13);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2668));
		{
			final RohpunktItem item = new RohpunktItem(4000, 47);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2715));
		{
			final RohpunktItem item = new RohpunktItem(4050, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2718));
		{
			final RohpunktItem item = new RohpunktItem(4075, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2720));
		{
			final RohpunktItem item = new RohpunktItem(4100, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2729));
		{
			final RohpunktItem item = new RohpunktItem(4125, 31);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2760));
		{
			final RohpunktItem item = new RohpunktItem(4175, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2763));
		{
			final RohpunktItem item = new RohpunktItem(4200, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2768));
		{
			final RohpunktItem item = new RohpunktItem(4225, 14);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2782));
		{
			final RohpunktItem item = new RohpunktItem(4250, 29);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2811));
		{
			final RohpunktItem item = new RohpunktItem(4275, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2812));
		{
			final RohpunktItem item = new RohpunktItem(4300, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2817));
		{
			final RohpunktItem item = new RohpunktItem(4325, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2825));
		{
			final RohpunktItem item = new RohpunktItem(4350, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2828));
		{
			final RohpunktItem item = new RohpunktItem(4375, 37);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2865));
		{
			final RohpunktItem item = new RohpunktItem(4450, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2872));
		{
			final RohpunktItem item = new RohpunktItem(4475, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2880));
		{
			final RohpunktItem item = new RohpunktItem(4500, 29);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2909));
		{
			final RohpunktItem item = new RohpunktItem(4525, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2912));
		{
			final RohpunktItem item = new RohpunktItem(4550, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2913));
		{
			final RohpunktItem item = new RohpunktItem(4575, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2916));
		{
			final RohpunktItem item = new RohpunktItem(4600, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2923));
		{
			final RohpunktItem item = new RohpunktItem(4625, 16);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2939));
		{
			final RohpunktItem item = new RohpunktItem(4700, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2944));
		{
			final RohpunktItem item = new RohpunktItem(4725, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2949));
		{
			final RohpunktItem item = new RohpunktItem(4750, 24);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2973));
		{
			final RohpunktItem item = new RohpunktItem(4825, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2980));
		{
			final RohpunktItem item = new RohpunktItem(4850, 7);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(2987));
		{
			final RohpunktItem item = new RohpunktItem(4875, 22);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3009));
		{
			final RohpunktItem item = new RohpunktItem(4925, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3010));
		{
			final RohpunktItem item = new RohpunktItem(4950, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3012));
		{
			final RohpunktItem item = new RohpunktItem(4975, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3018));
		{
			final RohpunktItem item = new RohpunktItem(5000, 21);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3039));
		{
			final RohpunktItem item = new RohpunktItem(5050, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3040));
		{
			final RohpunktItem item = new RohpunktItem(5075, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3042));
		{
			final RohpunktItem item = new RohpunktItem(5100, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3050));
		{
			final RohpunktItem item = new RohpunktItem(5125, 18);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3068));
		{
			final RohpunktItem item = new RohpunktItem(5150, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3069));
		{
			final RohpunktItem item = new RohpunktItem(5200, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3070));
		{
			final RohpunktItem item = new RohpunktItem(5225, 5);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3075));
		{
			final RohpunktItem item = new RohpunktItem(5250, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3084));
		{
			final RohpunktItem item = new RohpunktItem(5300, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3086));
		{
			final RohpunktItem item = new RohpunktItem(5325, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3087));
		{
			final RohpunktItem item = new RohpunktItem(5350, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3088));
		{
			final RohpunktItem item = new RohpunktItem(5375, 25);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3113));
		{
			final RohpunktItem item = new RohpunktItem(5475, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3114));
		{
			final RohpunktItem item = new RohpunktItem(5500, 26);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3140));
		{
			final RohpunktItem item = new RohpunktItem(5600, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3143));
		{
			final RohpunktItem item = new RohpunktItem(5625, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3154));
		{
			final RohpunktItem item = new RohpunktItem(5725, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3156));
		{
			final RohpunktItem item = new RohpunktItem(5750, 10);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3166));
		{
			final RohpunktItem item = new RohpunktItem(5875, 9);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3175));
		{
			final RohpunktItem item = new RohpunktItem(5975, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3176));
		{
			final RohpunktItem item = new RohpunktItem(6000, 4);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3180));
		{
			final RohpunktItem item = new RohpunktItem(6100, 1);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3181));
		{
			final RohpunktItem item = new RohpunktItem(6125, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3189));
		{
			final RohpunktItem item = new RohpunktItem(6225, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3191));
		{
			final RohpunktItem item = new RohpunktItem(6250, 11);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3202));
		{
			final RohpunktItem item = new RohpunktItem(6375, 8);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3210));
		{
			final RohpunktItem item = new RohpunktItem(6500, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3216));
		{
			final RohpunktItem item = new RohpunktItem(6625, 3);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3219));
		{
			final RohpunktItem item = new RohpunktItem(6750, 2);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3221));
		{
			final RohpunktItem item = new RohpunktItem(6875, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3227));
		{
			final RohpunktItem item = new RohpunktItem(7000, 4);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3231));
		{
			final RohpunktItem item = new RohpunktItem(7500, 6);
			items.add(item);
		}
		haeufigkeiten.add(Integer.valueOf(3237));

		for (final RohpunktItem item : items) {
			for (int i = 0; i < item.getAnzahl(); i++) {
				alleLoesungszettel.add(new LoesungszettelAdapter(item.getPunkte()));
			}
		}
	}

	@Test
	public void berechneKumulierteHaeufigkeit_klasse_2() {
		// Arrange
		final BerechneProzentrangPunkteCommand command = new BerechneProzentrangPunkteCommand();

		// Act + Assert
		for (int i = 0; i < haeufigkeiten.size(); i++) {
			final int expected = haeufigkeiten.get(i);
			final int actual = command.berechneKumulierteHaeufigkeit(items.get(i).getPunkte(), items);

			assertEquals(expected, actual);
		}
	}

	@Test
	public void berechneMedian_klasse_2() {
		// Arrange
		final BerechneMedianCommand command = new BerechneMedianCommand();

		// Act
		final double actual = command.berechne(alleLoesungszettel);

		// Assert
		assertEquals(Double.toString(25.75), Double.toString(actual));
	}
}
