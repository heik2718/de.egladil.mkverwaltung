//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;

import org.junit.jupiter.api.Test;


/**
 * GesamtpunktverteilungXmlTest
 */
public class GesamtpunktverteilungXmlTest {

	@Test
	public void unmarshall() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/gesamtpunktverteilung.xml")) {
			final JAXBContext jaxbContext = JAXBContext.newInstance(Gesamtpunktverteilung.class);
			final Gesamtpunktverteilung verteilung = (Gesamtpunktverteilung) jaxbContext.createUnmarshaller().unmarshal(in);

			// Assert
			assertEquals("Ergebnisse 2000 - Klasse 2", verteilung.getTitel());
			assertTrue(verteilung.getBasis().startsWith("Die Punktverteilung wurde auf der Grundlage der zurückgesendeten"));
			assertTrue(verteilung.getBewertung().startsWith("Die Kinder erhielten ein Startguthaben von 15,00 Punkten."));
			assertEquals(28, verteilung.getAnzahlTeilnehmer());
			assertEquals(16, verteilung.getGesamtpunktverteilungItems().size());
			assertEquals(15, verteilung.getAufgabeErgebnisItems().size());
			assertEquals(23, verteilung.getProzentrangItems().size());
			assertEquals("25,75", verteilung.getMedian());
		}
	}
}
