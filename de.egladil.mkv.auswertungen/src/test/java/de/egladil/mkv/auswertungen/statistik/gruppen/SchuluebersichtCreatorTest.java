//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * SchuluebersichtCreatorTest
 */
public class SchuluebersichtCreatorTest {

	@Test
	void createNormalfall() {
		// Arrange
		final List<Loesungszettel> alleLoesungszettel = new ArrayList<>();
		{
			final Loesungszettel loesungszettel = new Loesungszettel();
			final LoesungszettelRohdaten rohdaten = new LoesungszettelRohdaten.Builder(Auswertungsquelle.UPLOAD, Klassenstufe.ZWEI,
				4, 2250, "rrrrnfnfnfnrnrn", "rrrfnfnfnfnrnrn").build();
			loesungszettel.setLoesungszettelRohdaten(rohdaten);
			alleLoesungszettel.add(loesungszettel);
		}
		{
			final Loesungszettel loesungszettel = new Loesungszettel();
			final LoesungszettelRohdaten rohdaten = new LoesungszettelRohdaten.Builder(Auswertungsquelle.UPLOAD, Klassenstufe.EINS,
				2, 1775, "rrfnrrfnnnnn", "rrfnrrfnnnnn").build();
			loesungszettel.setLoesungszettelRohdaten(rohdaten);
			alleLoesungszettel.add(loesungszettel);
		}
		{
			final Loesungszettel loesungszettel = new Loesungszettel();
			final LoesungszettelRohdaten rohdaten = new LoesungszettelRohdaten.Builder(Auswertungsquelle.UPLOAD, Klassenstufe.ZWEI,
				4, 2250, "rrrrnfnfnfnrnrn", "rrrfnfnfnfnrnrn").build();
			loesungszettel.setLoesungszettelRohdaten(rohdaten);
			alleLoesungszettel.add(loesungszettel);
		}

		// Act
		final Schuluebersicht schuluebersicht = new SchuluebersichtCreator().createSchuluebersicht(alleLoesungszettel,
			"Blümchenschule");
		final List<Klassenstufe> keySet = schuluebersicht.getKeysSorted();

		// Assert
		assertEquals(2, keySet.size());

		final Iterator<Klassenstufe> iter = keySet.iterator();
		int index = 0;

		while (iter.hasNext()) {
			final Klassenstufe key = iter.next();

			switch (index) {
			case 0:
				assertEquals(Klassenstufe.EINS, key);
				break;
			case 1:
				assertEquals(Klassenstufe.ZWEI, key);
				break;
			default:
				break;
			}
			check(schuluebersicht, key);
			index++;
		}
	}

	private void check(final Schuluebersicht schuluebersicht, final Klassenstufe key) {
		final List<Loesungszettel> schuelerinnen = schuluebersicht.getKlassenstufenLoesungszettel(key);
		if (Klassenstufe.EINS == key) {
			assertEquals(1, schuelerinnen.size());
		}
		if (Klassenstufe.ZWEI == key) {
			assertEquals(2, schuelerinnen.size());
		}
	}

}
