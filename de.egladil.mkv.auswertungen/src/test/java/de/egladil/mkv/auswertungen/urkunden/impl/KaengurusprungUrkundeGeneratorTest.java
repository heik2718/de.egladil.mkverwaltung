//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.auswertungen.urkunden.EinzelurkundeGenerator;
import de.egladil.mkv.auswertungen.urkunden.EinzelurkundeGeneratorFactory;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundenmotivProvider;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitText;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitTextBuilder;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * KaengurusprungUrkundeGeneratorTest
 */
public class KaengurusprungUrkundeGeneratorTest {

	private static final String URKUNDE_FILE_EINE_ZEILE_OHNE_SCHULE = "/home/heike/temp/kaengurusprungurkunde_ohne_schule_1.pdf";

	private static final String URKUNDE_FILE_EINE_ZEILE = "/home/heike/temp/kaengurusprungurkunde_1.pdf";

	private static final String URKUNDE_KAENGURUSPRUNG_EN= "/home/heike/temp/certificate-kangaroo-jump.pdf";

	private static final String URKUNDE_FILE_ZWEI_ZEILEN = "/home/heike/temp/kaengurusprungurkunde_2.pdf";

	private static final String URKUNDE_FILE_DREI_ZEILEN = "/home/heike/temp/kaengurusprungurkunde_3.pdf";

	private UrkundeInhalt inhalt;

	private EinzelurkundeGenerator generator;

	@BeforeEach
	void setUp() {
		generator = EinzelurkundeGeneratorFactory.createKaengurusprungurkundeGenerator();
		final UrkundenmotivMitText motiv = new UrkundenmotivMitTextBuilder("2010",
			DefaultUrkundenmotivProvider.ausFarbschema(Farbschema.ORANGE), "21.11.2017").build();
		inhalt = Mockito.mock(UrkundeInhalt.class);
		Mockito.when(inhalt.getBackgroundImage()).thenReturn(motiv.getBackgroundImage());
		Mockito.when(inhalt.getHeadlineColor()).thenReturn(motiv.getHeadlineColor());
		Mockito.when(inhalt.getDatum()).thenReturn("21.11.2017");
		Mockito.when(inhalt.getKaengurusprung()).thenReturn(15);
		Mockito.when(inhalt.getKlassenname()).thenReturn("Fuchsklasse");
		Mockito.when(inhalt.getSchulname()).thenReturn("Krautgartenschule Mainz-Kostheim");
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");
		Mockito.when(inhalt.getWettbewerbsjahr()).thenReturn("2017");
		Mockito.when(inhalt.getPunkteText()).thenReturn("43,5");
		Mockito.when(inhalt.getTeilnehmerKuerzel()).thenReturn("ZRHL8DG620170615173734");
		Mockito.when(inhalt.getSprache()).thenReturn(Sprache.de);
	}

	@Test
	@DisplayName("generieren eine Zeile")
	void nameEineZeileGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");

		// final Teilnehmer teilnehmer = new TeilnehmerBuilder(Teilnahmeart.P, "HZTF6G89", "2018",
		// Klassenstufe.ZWEI).vorname("Marie")
		// .nachname("Allmannsdörffer").checkAndBuild();
		//
		// final LoesungszettelPunkteProvider punkteProvider = Mockito.mock(LoesungszettelPunkteProvider.class);
		// Mockito.when(punkteProvider.getPunkte()).thenReturn(4350);
		// final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkteBuilder(teilnehmer,
		// punkteProvider).build();
		//
		// final UrkundeInhalt urkundeInhalt = new DefaultUrkundeInhaltBuilder(motiv).klassenname("Fuchsklasse")
		// .teilnehmerUndPunkte(teilnehmerUndPunkte).build();

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_EINE_ZEILE)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_EINE_ZEILE).canRead());
	}

	@Test
	@DisplayName("generieren eine Zeile")
	void kaengurusprungEnglischGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");
		Mockito.when(inhalt.getSprache()).thenReturn(Sprache.en);

		// final Teilnehmer teilnehmer = new TeilnehmerBuilder(Teilnahmeart.P, "HZTF6G89", "2018",
		// Klassenstufe.ZWEI).vorname("Marie")
		// .nachname("Allmannsdörffer").checkAndBuild();
		//
		// final LoesungszettelPunkteProvider punkteProvider = Mockito.mock(LoesungszettelPunkteProvider.class);
		// Mockito.when(punkteProvider.getPunkte()).thenReturn(4350);
		// final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkteBuilder(teilnehmer,
		// punkteProvider).build();
		//
		// final UrkundeInhalt urkundeInhalt = new DefaultUrkundeInhaltBuilder(motiv).klassenname("Fuchsklasse")
		// .teilnehmerUndPunkte(teilnehmerUndPunkte).build();

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_KAENGURUSPRUNG_EN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_EINE_ZEILE).canRead());
	}

	@Test
	@DisplayName("generieren eine Zeile ohne Schule")
	void nameEineZeileOhneSchuleGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");
		Mockito.when(inhalt.getSchulname()).thenReturn(null);

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_EINE_ZEILE_OHNE_SCHULE)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_EINE_ZEILE_OHNE_SCHULE).canRead());
	}

	@Test
	@DisplayName("generieren zwei Zeilen")
	void nameZweiZeilenGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Luise Allmannsdörffer- Langenscheidt");
		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_ZWEI_ZEILEN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_ZWEI_ZEILEN).canRead());
	}

	@Test
	@DisplayName("generieren drei Zeilen")
	void nameDreiZeilenGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername())
			.thenReturn("Alexander Maximilian Edmundo Theodor von Allmannsdörffer- Langenscheidt Wa");

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_DREI_ZEILEN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_DREI_ZEILEN).canRead());
	}

	@Test
	@DisplayName("generieren drei Zeilen")
	void nameZuLang() throws IOException {
		Mockito.when(inhalt.getTeilnehmername())
			.thenReturn("Alexander Maximilian Edmundo Theodor von Allmannsdörffer- Langenscheidt Was");

		final Throwable ex = assertThrows(MKVException.class, () -> {
			generator.generate(inhalt);
		});
		assertEquals("IllegalArgumentException beim generieren von ZRHL8DG620170615173734: name passt nicht in drei Zeilen",
			ex.getMessage());
	}
}
