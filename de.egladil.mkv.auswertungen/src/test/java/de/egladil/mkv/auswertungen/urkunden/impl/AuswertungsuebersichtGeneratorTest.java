//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.auswertungen.urkunden.inhalt.AuswertungUebersichtZeile;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundeInhaltBuilder;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitText;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitTextBuilder;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * AuswertungsuebersichtGeneratorTest
 */
public class AuswertungsuebersichtGeneratorTest {

	@BeforeEach
	void setup() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
	}

	@Test
	@DisplayName("generieren klappt im Normalfall")
	void generierenKlappt() throws Exception {

		final AuswertungsuebersichtGenerator generator = new AuswertungsuebersichtGenerator();
		final UrkundenmotivMitText urkundenmotiv = new UrkundenmotivMitTextBuilder("2018",
			Mockito.mock(UrkundenmotivProvider.class), "24.12.2017").build();
		final Map<Klassenstufe, List<UrkundeInhalt>> urkunden = new HashMap<>();
		final Map<Klassenstufe, List<UrkundeInhalt>> kaenguruspruenge = new HashMap<>();

		final List<UrkundeInhalt> urkundenKlasse1 = new ArrayList<>();
		final List<UrkundeInhalt> urkundenKlasse2 = new ArrayList<>();

		{
			final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkte("Anna Johanna", "21,5", 2150, 3,
				Klassenstufe.EINS, Sprache.en);
			final DefaultUrkundeInhalt urkundeInhalt = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
				.teilnehmerUndPunkte(teilnehmerUndPunkte).klassenname(Klassenstufe.EINS.getLabel()).build();
			urkundenKlasse1.add(urkundeInhalt);
		}

		{
			final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkte("Benno Menno", "34,25", 3425, 5,
				Klassenstufe.EINS, Sprache.de);
			final DefaultUrkundeInhalt urkundeInhalt = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
				.teilnehmerUndPunkte(teilnehmerUndPunkte).klassenname(Klassenstufe.EINS.getLabel()).build();
			kaenguruspruenge.put(Klassenstufe.EINS, Arrays.asList(new DefaultUrkundeInhalt[] { urkundeInhalt }));
			urkundenKlasse1.add(urkundeInhalt);
		}

		{
			final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkte("Zwerg Nase", "38", 3800, 4, Klassenstufe.ZWEI,
				Sprache.de);
			final DefaultUrkundeInhalt urkundeInhalt = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
				.teilnehmerUndPunkte(teilnehmerUndPunkte).klassenname(Klassenstufe.ZWEI.getLabel()).build();
			kaenguruspruenge.put(Klassenstufe.ZWEI, Arrays.asList(new DefaultUrkundeInhalt[] { urkundeInhalt }));
			urkundenKlasse2.add(urkundeInhalt);
		}

		{
			final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkte("Karlo Karlino", "18,75", 1875, 1,
				Klassenstufe.ZWEI, Sprache.en);
			final DefaultUrkundeInhalt urkundeInhalt = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
				.teilnehmerUndPunkte(teilnehmerUndPunkte).klassenname(Klassenstufe.ZWEI.getLabel()).build();
			urkundenKlasse2.add(urkundeInhalt);
		}

		urkunden.put(Klassenstufe.EINS, urkundenKlasse1);
		urkunden.put(Klassenstufe.ZWEI, urkundenKlasse2);

		final byte[] uebersicht = generator.generiereUebersicht(urkunden, kaenguruspruenge, "2018");
		assertNotNull(uebersicht);

		final File file = File.createTempFile("uebersicht-", ".pdf");
		try (OutputStream out = new FileOutputStream(file)) {
			out.write(uebersicht, 0, uebersicht.length);
			out.flush();
			System.out.println("Pfad = " + file.getAbsolutePath());
		}
	}

//	@Test
	void generiereZeilenKlappt() {
		// Arrange

		final AuswertungDaten auswertungDaten = new AuswertungDaten();

		// Kängurusprung
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Helga Hahnemann");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(5625));
			Mockito.when(urkunde.getPunkteText()).thenReturn("56,25");
			Mockito.when(urkunde.getZusatz()).thenReturn("1a");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(8));
			auswertungDaten.addKaenguruUrkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Ratlos");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(5625));
			Mockito.when(urkunde.getPunkteText()).thenReturn("56,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(7));
			auswertungDaten.addKaenguruUrkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Schlemmer");
			Mockito.when(urkunde.getZusatz()).thenReturn("f0ebabad");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(5625));
			Mockito.when(urkunde.getPunkteText()).thenReturn("56,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(7));
			auswertungDaten.addKaenguruUrkunde(Klassenstufe.ZWEI, urkunde);
		}

		// Klasse 1
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Klara Fall");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("7,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Anja Meier");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(2125));
			Mockito.when(urkunde.getPunkteText()).thenReturn("21,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(4));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Helga Hahnemann");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getZusatz()).thenReturn("1b");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(1025));
			Mockito.when(urkunde.getPunkteText()).thenReturn("10,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Hannelore Krüger");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(4475));
			Mockito.when(urkunde.getPunkteText()).thenReturn("44,75");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(5));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Jannik Annik");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(1600));
			Mockito.when(urkunde.getPunkteText()).thenReturn("16");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Justin Bieber");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(1825));
			Mockito.when(urkunde.getPunkteText()).thenReturn("18,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}

		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Mike Feustel");
			Mockito.when(urkunde.getZusatz()).thenReturn("vom Fenster");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(1725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("17,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(1));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Mike Feustel");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 1");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(1725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("17,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(1));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.EINS, urkunde);
		}

		// Klasse 2
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Schlemmer");
			Mockito.when(urkunde.getZusatz()).thenReturn("f0ebabad");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(4975));
			Mockito.when(urkunde.getPunkteText()).thenReturn("49,75");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(5));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Schlemmer");
			Mockito.when(urkunde.getZusatz()).thenReturn("8b1b5120");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(3725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("37,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(3));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Schlemmer");
			Mockito.when(urkunde.getZusatz()).thenReturn("fdac3d65");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(3725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("37,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(1));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Schlemmer");
			Mockito.when(urkunde.getZusatz()).thenReturn("702c4183");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(3725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("37,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Schlemmer");
			Mockito.when(urkunde.getZusatz()).thenReturn("fff");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(3350));
			Mockito.when(urkunde.getPunkteText()).thenReturn("33,5");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Holla Drio");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(4750));
			Mockito.when(urkunde.getPunkteText()).thenReturn("47,5");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(4));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Ratlos");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(4750));
			Mockito.when(urkunde.getPunkteText()).thenReturn("47,5");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(5));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Max Planck");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(3725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("37,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(2));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}
		{
			final UrkundeInhalt urkunde = Mockito.mock(UrkundeInhalt.class);
			Mockito.when(urkunde.getTeilnehmername()).thenReturn("Rudi Heinz Schlemmer");
			Mockito.when(urkunde.getKlassenname()).thenReturn("Klasse 2");
			Mockito.when(urkunde.getPunkte()).thenReturn(Integer.valueOf(3725));
			Mockito.when(urkunde.getPunkteText()).thenReturn("37,25");
			Mockito.when(urkunde.getKaengurusprung()).thenReturn(Integer.valueOf(7));
			auswertungDaten.addTeilnehmerurkunde(Klassenstufe.ZWEI, urkunde);
		}

		// Act
		final List<AuswertungUebersichtZeile> zeilen = new AuswertungsuebersichtGenerator().generateZeilen(auswertungDaten);

		assertEquals(22, zeilen.size());
		// Assert
		assertEquals("Auswertung Minikänguru 2019", zeilen.get(0).getText());
		assertEquals("Platzierungen", zeilen.get(1).getText());
		assertEquals("Klasse 1", zeilen.get(2).getText());
		assertEquals("1. Helga Hahnemann (1b), Klasse 1, 48,75 Punkte", zeilen.get(3).getText());
		assertEquals("2. Hannelore Krüger, Klasse 1, 44,75 Punkte", zeilen.get(4).getText());
		assertEquals("3. Anja Meier, Klasse 1, 21,25 Punkte", zeilen.get(5).getText());
		assertEquals("4. Justin Bieber, Klasse 1, 18,25 Punkte", zeilen.get(6).getText());
		assertEquals("5. Mike Feustel, Klasse 1, 17,25 Punkte", zeilen.get(7).getText());
		assertEquals("    Mike Feustel (vom Fenster), Klasse 1, 17,25 Punkte", zeilen.get(8).getText());
		assertEquals("7. Jannik Annik, Klasse 1, 16 Punkte", zeilen.get(9).getText());
		assertEquals("8. Klara Fall, Klasse 1, 7,25 Punkte", zeilen.get(10).getText());
		assertEquals("Klasse 2", zeilen.get(11).getText());
		assertEquals("1. Rudi Schlemmer (f0ebabad), Klasse 2, 49,75 Punkte", zeilen.get(12).getText());
		assertEquals("2. Holla Drio, Klasse 2, 47,5 Punkte", zeilen.get(13).getText());
		assertEquals("    Rudi Ratlos, Klasse 2, 47,5 Punkte", zeilen.get(14).getText());
		assertEquals("4. Max Planck, Klasse 2, 37,25 Punkte", zeilen.get(15).getText());
		assertEquals("    Rudi Heinz Schlemmer, Klasse 2, 37,25 Punkte", zeilen.get(16).getText());
		assertEquals("    Rudi Schlemmer (702c4183), Klasse 2, 37,25 Punkte", zeilen.get(17).getText());
		assertEquals("    Rudi Schlemmer (8b1b5120), Klasse 2, 37,25 Punkte", zeilen.get(18).getText());
		assertEquals("    Rudi Schlemmer (fdac3d65), Klasse 2, 37,25 Punkte", zeilen.get(19).getText());
		assertEquals("9. Rudi Schlemmer (fff), Klasse 2, 33,5 Punkte", zeilen.get(20).getText());
		assertEquals("Inklusion", zeilen.get(21).getText());

		assertEquals("Gewinner Kängurusprung", zeilen.get(21).getText());
		assertEquals("Klasse 1", zeilen.get(22).getText());
		assertEquals("Helga Hahnemann, Klasse 1, Länge Kängurusprung: 8", zeilen.get(23).getText());
		assertEquals("Klasse 2", zeilen.get(24).getText());
		assertEquals("Rudi Ratlos, Klasse 2, Länge Kängurusprung: 7", zeilen.get(25).getText());
		assertEquals("Rudi Schlemmer (f0ebabad), Klasse 2, Länge Kängurusprung: 7", zeilen.get(26).getText());

		final Integer[] boldFontIndexe = new Integer[] { 0, 1, 2, 11, 21, 22, 24 };
		for (final Integer i : boldFontIndexe) {
			final AuswertungUebersichtZeile zeile = zeilen.get(i);
			assertTrue("Fehler bei Index " + i, zeile.isBoldFont());
		}
		final Integer[] normalFontIndexe = new Integer[] { 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23, 25,
			26 };
		for (final Integer i : normalFontIndexe) {
			final AuswertungUebersichtZeile zeile = zeilen.get(i);
			assertFalse("Fehler bei Index " + i, zeile.isBoldFont());
		}
	}

	@Test
	void generiereZeilenParameterNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new AuswertungsuebersichtGenerator().generateZeilen(null);
		});
		assertEquals("auswertungDaten null", ex.getMessage());
	}

}
