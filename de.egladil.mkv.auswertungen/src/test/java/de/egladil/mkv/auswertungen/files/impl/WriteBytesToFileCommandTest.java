//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * WriteBytesToFileCommandTest
 */
public class WriteBytesToFileCommandTest {

	@Test
	public void writeToFile_throws_when_pathName_null() throws Exception {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new WriteBytesToFileCommand(new byte[0]).writeToFile(null);
		});
		assertEquals("Parameter absPath", ex.getMessage());

	}

	@Test
	public void writeToFile_throws_when_bytes_null() throws Exception {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new WriteBytesToFileCommand(null).writeToFile("/home/heike/Downloads");
		});
		assertEquals("Parameter bytes", ex.getMessage());

	}

}
