//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.auswertungen.AbstractGuiceIT;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.payload.response.PublicGesamtpunktverteilung;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.dao.ITeilnehmerDao;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.request.TeilnehmerUrkundenauftrag;

/**
 * AuswertungServiceImplIT
 */
public class AuswertungServiceImplIT extends AbstractGuiceIT {

	public static final String JAHR = "2018";

	private static final String TEILNAHMEKUERZEL = "BPN390UP";

	private static final String BENUTZER_UUID = "bf01e395-7d12-4e53-8a7c-2d595ae7c263";

	private TeilnahmeIdentifier teilnahmeIdentifier;

	@Inject
	private AuswertungService service;

	@Inject
	private IAuswertungDownloadDao dao;

	@Inject
	private ITeilnehmerDao teilnehmerDao;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(TEILNAHMEKUERZEL, JAHR);
	}

	@Nested
	@DisplayName("Tests für generiereAuswertung mit verschiedenen Parameterkombinationen")
	class AuswertungTeilnehmerTests {
		@Test
		@DisplayName("happy hour mit 2 Teilnehmern")
		void generiereAuswertungFuerEinzelteilnehmerZweiTeilnehmer() {

			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setTeilnehmerKuerzel(new String[] { "JDJP9E2B20171217075222", "873X8GP420171217080517" });
			auftrag.setFarbschemaName(Farbschema.ORANGE.toString());

			final Optional<String> optDownloadcode = service.generiereAuswertungFuerEinzelteilnehmer(BENUTZER_UUID, auftrag,
				teilnahmeIdentifier);

			assertTrue(optDownloadcode.isPresent());

			final Optional<AuswertungDownload> opt = dao.findByUniqueKey(AuswertungDownload.class,
				AuswertungDownload.UNIQUE_ATTRIBUTE_NAME, optDownloadcode.get());

			assertTrue(opt.isPresent());
		}

		@Test
		@DisplayName("happy hour mit 1 Teilnehmern")
		void generiereAuswertungFuerEinzelteilnehmerEinTeilnehmer() {

			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setTeilnehmerKuerzel(new String[] { "JDJP9E2B20171217075222" });
			auftrag.setFarbschemaName(Farbschema.ORANGE.toString());

			final Optional<String> optDownloadcode = service.generiereAuswertungFuerEinzelteilnehmer(BENUTZER_UUID, auftrag,
				teilnahmeIdentifier);

			assertTrue(optDownloadcode.isPresent());

			final Optional<AuswertungDownload> opt = dao.findByUniqueKey(AuswertungDownload.class,
				AuswertungDownload.UNIQUE_ATTRIBUTE_NAME, optDownloadcode.get());

			assertTrue(opt.isPresent());
		}

		@Test
		@DisplayName("happy hour alle Teilnehmer zu Privatteilnahme")
		void generiereAuswertungFuerPrivatteilnehmerAlle() {
			final List<Teilnehmer> teilnehmer = teilnehmerDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("BPN390UP", "2018"));

			final List<String> alleTeilnehmerKuerzel = new ArrayList<>();
			for (final Teilnehmer t : teilnehmer) {
				if (StringUtils.isNoneBlank(t.getLoesungszettelkuerzel())) {
					alleTeilnehmerKuerzel.add(t.getKuerzel());
				}
			}

			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setTeilnehmerKuerzel(alleTeilnehmerKuerzel.toArray(new String[0]));
			auftrag.setFarbschemaName(Farbschema.ORANGE.toString());

			final Optional<String> optDownloadcode = service.generiereAuswertungFuerEinzelteilnehmer(BENUTZER_UUID, auftrag,
				teilnahmeIdentifier);

			assertTrue(optDownloadcode.isPresent());

			final Optional<AuswertungDownload> opt = dao.findByUniqueKey(AuswertungDownload.class,
				AuswertungDownload.UNIQUE_ATTRIBUTE_NAME, optDownloadcode.get());

			assertTrue(opt.isPresent());
		}
	}

	@Nested
	@DisplayName("Tests für generiereGesamtpunktverteilung mit verschiedenen Parameterkombinationen")
	class GesamtpunktverteilungTests {
		@Test
		void generiereGesamtpunktverteilung() throws Exception {
			final List<GesamtpunktverteilungDaten> daten = service.generiereGesamtpunktverteilung(JAHR);
			final PublicGesamtpunktverteilung pg = new PublicGesamtpunktverteilung("Gesamtpunktverteilung", JAHR, daten);

			final ObjectMapper objectWriter = new ObjectMapper();
			final String json = objectWriter.writeValueAsString(pg);
			// System.out.println(json);
		}

		@Test
		void generiereGesamtpunktverteilungMitKlasse1Ausnahme() {
			service.generiereGesamtpunktverteilung("2014");
		}
	}

	@Nested
	@DisplayName("Tests für Gruppenübersicht")
	class GruppenuebersichtTests {
	}

}
