//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * TeilnehmerUndPunkteTest
 */
public class TeilnehmerUndPunkteTest {

	@Test
	@DisplayName("IllegalArgumentException wenn name null")
	void teilnehmernameNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkte(null, "25.25", 2525, 1, Klassenstufe.EINS, Sprache.en);
		});
		assertEquals("name und punkte sind erforderlich", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn name blank")
	void teilnehmernameBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkte(" ", "25.25", 2525, 1, Klassenstufe.EINS, Sprache.en);
		});
		assertEquals("name und punkte sind erforderlich", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn punkte null")
	void punkteNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkte("Heinz", null, 0, 1, Klassenstufe.EINS, Sprache.de);
		});
		assertEquals("name und punkte sind erforderlich", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn punkte blank")
	void punkteBlank() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkte("Heinz", "  ", 0, 1, Klassenstufe.EINS, Sprache.en);
		});
		assertEquals("name und punkte sind erforderlich", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn kaengurusprung kleiner 0")
	void kaengurusprung0() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkte("Heinz", "45 ", 4500, -1, Klassenstufe.EINS, Sprache.en);
		});
		assertEquals("kaengurusprung muss größer oder gleich 0 sein.", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn klassenstufe null")
	void klassenstufeNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkte("Heinz", "45 ", 4500, 4, null, Sprache.de);
		});
		assertEquals("klassenstufe null", ex.getMessage());
	}
}
