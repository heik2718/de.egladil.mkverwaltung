//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.config;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.OsUtils;

/**
 * AuswertungenTestConfiguration
 */
public class AuswertungenTestConfiguration extends AbstractEgladilConfiguration {
	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von AuswertungenTestConfiguration
	 */
	public AuswertungenTestConfiguration() {
		this(OsUtils.getDevConfigRoot());

	}

	/**
	 * Erzeugt eine Instanz von AuswertungenTestConfiguration
	 */
	public AuswertungenTestConfiguration(String pathConfigRoot) {
		super(OsUtils.getDevConfigRoot());
	}

	@Override
	protected String getConfigFileName() {
		return "mkv_service.properties";
	}

};