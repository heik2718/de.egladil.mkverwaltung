//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.auswertungen.eingaben.ILoesungszettelBerechnungsstrategie;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * UploadBerechnungsstrategieFactoryTest
 */
public class UploadBerechnungsstrategieFactoryTest {

	private WorkbookKategorisierer kategorisierer;

	private Map<Klassenstufe, String> loesungscodes = new HashMap<>();

	private UploadBerechnungsstrategieFactory factory;

	@BeforeEach
	public void setUp() {
		kategorisierer = Mockito.mock(WorkbookKategorisierer.class);
		loesungscodes = new HashMap<>();
		loesungscodes.put(Klassenstufe.EINS, "CDADECCCBCDE");
		loesungscodes.put(Klassenstufe.ZWEI, "EACDDCDCCEBACBA");

		factory = new UploadBerechnungsstrategieFactory(loesungscodes, kategorisierer);
	}

	@Test
	public void erzeugeStrategie_returns_Antwortbuchstaben() {
		// Arrange
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();
		Mockito.when(kategorisierer.errateKategorie(workbook)).thenReturn(WorkbookKategorie.ANTWORTBUCHSTABEN);

		// Act
		final Optional<ILoesungszettelBerechnungsstrategie> opt = factory.erzeugeStrategie(workbook);

		// Assert
		assertTrue(opt.isPresent());
		assertEquals(WorkbookKategorie.ANTWORTBUCHSTABEN, opt.get().getCorrespondingWorkbookKategorie());
	}

	@Test
	public void erzeugeStrategie_returns_Wertungsbuchstaben() {
		// Arrange
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();
		Mockito.when(kategorisierer.errateKategorie(workbook)).thenReturn(WorkbookKategorie.WERTUNGSBUCHSTABEN);

		// Act
		final Optional<ILoesungszettelBerechnungsstrategie> opt = factory.erzeugeStrategie(workbook);

		// Assert
		assertTrue(opt.isPresent());
		assertEquals(WorkbookKategorie.WERTUNGSBUCHSTABEN, opt.get().getCorrespondingWorkbookKategorie());
	}

	@Test
	public void erzeugeStrategie_returns_empty() {
		// Arrange
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();
		Mockito.when(kategorisierer.errateKategorie(workbook)).thenReturn(WorkbookKategorie.NICHT_ENTSCHEIDBAR);

		// Act
		final Optional<ILoesungszettelBerechnungsstrategie> opt = factory.erzeugeStrategie(workbook);

		// Assert
		assertFalse(opt.isPresent());
	}

	@Test
	public void erzeugeStrategie_throws_NullPointerException() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			factory.erzeugeStrategie(null);
		});
		assertEquals("Parameter workbook", ex.getMessage());
	}
}
