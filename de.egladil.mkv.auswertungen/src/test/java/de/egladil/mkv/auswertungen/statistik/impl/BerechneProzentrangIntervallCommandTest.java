//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungItem;
import de.egladil.mkv.auswertungen.domain.Punktintervall;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * BerechneProzentrangIntervallCommandTest
 */
public class BerechneProzentrangIntervallCommandTest {

	private List<GesamtpunktverteilungItem> alleItems;

	private List<Punktintervall> punktintervalle;

	private int gesamt = 6054;

	private BerechneProzentrangIntervallCommand command;

	@BeforeEach
	public void setUp() {
		command = new BerechneProzentrangIntervallCommand();
		final IPunktintervallFactory punktintervallFactory = PunktintervallFactory.createFactory(Klassenstufe.ZWEI, "2018");
		punktintervalle = punktintervallFactory.getPunktintervalleDescending();
		alleItems = new ArrayList<>();
		for (int i = 0; i < punktintervalle.size(); i++) {
			final GesamtpunktverteilungItem item = new GesamtpunktverteilungItem();
			item.setPunktintervall(punktintervalle.get(i));
			switch (i) {
			case 0:
				item.setAnzahl(12);
				break;
			case 1:
				item.setAnzahl(9);
				break;
			case 2:
				item.setAnzahl(41);
				break;
			case 3:
				item.setAnzahl(148);
				break;
			case 4:
				item.setAnzahl(301);
				break;
			case 5:
				item.setAnzahl(497);
				break;
			case 6:
				item.setAnzahl(696);
				break;
			case 7:
				item.setAnzahl(795);
				break;
			case 8:
				item.setAnzahl(823);
				break;
			case 9:
				item.setAnzahl(800);
				break;
			case 10:
				item.setAnzahl(686);
				break;
			case 11:
				item.setAnzahl(543);
				break;
			case 12:
				item.setAnzahl(398);
				break;
			case 13:
				item.setAnzahl(211);
				break;
			case 14:
				item.setAnzahl(72);
				break;
			case 15:
				item.setAnzahl(22);
				break;
			default:
				break;
			}
			alleItems.add(item);
		}
	}

	@Test
	public void berechneKumulierteHaeufigkeit_klappt() {
		final List<Integer> expectedValues = Arrays
			.asList(new Integer[] { 6054, 6042, 6033, 5992, 5844, 5543, 5046, 4350, 3555, 2732, 1932, 1246, 703, 305, 94, 22 });

		for (int i = punktintervalle.size() - 1; i >= 0; i--) {
			final Punktintervall referenzintervall = punktintervalle.get(i);
			final int expected = expectedValues.get(i);

			// Act
			final int actual = command.berechneKumulierteHaeufigkeit(referenzintervall, alleItems);

			// Assert
			assertEquals("Fehler bei " + i, expected, actual);
		}
	}

	@Test
	public void berechne_klappt() {
		command.berechne(alleItems, gesamt);
	}
}
