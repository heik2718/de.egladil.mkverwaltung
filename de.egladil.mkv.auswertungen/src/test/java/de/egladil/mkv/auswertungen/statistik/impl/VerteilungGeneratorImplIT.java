//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.auswertungen.AbstractGuiceIT;
import de.egladil.mkv.auswertungen.domain.Gesamtpunktverteilung;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungTexte;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * VerteilungGeneratorImplIT
 */
public class VerteilungGeneratorImplIT extends AbstractGuiceIT {

	@Inject
	private ILoesungszettelDao loesungszettelDao;

	private JAXBContext jaxbContext;

	private GesamtpunktverteilungTexte texte;

	/**
	 * @see de.egladil.mkv.auswertungen.AbstractGuiceIT#setUp()
	 */
	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		texte = new GesamtpunktverteilungTexte();
		texte.setBasis("2526");
		texte.setBewertung("gagsuidgao");
		texte.setSectionEinzelergebnisse("Einzelergebnisse");
		texte.setTitel("Gesamtpuknktverteilun 2017 Klassenstufe 1");

		try {
			jaxbContext = JAXBContext.newInstance(Gesamtpunktverteilung.class);
		} catch (final JAXBException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void berechneGesamtpunktverteilungDaten_klasse_1() throws Exception {
		// Arrange
		final List<Loesungszettel> treffer = loesungszettelDao.findByJahrAndKlassenstufe("2017", Klassenstufe.EINS);
		assertFalse("Testsetting", treffer.isEmpty());

		final VerteilungGeneratorImpl verteilungGenerator = new VerteilungGeneratorImpl();
		final List<ILoesungszettel> loesungszettel = new ArrayList<>();
		loesungszettel.addAll(treffer);

		// Act
		final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(loesungszettel, Klassenstufe.EINS,
			"2017");

		//
		assertNotNull(daten);
		assertNotNull(daten.getMedian());
		final Gesamtpunktverteilung gesamtpunktverteilung = new Gesamtpunktverteilung(texte, daten);

		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
		marshaller.marshal(gesamtpunktverteilung, System.out);
	}

	@Test
	public void berechneGesamtpunktverteilungDaten_klasse_2() throws Exception {
		// Arrange
		final List<Loesungszettel> treffer = loesungszettelDao.findByJahrAndKlassenstufe("2017", Klassenstufe.ZWEI);
		assertFalse("Testsetting", treffer.isEmpty());

		final VerteilungGeneratorImpl verteilungGenerator = new VerteilungGeneratorImpl();
		final List<ILoesungszettel> loesungszettel = new ArrayList<>();
		loesungszettel.addAll(treffer);

		// Act
		final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(loesungszettel, Klassenstufe.ZWEI,
			"2017");

		//
		assertNotNull(daten);
		assertNotNull(daten.getMedian());
		final Gesamtpunktverteilung gesamtpunktverteilung = new Gesamtpunktverteilung(texte, daten);

		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
		marshaller.marshal(gesamtpunktverteilung, System.out);
	}
}
