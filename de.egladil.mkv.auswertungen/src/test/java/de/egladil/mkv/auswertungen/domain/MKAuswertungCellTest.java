//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;


/**
 * MKAuswertungCellTest
 */
public class MKAuswertungCellTest {

	@Test
	public void zellen_equal_wenn_address_equal() {
		// Arrange
		final MKCellAddress cellAddress = new MKCellAddress(2, 1);
		final MKAuswertungCell cell1 = new MKAuswertungCell(cellAddress, "bla");
		final MKAuswertungCell cell2 = new MKAuswertungCell(cellAddress, "blubb");

		// Assert
		assertEquals(cell1, cell2);
		assertTrue(cell1.hashCode() == cell2.hashCode());
	}

	@Test
	public void zellen_not_equal_wenn_address_not_equal() {
		// Arrange
		final MKAuswertungCell cell1 = new MKAuswertungCell(new MKCellAddress(2, 3), "bla");
		final MKAuswertungCell cell2 = new MKAuswertungCell(new MKCellAddress(2, 1), "blubb");

		assertNotNull(cell1.getCellAddress());
		assertNotNull(cell2.getCellAddress());

		// Assert
		assertFalse(cell1.equals(cell2));
	}

	@Test
	public void zellen_equal_wenn_gleiches_objekt() {
		final MKAuswertungCell cell1 = new MKAuswertungCell(new MKCellAddress(2, 3), "bla");
		final MKAuswertungCell cell2 = cell1;

		// Assert
		assertEquals(cell1, cell1);
		assertFalse(cell1.equals(null));
		assertFalse(new Object().equals(cell1));
		assertFalse(cell1.equals(new Object()));
		assertTrue(cell1.equals(cell2));
	}

	@Test
	public void constructor_trims_blank() {
		// Arrange
		final MKAuswertungCell cell1 = new MKAuswertungCell(new MKCellAddress(2, 3), " ");

		// Assert
		assertEquals("", cell1.getContents().get());
	}

	@Test
	public void constructor_trims_not_blank() {
		// Arrange
		final MKAuswertungCell cell1 = new MKAuswertungCell(new MKCellAddress(2, 3), " f ");

		// Assert
		assertEquals("f", cell1.getContents().get());
	}

	@Test
	public void cellNotEmpty_when_contents() {
		// Arrange
		final MKAuswertungCell cell1 = new MKAuswertungCell(new MKCellAddress(2, 3), "f");

		// Assert
		assertFalse(cell1.isEmpty());
	}

	@Test
	public void constructor_maps_null_to_blank() {
		// Arrange
		final MKAuswertungCell cell1 = new MKAuswertungCell(new MKCellAddress(2, 3), null);

		// Assert
		assertEquals("", cell1.getContents().get());
	}
}
