//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Wertung;

/**
 * BerechneAufgabenErgebnisCommandTest
 */
public class BerechneAufgabenErgebnisCommandTest {

	private final BerechneAufgabenErgebnisCommand command = new BerechneAufgabenErgebnisCommand();

	@BeforeEach
	public void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
	}

	private class LoesungszettelAdapter implements ILoesungszettel {

		private final Integer punkte;

		private final String wertungscode;

		private final Integer kaengurusprung;

		/**
		 * Erzeugt eine Instanz von LoesungszettelAdapter
		 */
		public LoesungszettelAdapter(final Integer punkte, final String wertungscode, final Integer kaengurusprung) {
			this.punkte = punkte;
			this.wertungscode = wertungscode;
			this.kaengurusprung = kaengurusprung;
		}

		@Override
		public Integer getPunkte() {
			return punkte;
		}

		@Override
		public String getWertungscode() {
			return wertungscode;
		}

		@Override
		public Integer getKaengurusprung() {
			return kaengurusprung;
		}

	}

	@Test
	void getWertungscode_throws_IndexOutOfBounds_index_negative() {
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			command.getWertungscode(new LoesungszettelAdapter(375, "ffffffffffff", 0), -1);
		});
		assertEquals("-1", ex.getMessage());

	}

	@Test
	void getWertungscode_throws_IndexOutOfBounds_index_zu_gross() {
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			command.getWertungscode(new LoesungszettelAdapter(375, "ffffffffffff", 0), 12);
		});
		assertEquals("12", ex.getMessage());

	}

	@Test
	void getWertungscode_klappt_anfang() {
		// Arrange
		final Wertung expected = Wertung.n;

		// Act
		final Wertung actual = command.getWertungscode(new LoesungszettelAdapter(375, "nfffffffffff", 0), 0);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getWertungscode_klappt_mitte() {
		// Arrange
		final Wertung expected = Wertung.r;

		// Act
		final Wertung actual = command.getWertungscode(new LoesungszettelAdapter(375, "fffrffffffff", 1), 3);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getWertungscode_klappt_ende() {
		// Arrange
		final Wertung expected = Wertung.n;

		// Act
		final Wertung actual = command.getWertungscode(new LoesungszettelAdapter(375, "fffffffffffn", 0), 11);

		// Assert
		assertEquals(expected, actual);
	}

	@Ignore
	void fehlenderTest() {
		fail("Tests für Aufgbenbewertung leicht, mittel, schwer fehlt");
	}
}
