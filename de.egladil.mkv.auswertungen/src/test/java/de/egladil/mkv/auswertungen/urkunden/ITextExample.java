//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundenmotivProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.utils.UrkundeConstants;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * ITextExample
 */
public class ITextExample {

	// rand sollte ca. 50pt werden. Danach kann kindVornameNachname bzw. schulname vergrößert oder - kleinert werden

	// maximale Breite eine Zeile = 320

	@Test
	public void createPdf() throws FileNotFoundException, DocumentException, IOException {
		final UrkundenmotivProvider backgroundProvider = DefaultUrkundenmotivProvider.ausFarbschema(Farbschema.GREEN);
		final String wettbewerbsjahr = "2018";
		final String datum = "März 2018";
		final String schule = "Montessori Grundschule Königs Wusterhausen"; // 30 Zeichen, dann umbrechen, max 60
		final String kindNameVorname = "Universums aus denen alle chemisch en Elemente";// "Alexander Maxi
																						// Weidenmueller"; //getName(new
																						// FontCalulator().maximaleAnzahlZeichen(sizeName));
		System.out.println("Kind Name Länge=" + kindNameVorname.length());

		final String klasse = "2a";
		final String punkte = "28,75";
		// Zeichen

		// 24 M passen nicht mehr mit font 18
		// final String kindNameVorname = "Emilia Clara Müller"; // "MMM MMMMMMMMMMMMMMMMMM";// 24 Zeichen,
		// dann umbrechen, max 40 Zeichen
		final int sizeName = UrkundeConstants.SIZE_NAME_SMALL;

		try (InputStream in = backgroundProvider.getBackgroundImage()) {

			final PdfReader reader = new PdfReader(in);

			final ByteArrayOutputStream output = new ByteArrayOutputStream();

			// final PdfStamper stamper = new PdfStamper(reader, new
			// FileOutputStream("/home/heike/temp/iTextHelloWorld.pdf"));
			final PdfStamper stamper = new PdfStamper(reader, output);
			final Rectangle page = PageSize.A4;
			System.out.println(page.toString());

			final PdfContentByte over = stamper.getOverContent(1);
			Phrase p = new Phrase("Beim Mathewettbewerb", UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_SMALL));
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_LEFT, UrkundeConstants.Y_TOP, 0);

			Font f = UrkundeConstants.getFont(UrkundeConstants.SIZE_TITLE, backgroundProvider);
			p = new Phrase("Minikänguru", f);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 110, 0);
			System.out.println("Minikänguru: " + (UrkundeConstants.Y_TOP - 110));

			p = new Phrase(wettbewerbsjahr, f);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 160, 0);
			System.out.println(wettbewerbsjahr + ": " + (UrkundeConstants.Y_TOP - 160));

			f = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL);
			p = new Phrase("hat", f);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 230, 0);
			System.out.println("hat: " + (UrkundeConstants.Y_TOP - 230));

			final Font fontName = UrkundeConstants.getFontBlack(sizeName);
			Chunk chunk = new Chunk(kindNameVorname, fontName);
			float width = chunk.getWidthPoint();
			System.out.println("Breite Kind " + width);
			p = new Phrase(chunk);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 280, 0);
			System.out.println(kindNameVorname + ": " + (UrkundeConstants.Y_TOP - 280));

			f = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL);
			p = new Phrase("Klasse " + klasse, f);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 310, 0);
			System.out.println(klasse + ": " + (UrkundeConstants.Y_TOP - 310));

			chunk = new Chunk(schule, f);
			width = chunk.getWidthPoint();
			System.out.println("Breite Schule " + width);
			p = new Phrase(chunk);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 340, 0);
			System.out.println(schule + ": " + (UrkundeConstants.Y_TOP - 340));

			p = new Phrase(punkte + " Punkte", fontName);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 430, 0);
			System.out.println(punkte + ": " + (UrkundeConstants.Y_TOP - 430));

			f = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL);
			p = new Phrase("erreicht", f);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_CENTER, UrkundeConstants.Y_TOP - 460, 0);
			System.out.println("erreicht: " + (UrkundeConstants.Y_TOP - 460));

			f = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_SMALL);
			p = new Phrase(datum, f);
			// darf sich nicht nach unten verscieben!
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, p, UrkundeConstants.X_LEFT, UrkundeConstants.Y_BOTTOM, 0);
			System.out.println(datum + ": " + (UrkundeConstants.Y_BOTTOM));

			stamper.close();
			reader.close();

			final byte[] data = output.toByteArray();

			try (final ByteArrayInputStream bis = new ByteArrayInputStream(data);
				final FileOutputStream fos = new FileOutputStream("/home/heike/temp/urkunde.pdf")) {
				IOUtils.copy(bis, fos);
				fos.flush();
			}
		}
	}
}
