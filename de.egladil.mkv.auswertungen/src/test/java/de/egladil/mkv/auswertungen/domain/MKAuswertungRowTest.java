//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * MKAuswertungRowTest
 */
public class MKAuswertungRowTest {

	@Test
	public void rows_are_equal_when_index_equal() {
		// Arrange
		final MKAuswertungRow row1 = TestHelper.getKlasse1EinName();
		final MKAuswertungRow row2 = TestHelper.getKlasse1EinName();
		row2.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
		assertEquals("Testsetting: arrangement nicht korrekt", row1.getIndex(), row2.getIndex());

		// Assert
		assertEquals(row1, row2);
		assertEquals(row1.hashCode(), row2.hashCode());
	}

	@Test
	public void rows_are_not_equal_when_index_not_equal() {
		// Arrange
		final MKAuswertungRow row1 = TestHelper.getKlasse1EinName();
		final MKAuswertungRow row = new MKAuswertungRow(17, Klassenstufe.EINS, 1);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo Dri"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "r"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "F"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "f"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "n"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "r"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "f"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "r"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "f"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "n"));
		assertFalse("Testsetting: arrangement nicht korrekt", row1.getIndex() == row.getIndex());

		// Assert
		assertFalse(row1.equals(row));
		assertFalse(row1.hashCode() == row.hashCode());
	}

	@Test
	public void row_equals_itself() {
		final MKAuswertungRow row1 = TestHelper.getKlasse1EinName();

		assertEquals(row1, row1);
		assertFalse(row1.equals(null));
		assertFalse(row1.equals(new Object()));
		assertFalse(new Object().equals(row1));
	}

	@Test
	public void addCell_throws_when_index_too_large() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			row.addCell(13, new MKAuswertungCell(new MKCellAddress(13, 13), "evil"));
		});
		assertEquals("index muss kleiner als 13 und größer als 0 sein. [index=13]", ex.getMessage());

	}

	@Test
	public void addCell_throws_when_index_too_low() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();

		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			row.addCell(-1, new MKAuswertungCell(new MKCellAddress(13, 13), "evil"));
		});
		assertEquals("index muss kleiner als 13 und größer als 0 sein. [index=-1]", ex.getMessage());

	}

	@Test
	public void getCellContent_throws_when_index_too_large() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();

		// Act + Assert
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			row.getCellContent(13);
		});
		assertEquals("index muss kleiner als 13 und größer als 0 sein. [index=13]", ex.getMessage());

	}

	@Test
	public void getCellContent_throws_when_index_too_low() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();

		// Act + Assert
		final Throwable ex = assertThrows(IndexOutOfBoundsException.class, () -> {
			row.getCellContent(-1);
		});
		assertEquals("index muss kleiner als 13 und größer als 0 sein. [index=-1]", ex.getMessage());

	}

	@Test
	public void addCell_tauscht_aus() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();
		final Optional<String> contents = row.getCellContent(4);
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "hehe"));

		// Act
		final Optional<String> actual = row.getCellContent(4);

		// Assert
		assertFalse(contents.get().equals(actual.get()));
		System.out.println(contents.get());
		System.out.println(actual.get());
	}

	@Test
	public void prettyPrint_ersetzt_null_durch_minus() {
		// Arrange
		final MKAuswertungRow row = TestHelper.getKlasse1EinName();
		row.addCell(4, null);

		// Assert
		assertEquals(" 13 : Hallo Dri r r F - n r f f r r f n ", row.prettyPrint());
	}

	@Test
	public void empty_when_alle_zellen_empty() {
		final MKAuswertungRow row = new MKAuswertungRow(17, Klassenstufe.EINS, 1);
		assertTrue(row.isEmpty());

		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), ""));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), null));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), ""));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), ""));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), ""));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), ""));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), ""));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), null));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), ""));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), ""));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), ""));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), ""));

		// Assert
		assertTrue(row.isEmpty());
	}

	@Test
	public void empty_when_eine_zelle_nicht_empty() {
		final MKAuswertungRow row = new MKAuswertungRow(17, Klassenstufe.EINS, 1);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), ""));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), null));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), ""));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "f"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), ""));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), ""));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), ""));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), null));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), ""));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), ""));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), ""));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), ""));

		// Assert
		assertFalse(row.isEmpty());
	}

	@Test
	public void toWertungsrelevanteZellen_kein_name() {
		// Arrange
		final MKAuswertungRow row = new MKAuswertungRow(17, Klassenstufe.EINS, 0);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "a"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "b"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "c"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "d"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "e"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "f"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "g"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "h"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "i"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "j"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "k"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "l"));

		// Act
		final MKAuswertungCell[] wertungszellen = row.toWertungsrelevanteZellen();

		// Assert
		assertEquals("a", wertungszellen[0].getContents().get());
		assertEquals("b", wertungszellen[1].getContents().get());
		assertEquals("c", wertungszellen[2].getContents().get());
		assertEquals("d", wertungszellen[3].getContents().get());
		assertEquals("e", wertungszellen[4].getContents().get());
		assertEquals("f", wertungszellen[5].getContents().get());
		assertEquals("g", wertungszellen[6].getContents().get());
		assertEquals("h", wertungszellen[7].getContents().get());
		assertEquals("i", wertungszellen[8].getContents().get());
		assertEquals("j", wertungszellen[9].getContents().get());
		assertEquals("k", wertungszellen[10].getContents().get());
		assertEquals("l", wertungszellen[11].getContents().get());
	}

	@Test
	public void toWertungsrelevanteZellen_ein_name() {
		// Arrange
		final MKAuswertungRow row = new MKAuswertungRow(17, Klassenstufe.EINS, 1);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Anna"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "a"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "b"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "c"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "d"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "e"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "f"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "g"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "h"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "i"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "j"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "k"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "l"));

		// Act
		final MKAuswertungCell[] wertungszellen = row.toWertungsrelevanteZellen();

		// Assert
		assertEquals("a", wertungszellen[0].getContents().get());
		assertEquals("b", wertungszellen[1].getContents().get());
		assertEquals("c", wertungszellen[2].getContents().get());
		assertEquals("d", wertungszellen[3].getContents().get());
		assertEquals("e", wertungszellen[4].getContents().get());
		assertEquals("f", wertungszellen[5].getContents().get());
		assertEquals("g", wertungszellen[6].getContents().get());
		assertEquals("h", wertungszellen[7].getContents().get());
		assertEquals("i", wertungszellen[8].getContents().get());
		assertEquals("j", wertungszellen[9].getContents().get());
		assertEquals("k", wertungszellen[10].getContents().get());
		assertEquals("l", wertungszellen[11].getContents().get());
	}

	@Test
	public void toWertungsrelevanteZellen_zwei_namen() {
		// Arrange
		final MKAuswertungRow row = new MKAuswertungRow(17, Klassenstufe.EINS, 2);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Anna"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "Susanna"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "a"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "b"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "c"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "e"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "f"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "g"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "h"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "i"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "j"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "k"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "l"));

		// Act
		final MKAuswertungCell[] wertungszellen = row.toWertungsrelevanteZellen();

		// Assert
		assertEquals("a", wertungszellen[0].getContents().get());
		assertEquals("b", wertungszellen[1].getContents().get());
		assertEquals("c", wertungszellen[2].getContents().get());
		assertEquals("d", wertungszellen[3].getContents().get());
		assertEquals("e", wertungszellen[4].getContents().get());
		assertEquals("f", wertungszellen[5].getContents().get());
		assertEquals("g", wertungszellen[6].getContents().get());
		assertEquals("h", wertungszellen[7].getContents().get());
		assertEquals("i", wertungszellen[8].getContents().get());
		assertEquals("j", wertungszellen[9].getContents().get());
		assertEquals("k", wertungszellen[10].getContents().get());
		assertEquals("l", wertungszellen[11].getContents().get());
	}
}
