//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.auswertungen.AbstractGuiceIT;
import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * UploadProcessorIT
 */
public class UploadProcessorIT extends AbstractGuiceIT {

	private static final String PATH_UPLOAD_DIR = "/home/heike/git/konfigurationen/mkverwaltung/upload_testdateien";

	private static final String SOURCE_DATEINAME = "S_3QD4RXO9_6ce9e947018b77d2faeeecb90376e8cd9746bed0b9bb28c4d98e9217ed5b8cbd.xlsx";

	private UploadProcessor uploadProcessor;

	@Inject
	private ILoesungszettelDao loesungszettelDao;

	@Inject
	private IWettbewerbsloesungDao wettbewerbsloesungDao;

	@Inject
	private IUploadDao uploadDao;

	@Inject
	private IUploadInfoDao uploadInfoDao;

	@Inject
	private AuswertungstabelleFileWriter fileWriter;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		uploadProcessor = new UploadProcessor(loesungszettelDao, wettbewerbsloesungDao, uploadDao, uploadInfoDao, null, fileWriter);
		uploadProcessor.setNurStatusNEU(false);
	}

	@Test
	public void processUploadAndExceptions_klappt() {
		final Optional<UploadInfo> opt = uploadInfoDao.findById(UploadInfo.class, 211);
		if (!opt.isPresent()) {
			fail("Test kann nicht starten, haben keine UploadInfo");
		}

		final UploadInfo alteInfo = opt.get();

		final String sourcePath = PATH_UPLOAD_DIR + File.separator + SOURCE_DATEINAME;
		final String filePath = PATH_UPLOAD_DIR + File.separator + alteInfo.getDateiname();
		for (final UploadStatus uploadStatus : UploadStatus.values()) {
			final File file = new File(filePath + uploadStatus.getFileExtension());
			FileUtils.deleteQuietly(file);
		}
		try {
			FileUtils.copyFile(new File(sourcePath), new File(filePath));
		} catch (final IOException e) {
			fail("Testsetting: konnte keine Testdatei erzeugen");
		}

		// Act
		uploadProcessor.processUploadAndExceptions(opt.get().getId(), PATH_UPLOAD_DIR);

		// Assert
		final File successFile = new File(filePath + UploadStatus.FERTIG.getFileExtension());
		assertTrue(successFile.isFile());

		final Optional<UploadInfo> optNeu = uploadInfoDao.findById(UploadInfo.class, opt.get().getId());
		assertTrue(optNeu.isPresent());
		final UploadInfo neueInfo = optNeu.get();
		assertEquals(UploadStatus.FERTIG, neueInfo.getUploadStatus());
	}

	@Test
	public void processUploadAndExceptions_datei_nicht_gefunden() {
		final Optional<UploadInfo> opt = uploadInfoDao.findById(UploadInfo.class, 182);
		if (!opt.isPresent()) {
			fail("Test kann nicht starten, haben keine UploadInfo");
		}

		final UploadInfo alteInfo = opt.get();

		final String filePath = PATH_UPLOAD_DIR + File.separator + alteInfo.getDateiname();
		for (final UploadStatus uploadStatus : UploadStatus.values()) {
			final File file = new File(filePath + uploadStatus.getFileExtension());
			FileUtils.deleteQuietly(file);
		}

		// Act
		uploadProcessor.processUploadAndExceptions(opt.get().getId(), PATH_UPLOAD_DIR);

		// Assert
		final File successFile = new File(filePath + UploadStatus.FEHLER.getFileExtension());
		assertFalse(successFile.isFile());

		final Optional<UploadInfo> optNeu = uploadInfoDao.findById(UploadInfo.class, opt.get().getId());
		assertTrue(optNeu.isPresent());
		final UploadInfo neueInfo = optNeu.get();
		assertEquals(UploadStatus.FEHLER, neueInfo.getUploadStatus());
	}

	@Test
	public void processUploadAndExceptions_mimetype_unknown() {
		final Optional<UploadInfo> opt = uploadInfoDao.findById(UploadInfo.class, 235);
		if (!opt.isPresent()) {
			fail("Test kann nicht starten, haben keine UploadInfo");
		}

		final UploadInfo alteInfo = opt.get();

		final String filePath = PATH_UPLOAD_DIR + File.separator + alteInfo.getDateiname();
		for (final UploadStatus uploadStatus : UploadStatus.values()) {
			final File file = new File(filePath + uploadStatus.getFileExtension());
			FileUtils.deleteQuietly(file);
		}

		// Act
		uploadProcessor.processUploadAndExceptions(opt.get().getId(), PATH_UPLOAD_DIR);

		// Assert
		final File successFile = new File(filePath + UploadStatus.FEHLER.getFileExtension());
		assertFalse(successFile.isFile());

		final Optional<UploadInfo> optNeu = uploadInfoDao.findById(UploadInfo.class, opt.get().getId());
		assertTrue(optNeu.isPresent());
		final UploadInfo neueInfo = optNeu.get();
		assertEquals(UploadStatus.FEHLER, neueInfo.getUploadStatus());
	}

	@Test
	public void processUploadAndExceptions_status_nicht_neu() {
		final Optional<UploadInfo> opt = uploadInfoDao.findById(UploadInfo.class, 195);
		if (!opt.isPresent()) {
			fail("Test kann nicht starten, haben keine UploadInfo");
		}

		assertEquals("Wollen nur Status LEER testen", UploadStatus.LEER, opt.get().getUploadStatus());

		uploadProcessor.setNurStatusNEU(true);

		// Act
		uploadProcessor.processUploadAndExceptions(opt.get().getId(), PATH_UPLOAD_DIR);

		// Assert
		final Optional<UploadInfo> optNeu = uploadInfoDao.findById(UploadInfo.class, opt.get().getId());
		assertTrue(optNeu.isPresent());
		final UploadInfo neueInfo = optNeu.get();
		assertEquals(opt.get().getUploadStatus(), neueInfo.getUploadStatus());
	}
}
