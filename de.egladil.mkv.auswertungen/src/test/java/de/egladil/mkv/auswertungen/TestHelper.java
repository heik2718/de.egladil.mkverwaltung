//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mkv.auswertungen.domain.MKAuswertungCell;
import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKCellAddress;
import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenschluessel;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * TestHelper
 */
public class TestHelper {

	public static final String XLS_ZWEI_NAMEN = "/korrekt/klasse_1.xls";

	public static final String XLSX_EIN_NAME = "/korrekt/klasse_2_eine_namenspalte.xlsx";

	public static final String XLS_KEIN_NAME = "/korrekt/klasse_2_ohne_namenspalten.xls";

	public static final String ODS_ZWEI_NAMEN = "/korrekt/klasse_1.ods";

	public static final String ODS_EIN_NAME = "/korrekt/klasse_2_eine_namenspalte.ods";

	public static final String ODS_KEIN_NAME = "/korrekt/klasse_2_ohne_namenspalten.ods";

	public static final Klassenschluessel AMEISEN = new Klassenschluessel(Klassenstufe.EINS, "Ameisen");

	public static final Klassenschluessel GIRAFFEN = new Klassenschluessel(Klassenstufe.ZWEI, "Giraffen");

	public static final Klassenschluessel LOEWEN = new Klassenschluessel(Klassenstufe.EINS, "Löwen");

	public static List<String[]> getKorrektZweiNamen() {
		final List<String[]> result = new ArrayList<>();
		result.add(new String[] { "Nora", "Peters", "f", "f", "f", "r", "f", "r", "r", "r", "r", "r", "r", "r" });
		result.add(new String[] { "Karl", "Weber", "r", "f", "f", "r", "r", "f", "r", "r", "f", "r", "r", "f" });
		result.add(new String[] { "Sebastian", "Schulz", "r", "f", "f", "f", "r", "f", "r", "r", "f", "r", "r", "r" });
		result.add(new String[] { "Alfred", "Stark", "r", "f", "f", "f", "r", "f", "r", "r", "f", "r", "r", "f" });
		result.add(new String[] { "Annegret", "Müller", "r", "f", "f", "r", "f", "f", "r", "r", "r", "f", "f", "r" });
		result.add(new String[] { "Jana", "Maier", "f", "f", "r", "r", "f", "r", "f", "r", "f", "r", "r", "f" });
		result.add(new String[] { "Imre", "Günes", "r", "f", "f", "r", "r", "f", "r", "r", "f", "r", "f", "f" });
		result.add(new String[] { "Max,Frisch", "f", "f", "r", "r", "r", "f", "r", "r", "f", "", "f", "r" });
		result.add(new String[] { "Stanislav,Lem", "f", "r", "r", "r", "r", "f", "f", "f", "r", "f", "f", "r" });
		result.add(new String[] { "Andreas,Kaspar", "r", "f", "f", "f", "r", "f", "r", "f", "f", "r", "f", "r" });
		result.add(new String[] { "Sophie,Vogel", "f", "f", "r", "r", "r", "f", "f", "f", "f", "r", "f", "f" });
		result.add(new String[] { "Sarah,Baum", "r", "f", "f", "r", "n", "r", "f", "r", "f", "f", "f", "f" });
		result.add(new String[] { "Musab,", "f", "f", "f", "f", "r", "f", "r", "f", "f", "f", "f", "f" });
		result.add(new String[] { "Benjamin", "", "f", "f", "f", "r", "f", "f", "f", "f", "f", "f", "f", "r" });
		result.add(new String[] { "Maxwell Craig", "", "f", "f", "f", "r", "f", "f", "f", "r", "f", "f", "f", "f" });
		return result;
	}

	public static List<String[]> getKorrektEinName() {
		final List<String[]> result = new ArrayList<>();
		result.add(new String[] { "Hans Wurst", "r", "", "f", "f", "f", "f", "f", "r", "f", "f", "r", "f", "f", "f", "f" });
		result.add(new String[] { "Gabi Fischer", "r", "r", "f", "f", "r", "r", "f", "f", "r", "r", "n", "n", "n", "n", "f" });
		result.add(new String[] { "Robert Karl", "r", "r", "f", "r", "f", "r", "r", "f", "f", "r", "r", "f", "r", "r", "f" });
		result.add(new String[] { "Rosi Grünwald", "r", "r", "f", "f", "f", "r", "n", "f", "n", "n", "f", "f", "f", "f", "f" });
		result.add(new String[] { "Jacob Tate", "f", "r", "r", "r", "n", "n", "n", "n", "n", "n", "n", "n", "n", "n", "n" });
		result.add(new String[] { "Justin Clark", "r", "r", "f", "n", "f", "f", "n", "f", "f", "n", "n", "f", "n", "f", "f" });
		result.add(new String[] { "Tamara Juul", "r", "f", "f", "r", "f", "r", "f", "r", "f", "f", "f", "f", "f", "f", "f" });
		result.add(new String[] { "Sarah Wagner", "r", "f", "r", "n", "f", "r", "n", "f", "f", "f", "n", "f", "n", "f", "f" });
		result.add(new String[] { "Tim Mayer", "r", "r", "f", "f", "f", "f", "f", "f", "r", "n", "n", "f", "f", "f", "f" });

		return result;
	}

	public static List<String[]> getKorrektKeinName() {
		final List<String[]> result = new ArrayList<>();
		result.add(new String[] { "r", "f", "f", "f", "f", "f", "f", "r", "f", "f", "r", "f", "f", "f", "f" });
		result.add(new String[] { "r", "r", "f", "f", "r", "r", "f", "f", "r", "r", "n", "n", "n", "n", "f" });
		result.add(new String[] { "r", "r", "f", "r", "f", "r", "r", "f", "f", "r", "r", "f", "r", "r", "f" });
		result.add(new String[] { "r", "r", "f", "f", "f", "r", "n", "f", "n", "n", "f", "f", "f", "f", "f" });
		result.add(new String[] { "f", "r", "r", "r", "n", "n", "n", "n", "n", "n", "n", "n", "n", "n", "n" });
		result.add(new String[] { "r", "r", "f", "n", "f", "f", "n", "f", "f", "n", "n", "f", "n", "f", "f" });
		result.add(new String[] { "r", "f", "f", "r", "f", "r", "f", "r", "f", "f", "f", "f", "f", "f", "f" });
		result.add(new String[] { "r", "f", "r", "", "f", "r", "n", "f", "f", "f", "n", "f", "n", "f", "f" });
		result.add(new String[] { "r", "r", "f", "f", "f", "f", "f", "f", "r", "n", "n", "f", "f", "f", "f" });
		return result;
	}

	/**
	 * rrFfnrffrrfnnrn
	 */
	public static MKAuswertungRow getKlasse2KeinName() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.ZWEI, 0);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "r"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "r"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "F"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "f"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "r"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "f"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "f"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "r"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "r"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "f"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "n"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "r"));
		row.addCell(14, new MKAuswertungCell(new MKCellAddress(0, 14), "n"));
		return row;
	}

	/**
	 * rrFfnrffrrfnnrn
	 */
	public static MKAuswertungRow getKlasse2EinName() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.ZWEI, 1);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo Dri"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "r"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "F"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "f"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "n"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "r"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "f"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "r"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "f"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "n"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "n"));
		row.addCell(14, new MKAuswertungCell(new MKCellAddress(0, 14), "r"));
		row.addCell(15, new MKAuswertungCell(new MKCellAddress(0, 15), "n"));
		return row;
	}

	/**
	 * rrFfnrffrrfnnrn
	 */
	public static MKAuswertungRow getKlasse2ZweiNamen() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.ZWEI, 2);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "Dri"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "r"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "F"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "f"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "r"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "f"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "f"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "n"));
		row.addCell(14, new MKAuswertungCell(new MKCellAddress(0, 14), "n"));
		row.addCell(15, new MKAuswertungCell(new MKCellAddress(0, 15), "r"));
		row.addCell(16, new MKAuswertungCell(new MKCellAddress(0, 16), "n"));
		return row;
	}

	/**
	 * rrFfnrffrrfn
	 */
	public static MKAuswertungRow getKlasse1KeinName() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "r"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "r"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "F"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "f"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "r"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "f"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "f"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "r"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "r"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "f"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));
		return row;
	}

	/**
	 * rrFfnrffrrfn
	 */
	public static MKAuswertungRow getKlasse1EinName() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 1);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo Dri"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "r"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "F"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "f"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "n"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "r"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "f"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "r"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "f"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "n"));
		return row;
	}

	/**
	 * rrFfnrffrrfn
	 */
	public static MKAuswertungRow getKlasse1ZweiNamen() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 2);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "Dri"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "r"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "F"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "f"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "r"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "f"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "f"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "n"));
		return row;
	}

	/**
	 * rrFfnrffrrfm => rrffnrffrrfn
	 */
	public static MKAuswertungRow getKlasse1ZweiNamenUndTypo() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 2);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "Dri"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "r"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "F"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "f"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "r"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "f"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "f"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "m"));
		return row;
	}

	/**
	 * nrFfnrffrrfn
	 */
	public static MKAuswertungRow getKlasse1ZweiNameMitNullcontent() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 2);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "Dri"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), null));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "r"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "F"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "f"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "r"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "f"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "f"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "n"));
		return row;
	}

	/**
	 * rnFfnrffrrfn
	 */
	public static MKAuswertungRow getKlasse1ZweiNameMitNullcell() {
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 2);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "Hallo"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "Dri"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "r"));
		row.addCell(3, null);
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "F"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "f"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "r"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "f"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "f"));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "r"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));
		row.addCell(12, new MKAuswertungCell(new MKCellAddress(0, 12), "f"));
		row.addCell(13, new MKAuswertungCell(new MKCellAddress(0, 13), "n"));
		return row;
	}

	/**
	 * Gibt dem Pfad zum Dev-Config-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevNewsletterDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/files/mails";
		}
		return "/home/heike/git/konfigurationen/mkverwaltung/files/mkvadmin/mails";
	}

	/**
	 * Gibt dem Pfad zum Dev-Upload-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevUploadDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/upload/mkv";
		}
		return "/home/heike/git/konfigurationen/mkverwaltung/files/mkvadmin/upload";
	}

	/**
	 * Gibt dem Pfad zum Dev-Upload-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevSandboxDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/upload/sandbox";
		}
		return "/home/heike/upload/sandbox";
	}

	public static List<TeilnehmerUndPunkte> getSchulrohdaten() {
		final List<TeilnehmerUndPunkte> alleTeilnehmer = new ArrayList<>();

		{
			final TeilnehmerUndPunkte tup = new TeilnehmerUndPunkte("Sonja Winter", "44,5", 4450, 4, Klassenstufe.ZWEI, Sprache.en);
			tup.setKlassenname(GIRAFFEN.getKlassenname());
			tup.setAntwortcode("ABDEDNAECCNNABD");
			tup.setBerechneterWertungscode("rrrrfnrrfrnnfff");
			alleTeilnehmer.add(tup);
		}
		{
			final TeilnehmerUndPunkte tup = new TeilnehmerUndPunkte("Zacharias Meier", "34,5", 3450, 4, Klassenstufe.EINS, Sprache.de);
			tup.setKlassenname(AMEISEN.getKlassenname());
			tup.setAntwortcode("EDNAECCNNABD");
			tup.setBerechneterWertungscode("rrrrfnrrfrnn");
			alleTeilnehmer.add(tup);
		}
		{
			final TeilnehmerUndPunkte tup = new TeilnehmerUndPunkte("Alina Franz", "27.55", 2755, 3, Klassenstufe.EINS, Sprache.de);
			tup.setKlassenname(AMEISEN.getKlassenname());
			tup.setAntwortcode("ADNAECCNNABD");
			tup.setBerechneterWertungscode("frrrfnrrfrnn");
			alleTeilnehmer.add(tup);
		}
		{
			final TeilnehmerUndPunkte tup = new TeilnehmerUndPunkte("Heiner Baum", "32.25", 3225, 3, Klassenstufe.EINS, Sprache.de);
			tup.setKlassenname(LOEWEN.getKlassenname());
			tup.setAntwortcode("ADNAECCNNABD");
			tup.setBerechneterWertungscode("frrrfnrrfrnn");
			alleTeilnehmer.add(tup);
		}

		return alleTeilnehmer;
	}




}
