//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.auswertungen.AbstractGuiceIT;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;

/**
 * SchulstatistikServiceImplIT
 */
public class SchulstatistikServiceImplIT extends AbstractGuiceIT {

	@Inject
	private SchulstatistikService service;

	@Test
	void generiereAuswertungKlappt() {
		// Arrange
		final String kuerzel = "YHHFQEDI20171228142026";
		final String benutzerUuid = "iche";

		// Act
		final Optional<String> optCode = service.generiereStatistikAktuellesJahr(benutzerUuid, kuerzel);

		// Assert
		assertTrue(optCode.isPresent());
	}

}
