//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundeInhaltBuilder;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitText;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitTextBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * TeilnehmerUrkundeKlassennameComparatorTest
 */
public class TeilnehmerUrkundeKlassenstufeComparatorTest {

	private UrkundenmotivMitText urkundenmotiv;

	private TeilnehmerUrkundeKlassenstufeComparator comparator;

	@BeforeEach
	void setUp() {
		urkundenmotiv = new UrkundenmotivMitTextBuilder("2018", Mockito.mock(UrkundenmotivProvider.class), "21.11.2017").build();
		comparator = new TeilnehmerUrkundeKlassenstufeComparator();
	}

	@Test
	@DisplayName("gleiche Klasse, gleicher Klassenname, gleiche Punktzahl alphabetisch nach Name")
	void sortieren1() {
		final TeilnehmerUndPunkte teilnehmerUndPunkte1 = new TeilnehmerUndPunkte("Anna Johanna", "21,5", 2150, 3, Klassenstufe.EINS,
			Sprache.de);
		final DefaultUrkundeInhalt urkundeInhalt1 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte1).klassenname(Klassenstufe.EINS.getLabel()).build();

		final TeilnehmerUndPunkte teilnehmerUndPunkte2 = new TeilnehmerUndPunkte("Anna Amalia", "21,5", 2150, 1, Klassenstufe.EINS,
			Sprache.en);
		final DefaultUrkundeInhalt urkundeInhalt2 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte2).klassenname(Klassenstufe.EINS.getLabel()).build();

		assertTrue(comparator.compare(urkundeInhalt1, urkundeInhalt2) > 0);

	}

	@Test
	@DisplayName("gleiche Klasse, gleicher Name, gleiche Punktzahl, verschiedene Klassennamen")
	void sortieren2() {
		final TeilnehmerUndPunkte teilnehmerUndPunkte1 = new TeilnehmerUndPunkte("Anna Johanna", "21,5", 2150, 3, Klassenstufe.EINS,
			Sprache.de);
		final DefaultUrkundeInhalt urkundeInhalt1 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte1).klassenname("Schnecken").build();

		final TeilnehmerUndPunkte teilnehmerUndPunkte2 = new TeilnehmerUndPunkte("Anna Johanna", "21,5", 2150, 3, Klassenstufe.EINS,
			Sprache.en);
		final DefaultUrkundeInhalt urkundeInhalt2 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte2).klassenname("Füchse").build();

		assertTrue(comparator.compare(urkundeInhalt1, urkundeInhalt2) > 0);

	}

	@Test
	@DisplayName("gleiche Klasse, gleicher Name, gleiche Punktzahl, gleiche Klassennamen, verschiedene Zusatz")
	void sortieren3() {
		final TeilnehmerUndPunkte teilnehmerUndPunkte1 = new TeilnehmerUndPunkte("Anna Johanna", "21,5", 2150, 3, Klassenstufe.EINS,
			Sprache.en);
		final DefaultUrkundeInhalt urkundeInhalt1 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte1).klassenname("Füchse").build();

		final TeilnehmerUndPunkte teilnehmerUndPunkte2 = new TeilnehmerUndPunkte("Anna Johanna", "21,5", 2150, 3, Klassenstufe.EINS,
			Sprache.de);
		teilnehmerUndPunkte2.setZusatz("Fenster");
		final DefaultUrkundeInhalt urkundeInhalt2 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte2).klassenname("Füchse").build();

		assertTrue(comparator.compare(urkundeInhalt1, urkundeInhalt2) < 0);

	}

	@Test
	@DisplayName("gleiche Klasse, gleicher Klassenname, gleiche Namen, verschiedene Punktzahl alphabetisch nach Name")
	void sortieren4() {
		final TeilnehmerUndPunkte teilnehmerUndPunkte1 = new TeilnehmerUndPunkte("Anna Amalia", "21,75", 2175, 3, Klassenstufe.EINS,
			Sprache.de);
		final DefaultUrkundeInhalt urkundeInhalt1 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte1).klassenname(Klassenstufe.EINS.getLabel()).build();

		final TeilnehmerUndPunkte teilnehmerUndPunkte2 = new TeilnehmerUndPunkte("Anna Amalia", "21,5", 2150, 3, Klassenstufe.EINS,
			Sprache.en);
		final DefaultUrkundeInhalt urkundeInhalt2 = new DefaultUrkundeInhaltBuilder(urkundenmotiv)
			.teilnehmerUndPunkte(teilnehmerUndPunkte2).klassenname(Klassenstufe.EINS.getLabel()).build();

		assertTrue(comparator.compare(urkundeInhalt1, urkundeInhalt2) < 0);
	}
}
