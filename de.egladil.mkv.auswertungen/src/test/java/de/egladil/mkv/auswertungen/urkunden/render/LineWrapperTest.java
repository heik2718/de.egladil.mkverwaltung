//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * LineWrapperTest
 */
@Ignore
public class LineWrapperTest {

	private static final String EINE_ZEILE_LARGE = "Universums aus denen alle che";

	private static final String EINE_ZEILE_MEDIUM = "Universums aus denen alle chemisc";

	private static final String EINE_ZEILE_SMALL = "Universums aus denen alle chemisch en";

	private static final String EINE_ZEILE_NORMAL = "Universums aus denen alle chemischen Eleme";

	@Nested
	@DisplayName("Für Teilnehmernamen gibt es 3 mögliche Fontgrößen")
	class TeilnehmerNameTest {
		@Test
		@DisplayName("kein Umbruch")
		void nameOhneUmbruch() {
			final String[] namen = { EINE_ZEILE_LARGE, EINE_ZEILE_MEDIUM, EINE_ZEILE_SMALL, EINE_ZEILE_NORMAL };
			final List<Integer> fontSizes = new ArrayList<>();
			for (int i = 0; i < UrkundeConstants.TEILNEHMER_SIZES_DESCENDING.length; i++) {
				fontSizes.add(UrkundeConstants.TEILNEHMER_SIZES_DESCENDING[i]);
			}
			fontSizes.add(UrkundeConstants.SIZE_TEXT_NORMAL);
			final Integer[] fonts = fontSizes.toArray(new Integer[0]);
			for (final String name : namen) {
				final List<String> expected = Arrays.asList(new String[] { name });

				final List<String> zeilen = new LineWrapper(fonts).wrapp(name);
				assertThat(zeilen, is(expected));
			}
		}

		@Test
		@DisplayName("Umbruch genau Mitte")
		void umbruchAufSpace() {
			final List<String> expected = Arrays
				.asList(new String[] { "Universums aus denen alle chemisch en", "Elemente aufgebaut sind Teilchen für Te" });
			final List<String> zeilen = new LineWrapper(UrkundeConstants.TEILNEHMER_SIZES_DESCENDING)
				.wrapp("Universums aus denen alle chemisch en Elemente aufgebaut sind Teilchen für Te");
			assertThat(zeilen, is(expected));
		}

		@Test
		@DisplayName("Umbruch Mitte nicht möglich ergibt 3 Zeilen")
		void keinUmbruchMoeglich() {
			final String name = "Universums aus denen alle chemischenElemente aufgebaut sind Teilchen für Teilc";
			final List<String> expected = Arrays
				.asList(new String[] { "Universums aus denen alle", "chemischenElemente aufgebaut sind", "Teilchen für Teilc" });
			final List<String> zeilen = new LineWrapper(UrkundeConstants.TEILNEHMER_SIZES_DESCENDING).wrapp(name);
			assertThat(zeilen, is(expected));
		}
	}

	@Nested
	@DisplayName("Für Schulnamen gibt es 1 mögliche Fontgröße")
	class SchulnameTest {

		@Test
		@DisplayName("Umbruch Mitte ergibt 2 Zeilen")
		void umbruchAufSpace() {
			final List<String> expected = Arrays
				.asList(new String[] { "Universums aus denen alle chemisch en", "Elemente aufgebaut sind Teilchen für Te" });
			final List<String> zeilen = new LineWrapper(UrkundeConstants.SCHULNAME_SIZES_DESCENDING)
				.wrapp("Universums aus denen alle chemisch en Elemente aufgebaut sind Teilchen für Te");
			assertThat(zeilen, is(expected));
		}

		@Test
		@DisplayName("Umbruch Mitte nicht möglich ergibt 3 Zeilen")
		void keinUmbruchMoeglich() {
			final String name = "Universums aus denen alle chemischenElement aufgebaut sind Teilchen für Teilchen zu Kett";
			new LengthTester().checkTooLongAndThrow(name, UrkundeConstants.SIZE_TEXT_NORMAL);
			final List<String> expected = Arrays.asList(
				new String[] { "Universums aus denen alle", "chemischenElemente aufgebaut sind Teilchen", "für Teilchen zu Kett" });
			final List<String> zeilen = new LineWrapper(UrkundeConstants.SCHULNAME_SIZES_DESCENDING).wrapp(name);
			assertThat(zeilen, is(expected));
		}
	}
}
