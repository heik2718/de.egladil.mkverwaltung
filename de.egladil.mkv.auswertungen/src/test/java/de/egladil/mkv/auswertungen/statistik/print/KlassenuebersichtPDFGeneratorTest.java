//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenschluessel;
import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenuebersicht;
import de.egladil.mkv.auswertungen.statistik.gruppen.Rootgruppeuebersicht;
import de.egladil.mkv.auswertungen.statistik.gruppen.RootgruppeuebersichtCreator;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.persistence.config.KontextReader;

/**
 * KlassenuebersichtPDFGeneratorTest
 */
public class KlassenuebersichtPDFGeneratorTest {

	private static final String SCHULNAME = "Blümchenschule";

	private Rootgruppeuebersicht rootgruppeuebersicht;

	@BeforeEach
	void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		final List<TeilnehmerUndPunkte> rohdaten = TestHelper.getSchulrohdaten();
		rootgruppeuebersicht = new RootgruppeuebersichtCreator().createSchuluebersicht(rohdaten, SCHULNAME);
	}

	@Test
	void tabelleKlasse2() throws Exception {

		// Arrange
		final String path = "/home/heike/temp/klassenuebersicht-klasse2.pdf";
		final KlassenuebersichtPDFGenerator generator = new KlassenuebersichtPDFGenerator();
		final Klassenschluessel klassenschluessel = TestHelper.LOEWEN;
		final Klassenuebersicht klassenuebersicht = rootgruppeuebersicht.getKlassenubersicht(klassenschluessel);

		// Act
		final byte[] pdf = generator.generiere(klassenuebersicht);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(path)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(path).canRead());
	}

}
