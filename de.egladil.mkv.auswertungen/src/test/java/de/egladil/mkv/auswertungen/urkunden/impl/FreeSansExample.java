//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
* FreeSansExample
*/

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.persistence.utils.FontProviderWrapper;
import de.egladil.mkv.persistence.utils.FontTyp;

public class FreeSansExample {

	public static final String DEST = "/home/heike/temp/unicode.pdf";

	public static void main(final String[] args) throws IOException, DocumentException {
		final File file = new File(DEST);
		file.getParentFile().mkdirs();
		new FreeSansExample().createPdf(DEST);
	}

	public void createPdf(final String dest) throws IOException, DocumentException {
//		final String text = new StringToUnicodeConverter().convert("ŚśŞşŠšȘșṠṡṢṣ ÝýŸÿŶŷȲȳ");
		final String text = "ŚśŞşŠšȘșṠṡṢṣ ÝýŸÿŶŷȲȳ";

		final Document document = new Document();
		final PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
		document.open();
		final Font f = new FontProviderWrapper().getFont(FontTyp.NORMAL, 12);
		Phrase p = new Phrase("This is incorrect: ");
		p.add(new Chunk(text, f));
		p.add(new Chunk(": 50.00 USD"));
		document.add(p);

		p = new Phrase("This is correct: ");
		p.add(new Chunk(text, f));
		p.add(new Phrase(": 50.00"));

		final ColumnText canvas = new ColumnText(writer.getDirectContent());
		canvas.setSimpleColumn(36, 750, 559, 780);
		canvas.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
		canvas.addElement(p);
		canvas.go();

		document.close();
		System.out.println("fertig");
	}

}
