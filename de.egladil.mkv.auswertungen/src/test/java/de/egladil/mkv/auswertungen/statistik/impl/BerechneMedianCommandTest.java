//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;

/**
 * BerechneMedianCommandTest
 */
public class BerechneMedianCommandTest {

	private final BerechneMedianCommand command = new BerechneMedianCommand();

	private class LoesungszettelAdapter implements ILoesungszettel {
		private final Integer punkte;

		/**
		 * Erzeugt eine Instanz von LoesungszettelAdapter
		 */
		public LoesungszettelAdapter(final Integer punkte) {
			this.punkte = punkte;
		}

		@Override
		public Integer getPunkte() {
			return punkte;
		}

		@Override
		public Integer getKaengurusprung() {
			return 12;
		}

		@Override
		public String getWertungscode() {
			return "rrrrrrrrrrrr";
		}

	}

	@Test
	public void berechne_throws_UnsupportedOperationException() {
		// Act + Assert
		final Throwable ex = assertThrows(UnsupportedOperationException.class, () -> {
			command.berechne(new ArrayList<>());
		});
		assertEquals("alleLoesungszettel darf nicht leer sein", ex.getMessage());
	}

	@Test
	public void berechne_nur_ein_loesungszettel() {
		// Arrange
		final List<ILoesungszettel> alleLoesungszettel = new ArrayList<>();
		alleLoesungszettel.add(new LoesungszettelAdapter(1875));

		final double expected = 18.75;

		// Act
		final double actual = command.berechne(alleLoesungszettel);

		// Assert
		assertEquals(expected, actual, 0.0);
	}

	@Test
	public void berechne_ungerade_anzahl() {
		// Arrange
		final List<ILoesungszettel> alleLoesungszettel = new ArrayList<>();
		alleLoesungszettel.add(new LoesungszettelAdapter(1875));
		alleLoesungszettel.add(new LoesungszettelAdapter(4325));
		alleLoesungszettel.add(new LoesungszettelAdapter(5325));
		alleLoesungszettel.add(new LoesungszettelAdapter(4900));
		alleLoesungszettel.add(new LoesungszettelAdapter(3950));

		final double expected = 43.25;

		// Act
		final double actual = command.berechne(alleLoesungszettel);

		// Assert
		assertEquals(expected, actual, 0.0);

		// verification
		final DescriptiveStatistics stats = new DescriptiveStatistics();

		// Add the data from the array
		stats.addValue(Double.valueOf("1875"));
		stats.addValue(Double.valueOf("4325"));
		stats.addValue(Double.valueOf("5325"));
		stats.addValue(Double.valueOf("4900"));
		stats.addValue(Double.valueOf("3950"));

		// Compute some statistics
		final double median = stats.getPercentile(50)/100;
		assertEquals(expected, median, 3);
	}

	@Test
	public void berechne_gerade_anzahl() {
		// Arrange
		final List<ILoesungszettel> alleLoesungszettel = new ArrayList<>();
		alleLoesungszettel.add(new LoesungszettelAdapter(1875));
		alleLoesungszettel.add(new LoesungszettelAdapter(4325));
		alleLoesungszettel.add(new LoesungszettelAdapter(4900));
		alleLoesungszettel.add(new LoesungszettelAdapter(3950));
		final double expected = 41.375;

		// Act
		final double actual = command.berechne(alleLoesungszettel);

		// Assert
		assertEquals(expected, actual, 0.0);

		// verification
		final DescriptiveStatistics stats = new DescriptiveStatistics();

		// Add the data from the array
		stats.addValue(Double.valueOf("1875"));
		stats.addValue(Double.valueOf("4325"));
		stats.addValue(Double.valueOf("4900"));
		stats.addValue(Double.valueOf("3950"));

		// Compute some statistics
		final double median = stats.getPercentile(50)/100;
		assertEquals(expected, median, 3);
	}
}
