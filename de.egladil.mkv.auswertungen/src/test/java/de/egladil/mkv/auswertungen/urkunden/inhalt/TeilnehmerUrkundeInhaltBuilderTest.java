//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * TeilnehmerUrkundeInhaltBuilderTest
 */
public class TeilnehmerUrkundeInhaltBuilderTest {

	private UrkundenmotivMitText urkundenmotiv;

	private UrkundenmotivProvider provider;

	private String wettbewerbsjahr = "2018";

	private String schulname = "Krautgartenschule";

	private TeilnehmerUndPunkte teilnehmerUndPunkte;

	private final String datum = "23.03.2018";

	@BeforeEach
	void setUp() {
		provider = Mockito.mock(UrkundenmotivProvider.class);
		urkundenmotiv = new UrkundenmotivMitTextBuilder(wettbewerbsjahr, provider, datum).schulname(schulname).build();
		this.teilnehmerUndPunkte = new TeilnehmerUndPunkte("Claudio Schmidt", "34,5", 3450, 3, Klassenstufe.EINS, Sprache.de);
	}

	@Test
	@DisplayName("happy hour")
	void buildErfolgreich() {
		final String klassenname = "Fuchsklasse";

		final String teilnehmername = "Claudio Schmidt";

		final String punkte = "34,5";

		final DefaultUrkundeInhalt result = new DefaultUrkundeInhaltBuilder(urkundenmotiv).schulname("Krautgartenschule").klassenname(klassenname)
			.teilnehmerUndPunkte(teilnehmerUndPunkte).build();

		assertEquals(klassenname, result.getKlassenname());
		assertEquals(wettbewerbsjahr, result.getWettbewerbsjahr());
		assertEquals(schulname, result.getSchulname());
		assertEquals(teilnehmername, result.getTeilnehmername());
		assertEquals(punkte, result.getPunkteText());
		assertEquals(datum, result.getDatum());
		assertEquals(Sprache.de, result.getSprache());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn provider null")
	void konstruktorOhneUrkundenmotiv() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new DefaultUrkundeInhaltBuilder(null);
		});

		assertEquals("UrkundenmotivMitText ist erforderlich", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn teilnehmerUndPunkte null")
	void buildTeilnehmerUndPunkte() {
		final Throwable ex = assertThrows(IllegalStateException.class, () -> {
			new DefaultUrkundeInhaltBuilder(urkundenmotiv).klassenname("SAD").build();
		});

		assertEquals("teilnehmerUndPunkte ist erforderlich. Der Builder ist nicht korrekt initialisiert.", ex.getMessage());
	}
}
