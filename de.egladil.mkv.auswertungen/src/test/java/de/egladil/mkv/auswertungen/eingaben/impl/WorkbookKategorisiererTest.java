//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.MKAuswertungCell;
import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.domain.MKCellAddress;
import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * WorkbookKategorisiererTest
 */
public class WorkbookKategorisiererTest {

	private final WorkbookKategorisierer kategorisierer = new WorkbookKategorisierer();

	@Test
	public void errateKategorie_row_ergibt_antwortbuchstaben() {
		// Arrange
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "c"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

		// Act + Assert
		assertEquals(WorkbookKategorie.ANTWORTBUCHSTABEN, kategorisierer.errateKategorie(row));
	}

	@Test
	public void errateKategorie_workbook_ergibt_antwortbuchstaben() {
		// Arrange
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();
		{
			final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
			row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
			row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
			row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
			row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
			row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
			row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
			row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
			row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
			row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
			row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
			row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "e"));
			row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

			workbook.addRow(row);
		}
		{
			final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
			row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
			row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
			row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
			row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
			row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
			row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
			row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
			row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
			row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
			row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
			row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "c"));
			row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

			workbook.addRow(row);
		}

		// Act + Assert
		assertEquals(WorkbookKategorie.ANTWORTBUCHSTABEN, kategorisierer.errateKategorie(workbook));
	}

	@Test
	public void errateKategorie_row_ergibt_wertungsbuchstaben() {
		// Arrange
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "e"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));

		// Act + Assert
		assertEquals(WorkbookKategorie.WERTUNGSBUCHSTABEN, kategorisierer.errateKategorie(row));
	}

	@Test
	public void errateKategorie_workbook_ergibt_wertungsbuchstaben() {
		// Arrange
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();
		{
			final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
			row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
			row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
			row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
			row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
			row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
			row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
			row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
			row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
			row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
			row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
			row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "e"));
			row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

			workbook.addRow(row);
		}

		{
			final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
			row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
			row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
			row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
			row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
			row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
			row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
			row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
			row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
			row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
			row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
			row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "e"));
			row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "r"));

			workbook.addRow(row);
		}

		// Act + Assert
		assertEquals(WorkbookKategorie.WERTUNGSBUCHSTABEN, kategorisierer.errateKategorie(workbook));
	}

	@Test
	public void errateKategorie_row_ergibt_nicht_entscheidbar() {
		// Arrange
		final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
		row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
		row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
		row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
		row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
		row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
		row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
		row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
		row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
		row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
		row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
		row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "e"));
		row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

		// Act + Assert
		assertEquals(WorkbookKategorie.NICHT_ENTSCHEIDBAR, kategorisierer.errateKategorie(row));
	}

	@Test
	public void errateKategorie_workbook_ergibt_nicht_entscheidbar() {
		// Arrange
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();
		{
			final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
			row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
			row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
			row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
			row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "e"));
			row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
			row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
			row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
			row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
			row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "d"));
			row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), ""));
			row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "e"));
			row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

			workbook.addRow(row);
		}
		{
			final MKAuswertungRow row = new MKAuswertungRow(13, Klassenstufe.EINS, 0);
			row.addCell(0, new MKAuswertungCell(new MKCellAddress(0, 0), "n"));
			row.addCell(1, new MKAuswertungCell(new MKCellAddress(0, 1), "n"));
			row.addCell(2, new MKAuswertungCell(new MKCellAddress(0, 2), "n"));
			row.addCell(3, new MKAuswertungCell(new MKCellAddress(0, 3), "n"));
			row.addCell(4, new MKAuswertungCell(new MKCellAddress(0, 4), "n"));
			row.addCell(5, new MKAuswertungCell(new MKCellAddress(0, 5), "d"));
			row.addCell(6, new MKAuswertungCell(new MKCellAddress(0, 6), "n"));
			row.addCell(7, new MKAuswertungCell(new MKCellAddress(0, 7), "n"));
			row.addCell(8, new MKAuswertungCell(new MKCellAddress(0, 8), "n"));
			row.addCell(9, new MKAuswertungCell(new MKCellAddress(0, 9), "n"));
			row.addCell(10, new MKAuswertungCell(new MKCellAddress(0, 10), "n"));
			row.addCell(11, new MKAuswertungCell(new MKCellAddress(0, 11), "n"));

			workbook.addRow(row);
		}

		// Act + Assert
		assertEquals(WorkbookKategorie.NICHT_ENTSCHEIDBAR, kategorisierer.errateKategorie(workbook));
	}

}
