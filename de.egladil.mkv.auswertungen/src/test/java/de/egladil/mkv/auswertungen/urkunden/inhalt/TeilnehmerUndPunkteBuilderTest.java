//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeBuilder;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * TeilnehmerUndPunkteBuilderTest
 */
public class TeilnehmerUndPunkteBuilderTest {

	private static final String WERTUNGSCODE = "rrrrrrrrrrnn";

	private ILoesungszettel punkteProvider;

	private final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("H6343GD4", "2018");

	@BeforeEach
	void setUp() {
		punkteProvider = Mockito.mock(ILoesungszettel.class);
	}

	@Test
	@DisplayName("build mit normalen Daten klappt")
	void buildKlappt() {

		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.EINS).vorname("Heinz")
			.nachname("Strunz").checkAndBuild();
		final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(Teilnahmeart.S, teilnahmeIdentifier.getKuerzel(),
			teilnahmeIdentifier.getJahr()).klassenstufe(Klassenstufe.EINS).name("Eichhörnchenklasse").checkAndBuild();
		auswertungsgruppe.addTeilnehmer(teilnehmer);

		Mockito.when(punkteProvider.getKaengurusprung()).thenReturn(5);
		Mockito.when(punkteProvider.getPunkte()).thenReturn(5450);
		Mockito.when(punkteProvider.getWertungscode()).thenReturn(WERTUNGSCODE);

		final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkteBuilder(teilnehmer, punkteProvider).build();

		assertNotNull(teilnehmerUndPunkte);
		assertEquals("Heinz Strunz", teilnehmerUndPunkte.getName());
		assertEquals(5, teilnehmerUndPunkte.getKaengurusprung());
		assertEquals("54,50", teilnehmerUndPunkte.getPunkte());
		assertEquals(WERTUNGSCODE, teilnehmerUndPunkte.getBerechneterWertungscode());
		assertNotNull(teilnehmerUndPunkte.getKlassenname());
	}

	@Test
	void initMitTeilnehmerNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkteBuilder(null, punkteProvider);
		});
		assertEquals("teilnehmer ist erforderlich", ex.getMessage());
	}

	@Test
	void initMitPunkteproviderNull() {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.EINS).vorname("Heinz")
			.nachname("Strunz").checkAndBuild();
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new TeilnehmerUndPunkteBuilder(teilnehmer, null);
		});
		assertEquals("punkteProvider ist erforderlich", ex.getMessage());
	}

}
