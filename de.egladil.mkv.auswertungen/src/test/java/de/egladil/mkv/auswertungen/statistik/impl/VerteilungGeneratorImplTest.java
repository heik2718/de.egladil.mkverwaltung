//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * VerteilungGeneratorImplTest
 */
public class VerteilungGeneratorImplTest {

	@Test
	public void berechneVerteilung_throws_nullPointerException_wenn_loesungszettel_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new VerteilungGeneratorImpl().berechneGesamtpunktverteilungDaten(null, Klassenstufe.EINS, "2017");
		});
		assertEquals("Parameter loesungszettel", ex.getMessage());

	}

	@Test
	public void berechneVerteilung_throws_nullPointerException_wenn_klasse_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new VerteilungGeneratorImpl().berechneGesamtpunktverteilungDaten(new ArrayList<>(), null, "2017");
		});
		assertEquals("Parameter klasse", ex.getMessage());

	}

	@Test
	public void berechneVerteilung_throws_nullPointerException_wenn_jahr_null() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new VerteilungGeneratorImpl().berechneGesamtpunktverteilungDaten(new ArrayList<>(), Klassenstufe.ZWEI, null);
		});
		assertEquals("Parameter jahr", ex.getMessage());

	}

}
