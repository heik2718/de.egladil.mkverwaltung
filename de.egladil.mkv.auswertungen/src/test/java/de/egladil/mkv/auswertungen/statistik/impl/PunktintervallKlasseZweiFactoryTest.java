//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.Punktintervall;


/**
 * PunktintervallKlasseZweiFactoryTest
 */
public class PunktintervallKlasseZweiFactoryTest {

	private final PunktintervallKlasseZweiFactory factory = new PunktintervallKlasseZweiFactory();

	@Test
	public void create_throws_when_minVal_negative() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			factory.createPunktintervall(-1);
		});
		assertEquals("minVal muss zwischen 0 und 7500 liegen: minVal=-1", ex.getMessage());

	}

	@Test
	public void create_throws_when_minVal_too_large() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			factory.createPunktintervall(7501);
		});
		assertEquals("minVal muss zwischen 0 und 7500 liegen: minVal=7501", ex.getMessage());

	}

	@Test
	public void create_produces_correct_intervalls(){
		{
			final Punktintervall intervall = factory.createPunktintervall(7500);
			assertEquals(7500, intervall.getMaxVal());
		}
		{
			final Punktintervall intervall = factory.createPunktintervall(7000);
			assertEquals(7200, intervall.getMaxVal());
		}
		{
			final Punktintervall intervall = factory.createPunktintervall(6500);
			assertEquals(6900, intervall.getMaxVal());
		}
	}

	@Test
	public void getPunktintervalleDescending_klappt(){
		final List<Punktintervall> actual = factory.getPunktintervalleDescending();
		final List<Punktintervall> expected = getExpectedPunktintervalle();
		assertEquals(expected.size(), actual.size());
		for (int i = 0; i < expected.size(); i++){
			assertEquals("Fehler bei " + i, expected.get(i), actual.get(i));
		}
	}

	private List<Punktintervall> getExpectedPunktintervalle(){
		final List<Punktintervall> result = new ArrayList<>();
		result.add(factory.createPunktintervall(7500));
		result.add(factory.createPunktintervall(7000));
		result.add(factory.createPunktintervall(6500));
		result.add(factory.createPunktintervall(6000));
		result.add(factory.createPunktintervall(5500));
		result.add(factory.createPunktintervall(5000));
		result.add(factory.createPunktintervall(4500));
		result.add(factory.createPunktintervall(4000));
		result.add(factory.createPunktintervall(3500));
		result.add(factory.createPunktintervall(3000));
		result.add(factory.createPunktintervall(2500));
		result.add(factory.createPunktintervall(2000));
		result.add(factory.createPunktintervall(1500));
		result.add(factory.createPunktintervall(1000));
		result.add(factory.createPunktintervall(500));
		result.add(factory.createPunktintervall(0));
		return result;
	}

}
