//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.renderer;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;


/**
* DoubleExactRendererTest
*/
public class DoubleExactRendererTest {

	@Test
	public void render_dreistellig(){
		// Arrange
		final double d = 41.375;
		final String expected = "41,375";

		// Act
		final String actual = new DoubleExactRenderer(d).render();

		// Assert
		assertEquals(expected, actual);
	}

}
