//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.renderer;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.Punktintervall;

/**
 * PunktintervallRendererTest
 */
public class PunktintervallRendererTest {

	@Test
	public void render_klappt() {
		// Arrange
		final String expected = "50,00-54,75";
		final Punktintervall punktintervall = new Punktintervall.Builder(5000).laenge(Punktintervall.DEFAULT_LAENGE).build();
		final PunktintervallRenderer renderer = new PunktintervallRenderer(punktintervall);

		// Act
		final String actual = renderer.render();

		// Assert
		assertEquals(expected, actual);
	}

}
