//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.RohpunktItem;

/**
 * BerechneProzentrangPunkteCommandTest
 */
public class BerechneProzentrangPunkteCommandTest {

	private final BerechneProzentrangPunkteCommand command = new BerechneProzentrangPunkteCommand();

	private List<RohpunktItem> rohpunktItems;

	private Map<Integer, RohpunktItem> punktklassen;

	@BeforeEach
	public void setUp() {
		rohpunktItems = new ArrayList<>();
		punktklassen = new HashMap<>();

		{
			final RohpunktItem item = new RohpunktItem();
			item.setPunkte(0);
			item.setAnzahl(6);
			rohpunktItems.add(item);
			punktklassen.put(0, item);
		}

		{
			final RohpunktItem item = new RohpunktItem();
			item.setPunkte(200);
			item.setAnzahl(1);
			rohpunktItems.add(item);
			punktklassen.put(200, item);
		}

		{
			final RohpunktItem item = new RohpunktItem();
			item.setPunkte(375);
			item.setAnzahl(10);
			rohpunktItems.add(item);
			punktklassen.put(375, item);
		}

		{
			final RohpunktItem item = new RohpunktItem();
			item.setPunkte(450);
			item.setAnzahl(1);
			rohpunktItems.add(item);
			punktklassen.put(450, item);
		}

		{
			final RohpunktItem item = new RohpunktItem();
			item.setPunkte(475);
			item.setAnzahl(4);
			rohpunktItems.add(item);
			punktklassen.put(475, item);
		}

		{
			final RohpunktItem item = new RohpunktItem();
			item.setPunkte(500);
			item.setAnzahl(4);
			rohpunktItems.add(item);
			punktklassen.put(500, item);
		}
	}

	@Test // // http://www.osa.fu-berlin.de/psychologie/aufgaben/diagnostik/
	public void berechneKumulierteHaeufigkeit_klappt() {
		// Arrange
		final Map<Integer, Integer> expectedHaeufigkeiten = new HashMap<>();
		expectedHaeufigkeiten.put(0, 6);
		expectedHaeufigkeiten.put(200, 7);
		expectedHaeufigkeiten.put(375, 17);
		expectedHaeufigkeiten.put(450, 18);
		expectedHaeufigkeiten.put(475, 22);
		expectedHaeufigkeiten.put(500, 26);

		final Map<Integer, Integer> actualHaeufigkeiten = new HashMap<>();

		// Act
		for (final Integer punkte : expectedHaeufigkeiten.keySet()) {
			final int kum = command.berechneKumulierteHaeufigkeit(punkte, rohpunktItems);
			actualHaeufigkeiten.put(punkte, Integer.valueOf(kum));
		}

		// Assert
		for (final Integer punkte : expectedHaeufigkeiten.keySet()) {
			assertEquals("Fehler bei " + punkte + " Punkten", expectedHaeufigkeiten.get(punkte), actualHaeufigkeiten.get(punkte));
		}
	}

	@Test
	public void berechne_klappt() {
		// Act
		command.berechne(rohpunktItems, 6054);

		// assert
		for (final RohpunktItem item : rohpunktItems) {
			if (item.getPunkte() == 0) {
				assertEquals("0,099", item.getProzentrangText());
			}
			if (item.getPunkte() == 200) {
				assertEquals("0,116", item.getProzentrangText());
			}
			if (item.getPunkte() == 375) {
				assertEquals("0,281", item.getProzentrangText());
			}
			if (item.getPunkte() == 450) {
				assertEquals("0,297", item.getProzentrangText());
			}
			if (item.getPunkte() == 475) {
				assertEquals("0,363", item.getProzentrangText());
			}
			if (item.getPunkte() == 500) {
				assertEquals("0,429", item.getProzentrangText());
			}
		}
	}
}
