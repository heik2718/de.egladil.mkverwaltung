//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.request.TeilnehmerUrkundenauftrag;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * AuswertungServiceImplTest
 */
public class AuswertungServiceImplTest {

	private AuswertungServiceImpl service;

	@BeforeEach
	void setUp() {
		service = new AuswertungServiceImpl(Mockito.mock(ILoesungszettelDao.class), Mockito.mock(IAuswertungsgruppeDao.class),
			Mockito.mock(IAuswertungDownloadDao.class), Mockito.mock(TeilnahmenFacade.class), Mockito.mock(TeilnehmerFacade.class),
			Mockito.mock(IVerteilungGenerator.class));
	}

	@Nested
	@DisplayName("Tests für Parameter von generiereAuswertungFuerEinzelteilnehmer()")
	class GeneriereEinzelauswertungParameter {

		@Test
		@DisplayName("IllegalArgumentException wenn payload null")
		void generiereAuswertungFuerEinzelteilnehmerPayloadNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				service.generiereAuswertungFuerEinzelteilnehmer("gakgd", null,
					TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("gas", "2018"));
			});
			assertEquals("auftrag null", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn farbschema null")
		void generiereAuswertungFuerEinzelteilnehmerFarbschemaNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {

				final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
				auftrag.setTeilnehmerKuerzel(new String[] { "DTDTZDXFX" });
				service.generiereAuswertungFuerEinzelteilnehmer("ahshh", auftrag,
					TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("DTDTZDXFX", "2018"));
			});

			assertEquals("auftrag.farbschema null", ex.getMessage());

		}

		@Test
		@DisplayName("IllegalArgumentException wenn kein Teilnehmer")
		void generiereAuswertungFuerEinzelteilnehmerKeinTeilnehmer() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
				auftrag.setFarbschemaName(Farbschema.BLUE.toString());
				auftrag.setTeilnehmerKuerzel(new String[0]);
				service.generiereAuswertungFuerEinzelteilnehmer("ahshh", auftrag,
					TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("DTDTZDXFX", "2018"));
			});

			assertEquals("auftrag.teilnehmerKuerzel leer", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn keine teilnehmerkuerzel")
		void generiereAuswertungFuerEinzelteilnehmerKeineTeilnehmerkuerzel() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
				auftrag.setFarbschemaName(Farbschema.BLUE.toString());
				service.generiereAuswertungFuerEinzelteilnehmer("ahshh", auftrag,
					TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("DTDTZDXFX", "2018"));
			});

			assertEquals("auftrag.teilnehmerKuerzel null", ex.getMessage());
		}

		@Test
		@DisplayName("IllegalArgumentException wenn benutzerUuid null")
		void generiereAuswertungFuerEinzelteilnehmerBenutzerUuidNull() {
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
				auftrag.setTeilnehmerKuerzel(new String[] { "ZDZRSRSZ6CFDZJ" });
				auftrag.setFarbschemaName(Farbschema.BLUE.toString());
				service.generiereAuswertungFuerEinzelteilnehmer(null, auftrag, null);
			});

			assertEquals("benutzerUuid null oder leer", ex.getMessage());

		}

		@Test
		@DisplayName("IllegalArgumentException wenn benutzerUuid blank")
		void generiereAuswertungFuerEinzelteilnehmerBenutzerUuidBlank() {

			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
				auftrag.setTeilnehmerKuerzel(new String[] { "ZDZRSRSZ6CFDZJ" });
				auftrag.setFarbschemaName(Farbschema.BLUE.toString());
				service.generiereAuswertungFuerEinzelteilnehmer(" ", auftrag,
					TeilnahmeIdentifier.createPrivatteilnahmeIdentifier("DTDTZDXFX", "2018"));
			});

			assertEquals("benutzerUuid null oder leer", ex.getMessage());
		}
	}
}
