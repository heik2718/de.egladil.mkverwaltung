//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenschluessel;
import de.egladil.mkv.auswertungen.statistik.gruppen.Rootgruppeuebersicht;
import de.egladil.mkv.auswertungen.statistik.gruppen.RootgruppeuebersichtCreator;
import de.egladil.mkv.auswertungen.statistik.impl.VerteilungGeneratorImpl;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;

/**
 * AufgabenuebersichtPDFGeneratorTest
 */
public class AufgabenuebersichtPDFGeneratorTest {

	private static final String SCHULNAME = "Blümchenschule";

	private IVerteilungGenerator verteilungGenerator;

	private Rootgruppeuebersicht rootgruppeuebersicht;

	@BeforeEach
	void setUp() {
		verteilungGenerator = new VerteilungGeneratorImpl();
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		final List<TeilnehmerUndPunkte> rohdaten = TestHelper.getSchulrohdaten();
		rootgruppeuebersicht = new RootgruppeuebersichtCreator().createSchuluebersicht(rohdaten, SCHULNAME);
	}

	@Test
	void generiereZweiSchueler() throws Exception {

		// Arrange
		final String path = "/home/heike/temp/aufgabenuebersicht.pdf";
		final Klassenschluessel klassenschluessel = TestHelper.AMEISEN;
		final List<TeilnehmerUndPunkteLoesungszettelAdapter> zettel = rootgruppeuebersicht.getKlassenliste(klassenschluessel);
		final List<ILoesungszettel> alleZettel = new ArrayList<>();
		alleZettel.addAll(zettel);

		final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(alleZettel,
			klassenschluessel.getKlassenstufe(), String.valueOf(KontextReader.getInstance().getKontext().getWettbewerbsjahr()));

		// Act
		final byte[] pdf = new AufgabenuebersichtPDFGenerator().generiere(daten, klassenschluessel.getKlassenname());

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(path)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		// Assert
		assertTrue(new File(path).canRead());
	}
}
