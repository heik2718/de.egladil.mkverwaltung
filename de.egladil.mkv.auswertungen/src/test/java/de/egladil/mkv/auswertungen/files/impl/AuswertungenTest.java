//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.exceptions.UnprocessableAuswertungException;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;

/**
 * AuswertungenTest
 */
public class AuswertungenTest {

	private static final int MAX_EXTRACTED_BYTES = 2097152;

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungenTest.class);

	 @Test
	public void readAuswertung_xlsx() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/qs/P_VZANFNKV_7f10c7eca94d2110547dfc6609b56993630c90582c3dee436c4830a3947e2946.xlsx")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_OPEN_XML);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertung_xls() throws Exception {
		try (InputStream in = getClass()
			.getResourceAsStream("/qs/S_YFI9TZTT_02b1a48e45a75caf8a2c2049603d019da3a7bbaf79fe64194526833af5d78d9a.xls")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_MSEXCEL);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertung_ods() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/qs/P_VZANFNKV_a0ede37a67c513adf375ba216f53f4fbd3018c5e711d68ebfb93c5d772864b58.ods")) {
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				for (final MKAuswertungRow row : rows) {
					try {
						System.out.println(row.toNutzereingabe());
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage());
				fail("UnprocessableAuswertungException");
			}
		}
	}

	 @Test
	public void checksummeBerechnen() throws IOException {
		try (
			// InputStream in = getClass()
			// .getResourceAsStream("/qs/adv-vereinbarung-1.0.pdf");
			InputStream in = getClass().getResourceAsStream("/qs/S_EEF8FOYK_0e1dfbccf527cd93095abd92550274a088e20b58de45c4b09dab7dd18e25dca0.xlsx");
			StringWriter sw = new StringWriter()) {
			IOUtils.copy(in, sw);

			final byte[] data = sw.toString().getBytes();

			final String checksumme = new UploadUtils().getChecksum(data);

			System.out.println(checksumme);
		}
	}

	// @Test
	public void getMimeType() throws Exception {
		try (
			InputStream in = getClass()
				.getResourceAsStream("/qs/S_1H9EVDFR_39cd2bbffb8acea34fa3c5fa0833f5a606d0a3761e2984bab867beeb6f1ca55b.unknown");
			StringWriter sw = new StringWriter()) {
			IOUtils.copy(in, sw);

			final byte[] data = sw.toString().getBytes();

			final String mimetype = new UploadUtils().getMimeType(data);

			System.out.println(mimetype);
		}
	}

	@Test
	public void readAuswertungInklusionMitNamen_xls() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/korrekt/inklusion_eine_namensspalte.xls")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_MSEXCEL);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				assertEquals(2, rows.size());
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertungInklusionOhneNamen_xls() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/korrekt/inklusion_keine_namensspalte.xls")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_MSEXCEL);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				assertEquals(2, rows.size());
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertungInklusionMitNamen_xlsx() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/korrekt/inklusion_eine_namensspalte.xlsx")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_OPEN_XML);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				assertEquals(2, rows.size());
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertungInklusionOhneNamen_xlsx() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/korrekt/inklusion_keine_namensspalte.xlsx")) {
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_OPEN_XML);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				assertEquals(2, rows.size());
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertungInklusionMitNamen_ods() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/korrekt/inklusion_eine_namensspalte.ods")) {
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				assertEquals(2, rows.size());
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}

	@Test
	public void readAuswertungInklusionOhneNamen_ods() throws Exception {
		try (InputStream in = getClass().getResourceAsStream("/korrekt/inklusion_keine_namensspalte.ods")) {
			final AuswertungOpenOfficeStreamReader streamReader = (AuswertungOpenOfficeStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.ODS);
			try {
				final MKAuswertungWorkbook workbook = streamReader.readAuswertung(in, MAX_EXTRACTED_BYTES);
				final List<MKAuswertungRow> rows = workbook.getSortedCopyOfRows();
				assertEquals(2, rows.size());
				for (final MKAuswertungRow row : rows) {
					try {
						row.toNutzereingabe();
					} catch (final Exception e) {
						fail(e.getClass().getSimpleName() + " in Zeile " + row.getIndex() + ": " + e.getMessage());
					}
				}
			} catch (final UnprocessableAuswertungException e) {
				LOG.error(e.getMessage(), e);
				fail("UnprocessableAuswertungException");
			}
		}
	}
}
