//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.tools.parser.OOElementType;
import de.egladil.tools.parser.OpenOfficeTableElement;

/**
 * OpenOfficeAssistentTest
 */
public class OpenOfficeAssistentTest {

	@Test
	public void findFirstIndexWithContent_throws_NullPointerException_when_row_null() {
		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			OpenOfficeAssistent.findFirstIndexWithContent(null, "U");
		});
		assertEquals("row null", ex.getMessage());

	}

	@Test
	public void findFirstIndexWithContent_throws_IllegalArgumentException_when_content_null() {
		// Act + Assert
		final OpenOfficeTableElement tableRow = new OpenOfficeTableElement(OOElementType.ROW);

		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			OpenOfficeAssistent.findFirstIndexWithContent(tableRow, null);
		});
		assertEquals("content blank", ex.getMessage());

	}

	@Test
	public void findFirstIndexWithContent_throws_IllegalArgumentException_when_content_blank() {
		// Act + Assert
		final OpenOfficeTableElement tableRow = new OpenOfficeTableElement(OOElementType.ROW);

		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			OpenOfficeAssistent.findFirstIndexWithContent(tableRow, " ");
		});
		assertEquals("content blank", ex.getMessage());

	}

	@Test
	public void isWertungseintrag_keine_namenspalten() {
		// Arrange
		final int anzahlNamenspalten = 0;

		// Act + Assert
		assertTrue(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 0));
		assertFalse(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 1));
		assertTrue(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 2));

	}

	@Test
	public void isWertungseintrag_zwei_namenspalten() {
		// Arrange
		final int anzahlNamenspalten = 2;

		// Act + Assert
		assertTrue(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 0));
		assertFalse(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 1));
		assertTrue(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 2));

	}

	@Test
	public void isWertungseintrag_eine_namenspalte() {
		// Arrange
		final int anzahlNamenspalten = 1;

		// Act + Assert
		assertFalse(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 0));
		assertTrue(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 1));
		assertFalse(OpenOfficeAssistent.isWertungseintrag(anzahlNamenspalten, 2));

	}
}
