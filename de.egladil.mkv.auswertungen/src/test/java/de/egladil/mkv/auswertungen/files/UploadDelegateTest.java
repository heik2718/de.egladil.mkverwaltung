//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.auswertungen.files.impl.UploadUtils;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.utils.MockInMemoryDB;

/**
 * UploadDelegateTest
 */
public class UploadDelegateTest {

	private static final String CHECKSUM = "jkahskfgaghihasifhahalsd89ax";

	private static final String PATH_UPLOAD_DIR = "/home/heike/upload/mkv";

	private static final String PATH_SANDBOX_DIR = "/home/heike/upload/sandbox";

	private static final Logger LOG = LoggerFactory.getLogger(UploadDelegateTest.class);

	private static final String BENUTZER_UUID = "gagsasofoa-ds8ahkashh";

	private static final String VALID_MIMETYPE = UploadMimeType.VDN_MSEXCEL.toString();

	private static final String INVALID_MIMETYPE = "application/pdf";

	private static final String MAX_FILESIZE_TEXT = "600 kB";

	private ILehrerkontoDao lehrerkontoDao;

	private IPrivatkontoDao privatkontoDao;

	private AuswertungstabelleFileWriter fileWriter;

	private IProtokollService protokollService;

	private IUploadDao uploadDao;

	private UploadDelegate uploadDelegate;

	@SuppressWarnings("deprecation")
	private MockInMemoryDB mockDB = new MockInMemoryDB();

	@BeforeEach
	public void setUp() {
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		protokollService = Mockito.mock(IProtokollService.class);
		uploadDao = Mockito.mock(IUploadDao.class);
		fileWriter = Mockito.mock(AuswertungstabelleFileWriter.class);

		uploadDelegate = new UploadDelegate(lehrerkontoDao, privatkontoDao, uploadDao, protokollService, fileWriter);
	}

	@Test
	public void validatePayloadSize_wirft_NullPointerException_wenn_benutzerUuid_null() throws Exception {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.validatePayloadSize(null, 19l, 20, MAX_FILESIZE_TEXT);
		});
		assertEquals("benutzerUuid null", ex.getMessage());

	}

	@Test
	public void validatePayloadSize_wirft_NullPointerException_wenn_maxFileSizeText_null() throws Exception {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.validatePayloadSize(BENUTZER_UUID, 19, 20, null);
		});
		assertEquals("maxFileSizeText null", ex.getMessage());

	}

	@Test
	public void validatePayloadSize_klappt_bei_zu_grosser_datei() throws Exception {
		// Arrange
		final UploadUtils uploadUtils = Mockito.mock(UploadUtils.class);
		final byte[] data = "BBBBBBBBBB".getBytes();
		Mockito.when(uploadUtils.getMimeType(data)).thenReturn(VALID_MIMETYPE);
		uploadDelegate.setUploadUtils(uploadUtils);

		final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_SIZE.getKuerzel(), BENUTZER_UUID, "Größe 10 byte");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		final Optional<APIResponsePayload> opt = uploadDelegate.validatePayloadSize(BENUTZER_UUID, 10, 9, MAX_FILESIZE_TEXT);

		// Assert
		assertTrue(opt.isPresent());
		final APIResponsePayload pm = opt.get();
		assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
	}

	@Test
	public void validatePayloadSize_ignoriert_unknown_mimetype() throws Exception {
		// Arrange
		final UploadUtils uploadUtils = Mockito.mock(UploadUtils.class);
		final byte[] data = "B".getBytes();
		Mockito.when(uploadUtils.getMimeType(data)).thenReturn(INVALID_MIMETYPE);
		uploadDelegate.setUploadUtils(uploadUtils);

		final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_MIMETYPE.getKuerzel(), BENUTZER_UUID,
			"MIMETYPE " + INVALID_MIMETYPE);
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		final Optional<APIResponsePayload> opt = uploadDelegate.validatePayloadSize(BENUTZER_UUID, 1, 600000, MAX_FILESIZE_TEXT);

		// Assert
		assertFalse(opt.isPresent());
	}

	@Test
	public void uploadBytes_protokolliert_falschen_mimetype() throws Exception {
		// Arrange eilnahmeschluessel [teilnahmeart=S, jahr=2017, identifizierendesKuerzel=HTGFRD5H]
		// jkahskfgaghihasifhahalsd89ax
		final UploadUtils uploadUtils = Mockito.mock(UploadUtils.class);
		final byte[] data = "B".getBytes();
		Mockito.when(uploadUtils.getMimeType(data)).thenReturn(INVALID_MIMETYPE);
		Mockito.when(uploadUtils.getChecksum(data)).thenReturn(CHECKSUM);
		uploadDelegate.setUploadUtils(uploadUtils);
		uploadDelegate.setTabelleFileWriter(new AuswertungstabelleFileWriter(protokollService));

		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.createSchulteilnahmeIdentifier("HTGFRD5H", "2017");
		final Upload upload = new Upload();
		upload.initWith(teilnahmeIdentifier);
		upload.setChecksumme(CHECKSUM);

		Mockito.when(uploadDao.findUpload(teilnahmeIdentifier, CHECKSUM)).thenReturn(Optional.empty());

		final Optional<Lehrerkonto> konto = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_1);
		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(konto);

		final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_MIMETYPE.getKuerzel(), MockInMemoryDB.UUID_LEHRER_1,
			"Unbekannter MIMETYPE - Dateiname S_HTGFRD5H_df7e70e5021544f4834bbee64a9e3789febc4be81470df629cad6ddb03320a5c.unknown");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		final UploadLog uploadLog = uploadDelegate.uploadBytes(MockInMemoryDB.UUID_LEHRER_1, data, PATH_UPLOAD_DIR,
			PATH_SANDBOX_DIR, "2017");
		final APIResponsePayload pm = uploadLog.getApiResponsePayload();

		// Assert
		assertEquals(MessageLevel.INFO.toString(), pm.getApiMessage().getLevel());
		assertEquals("Die Datei wurde erfolgreich hochgeladen. Vielen Dank!", pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
		LOG.info(uploadLog.getMessage());
		final Optional<Upload> optUpload = uploadLog.getUpload();
		if (optUpload.isPresent()) {
			LOG.info(optUpload.get().toString());
		}
	}

	@Test
	public void validatePayloadSize_klappt_wenn_alles_korrekt() throws Exception {
		// Arrange
		final UploadUtils uploadUtils = Mockito.mock(UploadUtils.class);
		final byte[] data = "B".getBytes();
		Mockito.when(uploadUtils.getMimeType(data)).thenReturn(VALID_MIMETYPE);
		uploadDelegate.setUploadUtils(uploadUtils);

		// Act
		final Optional<APIResponsePayload> opt = uploadDelegate.validatePayloadSize(BENUTZER_UUID, 599999, 600000, MAX_FILESIZE_TEXT);

		// Assert
		assertFalse(opt.isPresent());
	}

	@Test
	public void uploadBytes_wirft_NullPointerException_wenn_benutzerUuid_null() throws Exception {
		final byte[] data = "gksgdf".getBytes();

		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.uploadBytes(null, data, PATH_UPLOAD_DIR, PATH_SANDBOX_DIR, "2017");
		});
		assertEquals("benutzerUuid null", ex.getMessage());

	}

	@Test
	public void uploadBytes_wirft_NullPointerException_wenn_data_null() throws Exception {
		// Act + Assert
		final byte[] data = null;
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.uploadBytes(BENUTZER_UUID, data, PATH_UPLOAD_DIR, PATH_SANDBOX_DIR, "2017");
		});
		assertEquals("data null", ex.getMessage());

	}

	@Test
	public void uploadBytes_wirft_NullPointerException_wenn_uploadPath_null() throws Exception {
		final byte[] data = "gksgdf".getBytes();

		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.uploadBytes(BENUTZER_UUID, data, null, PATH_SANDBOX_DIR, "2017");
		});
		assertEquals("pathUploadDir null", ex.getMessage());

	}

	@Test
	public void uploadBytes_wirft_NullPointerException_wenn_sandboxPath_null() throws Exception {
		final byte[] data = "gksgdf".getBytes();

		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.uploadBytes(BENUTZER_UUID, data, PATH_UPLOAD_DIR, null, "2017");
		});
		assertEquals("pathUploadDir null", ex.getMessage());

	}

	@Test
	public void uploadBytes_wirft_NullPointerException_wenn_wettbewerbsjahr_null() throws Exception {
		final byte[] data = "gksgdf".getBytes();

		// Act + Assert
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			uploadDelegate.uploadBytes(BENUTZER_UUID, data, PATH_UPLOAD_DIR, PATH_SANDBOX_DIR, null);
		});
		assertEquals("wettbewerbsjahr null", ex.getMessage());

	}

	@Test
	public void uploadBytes_wirft_UnknownAccountException_wenn_kein_mkvkonto() throws Exception {
		// Arrange
		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());
		Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());
		final byte[] data = "gksgdf".getBytes();

		// Act + Assert
		final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
			uploadDelegate.uploadBytes(BENUTZER_UUID, data, PATH_UPLOAD_DIR, PATH_SANDBOX_DIR, "2017");
		});
		assertEquals("kein Lehrerkonto oder Privatkonto mit UUID [gagsasofoa-ds8ahkashh] gefunden", ex.getMessage());

	}

	@Test
	public void uploadBytes_speichert_nicht_wenn_keine_teilnahme_im_wettbewerbsjahr() throws Exception {
		// Arrange
		final Lehrerkonto lehrerkonto = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_3).get();
		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_3)).thenReturn(Optional.of(lehrerkonto));
		Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());
		final byte[] data = "gksgdf".getBytes();
		final UploadUtils uploadUtils = Mockito.mock(UploadUtils.class);
		Mockito.when(uploadUtils.getMimeType(data)).thenReturn(VALID_MIMETYPE);
		Mockito.when(uploadUtils.getChecksum(data)).thenReturn(MockInMemoryDB.CHECSUMME_LEHRERKONTO_1);
		uploadDelegate.setUploadUtils(uploadUtils);

		// Act + Assert
		final UploadLog uploadLog = uploadDelegate.uploadBytes(MockInMemoryDB.UUID_LEHRER_3, data, PATH_UPLOAD_DIR,
			PATH_SANDBOX_DIR, "2017");
		final APIResponsePayload pm = uploadLog.getApiResponsePayload();
		assertEquals(MessageLevel.INFO.toString(), pm.getApiMessage().getLevel());
		LOG.info(uploadLog.getMessage());
	}
}
