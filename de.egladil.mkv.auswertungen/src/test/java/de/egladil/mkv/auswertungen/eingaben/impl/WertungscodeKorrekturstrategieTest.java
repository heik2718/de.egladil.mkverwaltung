//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * WertungscodeKorrekturstrategieTest
 */
public class WertungscodeKorrekturstrategieTest {

	private final WertungscodeKorrekturstrategie strategie = new WertungscodeKorrekturstrategie();

	@Test
	public void korrigiere_keinTypo_klappt() {
		// Arrange
		final String nutzereingaben = "r,r,f,f,n,r,f,f,r,r,f,n,n,r,n";
		final String expected = "r,r,f,f,n,r,f,f,r,r,f,n,n,r,n";

		// Act
		final String actual = strategie.korrigiere(nutzereingaben);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void korrigiere_typo_klappt() {
		// Arrange
		final String nutzereingaben = "r,r,f,g,n,r,f,f,e,r,f,n";
		final String expected = "r,r,f,f,n,r,f,f,r,r,f,n";

		// Act
		final String actual = strategie.korrigiere(nutzereingaben);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void korrigiere_throws_NullPointerException() {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			strategie.korrigiere(null);
		});
		assertEquals("nutzereingabe", ex.getMessage());

	}

	@Test
	public void korrigiere_behandelt_blank_als_nicht_bearbeitet() {
		assertEquals("n", strategie.korrigiere(""));
	}

	@Test
	public void korrigiere_behandelt_e_als_r() {
		assertEquals("r", strategie.korrigiere("e"));
	}

	@Test
	public void korrigiere_behandelt_r_als_r() {
		assertEquals("r", strategie.korrigiere("r"));
	}

	@Test
	public void korrigiere_behandelt_t_als_r() {
		assertEquals("r", strategie.korrigiere("t"));
	}

	@Test
	public void korrigiere_behandelt_E_als_r() {
		assertEquals("r", strategie.korrigiere("E"));
	}

	@Test
	public void korrigiere_behandelt_R_als_r() {
		assertEquals("r", strategie.korrigiere("R"));
	}

	@Test
	public void korrigiere_behandelt_T_als_r() {
		assertEquals("r", strategie.korrigiere("T"));
	}

	@Test
	public void korrigiere_behandelt_d_als_f() {
		assertEquals("f", strategie.korrigiere("d"));
	}

	@Test
	public void korrigiere_behandelt_f_als_f() {
		assertEquals("f", strategie.korrigiere("f"));
	}

	@Test
	public void korrigiere_behandelt_g_als_f() {
		assertEquals("f", strategie.korrigiere("g"));
	}

	@Test
	public void korrigiere_behandelt_D_als_f() {
		assertEquals("f", strategie.korrigiere("D"));
	}

	@Test
	public void korrigiere_behandelt_F_als_f() {
		assertEquals("f", strategie.korrigiere("F"));
	}

	@Test
	public void korrigiere_behandelt_G_als_f() {
		assertEquals("f", strategie.korrigiere("G"));
	}

	@Test
	public void bewertet_alle_anderen_als_n() {
		final char[] allezeichen = "abchijklmnopqsuvwxyzäöüß0123456789ABCHIJKLMNOPQSUVWXYZÄÖÜ ".toCharArray();
		for (final char c : allezeichen) {
			assertEquals("Fehler bei " + c, "n", strategie.korrigiere(String.valueOf(c)));
		}
	}

	@Test
	public void korrigiere_leer_statt_n_am_anfang(){
		// Arrange
		final String nutzereingaben = ",,,,,,,,,,,f";
		final String expected = "n,n,n,n,n,n,n,n,n,n,n,f";
		assertEquals(expected, strategie.korrigiere(nutzereingaben));
	}

	@Test
	public void korrigiere_leer_statt_n_in_der_mitte(){
		// Arrange
		final String nutzereingaben = ",,,r,,,,,,,,";
		final String expected = "n,n,n,r,n,n,n,n,n,n,n,n";
		assertEquals(expected, strategie.korrigiere(nutzereingaben));
	}

	@Test
	public void korrigiere_leer_statt_n_am_ende(){
		// Arrange
		final String nutzereingaben = "n,f,n,,,,,,,,,";
		final String expected = "n,f,n,n,n,n,n,n,n,n,n,n";
		assertEquals(expected, strategie.korrigiere(nutzereingaben));
	}
}
