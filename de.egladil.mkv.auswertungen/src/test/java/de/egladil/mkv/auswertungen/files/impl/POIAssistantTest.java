//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;

/**
 * POIAssistantTest
 */
public class POIAssistantTest {

	@Test
	public void findAddressWithStringContents_klappt() throws IOException {
		InputStream in = null;
		Workbook workbook = null;
		try {
			// Arrange
			in = getClass().getResourceAsStream(TestHelper.XLS_ZWEI_NAMEN);
			final AbstractAuswertungExcelStreamReader streamReader = (AbstractAuswertungExcelStreamReader) AuswertungStreamReaderFactory
				.create(UploadMimeType.VDN_MSEXCEL);

			workbook = streamReader.getWorkbook(in);
			final Sheet sheet = workbook.getSheetAt(0);

			// Act
			final CellAddress cellAddress = POIAssistant.findAddressWithStringContents(sheet,
				IAuswertungStreamReader.STARTADDRESS_MARKER);

			// Assert
			assertNotNull(cellAddress);
			assertEquals(4, cellAddress.getRow());
			assertEquals(2, cellAddress.getColumn());
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

}
