//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.urkunden.PdfMerger;

/**
 * PdfMergerTest
 */
public class PdfMergerTest {

	private static final String FIRST_PAGE = "/overlay_green.pdf";

	private static final String SECOND_PAGE = "/overlay_orange.pdf";

	private static final String THIRD_PAGE = "/overlay_blue.pdf";

	private static final String DEST_1 = "/home/heike/temp/seiten_1.pdf";

	private static final String DEST_2 = "/home/heike/temp/seiten_2.pdf";

	private static final String DEST_3 = "/home/heike/temp/seiten_3.pdf";

	@Test
	@DisplayName("eine Seite")
	void mergeEineSeite() throws Exception {

		final List<byte[]> pdfs = getPages(Arrays.asList(new String[] { FIRST_PAGE }));
		final byte[] result = new PdfMerger().concatPdf(pdfs);

		try (final FileOutputStream out = new FileOutputStream(DEST_1)) {
			out.write(result);
			out.flush();
			assertTrue(new File(DEST_1).canRead());
		}

	}

	@Test
	@DisplayName("zwei Seiten")
	void mergeTwoPages() throws Exception {
		final List<byte[]> pdfs = getPages(Arrays.asList(new String[] { SECOND_PAGE, THIRD_PAGE }));
		final byte[] result = new PdfMerger().concatPdf(pdfs);

		try (final FileOutputStream out = new FileOutputStream(DEST_2)) {
			out.write(result);
			out.flush();
			assertTrue(new File(DEST_2).canRead());
		}
	}

	@Test
	@DisplayName("drei seiten")
	void mergeRecursive() throws Exception {

		final List<byte[]> pdfs = getPages(Arrays.asList(new String[] { THIRD_PAGE, SECOND_PAGE, FIRST_PAGE }));
		final byte[] result = new PdfMerger().concatPdf(pdfs);

		try (final FileOutputStream out = new FileOutputStream(DEST_3)) {
			out.write(result);
			out.flush();
			assertTrue(new File(DEST_3).canRead());
		}

	}

	@Test
	@DisplayName("IllegalArgumentException wenn null")
	void mergeNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new PdfMerger().concatPdf(null);
		});
		assertEquals("pdfs sind null oder leer", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn leer")
	void mergeLeer() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new PdfMerger().concatPdf(new ArrayList<>());
		});
		assertEquals("pdfs sind null oder leer", ex.getMessage());
	}

	private List<byte[]> getPages(final List<String> classpaths) throws IOException {
		final List<byte[]> result = new ArrayList<>();
		for (final String classpath : classpaths) {
			result.add(getData(classpath));
		}
		return result;
	}

	private byte[] getData(final String scr) throws IOException {
		try (InputStream in = getClass().getResourceAsStream(scr)) {
			final byte[] data = IOUtils.toByteArray(in);
			return data;
		}
	}
}
