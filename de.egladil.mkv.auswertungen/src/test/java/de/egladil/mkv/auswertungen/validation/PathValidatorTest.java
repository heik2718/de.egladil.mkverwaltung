//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import de.egladil.common.validation.PathValidator;
import de.egladil.mkv.auswertungen.TestHelper;
import de.egladil.mkv.persistence.file.DateiValidationStrategie;
import de.egladil.mkv.persistence.file.VerzeichnisValidationStrategy;

/**
 * PathValidatorTest
 */
public class PathValidatorTest {

	@Test
	public void validateFile_wirft_IOException_wenn_datei_nicht_existiert() throws Exception {
		// Arrange
		final String path = TestHelper.getDevNewsletterDir() + "/mailinhalt";

		final Throwable ex = assertThrows(IOException.class, () -> {
			new PathValidator().validate(path, new DateiValidationStrategie());
		});
		assertEquals("/home/heike/git/konfigurationen/mkverwaltung/files/mkvadmin/mails/mailinhalt existiert nicht oder ist keine reguläre Datei", ex.getMessage());

	}

	@Test
	public void validateFile_wirft_IOException_wenn_datei_nicht_gelesen_werden_darf() throws Exception {
		// Arrange
		final String path = TestHelper.getDevNewsletterDir() + "/unreadable";

		// Act + Assert
		final Throwable ex = assertThrows(IOException.class, () -> {
			new PathValidator().validate(path, new DateiValidationStrategie());
		});
		assertEquals("Datei /home/heike/git/konfigurationen/mkverwaltung/files/mkvadmin/mails/unreadable kann nicht geändert werden", ex.getMessage());

	}

	@Test
	public void validateFile_wirft_keine_exception() {
		// Arrange
		final String path = TestHelper.getDevUploadDir() + "/existing.ods";

		// Act + Assert
		try {
			new PathValidator().validate(path, new DateiValidationStrategie());
		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void validateDir_wirft_keine_exception() {
		// Arrange
		final String path = TestHelper.getDevUploadDir();

		// Act + Assert
		try {
			new PathValidator().validate(path, new VerzeichnisValidationStrategy());
		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void validateDir_wirft_IOException_wenn_root() {
		// Arrange
		final String path = "/root/nicht_loeschen";

		// Act + Assert
		final Throwable ex = assertThrows(IOException.class, () -> {
			new PathValidator().validate(path, new VerzeichnisValidationStrategy());
		});
		assertEquals("/root/nicht_loeschen existiert nicht oder ist kein reguläres Verzeichnis", ex.getMessage());
	}
}
