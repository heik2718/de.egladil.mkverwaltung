//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.auswertungen.urkunden.EinzelurkundeGenerator;
import de.egladil.mkv.auswertungen.urkunden.EinzelurkundeGeneratorFactory;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundenmotivProvider;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitText;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitTextBuilder;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * TeilnehmerUrkundeGeneratorTest
 */
public class TeilnehmerUrkundeGeneratorTest {

	private static final String URKUNDE_FILE_EINE_ZEILE = "/home/heike/temp/teilnehmerurkunde_1.pdf";

	private static final String URKUNDE_FILE_ZWEI_ZEILEN = "/home/heike/temp/teilnehmerurkunde_2.pdf";

	private static final String URKUNDE_FILE_DREI_ZEILEN = "/home/heike/temp/teilnehmerurkunde_3.pdf";

	private static final String URKUNDE_FILE_SCHULNAME_UMGRBROCHEN = "/home/heike/temp/schulname.pdf";

	private static final String URKUNDE_WETTBEWERB_EN = "/home/heike/temp/certificate-competition.pdf";


	private UrkundeInhalt inhalt;

	private EinzelurkundeGenerator generator;

	@BeforeEach
	void setUp() {
		generator = EinzelurkundeGeneratorFactory.createTeilnehmerurkundeGenerator();
		final UrkundenmotivMitText motiv = new UrkundenmotivMitTextBuilder("2010",
			DefaultUrkundenmotivProvider.ausFarbschema(Farbschema.ORANGE), "21.11.2017").build();
		inhalt = Mockito.mock(UrkundeInhalt.class);
		Mockito.when(inhalt.getBackgroundImage()).thenReturn(motiv.getBackgroundImage());
		Mockito.when(inhalt.getHeadlineColor()).thenReturn(motiv.getHeadlineColor());
		Mockito.when(inhalt.getDatum()).thenReturn("21.11.2017");
		Mockito.when(inhalt.getKaengurusprung()).thenReturn(5);
		Mockito.when(inhalt.getKlassenname()).thenReturn("Fuchsklasse");
		Mockito.when(inhalt.getSchulname()).thenReturn("Krautgartenschule Mainz-Kostheim");
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");
		Mockito.when(inhalt.getWettbewerbsjahr()).thenReturn("2017");
		Mockito.when(inhalt.getPunkteText()).thenReturn("43,5");
		Mockito.when(inhalt.getTeilnehmerKuerzel()).thenReturn("ZRHL8DG620170615173734");
		Mockito.when(inhalt.getSprache()).thenReturn(Sprache.de);
	}

	@Test
	void generateForThumbnails() throws Exception {
		for (final Farbschema farbschema : Farbschema.values()) {
			final UrkundenmotivMitText motiv = new UrkundenmotivMitTextBuilder("2018",
				DefaultUrkundenmotivProvider.ausFarbschema(farbschema), "14.03.2018").build();
			Mockito.when(inhalt.getBackgroundImage()).thenReturn(motiv.getBackgroundImage());
			Mockito.when(inhalt.getHeadlineColor()).thenReturn(motiv.getHeadlineColor());
			Mockito.when(inhalt.getDatum()).thenReturn("14.03.2018");
			Mockito.when(inhalt.getKaengurusprung()).thenReturn(7);
			Mockito.when(inhalt.getKlassenname()).thenReturn("Klasse 2c");
			Mockito.when(inhalt.getSchulname()).thenReturn("David-Hilbert-Schule");
			Mockito.when(inhalt.getTeilnehmername()).thenReturn("Albert Einstein");
			Mockito.when(inhalt.getWettbewerbsjahr()).thenReturn("2018");
			Mockito.when(inhalt.getPunkteText()).thenReturn("67,25");
			Mockito.when(inhalt.getTeilnehmerKuerzel()).thenReturn("ZRHL8DG620170615173734");

			final String filepath = "/home/heike/temp/urkunde_" + farbschema.toString().toLowerCase() + ".pdf";
			final byte[] pdf = generator.generate(inhalt);
			assertNotNull(pdf);

			try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
				final FileOutputStream fos = new FileOutputStream(filepath)) {
				IOUtils.copy(bis, fos);
				fos.flush();
			}

			assertTrue(new File(filepath).canRead());
		}
	}

	@Test
	@DisplayName("generieren zwei Zeilen")
	void englischGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");
		Mockito.when(inhalt.getSprache()).thenReturn(Sprache.en);
		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_WETTBEWERB_EN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_ZWEI_ZEILEN).canRead());
	}



	@Test
	@DisplayName("generieren eine Zeile")
	void nameEineZeileGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("ŚśŞşŠšȘșṠṡṢṣ ÝýŸÿŶŷȲȳ");
//		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Allmannsdörffer");

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_EINE_ZEILE)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_EINE_ZEILE).canRead());
	}

	@Test
	@DisplayName("generieren zwei Zeilen")
	void nameZweiZeilenGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername()).thenReturn("Marie Luise Allmannsdörffer- Langenscheidt");

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_ZWEI_ZEILEN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_ZWEI_ZEILEN).canRead());
	}

	@Test
	@DisplayName("generieren drei Zeilen")
	void nameDreiZeilenGenerieren() throws IOException {
		Mockito.when(inhalt.getTeilnehmername())
			.thenReturn("Alexander Maximilian Edmundo Theodor von Allmannsdörffer- Langenscheidt Wa");

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_DREI_ZEILEN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_DREI_ZEILEN).canRead());
	}

	@Test
	@DisplayName("generieren drei Zeilen")
	void nameZuLang() throws IOException {
		Mockito.when(inhalt.getTeilnehmername())
			.thenReturn("Alexander Maximilian Edmundo Theodor von Allmannsdörffer- Langenscheidt Was");

		final Throwable ex = assertThrows(MKVException.class, () -> {
			generator.generate(inhalt);
		});
		assertEquals("IllegalArgumentException beim generieren von ZRHL8DG620170615173734: name passt nicht in drei Zeilen",
			ex.getMessage());
	}

	@Test
	@DisplayName("generieren Zeilenumbruch im Schulnamen")
	void schulnameZeilenumbruchGenerieren() throws IOException {
		Mockito.when(inhalt.getSchulname()).thenReturn("Neue Grundschule Potsdam anerkannte Ersatzschule");

		final byte[] pdf = generator.generate(inhalt);
		assertNotNull(pdf);

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(URKUNDE_FILE_SCHULNAME_UMGRBROCHEN)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(URKUNDE_FILE_SCHULNAME_UMGRBROCHEN).canRead());
	}
}
