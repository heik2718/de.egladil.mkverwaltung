//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.auswertungen.domain.Punktintervall;
import de.egladil.mkv.auswertungen.domain.Punktintervall.Builder;

/**
* PunktintervallInklusionFactoryTest
*/
public class PunktintervallInklusionFactoryTest {

	private PunktintervallInklusionFactory factory = new PunktintervallInklusionFactory();

	@Test
	public void create_throws_when_minVal_negative() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			factory.createPunktintervall(-1);
		});
		assertEquals("minVal muss zwischen 0 und 3600 liegen: minVal=-1", ex.getMessage());

	}

	@Test
	public void create_throws_when_minVal_too_large() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			factory.createPunktintervall(3601);
		});
		assertEquals("minVal muss zwischen 0 und 3600 liegen: minVal=3601", ex.getMessage());

	}

	@Test
	public void create_produces_correct_intervalls() {
		{
			final Punktintervall intervall = factory.createPunktintervall(3600);
			assertEquals(3600, intervall.getMaxVal());
		}
		{
			final Punktintervall intervall = factory.createPunktintervall(3000);
			assertEquals(3300, intervall.getMaxVal());
		}
	}

	@Test
	public void getPunktintervalleDescending_klappt() {
		final List<Punktintervall> actual = factory.getPunktintervalleDescending();
		final List<Punktintervall> expected = getExpectedPunktintervalle();
		assertEquals(expected.size(), actual.size());
		for (int i = 0; i < expected.size(); i++) {
			assertEquals("Fehler bei " + i, expected.get(i), actual.get(i));
		}
	}

	private List<Punktintervall> getExpectedPunktintervalle() {
		final List<Punktintervall> result = new ArrayList<>();
		result.add(new Builder(3600).maxVal(3600).build());
		result.add(new Builder(3000).maxVal(3300).build());
		result.add(new Builder(2500).maxVal(2900).build());
		result.add(new Builder(2000).maxVal(2450).build());
		result.add(new Builder(1500).maxVal(1950).build());
		result.add(new Builder(1000).maxVal(1450).build());
		result.add(new Builder(500).maxVal(950).build());
		result.add(new Builder(0).maxVal(450).build());
		return result;
	}
}
