//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;


/**
 * UploadUtilsTest
 */
public class UploadUtilsTest {

	@Test
	public void getMimeType_throws_when_parameter_null() throws IOException {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new UploadUtils().getMimeType(null);
		});
		assertEquals("bytes", ex.getMessage());

	}

    @Test
	public void readBytes_file_throws_when_parameter_null() throws IOException {
    	final File file = null;
    	final Throwable ex = assertThrows(NullPointerException.class, () -> {
    		new UploadUtils().readBytes(file);
		});
		assertEquals("Parameter in", ex.getMessage());

	}

    @Test
	public void readBytes_instream_throws_when_parameter_null() throws IOException {
    	final InputStream in = null;
    	final Throwable ex = assertThrows(NullPointerException.class, () -> {
    		new UploadUtils().readBytes(in);
		});
		assertEquals("in", ex.getMessage());

	}

	@Test
	public void getChecksum_throws_when_parameter_null() throws Exception {
		final Throwable ex = assertThrows(NullPointerException.class, () -> {
			new UploadUtils().getChecksum(null);
		});
		assertEquals("bytes", ex.getMessage());

	}

	@Test
	public void getChecksum_klappt() {
		// Arrange
		final String str = "Ggagdgawzioic898aß asiod a9sa908";
		final byte[] bytes = str.getBytes();
		final String expected = "a5ba7df6bb5ba459b036e0e5625dd6667cd3c484127c69f5644e3a658167e480";

		// Act
		final String checksum = new UploadUtils().getChecksum(bytes);

		// Assert
		assertEquals(expected, checksum);
	}


}