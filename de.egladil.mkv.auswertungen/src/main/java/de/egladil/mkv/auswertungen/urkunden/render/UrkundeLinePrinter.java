//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * UrkundeLinePrinter
 */
public class UrkundeLinePrinter {

	private final static Logger LOG = LoggerFactory.getLogger(UrkundeLinePrinter.class);

	public void printTextLeft(final PdfContentByte content, final String text, final Font font, final int deltaY) {
		printText(content, text, font, UrkundeConstants.X_LEFT, deltaY);
	}

	public void printTextCenter(final PdfContentByte content, final String text, final Font font, final int deltaY) {
		printText(content, text, font, UrkundeConstants.X_CENTER, deltaY);
	}

	public void printText(final PdfContentByte content, final String text, final Font font, final int xPos, final int deltaY) {
		final Phrase p = new Phrase(text, font);
		final int yAbsolut = UrkundeConstants.Y_TOP - deltaY;
		ColumnText.showTextAligned(content, Element.ALIGN_LEFT, p, xPos, yAbsolut, 0);
		LOG.debug(text + ": " + yAbsolut);
	}

}
