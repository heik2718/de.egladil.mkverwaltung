//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.persistence;

/**
 * IUploadListener
 */
public interface IUploadListener {

	/**
	 * Gibt den Upload zur Verarbeitung weiter.
	 *
	 * @param uploadId Long die technische ID des uploads
	 * @param uploadDirPath String absoluter Pfad zum Verzeichnis mit den hochgeladenen Dateien.
	 */
	void processUpload(Long uploadId, String uploadDirPath);
}
