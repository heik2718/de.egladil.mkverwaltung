//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

/**
 * ITikaWrapper
 */
public interface ITikaWrapper {

	String detect(byte[] bytes);

}
