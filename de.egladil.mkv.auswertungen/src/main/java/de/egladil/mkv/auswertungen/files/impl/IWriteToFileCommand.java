package de.egladil.mkv.auswertungen.files.impl;

import java.io.IOException;

public interface IWriteToFileCommand {

	/**
	 * Schreibt die gegebenen bytes in die durch pathName gegebene Datei. Rückgabe ist nicht void wegen Testbarkeit mit
	 * Mockito.
	 *
	 * @param absPath String der absoluten Pfadnamen der zu erzeugenden oder überschreibenden Datei.
	 * @throws IOException
	 * @throws NullPointerException wenn einer der Parameter null ist.
	 */
	void writeToFile(String absPath) throws IOException;
}

