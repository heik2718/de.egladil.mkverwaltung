//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import org.apache.commons.lang3.StringUtils;

import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * AuswertungAusWertungBerechnungsstrategie erzeugt einen Loesungszettel auf der Grundlage von Wertung-Einträgen.
 */
public class AuswertungAusWertungBerechnungsstrategie extends AbstractAuswertungBerechnungsstategie {

	private final INutzereingabeKorrekturstrategie korrekturstrategie;

	/**
	 * Erzeugt eine Instanz von AuswertungAusWertungBerechnungsstrategie
	 */
	public AuswertungAusWertungBerechnungsstrategie() {
		this.korrekturstrategie = new WertungscodeKorrekturstrategie();
	}

	/**
	* @see de.egladil.mkv.auswertungen.eingaben.impl.AbstractAuswertungBerechnungsstategie#berechneWertungscode(java.lang.String, Klassenstufe)
	*/
	@Override
	protected String berechneWertungscode(String korrigierteBenutzereingabe, Klassenstufe klassenstufe) {
		if (korrigierteBenutzereingabe == null){
			throw new NullPointerException("korrigierteBenutzereingabe");
		}
		return StringUtils.remove(korrigierteBenutzereingabe, ',');
	}



	/**
	 * @see de.egladil.mkv.auswertungen.eingaben.impl.AbstractAuswertungBerechnungsstategie#getKorrekturstrategie()
	 */
	@Override
	protected INutzereingabeKorrekturstrategie getKorrekturstrategie() {
		return korrekturstrategie;
	}

	/**
	* @see de.egladil.mkv.auswertungen.eingaben.impl.AbstractAuswertungBerechnungsstategie#getCorrespondingWorkbookKategorie()
	*/
	@Override
	public WorkbookKategorie getCorrespondingWorkbookKategorie() {
		return WorkbookKategorie.WERTUNGSBUCHSTABEN;
	}
}
