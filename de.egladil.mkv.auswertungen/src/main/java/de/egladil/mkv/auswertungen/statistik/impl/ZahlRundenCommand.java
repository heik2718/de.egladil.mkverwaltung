//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * ZahlRundenCommand rundet ein double auf eine gewünschte Anzahl Nachkommastellen.
 */
public class ZahlRundenCommand {

	/**
	 *
	 * @param zahl double
	 * @param anzahlNachkommastellen int muss nichtnegativ sein.
	 * @return double
	 * @throws IllegalArgumentException
	 */
	public double rundeAufAnzahlNachkommastellen(double zahl, int anzahlNachkommastellen) {
		if (anzahlNachkommastellen < 0) {
			throw new IllegalArgumentException("anzahlNachkommastellen muss nichtnegativ sein.");
		}
		return new BigDecimal(zahl).setScale(anzahlNachkommastellen, RoundingMode.HALF_UP).doubleValue();
	}

}
