//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import java.util.Map;

import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.persistence.berechnungen.WertungscodeCommand;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * AuswertungAusAntwortbuchstabeBerechnungsstrategie
 */
public class AuswertungAusAntwortbuchstabeBerechnungsstrategie extends AbstractAuswertungBerechnungsstategie {

	private final AntwortbuchstabenKorrekturstrategie korrekturstrategie;

	private final Map<Klassenstufe, String> loesungscodes;

	/**
	 *
	 * Erzeugt eine Instanz von AuswertungAusAntwortbuchstabeBerechnungsstrategie
	 */
	public AuswertungAusAntwortbuchstabeBerechnungsstrategie(Map<Klassenstufe, String> loesungscodes) {
		if (loesungscodes == null) {
			throw new NullPointerException("Parameter loesungscodes");
		}
		this.korrekturstrategie = new AntwortbuchstabenKorrekturstrategie();
		this.loesungscodes = loesungscodes;
	}

	/**
	 * @see de.egladil.mkv.auswertungen.eingaben.impl.AbstractAuswertungBerechnungsstategie#getKorrekturstrategie()
	 */
	@Override
	protected INutzereingabeKorrekturstrategie getKorrekturstrategie() {
		return korrekturstrategie;
	}

	/**
	 * @see de.egladil.mkv.auswertungen.eingaben.impl.AbstractAuswertungBerechnungsstategie#berechneWertungscode(java.lang.String,
	 * Klassenstufe)
	 */
	@Override
	protected String berechneWertungscode(String korrigierteBenutzereingabe, Klassenstufe klassenstufe) {
		if (korrigierteBenutzereingabe == null) {
			throw new NullPointerException("korrigierteBenutzereingabe");
		}
		String loesungscode = loesungscodes.get(klassenstufe);
		return new WertungscodeCommand(loesungscode).berechneWertungscode(korrigierteBenutzereingabe);
	}

	/**
	 * @see de.egladil.mkv.auswertungen.eingaben.impl.AbstractAuswertungBerechnungsstategie#getCorrespondingWorkbookKategorie()
	 */
	@Override
	public WorkbookKategorie getCorrespondingWorkbookKategorie() {
		return WorkbookKategorie.ANTWORTBUCHSTABEN;
	}
}
