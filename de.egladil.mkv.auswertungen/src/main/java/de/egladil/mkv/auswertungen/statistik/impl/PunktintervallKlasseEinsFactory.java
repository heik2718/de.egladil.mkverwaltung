//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import de.egladil.mkv.auswertungen.domain.Punktintervall;

/**
 * PunktintervallKlasseEinsFactory
 */
public class PunktintervallKlasseEinsFactory extends PunktintervallFactory {

	/**
	 * Erzeugt eine Instanz von PunktintervallKlasseEinsFactory
	 */
	PunktintervallKlasseEinsFactory() {
	}

	@Override
	protected int anzahlIntervalle() {
		return 13;
	}

	@Override
	protected Punktintervall createPunktintervall(final int minVal) {
		if (minVal < 0 || minVal > 6000) {
			throw new IllegalArgumentException("minVal muss zwischen 0 und 6000 liegen: minVal=" + minVal);
		}
		int obere = minVal + Punktintervall.DEFAULT_LAENGE;
		switch (obere) {
		case 5475:
			obere = 5400;
			break;
		case 5975:
			obere = 5700;
			break;
		default:
			break;
		}
		if (obere > 6000) {
			obere = 6000;
		}
		return new Punktintervall.Builder(minVal).maxVal(obere).build();
	}

}
