//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * AuswertungDaten
 */
public class AuswertungDaten {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungDaten.class);

	private Map<Klassenstufe, List<UrkundeInhalt>> teilnehmerurkunden = new HashMap<>();

	private Map<Klassenstufe, List<UrkundeInhalt>> kaengurusprungUrkunden = new HashMap<>();

	private Map<Klassenstufe, Integer> anzahlenKaengurugewinner = new HashMap<>();

	private Map<Klassenstufe, Integer> anzahlenTeilnehmer = new HashMap<>();


	public void addAnzahlKaenurugewinner(final Klassenstufe klassenstufe, final int anzahl) {
		this.anzahlenKaengurugewinner.put(klassenstufe, anzahl);
	}

	public void addTeilnehmerurkunde(final Klassenstufe klassenstufe, final UrkundeInhalt teilnehmerurkunde) {
		final List<UrkundeInhalt> urkunden = teilnehmerurkunden.getOrDefault(klassenstufe, new ArrayList<>());
		urkunden.add(teilnehmerurkunde);
		this.teilnehmerurkunden.put(klassenstufe, urkunden);
		int anz = anzahlenTeilnehmer.getOrDefault(klassenstufe, 0);
		anz++;
		anzahlenTeilnehmer.put(klassenstufe, anz);
	}

	public void addAllTeilnehmerurkunde(final Klassenstufe klassenstufe, final List<UrkundeInhalt> urkunden) {
		if (this.teilnehmerurkunden.get(klassenstufe) != null && !this.teilnehmerurkunden.get(klassenstufe).isEmpty()) {
			LOG.warn("Klassenstufe {} wird überschrieben!!!", klassenstufe);
		}
		this.teilnehmerurkunden.put(klassenstufe, urkunden);
		anzahlenTeilnehmer.put(klassenstufe, urkunden.size());
	}

	public void addKaenguruUrkunde(final Klassenstufe klassenstufe, final UrkundeInhalt urkunde) {
		final List<UrkundeInhalt> urkunden = kaengurusprungUrkunden.getOrDefault(klassenstufe, new ArrayList<>());
		urkunden.add(urkunde);
		kaengurusprungUrkunden.put(klassenstufe, urkunden);
	}

	public void addAllKaenguruUrkunde(final Klassenstufe klassenstufe, final Collection<UrkundeInhalt> urkunden) {
		final List<UrkundeInhalt> urkundenKlassenstufe = kaengurusprungUrkunden.getOrDefault(klassenstufe, new ArrayList<>());
		urkundenKlassenstufe.addAll(urkunden);
		kaengurusprungUrkunden.put(klassenstufe, urkundenKlassenstufe);
	}

	public final Map<Klassenstufe, List<UrkundeInhalt>> getTeilnehmerurkunden() {
		return teilnehmerurkunden;
	}

	public final Map<Klassenstufe, List<UrkundeInhalt>> getKaengurusprungUrkunden() {
		return kaengurusprungUrkunden;
	}

	public List<UrkundeInhalt> getUrkundenFuerKlassenstufeSorted(final Klassenstufe klassenstufe) {
		final List<UrkundeInhalt> result = teilnehmerurkunden.get(klassenstufe);
		if (result != null) {
			Collections.sort(result, new TeilnehmerUrkundeKlassenstufeComparator());
		}
		return result;
	}

	public List<UrkundeInhalt> getKaenguruspruengeFuerKlassenstufeSorted(final Klassenstufe klassenstufe) {
		final List<UrkundeInhalt> result = kaengurusprungUrkunden.get(klassenstufe);
		if (result != null) {
			Collections.sort(result, new KaengurusprungUrkundeKlassenstufeComparator());
		}
		return result;
	}

	final void setTeilnehmerurkunden(final Map<Klassenstufe, List<UrkundeInhalt>> teilnehmerurkunden) {
		this.teilnehmerurkunden = teilnehmerurkunden;
	}

	final void setKaengurusprungUrkunden(final Map<Klassenstufe, List<UrkundeInhalt>> kaengurusprungUrkunden) {
		this.kaengurusprungUrkunden = kaengurusprungUrkunden;
	}

	public int getAnzahlKaengurugewinner(final Klassenstufe klassenstufe) {
		return anzahlenKaengurugewinner.getOrDefault(klassenstufe, 0);
	}

	public int getAnzahlTeilnehmer(final Klassenstufe klassenstufe) {
		return anzahlenTeilnehmer.getOrDefault(klassenstufe, 0);
	}

	public int getAnzahlKaengurugewinner() {
		return anzahlenKaengurugewinner.values().stream().mapToInt(Integer::intValue).sum();
	}

	public boolean shallPrintWarnungKaengurusprung() {
		final OptionalInt optInt = anzahlenKaengurugewinner.values().stream().mapToInt(Integer::intValue).max();
		if (optInt.isPresent()) {
			return optInt.getAsInt() > 1;
		}
		return false;
	}
}
