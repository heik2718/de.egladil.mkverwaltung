//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.domain.AufgabeErgebnisItem;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.urkunden.UebersichtFontProvider;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AufgabenuebersichtPDFGenerator
 */
public class AufgabenuebersichtPDFGenerator {

	private final UebersichtFontProvider fontProvider;

	private final PdfCellRenderer cellRendererRight = new PdfCellRightTextRenderer();

	private final PdfCellRenderer cellRendererCenter = new PdfCellCenteredTextRenderer();

	/**
	 * AufgabenuebersichtPDFGenerator
	 */
	public AufgabenuebersichtPDFGenerator() {
		this.fontProvider = new UebersichtFontProvider();
	}

	/**
	 * Generiert eine Tabelle mit den Aufgabenergebnissen.
	 *
	 * @param aufgabenergebnisse
	 * @return
	 */
	public byte[] generiere(final GesamtpunktverteilungDaten daten, final String klassenname) {

		final Document doc = new Document(PageSize.A4);
		final int numCols = 7;

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

			PdfWriter.getInstance(doc, out);
			doc.open();

			final PdfPTable table = new PdfPTable(numCols);

			final Font font = fontProvider.getFontBold();
			final PdfPCell cell = cellRendererCenter.createCell(font, "Aufgabenübersicht " + klassenname, numCols);
			table.addCell(cell);

			addHeaders(table);

			for (final AufgabeErgebnisItem tup : daten.getAufgabeErgebnisItemsSorted()) {
				addRow(table, tup);
			}

			doc.add(table);
			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}

	private void addHeaders(final PdfPTable table) {
		final Font font = fontProvider.getFontBold();

		final String[] headings = new String[] { "Aufgabe", "richtig", "% richtig", "falsch", "% falsch", "nicht",
			"% nicht" };

		for (final String text : headings) {
			final PdfPCell cell = cellRendererCenter.createCell(font, text, 1);
			table.addCell(cell);
		}
	}

	private void addRow(final PdfPTable table, final AufgabeErgebnisItem item) {
		final Font font = fontProvider.getFontNormal();
		PdfPCell cell = cellRendererRight.createCell(font, item.getNummer(), 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, String.valueOf(item.getAnzahlRichtigGeloest()), 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, item.getAnteilRichtigText(), 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, String.valueOf(item.getAnzahlFalschGeloest()), 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, item.getAnteilFalschText(), 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, String.valueOf(item.getAnzahlNichtGeloest()), 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, item.getAnteilNichtGeloestText(), 1);
		table.addCell(cell);

	}

}
