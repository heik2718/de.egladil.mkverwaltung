//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * HeaderRenderer
 */
public class HeaderRenderer implements UrkundeAbschnittRenderer {

	private static final String HEADER_TEXT_DE = "Beim Mathewettbewerb";

	private static final String HEADER_TEXT_EN = "Math competition";

	private final UrkundeLinePrinter linePrinter;

	/**
	 * HeaderRenderer
	 */
	public HeaderRenderer() {
		linePrinter = new UrkundeLinePrinter();
	}

	@Override
	public int printAbschnittAndShiftVerticalPosition(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		if (Sprache.en == urkundeInhalt.getSprache()) {
			linePrinter.printTextLeft(content, HEADER_TEXT_EN, UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_SMALL), 0);
		} else {
			linePrinter.printTextLeft(content, HEADER_TEXT_DE, UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_SMALL), 0);
		}
		return 110;
	}
}
