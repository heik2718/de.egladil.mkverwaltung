//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.util.List;

/**
 * LineWrapStrategy
 */
public interface LineWrapStrategy {

	/**
	 * Bricht den gegebenen namen um oder auch nicht.
	 *
	 * @param name String
	 * @return List die Zeilen
	 */
	List<String> breakLines(final String name);

}
