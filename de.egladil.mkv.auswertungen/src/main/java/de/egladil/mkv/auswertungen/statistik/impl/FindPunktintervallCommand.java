//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.Collection;
import java.util.Optional;

import de.egladil.mkv.auswertungen.domain.Punktintervall;

/**
 * FindPunktintervallCommand
 */
public class FindPunktintervallCommand {

	private final Collection<Punktintervall> alleIntervalle;

	/**
	 * Erzeugt eine Instanz von FindPunktintervallCommand
	 */
	public FindPunktintervallCommand(Collection<Punktintervall> alleIntervalle) {
		this.alleIntervalle = alleIntervalle;
	}

	/**
	 * Sucht das Punktintervall, zu dem diese Punktzahl gehört.
	 *
	 * @param punkte
	 * @return Optional
	 */
	public Optional<Punktintervall> find(int punkte) {
		for (Punktintervall intervall : alleIntervalle) {
			if (intervall.contains(punkte)) {
				return Optional.of(intervall);
			}
		}
		return Optional.empty();
	}

}
