//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;

/**
 * TeilnehmerUndPunkteLoesungszettelAdapter
 */
public class TeilnehmerUndPunkteLoesungszettelAdapter implements ILoesungszettel {

	private final TeilnehmerUndPunkte teilnehmerUndPunkte;

	/**
	 * TeilnehmerUndPunkteLoesungszettelAdapter
	 */
	public TeilnehmerUndPunkteLoesungszettelAdapter(final TeilnehmerUndPunkte teilnehmerUndPunkte) {
		this.teilnehmerUndPunkte = teilnehmerUndPunkte;
	}

	@Override
	public Integer getPunkte() {
		return teilnehmerUndPunkte.getPunktzahl();
	}

	@Override
	public String getWertungscode() {
		return teilnehmerUndPunkte.getBerechneterWertungscode();
	}

	@Override
	public Integer getKaengurusprung() {
		return teilnehmerUndPunkte.getKaengurusprung();
	}

	public String getName() {
		return this.teilnehmerUndPunkte.getName();
	}

	public String getZusatz() {
		return this.teilnehmerUndPunkte.getZusatz();
	}
}
