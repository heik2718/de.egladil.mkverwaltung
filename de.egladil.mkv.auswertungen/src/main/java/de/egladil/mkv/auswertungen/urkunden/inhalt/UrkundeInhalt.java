//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import java.io.InputStream;

import com.itextpdf.text.BaseColor;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * UrkundeInhalt stellt die Daten zum Drucken einer AuswertungDownload zur Verfügung.
 */
public interface UrkundeInhalt {

	BaseColor getHeadlineColor();

	InputStream getBackgroundImage();

	String getDatum();

	int getKaengurusprung();

	String getPunkteText();

	String getTeilnehmername();

	String getZusatz();

	String getKlassenname();

	String getSchulname();

	String getWettbewerbsjahr();

	String getTeilnehmerKuerzel();

	Klassenstufe getKlassenstufe();

	int getPunkte();

	String getAntwortcode();

	String getWertungscode();

	Sprache getSprache();
}
