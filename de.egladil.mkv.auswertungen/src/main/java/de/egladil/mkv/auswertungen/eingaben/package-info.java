//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

/**
 * Klassen, die die speziellen Nutzereingaben auswertuen und daraus Rohdaten für die Auswertungen erzeugen.
 */
package de.egladil.mkv.auswertungen.eingaben;