//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import de.egladil.tools.parser.OpenOfficeTableElement;

/**
 * IOpenOfficeParsingState
 */
public interface IOpenOfficeParsingState {

	/**
	 * @param tableRow
	 */
	void handle(OpenOfficeTableElement tableRow);

	/**
	 *
	 * @return
	 */
	boolean isFinished();

}
