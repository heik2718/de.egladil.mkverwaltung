//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenschluessel;
import de.egladil.mkv.auswertungen.statistik.gruppen.LoesungszettelKlassenstufeProvider;
import de.egladil.mkv.auswertungen.statistik.gruppen.Rootgruppeuebersicht;
import de.egladil.mkv.auswertungen.urkunden.PdfMerger;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * SchuluebersichtAktuellesJahrPDFGenerator
 */
public class SchuluebersichtAktuellesJahrPDFGenerator extends BaseSchuluebersichtPDFGenerator {

	private final KlassenuebersichtPDFGenerator klassenuebersichtGenerator;

	/**
	 * SchuluebersichtAktuellesJahrPDFGenerator
	 */
	public SchuluebersichtAktuellesJahrPDFGenerator() {
		super(KontextReader.getInstance().getKontext().getWettbewerbsjahr());
		this.klassenuebersichtGenerator = new KlassenuebersichtPDFGenerator();
	}

	public byte[] generiere(final Rootgruppeuebersicht rootgruppeuebersicht, final String schulname) {

		final List<byte[]> seiten = new ArrayList<>();

		seiten.add(generiereDeckblatt(rootgruppeuebersicht));

		for (final Klassenschluessel klassenschluessel : rootgruppeuebersicht.getKeysSorted()) {
			final List<ILoesungszettel> schuelerinnen = new ArrayList<>();
			schuelerinnen.addAll(rootgruppeuebersicht.getKlassenliste(klassenschluessel));
			final byte[] klassenuebersicht = klassenuebersichtGenerator
				.generiere(rootgruppeuebersicht.getKlassenubersicht(klassenschluessel));
			seiten.add(klassenuebersicht);

			if (schuelerinnen.size() > 1) {
				final byte[] aufgabenuebersicht = generiereAufgabenuebersicht(klassenschluessel.getKlassenstufe(),
					klassenschluessel.getKlassenname(), schuelerinnen);
				seiten.add(aufgabenuebersicht);
			}
		}

		final byte[] result = new PdfMerger().concatPdf(seiten);
		return result;
	}

	private byte[] generiereAufgabenuebersicht(final Klassenstufe klassenstufe, final String klassenname,
		final List<ILoesungszettel> loesungszettel) {

		final GesamtpunktverteilungDaten daten = getVerteilungGenerator().berechneGesamtpunktverteilungDaten(loesungszettel,
			klassenstufe, getWettbewerbsjahr());
		final byte[] aufgabenuebersicht = getAufgabenuebersichtGenerator().generiere(daten, klassenname);
		return aufgabenuebersicht;
	}

	@Override
	protected byte[] generiereDeckblatt(final LoesungszettelKlassenstufeProvider rootgruppeuebersicht) {

		final String medianInklusion = getMedian(rootgruppeuebersicht, Klassenstufe.IKID);
		final String medianKlasseEins = getMedian(rootgruppeuebersicht, Klassenstufe.EINS);
		final String medianKlasseZwei = getMedian(rootgruppeuebersicht, Klassenstufe.ZWEI);

		final Document doc = new Document(PageSize.A4);

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

			PdfWriter.getInstance(doc, out);
			doc.open();

			final Paragraph element = getTitle(rootgruppeuebersicht);
			element.setAlignment(Element.ALIGN_CENTER);
			doc.add(element);

			final Font font = getFontProvider().getFontNormal();
			doc.add(Chunk.NEWLINE);

			if (medianKlasseEins != null) {
				doc.add(new Paragraph("Schulmedian Klasse 1: " + medianKlasseEins, font));
			}
			if (medianKlasseZwei != null) {
				doc.add(new Paragraph("Schulmedian Klasse 2: " + medianKlasseZwei, font));
			}
			if (medianInklusion != null) {
				doc.add(new Paragraph("Schulmedian Inklusion: " + medianInklusion, font));
			}

			doc.add(Chunk.NEWLINE);
			doc.add(getParagraphMediandefinition());

			doc.add(Chunk.NEWLINE);
			if (rootgruppeuebersicht.anzahlKinder() > 1) {
				doc.add(new Paragraph(getInhaltsangabe(), font));
			}

			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}

	@Override
	protected String getInhaltsangabe() {
		return "Die folgenden Seiten zeigen je Klasse die Wertungen der Kinder und, wenn es mehr als ein Kind in der Klasse gibt, die Übersicht über die gelösten Aufgaben.";
	}
}
