//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.auswertungen.files.impl.UploadUtils;
import de.egladil.mkv.auswertungen.files.impl.WriteBytesToFileCommand;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;
import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * AuswertungstabelleFileWriter macht security checks und schreibt die gegebenen Daten in das Upload-Verzeihchnis für
 * hochgeldene Auswertungen.
 */
@Singleton
public class AuswertungstabelleFileWriter {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungstabelleFileWriter.class);

	private final IProtokollService protokollService;

	private UploadUtils uploadUtils = new UploadUtils();

	private String pathUploadDir;

	private String pathSandboxDir;

	/**
	 * Erzeugt eine Instanz von AuswertungstabelleFileWriter
	 */
	@Inject
	public AuswertungstabelleFileWriter(final IProtokollService protokollService) {
		this.protokollService = protokollService;
	}

	public AuswertungstabelleFileWriter initPaths(final String pathUploadDir, final String pathSandboxDir) {
		this.pathUploadDir = pathUploadDir;
		this.pathSandboxDir = pathSandboxDir;
		return this;
	}

	/**
	 *
	 * Es wird ein Upload erzeugt, der die Attribute
	 *
	 * @param data
	 * @param teilnahmeIdentifier
	 * @param benutzerUuid
	 * @return
	 * @throws ParserSecurityException
	 * @throws NullPointerException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public Upload writeFile(final byte[] data, final TeilnahmeIdentifier teilnahmeIdentifier, final String benutzerUuid)
		throws ParserSecurityException, NullPointerException, IOException {

		final String mimeType = uploadUtils.getMimeType(data);
		final String checksum = uploadUtils.getChecksum(data);
		final UploadMimeType uploadType = UploadMimeType.valueOf(mimeType);
		final String dateiname = AuswertungstabellenUtils.getDateiname(teilnahmeIdentifier, checksum, uploadType);

		if (UploadMimeType.UNKNOWN == uploadType) {
			LOG.warn("Unbekannten MIME-TYPE {} erhalten. Verschiebe Datei {} in Sandbox für später.", mimeType, dateiname);
		}

		AuswertungstabellenUtils.checkUploadMimetype(benutzerUuid, uploadType, mimeType, dateiname, protokollService);

		String absolutePathName = getAbsolutePathName(uploadType, dateiname);
		new WriteBytesToFileCommand(data).writeToFile(absolutePathName);

		Upload upload = Upload.createStatusNeuByDateiInfos(uploadType, checksum, dateiname);

		return upload;
	}

	/**
	 * Versucht, die Datei wieder zu löschen. Alle Exceptions werden gefangen und geloggt.
	 *
	 * @param absolutePathName String absoluter Pfad zur Datei. Falls er null ist, wird nichts getan.
	 */
	public void deleteDateiQuietly(final UploadMimeType uploadType, final String dateiname) {
		if (dateiname == null || uploadType == null) {
			LOG.warn("uploadType oder dateiname null: Es wird nichts gelöscht.");
			return;
		}
		String absolutePathName = getAbsolutePathName(uploadType, dateiname);
		try {
			final Path path = Paths.get(absolutePathName);
			Files.delete(path);
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DATENMUELL + "beim Upload. Datei {} konnten nicht gelöscht werden. {}",
				absolutePathName, e.getMessage());
		}
	}

	private String getAbsolutePathName(final UploadMimeType uploadType, final String dateiname) {
		String absolutePathName = null;
		if (UploadMimeType.UNKNOWN == uploadType) {
			absolutePathName = pathSandboxDir + File.separator + dateiname;
		} else {
			absolutePathName = pathUploadDir + File.separator + dateiname;
		}
		return absolutePathName;
	}

	void renameFileQuietly(final UploadInfo uploadInfo, final String uploadDirPath, final String extension) {
		String oldFileName = uploadInfo.getDateiname();
		this.renameFileQuietly(pathUploadDir, oldFileName, extension);
	}

	/**
	 * Die Datei im Verzeichnis uploadDirPath wird umbenannt, indem an ihren Namen die extension angehängt wird.
	 * Exceptions werden gefangen und geloggt.
	 *
	 * @param uploadDirPath
	 * @param oldFilename
	 * @param extension
	 */
	public void renameFileQuietly(final String uploadDirPath, final String oldFilename, final String extension) {
		final String absPath = uploadDirPath + File.separator + oldFilename;
		final Path source = Paths.get(absPath);
		try {
			Files.move(source, source.resolveSibling(absPath + extension));
		} catch (final IOException e) {
			LOG.warn("File konnte nicht umbenannt werden: {} - {}", e.getClass().getSimpleName(), e.getMessage());
		}
	}

	/**
	 * Verschiebt die Datei in das Zielverzeichis. Exceptions werden geworfen, damit man mitzählen kann.
	 *
	 * @param pathUploadDir
	 * @param pathTargetDir
	 * @param dateiname
	 * @throws IOException
	 */
	public void moveFile(final String pathUploadDir, final String pathTargetDir, final String dateiname) throws IOException {
		final Path pathTarget = Paths.get(pathTargetDir + File.separator + dateiname);
		Path pathSource = Paths.get(pathUploadDir + File.separator + dateiname);
		Files.move(pathSource, pathTarget.resolveSibling(pathTarget));
	}

}
