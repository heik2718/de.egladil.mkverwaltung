//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.egladil.mkv.auswertungen.domain.AufgabeErgebnisItem;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungItem;
import de.egladil.mkv.auswertungen.domain.Punktintervall;
import de.egladil.mkv.auswertungen.domain.RohpunktItem;
import de.egladil.mkv.auswertungen.renderer.DoubleExactRenderer;
import de.egladil.mkv.auswertungen.renderer.PunktintervallRenderer;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;

/**
 * VerteilungGeneratorImpl
 */
public class VerteilungGeneratorImpl implements IVerteilungGenerator {

	private final Map<Punktintervall, List<ILoesungszettel>> punktintervallKlassen = new HashMap<>();

	private final Map<Integer, List<ILoesungszettel>> punktklassen = new HashMap<>();

	private final Map<String, Integer> aufgabennummerIndexMap = new HashMap<>();

	private FindPunktintervallCommand findPunktintervallCommand;

	private final BerechneMedianCommand berecheneMedianCommand = new BerechneMedianCommand();

	private final BerechneProzentrangIntervallCommand berechneProzentrangIntervallCommand = new BerechneProzentrangIntervallCommand();

	private final BerechneProzentrangPunkteCommand berechneProzentrangPunkteCommand = new BerechneProzentrangPunkteCommand();

	private final BerechneAufgabenErgebnisCommand berechneAufgabenErgebnisCommand = new BerechneAufgabenErgebnisCommand();

	@Override
	public GesamtpunktverteilungDaten berechneGesamtpunktverteilungDaten(final List<ILoesungszettel> loesungszettel,
		final Klassenstufe klassenstufe, final String jahr) {
		if (loesungszettel == null) {
			throw new NullPointerException("Parameter loesungszettel");
		}
		if (klassenstufe == null) {
			throw new NullPointerException("Parameter klasse");
		}
		if (jahr == null) {
			throw new NullPointerException("Parameter jahr");
		}
		checkKlasseJahr(klassenstufe, jahr);
		clearMaps();
		final GesamtpunktverteilungDaten daten = new GesamtpunktverteilungDaten();
		daten.setKlassenstufe(klassenstufe);
		initPunktintervallKlassen(PunktintervallFactory.createFactory(klassenstufe, jahr));
		initAufgabeErgebnisItems(klassenstufe, jahr);
		sortiereLoesungszetteNachIntervallklassenUndPunktklassen(loesungszettel);
		final double median = berecheneMedianCommand.berechne(loesungszettel);
		daten.setMedian(new DoubleExactRenderer(median).render());
		daten.setAnzahlTeilnehmer(loesungszettel.size());
		addGesamtpunktverteilung(daten);
		addAufgabenverteilung(daten, loesungszettel);
		addEinzelpunktverteilung(daten);
		return daten;
	}

	@Override
	public String berechneMedian(final List<ILoesungszettel> loesungszettel) {
		if (loesungszettel.isEmpty()) {
			throw new IllegalArgumentException("loesungszettel empty");
		}
		final double median = berecheneMedianCommand.berechne(loesungszettel);
		return new DoubleExactRenderer(median).render();
	}

	private void clearMaps() {
		aufgabennummerIndexMap.clear();
		punktintervallKlassen.clear();
		punktklassen.clear();
	}

	/**
	 * Prüft die Kombination aus Klassenstufe und Jahr.
	 *
	 * @param klassenstufe
	 * @param jahr
	 * @throws IllegalArgumentException
	 */
	private void checkKlasseJahr(final Klassenstufe klassenstufe, final String jahr) {
		klassenstufe.getAufgabennummern(jahr);
	}

	private void addAufgabenverteilung(final GesamtpunktverteilungDaten daten, final List<ILoesungszettel> loesungszettel) {
		for (final String nummer : aufgabennummerIndexMap.keySet()) {
			final AufgabeErgebnisItem item = berechneAufgabenErgebnisCommand.create(nummer, aufgabennummerIndexMap.get(nummer),
				loesungszettel);
			daten.addAufgabeErgebnis(item);
		}
	}

	private void addGesamtpunktverteilung(final GesamtpunktverteilungDaten daten) {
		for (final Punktintervall intervall : punktintervallKlassen.keySet()) {
			final GesamtpunktverteilungItem item = new GesamtpunktverteilungItem();
			final List<ILoesungszettel> loesungszettel = punktintervallKlassen.get(intervall);
			item.setAnzahl(loesungszettel.size());
			item.setPunktintervall(intervall);
			item.setPunktintervallText(new PunktintervallRenderer(intervall).render());
			daten.addItem(item);
		}
		berechneProzentrangIntervallCommand.berechne(daten.getGesamtpunktverteilungItemsSorted(), daten.getAnzahlTeilnehmer());
	}

	private void addEinzelpunktverteilung(final GesamtpunktverteilungDaten daten) {
		for (final Integer punkte : punktklassen.keySet()) {
			final RohpunktItem rohpunktitem = new RohpunktItem();
			final List<ILoesungszettel> loesungszettel = punktklassen.get(punkte);
			rohpunktitem.setAnzahl(loesungszettel.size());
			rohpunktitem.setPunkte(punkte.intValue());
			daten.addRohpunktItem(rohpunktitem);
		}
		berechneProzentrangPunkteCommand.berechne(daten.getRohpunktItemsSorted(), daten.getAnzahlTeilnehmer());
	}

	private void initPunktintervallKlassen(final IPunktintervallFactory punktintervallFactory) {
		final List<Punktintervall> punktintervalle = punktintervallFactory.getPunktintervalleDescending();
		for (final Punktintervall intervall : punktintervalle) {
			punktintervallKlassen.put(intervall, new ArrayList<>());
		}
		findPunktintervallCommand = new FindPunktintervallCommand(punktintervalle);
	}

	private void initAufgabeErgebnisItems(final Klassenstufe klassenstufe, final String jahr) {
		final List<String> aufgabennummern = klassenstufe.getAufgabennummern(jahr);
		for (int i = 0; i < klassenstufe.getAnzahlAufgaben(jahr); i++) {
			final String nummer = aufgabennummern.get(i);
			aufgabennummerIndexMap.put(nummer, Integer.valueOf(i));
		}
	}

	/**
	 * Sortiert die Lösungszettel in PunktintervallKlassen und Punktklassen.
	 *
	 * @param alleLoesungszettel
	 */
	private void sortiereLoesungszetteNachIntervallklassenUndPunktklassen(final List<ILoesungszettel> alleLoesungszettel) {
		for (final ILoesungszettel loesungszettel : alleLoesungszettel) {
			final int punkte = loesungszettel.getPunkte();
			final Optional<Punktintervall> opt = findPunktintervallCommand.find(punkte);
			if (!opt.isPresent()) {
				throw new MKAuswertungenException("Lösungszettel gehört zu keiner Punktklasse: " + loesungszettel.toString());
			}
			final List<ILoesungszettel> klasse = punktintervallKlassen.get(opt.get());
			klasse.add(loesungszettel);

			final List<ILoesungszettel> punktklasse = getOrCreatePunktklasse(punkte);
			punktklasse.add(loesungszettel);
			punktklassen.put(loesungszettel.getPunkte(), punktklasse);
		}
	}

	private List<ILoesungszettel> getOrCreatePunktklasse(final int punkte) {
		final List<ILoesungszettel> punktklasse = punktklassen.get(punkte);
		return punktklasse == null ? new ArrayList<>() : punktklasse;
	}
}
