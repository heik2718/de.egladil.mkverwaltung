//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import de.egladil.mkv.auswertungen.urkunden.EinzelurkundeGenerator;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.render.FooterRenderer;
import de.egladil.mkv.auswertungen.urkunden.render.HauptabschnittRenderer;
import de.egladil.mkv.auswertungen.urkunden.render.HeaderRenderer;
import de.egladil.mkv.auswertungen.urkunden.render.UrkundeAbschnittRenderer;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * EinzelurkundeGeneratorImpl
 */
public class EinzelurkundeGeneratorImpl implements EinzelurkundeGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(EinzelurkundeGeneratorImpl.class);

	private final UrkundeAbschnittRenderer headerRenderer;

	private final UrkundeAbschnittRenderer hauptabschnittRenderer;

	private final UrkundeAbschnittRenderer footerRenderer;

	private final UrkundeAbschnittRenderer punkteRenderer;

	/**
	 * EinzelurkundeGeneratorImpl
	 */
	public EinzelurkundeGeneratorImpl(final UrkundeAbschnittRenderer punkteRenderer) {
		this.headerRenderer = new HeaderRenderer();
		this.hauptabschnittRenderer = new HauptabschnittRenderer();
		this.footerRenderer = new FooterRenderer();
		this.punkteRenderer = punkteRenderer;
	}

	@Override
	public byte[] generate(final UrkundeInhalt urkundeInhalt) throws MKVException {
		LOG.debug("generieren AuswertungDownload für Teilnehmer {}", urkundeInhalt.getTeilnehmerKuerzel());
		PdfReader reader = null;
		PdfStamper stamper = null;
		try (InputStream is = urkundeInhalt.getBackgroundImage(); final ByteArrayOutputStream os = new ByteArrayOutputStream()) {

			reader = new PdfReader(is);
			stamper = new PdfStamper(reader, os);

			final PdfContentByte content = stamper.getOverContent(1);

			int verschiebungVonTop = headerRenderer.printAbschnittAndShiftVerticalPosition(content, urkundeInhalt, 0);
			verschiebungVonTop = hauptabschnittRenderer.printAbschnittAndShiftVerticalPosition(content, urkundeInhalt,
				verschiebungVonTop);
			verschiebungVonTop = punkteRenderer.printAbschnittAndShiftVerticalPosition(content, urkundeInhalt, verschiebungVonTop);
			verschiebungVonTop = footerRenderer.printAbschnittAndShiftVerticalPosition(content, urkundeInhalt, verschiebungVonTop);

			stamper.close();
			reader.close();

			final byte[] data = os.toByteArray();
			return data;

		} catch (final IOException | DocumentException e) {
			LOG.error("{} beim generieren von Teilnehmerurkunde {}: {}", e.getClass().getSimpleName(),
				urkundeInhalt.getTeilnehmerKuerzel(), e.getMessage(), e);
			throw new MKVException("Exception beim verarbeiten des backgroundImages " + e.getMessage(), e);
		} catch (final IllegalArgumentException e) {
			final String msg = "IllegalArgumentException beim generieren von " + urkundeInhalt.getTeilnehmerKuerzel() + ": "
				+ e.getMessage();
			LOG.error(msg);
			throw new MKVException(msg, e);
		} finally {
			closeStamperQuietly(stamper);
			reader.close();
		}
	}

	private void closeStamperQuietly(final PdfStamper stamper) {
		if (stamper != null) {
			try {
				stamper.close();
			} catch (DocumentException | IOException e) {
				LOG.warn("stamper konnte nicht freigegeben werden: {}", e.getMessage(), e);
			}
		}
	}
}
