//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

/**
 * MKAuswertungCell ist eine Zelle in einer Minkänguru-Auswertungstabelle.
 */
public class MKAuswertungCell {

	private final MKCellAddress cellAddress;

	private final String contents;

	/**
	 * @param cellAddress MKCellAddress darf nicht null sein.
	 * @param contents String darf auch null sein.
	 * @throws NullPointerException wenn cellAddress null ist.
	 */
	public MKAuswertungCell(MKCellAddress cellAddress, String contents) {
		if (cellAddress == null) {
			throw new NullPointerException("Parameter cellAddress");
		}
		this.cellAddress = cellAddress;
		this.contents = contents == null ? "" : contents.trim();
	}

	public MKCellAddress getCellAddress() {
		return cellAddress;
	}

	/**
	 *
	 * contents kann auch null sein!
	 *
	 * @return
	 */
	public Optional<String> getContents() {
		return Optional.ofNullable(contents);
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cellAddress == null) ? 0 : cellAddress.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MKAuswertungCell other = (MKAuswertungCell) obj;
		if (cellAddress == null) {
			if (other.cellAddress != null)
				return false;
		} else if (!cellAddress.equals(other.cellAddress))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return contents == null ? "" : contents;
	}

	public boolean isEmpty() {
		if (StringUtils.isBlank(contents)) {
			return true;
		}
		return "0".equals(contents);
	}
}
