//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.persistence;

import java.util.Optional;

import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * ProcessUploadResult
 */
public class ProcessUploadResult {

	private final Optional<UploadInfo> optUploadInfo;

	private UploadStatus statusNeu;

	private String message;

	/**
	 * Erzeugt eine Instanz von ProcessUploadResult
	 */
	public ProcessUploadResult(Optional<UploadInfo> optUploadInfo) {
		this.optUploadInfo = optUploadInfo;
	}

	public UploadStatus getStatusNeu() {
		return statusNeu;
	}

	public void setStatusNeu(UploadStatus statusNeu) {
		this.statusNeu = statusNeu;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Optional<UploadInfo> getOptUploadInfo() {
		return optUploadInfo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("ProcessUploadResult [statusNeu=");
		sb.append(statusNeu);
		sb.append(", message=");
		sb.append(message);
		if (optUploadInfo.isPresent()) {
			sb.append(optUploadInfo.get().toString());
		}
		sb.append("]");
		return sb.toString();
	}

}
