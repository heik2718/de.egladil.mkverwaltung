//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

/**
 * AntwortbuchstabenKorrekturstrategie
 */
public class AntwortbuchstabenKorrekturstrategie implements INutzereingabeKorrekturstrategie {

	/**
	 * @see de.egladil.mkv.auswertungen.eingaben.impl.INutzereingabeKorrekturstrategie#korrigiere(java.lang.String)
	 */
	@Override
	public String korrigiere(String nutzereingabe) {
		throw new IllegalStateException("Methode noch nicht implementiert");
	}


}
