//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenschluessel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * DefaultKlassenschluesselComparator
 */
public class DefaultKlassenschluesselComparator implements Comparator<Klassenschluessel> {

	@Override
	public int compare(final Klassenschluessel arg0, final Klassenschluessel arg1) {
		final Klassenstufe erste = arg0.getKlassenstufe();
		final Klassenstufe zweite = arg1.getKlassenstufe();
		if (erste != zweite) {
			return erste.getNummer() - zweite.getNummer();
		}
		final String erster = arg0.getKlassenname();
		final String zweiter = arg1.getKlassenname();

		return erster.compareToIgnoreCase(zweiter);
	}
}
