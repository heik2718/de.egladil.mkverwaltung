//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

/**
 * Dmainobjekte für die Verarbeitung von Lösungszetteln aus Dateien oder Onlinedaten.
 */
package de.egladil.mkv.auswertungen.domain;