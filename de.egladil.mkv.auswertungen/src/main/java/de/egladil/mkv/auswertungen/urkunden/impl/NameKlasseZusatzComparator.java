//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;

/**
 * NameKlasseZusatzComparator enthält die von den Punkten unabhängigen Sortierungen von UrkundeInhalt-Instanzen.
 */
public class NameKlasseZusatzComparator implements Comparator<UrkundeInhalt> {

	@Override
	public int compare(final UrkundeInhalt arg0, final UrkundeInhalt arg1) {
		final String teilnehmername0 = arg0.getTeilnehmername();
		final String teilnehmername1 = arg1.getTeilnehmername();
		if (!teilnehmername0.equals(teilnehmername1)) {
			return teilnehmername0.compareTo(teilnehmername1);
		}
		final String klassenname0 = arg0.getKlassenname();
		final String klassenname1 = arg1.getKlassenname();
		if (!klassenname0.equals(klassenname1)) {
			return klassenname0.compareTo(klassenname1);
		}
		final String zusatz0 = arg0.getZusatz();
		final String zusatz1 = arg1.getZusatz();
		if (zusatz0 == null && zusatz1 == null) {
			return 0;
		}
		if (zusatz0 == null) {
			return -1;
		}
		if (zusatz1 == null) {
			return 1;
		}
		return zusatz0.compareTo(zusatz1);
	}

}
