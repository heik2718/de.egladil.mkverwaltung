//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import org.apache.commons.lang3.StringUtils;

import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * UrkundenmotivMitTextBuilder
 */
public class UrkundenmotivMitTextBuilder {

	private final String nestedWettbewerbsjahr;

	private final UrkundenmotivProvider nestedMotivProvider;

	private final String nestedDatum;

	private String nestedSchulname;

	/**
	 * UrkundenmotivMitTextBuilder
	 */
	public UrkundenmotivMitTextBuilder(final String nestedWettbewerbsjahr, final UrkundenmotivProvider nestedMotivProvider,
		final String nestedDatum) {
		if (StringUtils.isAnyBlank(nestedWettbewerbsjahr, nestedDatum)) {
			throw new IllegalArgumentException("Datum und Wettbewerbsjahr sind erforderlich");
		}
		if (nestedMotivProvider == null) {
			throw new IllegalArgumentException("der UrkundenmotivProvider ist erforderlich");
		}
		this.nestedMotivProvider = nestedMotivProvider;
		this.nestedWettbewerbsjahr = nestedWettbewerbsjahr;
		this.nestedDatum = nestedDatum;
	}

	public UrkundenmotivMitTextBuilder schulname(final String schulname) {
		this.nestedSchulname = schulname;
		return this;
	}

	public UrkundenmotivMitText build() {
		return new UrkundenmotivMitText(nestedWettbewerbsjahr, nestedSchulname, nestedMotivProvider, nestedDatum);
	}
}
