//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;
import de.egladil.tools.parser.OpenOfficeTableElement;

/**
 * OpenOfficeAssistent
 */
public class OpenOfficeAssistent {

	/**
	 * Sucht in der gegebenen Zeile nach der Zelle mit content und gibt deren Index (0-basiert) zurück.
	 *
	 * @param tableCells List darf nicht null sein.
	 * @param content String der Verleichstext. Darf nicht blank sein.
	 * @return int -1, wenn es den Inhalt nicht gibt.
	 */
	public static int findFirstIndexWithContent(final OpenOfficeTableElement tableCells, final String content)
		throws IllegalArgumentException {

		if (tableCells == null) {
			throw new NullPointerException("row null");
		}
		if (StringUtils.isBlank(content)) {
			throw new IllegalArgumentException("content blank");
		}
		for (int i = 0; i < tableCells.size(); i++) {
			final OpenOfficeTableElement node = tableCells.get(i);
			final String val = node.getContent();
			if (content.equalsIgnoreCase(val)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Puzzled den Inhalt der Zelle raus.
	 *
	 * @param tdElement
	 * @return String nicht null;
	 */
	public static String getCellValue(final Element tdElement) {
		final Elements pElements = tdElement.getElementsByTag("p");
		final Iterator<Element> iter = pElements.iterator();
		while (iter.hasNext()) {
			final Element el = iter.next();
			return el.ownText();
		}
		// if (pElements.size() > 0) {
		// List<Node> childNodes = pElements.childNodes();
		// if (childNodes.size() == 1) {
		// return childNodes.get(0).toString().trim();
		// }
		// }
		return "";
	}

	/**
	 * Versucht, anhand der Headline zu ermitteln, um welche Klassenstufe es sich handelt.
	 *
	 * @param tableRow
	 * @return
	 * @throws MKAuswertungenException
	 */
	public static Klassenstufe getKlasse(final OpenOfficeTableElement tableRow) throws MKAuswertungenException {
		// Nach Klassenstufe 2 muss zuerst gesucht werden, da das Klassenstufe-1-Marker auch in
		// Klassenstufe-2-Auswertungen vorkommt.
		int index = OpenOfficeAssistent.findFirstIndexWithContent(tableRow, IAuswertungStreamReader.KLASSE_2_MARKER);
		if (index >= 0) {
			return Klassenstufe.ZWEI;
		}
		index = OpenOfficeAssistent.findFirstIndexWithContent(tableRow, IAuswertungStreamReader.KLASSE_1_MARKER);
		if (index >= 0) {
			return Klassenstufe.EINS;
		}
		index = OpenOfficeAssistent.findFirstIndexWithContent(tableRow, IAuswertungStreamReader.INKLUSION_MARKER);
		if (index >= 0) {
			return Klassenstufe.IKID;
		}
		throw new MKAuswertungenException(
			"Das sheet scheint weder zu einer Klassenstufe-1- noch zu einer Klassenstufe-2-Auswertung zu gehören");
	}

	/**
	 * Ermittelt die Anzahl der Namenspalten in der Tabelle.
	 *
	 * @param tableCells
	 * @return
	 */
	public static int getAnzahlNamenspalten(final OpenOfficeTableElement tableCells) {
		final int index = OpenOfficeAssistent.findFirstIndexWithContent(tableCells, IAuswertungStreamReader.STARTADDRESS_MARKER);
		return index;
	}

	/**
	 * TODO Teesten!!!
	 *
	 * @param anzahlNamenspalten
	 * @param i
	 * @return
	 */
	public static boolean isWertungseintrag(final int anzahlNamenspalten, final int i) {
		return anzahlNamenspalten == 1 && i % 2 == 1 || anzahlNamenspalten != 1 && i % 2 == 0;
	}
}
