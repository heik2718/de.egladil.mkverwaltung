//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.tools.parser.OpenOfficeTableElement;

/**
 * SearchingHaeadlineState
 */
public class SearchingHaeadlineState implements IOpenOfficeParsingState {

	private boolean datenStarted = false;

	private int anzahlNamenspalten = 0;

	private Klassenstufe klassenstufe;

	private boolean finished = false;

	private final List<MKAuswertungRow> zeilen = new ArrayList<>();

	@Override
	public void handle(final OpenOfficeTableElement tableRow) {
		if (finished) {
			return;
		}
		if (tableRow.isHeadline()) {
			datenStarted = true;
			anzahlNamenspalten = getAnzahlNamenspalten(tableRow);
			klassenstufe = OpenOfficeAssistent.getKlasse(tableRow);
			finished = true;
		}
	}

	public void addMKAuswertungRow(final MKAuswertungRow row) {
		if (!zeilen.contains(row)) {
			zeilen.add(row);
		}
	}

	public boolean isDatenStarted() {
		return datenStarted;
	}

	public int getAnzahlNamenspalten() {
		return anzahlNamenspalten;
	}

	public Klassenstufe getKlasse() {
		return klassenstufe;
	}

	private int getAnzahlNamenspalten(final OpenOfficeTableElement tableRow) {
		final int index = OpenOfficeAssistent.findFirstIndexWithContent(tableRow, IAuswertungStreamReader.STARTADDRESS_MARKER);
		return index;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	/**
	 * Liefert die Membervariable zeilen
	 *
	 * @return die Membervariable zeilen
	 */
	public List<MKAuswertungRow> getZeilen() {
		return Collections.unmodifiableList(zeilen);
	}

	public boolean isEmpty() {
		return zeilen.isEmpty();
	}
}
