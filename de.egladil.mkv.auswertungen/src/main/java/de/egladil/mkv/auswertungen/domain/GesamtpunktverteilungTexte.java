//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

/**
 * GesamtpunktverteilungTexte enthält die Textuellen Bestandteile der Gesamtpunktverteilung.
 */
public class GesamtpunktverteilungTexte {

	private String titel;

	private String basis;

	private String bewertung;

	private String sectionEinzelergebnisse = "Lösungen je Aufgabe";

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getBewertung() {
		return bewertung;
	}

	public void setBewertung(String bewertung) {
		this.bewertung = bewertung;
	}

	public String getSectionEinzelergebnisse() {
		return sectionEinzelergebnisse;
	}

	public void setSectionEinzelergebnisse(String sectionEinzelergebnisse) {
		this.sectionEinzelergebnisse = sectionEinzelergebnisse;
	}

}
