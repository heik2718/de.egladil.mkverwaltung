//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.tools.mime.UploadDetector;
import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * UploadUtils
 */
public class UploadUtils {

	private static final Logger LOG = LoggerFactory.getLogger(UploadUtils.class);

	/**
	 * Ermittelt den MIME-Type der bytes.
	 *
	 * @param bytes darf nicht null sein.
	 * @return String
	 * @throws IOException
	 * @throws NullPointerException wenn bytes null
	 * @throws ParserSecurityException
	 *
	 */
	public String getMimeType(final byte[] bytes) throws IOException, NullPointerException, ParserSecurityException {
		if (bytes == null) {
			throw new NullPointerException("bytes");
		}
		final String mimeType = new UploadDetector().detect(bytes);
		return mimeType;
	}

	/**
	 * Wandelt den InputStream in ein byte-Array um.
	 *
	 * @param in InputStream darf nicht null sein
	 * @return
	 * @throws IOException
	 * @throws NullPointerException wenn in null ist
	 */
	public byte[] readBytes(final InputStream in) throws IOException, NullPointerException {
		if (in == null) {
			throw new NullPointerException("in");
		}
		return IOUtils.toByteArray(in);
	}

	/**
	 * Wandelt den Inhalt eines Files in ein byte-Array um.
	 *
	 * @param in InputStream darf nicht null sein
	 * @return
	 * @throws IOException
	 * @throws NullPointerException wenn in null ist
	 */
	public byte[] readBytes(final File file) throws IOException, NullPointerException {
		if (file == null) {
			throw new NullPointerException("Parameter in");
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}

	/**
	 * Berechnet die Checksumme des byte-Arrays.
	 *
	 * @param bytes byte[] darf nicht null sein
	 * @return
	 * @throws NullPointerException wenn bytes null.
	 */
	public String getChecksum(final byte[] bytes) throws NullPointerException {
		if (bytes == null) {
			throw new NullPointerException("bytes");
		}
		final byte[] checksum = DigestUtils.sha256(bytes);
		final String result = Hex.encodeHexString(checksum);
		return result;
	}

	/**
	 * Legt tdas Verzeichnis an, wenn es noch nicht vorhanden ist.
	 *
	 * @param pathTargetDir String absoluter Pfad.
	 * @throws IOException
	 */
	public void createDirIfNotExists(final String pathTargetDir) throws IOException {
		final Path pathTarget = Paths.get(pathTargetDir);

		if (!Files.isDirectory(pathTarget, new LinkOption[] {})) {
			Files.createDirectory(pathTarget);
			LOG.info("Verzeichnis {} angelegt", pathTarget);
		}

	}
}
