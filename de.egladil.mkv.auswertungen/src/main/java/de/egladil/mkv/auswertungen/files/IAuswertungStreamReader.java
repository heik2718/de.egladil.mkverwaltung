//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files;

import java.io.IOException;
import java.io.InputStream;

import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.exceptions.UnprocessableAuswertungException;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;

/**
 * IAuswertungStreamReader
 */
public interface IAuswertungStreamReader {

	String STARTADDRESS_MARKER = "A-1";

	String KLASSE_2_MARKER = "C-5";

	String KLASSE_1_MARKER = "C-4";

	String INKLUSION_MARKER = "C-2";

	/**
	 * Liest den InputStream ein und wandelt ihn in ein Workbook um.<br>
	 * <br>
	 * <strong>Achtung: </strong> Der InputStream wird NICHT geschlossen.
	 *
	 * @param in InputStream darf nicht null sein.
	 * @return MKAuswertungWorkbook
	 */
	MKAuswertungWorkbook readAuswertung(InputStream in, int maxLengthExtracted)
		throws IOException, MKAuswertungenException, UnprocessableAuswertungException;
}
