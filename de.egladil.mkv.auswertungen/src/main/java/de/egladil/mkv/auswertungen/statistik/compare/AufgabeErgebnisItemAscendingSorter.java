//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.domain.AufgabeErgebnisItem;

/**
 * AufgabeErgebnisItemAscendingSorter
 */
public class AufgabeErgebnisItemAscendingSorter implements Comparator<AufgabeErgebnisItem> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(AufgabeErgebnisItem arg0, AufgabeErgebnisItem arg1) {
		return arg0.getIndex() - arg1.getIndex();
	}
}
