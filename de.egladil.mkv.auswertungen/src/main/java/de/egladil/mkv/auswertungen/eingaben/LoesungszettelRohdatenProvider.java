//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben;

import de.egladil.mkv.persistence.berechnungen.AbstractBerechnePunkteCommand;
import de.egladil.mkv.persistence.berechnungen.FindKaengurusprungCommand;
import de.egladil.mkv.persistence.berechnungen.IBerechnePunkteCommand;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * LoesungszettelRohdatenProvider
 */
public class LoesungszettelRohdatenProvider {

	private final Klassenstufe klassenstufe;

	private final String berechneterWertungscode;

	private final String nutzereingabe;

	private final boolean typo;

	private final Auswertungsquelle auswertungsquelle;

	private final FindKaengurusprungCommand findKaengurusprungCommand = new FindKaengurusprungCommand();

	/**
	 * LoesungszettelRohdatenProvider
	 */
	public LoesungszettelRohdatenProvider(final Klassenstufe klassenstufe, final String berechneterWertungscode,
		final String nutzereingabe, final boolean typo, final Auswertungsquelle auswertungsquelle) {
		this.klassenstufe = klassenstufe;
		this.berechneterWertungscode = berechneterWertungscode;
		this.nutzereingabe = nutzereingabe;
		this.typo = typo;
		this.auswertungsquelle = auswertungsquelle;
	}

	public LoesungszettelRohdaten createLoesungszettelRohdaten() {
		final int kaengurusprung = findKaengurusprungCommand.findKaengurusprung(berechneterWertungscode);
		final IBerechnePunkteCommand berechnePunkteCommand = AbstractBerechnePunkteCommand.createCommand(klassenstufe);
		final int punkte = berechnePunkteCommand.berechne(berechneterWertungscode);

		final LoesungszettelRohdaten loesungszettel = new LoesungszettelRohdaten.Builder(auswertungsquelle, klassenstufe,
			kaengurusprung, punkte, berechneterWertungscode, nutzereingabe).typo(typo).build();

		return loesungszettel;
	}
}
