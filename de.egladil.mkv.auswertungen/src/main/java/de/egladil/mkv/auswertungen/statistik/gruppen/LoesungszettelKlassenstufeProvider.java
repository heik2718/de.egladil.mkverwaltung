//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.List;

import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * LoesungszettelKlassenstufeProvider
 */
public interface LoesungszettelKlassenstufeProvider {

	List<ILoesungszettel> getLoesungszettel(Klassenstufe klassenstufe);

	int anzahlKinder();

	String getSchulname();

}
