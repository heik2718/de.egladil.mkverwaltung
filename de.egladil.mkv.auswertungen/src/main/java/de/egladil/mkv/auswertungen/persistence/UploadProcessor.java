//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.persistence;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.config.GlobalConstants;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.IMailservice;
import de.egladil.mkv.auswertungen.AnwenderdatenAuswertenOperation;
import de.egladil.mkv.auswertungen.VerarbeitetesWorkbook;
import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.auswertungen.Wettbewerbsloesung;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * Der UploadProcessor verarbeitet die Daten eines einzelnen Uploads zu Rohdaten.
 */
public class UploadProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(UploadProcessor.class);

	private final ILoesungszettelDao loesungszettelDao;

	private final IWettbewerbsloesungDao wettbewerbsloesungDao;

	private final IUploadDao uploadDao;

	private final IUploadInfoDao uploadInfoDao;

	private final int maxExtractedBytes;

	private final IMailservice mailService;

	private final AuswertungstabelleFileWriter fileWriter;

	private boolean nurStatusNEU = true;

	/**
	 * Erzeugt eine Instanz von UploadProcessor
	 */
	public UploadProcessor(final ILoesungszettelDao loesungszettelDao, final IWettbewerbsloesungDao wettbewerbsloesungDao,
		final IUploadDao uploadDao, final IUploadInfoDao uploadInfoDao, final IMailservice mailService,
		final AuswertungstabelleFileWriter fileWriter) {
		this.loesungszettelDao = loesungszettelDao;
		this.wettbewerbsloesungDao = wettbewerbsloesungDao;
		this.uploadDao = uploadDao;
		this.uploadInfoDao = uploadInfoDao;
		this.mailService = mailService;
		this.fileWriter = fileWriter;
		this.maxExtractedBytes = KontextReader.getInstance().getKontext().getMaxExtractedBytes();
	}

	/**
	 * Verarbeitet den Upload. Exceptions werden nicht propagiert.
	 *
	 * @param uploadId Long die ID eines uploads.
	 * @param uploadDirPath
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public ProcessUploadResult processUploadAndExceptions(final Long uploadId, final String uploadDirPath) {

		final Optional<UploadInfo> optUploadInfo = uploadInfoDao.findById(UploadInfo.class, uploadId);
		if (!optUploadInfo.isPresent()) {
			LOG.warn("UploadInfo mit ID {} existiert nicht", uploadId);
			final ProcessUploadResult result = new ProcessUploadResult(optUploadInfo);
			result.setMessage("UploadInfo mit ID " + uploadId + " existiert nicht");
			return result;
		}
		final UploadInfo uploadInfo = optUploadInfo.get();
		return this.processUploadAndExceptions(uploadInfo, uploadDirPath);
	}

	/**
	 * Verarbeitet den Upload. Exceptions werden nicht propagiert.
	 *
	 * @param uploadId Long die ID eines uploads.
	 * @param uploadDirPath
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public ProcessUploadResult processUploadAndExceptions(final UploadInfo uploadInfo, final String uploadDirPath) {

		final ProcessUploadResult result = new ProcessUploadResult(Optional.of(uploadInfo));

		if (nurStatusNEU && uploadInfo.getUploadStatus() != UploadStatus.NEU) {
			LOG.warn("Upload {} ist nicht neu - wird ignoriert", uploadInfo);
			result.setMessage("UploadInfo mit ID " + uploadInfo.getId() + " ist nicht neu - wird inoriert");
			return result;
		}

		final Map<Klassenstufe, String> loesungscodes = getLoesungscodes(uploadInfo.getJahr());
		UploadStatus neuerStatus = null;

		try {
			final Optional<VerarbeitetesWorkbook> opt = new AnwenderdatenAuswertenOperation(loesungscodes, maxExtractedBytes)
				.verarbeiteDaten(uploadInfo, uploadDirPath);

			if (opt.isPresent()) {

				final VerarbeitetesWorkbook verarbeitetesWorkbook = opt.get();
				if (verarbeitetesWorkbook.isLeer()) {
					neuerStatus = UploadStatus.LEER;
				} else {
					handleDaten(uploadInfo, uploadDirPath, verarbeitetesWorkbook);
					neuerStatus = UploadStatus.FERTIG;
				}
			} else {
				neuerStatus = UploadStatus.FEHLER;
			}
			markUploadQuietly(uploadInfo, neuerStatus, uploadDirPath);
			result.setStatusNeu(neuerStatus);
			return result;
		} catch (final Exception e) {
			LOG.error("Fehler beim Verarbeiten des Uploads: {}, {}", e.getMessage(), uploadInfo);
			LOG.debug(e.getMessage(), e);
			neuerStatus = UploadStatus.FEHLER;
			markUploadQuietly(uploadInfo, neuerStatus, uploadDirPath);
			result.setMessage("e.getMessage()");
			result.setStatusNeu(neuerStatus);
			return result;
		} finally {
			if (this.mailService != null && neuerStatus != UploadStatus.FERTIG) {
				this.uploadfehlerMailVersenden(neuerStatus, uploadInfo);
			}
		}
	}

	private void handleDaten(final UploadInfo uploadInfo, final String uploadDirPath,
		final VerarbeitetesWorkbook verarbeitetesWorkbook) {

		final List<Loesungszettel> alleLoesungszettel = new ArrayList<>();
		final Klassenstufe klassenstufe = verarbeitetesWorkbook.getKlasse();
		int nummer = loesungszettelDao.getMaxNummer(uploadInfo.getTeilnahmeart(), uploadInfo.getJahr(),
			uploadInfo.getTeilnahmekuerzel(), klassenstufe);
		for (final LoesungszettelRohdaten rohdaten : verarbeitetesWorkbook.getRohdaten()) {
			final Loesungszettel loesungszettel = new Loesungszettel.Builder(uploadInfo.getJahr(), uploadInfo.getTeilnahmeart(),
				uploadInfo.getTeilnahmekuerzel(), ++nummer, rohdaten).withSprache(Sprache.de).build();
			alleLoesungszettel.add(loesungszettel);
		}
		loesungszettelDao.persist(alleLoesungszettel);
	}

	private void markUploadQuietly(final UploadInfo uploadInfo, final UploadStatus neuerStatus, final String uploadDirPath) {
		try {
			final Optional<Upload> optUpload = uploadDao.findById(Upload.class, uploadInfo.getId());
			if (!optUpload.isPresent()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "UploadInfo mit ID {} vorhanden, aber Upload nicht",
					uploadInfo.getId());
				moveFileQuietly(uploadInfo, uploadDirPath, ".impossible");
				return;
			}
			final Upload upload = optUpload.get();
			upload.setUploadStatus(neuerStatus);
			uploadDao.persist(upload);
			moveFileQuietly(uploadInfo, uploadDirPath, upload.getUploadStatus().getFileExtension());
		} catch (final Exception e) {
			LOG.error("Fehler beim Speichrn des Uploads: {}, {}", e.getMessage(), uploadInfo);
		}
	}

	private void moveFileQuietly(final UploadInfo uploadInfo, final String uploadDirPath, final String extension) {
		String dateiname = uploadInfo.getDateiname();
		fileWriter.renameFileQuietly(uploadDirPath, dateiname, extension);
	}

	private Map<Klassenstufe, String> getLoesungscodes(final String wettbewerbsjahr) {
		final Map<Klassenstufe, String> result = new HashMap<>();
		final List<Wettbewerbsloesung> wettbewerbsloesungen = wettbewerbsloesungDao.findByJahr(wettbewerbsjahr);
		for (final Wettbewerbsloesung l : wettbewerbsloesungen) {
			result.put(l.getKlasse(), l.getLoesungscode());
		}
		return result;
	}

	/**
	 * Versendet eine Mail an mich selbst, wenn ein Upload fehlerhaft war.
	 */
	void uploadfehlerMailVersenden(final UploadStatus uploadStatus, final UploadInfo uploadInfo) {

		if (this.mailService == null) {
			LOG.debug("Haben keinen Mailserver");
			return;
		}

		final String environment = KontextReader.getInstance().getKontext().getEnvironment();

		try {
			final String empfaenger = "minikaenguru@egladil.de";
			final String betreff = "Minikänguru (" + environment + "): Upload leer oder fehlerhaft";

			final IEmailDaten mail = new IEmailDaten() {
				@Override
				public String getText() {
					final String text = "UploadStatus '" + uploadStatus + "' beim Hochladen: "
						+ (uploadInfo != null ? uploadInfo.toString() : " - ");
					return text;
				}

				@Override
				public Collection<String> getHiddenEmpfaenger() {
					return null;
				}

				@Override
				public String getEmpfaenger() {
					return empfaenger;
				}

				@Override
				public String getBetreff() {
					return betreff;
				}

				@Override
				public List<String> alleEmpfaengerFuersLog() {
					return Arrays.asList(new String[] { empfaenger });
				}
			};

			mailService.sendMail(mail);
		} catch (final Exception e) {
			LOG.warn("Die Uploadfehler-Mail an mich selbst wurde nicht versendet: {}", e.getMessage());
		}
	}

	void setNurStatusNEU(final boolean nurStatusNEU) {
		this.nurStatusNEU = nurStatusNEU;
	}
}
