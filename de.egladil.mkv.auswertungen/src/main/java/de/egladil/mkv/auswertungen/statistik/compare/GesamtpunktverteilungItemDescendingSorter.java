//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungItem;

/**
 * GesamtpunktverteilungItemDescendingSorter sortiert die Gesamtpunktverteilung anhand der Punktintervalle absteigend.
 */
public class GesamtpunktverteilungItemDescendingSorter implements Comparator<GesamtpunktverteilungItem> {

	private final PunktintervallDescendingSorter wrappedSorter = new PunktintervallDescendingSorter();

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(GesamtpunktverteilungItem o1, GesamtpunktverteilungItem o2) {
		return wrappedSorter.compare(o1.getPunktintervall(), o2.getPunktintervall());
	}

}
