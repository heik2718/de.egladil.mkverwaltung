//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * Klassenschluessel
 */
public class Klassenschluessel {

	private final Klassenstufe klassenstufe;

	private final String klassenname;

	/**
	 * Klassenschluessel
	 */
	public Klassenschluessel(final Klassenstufe klassenstufe, final String klassenname) {
		this.klassenstufe = klassenstufe;
		this.klassenname = klassenname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((klassenname == null) ? 0 : klassenname.hashCode());
		result = prime * result + ((klassenstufe == null) ? 0 : klassenstufe.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Klassenschluessel other = (Klassenschluessel) obj;
		if (klassenname == null) {
			if (other.klassenname != null) {
				return false;
			}
		} else if (!klassenname.equals(other.klassenname)) {
			return false;
		}
		if (klassenstufe != other.klassenstufe) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Klassenschluessel [klassenstufe=");
		builder.append(klassenstufe);
		builder.append(", klassenname=");
		builder.append(klassenname);
		builder.append("]");
		return builder.toString();
	}

	public final Klassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	public final String getKlassenname() {
		return klassenname;
	}

}
