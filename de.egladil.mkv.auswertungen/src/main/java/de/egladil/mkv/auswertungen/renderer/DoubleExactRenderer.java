//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.renderer;

/**
 * DoubleExactRenderer Verwandelt ein double in einen String mit der vollständigen Nachkommastellenzahl.
 */
public class DoubleExactRenderer {

	private final double zahl;

	/**
	 * Erzeugt eine Instanz von DoubleExactRenderer
	 */
	public DoubleExactRenderer(double zahl) {
		this.zahl = zahl;
	}

	public String render() {
		String str = "" + zahl;
		return str.replace(".", ",");
	}
}
