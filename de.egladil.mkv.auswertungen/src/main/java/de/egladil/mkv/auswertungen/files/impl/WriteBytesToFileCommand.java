//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * WriteBytesToFileCommand
 */
public class WriteBytesToFileCommand implements IWriteToFileCommand {

	private final byte[] data;

	/**
	 * Erzeugt eine Instanz von WriteBytesToFileCommand
	 */
	public WriteBytesToFileCommand(final byte[] data) {
		this.data = data;
	}

	@Override
	public void writeToFile(final String absPath) throws IOException {
		if (absPath == null) {
			throw new NullPointerException("Parameter absPath");
		}
		if (data == null) {
			throw new NullPointerException("Parameter bytes");
		}
		final Path path = Paths.get(absPath);
		Files.write(path, data);
	}
}
