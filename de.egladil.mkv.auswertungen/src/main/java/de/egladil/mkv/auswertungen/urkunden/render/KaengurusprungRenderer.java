//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.io.IOException;
import java.text.MessageFormat;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.FontCalulator;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * KaengurusprungRenderer rendert den Kängurusprung-Text.
 */
public class KaengurusprungRenderer implements UrkundeAbschnittRenderer {

	private static final String SATZENDE_DE = "den weitesten Kängurusprung gemacht";

	private static final String MF_PATTERN_SPRUNGLAENGE_DE = "mit {0} richtigen Antworten in Folge";

	private static final String SATZENDE_EN = "made the farthest kangaroo jump";

	private static final String MF_PATTERN_SPRUNGLAENGE_EN = "with {0} correct answers in a row";

	private final UrkundeLinePrinter linePrinter;

	private final FontCalulator fontCalculator;

	/**
	 * KaengurusprungRenderer
	 */
	public KaengurusprungRenderer() {
		linePrinter = new UrkundeLinePrinter();
		fontCalculator = new FontCalulator();
	}

	@Override
	public int printAbschnittAndShiftVerticalPosition(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		int deltaY = differenceFromTopPositionPoints;
		final int fontSize = UrkundeConstants.SIZE_TEXT_NORMAL;
		String text = null;
		if (Sprache.en == urkundeInhalt.getSprache()) {
//			text = MessageFormat.format(MF_PATTERN_SPRUNGLAENGE_EN, new Object[] { urkundeInhalt.getKaengurusprung() });
			text = SATZENDE_EN;
		} else {
			text = MessageFormat.format(MF_PATTERN_SPRUNGLAENGE_DE, new Object[] { urkundeInhalt.getKaengurusprung() });
		}

		deltaY = fontCalculator.berechneDeltaY(text, deltaY, fontSize);
		linePrinter.printTextCenter(content, text, UrkundeConstants.getFontBlack(fontSize), deltaY);

		deltaY += UrkundeConstants.POINTS_BETWEEN_ROWS;

		if (Sprache.en == urkundeInhalt.getSprache()) {
			text = MessageFormat.format(MF_PATTERN_SPRUNGLAENGE_EN, new Object[] { urkundeInhalt.getKaengurusprung() });
//			linePrinter.printTextCenter(content, SATZENDE_EN, UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL),
//				deltaY);
			linePrinter.printTextCenter(content, text, UrkundeConstants.getFontBlack(fontSize), deltaY);
		} else {
			linePrinter.printTextCenter(content, SATZENDE_DE, UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL),
				deltaY);
		}

		return deltaY;
	}

}
