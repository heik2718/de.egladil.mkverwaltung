//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import java.util.Map;
import java.util.Optional;

import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.auswertungen.eingaben.ILoesungszettelBerechnungsstrategie;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * UploadBerechnungsstrategieFactory ist der Auswerter für hochgeladene Tabellendokumente. Es wird die typo-tolerante
 * Bewertungsstrategie verwendet.
 */
public class UploadBerechnungsstrategieFactory {

	private final Map<Klassenstufe, String> loesungscodes;

	private WorkbookKategorisierer kategorisierer = new WorkbookKategorisierer();

	/**
	 * Erzeugt eine Instanz von UploadBerechnungsstrategieFactory
	 */
	public UploadBerechnungsstrategieFactory(Map<Klassenstufe, String> loesungscodes) {
		this.loesungscodes = loesungscodes;
	}

	/**
	 * Für Tests kann hier ein Kateorisierer-Mock mitgegeben werden.
	 */
	UploadBerechnungsstrategieFactory(Map<Klassenstufe, String> loesungsbuchstaben, WorkbookKategorisierer kategorisierer) {
		this.loesungscodes = loesungsbuchstaben;
		this.kategorisierer = kategorisierer;
	}

	/**
	 * Erzeugt die zum gegebenen workbook passende Berechnungsstrategie.
	 *
	 * @param workbook MKAuswertungWorkbook darf nicht null sein.
	 * @return Optional ILoesungszettelBerechnungsstrategie. Falls die Kategorie nicht entscheidbar ist, kommt ein
	 * leeres zurück.
	 * @throws NullPointerException falls workbook null
	 */
	public Optional<ILoesungszettelBerechnungsstrategie> erzeugeStrategie(final MKAuswertungWorkbook workbook) {
		if (workbook == null){
			throw new NullPointerException("Parameter workbook");
		}
		WorkbookKategorie kategorie = kategorisierer.errateKategorie(workbook);
		switch (kategorie) {
		case ANTWORTBUCHSTABEN:
			return Optional.of(new AuswertungAusAntwortbuchstabeBerechnungsstrategie(loesungscodes));
		case WERTUNGSBUCHSTABEN:
			return Optional.of(new AuswertungAusWertungBerechnungsstrategie());
		default:
			break;
		}
		return Optional.empty();
	}
}
