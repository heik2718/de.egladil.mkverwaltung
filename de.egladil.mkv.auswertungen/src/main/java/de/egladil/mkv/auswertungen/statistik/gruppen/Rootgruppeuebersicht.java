//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.egladil.mkv.auswertungen.statistik.compare.DefaultKlassenschluesselComparator;
import de.egladil.mkv.auswertungen.statistik.compare.TeilnehmerUndPunkteNameComparator;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * Rootgruppeuebersicht fasst alle TeilnehmerUndPunkte eine Auswertungsgruppe zusammen.
 */
public class Rootgruppeuebersicht implements LoesungszettelKlassenstufeProvider {

	private final String schulname;

	private final DefaultKlassenschluesselComparator klassenschluesselComparator = new DefaultKlassenschluesselComparator();

	private final TeilnehmerUndPunkteNameComparator teilnehmerComparator = new TeilnehmerUndPunkteNameComparator();

	private Map<Klassenschluessel, List<TeilnehmerUndPunkteLoesungszettelAdapter>> klassenMap = new HashMap<>();

	/**
	 * Rootgruppeuebersicht
	 */
	public Rootgruppeuebersicht(final String schulname) {
		this.schulname = schulname;
	}

	public void putSchuelerin(final Klassenschluessel klassenschluessel,
		final TeilnehmerUndPunkteLoesungszettelAdapter schuelerin) {
		if (klassenschluessel == null) {
			throw new IllegalArgumentException("klassenschluessel null");
		}
		if (schuelerin == null) {
			throw new IllegalArgumentException("schuelerin null");
		}
		List<TeilnehmerUndPunkteLoesungszettelAdapter> klasse = klassenMap.get(klassenschluessel);
		if (klasse == null) {
			klasse = new ArrayList<>();
		}
		klasse.add(schuelerin);
		klassenMap.put(klassenschluessel, klasse);
	}

	/**
	 * Gibt die Rohdaten einer durch den Klassenschluessel definierten Klasse alphabetisch nach Name sortiert zurück.
	 *
	 * @param klassenschluessel
	 * @return List
	 */
	public List<TeilnehmerUndPunkteLoesungszettelAdapter> getKlassenliste(final Klassenschluessel klassenschluessel) {
		if (klassenschluessel == null) {
			throw new IllegalArgumentException("klassenschluessel null");
		}
		final List<TeilnehmerUndPunkteLoesungszettelAdapter> result = klassenMap.get(klassenschluessel);
		Collections.sort(result, teilnehmerComparator);
		return result;
	}

	/**
	 *
	 * @param klassenschluessel
	 * @return Klassenuebersicht
	 */
	public Klassenuebersicht getKlassenubersicht(final Klassenschluessel klassenschluessel) {
		if (klassenschluessel == null) {
			throw new IllegalArgumentException("klassenschluessel null");
		}
		return new Klassenuebersicht(klassenschluessel, getKlassenliste(klassenschluessel));
	}

	/**
	 *
	 * @return List alle keys der Map mit dem DefaultKlassenschluesselComparator sortiert.
	 */
	public List<Klassenschluessel> getKeysSorted() {
		final List<Klassenschluessel> result = new ArrayList<>();
		result.addAll(klassenMap.keySet());
		Collections.sort(result, klassenschluesselComparator);
		return result;
	}

	@Override
	public int anzahlKinder() {
		int result = 0;
		for (final List<TeilnehmerUndPunkteLoesungszettelAdapter> tups : klassenMap.values()) {
			result += tups.size();
		}
		return result;
	}

	@Override
	public List<ILoesungszettel> getLoesungszettel(final Klassenstufe klassenstufe) {

		final List<ILoesungszettel> alleZettel = new ArrayList<>();

		for (final Klassenschluessel key : getKeysSorted()) {
			if (key.getKlassenstufe().equals(klassenstufe)) {
				final List<TeilnehmerUndPunkteLoesungszettelAdapter> klassenliste = getKlassenliste(key);
				if (klassenliste != null) {
					alleZettel.addAll(klassenliste);
				}
			}
		}

		return alleZettel;
	}

	@Override
	public String getSchulname() {
		return schulname;
	}
}
