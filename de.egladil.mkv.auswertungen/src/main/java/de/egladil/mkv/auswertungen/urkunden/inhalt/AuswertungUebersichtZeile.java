//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

/**
 * AuswertungUebersichtZeile separiert den Inhalt einer Zeile der Auswertungübersicht vom Einfügen in ein PdfDocument.
 */
public class AuswertungUebersichtZeile {

	private final String text;

	private final boolean boldFont;

	/**
	 * AuswertungUebersichtZeile
	 */
	public AuswertungUebersichtZeile(final String text, final boolean boldFont) {
		this.text = text;
		this.boldFont = boldFont;
	}

	public final String getText() {
		return text;
	}

	public final boolean isBoldFont() {
		return boldFont;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("AuswertungUebersichtZeile [text=");
		builder.append(text);
		builder.append(", boldFont=");
		builder.append(boldFont);
		builder.append("]");
		return builder.toString();
	}
}
