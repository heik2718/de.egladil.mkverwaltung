//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * VerarbeitetesWorkbook ist eine Klammer für die Liste der LoesungszettelRohdaten.
 */
public class VerarbeitetesWorkbook {

	private List<LoesungszettelRohdaten> rohdaten = new ArrayList<>();

	private Klassenstufe klassenstufe;

	public void addEntry(LoesungszettelRohdaten entry) {
		rohdaten.add(entry);
	}

	/**
	 *
	 * @return unmodifiable List
	 */
	public List<LoesungszettelRohdaten> getRohdaten() {
		return Collections.unmodifiableList(rohdaten);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (LoesungszettelRohdaten zeile : rohdaten) {
			sb.append("\n");
			sb.append(zeile.toString());
		}
		return sb.toString();
	}

	public boolean isLeer() {
		return rohdaten.isEmpty();
	}

	public Klassenstufe getKlasse() {
		return klassenstufe;
	}

	public void setKlasse(Klassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
	}
}
