//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.urkunden.UebersichtFontProvider;
import de.egladil.mkv.auswertungen.urkunden.inhalt.AuswertungUebersichtZeile;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AuswertungsuebersichtGenerator
 */
public class AuswertungsuebersichtGenerator {

	private static final String MF_MEHRERE_KAENGURUSPRUENGE = "{0} von {1} Kindern haben den weitesten Kängurusprung gemacht";

	private final UebersichtFontProvider fontProvider;

	private final String wettbewerbsjahr;

	/**
	 * AuswertungsuebersichtGenerator
	 */
	public AuswertungsuebersichtGenerator() {
		this.fontProvider = new UebersichtFontProvider();
		wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	/**
	 *
	 * @param urkunden List
	 * @return byte[]
	 */
	public byte[] generiereUebersicht(final AuswertungDaten auswertungDaten) {

		final List<AuswertungUebersichtZeile> zeilen = generateZeilen(auswertungDaten);

		final Document doc = new Document(PageSize.A4);
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			PdfWriter.getInstance(doc, out);
			doc.open();
			for (final AuswertungUebersichtZeile zeile : zeilen) {
				final Font font = zeile.isBoldFont() ? fontProvider.getFontBold() : fontProvider.getFontNormal();
				doc.add(new Paragraph(zeile.getText(), font));
			}
			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}

	List<AuswertungUebersichtZeile> generateZeilen(final AuswertungDaten auswertungDaten) {

		if (auswertungDaten == null) {
			throw new IllegalArgumentException("auswertungDaten null");
		}

		final List<AuswertungUebersichtZeile> zeilen = new ArrayList<>();

		zeilen.add(new AuswertungUebersichtZeile("Auswertung Minikänguru " + wettbewerbsjahr, true));
		zeilen.add(new AuswertungUebersichtZeile("Platzierungen", true));

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			zeilen.add(new AuswertungUebersichtZeile(klassenstufe.getLabel(), true));
			zeilen.addAll(getTextPlatzierungFuerKlassenstufe(auswertungDaten, klassenstufe));
		}

		if (auswertungDaten.getAnzahlKaengurugewinner() > 0) {
			zeilen.add(new AuswertungUebersichtZeile("Gewinner Kängurusprung", true));

			for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
				zeilen.add(new AuswertungUebersichtZeile(klassenstufe.getLabel(), true));
				zeilen.addAll(getTextKaengurusprungFuerKlassenstufe(auswertungDaten, klassenstufe));
			}
		}

		if (auswertungDaten.shallPrintWarnungKaengurusprung()) {
			zeilen.add(new AuswertungUebersichtZeile("\n", false));
			zeilen.add(new AuswertungUebersichtZeile(
				"In mindestens einer Klassenstufe gab es mehr als ein Kind mit dem weitesten Kängurusprung.", true));
			zeilen.add(new AuswertungUebersichtZeile(
				"Für diese Kinder wurden keine Kängurusprungurkunden erzeugt. Falls Sie für alle diese Kinder ebenfalls Kängurusprungurkunden drucken möchten, drucken Sie bitte die Urkunden noch einmal und klicken dabei die Checkbox \"nur Urkunden Kängurusprung\" an.",
				false));
		}
		return zeilen;
	}

	private List<AuswertungUebersichtZeile> getTextKaengurusprungFuerKlassenstufe(final AuswertungDaten auswertungDaten,
		final Klassenstufe klassenstufe) {

		final int anzahl = auswertungDaten.getAnzahlKaengurugewinner(klassenstufe);
		final List<AuswertungUebersichtZeile> result = new ArrayList<>();
		if (anzahl == 1) {
			final List<UrkundeInhalt> urkunden = auswertungDaten.getKaenguruspruengeFuerKlassenstufeSorted(klassenstufe);
			if (urkunden != null) {
				for (final UrkundeInhalt urkunde : urkunden) {
					result.add(new AuswertungUebersichtZeile(getTextKaengurusprung(urkunde), false));
				}
			}
		} else {
			final int anzahlTeilnehmer = auswertungDaten.getAnzahlTeilnehmer(klassenstufe);
			result.add(new AuswertungUebersichtZeile(MessageFormat.format(MF_MEHRERE_KAENGURUSPRUENGE,
				new Object[] { Integer.valueOf(anzahl), Integer.valueOf(anzahlTeilnehmer) }), false));
		}
		return result;
	}

	private List<AuswertungUebersichtZeile> getTextPlatzierungFuerKlassenstufe(final AuswertungDaten auswertungDaten,
		final Klassenstufe klassenstufe) {
		final List<AuswertungUebersichtZeile> result = new ArrayList<>();

		int platz = 1;
		int platzGleich = 1;
		int punkteVorgaenger = -1;

		final List<UrkundeInhalt> klassenliste = auswertungDaten.getUrkundenFuerKlassenstufeSorted(klassenstufe);
		if (klassenliste != null) {
			for (final UrkundeInhalt urkundeInhalt : klassenliste) {
				final int punkteAktuell = urkundeInhalt.getPunkte();
				boolean mitPlatz = true;
				if (punkteAktuell < punkteVorgaenger) {
					platz = platz + platzGleich;
					platzGleich = 1;
				}
				if (punkteAktuell == punkteVorgaenger) {
					platzGleich++;
					mitPlatz = false;
				}
				final String text = getTextPunkte(urkundeInhalt, platz, mitPlatz);
				result.add(new AuswertungUebersichtZeile(text, false));
				punkteVorgaenger = punkteAktuell;
			}
		}
		return result;
	}

	/**
	 *
	 * @param urkunden List
	 * @return byte[]
	 */
	byte[] generiereUebersicht(final Map<Klassenstufe, List<UrkundeInhalt>> urkunden,
		final Map<Klassenstufe, List<UrkundeInhalt>> kaenguruspruenge, final String wettbewerbsjahr) {
		final AuswertungDaten auswertungDaten = new AuswertungDaten();
		auswertungDaten.setKaengurusprungUrkunden(kaenguruspruenge);
		auswertungDaten.setTeilnehmerurkunden(urkunden);
		return this.generiereUebersicht(auswertungDaten);
	}

	private String getTextPunkte(final UrkundeInhalt urkundeInhalt, final int platz, final boolean mitPlatz) {
		final StringBuffer sb = new StringBuffer();
		if (mitPlatz) {
			sb.append(platz);
			sb.append(". ");
		} else {
			final String p = platz + ".";
			for (int i = 0; i < p.length() + 2; i++) {
				sb.append(" ");
			}
		}
		sb.append(getTextNameZusatzKlassenname(urkundeInhalt));
		sb.append(", ");
		sb.append(urkundeInhalt.getPunkteText());
		sb.append(" Punkte");
		return sb.toString();
	}

	private String getTextNameZusatzKlassenname(final UrkundeInhalt urkundeInhalt) {
		final StringBuffer sb = new StringBuffer();
		sb.append(urkundeInhalt.getTeilnehmername());
		if (StringUtils.isNoneBlank(urkundeInhalt.getZusatz())) {
			sb.append(" (");
			sb.append(urkundeInhalt.getZusatz());
			sb.append(")");
		}
		sb.append(", ");
		sb.append(urkundeInhalt.getKlassenname());
		return sb.toString();
	}

	private String getTextKaengurusprung(final UrkundeInhalt urkundeInhalt) {
		final StringBuffer sb = new StringBuffer();
		sb.append(getTextNameZusatzKlassenname(urkundeInhalt));
		sb.append(", Länge Kängurusprung: ");
		sb.append(urkundeInhalt.getKaengurusprung());
		return sb.toString();
	}
}
