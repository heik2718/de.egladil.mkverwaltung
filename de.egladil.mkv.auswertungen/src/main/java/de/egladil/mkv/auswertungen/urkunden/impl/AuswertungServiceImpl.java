//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.auswertungen.urkunden.EinzelurkundeGeneratorFactory;
import de.egladil.mkv.auswertungen.urkunden.PdfMerger;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundenmotivProvider;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.dao.IAuswertungsgruppeDao;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.SchuleUrkundenauftrag;
import de.egladil.mkv.persistence.payload.request.TeilnehmerUrkundenauftrag;
import de.egladil.mkv.persistence.payload.response.compare.TeilnahmejahrDescComparator;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmejahr;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.impl.CollectTeilnehmerkuerzelCommand;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * AuswertungServiceImpl
 */
@Singleton
public class AuswertungServiceImpl implements AuswertungService {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungServiceImpl.class);

	private static final String DATUM_FORMAT = "dd.MM.yyyy";

	private final ILoesungszettelDao loesungszettelDao;

	private final IAuswertungsgruppeDao auswertungsgruppeDao;

	private final IAuswertungDownloadDao auswertungDownloadDao;

	private final TeilnahmenFacade teilnahmenFacade;

	private final TeilnehmerFacade teilnehmerFacade;

	private final IVerteilungGenerator verteilungGenerator;

	/**
	 * AuswertungServiceImpl
	 */
	@Inject
	public AuswertungServiceImpl(final ILoesungszettelDao loesungszettelDao, final IAuswertungsgruppeDao auswertungsgruppeDao,
		final IAuswertungDownloadDao auswertungDownloadDao, final TeilnahmenFacade teilnahmenFacade,
		final TeilnehmerFacade teilnehmerFacade, final IVerteilungGenerator verteilungGenerator) {
		this.loesungszettelDao = loesungszettelDao;
		this.auswertungsgruppeDao = auswertungsgruppeDao;
		this.auswertungDownloadDao = auswertungDownloadDao;
		this.teilnahmenFacade = teilnahmenFacade;
		this.teilnehmerFacade = teilnehmerFacade;
		this.verteilungGenerator = verteilungGenerator;
	}

	@Override
	public Optional<String> generiereAuswertungFuerEinzelteilnehmer(final String benutzerUuid,
		final TeilnehmerUrkundenauftrag auftrag, final TeilnahmeIdentifier teilnahmeIdentifier) {

		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid null oder leer");
		}
		if (auftrag == null) {
			throw new IllegalArgumentException("auftrag null");
		}
		if (auftrag.getFarbschemaName() == null) {
			throw new IllegalArgumentException("auftrag.farbschema null");
		}
		if (auftrag.getTeilnehmerKuerzel() == null) {
			throw new IllegalArgumentException("auftrag.teilnehmerKuerzel null");
		}
		if (auftrag.getTeilnehmerKuerzel().length == 0) {
			throw new IllegalArgumentException("auftrag.teilnehmerKuerzel leer");
		}

		final Farbschema farbschema = Farbschema.valueOf(auftrag.getFarbschemaName());
		final Optional<String> result = auswertungGenerieren(benutzerUuid, farbschema, auftrag.getTeilnehmerKuerzel(),
			getDatum(auftrag.getDatum()), teilnahmeIdentifier);
		return result;
	}

	@Override
	public Optional<String> generiereKaengurusprungurkundenfuerEinzelteilnehmer(final String benutzerUuid,
		final TeilnehmerUrkundenauftrag auftrag, final TeilnahmeIdentifier teilnahmeIdentifier) {

		if (StringUtils.isBlank(benutzerUuid)) {
			throw new IllegalArgumentException("benutzerUuid null oder leer");
		}
		if (auftrag == null) {
			throw new IllegalArgumentException("auftrag null");
		}
		if (auftrag.getFarbschemaName() == null) {
			throw new IllegalArgumentException("auftrag.farbschema null");
		}
		if (auftrag.getTeilnehmerKuerzel() == null) {
			throw new IllegalArgumentException("auftrag.teilnehmerKuerzel null");
		}
		if (auftrag.getTeilnehmerKuerzel().length == 0) {
			throw new IllegalArgumentException("auftrag.teilnehmerKuerzel leer");
		}

		final Farbschema farbschema = Farbschema.valueOf(auftrag.getFarbschemaName());
		final Optional<String> result = kaengurusprungUrkundenGenerieren(benutzerUuid, farbschema, auftrag.getTeilnehmerKuerzel(),
			getDatum(auftrag.getDatum()), teilnahmeIdentifier);
		return result;
	}

	@Override
	public Optional<String> generiereSchulauswertung(final String benutzerUuid, final SchuleUrkundenauftrag auftrag,
		final TeilnahmeIdentifier teilnahmeIdentifier) {

		if (auftrag == null) {
			throw new IllegalArgumentException("auftrag null");
		}
		if (auftrag.getFarbschemaName() == null) {
			throw new IllegalArgumentException("auftrag.farbschema null");
		}
		if (auftrag.getKuerzelRootgruppe() == null) {
			throw new IllegalArgumentException("auftrag.kuerzelRootgruppe null");
		}

		final Optional<Auswertungsgruppe> opt = auswertungsgruppeDao.findByUniqueKey(Auswertungsgruppe.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, auftrag.getKuerzelRootgruppe());

		if (!opt.isPresent()) {
			throw new ResourceNotFoundException(
				"konnte Rootgruppe mit kuerzel " + auftrag.getKuerzelRootgruppe() + " nicht finden");
		}

		final Farbschema farbschema = Farbschema.valueOf(auftrag.getFarbschemaName());

		final List<String> teilnehmerkuerzel = new ArrayList<>();
		new CollectTeilnehmerkuerzelCommand(opt.get(), teilnehmerkuerzel).execute();

		if (teilnehmerkuerzel.isEmpty()) {
			throw new MKVException("Die Schule hat keine registrierten Kinder oder es wurden noch keine Antworten erfasst.");
		}

		final Optional<String> result = auswertungGenerieren(benutzerUuid, farbschema, teilnehmerkuerzel.toArray(new String[] {}),
			getDatum(auftrag.getDatum()), teilnahmeIdentifier);
		return result;
	}

	@Override
	public Optional<String> generiereKaengurusprungurkundenfuerSchule(final String benutzerUuid,
		final SchuleUrkundenauftrag auftrag, final TeilnahmeIdentifier teilnahmeIdentifier) {
		if (auftrag == null) {
			throw new IllegalArgumentException("auftrag null");
		}
		if (auftrag.getFarbschemaName() == null) {
			throw new IllegalArgumentException("auftrag.farbschema null");
		}
		if (auftrag.getKuerzelRootgruppe() == null) {
			throw new IllegalArgumentException("auftrag.kuerzelRootgruppe null");
		}

		final Optional<Auswertungsgruppe> opt = auswertungsgruppeDao.findByUniqueKey(Auswertungsgruppe.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, auftrag.getKuerzelRootgruppe());

		if (!opt.isPresent()) {
			throw new ResourceNotFoundException(
				"konnte Rootgruppe mit kuerzel " + auftrag.getKuerzelRootgruppe() + " nicht finden");
		}

		final Farbschema farbschema = Farbschema.valueOf(auftrag.getFarbschemaName());

		final List<String> teilnehmerkuerzel = new ArrayList<>();
		new CollectTeilnehmerkuerzelCommand(opt.get(), teilnehmerkuerzel).execute();

		if (teilnehmerkuerzel.isEmpty()) {
			throw new MKVException("Die Schule hat keine registrierten Kinder oder es wurden noch keine Antworten erfasst.");
		}
		if (teilnehmerkuerzel.size() == 1) {
			return Optional.empty();
		}
		final Optional<String> result = kaengurusprungUrkundenGenerieren(benutzerUuid, farbschema,
			teilnehmerkuerzel.toArray(new String[teilnehmerkuerzel.size()]), getDatum(auftrag.getDatum()), teilnahmeIdentifier);
		return result;

	}

	private Optional<String> auswertungGenerieren(final String benutzerUuid, final Farbschema farbschema,
		final String[] teilnehmerkuerzel, final String datum, final TeilnahmeIdentifier teilnahmeIdentifier) {

		final UrkundenmotivProvider urkundenMotivProvider = DefaultUrkundenmotivProvider.ausFarbschema(farbschema);
		final int gesamt = teilnehmerkuerzel.length;
		LOG.info("generieren Urkunden für {} Teilnehmer", gesamt);

		final AuswertungDaten daten = new AuswertungDatenGenerator(loesungszettelDao, teilnehmerFacade)
			.generateComplete(teilnehmerkuerzel, urkundenMotivProvider, datum);

		final List<byte[]> urkundenPdf = new ArrayList<>();

		if (teilnehmerkuerzel.length > 1) {
			final AuswertungsuebersichtGenerator auswertungsuebersichtGenerator = new AuswertungsuebersichtGenerator();
			urkundenPdf.add(auswertungsuebersichtGenerator.generiereUebersicht(daten));
		}

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			if (teilnehmerkuerzel.length > 1) {
				final List<UrkundeInhalt> kaenguruspruenge = daten.getKaenguruspruengeFuerKlassenstufeSorted(klassenstufe);
				if (kaenguruspruenge != null) {
					for (final UrkundeInhalt kaengurusprung : kaenguruspruenge) {
						final byte[] pdf = EinzelurkundeGeneratorFactory.createKaengurusprungurkundeGenerator()
							.generate(kaengurusprung);
						urkundenPdf.add(pdf);
					}
				}
			}
			final List<UrkundeInhalt> teilnehmerurkunden = daten.getUrkundenFuerKlassenstufeSorted(klassenstufe);
			if (teilnehmerurkunden != null) {
				for (final UrkundeInhalt urkunde : teilnehmerurkunden) {
					final byte[] pdf = EinzelurkundeGeneratorFactory.createTeilnehmerurkundeGenerator().generate(urkunde);
					urkundenPdf.add(pdf);
				}
			}
		}

		final byte[] pdfs = new PdfMerger().concatPdf(urkundenPdf);

		final AuswertungDownload auswertungDownload = AuswertungDownload.forTeilnahmeIdentifier(teilnahmeIdentifier);
		auswertungDownload.setDaten(pdfs);
		auswertungDownload.setGeaendertDurch(benutzerUuid);
		auswertungDownload.setDownloadCode(new KuerzelGenerator().generateDefaultKuerzelWithTimestamp());

		final AuswertungDownload persisted = auswertungDownloadDao.persist(auswertungDownload);

		return Optional.of(persisted.getDownloadCode());

	}

	private Optional<String> kaengurusprungUrkundenGenerieren(final String benutzerUuid, final Farbschema farbschema,
		final String[] teilnehmerkuerzel, final String datum, final TeilnahmeIdentifier teilnahmeIdentifier) {
		final UrkundenmotivProvider urkundenMotivProvider = DefaultUrkundenmotivProvider.ausFarbschema(farbschema);
		LOG.info("generieren Kängurusprungurkunden für {}", teilnahmeIdentifier);

		final AuswertungDaten daten = new AuswertungDatenGenerator(loesungszettelDao, teilnehmerFacade)
			.generateJumpCerts(teilnehmerkuerzel, urkundenMotivProvider, datum);

		final List<byte[]> urkundenPdf = new ArrayList<>();

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			if (teilnehmerkuerzel.length > 1) {
				final List<UrkundeInhalt> kaenguruspruenge = daten.getKaenguruspruengeFuerKlassenstufeSorted(klassenstufe);
				if (kaenguruspruenge != null) {
					for (final UrkundeInhalt kaengurusprung : kaenguruspruenge) {
						final byte[] pdf = EinzelurkundeGeneratorFactory.createKaengurusprungurkundeGenerator()
							.generate(kaengurusprung);
						urkundenPdf.add(pdf);
					}
				}
			}
		}

		final byte[] pdfs = new PdfMerger().concatPdf(urkundenPdf);

		final AuswertungDownload auswertungDownload = AuswertungDownload.forTeilnahmeIdentifier(teilnahmeIdentifier);
		auswertungDownload.setDaten(pdfs);
		auswertungDownload.setGeaendertDurch(benutzerUuid);
		auswertungDownload.setDownloadCode(new KuerzelGenerator().generateDefaultKuerzelWithTimestamp());

		final AuswertungDownload persisted = auswertungDownloadDao.persist(auswertungDownload);

		return Optional.of(persisted.getDownloadCode());

	}

	@Override
	@Deprecated
	public Optional<AuswertungDownload> findAndIncrementAuswertungDownload(final String downloadcode) {
		final Optional<AuswertungDownload> opt = auswertungDownloadDao.findByUniqueKey(AuswertungDownload.class,
			AuswertungDownload.UNIQUE_ATTRIBUTE_NAME, downloadcode);
		if (opt.isPresent()) {
			final AuswertungDownload d = opt.get();
			d.setAnzahl(d.getAnzahl() + 1);
			final AuswertungDownload persisted = auswertungDownloadDao.persist(d);
			return Optional.of(persisted);
		}
		return Optional.empty();
	}

	public String getDatum(final String datumString) {
		if (StringUtils.isNoneBlank(datumString)) {
			return datumString;
		}
		return new SimpleDateFormat(DATUM_FORMAT).format(new Date());
	}

	@Override
	public List<GesamtpunktverteilungDaten> generiereGesamtpunktverteilung(final String jahr) {
		final List<String> jahre = teilnahmenFacade.getAuswertbareJahre();
		if (!jahre.contains(jahr)) {
			throw new ResourceNotFoundException("Keine Daten für " + jahr + " vorhanden");
		}

		final List<GesamtpunktverteilungDaten> result = new ArrayList<>();

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			final GesamtpunktverteilungDaten daten = this.generiereGesamtpunktverteilung(jahr, klassenstufe);
			if (daten != null) {
				result.add(daten);
			}
		}

		return result;
	}

	@Override
	public List<GesamtpunktverteilungDaten> generiereGesamtpunktverteilung(final String jahr, final String alleKuerzel)
		throws ResourceNotFoundException {

		final List<GesamtpunktverteilungDaten> result = new ArrayList<>();

		final List<String> kuerzelliste = Splitter.on(',').omitEmptyStrings().trimResults().splitToList(alleKuerzel);

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			final GesamtpunktverteilungDaten daten = this.generiereGesamtpunktverteilung(jahr, klassenstufe, kuerzelliste);
			if (daten != null) {
				result.add(daten);
			}
		}

		return result;
	}

	@Override
	public Map<Klassenstufe, String> berechneMediane(final String jahr) {

		final Map<Klassenstufe, String> result = new HashMap<>();

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			final List<Loesungszettel> loesungszettel = teilnahmenFacade
				.findLoesungszettelByJahrUndKlassenstufe(String.valueOf(jahr), klassenstufe);

			if (!loesungszettel.isEmpty()) {
				final List<ILoesungszettel> datenbasis = new ArrayList<>(loesungszettel.size());
				datenbasis.addAll(loesungszettel);
				final String median = this.verteilungGenerator.berechneMedian(datenbasis);
				result.put(klassenstufe, median);
			}
		}

		return result;
	}

	private GesamtpunktverteilungDaten generiereGesamtpunktverteilung(final String jahr, final Klassenstufe klassenstufe) {

		final List<Loesungszettel> loesungszettel = teilnahmenFacade.findLoesungszettelByJahrUndKlassenstufe(String.valueOf(jahr),
			klassenstufe);

		if (loesungszettel.isEmpty()) {
			LOG.warn("Keine Gesamtauswertung mit Jahr {} und Klassenstufe {} vorhanden", jahr, klassenstufe.getNummer());
			return null;
		}

		final List<ILoesungszettel> alleZettel = new ArrayList<>();
		alleZettel.addAll(loesungszettel);

		final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(alleZettel, klassenstufe,
			String.valueOf(jahr));
		return daten;
	}

	private GesamtpunktverteilungDaten generiereGesamtpunktverteilung(final String jahr, final Klassenstufe klassenstufe,
		final List<String> kuerzelliste) {

		final List<String> landkuerzel = kuerzelliste.stream().filter(k -> !MKVConstants.GRUPPE_PRIVATTEILNAHMEN_KUERZEL.equals(k))
			.collect(Collectors.toList());

		final List<Loesungszettel> loesungszettel = new ArrayList<>();

		final boolean mitPrivatteilnahmen = kuerzelliste.contains(MKVConstants.GRUPPE_PRIVATTEILNAHMEN_KUERZEL);

		if (!landkuerzel.isEmpty()) {
			loesungszettel.addAll(teilnahmenFacade.findLoesungszettelSchulen(jahr, klassenstufe, kuerzelliste));
		}
		if (mitPrivatteilnahmen) {
			loesungszettel.addAll(teilnahmenFacade.findLoesungszettelPrivat(jahr, klassenstufe));
		}

		if (loesungszettel.isEmpty()) {
			LOG.warn("Keine Gesamtauswertung mit Jahr {} und Klassenstufe {} vorhanden", jahr, klassenstufe.getNummer());
			return null;
		}

		final List<ILoesungszettel> alleZettel = new ArrayList<>();
		alleZettel.addAll(loesungszettel);

		final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(alleZettel, klassenstufe,
			String.valueOf(jahr));
		return daten;
	}

	@Override
	public List<Teilnahmejahr> getAuswertbareJahre() {
		final List<String> jahre = teilnahmenFacade.getAuswertbareJahre();
		final List<Teilnahmejahr> result = new ArrayList<>(jahre.size());

		for (final String jahr : jahre) {
			final Teilnahmejahr tj = new Teilnahmejahr(jahr);

			final HateoasPayload hateoasPayload = new HateoasPayload(jahr, "/auswertungen/jahre");
			hateoasPayload.addLink(new HateoasLink("/auswertungen/" + jahr + "/laender", "Übersicht Länder " + jahr, HttpMethod.GET,
				MediaType.APPLICATION_JSON));

			tj.setHateoasPayload(hateoasPayload);

			result.add(tj);
		}

		final Teilnahmejahr aktuellesJahr = new Teilnahmejahr(KontextReader.getInstance().getKontext().getWettbewerbsjahr());
		if (!result.contains(aktuellesJahr)) {
			result.add(aktuellesJahr);
		}

		Collections.sort(result, new TeilnahmejahrDescComparator());
		return result;
	}
}
