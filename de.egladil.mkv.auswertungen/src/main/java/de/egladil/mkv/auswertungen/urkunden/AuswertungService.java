//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.request.SchuleUrkundenauftrag;
import de.egladil.mkv.persistence.payload.request.TeilnehmerUrkundenauftrag;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmejahr;

/**
 * AuswertungService stellt eine einheitliche API zum Generieren und persistieren von Auswertungen zur Verfügung.
 */
public interface AuswertungService {

	/**
	 * Erzeugt für alle Teilnehmer in dem Auftrag eine Urkunde, sowie bei mehr als einem Teilnehmer eine Auswertung,
	 * fasst diese zu einem einzelnen PDF zusammen und persistiert sie in der Tabelle auswertungdownloads.
	 *
	 * @param benutzerUuid String der änderer
	 * @param auftrag TeilnehmerUrkundenauftrag darf nicht null sein und muss mindestens einen PublicTeilnehmer
	 * enthalten.
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 *
	 * @return Optional den downloadcode für Hypermedia.
	 */
	Optional<String> generiereAuswertungFuerEinzelteilnehmer(String benutzerUuid, TeilnehmerUrkundenauftrag auftrag,
		TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Erzeugt für alle Kängurusprungewinner der gegebenen Teilnehmer eine Kängurusprungurkunde und persistiert sie in
	 * der Tabelle auswertungdownloads.
	 *
	 * @param benutzerUuid String der änderer
	 * @param auftrag TeilnehmerUrkundenauftrag darf nicht null sein und muss mindestens einen PublicTeilnehmer
	 * enthalten.
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 *
	 * @return Optional den downloadcode für Hypermedia.
	 */
	Optional<String> generiereKaengurusprungurkundenfuerEinzelteilnehmer(String benutzerUuid, TeilnehmerUrkundenauftrag auftrag,
		TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Erzeugt für alle Teilnehmer der gegebenen Schule eine Urkunde, sowie eine Auswertung, fasst diese zu einem
	 * einzelnen PDF zusammen und persistiert sie in der Tabelle auswertungdownloads.
	 *
	 * @param benutzerUuid
	 * @param auftrag
	 * @param teilnahmeIdentifier
	 * @return Optional den downloadcode für Hypermedia.
	 */
	Optional<String> generiereSchulauswertung(String benutzerUuid, SchuleUrkundenauftrag auftrag,
		TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Erzeugt für alle Kängurusprungewinner der gegebenen Schule eine Kängurusprungurkunde und persistiert sie in der
	 * Tabelle auswertungdownloads.
	 *
	 * @param benutzerUuid
	 * @param auftrag
	 * @param teilnahmeIdentifier
	 * @return Optional den downloadcode für Hypermedia.
	 */
	Optional<String> generiereKaengurusprungurkundenfuerSchule(String benutzerUuid, SchuleUrkundenauftrag auftrag,
		TeilnahmeIdentifier teilnahmeIdentifier);

	/**
	 * Gibt den Download zum gegebenen Downloadcode zurück, falls vorhanden. In diesem Fall wird auch der
	 * Download-Zähler aktualisiert.
	 *
	 * @param downloadcode String darf nicht null sein
	 *
	 * @return Optional
	 */
	@Deprecated
	Optional<AuswertungDownload> findAndIncrementAuswertungDownload(String downloadcode);

	/**
	 * Generiert für jede Klassenstufe die GesamtpunktverteilungDaten.
	 *
	 * @param jahr String das gewünschte Jahr.
	 * @return List
	 * @throws ResourceNotFoundException falls das Jahr jahr nicht auswertbar ist.
	 */
	List<GesamtpunktverteilungDaten> generiereGesamtpunktverteilung(String jahr) throws ResourceNotFoundException;

	/**
	 * Generiert für alle Länder bzw. die Privatteilnahmen in der kuerzelliste für jede Klassenstufe die
	 * GesamtpunktverteilungDaten.
	 *
	 * @param jahr String das gewünschte Jahr.
	 * @param kuerzelliste String kommaseparierte Länderkürzel oder PRIVAT
	 * @return List
	 * @throws ResourceNotFoundException falls das Jahr jahr nicht auswertbar ist.
	 */
	List<GesamtpunktverteilungDaten> generiereGesamtpunktverteilung(String jahr, String kuerzelliste)
		throws ResourceNotFoundException;

	/**
	 * Berechnet die Mediane für das gegebene Wettbewerbsjahr.
	 *
	 * @param jahr String
	 * @return Map
	 */
	Map<Klassenstufe, String> berechneMediane(String jahr);

	/**
	 * @return List
	 */
	List<Teilnahmejahr> getAuswertbareJahre();

}
