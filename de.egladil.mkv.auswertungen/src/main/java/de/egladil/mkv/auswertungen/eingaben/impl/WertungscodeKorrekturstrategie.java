//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * WertungscodeKorrekturstrategie
 */
public class WertungscodeKorrekturstrategie implements INutzereingabeKorrekturstrategie {

	private static final List<String> EINGABE_R = Arrays.asList(new String[] { "e", "r", "t" });

	private static final List<String> EINGABE_F = Arrays.asList(new String[] { "d", "f", "g" });

	/**
	 * @see de.egladil.mkv.auswertungen.eingaben.impl.INutzereingabeKorrekturstrategie#korrigiere(java.lang.String)
	 */
	@Override
	public String korrigiere(String nutzereingabe) {
		if (nutzereingabe == null) {
			throw new NullPointerException("nutzereingabe");
		}
		if (StringUtils.isBlank(nutzereingabe)){
			return "n";
		}
		String[] substrings = StringUtils.splitPreserveAllTokens(nutzereingabe, ',');
		String[] korrigierteSubstrings = new String[substrings.length];
		for (int i = 0; i < substrings.length; i++) {
			korrigierteSubstrings[i] = korrigiereSubstring(substrings[i]);
		}
		return StringUtils.join(korrigierteSubstrings, ",");
	}

	private String korrigiereSubstring(String substring) {
		if (EINGABE_R.contains(substring.toLowerCase())) {
			return "r";
		}
		if (EINGABE_F.contains(substring.toLowerCase())) {
			return "f";
		}
		return "n";
	}
}
