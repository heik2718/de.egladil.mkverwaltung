//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * KatalogItemKlasse ist die Gruppierung von Loesungszettelrohdaten zum gleichen KatalogItem klassenstufenweise. Kann
 * verwendet werden, um aus Lösungszettelrohdaten eine Schulübersicht über Median und Antworthäufigkeiten zu berechnen.
 */
public class KatalogItemKlasse {

	private final KatalogItem katalogItem;

	private final Map<Klassenstufe, List<LoesungszettelRohdaten>> datenMap = new HashMap<>();

	/**
	 * KatalogItemKlasse
	 */
	public KatalogItemKlasse(final KatalogItem katalogItem) {
		this.katalogItem = katalogItem;
	}

	public void putRohdaten(final Klassenstufe klassenstufe, final LoesungszettelRohdaten rohdaten) {
		List<LoesungszettelRohdaten> daten = datenMap.get(klassenstufe);
		if (daten == null) {
			daten = new ArrayList<>();
		}
		daten.add(rohdaten);
		datenMap.put(klassenstufe, daten);
	}

	/**
	 * @param klassenstufe Klassenstufe
	 * @return List oder null
	 */
	public List<LoesungszettelRohdaten> getRohdaten(final Klassenstufe klassenstufe) {
		return datenMap.get(klassenstufe);
	}

	public final KatalogItem getKatalogItem() {
		return katalogItem;
	}

	public final Map<Klassenstufe, List<LoesungszettelRohdaten>> getDatenMap() {
		return datenMap;
	}

}
