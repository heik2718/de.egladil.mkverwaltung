//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

/**
 * Klassen, die Dateien z.B. aus Uploads verarbeiten.
 */
package de.egladil.mkv.auswertungen.files;