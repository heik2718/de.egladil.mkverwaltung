//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.mkv.auswertungen.files.impl.IWriteToFileCommand;
import de.egladil.mkv.auswertungen.files.impl.UploadUtils;
import de.egladil.mkv.auswertungen.files.impl.WriteBytesToFileCommand;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Dateiart;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.Downloaddaten;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IdentifierMKVKontoProvider;
import de.egladil.mkv.persistence.utils.MockInMemoryDB;
import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * UploadService
 */
@Singleton
public class UploadDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(UploadDelegate.class);

	private static final String PROTOKOLL_FILESIZE_PATTERN = "Größe {0} byte";

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final IProtokollService protokollService;

	private AuswertungstabelleFileWriter tabelleFileWriter;

	private final IUploadDao uploadDao;

	private UploadUtils uploadUtils = new UploadUtils();

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	/**
	 * Erzeugt eine Instanz von UploadDelegate
	 */
	@Inject
	public UploadDelegate(final ILehrerkontoDao lehrerkontoDao, final IPrivatkontoDao privatkontoDao, final IUploadDao uploadDao,
		final IProtokollService protokollService, final AuswertungstabelleFileWriter tabelleFileWriter) {
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.uploadDao = uploadDao;
		this.protokollService = protokollService;
		this.tabelleFileWriter = tabelleFileWriter;
	}

	/**
	 * Prüft die Größe des payloads. Quarantäne-Verzeichnis gelegt.
	 *
	 * @param benutzerUuid String die UUID desjenigen, der eine Datei hochläd. Darf nicht null sein.
	 * @param data byte[] die gesendeten Daten - darf nicht null sein.
	 * @param maxFileSize long die erlaubte Größe
	 * @param maxFileSizeText String der Text, der in einer Warnmeldung angezeigt wird, wenn die Datei zu groß ist. Darf
	 * nicht null sein.
	 * @return Optional<PublicMessage>
	 * @throws NullPointerException wenn einer der Parameter null ist
	 */
	public Optional<APIResponsePayload> validatePayloadSize(final String benutzerUuid, final long size, final int maxFileSize,
		final String maxFileSizeText) throws NullPointerException, IOException {
		if (benutzerUuid == null) {
			throw new NullPointerException("benutzerUuid null");
		}
		if (maxFileSizeText == null) {
			throw new NullPointerException("maxFileSizeText null");
		}
		if (size > maxFileSize) {
			LOG.debug("[tatsaechliche Groesse=" + size + ", erlaubte Groesse=" + maxFileSize + "]");
			final String message = MessageFormat.format(applicationMessages.getString("upload.size"),
				new Object[] { maxFileSizeText });
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.warn(message));
			final String protokollMessage = MessageFormat.format(PROTOKOLL_FILESIZE_PATTERN, new Object[] { "" + size });
			final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_SIZE.getKuerzel(), benutzerUuid, protokollMessage);
			protokollService.protokollieren(ereignis);
			return Optional.of(pm);
		}
		return Optional.empty();
	}

	/**
	 * Schreibt die Daten ins UploadVerzeichnis, speichert die Info im Benutzerkonto und protokolliert.
	 *
	 * @param benutzerUuid String UUID des Benutzers (Lehrers oder Privatbenutzers) - darf nicht null sein
	 * @param data byte[] die Datei - darf nicht null sein
	 * @param pathUploadDir String Pfad zum Upload-Verzeichnis - darf nicht null sein
	 * @param pathSandboxDir String Pfad zum Sandbox-Verzeichnis für potentiell gefährliche Dateien - darf nicht null
	 * sein
	 * @param wettbewerbsjahr String das Wettbewerbsjahr - darf nicht null sein
	 * @return Optional
	 * @throws NullPointerException wenn einer der Parameter null ist
	 */
	public UploadLog uploadBytes(final String benutzerUuid, final byte[] data, final String pathUploadDir,
		final String pathSandboxDir, final String wettbewerbsjahr)
		throws NullPointerException, IOException, URISyntaxException, UnknownAccountException, MKVException,
		EgladilConcurrentModificationException, EgladilStorageException, ParserSecurityException {
		if (benutzerUuid == null) {
			throw new NullPointerException("benutzerUuid null");
		}
		if (data == null) {
			throw new NullPointerException("data null");
		}
		if (pathUploadDir == null) {
			throw new NullPointerException("pathUploadDir null");
		}
		if (pathSandboxDir == null) {
			throw new NullPointerException("pathUploadDir null");
		}
		if (wettbewerbsjahr == null) {
			throw new NullPointerException("wettbewerbsjahr null");
		}
		final Optional<IMKVKonto> opt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(benutzerUuid, lehrerkontoDao,
			privatkontoDao);
		if (!opt.isPresent()) {
			throw new UnknownAccountException("kein Lehrerkonto oder Privatkonto mit UUID [" + benutzerUuid + "] gefunden");
		}
		final IMKVKonto konto = opt.get();

		final UploadLog result = createWriteAndPersistDatei(data, pathUploadDir, pathSandboxDir, wettbewerbsjahr, konto,
			new WriteBytesToFileCommand(data));
		return result;
	}

	/**
	 * Erzeugt eine Upload, sofern es noch keine mit der gegebenen Checksumme gibt.
	 *
	 * @param data
	 * @param pathUploadDir
	 * @param wettbewerbsjahr
	 * @param konto
	 * @param uploadMimeType
	 * @return
	 */
	UploadLog createWriteAndPersistDatei(final byte[] data, final String pathUploadDir, final String pathSandboxDir,
		final String wettbewerbsjahr, final IMKVKonto konto, final IWriteToFileCommand writeCommand)
		throws IllegalArgumentException, IOException, URISyntaxException, MKVException {

		final TeilnahmeIdentifierProvider teilnahme = konto.getTeilnahmeZuJahr(wettbewerbsjahr);

		if (teilnahme == null) {
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Konto hat keine Teilnahme im aktuellen Jahr: {}", konto);
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.info(applicationMessages.getString("upload.success")));
			return new UploadLog(pm, Optional.empty(), "Teilnahme nicht gefunden.");
		}
		APIResponsePayload pm = new APIResponsePayload(APIMessage.info(applicationMessages.getString("upload.success")));
		Upload upload = null;
		try {
			// String checksum = uploadUtils.getChecksum(data);

			final Optional<TeilnahmeIdentifier> optSchluessel = konto.getTeilnahmeIdentifier(wettbewerbsjahr);
			if (!optSchluessel.isPresent()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_UPLOAD
					+ "der Benutzer ist zum aktuellen Wettbewerbsjahr nicht angemeldet. Ignoriere die Daten. {}", konto);
				pm = new APIResponsePayload(APIMessage.info(applicationMessages.getString("upload.success")));
				return new UploadLog(pm, Optional.empty(), "Der Benutzer ist zum aktuellen Wettbewerbsjahr nicht angemeldet.");
			}
			final String checksum = uploadUtils.getChecksum(data);

			final TeilnahmeIdentifier teilnahmeIdentifier = optSchluessel.get();
			final Optional<Upload> optUpload = uploadDao.findUpload(teilnahmeIdentifier, checksum);

			if (optUpload.isPresent()) {
				LOG.info(GlobalConstants.LOG_PREFIX_UPLOAD + "schon bekannt. {}", optUpload.get());
				pm = new APIResponsePayload(APIMessage.info(applicationMessages.getString("upload.bekannt")));
				return new UploadLog(pm, Optional.empty(), "Datei wurde bereits hochgeladen.");
			}

			upload = tabelleFileWriter.initPaths(pathUploadDir, pathSandboxDir).writeFile(data, teilnahmeIdentifier,
				konto.getUuid());
			upload.initWith(optSchluessel.get());

			if (UploadMimeType.UNKNOWN == upload.getMimetype()) {

				final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_MIMETYPE.getKuerzel(), MockInMemoryDB.UUID_LEHRER_1,
					"Unbekannter MIMETYPE - Dateiname " + upload.getDateiname());
				protokollService.protokollieren(ereignis);
			}

			final List<Downloaddaten> alleDownloads = konto.alleDownloads();
			final boolean ohneDownload = !hatAufgabenHeruntergeladen(alleDownloads, wettbewerbsjahr);

			upload.setUploadOhneDownloads(ohneDownload);

			final Upload persistenterUpload = uploadDao.persist(upload);

			if (persistenterUpload != null) {
				return new UploadLog(pm, Optional.of(persistenterUpload), "neuer Upload");
			}
			return new UploadLog(pm, Optional.of(upload), "neuer Upload");

		} catch (final ParserSecurityException e) {
			LOG.warn("{} {} hat Datei mit potentiell gefaehrlichem Inhalt hochgeladen: {}", GlobalConstants.LOG_PREFIX_BOT,
				konto.getUuid(), e.getMessage());
			return new UploadLog(pm, Optional.empty(), "ParserSecurityException");
		} catch (final PersistenceException e) {

			if (upload != null) {
				tabelleFileWriter.deleteDateiQuietly(upload.getMimetype(), upload.getDateiname());
			}
			final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optConc.isPresent()) {
				throw new EgladilConcurrentModificationException("Jemand anders hat den Upload vorher geaendert");
			}
			throw new EgladilStorageException("PersistenceException beim Speichern eines Uploads: " + e.getMessage(), e);
		}
	}

	/**
	 *
	 * @param alleDownloads
	 * @param wettbewerbsjahr
	 * @return boolean
	 */
	boolean hatAufgabenHeruntergeladen(final List<Downloaddaten> alleDownloads, final String wettbewerbsjahr) {
		for (final Downloaddaten downloaddaten : alleDownloads) {
			if (downloaddaten.getJahr().equals(wettbewerbsjahr)
				&& (downloaddaten.getDateiname().equals(Dateiart.UNTERLAGEN_KLASSE_1.getFileName(wettbewerbsjahr))
					|| downloaddaten.getDateiname().equals(Dateiart.UNTERLAGEN_KLASSE_2.getFileName(wettbewerbsjahr)))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Zu Testzwecken kann hier ein Mock gesetzt werden.
	 *
	 * @param uploadUtils
	 */
	protected void setUploadUtils(final UploadUtils uploadUtils) {
		this.uploadUtils = uploadUtils;
	}

	void setTabelleFileWriter(final AuswertungstabelleFileWriter tabelleFileWriter) {
		this.tabelleFileWriter = tabelleFileWriter;
	}



}
