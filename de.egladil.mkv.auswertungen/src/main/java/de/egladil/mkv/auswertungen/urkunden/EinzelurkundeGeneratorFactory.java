//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden;

import de.egladil.mkv.auswertungen.urkunden.impl.EinzelurkundeGeneratorImpl;
import de.egladil.mkv.auswertungen.urkunden.render.KaengurusprungRenderer;
import de.egladil.mkv.auswertungen.urkunden.render.TeilnehmerPunkteRenderer;

/**
 * EinzelurkundeGeneratorFactory
 */
public class EinzelurkundeGeneratorFactory {

	public static EinzelurkundeGenerator createTeilnehmerurkundeGenerator() {
		return new EinzelurkundeGeneratorImpl(new TeilnehmerPunkteRenderer());
	}

	public static EinzelurkundeGenerator createKaengurusprungurkundeGenerator() {
		return new EinzelurkundeGeneratorImpl(new KaengurusprungRenderer());
	}
}
