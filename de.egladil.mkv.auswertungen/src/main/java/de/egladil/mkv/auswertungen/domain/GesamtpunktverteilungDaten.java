//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.auswertungen.statistik.compare.AufgabeErgebnisItemAscendingSorter;
import de.egladil.mkv.auswertungen.statistik.compare.GesamtpunktverteilungItemDescendingSorter;
import de.egladil.mkv.auswertungen.statistik.compare.RohpunktItemDescendingSorter;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * GesamtpunktverteilungDaten enthält die Daten der Gesamtpunktverteilung.
 */
public class GesamtpunktverteilungDaten {

	@JsonProperty
	private Klassenstufe klassenstufe;

	@JsonProperty
	private int anzahlTeilnehmer;

	@JsonProperty
	private String median;

	@JsonIgnore
	private List<GesamtpunktverteilungItem> gesamtpunktverteilungItems = new ArrayList<>();

	@JsonIgnore
	private List<AufgabeErgebnisItem> aufgabeErgebnisItems = new ArrayList<>();

	@JsonIgnore
	private List<RohpunktItem> rohpunktItems = new ArrayList<>();

	/**
	 * GesamtpunktverteilungDaten
	 */
	public GesamtpunktverteilungDaten() {
	}

	public void addItem(final GesamtpunktverteilungItem item) {
		if (!gesamtpunktverteilungItems.contains(item)) {
			gesamtpunktverteilungItems.add(item);
		}
	}

	public void addAufgabeErgebnis(final AufgabeErgebnisItem item) {
		if (!aufgabeErgebnisItems.contains(item)) {
			aufgabeErgebnisItems.add(item);
		}
	}

	public void addRohpunktItem(final RohpunktItem item) {
		if (!rohpunktItems.contains(item)) {
			rohpunktItems.add(item);
		}
	}

	public int getAnzahlTeilnehmer() {
		return anzahlTeilnehmer;
	}

	public void setAnzahlTeilnehmer(final int anzahlTeilnehmer) {
		this.anzahlTeilnehmer = anzahlTeilnehmer;
	}

	public String getMedian() {
		return median;
	}

	public void setMedian(final String median) {
		this.median = median;
	}

	@JsonProperty("gesamtpunktverteilungItems")
	public List<GesamtpunktverteilungItem> getGesamtpunktverteilungItemsSorted() {
		Collections.sort(gesamtpunktverteilungItems, new GesamtpunktverteilungItemDescendingSorter());
		return gesamtpunktverteilungItems;
	}

	@JsonProperty("aufgabeErgebnisItems")
	public List<AufgabeErgebnisItem> getAufgabeErgebnisItemsSorted() {
		Collections.sort(aufgabeErgebnisItems, new AufgabeErgebnisItemAscendingSorter());
		return aufgabeErgebnisItems;
	}

	@JsonProperty("rohpunktItems")
	public List<RohpunktItem> getRohpunktItemsSorted() {
		Collections.sort(rohpunktItems, new RohpunktItemDescendingSorter());
		return rohpunktItems;
	}

	public final Klassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	public final void setKlassenstufe(final Klassenstufe klassenstufe) {
		this.klassenstufe = klassenstufe;
	}

}
