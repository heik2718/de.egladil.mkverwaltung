//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import java.util.List;

import com.itextpdf.text.Paragraph;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.gruppen.LoesungszettelKlassenstufeProvider;
import de.egladil.mkv.auswertungen.statistik.impl.VerteilungGeneratorImpl;
import de.egladil.mkv.auswertungen.urkunden.UebersichtFontProvider;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * BaseSchuluebersichtPDFGenerator
 */
public abstract class BaseSchuluebersichtPDFGenerator {

	private final UebersichtFontProvider fontProvider;

	private final IVerteilungGenerator verteilungGenerator;

	private final AufgabenuebersichtPDFGenerator aufgabenuebersichtGenerator;

	private final String wettbewerbsjahr;

	/**
	 * BaseSchuluebersichtPDFGenerator
	 */
	public BaseSchuluebersichtPDFGenerator(final String wettbewerbsjahr) {
		this.fontProvider = new UebersichtFontProvider();
		this.verteilungGenerator = new VerteilungGeneratorImpl();
		this.aufgabenuebersichtGenerator = new AufgabenuebersichtPDFGenerator();
		this.wettbewerbsjahr = wettbewerbsjahr;
	}

	protected abstract byte[] generiereDeckblatt(final LoesungszettelKlassenstufeProvider rootgruppeuebersicht);



	/**
	 * @param klassenstufe Klassenstufe
	 * @param klassenname String
	 * @param loesungszettel List<ILoesungszettel>
	 * @return byte[]
	 */
	protected byte[] generiereAufgabenuebersicht(final GesamtpunktverteilungDaten daten, final String klassenname) {
		final byte[] aufgabenuebersicht = aufgabenuebersichtGenerator.generiere(daten, klassenname);
		return aufgabenuebersicht;
	}

	protected String getMedian(final LoesungszettelKlassenstufeProvider rootgruppeuebersicht, final Klassenstufe klassenstufe) {

		final List<ILoesungszettel> alleZettel = rootgruppeuebersicht.getLoesungszettel(klassenstufe);
		return getMedian(alleZettel, klassenstufe);
	}

	private String getMedian(final List<ILoesungszettel> alleZettel, final Klassenstufe klassenstufe) {
		if (!alleZettel.isEmpty()) {
			final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(alleZettel,
				klassenstufe, wettbewerbsjahr);

			return daten.getMedian();
		}
		return null;
	}

	protected final IVerteilungGenerator getVerteilungGenerator() {
		return verteilungGenerator;
	}

	protected abstract String getInhaltsangabe();

	protected final UebersichtFontProvider getFontProvider() {
		return fontProvider;
	}

	/**
	 * @param rootgruppeuebersicht
	 * @return Paragraph
	 */
	protected Paragraph getTitle(final LoesungszettelKlassenstufeProvider rootgruppeuebersicht) {
		return new Paragraph("Minikänguru-Wettbewerb " + wettbewerbsjahr + " - " + rootgruppeuebersicht.getSchulname(),
			getFontProvider().getFontBold(14));
	}

	/**
	 * @return Paragraph
	 */
	protected Paragraph getParagraphMediandefinition() {
		return new Paragraph(
			"Der Median ist die mittlere erreichte Punktzahl aller Kinder: die Hälfte aller teilnehmenden Kinder Ihrer Schule mit dieser Klassenstufe hat mindestens diese Punktzahl erreicht, die Hälfte höchstens.",
			getFontProvider().getFontNormal());
	}

	protected final String getWettbewerbsjahr() {
		return wettbewerbsjahr;
	}

	protected final AufgabenuebersichtPDFGenerator getAufgabenuebersichtGenerator() {
		return aufgabenuebersichtGenerator;
	}
}
