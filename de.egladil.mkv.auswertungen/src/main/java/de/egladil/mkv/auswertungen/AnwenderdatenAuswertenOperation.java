//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.eingaben.ILoesungszettelBerechnungsstrategie;
import de.egladil.mkv.auswertungen.eingaben.impl.UploadBerechnungsstrategieFactory;
import de.egladil.mkv.auswertungen.exceptions.UnprocessableAuswertungException;
import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.auswertungen.files.impl.AuswertungStreamReaderFactory;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * AnwenderdatenAuswertenOperation ist eine Fassadenoperation, die einen InputStream in ein VerarbeitetesWorkbook
 * umwandelt.
 */
public class AnwenderdatenAuswertenOperation {

	private final UploadBerechnungsstrategieFactory berechnungsstrategieFactory;

	private final int maxExtractedBytes;

	/**
	 * Erzeugt eine Instanz von AnwenderdatenAuswertenOperation
	 */
	public AnwenderdatenAuswertenOperation(final Map<Klassenstufe, String> loesungscodes, final int maxExtractedBytes) {
		this.berechnungsstrategieFactory = new UploadBerechnungsstrategieFactory(loesungscodes);
		this.maxExtractedBytes = maxExtractedBytes;
	}

	/**
	 * Verarbeitet den InputStream zu einem VerarbeitetesWorkbook-Objekt.
	 *
	 * @param uploadInfo UploadInfo darf nicht null sein.
	 * @return Optional von VerarbeitetesWorkbook. Falls empty, dann konnte der InputStream nicht ausgewertet werden.
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws UnprocessableAuswertungException
	 * @throws NullPointerException
	 */
	public Optional<VerarbeitetesWorkbook> verarbeiteDaten(final UploadInfo uploadInfo, final String pathUploadDir)
		throws UnprocessableAuswertungException, IOException, FileNotFoundException {
		if (uploadInfo == null) {
			throw new NullPointerException("uploadInfo");
		}
		final IAuswertungStreamReader auswertungStreamReader = AuswertungStreamReaderFactory.create(uploadInfo.getMimetype());
		final String pathFile = pathUploadDir + File.separator + uploadInfo.getDateiname();

		try (InputStream in = getDaten(pathFile)) {
			final MKAuswertungWorkbook workbook = auswertungStreamReader.readAuswertung(in, maxExtractedBytes);
			if (workbook.isEmpty()) {
				return Optional.of(new VerarbeitetesWorkbook());
			}
			final Optional<ILoesungszettelBerechnungsstrategie> optStrategie = berechnungsstrategieFactory
				.erzeugeStrategie(workbook);
			if (optStrategie.isPresent()) {
				final ILoesungszettelBerechnungsstrategie strategie = optStrategie.get();
				final VerarbeitetesWorkbook result = new VerarbeitetesWorkbook();
				for (final MKAuswertungRow row : workbook.getRows()) {
					if (result.getKlasse() == null) {
						result.setKlasse(row.getKlasse());
					}
					final LoesungszettelRohdaten rohdaten = strategie.berechneRohdaten(row);
					result.addEntry(rohdaten);
				}
				return Optional.of(result);
			}
		}
		return Optional.empty();
	}

	private InputStream getDaten(final String pathFile) throws FileNotFoundException {
		final File file = new File(pathFile);
		return new FileInputStream(file);
	}
}
