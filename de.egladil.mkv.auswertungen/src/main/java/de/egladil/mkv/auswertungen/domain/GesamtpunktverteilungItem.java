//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * GesamtpunktverteilungItem
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GesamtpunktverteilungItem {

	@XmlTransient
	@JsonIgnore
	private Punktintervall punktintervall;

	@XmlTransient
	@JsonIgnore
	private int anzahl;

	@XmlTransient
	@JsonIgnore
	private double prozentrang;

	@XmlElement(name = "intervall")
	@JsonProperty("intervall")
	private String punktintervallText;

	@XmlElement(name = "anzahlImIntervall")
	@JsonProperty("anzahlImIntervall")
	private String anzahlText;

	@XmlElement(name = "intervallPR")
	@JsonProperty("intervallPR")
	private String prozentrangText;

	public Punktintervall getPunktintervall() {
		return punktintervall;
	}

	public void setPunktintervall(final Punktintervall punktintervall) {
		this.punktintervall = punktintervall;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(final int anzahl) {
		this.anzahl = anzahl;
		this.anzahlText = "" + anzahl;
	}

	public double getProzentrang() {
		return prozentrang;
	}

	public void setProzentrang(final double prozentrang) {
		this.prozentrang = prozentrang;
	}

	public String getPunktintervallText() {
		return punktintervallText;
	}

	public void setPunktintervallText(final String punktintervallText) {
		this.punktintervallText = punktintervallText;
	}

	public String getAnzahlText() {
		return anzahlText;
	}

	public String getProzentrangText() {
		return prozentrangText;
	}

	public void setProzentrangText(final String prozentrangText) {
		this.prozentrangText = prozentrangText;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((punktintervall == null) ? 0 : punktintervall.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final GesamtpunktverteilungItem other = (GesamtpunktverteilungItem) obj;
		if (punktintervall == null) {
			if (other.punktintervall != null)
				return false;
		} else if (!punktintervall.equals(other.punktintervall))
			return false;
		return true;
	}
}
