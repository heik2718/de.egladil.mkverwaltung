//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * Schuluebersicht
 */
public class Schuluebersicht implements LoesungszettelKlassenstufeProvider {

	private final String schulname;

	private Map<Klassenstufe, List<Loesungszettel>> klassenstufenMap = new HashMap<>();

	/**
	 * Schuluebersicht
	 */
	public Schuluebersicht(final String schulname) {
		this.schulname = schulname;
	}

	public void putLoesungszettel(final Loesungszettel loesungszettel) {

		if (loesungszettel == null) {
			throw new IllegalArgumentException("loesungszettel null");
		}
		final Klassenstufe klassenstufe = loesungszettel.getLoesungszettelRohdaten().getKlassenstufe();

		List<Loesungszettel> klassenstufeZettel = klassenstufenMap.get(klassenstufe);
		if (klassenstufeZettel == null) {
			klassenstufeZettel = new ArrayList<>();
		}
		klassenstufeZettel.add(loesungszettel);
		klassenstufenMap.put(klassenstufe, klassenstufeZettel);
	}

	public List<Loesungszettel> getKlassenstufenLoesungszettel(final Klassenstufe klassenstufe) {

		if (klassenstufe == null) {
			throw new IllegalArgumentException("klassenstufe null");
		}

		return klassenstufenMap.get(klassenstufe);
	}

	@Override
	public int anzahlKinder() {
		int result = 0;
		for (final List<Loesungszettel> loesungszettel : klassenstufenMap.values()) {
			result += loesungszettel.size();
		}
		return result;
	}

	@Override
	public final String getSchulname() {
		return schulname;
	}

	@Override
	public List<ILoesungszettel> getLoesungszettel(final Klassenstufe klassenstufe) {
		final List<ILoesungszettel> result = new ArrayList<>();
		final List<Loesungszettel> loesungszettel = klassenstufenMap.get(klassenstufe);
		if (loesungszettel != null) {
			result.addAll(loesungszettel);
		}
		return result;
	}

	public List<Klassenstufe> getKeysSorted() {
		final List<Klassenstufe> result = new ArrayList<>();
		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			if (klassenstufenMap.get(klassenstufe) != null) {
				result.add(klassenstufe);
			}
		}
		return result;
	}
}
