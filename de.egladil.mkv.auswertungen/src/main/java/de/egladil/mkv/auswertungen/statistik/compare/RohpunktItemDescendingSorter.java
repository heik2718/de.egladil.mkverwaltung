//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.domain.RohpunktItem;

/**
 * RohpunktItemDescendingSorter sortiert die RohpunktItem absteigend nach Punkten.
 */
public class RohpunktItemDescendingSorter implements Comparator<RohpunktItem> {

	@Override
	public int compare(RohpunktItem arg0, RohpunktItem arg1) {
		return arg1.getPunkte() - arg0.getPunkte();
	}
}
