//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.auswertungen.statistik.gruppen.Rootgruppeuebersicht;
import de.egladil.mkv.auswertungen.statistik.gruppen.RootgruppeuebersichtCreator;
import de.egladil.mkv.auswertungen.statistik.gruppen.Schuluebersicht;
import de.egladil.mkv.auswertungen.statistik.gruppen.SchuluebersichtCreator;
import de.egladil.mkv.auswertungen.statistik.print.SchuluebersichtAktuellesJahrPDFGenerator;
import de.egladil.mkv.auswertungen.statistik.print.SchuluebersichtPDFGenerator;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteBuilder;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.IAuswertungDownloadDao;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * SchulstatistikServiceImpl stellt Funktionalität im Zusammenhang mit Schulstatistiken zur Verfügung.
 */
@Singleton
public class SchulstatistikServiceImpl implements SchulstatistikService {

	private static final Logger LOG = LoggerFactory.getLogger(SchulstatistikServiceImpl.class);

	private final AuswertungsgruppenService embeddedAuswertungsruppenService;

	private final TeilnehmerFacade teilnehmerFacade;

	private final TeilnahmenFacade teilnahmenFacade;

	private final IAuswertungDownloadDao auswertungDownloadDao;

	private final IVerteilungGenerator verteilungGenerator;

	/**
	 * SchulstatistikServiceImpl
	 */
	@Inject
	public SchulstatistikServiceImpl(final AuswertungsgruppenService embeddedAuswertungsruppenService,
		final TeilnehmerFacade teilnehmerFacade, final TeilnahmenFacade teilnahmenFacade,
		final IAuswertungDownloadDao auswertungDownloadDao, final IVerteilungGenerator verteilungGenerator) {
		this.embeddedAuswertungsruppenService = embeddedAuswertungsruppenService;
		this.teilnehmerFacade = teilnehmerFacade;
		this.teilnahmenFacade = teilnahmenFacade;
		this.auswertungDownloadDao = auswertungDownloadDao;
		this.verteilungGenerator = verteilungGenerator;
	}

	@Override
	@Transactional
	public Optional<String> generiereStatistikAktuellesJahr(final String benutzerUuid, final String kuerzelRootgruppe)
		throws ResourceNotFoundException {

		final Optional<Auswertungsgruppe> optAuswertungsgruppe = embeddedAuswertungsruppenService
			.findAuswertungsgruppe(kuerzelRootgruppe);
		if (!optAuswertungsgruppe.isPresent()) {
			throw new ResourceNotFoundException("keine Auswertungsgruppe mit kuerzel " + kuerzelRootgruppe + " vorhanden");
		}
		final AuswertungDownload auswertungDownload = auswertungGenerieren(benutzerUuid, optAuswertungsgruppe.get());

		final AuswertungDownload persisted = internalPersist(benutzerUuid, auswertungDownload);
		return Optional.of(persisted.getDownloadCode());
	}

	private AuswertungDownload auswertungGenerieren(final String benutzerUuid, final Auswertungsgruppe auswertungsgruppe) {

		LOG.info("Schulstatistik aktuelles Jahr für Schule {} (kuerzel={} generiert)", auswertungsgruppe.getName(),
			auswertungsgruppe.getKuerzel());

		final List<Teilnehmer> teilnehmer = teilnehmerFacade.getTeilnehmer(auswertungsgruppe);

		final List<TeilnehmerUndPunkte> teilnehmerUndPunkte = new ArrayList<>();
		for (final Teilnehmer t : teilnehmer) {
			final Optional<Loesungszettel> optLoes = teilnehmerFacade.getLoesungszettel(t);
			if (optLoes.isPresent()) {
				final TeilnehmerUndPunkte tup = new TeilnehmerUndPunkteBuilder(t, optLoes.get().getLoesungszettelRohdaten())
					.build();
				teilnehmerUndPunkte.add(tup);
			}
		}
		final Rootgruppeuebersicht rootgruppeuebersicht = new RootgruppeuebersichtCreator()
			.createSchuluebersicht(teilnehmerUndPunkte, auswertungsgruppe.getName());
		final byte[] pdf = new SchuluebersichtAktuellesJahrPDFGenerator().generiere(rootgruppeuebersicht,
			auswertungsgruppe.getName());

		final AuswertungDownload auswertungDownload = AuswertungDownload
			.forTeilnahmeIdentifier(auswertungsgruppe.provideTeilnahmeIdentifier());
		auswertungDownload.setDaten(pdf);
		return auswertungDownload;
	}

	@Override
	public Optional<AuswertungDownload> findDownload(final String downloadcode) {
		return auswertungDownloadDao.findByUniqueKey(AuswertungDownload.class, AuswertungDownload.UNIQUE_ATTRIBUTE_NAME,
			downloadcode);
	}

	@Override
	public void deleteDownloadQietly(final String benutzerUuid, final AuswertungDownload download) {
		if (download == null) {
			return;
		}
		try {
			auswertungDownloadDao.delete(download);
			LOG.debug("Auswertungsdownload {} gelöscht", download.getDownloadCode());
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DATENMUELL + "Fehler beim Löschen des AuswertungDownloads {}: {}",
				download.getDownloadCode(), e.getMessage(), e);
		}
	}

	@Override
	@Transactional
	public Optional<String> generiereSchulstatistik(final String benutzerUuid, final TeilnahmeIdentifier teilnahmeIdentifier,
		final String schulname) {

		LOG.info("Schulstatistik für Teilnahme {} generiert", teilnahmeIdentifier);

		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}
		if (teilnahmeIdentifier == null) {
			throw new IllegalArgumentException("teilnahmeIdentifier null");
		}

		final List<Loesungszettel> alleLoesungszettel = teilnehmerFacade.getLoesungszettel(teilnahmeIdentifier);

		final Schuluebersicht schuluebersicht = new SchuluebersichtCreator().createSchuluebersicht(alleLoesungszettel, schulname);

		if (schuluebersicht.anzahlKinder() == 0) {
			throw new PreconditionFailedException("Es gibt keine Daten für die gegebene Teilnahme");
		}
		final AuswertungDownload auswertungDownload = statistikGenerieren(teilnahmeIdentifier, schuluebersicht);

		final AuswertungDownload persisted = internalPersist(benutzerUuid, auswertungDownload);
		return Optional.of(persisted.getDownloadCode());
	}

	private AuswertungDownload statistikGenerieren(final TeilnahmeIdentifier teilnahmeIdentifier,
		final Schuluebersicht schuluebersicht) {

		final String wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
		final String auswertungsjahr = teilnahmeIdentifier.getJahr();
		final boolean mitGesamtmedian = !wettbewerbsjahr.equals(auswertungsjahr);
		final byte[] pdf = new SchuluebersichtPDFGenerator(auswertungsjahr, this.teilnahmenFacade, this.verteilungGenerator,
			mitGesamtmedian).generiere(schuluebersicht);

		final AuswertungDownload auswertungDownload = AuswertungDownload.forTeilnahmeIdentifier(teilnahmeIdentifier);
		auswertungDownload.setDaten(pdf);
		return auswertungDownload;
	}

	private AuswertungDownload internalPersist(final String benutzerUuid, final AuswertungDownload auswertungDownload) {
		auswertungDownload.setGeaendertDurch(benutzerUuid);
		auswertungDownload.setDownloadCode(new KuerzelGenerator().generateDefaultKuerzelWithTimestamp());
		final AuswertungDownload persisted = auswertungDownloadDao.persist(auswertungDownload);
		return persisted;
	}

}
