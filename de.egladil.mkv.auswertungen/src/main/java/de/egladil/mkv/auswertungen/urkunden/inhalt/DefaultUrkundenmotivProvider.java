//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.itextpdf.text.BaseColor;

import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.auswertungen.IndividuellesUrkundenmotiv;
import de.egladil.mkv.persistence.domain.auswertungen.Ueberschriftfarbe;
import de.egladil.mkv.persistence.domain.auswertungen.VordefiniertesUrkundenmotiv;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * DefaultUrkundenmotivProvider
 */
public class DefaultUrkundenmotivProvider implements UrkundenmotivProvider {

	private final byte[] data;

	private BaseColor baseColor;

	/**
	 * Erzeugt einen ByteArryBackgroundProvider aus einem individuellen Urkundenmotiv.
	 *
	 * @param individuellesUrkundenmotiv
	 * @return DefaultUrkundenmotivProvider
	 */
	public static DefaultUrkundenmotivProvider ausIndividuellemUrkundenmotiv(
		final IndividuellesUrkundenmotiv individuellesUrkundenmotiv) {
		return new DefaultUrkundenmotivProvider(individuellesUrkundenmotiv);
	}

	/**
	 * Erzeugt einen ByteArryBackgroundProvider aus einem durch das farbschema vordefinierten Urkundenmotiv.
	 *
	 * @param individuellesUrkundenmotiv
	 * @return DefaultUrkundenmotivProvider
	 */
	public static DefaultUrkundenmotivProvider ausFarbschema(final Farbschema farbschema) {
		final VordefiniertesUrkundenmotiv urkundenmotiv = VordefiniertesUrkundenmotiv.fromFarbschema(farbschema);
		final String classpathResource = urkundenmotiv.getBackgroundClasspathResource();
		try (InputStream in = DefaultUrkundenmotivProvider.class.getResourceAsStream(classpathResource)) {
			final byte[] data = IOUtils.toByteArray(in);
			return new DefaultUrkundenmotivProvider(data, urkundenmotiv.getUeberschriftfarbe());
		} catch (final IOException e) {
			throw new EgladilConfigurationException(
				"Kann classpathResource zu color " + urkundenmotiv.toString() + " nicht lesen: " + classpathResource);
		}
	}

	private DefaultUrkundenmotivProvider(final byte[] data, final Ueberschriftfarbe ueberscriftfarbe) {
		this.data = data;
		this.initBaseColor(ueberscriftfarbe);
	}

	private DefaultUrkundenmotivProvider(final IndividuellesUrkundenmotiv individuellesUrkundenmotiv) {
		this.data = individuellesUrkundenmotiv.getOverlay();
		this.initBaseColor(individuellesUrkundenmotiv.getUberschiftfarbe());

	}

	private void initBaseColor(final Ueberschriftfarbe ueberschriftfarbe) {
		baseColor = new BaseColor(ueberschriftfarbe.getRgbRed(), ueberschriftfarbe.getRgbGreen(), ueberschriftfarbe.getRgbBlue());
	}

	@Override
	public InputStream getBackgroundImage() {
		return new ByteArrayInputStream(data);
	}

	@Override
	public BaseColor getHeadlineColor() {
		return baseColor;
	}
}
