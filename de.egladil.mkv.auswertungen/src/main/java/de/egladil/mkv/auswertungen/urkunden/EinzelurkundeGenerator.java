//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * EinzelurkundeGenerator
 */
public interface EinzelurkundeGenerator {

	/**
	 * Generiert eine einzelne Urkunde.
	 *
	 * @param urkundeInhalt DefaultUrkundeInhalt darf nicht null sein.
	 * @return byte[] das PDF
	 * @throws MKVException bei Exceptions aus iTextPdf bzw. IllegalArgumentException
	 */
	byte[] generate(final UrkundeInhalt urkundeInhalt) throws MKVException;
}
