//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.exceptions.UnprocessableAuswertungException;
import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;
import de.egladil.tools.parser.OpenOfficeParser;
import de.egladil.tools.parser.OpenOfficeTableElement;
import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * AuswertungOpenOfficeStreamReader
 */
public class AuswertungOpenOfficeStreamReader implements IAuswertungStreamReader {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungOpenOfficeStreamReader.class);

	private SearchingHaeadlineState searchingHaeadlineState = new SearchingHaeadlineState();

	private IOpenOfficeParsingState actualState = searchingHaeadlineState;

	@Override
	public MKAuswertungWorkbook readAuswertung(final InputStream in, final int maxLengthExtracted) throws IOException,
		IllegalArgumentException, MKAuswertungenException, UnprocessableAuswertungException, ParserSecurityException {
		if (in == null) {
			throw new IllegalArgumentException("in null");
		}
		final MKAuswertungWorkbook workbook = new MKAuswertungWorkbook();

		try {
			final List<OpenOfficeTableElement> tableRows = parseStream(in, maxLengthExtracted);

			int index = 0;
			for (final OpenOfficeTableElement tableRow : tableRows) {
				actualState.handle(tableRow);

				if (actualState.isFinished()) {
					actualState = new ExtractingDataState(searchingHaeadlineState, index++);
				}
			}

			if (!searchingHaeadlineState.isEmpty()) {
				workbook.addAll(searchingHaeadlineState.getZeilen());
			}
			LOG.debug("MKAuswertungWorkbook mit {} Zeile(n) erzeugt", workbook.size());
			return workbook;
		} catch (ParserConfigurationException | SAXException | XMLStreamException e) {
			throw new MKAuswertungenException("Fehler beim Parsen des contents: " + e.getMessage(), e);

		}

	}

	private List<OpenOfficeTableElement> parseStream(final InputStream in, final int maxLengthExtracted)
		throws ParserSecurityException, IOException, ParserConfigurationException, SAXException, XMLStreamException {

		final OpenOfficeParser openOfficeParser = new OpenOfficeParser();

		final String content = openOfficeParser.getContentSafe(in, maxLengthExtracted);
		return openOfficeParser.parseContent(content);
	}
}
