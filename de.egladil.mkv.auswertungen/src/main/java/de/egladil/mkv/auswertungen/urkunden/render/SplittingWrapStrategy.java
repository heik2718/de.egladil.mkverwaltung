//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Splitter;

import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.MergeWordsInvoker;

/**
 * SplittingWrapStrategy
 */
public class SplittingWrapStrategy implements LineWrapStrategy {

	private static final Splitter SPLITTER = Splitter.on(" ").trimResults();

	private final Integer[] availableFontSizes;

	private final LengthTester lengthTester;

	private final MergeWordsInvoker invoker;

	private final List<String> resultierendeZeilen;

	/**
	 * SplittingWrapStrategy
	 */
	public SplittingWrapStrategy(final Integer[] availableFontSizes) {
		this.availableFontSizes = availableFontSizes;
		resultierendeZeilen = new ArrayList<>();
		lengthTester = new LengthTester();
		invoker = new MergeWordsInvoker();
	}

	@Override
	public List<String> breakLines(final String name) {
		final List<String> worte = SPLITTER.splitToList(name);
		final List<String> volleZeile = sammleWorteBisMaximaleBreite(worte);
		final List<String> rest = ermittleRest(worte, volleZeile);

		final String mergedRest = invoker.mergeToLine(rest);
		if (lengthTester.needsWrapping(mergedRest, availableFontSizes)) {
			breakLines(mergedRest);
		} else {
			resultierendeZeilen.add(mergedRest);
		}
		return resultierendeZeilen;
	}

	private List<String> sammleWorteBisMaximaleBreite(final List<String> worte) {
		final List<String> ersteZeile = new ArrayList<>();
		String zeile = "";
		for (int i = 0; i < worte.size(); i++) {
			final String wort = worte.get(i);
			ersteZeile.add(wort.trim());
			zeile = invoker.mergeToLine(ersteZeile);
			if (lengthTester.needsWrapping(zeile.trim(), availableFontSizes)) {
				ersteZeile.remove(i);
				break;
			}
		}
		resultierendeZeilen.add(invoker.mergeToLine(ersteZeile));
		return ersteZeile;
	}

	private List<String> ermittleRest(final List<String> worte, final List<String> verarbeiteteWorte) {
		final List<String> zweiteZeile = new ArrayList<>();
		if (verarbeiteteWorte.size() < worte.size()) {
			for (int i = verarbeiteteWorte.size(); i < worte.size(); i++) {
				zweiteZeile.add(worte.get(i));
			}
		}
		return zweiteZeile;
	}
}
