//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

/**
 * WorkbookKategorie kann eine Eingabe von Antwortbuchstaben (A-N), Wertungsbuchstaben (f,r,n) oder unbekannt sein, da
 * Eingabefehler dazu führen können, dass man nicht exakt zwischen Antwortbuchtsaben und Werrtungsbuchstaben
 * unterscheiden kann.
 */
public enum WorkbookKategorie {

	ANTWORTBUCHSTABEN,
	WERTUNGSBUCHSTABEN,
	NICHT_ENTSCHEIDBAR
}
