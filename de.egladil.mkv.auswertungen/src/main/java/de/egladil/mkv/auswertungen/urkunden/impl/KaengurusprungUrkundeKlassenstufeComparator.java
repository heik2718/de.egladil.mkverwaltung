//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;

/**
 * TeilnehmerUrkundeKlassennameComparator sortiert nach Klassenstufe, innerhalb der gleichen Klassenstufe absteigend
 * nach Länge des Kängurusprungs und alphabetisch nach Namen.
 */
public class KaengurusprungUrkundeKlassenstufeComparator implements Comparator<UrkundeInhalt> {

	@Override
	public int compare(final UrkundeInhalt arg0, final UrkundeInhalt arg1) {
		if (arg0.getKlassenstufe() != arg1.getKlassenstufe()) {
			return arg0.getKlassenstufe().getNummer() - arg1.getKlassenstufe().getNummer();
		}
		if (arg0.getKaengurusprung() != arg1.getKaengurusprung()) {
			return arg1.getKaengurusprung() - arg0.getKaengurusprung();
		}
		return new NameKlasseZusatzComparator().compare(arg0, arg1);
	}
}
