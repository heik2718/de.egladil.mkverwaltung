//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;

/**
 * AuswertungStreamReaderFactory
 */
public abstract class AuswertungStreamReaderFactory {

	/**
	 * Gibt den korrekten AuswertungStreamReader zurück.
	 *
	 * @param uploadMimeType UploadMimeType darf nicht null sein.
	 * @return
	 */
	public static IAuswertungStreamReader create(final UploadMimeType uploadMimeType) throws IllegalArgumentException, IOException {
		if (uploadMimeType == null) {
			throw new IllegalArgumentException("uploadMimeType null");
		}
		switch (uploadMimeType) {
		case VDN_MSEXCEL:
			return createForOLE2();
		case VDN_OPEN_XML:
			return createForOfficeXML();
		case ODS:
			return new AuswertungOpenOfficeStreamReader();
		default:
			break;
		}

		throw new IllegalArgumentException("uploadMimeType " + uploadMimeType + " kann nicht verarbeitet werden");
	}

	private static IAuswertungStreamReader createForOfficeXML() {
		return new AbstractAuswertungExcelStreamReader() {
			@Override
			protected Workbook getWorkbook(final InputStream in) throws IllegalArgumentException, IOException {
				return new XSSFWorkbook(in);
			}
		};
	}

	private static IAuswertungStreamReader createForOLE2() {
		return new AbstractAuswertungExcelStreamReader() {
			@Override
			protected Workbook getWorkbook(final InputStream in) throws IllegalArgumentException, IOException {
				return new HSSFWorkbook(in);
			}
		};
	}
}
