//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.MKAuswertungCell;
import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKCellAddress;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.tools.parser.OpenOfficeTableElement;

/**
 * ExtractingDataState
 */
public class ExtractingDataState implements IOpenOfficeParsingState {

	private static final Logger LOG = LoggerFactory.getLogger(ExtractingDataState.class);

	private final Klassenstufe klassenstufe;

	private final int anzahlNamenspalten;

	private final int index;

	private final int maxCol;

	private final SearchingHaeadlineState collectingDataState;

	public ExtractingDataState(final SearchingHaeadlineState searchingHaeadlineState, final int i) {
		if (searchingHaeadlineState == null) {
			throw new NullPointerException("Parameter searchingHaeadlineState");
		}
		this.collectingDataState = searchingHaeadlineState;
		this.klassenstufe = searchingHaeadlineState.getKlasse();
		this.anzahlNamenspalten = searchingHaeadlineState.getAnzahlNamenspalten();
		this.index = i;

		switch (klassenstufe) {
		case IKID:
			maxCol = anzahlNamenspalten + 12;
			break;
		case EINS:
			maxCol = anzahlNamenspalten + 24;
			break;
		case ZWEI:
			maxCol = anzahlNamenspalten + 30;
			break;
		default:
			throw new MKVException("noch nicht eingebaute Klassenstufe " + klassenstufe + " gefunden");
		}
	}

	@Override
	public void handle(final OpenOfficeTableElement tableCells) {
		final MKAuswertungRow row = generateRow(tableCells, index);
		if (row != null && !row.isEmpty()) {
			LOG.debug(row.toNutzereingabe());
			collectingDataState.addMKAuswertungRow(row);
		}
	}

	private MKAuswertungRow generateRow(final OpenOfficeTableElement tableRow, final int index) throws MKAuswertungenException {
		try {
			final MKAuswertungRow result = new MKAuswertungRow(index, klassenstufe, anzahlNamenspalten);
			if (tableRow.size() == 0 || tableRow.size() < maxCol) {
				return result;
			}
			// if (index == 9) {
			// LOG.debug("Index = {}", index);
			// }
			processNamenspalten(tableRow, index, result);
			processDatenspalten(tableRow, result);
			return result;
		} catch (final IndexOutOfBoundsException e) {
			throw new MKAuswertungenException(
				"IndexOutOfBoundsException bei TableRow " + tableRow.toString() + ", index " + index + ": " + e.getMessage(), e);
		}
	}

	/**
	 * Liest alle potentiellen Namenspalten.
	 *
	 * @param tableRow
	 * @param index
	 * @param result
	 */
	private void processNamenspalten(final OpenOfficeTableElement tableRow, final int index, final MKAuswertungRow result) {
		for (int i = 0; i < anzahlNamenspalten; i++) {
			processNamenspalte(i, tableRow, index, result);
		}
	}

	/**
	 * Liest die Namenspalte mit Index spaltenindex aus.
	 *
	 * @param spaltenindex
	 * @param tableRow
	 * @param index
	 * @param resultingRow
	 */
	private void processNamenspalte(final int spaltenindex, final OpenOfficeTableElement tableRow, final int index,
		final MKAuswertungRow resultingRow) {
		if (tableRow.size() >= spaltenindex + 1) {
			createAndAddCell(spaltenindex, tableRow, index, resultingRow);
		}
	}

	/**
	 * @param spaltenindex
	 * @param tableRow
	 * @param index
	 * @param resultingRow
	 */
	private void createAndAddCell(final int spaltenindex, final OpenOfficeTableElement tableRow, final int index,
		final MKAuswertungRow resultingRow) {
		final OpenOfficeTableElement cell = tableRow.get(index);
		if (cell != null) {
			resultingRow.addCell(spaltenindex, createMKCell(index, spaltenindex, cell.getContent()));
		}
	}

	/**
	 * Liest alle Datenspalten nach den Namenspalten aus.
	 *
	 * @param tableRow
	 * @param resultingRow
	 */
	private void processDatenspalten(final OpenOfficeTableElement tableRow, final MKAuswertungRow resultingRow) {
		int cellIndex = anzahlNamenspalten;
		for (int spaltenindex = anzahlNamenspalten; spaltenindex < maxCol; spaltenindex++) {

			final OpenOfficeTableElement cell = tableRow.get(spaltenindex);
			if (cell != null && !cell.isFormula()) {
				resultingRow.addCell(cellIndex, createMKCell(cellIndex, spaltenindex, cell.getContent()));
				cellIndex++;
			}
		}
	}

	/**
	 * Erzeugt eine neue MKCell.
	 *
	 * @param row
	 * @param col
	 * @param cellValue
	 * @return
	 */
	private MKAuswertungCell createMKCell(final int row, final int col, final String cellValue) {
		return new MKAuswertungCell(new MKCellAddress(row, col), cellValue);
	}

	/**
	 * @see de.egladil.mkv.auswertungen.files.impl.IOpenOfficeParsingState#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return true;
	}
}
