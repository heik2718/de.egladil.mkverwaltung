//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.List;

import de.egladil.mkv.auswertungen.domain.Punktintervall;

/**
 * IPunktintervallFactory
 */
public interface IPunktintervallFactory {

	/**
	 * Gibt alle Punktintervalle eines Minikänguruwettbewerbs in absteigender Reihenfolge sortiert zurück;
	 *
	 * @return List von Punktintervall-Instanzen.
	 */
	List<Punktintervall> getPunktintervalleDescending();
}
