//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.auswertungen.eingaben.ILoesungszettelBerechnungsstrategie;
import de.egladil.mkv.auswertungen.eingaben.LoesungszettelRohdatenProvider;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * AbstractAuswertungBerechnungsstategie
 */
public abstract class AbstractAuswertungBerechnungsstategie implements ILoesungszettelBerechnungsstrategie {

	@Override
	public final LoesungszettelRohdaten berechneRohdaten(final MKAuswertungRow auswertungRow) {
		if (auswertungRow == null) {
			throw new NullPointerException("Parameter auswertungRow");
		}
		final Klassenstufe klassenstufe = auswertungRow.getKlasse();
		final String nutzereingabe = auswertungRow.toNutzereingabe();
		final String korrigierteBenutzereingabe = getKorrekturstrategie().korrigiere(nutzereingabe);
		final boolean typo = !nutzereingabe.equalsIgnoreCase(korrigierteBenutzereingabe);

		final String berechneterWertungscode = berechneWertungscode(korrigierteBenutzereingabe, auswertungRow.getKlasse());

		final LoesungszettelRohdatenProvider rohdatenProvider = new LoesungszettelRohdatenProvider(klassenstufe,
			berechneterWertungscode, nutzereingabe, typo, Auswertungsquelle.UPLOAD);
		final LoesungszettelRohdaten loesungszettel = rohdatenProvider.createLoesungszettelRohdaten();

		return loesungszettel;
	}

	/**
	 * Gibt die spezielle Korrekturstrategie zurück, die verwendet wird.
	 *
	 * @return
	 */
	protected abstract INutzereingabeKorrekturstrategie getKorrekturstrategie();

	/**
	 * Berechnet den Wertungscode, also den nicht kommaseparierten String aus f,r und n.
	 *
	 * @param korrigierteBenutzereingabe String mit Kommas
	 * @param klassenstufe Klassenstufe darf nicht null sein.
	 * @return String aus f,r und n ohne Kommas
	 */
	protected abstract String berechneWertungscode(String korrigierteBenutzereingabe, Klassenstufe klassenstufe);

	/**
	 *
	 * @see de.egladil.mkv.auswertungen.eingaben.ILoesungszettelBerechnungsstrategie#getCorrespondingWorkbookKategorie()
	 */
	@Override
	public abstract WorkbookKategorie getCorrespondingWorkbookKategorie();
}
