//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.DefaultUrkundeInhaltBuilder;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteBuilder;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitText;
import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundenmotivMitTextBuilder;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * AuswertungDatenGenerator
 */
public class AuswertungDatenGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungDatenGenerator.class);

	// private static final String DATUM_FORMAT = "dd.MM.yyyy";

	private final ILoesungszettelDao loesungszettelDao;

	private final TeilnehmerFacade teilnehmerFacade;

	/**
	 * AuswertungDatenGenerator
	 */
	public AuswertungDatenGenerator(final ILoesungszettelDao loesungszettelDao, final TeilnehmerFacade teilnehmerFacade) {
		this.loesungszettelDao = loesungszettelDao;
		this.teilnehmerFacade = teilnehmerFacade;
	}

	/**
	 * Erzeugt und gruppiert Teilnehmerurkunde-Objekte.
	 *
	 * @param alleTeilnehmerkuerzel String[]
	 * @param urkundenMotivProvider UrkundenmotivProvider
	 * @param datum String darf nicht blank sein.
	 * @return AuswertungDaten
	 */
	public AuswertungDaten generateComplete(final String[] alleTeilnehmerkuerzel, final UrkundenmotivProvider urkundenMotivProvider,
		final String datum) {
		// final String datum = new SimpleDateFormat(DATUM_FORMAT).format(new Date());
		final AuswertungDaten result = new AuswertungDaten();
		final List<DefaultUrkundeInhalt> alleTeilnehmerurkunden = createTeilnehmerurkunden(alleTeilnehmerkuerzel,
			urkundenMotivProvider, datum);

		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {

			final List<UrkundeInhalt> teilnehmerurkunden = alleTeilnehmerurkunden.stream()
				.filter(u -> u.getKlassenstufe() == klassenstufe).collect(Collectors.toList());
			result.addAllTeilnehmerurkunde(klassenstufe, teilnehmerurkunden);

			final List<UrkundeInhalt> kaengurus = getGewinnerKaengurusprung(alleTeilnehmerurkunden, klassenstufe);
			if (kaengurus != null) {
				result.addAnzahlKaenurugewinner(klassenstufe, kaengurus.size());
				if (kaengurus.size() == 1) {
					result.addKaenguruUrkunde(klassenstufe, kaengurus.get(0));
				}
			}
		}
		return result;
	}

	/**
	 * Erzeugt und gruppiert Teilnehmerurkunde-Objekte für die Kängurusprunggewinner.
	 *
	 * @param alleTeilnehmerKuerzel String[]
	 * @param urkundenMotivProvider UrkundenmotivProvider
	 * @param datum String darf nicht blank sein.
	 * @return AuswertungDaten
	 */
	public AuswertungDaten generateJumpCerts(final String[] alleTeilnehmerKuerzel,
		final UrkundenmotivProvider urkundenMotivProvider, final String datum) {
		// final String datum = new SimpleDateFormat(DATUM_FORMAT).format(new Date());
		final AuswertungDaten result = new AuswertungDaten();

		final List<DefaultUrkundeInhalt> alleTeilnehmerurkunden = createTeilnehmerurkunden(alleTeilnehmerKuerzel,
			urkundenMotivProvider, datum);
		for (final Klassenstufe klassenstufe : Klassenstufe.valuesSorted()) {
			final List<UrkundeInhalt> kaengurus = getGewinnerKaengurusprung(alleTeilnehmerurkunden, klassenstufe);
			if (kaengurus != null) {
				result.addAllKaenguruUrkunde(klassenstufe, kaengurus);
			}
		}
		return result;
	}

	private List<DefaultUrkundeInhalt> createTeilnehmerurkunden(final String[] alleTeilnehmerKuerzel,
		final UrkundenmotivProvider urkundenMotivProvider, final String datum) {
		// final String datum = new SimpleDateFormat(DATUM_FORMAT).format(new Date());
		final List<DefaultUrkundeInhalt> alleTeilnehmerurkunden = new ArrayList<>();
		for (final String kuerzel : alleTeilnehmerKuerzel) {
			final Optional<Teilnehmer> optTeilnehmer = this.teilnehmerFacade.getTeilnehmer(kuerzel);
			if (optTeilnehmer.isPresent()) {
				final Teilnehmer teilnehmer = optTeilnehmer.get();
				final Optional<Loesungszettel> optLoes = loesungszettelDao.findByUniqueKey(Loesungszettel.class,
					MKVConstants.KUERZEL_ATTRIBUTE_NAME, teilnehmer.getLoesungszettelkuerzel());
				if (optLoes.isPresent()) {
					final LoesungszettelRohdaten rohdaten = optLoes.get().getLoesungszettelRohdaten();
					final DefaultUrkundeInhalt defaultUrkundeInhalt = createTeilnehmerUrkundeInhalt(urkundenMotivProvider, datum,
						teilnehmer, rohdaten);

					alleTeilnehmerurkunden.add(defaultUrkundeInhalt);

				} else {
					LOG.warn("Lösungszettel mit KUERZEL {} zu Teilnehmer mit KUERZEL {} existiert nicht",
						teilnehmer.getLoesungszettelkuerzel(), kuerzel);
				}
			} else {
				LOG.warn("Teilnehmer mit KUERZEL {} existiert nicht", kuerzel);
			}
		}
		return alleTeilnehmerurkunden;
	}

	private DefaultUrkundeInhalt createTeilnehmerUrkundeInhalt(final UrkundenmotivProvider urkundenMotivProvider,
		final String datum, final Teilnehmer teilnehmer, final LoesungszettelRohdaten rohdaten) {
		final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkteBuilder(teilnehmer, rohdaten)
			.zusatz(teilnehmer.getZusatz()).build();
		final String klassenname = getKlassenname(teilnehmer);
		final UrkundenmotivMitText urkundenmotivMitText = new UrkundenmotivMitTextBuilder(teilnehmer.getJahr(),
			urkundenMotivProvider, datum).build();

		final Auswertungsgruppe klasse = teilnehmer.getAuswertungsgruppe();
		Auswertungsgruppe schule = null;

		if (klasse != null) {
			schule = klasse.getParent();
		}

		DefaultUrkundeInhalt urkundeInhalt = null;
		if (schule == null) {
			urkundeInhalt = new DefaultUrkundeInhaltBuilder(urkundenmotivMitText).klassenname(klassenname)
				.teilnehmerUndPunkte(teilnehmerUndPunkte).build();
		} else {
			urkundeInhalt = new DefaultUrkundeInhaltBuilder(urkundenmotivMitText).klassenname(klassenname)
				.schulname(schule.getName()).teilnehmerUndPunkte(teilnehmerUndPunkte).build();
		}

		return urkundeInhalt;
	}

	private List<UrkundeInhalt> getGewinnerKaengurusprung(final List<DefaultUrkundeInhalt> kaengurusprungUrkunden,
		final Klassenstufe klassenstufe) {
		int maxKaengurusprung = 1;
		final List<UrkundeInhalt> urkundenMitMax = new ArrayList<>();
		for (final DefaultUrkundeInhalt urkunde : kaengurusprungUrkunden) {
			if (urkunde.getKlassenstufe() == klassenstufe) {
				final int kaengurusprung = urkunde.getKaengurusprung();
				if (kaengurusprung == maxKaengurusprung) {
					urkundenMitMax.add(urkunde);
				}
				if (kaengurusprung > maxKaengurusprung) {
					maxKaengurusprung = kaengurusprung;
					urkundenMitMax.clear();
					urkundenMitMax.add(urkunde);
				}
			}
		}
		return urkundenMitMax;
	}

	private String getKlassenname(final Teilnehmer teilnehmer) {
		if (teilnehmer.getAuswertungsgruppe() != null) {
			return teilnehmer.getAuswertungsgruppe().getName();
		}
		return teilnehmer.getKlassenstufe().getLabel();
	}
}
