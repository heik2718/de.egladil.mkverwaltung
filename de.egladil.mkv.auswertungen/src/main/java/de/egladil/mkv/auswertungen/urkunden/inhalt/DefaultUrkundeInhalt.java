//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import java.io.InputStream;

import com.itextpdf.text.BaseColor;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * DefaultUrkundeInhalt
 */
public final class DefaultUrkundeInhalt implements UrkundeInhalt {

	private final UrkundenmotivMitText urkundenmotivMitText;

	private final TeilnehmerUndPunkte teilnehmerUndPunkte;

	/**
	 * DefaultUrkundeInhalt
	 */
	DefaultUrkundeInhalt(final UrkundenmotivMitText urkundenmotivMitText, final TeilnehmerUndPunkte teilnehmerUndPunkte,
		final String schulname, final String klassenname) {
		this.urkundenmotivMitText = urkundenmotivMitText;
		this.teilnehmerUndPunkte = teilnehmerUndPunkte;
		this.teilnehmerUndPunkte.setKlassenname(klassenname);
		this.teilnehmerUndPunkte.setSchulname(schulname);
	}

	@Override
	public final String getWettbewerbsjahr() {
		return urkundenmotivMitText.getWettbewerbsjahr();
	}

	@Override
	public final String getSchulname() {
		return this.teilnehmerUndPunkte.getSchulname();
	}

	@Override
	public InputStream getBackgroundImage() {
		return urkundenmotivMitText.getBackgroundImage();
	}

	@Override
	public BaseColor getHeadlineColor() {
		return urkundenmotivMitText.getHeadlineColor();
	}

	@Override
	public final String getKlassenname() {
		return teilnehmerUndPunkte.getKlassenname();
	}

	@Override
	public final String getTeilnehmername() {
		return teilnehmerUndPunkte.getName();
	}

	@Override
	public final String getPunkteText() {
		return teilnehmerUndPunkte.getPunkte();
	}

	@Override
	public final String getDatum() {
		return urkundenmotivMitText.getDatum();
	}

	@Override
	public int getKaengurusprung() {
		return teilnehmerUndPunkte.getKaengurusprung();
	}

	@Override
	public String getTeilnehmerKuerzel() {
		return teilnehmerUndPunkte.getKuerzel();
	}

	@Override
	public String getZusatz() {
		return teilnehmerUndPunkte.getZusatz();
	}

	@Override
	public Klassenstufe getKlassenstufe() {
		return teilnehmerUndPunkte.getKlassenstufe();
	}

	@Override
	public int getPunkte() {
		return teilnehmerUndPunkte.getPunktzahl();
	}

	@Override
	public String getAntwortcode() {
		return teilnehmerUndPunkte.getAntwortcode();
	}

	@Override
	public String getWertungscode() {
		return teilnehmerUndPunkte.getBerechneterWertungscode();
	}

	@Override
	public Sprache getSprache() {
		return teilnehmerUndPunkte.getSprache();
	}
}
