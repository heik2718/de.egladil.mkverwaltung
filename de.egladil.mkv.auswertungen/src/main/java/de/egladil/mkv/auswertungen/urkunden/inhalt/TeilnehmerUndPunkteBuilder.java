//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import java.util.Arrays;

import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.renderer.PunkteRenderer;
import de.egladil.mkv.persistence.utils.MergeWordsInvoker;

/**
 * TeilnehmerUndPunkteBuilder
 */
public class TeilnehmerUndPunkteBuilder {

	private final Teilnehmer teilnehmer;

	private String zusatz;

	private final ILoesungszettel punkteProvider;

	private MergeWordsInvoker mergeInvoker;

	/**
	 * TeilnehmerUndPunkteBuilder
	 */
	public TeilnehmerUndPunkteBuilder(final Teilnehmer teilnehmer, final ILoesungszettel punkteProvider) {
		if (teilnehmer == null) {
			throw new IllegalArgumentException("teilnehmer ist erforderlich");
		}
		if (punkteProvider == null) {
			throw new IllegalArgumentException("punkteProvider ist erforderlich");
		}
		this.teilnehmer = teilnehmer;
		this.punkteProvider = punkteProvider;
		this.mergeInvoker = new MergeWordsInvoker();
	}

	public TeilnehmerUndPunkteBuilder zusatz(final String zusatz) {
		this.zusatz = zusatz;
		return this;
	}

	/**
	 * Erzeugt eine valide Instanz von TeilnehmerUndPunkte.
	 *
	 * @return TeilnehmerUndPunkte
	 */
	public TeilnehmerUndPunkte build() {
		final String name = mergeInvoker
			.mergeToLine(Arrays.asList(new String[] { teilnehmer.getVorname(), teilnehmer.getNachname() }));
		final int punktzahl = punkteProvider.getPunkte();
		final String punkte = new PunkteRenderer(punktzahl).render();
		final TeilnehmerUndPunkte teilnehmerUndPunkte = new TeilnehmerUndPunkte(name, punkte, punktzahl,
			punkteProvider.getKaengurusprung(), teilnehmer.getKlassenstufe(), teilnehmer.getSprache());
		teilnehmerUndPunkte.setKuerzel(teilnehmer.getKuerzel());
		if (zusatz != null) {
			teilnehmerUndPunkte.setZusatz(zusatz);
		}
		teilnehmerUndPunkte.setBerechneterWertungscode(punkteProvider.getWertungscode());
		if (teilnehmer.getAuswertungsgruppe() != null) {
			teilnehmerUndPunkte.setKlassenname(teilnehmer.getAuswertungsgruppe().getName());
		}
		return teilnehmerUndPunkte;
	}
}
