//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

/**
 * Dieses Artifakt dient zur Auswertung von Minikänguru-Wettbewerben. Es umfasst alles, was mit der Verarbeitung von
 * Daten aus Lösungszetteln zu tum hat.
 */
package de.egladil.mkv.auswertungen;