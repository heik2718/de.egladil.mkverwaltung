//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;

/**
 * Klassenuebersicht
 */
public class Klassenuebersicht {

	private final Klassenschluessel klassenschluessel;

	private final List<TeilnehmerUndPunkteLoesungszettelAdapter> schuelerinnen;

	/**
	 * Klassenuebersicht
	 */
	public Klassenuebersicht(final Klassenschluessel klassenschluessel, final List<TeilnehmerUndPunkteLoesungszettelAdapter> schuelerinnen) {
		this.klassenschluessel = klassenschluessel;
		this.schuelerinnen = schuelerinnen == null ? new ArrayList<>() : schuelerinnen;
	}

	public final Klassenschluessel getKlassenschluessel() {
		return klassenschluessel;
	}

	public final List<TeilnehmerUndPunkteLoesungszettelAdapter> getSchuelerinnen() {
		return schuelerinnen;
	}

}
