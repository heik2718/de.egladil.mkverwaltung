//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Gesamtpunktverteilung
 */
@XmlRootElement(name = "gesamtpunktverteilung")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "punkte", propOrder = { "titel", "basis", "bewertung", "gesamtpunktverteilungItems", "anzahlTeilnehmer", "median",
	"rohpunktItems", "sectionEinzelergebnisse", "aufgabeErgebnisItems" })
public class Gesamtpunktverteilung {

	@XmlElement(name = "titel")
	private String titel;

	@XmlElement(name = "basis")
	private String basis;

	@XmlElement(name = "bewertung")
	private String bewertung;

	@XmlElement(name = "teilnehmerzahl")
	private int anzahlTeilnehmer;

	@XmlElement(name = "sectionEinzelergebnisse")
	private String sectionEinzelergebnisse = "Lösungen je Aufgabe";

	@XmlElement(name = "median")
	private String median;

	@XmlElement(name = "intervallItem")
	private List<GesamtpunktverteilungItem> gesamtpunktverteilungItems = new ArrayList<>();

	@XmlElement(name = "aufgabeErgebnis")
	private List<AufgabeErgebnisItem> aufgabeErgebnisItems = new ArrayList<>();

	@XmlElement(name = "rohpunktitem")
	private List<RohpunktItem> rohpunktItems = new ArrayList<>();

	/**
	 * Erzeugt eine Instanz von Gesamtpunktverteilung
	 */
	Gesamtpunktverteilung() {
	}

	/**
	 *
	 * @param texte GesamtpunktverteilungTexte darf nicht null sein.
	 * @param daten GesamtpunktverteilungDaten darf nicht null sein.
	 */
	public Gesamtpunktverteilung(GesamtpunktverteilungTexte texte, GesamtpunktverteilungDaten daten) {
		this.anzahlTeilnehmer = daten.getAnzahlTeilnehmer();
		this.aufgabeErgebnisItems = daten.getAufgabeErgebnisItemsSorted();
		this.basis = texte.getBasis();
		this.bewertung = texte.getBewertung();
		this.gesamtpunktverteilungItems = daten.getGesamtpunktverteilungItemsSorted();
		this.median = daten.getMedian();
		this.rohpunktItems = daten.getRohpunktItemsSorted();
		this.sectionEinzelergebnisse = texte.getSectionEinzelergebnisse();
		this.titel = texte.getTitel();
	}

	public String getTitel() {
		return titel;
	}

	public String getBasis() {
		return basis;
	}

	public String getBewertung() {
		return bewertung;
	}

	public int getAnzahlTeilnehmer() {
		return anzahlTeilnehmer;
	}

	public String getSectionEinzelergebnisse() {
		return sectionEinzelergebnisse;
	}

	public String getMedian() {
		return median;
	}

	public List<GesamtpunktverteilungItem> getGesamtpunktverteilungItems() {
		return gesamtpunktverteilungItems;
	}

	public List<AufgabeErgebnisItem> getAufgabeErgebnisItems() {
		return aufgabeErgebnisItems;
	}

	public List<RohpunktItem> getProzentrangItems() {
		return rohpunktItems;
	}
}
