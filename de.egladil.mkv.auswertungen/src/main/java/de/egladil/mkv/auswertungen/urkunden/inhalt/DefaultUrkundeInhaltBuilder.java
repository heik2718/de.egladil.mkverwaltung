//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

/**
 * DefaultUrkundeInhaltBuilder
 */
public final class DefaultUrkundeInhaltBuilder {

	private final UrkundenmotivMitText nestedSchuleUrkundeText;

	private String nestedKlassenname;

	private String nestedSchulname;

	private TeilnehmerUndPunkte nestedTeilnehmerUndPunkte;

	/**
	 * DefaultUrkundeInhaltBuilder
	 */
	public DefaultUrkundeInhaltBuilder(final UrkundenmotivMitText nestedSchuleUrkundeText) {
		if (nestedSchuleUrkundeText == null) {
			throw new IllegalArgumentException("UrkundenmotivMitText ist erforderlich");
		}
		this.nestedSchuleUrkundeText = nestedSchuleUrkundeText;
	}

	public DefaultUrkundeInhaltBuilder teilnehmerUndPunkte(final TeilnehmerUndPunkte teilnehmerUndPunkte) {
		this.nestedTeilnehmerUndPunkte = teilnehmerUndPunkte;
		return this;
	}

	public DefaultUrkundeInhaltBuilder klassenname(final String klassenname) {
		this.nestedKlassenname = klassenname;
		return this;
	}

	public DefaultUrkundeInhaltBuilder schulname(final String schulname) {
		this.nestedSchulname = schulname;
		return this;
	}

	public DefaultUrkundeInhalt build() {
		if (nestedTeilnehmerUndPunkte == null) {
			throw new IllegalStateException("teilnehmerUndPunkte ist erforderlich. Der Builder ist nicht korrekt initialisiert.");
		}
		return new DefaultUrkundeInhalt(nestedSchuleUrkundeText, nestedTeilnehmerUndPunkte, nestedSchulname, nestedKlassenname);
	}
}
