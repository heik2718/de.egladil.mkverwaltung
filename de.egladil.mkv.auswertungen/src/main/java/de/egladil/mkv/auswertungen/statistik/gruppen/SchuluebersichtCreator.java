//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.List;

import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;

/**
 * SchuluebersichtCreator
 */
public class SchuluebersichtCreator {

	/**
	 *
	 * @param alleLoesungszettel List
	 * @return Schuluebersicht
	 */
	public Schuluebersicht createSchuluebersicht(final List<Loesungszettel> alleLoesungszettel, final String schulname) {

		final Schuluebersicht schuluebersicht = new Schuluebersicht(schulname);

		for (final Loesungszettel loesungszettel : alleLoesungszettel) {
			schuluebersicht.putLoesungszettel(loesungszettel);
		}

		return schuluebersicht;
	}

}
