//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungItem;
import de.egladil.mkv.auswertungen.domain.Punktintervall;
import de.egladil.mkv.auswertungen.renderer.DoubleExactRenderer;

/**
 * BerechneProzentrangIntervallCommand
 */
public class BerechneProzentrangIntervallCommand {

	private static final Logger LOG = LoggerFactory.getLogger(BerechneProzentrangIntervallCommand.class);

	private static final int ANZAHL_NACHKOMMASTELLEN = 2;

	private final ZahlRundenCommand zahlRundenCommand = new ZahlRundenCommand();

	public void berechne(List<GesamtpunktverteilungItem> verteilungItems, int anzahlTeilnehmer) {
		LOG.debug("Gesamtzahl: {}", anzahlTeilnehmer);
		for (GesamtpunktverteilungItem item : verteilungItems) {
			int anzahlGleichSchlechter = berechneKumulierteHaeufigkeit(item.getPunktintervall(), verteilungItems);
			double pr = (Double.valueOf(anzahlGleichSchlechter) * 100.0) / anzahlTeilnehmer;
			pr = zahlRundenCommand.rundeAufAnzahlNachkommastellen(pr, ANZAHL_NACHKOMMASTELLEN);
			item.setProzentrang(pr);
			item.setProzentrangText(new DoubleExactRenderer(pr).render());
			LOG.debug("GesamtpunktverteilungItem: {}, Prozentrang: {}", item.getPunktintervall(), item.getProzentrangText());
		}
	}

	/**
	 * Zählt die Items, deren Punktintervall gleich oder schlechter als das gegebene Intervall ist.
	 *
	 * @param punktintervall
	 * @param verteilungItems
	 * @return
	 */
	int berechneKumulierteHaeufigkeit(Punktintervall punktintervall, List<GesamtpunktverteilungItem> verteilungItems) {
		int anzahl = 0;
		for (GesamtpunktverteilungItem item : verteilungItems) {
			Punktintervall aktuellesIntervall = item.getPunktintervall();
			if (punktintervall.getMaxVal() >= aktuellesIntervall.getMinVal()) {
				anzahl += item.getAnzahl();
			}
		}
		LOG.debug("Referenzintervall: {}, anzahl: {}", punktintervall, anzahl);
		return anzahl;
	}
}
