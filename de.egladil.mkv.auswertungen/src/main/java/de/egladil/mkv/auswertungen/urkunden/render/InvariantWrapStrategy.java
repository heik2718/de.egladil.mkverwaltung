//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.util.Arrays;
import java.util.List;

/**
 * InvariantWrapStrategy lässt alles so, wie es ist
 */
public class InvariantWrapStrategy implements LineWrapStrategy {

	@Override
	public List<String> breakLines(final String name) {
		return Arrays.asList(new String[] { name });
	}
}
