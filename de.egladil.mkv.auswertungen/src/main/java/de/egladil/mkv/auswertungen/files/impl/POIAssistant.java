//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellAddress;

import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;

/**
 * POIAssistant ist ein kleiner Helfer für Dinge, die mit POI zu tun haben.
 */
public class POIAssistant {

	/**
	 * Ermittelt, ob es sich um die Überschriftenzeile handelt
	 *
	 * @param row Row darf nicht null sein.
	 * @param startAddress
	 * @return boolean
	 * @throws NullPointerException wenn row null
	 */
	@SuppressWarnings("deprecation")
	public static boolean isHeadline(final Row row) {
		if (row == null) {
			throw new NullPointerException("Parameter row");
		}
		final short firstNum = row.getFirstCellNum();
		if (firstNum < 0) {
			return false;
		}
		final Iterator<Cell> iter = row.cellIterator();
		while (iter.hasNext()) {
			final Cell cell = iter.next();
			if (CellType.STRING == cell.getCellTypeEnum()
				&& IAuswertungStreamReader.STARTADDRESS_MARKER.equalsIgnoreCase(cell.getStringCellValue())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Ermittelt, ob es sich um eine Datenzeile handelt. Danetzeilen enthalten haben in meinem Kontext eine Formel.
	 *
	 * @param row Row darf nicht null sein
	 * @return boolean
	 * @throws NullPointerException wenn row null
	 */
	@SuppressWarnings("deprecation")
	public static boolean isDatenRow(final Row row) {
		if (row == null) {
			throw new NullPointerException("Parameter row");
		}
		final short firstNum = row.getFirstCellNum();
		if (firstNum < 0) {
			return false;
		}
		if (POIAssistant.isHeadline(row)) {
			return false;
		}
		final Iterator<Cell> iter = row.cellIterator();
		while (iter.hasNext()) {
			final Cell cell = iter.next();
			if (CellType.FORMULA == cell.getCellTypeEnum()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Sucht die Zelle mit den Text contents.
	 *
	 * @param sheet Sheet darf nicht null sein
	 * @param contents String darf nicht null sein
	 * @return CellAddress
	 * @throws NullPointerException wenn einer der Parameter null ist.
	 */
	@SuppressWarnings("deprecation")
	public static CellAddress findAddressWithStringContents(final Sheet sheet, final String contents) {
		if (sheet == null) {
			throw new NullPointerException("Parameter sheet");
		}
		if (contents == null) {
			throw new NullPointerException("Parameter contents");
		}
		final Iterator<Row> rows = sheet.rowIterator();
		while (rows.hasNext()) {
			final Row row = rows.next();
			final Iterator<Cell> cells = row.cellIterator();
			while (cells.hasNext()) {
				final Cell cell = cells.next();
				if (CellType.STRING == cell.getCellTypeEnum()) {
					final String val = cell.getStringCellValue();
					if (contents.equalsIgnoreCase(val)) {
						return cell.getAddress();
					}
				}
			}
		}
		return null;
	}

	/**
	 * Gibt den String-Wert der Zelle zurück, falls es sich um eine Zelle vom Typ STRING handelt. sonst null.
	 *
	 * @param cell Cell darf nicht null sein
	 * @return String oder null
	 * @throws NullPointerException wenn cell null
	 */
	@SuppressWarnings("deprecation")
	public static String getCellValue(final Cell cell) {
		if (cell == null) {
			throw new NullPointerException("Parameter cell");
		}
		String val = null;
		if (CellType.STRING == cell.getCellTypeEnum()) {
			val = cell.getStringCellValue();
			if (val == null) {
				val = "";
			}
		} else {
			if (CellType.NUMERIC == cell.getCellTypeEnum()) {
				final double d = cell.getNumericCellValue();
				val = "" + Math.round(d);
			}
		}
		return val;
	}

	/**
	 * Eine Text-Zelle enthält entweder einen String oder ist leer.
	 *
	 * @param maxCol
	 * @param cell
	 * @param actCol
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean isTextCell(final Cell cell) {
		return CellType.STRING == cell.getCellTypeEnum() || CellType.BLANK == cell.getCellTypeEnum()
			|| CellType.NUMERIC == cell.getCellTypeEnum();
	}

	public static boolean isEmptySheet(final Sheet sheet) {
		final CellAddress startMarker = findAddressWithStringContents(sheet, IAuswertungStreamReader.STARTADDRESS_MARKER);
		if (startMarker == null) {
			return true;
		}
		return false;
	}

	/**
	 * Gibt die Klassenstufe zurück, zu der das Arbeitsblatt gehört.
	 *
	 * @param sheet
	 * @return Klassenstufe oder null, wenn leer.
	 */
	public static Klassenstufe getKlasse(final Sheet sheet) throws MKAuswertungenException {
		if (isEmptySheet(sheet)) {
			return null;
		}
		// Nach Klassenstufe 2 muss zuerst gesucht werden, da das Klassenstufe-1-Marker auch in Klassenstufe-2-Auswertungen vorkommt.
		CellAddress address = POIAssistant.findAddressWithStringContents(sheet, IAuswertungStreamReader.KLASSE_2_MARKER);
		if (address != null) {
			return Klassenstufe.ZWEI;
		}
		address = POIAssistant.findAddressWithStringContents(sheet, IAuswertungStreamReader.KLASSE_1_MARKER);
		if (address != null) {
			return Klassenstufe.EINS;
		}
		address = POIAssistant.findAddressWithStringContents(sheet, IAuswertungStreamReader.INKLUSION_MARKER);
		if (address != null) {
			return Klassenstufe.IKID;
		}
		throw new MKAuswertungenException(
			"Das sheet scheint weder zu einer Klassenstufe-1- noch zu einer Klassenstufe-2-Auswertung zu gehören");
	}
}
