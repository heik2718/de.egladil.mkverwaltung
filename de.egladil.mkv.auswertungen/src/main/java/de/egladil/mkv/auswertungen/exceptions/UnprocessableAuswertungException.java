//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.exceptions;

/**
 * UnprocessableAuswertungException ist eine RuntimeException, die auf Fehler im eingelesenen InputStream hinweist.
 */
public class UnprocessableAuswertungException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von MKAuswertungenException
	 */
	public UnprocessableAuswertungException(String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von MKAuswertungenException
	 */
	public UnprocessableAuswertungException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von MKAuswertungenException
	 */
	public UnprocessableAuswertungException(String message, Throwable cause, boolean enableSuppression,
		boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
