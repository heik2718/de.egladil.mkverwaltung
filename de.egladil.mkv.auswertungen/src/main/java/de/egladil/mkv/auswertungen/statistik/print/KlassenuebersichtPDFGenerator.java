//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.statistik.gruppen.Klassenuebersicht;
import de.egladil.mkv.auswertungen.urkunden.UebersichtFontProvider;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.renderer.PunkteRenderer;

/**
 * KlassenuebersichtPDFGenerator
 */
public class KlassenuebersichtPDFGenerator {

	private final UebersichtFontProvider fontProvider;

	private final String wettbewerbsjahr;

	private final PdfCellRenderer cellRendererLeft = new PdfCellLeftTextRenderer();

	private final PdfCellRenderer cellRendererRight = new PdfCellRightTextRenderer();

	private final PdfCellRenderer cellRendererCenter = new PdfCellCenteredTextRenderer();

	/**
	 * KlassenuebersichtPDFGenerator
	 */
	public KlassenuebersichtPDFGenerator() {
		this.fontProvider = new UebersichtFontProvider();
		wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	public byte[] generiere(final Klassenuebersicht klassenuebersicht) {

		final Document doc = new Document(PageSize.A4.rotate());
		final int numCols = klassenuebersicht.getKlassenschluessel().getKlassenstufe().getAnzahlAufgaben() + 7;

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

			PdfWriter.getInstance(doc, out);
			doc.open();

			final PdfPTable table = new PdfPTable(numCols);

			final Font font = fontProvider.getFontBold();
			final PdfPCell cell = cellRendererCenter.createCell(font, "Wertungen "
				+ klassenuebersicht.getKlassenschluessel().getKlassenname() + " (r = richtig, f = falsch, n = nicht gelöst)",
				numCols);
			table.addCell(cell);

			addHeaders(table, klassenuebersicht.getKlassenschluessel().getKlassenstufe());

			for (final TeilnehmerUndPunkteLoesungszettelAdapter tup : klassenuebersicht.getSchuelerinnen()) {
				addRow(table, tup);
			}

			doc.add(table);
			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}

	private void addHeaders(final PdfPTable table, final Klassenstufe klassenstufe) {
		final Font font = fontProvider.getFontBold();
		PdfPCell cell = cellRendererLeft.createCell(font, "Name", 5);
		table.addCell(cell);
		final List<String> aufgabennummern = klassenstufe.getAufgabennummern(wettbewerbsjahr);
		for (final String nummer : aufgabennummern) {
			cell = cellRendererCenter.createCell(font, nummer, 1);
			table.addCell(cell);
		}
		cell = cellRendererLeft.createCell(font, "Punkte", 2);
		table.addCell(cell);
	}

	private void addRow(final PdfPTable table, final TeilnehmerUndPunkteLoesungszettelAdapter schueler) {
		final Font font = fontProvider.getFontNormal();
		PdfPCell cell = cellRendererLeft.createCell(font, schueler.getName(), 5);
		table.addCell(cell);

		for (final char wertungscode : schueler.getWertungscode().toCharArray()) {
			cell = cellRendererCenter.createCell(font, String.valueOf(wertungscode), 1);
			table.addCell(cell);
		}
		cell = cellRendererRight.createCell(font, new PunkteRenderer(schueler.getPunkte()).render(), 2);
		table.addCell(cell);
	}
}
