//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import de.egladil.mkv.auswertungen.domain.Punktintervall;

/**
 * PunktintervallKlasseZweiFactory
 */
public class PunktintervallKlasseZweiFactory extends PunktintervallFactory {

	/**
	 * Erzeugt eine Instanz von PunktintervallKlasseZweiFactory
	 */
	PunktintervallKlasseZweiFactory() {
	}

	/**
	 * @see de.egladil.mkv.auswertungen.statistik.impl.PunktintervallFactory#anzahlIntervalle()
	 */
	@Override
	protected int anzahlIntervalle() {
		return 16;
	}

	/**
	 *
	 * @see de.egladil.mkv.auswertungen.statistik.impl.PunktintervallFactory#createPunktintervall(int)
	 */
	@Override
	protected Punktintervall createPunktintervall(int minVal) {
		if (minVal < 0 || minVal > 7500) {
			throw new IllegalArgumentException("minVal muss zwischen 0 und 7500 liegen: minVal=" + minVal);
		}
		int obere = minVal + Punktintervall.DEFAULT_LAENGE;
		switch (obere) {
		case 6975:
			obere = 6900;
			break;
		case 7475:
			obere = 7200;
			break;
		default:
			break;
		}
		if (obere > 7500) {
			obere = 7500;
		}
		return new Punktintervall.Builder(minVal).maxVal(obere).build();
	}
}
