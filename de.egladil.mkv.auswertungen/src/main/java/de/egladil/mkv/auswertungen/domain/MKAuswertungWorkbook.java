//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * MKAuswertungWorkbook
 */
public class MKAuswertungWorkbook {

	/** Der key dient hauptsächlich Testzwecken, um equals(), hashCode() überschreiben zu können. */
	private String key = "DEFAULT0";

	private List<MKAuswertungRow> rows = new ArrayList<>();

	/**
	 * Erzeugt eine Instanz von MKAuswertungWorkbook
	 */
	public MKAuswertungWorkbook() {
	}

	/**
	 * Erzeugt eine Instanz von MKAuswertungWorkbook mit einem key
	 */
	public MKAuswertungWorkbook(String key) {
		this.key = key;
	}

	public void addRow(MKAuswertungRow row) {
		rows.add(row);
	}

	/**
	 *
	 * @param rows
	 */
	public void addAll(List<MKAuswertungRow> rows) {
		this.rows.addAll(rows);
	}

	/**
	 * Gibt alle Zeilen des Workbooks zurück.
	 *
	 * @return List unmodifiable
	 */
	public List<MKAuswertungRow> getRows() {
		return Collections.unmodifiableList(rows);
	}

	/**
	 * Gibt eine Kopie der Zeilen nach index sortiert zurück.
	 *
	 * @return
	 */
	public List<MKAuswertungRow> getSortedCopyOfRows() {
		List<MKAuswertungRow> result = new ArrayList<>();
		result.addAll(rows);
		Collections.sort(result);
		return result;
	}

	/**
	 * Gibt an, ob das Workbook Daten enthält.
	 *
	 * @return
	 */
	public boolean isEmpty() {
		return rows.isEmpty();
	}

	public String prettyPrint() {
		StringBuffer sb = new StringBuffer();
		for (MKAuswertungRow t : rows) {
			sb.append(t.prettyPrint());
			sb.append("\n");
		}
		return sb.toString();
	}

	/**
	 * Anzahl der MKAuswertungRows.
	 *
	 * @return
	 */
	public int size() {
		return rows.size();
	}

	public Klassenstufe getKlasse() {
		if (rows == null || rows.isEmpty()) {
			return null;
		}
		return rows.get(0).getKlasse();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MKAuswertungWorkbook other = (MKAuswertungWorkbook) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
}
