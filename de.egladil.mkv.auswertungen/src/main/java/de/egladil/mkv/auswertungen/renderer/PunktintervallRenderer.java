//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.renderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.Punktintervall;
import de.egladil.mkv.persistence.renderer.PunkteRenderer;

/**
 * PunktintervallRenderer erzeugt die für eine Ausgabe formatierte Darstellung eines Punktintervalls.
 */
public class PunktintervallRenderer {

	private static final Logger LOG = LoggerFactory.getLogger(PunktintervallRenderer.class);

	private final Punktintervall punktintervall;

	/**
	 * Erzeugt eine Instanz von PunktintervallRenderer
	 */
	public PunktintervallRenderer(Punktintervall punktintervall) {
		this.punktintervall = punktintervall;
	}

	/**
	 *
	 * @return
	 */
	public String render() {
		String untereGrenze = new PunkteRenderer(punktintervall.getMinVal()).render();
		String obereGrenze = new PunkteRenderer(punktintervall.getMaxVal()).render();
		String text = untereGrenze + "-" + obereGrenze;
		LOG.debug("rein: {}, raus: {}", punktintervall.toString(), text);
		return text;
	}
}
