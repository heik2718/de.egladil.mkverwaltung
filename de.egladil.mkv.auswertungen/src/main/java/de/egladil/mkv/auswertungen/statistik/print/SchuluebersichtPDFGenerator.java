//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.gruppen.LoesungszettelKlassenstufeProvider;
import de.egladil.mkv.auswertungen.statistik.gruppen.Schuluebersicht;
import de.egladil.mkv.auswertungen.urkunden.PdfMerger;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;

/**
 * SchuluebersichtPDFGenerator generiert ein Deckblatt mit dem Schulmedian, klassenstufenweise Aufgabenübersichten und
 * Rohpunktübersichten ohne Prozentrang (also nur Punkte + Anzahl Kinder) je Klassenstufe.
 */
public class SchuluebersichtPDFGenerator extends BaseSchuluebersichtPDFGenerator {

	private final IVerteilungGenerator verteilungGenerator;

	private final TeilnahmenFacade teilnahmenFacade;

	private final boolean mitGesamtmedian;

	/**
	 * SchuluebersichtPDFGenerator
	 */
	public SchuluebersichtPDFGenerator(final String wettbewerbsjahr, final TeilnahmenFacade teilnehmerFacade,
		final IVerteilungGenerator verteilungGenerator, final boolean mitGesamtmedian) {
		super(wettbewerbsjahr);
		this.teilnahmenFacade = teilnehmerFacade;
		this.verteilungGenerator = verteilungGenerator;
		this.mitGesamtmedian = mitGesamtmedian;
	}

	@Override
	protected byte[] generiereDeckblatt(final LoesungszettelKlassenstufeProvider loesungszettelProvider) {

		final String medianKlasseEins = getMedian(loesungszettelProvider, Klassenstufe.EINS);
		final String medianKlasseZwei = getMedian(loesungszettelProvider, Klassenstufe.ZWEI);
		final String medianInklusion = getMedian(loesungszettelProvider, Klassenstufe.IKID);

		final Document doc = new Document(PageSize.A4);

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

			PdfWriter.getInstance(doc, out);
			doc.open();

			final Paragraph element = getTitle(loesungszettelProvider);
			element.setAlignment(Element.ALIGN_CENTER);
			doc.add(element);

			final Font font = getFontProvider().getFontNormal();
			doc.add(Chunk.NEWLINE);

			if (medianKlasseEins != null) {

				final List<Loesungszettel> loesungszettel = teilnahmenFacade
					.findLoesungszettelByJahrUndKlassenstufe(String.valueOf(getWettbewerbsjahr()), Klassenstufe.EINS);

				if (!loesungszettel.isEmpty()) {
					String text = "Schulmedian Klasse 1: " + medianKlasseEins;

					if (mitGesamtmedian) {
						final List<ILoesungszettel> datenbasis = new ArrayList<>(loesungszettel.size());
						datenbasis.addAll(loesungszettel);
						final String median = this.verteilungGenerator.berechneMedian(datenbasis);

						text += " (Gesamtmedian " + median + ")";
					}

					doc.add(new Paragraph(text, font));
				}
			}
			if (medianKlasseZwei != null) {
				final List<Loesungszettel> loesungszettel = teilnahmenFacade
					.findLoesungszettelByJahrUndKlassenstufe(String.valueOf(getWettbewerbsjahr()), Klassenstufe.ZWEI);

				if (!loesungszettel.isEmpty()) {
					String text = "Schulmedian Klasse 2: " + medianKlasseZwei;

					if (mitGesamtmedian) {
						final List<ILoesungszettel> datenbasis = new ArrayList<>(loesungszettel.size());
						datenbasis.addAll(loesungszettel);
						final String median = this.verteilungGenerator.berechneMedian(datenbasis);
						text += " (Gesamtmedian " + median + ")";
					}
					doc.add(new Paragraph(text, font));
				}

			}
			if (medianInklusion != null) {
				final List<Loesungszettel> loesungszettel = teilnahmenFacade
					.findLoesungszettelByJahrUndKlassenstufe(String.valueOf(getWettbewerbsjahr()), Klassenstufe.IKID);

				if (!loesungszettel.isEmpty()) {
					String text = "Schulmedian Inklusion: " + medianInklusion;

					if (mitGesamtmedian) {
						final List<ILoesungszettel> datenbasis = new ArrayList<>(loesungszettel.size());
						datenbasis.addAll(loesungszettel);
						final String median = this.verteilungGenerator.berechneMedian(datenbasis);
						text += " (Gesamtmedian " + median + ")";
					}
					doc.add(new Paragraph(text, font));
				}

			}

			doc.add(Chunk.NEWLINE);
			doc.add(getParagraphMediandefinition());

			doc.add(Chunk.NEWLINE);
			if (loesungszettelProvider.anzahlKinder() > 1) {
				doc.add(new Paragraph(getInhaltsangabe(), font));
			}

			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}

	/**
	 * Generiert den Statistikreport für die gegebene Schulübersicht.
	 *
	 * @param schuluebersicht Schuluebersicht
	 * @return byte[]
	 */
	public byte[] generiere(final Schuluebersicht schuluebersicht) {

		final List<byte[]> seiten = new ArrayList<>();

		seiten.add(generiereDeckblatt(schuluebersicht));

		for (final Klassenstufe klassenstufe : schuluebersicht.getKeysSorted()) {
			final List<ILoesungszettel> loesungszettel = schuluebersicht.getLoesungszettel(klassenstufe);

			final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(loesungszettel,
				klassenstufe, getWettbewerbsjahr());
			final byte[] aufgabenuebersicht = generiereAufgabenuebersicht(daten, klassenstufe.getLabel());
			seiten.add(aufgabenuebersicht);

			final byte[] punkteUndAnzahlUebersicht = generierePunkttabelle(daten, klassenstufe.getLabel());
			seiten.add(punkteUndAnzahlUebersicht);
		}

		final byte[] result = new PdfMerger().concatPdf(seiten);
		return result;
	}

	private byte[] generierePunkttabelle(final GesamtpunktverteilungDaten daten, final String klassenname) {

		return new PunkteMitAnzahlPDFGenerator().generiere(daten, klassenname);
	}

	@Override
	protected String getInhaltsangabe() {
		return "Die folgenden Seiten zeigen je Klassenstufe die Übersicht über die gelösten Aufgaben sowie die erreichten Punkte.";
	}
}
