//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;

/**
 * UrkundeAbschnittRenderer
 */
public interface UrkundeAbschnittRenderer {

	/**
	 * Erzeugt den Text für den speziellen Abschnitt.
	 *
	 * @param content
	 * @param urkundeInhalt
	 * @param differenceFromTopPositionPoints int
	 * @return int die neue vertikale Verschiebung in bezug auf TOP.
	 */
	int printAbschnittAndShiftVerticalPosition(PdfContentByte content, UrkundeInhalt urkundeInhalt,
		int differenceFromTopPositionPoints)  throws DocumentException, IOException;

}
