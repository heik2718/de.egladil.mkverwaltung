//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.payload.response;

import java.util.List;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;

/**
 * PublicGesamtpunktverteilung
 */
public class PublicGesamtpunktverteilung {

	private String name;

	private String jahr;

	private List<GesamtpunktverteilungDaten> daten;

	/**
	 * PublicGesamtpunktverteilung
	 */
	public PublicGesamtpunktverteilung() {
	}

	/**
	 * PublicGesamtpunktverteilung
	 */
	public PublicGesamtpunktverteilung(final String name, final String jahr, final List<GesamtpunktverteilungDaten> daten) {
		this.name = name;
		this.jahr = jahr;
		this.daten = daten;
	}

	public final String getJahr() {
		return jahr;
	}

	public final List<GesamtpunktverteilungDaten> getDaten() {
		return daten;
	}

	public final String getName() {
		return name;
	}
}
