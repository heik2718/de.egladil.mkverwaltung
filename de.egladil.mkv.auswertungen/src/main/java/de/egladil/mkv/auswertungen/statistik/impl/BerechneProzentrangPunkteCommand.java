//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.RohpunktItem;
import de.egladil.mkv.auswertungen.renderer.DoubleExactRenderer;

/**
 * BerechneProzentrangPunkteCommand berechnet den Prozentrang für eine gegebene Punktzahl innerhalb der Stichprobe. Der
 * Prozentrang ist der prozentuale Anteil der kumulierten Häufigkeit bezüglich der Stichprobengröße.
 */
public class BerechneProzentrangPunkteCommand {

	private static final Logger LOG = LoggerFactory.getLogger(BerechneProzentrangPunkteCommand.class);

	private static final int ANZAHL_NACHKOMMASTELLEN = 3;

	private final ZahlRundenCommand zahlRundenCommand = new ZahlRundenCommand();

	/**
	 * Berechnet für jedes Rohpinktitem den Prozentrang und setzt ihn als Attribut.
	 *
	 * @param rohpunktitems List darf nicht null sein
	 * @param anzahlTeilnehmer int muss nichtnegativ sein.
	 * @return
	 */
	public void berechne(List<RohpunktItem> rohpunktitems, int anzahlTeilnehmer) {
		LOG.debug("Gesamtzahl: {}", anzahlTeilnehmer);
		for (RohpunktItem item : rohpunktitems) {
			int anzahlGleichSchlechter = berechneKumulierteHaeufigkeit(item.getPunkte(), rohpunktitems);
			double pr = (Double.valueOf(anzahlGleichSchlechter) * 100.0) / anzahlTeilnehmer;
			pr = zahlRundenCommand.rundeAufAnzahlNachkommastellen(pr, ANZAHL_NACHKOMMASTELLEN);
			item.setProzentrang(pr);
			item.setProzentrangText(new DoubleExactRenderer(pr).render());
			LOG.debug("Rohpunktitem: {}, Prozentrang: {}", item.getPunkte(), item.getProzentrangText());
		}
	}

	/**
	 * Die kumulierte Häufigkeit ist die Anzahl der Teilnehmer mit der gleichen oder einer schlechteren Punktzahl.
	 *
	 * @param punkte int Referenzpunktzahl.
	 * @param rohpunktitems List Gesamtstichprobe.
	 * @return int die kumulierte Häufigkeit.
	 */
	int berechneKumulierteHaeufigkeit(int punkte, List<RohpunktItem> rohpunktitems) {
		int anzahl = 0;
		for (RohpunktItem item : rohpunktitems) {
			if (punkte >= item.getPunkte()) {
				anzahl+=item.getAnzahl();
			}
		}
		return anzahl;
	}
}
