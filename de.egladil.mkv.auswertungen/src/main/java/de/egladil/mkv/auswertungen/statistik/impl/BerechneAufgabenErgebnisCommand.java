//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.AufgabeErgebnisItem;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Wertung;

/**
 * BerechneAufgabenErgebnisCommand
 */
public class BerechneAufgabenErgebnisCommand {

	private static final Logger LOG = LoggerFactory.getLogger(BerechneAufgabenErgebnisCommand.class);

	private static final int ANZAHL_NACHKOMMASTELLEN = 2;

	private final ZahlRundenCommand zahlRundenCommand = new ZahlRundenCommand();

	/**
	 * Erzeugt das AufabeErgebnisItem.
	 *
	 * @param aufgabeErgebnisItem
	 * @param loesungszettel
	 */
	public AufgabeErgebnisItem create(final String nummer, final int index, final List<ILoesungszettel> loesungszettel) {
		LOG.debug("Nummer = {}, index = {}", nummer, index);
		int anzahlRichtig = 0;
		int anzahlNicht = 0;
		int anzahlFalsch = 0;
		for (final ILoesungszettel zettel : loesungszettel) {
			final Wertung wertung = getWertungscode(zettel, index);
			switch (wertung) {
			case r:
				anzahlRichtig++;
				break;
			case n:
				anzahlNicht++;
				break;
			case f:
				anzahlFalsch++;
				break;
			default:
				break;
			}
		}
		final AufgabeErgebnisItem item = new AufgabeErgebnisItem.Builder(index, nummer).richtig(anzahlRichtig).nicht(anzahlNicht)
			.falsch(anzahlFalsch).build();
		berechenProzente(item, anzahlFalsch + anzahlNicht + anzahlRichtig);
		return item;
	}

	/**
	 * Berechnet die Prozente.
	 *
	 * @param item
	 */
	private void berechenProzente(final AufgabeErgebnisItem item, final int anzahlGesamt) {
		item.setAnteilRichtigGeloest(berechneUndRundeProzente(item.getAnzahlRichtigGeloest(), anzahlGesamt));
		item.setAnteilNichtGeloest(berechneUndRundeProzente(item.getAnzahlNichtGeloest(), anzahlGesamt));
		item.setAnteilFalschGeloest(berechneUndRundeProzente(item.getAnzahlFalschGeloest(), anzahlGesamt));
	}

	double berechneUndRundeProzente(final int anzahl, final int anzahlGesamt) {
		double pr = Double.valueOf(anzahl) / anzahlGesamt * 100.0;
		pr = zahlRundenCommand.rundeAufAnzahlNachkommastellen(pr, ANZAHL_NACHKOMMASTELLEN);
		return pr;
	}

	/**
	 * Gibt den Wertungscode für den gegebenen index zurück.
	 *
	 * @param index
	 * @return String f, r oder n
	 * @throws IndexOutOfBoundsException falls der index negativ oder zu groß ist.
	 */
	Wertung getWertungscode(final ILoesungszettel loesungszettel, final int index) {
		final String wertungscode = loesungszettel.getWertungscode();
		if (wertungscode != null) {
			LOG.debug("wertungscode.length = {}", wertungscode.length());
			final char[] charArray = wertungscode.toCharArray();
			return Wertung.valueOfStringIgnoringCase(String.valueOf(charArray[index]));
		}
		return null;
	}
}
