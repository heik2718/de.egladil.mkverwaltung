//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files;

import java.text.MessageFormat;

import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.domain.enums.UploadMimeType;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * AuswertungstabellenUtils
 */
public final class AuswertungstabellenUtils {

	private static final String PROTOKOLL_MIMETYPE_PATTERN = "MIMETYPE {0} - Dateiname {1}";

	private static final String DATEINAME_PATTERN = "{0}_{1}_{2}";

	/**
	 * Erzeugt eine Instanz von AuswertungstabellenUtils
	 */
	private AuswertungstabellenUtils() {
	}

	public static String getDateiname(final TeilnahmeIdentifier teilnahmeIdentifier, final String checksumme,
		final UploadMimeType uploadType) {

		final String dateiname = MessageFormat.format(DATEINAME_PATTERN, new Object[] { teilnahmeIdentifier.getTeilnahmeart(),
			teilnahmeIdentifier.getKuerzel(), checksumme + uploadType.getFileExtension() });

		return dateiname;
	}

	public static void checkUploadMimetype(final String benutzerUuid, final UploadMimeType uploadMimeType, final String mimeType,
		final String dateiname, final IProtokollService protokollService) {

		if (uploadMimeType == UploadMimeType.UNKNOWN) {
			final String protokollMessage = MessageFormat.format(PROTOKOLL_MIMETYPE_PATTERN, new Object[] { mimeType, dateiname });
			final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_MIMETYPE.getKuerzel(), benutzerUuid, protokollMessage);
			protokollService.protokollieren(ereignis);
		}
	}

}
