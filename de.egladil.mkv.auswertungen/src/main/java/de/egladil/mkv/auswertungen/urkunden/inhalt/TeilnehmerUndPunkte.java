//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.annotations.Antwortcode;
import de.egladil.mkv.persistence.annotations.Wertungscode;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;

/**
 * TeilnehmerUndPunkte
 */
public class TeilnehmerUndPunkte {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnehmerUndPunkte.class);

	private final String name;

	private final String punkte;

	private final int kaengurusprung;

	private String kuerzel;

	private final int punktzahl;

	private String zusatz;

	private final Klassenstufe klassenstufe;

	private String klassenname;

	private String schulname;

	private String teilnehmerKuerzel;

	private final Sprache sprache;

	@Antwortcode
	private String antwortcode;

	/** Aneinanderreihung von 12 bzw. 15 Bewertungen f,r,n (Beispiel 'fnnfffrrffrn') */
	@Wertungscode
	private String berechneterWertungscode;

	/**
	 * TeilnehmerUndPunkte
	 */
	public TeilnehmerUndPunkte(final String name, final String punkte, final int punktzahl, final int kaengurusprung,
		final Klassenstufe klassenstufe, final Sprache sprache) {
		if (klassenstufe == null) {
			throw new IllegalArgumentException("klassenstufe null");
		}
		if (StringUtils.isAnyBlank(new String[] { name, punkte })) {
			LOG.error("name={}, punkte={}", name, punkte);
			throw new IllegalArgumentException("name und punkte sind erforderlich");
		}
		this.punktzahl = punktzahl;
		if (kaengurusprung < 0) {
			throw new IllegalArgumentException("kaengurusprung muss größer oder gleich 0 sein.");
		}
		this.name = name;
		this.punkte = punkte;
		this.kaengurusprung = kaengurusprung;
		this.klassenstufe = klassenstufe;
		this.sprache = sprache;
	}

	public final String getName() {
		return name;
	}

	public final String getPunkte() {
		return punkte;
	}

	public final int getKaengurusprung() {
		return kaengurusprung;
	}

	void setKuerzel(final String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public String getKuerzel() {
		return this.kuerzel;
	}

	public final int getPunktzahl() {
		return punktzahl;
	}

	public final void setZusatz(final String zusatz) {
		this.zusatz = zusatz;
	}

	public final String getZusatz() {
		return zusatz;
	}

	public final Klassenstufe getKlassenstufe() {
		return klassenstufe;
	}

	public final String getKlassenname() {
		return StringUtils.isNoneBlank(klassenname) ? klassenname : klassenstufe.getLabel();
	}

	public final void setKlassenname(final String klassenname) {
		this.klassenname = klassenname;
	}

	public final String getTeilnehmerKuerzel() {
		return teilnehmerKuerzel;
	}

	public final void setTeilnehmerKuerzel(final String teilnehmerKuerzel) {
		this.teilnehmerKuerzel = teilnehmerKuerzel;
	}

	public final String getSchulname() {
		return schulname;
	}

	public final void setSchulname(final String schulname) {
		this.schulname = schulname;
	}

	public final String getAntwortcode() {
		return antwortcode;
	}

	public final void setAntwortcode(final String antwortcode) {
		this.antwortcode = antwortcode;
	}

	public final String getBerechneterWertungscode() {
		return berechneterWertungscode;
	}

	public final void setBerechneterWertungscode(final String berechneterWertungscode) {
		this.berechneterWertungscode = berechneterWertungscode;
	}

	public final Sprache getSprache() {
		return sprache;
	}

}
