//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.io.IOException;
import java.text.MessageFormat;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.FontCalulator;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * TeilnehmerPunkteRenderer rendert die erreichte Punktzahl
 */
public class TeilnehmerPunkteRenderer implements UrkundeAbschnittRenderer {

	private static final String SATZENDE_DE = "erreicht";

	private static final String SATZENDE_EN = "has succesfully participated with a score of";

	private static final String MF_PATTERN_PUNKTE_DE = "{0} Punkte";

	private static final String MF_PATTERN_PUNKTE_EN = "{0} points";

	private final UrkundeLinePrinter linePrinter;

	private final FontCalulator fontCalculator;

	/**
	 * TeilnehmerPunkteRenderer
	 */
	public TeilnehmerPunkteRenderer() {
		linePrinter = new UrkundeLinePrinter();
		fontCalculator = new FontCalulator();
	}

	@Override
	public int printAbschnittAndShiftVerticalPosition(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		if (Sprache.en == urkundeInhalt.getSprache()) {
			return printAbschnittEnglisch(content, urkundeInhalt, differenceFromTopPositionPoints);
		}

		return printAbschnittDeutsch(content, urkundeInhalt, differenceFromTopPositionPoints);
	}

	private int printAbschnittDeutsch(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		int deltaY = differenceFromTopPositionPoints;
		final int fontSize = UrkundeConstants.SIZE_TEXT_NORMAL;
		final String text = MessageFormat.format(MF_PATTERN_PUNKTE_DE, new Object[] { urkundeInhalt.getPunkteText() });

		deltaY = fontCalculator.berechneDeltaY(text, deltaY, fontSize);
		linePrinter.printTextCenter(content, text, UrkundeConstants.getFontBlack(fontSize), deltaY);

		deltaY += UrkundeConstants.POINTS_BETWEEN_ROWS;
		// deltaY = berechneDeltaY(text, deltaY, UrkundeConstants.SIZE_TEXT_NORMAL);
		linePrinter.printTextCenter(content, SATZENDE_DE, UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL),
			deltaY);

		return deltaY;
	}

	private int printAbschnittEnglisch(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		int deltaY = differenceFromTopPositionPoints + UrkundeConstants.POINTS_BETWEEN_ROWS;
		final int fontSize = UrkundeConstants.SIZE_TEXT_NORMAL;
		linePrinter.printTextCenter(content, SATZENDE_EN, UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL),
			deltaY);


		final String text = MessageFormat.format(MF_PATTERN_PUNKTE_EN, new Object[] { urkundeInhalt.getPunkteText() });
//		deltaY = fontCalculator.berechneDeltaY(text, deltaY, fontSize);
		deltaY += UrkundeConstants.POINTS_BETWEEN_ROWS;

		linePrinter.printTextCenter(content, text, UrkundeConstants.getFontBlack(fontSize), deltaY);
		return deltaY;
	}

}
