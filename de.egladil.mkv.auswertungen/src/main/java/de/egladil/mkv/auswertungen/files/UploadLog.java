//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files;

import java.util.Optional;

import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;

/**
 * UploadLog enthält alle Informationen über einen Upload. apiResponsePayload ist das, was nach draußen gegeben werden kann.
 * upload sind die internen Informationen z.B. für den ADMIN-Upload.
 */
public class UploadLog {

	private final APIResponsePayload apiResponsePayload;

	private final Optional<Upload> upload;

	private final String message;

	/**
	 * Erzeugt eine Instanz von UploadLog.
	 *
	 * @param apiResponsePayload PublicMessage darf nicht null sein.
	 * @param upload Optional of Upload darf nicht null sein.
	 * @param mesage String eine Message mit Kontext
	 * @throws NullPointerException wenn einer der Parameter null ist.
	 */
	public UploadLog(final APIResponsePayload publicMessage, final Optional<Upload> upload, final String message) {
		if (publicMessage == null) {
			throw new NullPointerException("Parameter apiResponsePayload");
		}
		if (upload == null) {
			throw new NullPointerException("Parameter upload");
		}
		this.apiResponsePayload = publicMessage;
		this.upload = upload;
		this.message = message;
	}

	public APIResponsePayload getApiResponsePayload() {
		return apiResponsePayload;
	}

	public Optional<Upload> getUpload() {
		return upload;
	}

	public String getMessage() {
		return message;
	}
}
