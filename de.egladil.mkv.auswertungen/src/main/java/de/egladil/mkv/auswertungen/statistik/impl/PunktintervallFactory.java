//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.egladil.mkv.auswertungen.domain.Punktintervall;
import de.egladil.mkv.auswertungen.statistik.compare.PunktintervallDescendingSorter;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * PunktintervallFactory
 */
public abstract class PunktintervallFactory implements IPunktintervallFactory {

	/**
	 * Erzeugt die passende Factory.
	 *
	 * @param klassenstufe
	 * @return
	 */
	public static IPunktintervallFactory createFactory(final Klassenstufe klassenstufe, final String wettbewerbsjahr) {

		final int jahr = Integer.valueOf(wettbewerbsjahr);

		switch (klassenstufe) {
		case EINS:
			if (jahr < 2017) {
				return new PunktintervallKlasseZweiFactory();
			}
			return new PunktintervallKlasseEinsFactory();
		case ZWEI:
			return new PunktintervallKlasseZweiFactory();
		case IKID:
			return new PunktintervallInklusionFactory();
		default:
			throw new IllegalArgumentException("Keine Factory für Klassenstufe " + klassenstufe + " implementiert");
		}
	}

	/**
	 * Erzeugt eine Instanz von PunktintervallFactory
	 */
	PunktintervallFactory() {
	}

	/**
	 * @see de.egladil.mkv.auswertungen.statistik.impl.IPunktintervallFactory#getPunktintervalleDescending()
	 */
	@Override
	public List<Punktintervall> getPunktintervalleDescending() {
		final List<Punktintervall> result = new ArrayList<>();
		int minVal = 0;
		for (int i = 0; i < anzahlIntervalle(); i++) {
			minVal = i * 500;
			result.add(createPunktintervall(minVal));
		}
		Collections.sort(result, new PunktintervallDescendingSorter());
		return result;
	}

	/**
	 * Gibt die Anzahl der Intervalle zurück.
	 *
	 * @return
	 */
	protected abstract int anzahlIntervalle();

	/**
	 * Erzeugt das Punktintervall, das bei minVal beginnt.
	 *
	 * @param minVal
	 * @return PunktIntervall
	 * @throws IllegalArgumentException wenn minVal kleiner 0 oder größer als die maximal zu erreichende Punktzahl ist.
	 */
	protected abstract Punktintervall createPunktintervall(int minVal);

}
