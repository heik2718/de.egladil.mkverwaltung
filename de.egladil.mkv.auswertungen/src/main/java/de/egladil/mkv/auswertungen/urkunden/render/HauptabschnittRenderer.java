//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.FontCalulator;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * HauptabschnittRenderer rendert alles ausschließlich der Punkte
 */
public class HauptabschnittRenderer implements UrkundeAbschnittRenderer {

	private static final String HAUPTVERB_DE = "hat";

	private static final String HAUPTVERB_EN = "";

	private static final String UEBERSCHRIFT_DE = "Minikänguru";

	private static final String UEBERSCHRIFT_EN = "Minikangaroo";

	private final UrkundeLinePrinter linePrinter;

	private final FontCalulator fontCalculator;

	/**
	 * HauptabschnittRenderer
	 */
	public HauptabschnittRenderer() {
		linePrinter = new UrkundeLinePrinter();
		fontCalculator = new FontCalulator();
	}

	@Override
	public int printAbschnittAndShiftVerticalPosition(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		int deltaY = differenceFromTopPositionPoints;

		Font font = UrkundeConstants.getFont(UrkundeConstants.SIZE_TITLE, urkundeInhalt.getHeadlineColor());
		if (Sprache.en == urkundeInhalt.getSprache()) {
			linePrinter.printTextCenter(content, UEBERSCHRIFT_EN, font, deltaY);
		} else {
			linePrinter.printTextCenter(content, UEBERSCHRIFT_DE, font, deltaY);
		}

		String text = urkundeInhalt.getWettbewerbsjahr();
		deltaY = fontCalculator.berechneDeltaY(text, deltaY, UrkundeConstants.SIZE_TITLE);
		linePrinter.printTextCenter(content, text, font, deltaY);

		if (Sprache.en == urkundeInhalt.getSprache()) {
			text = HAUPTVERB_EN;
		} else {
			text = HAUPTVERB_DE;
		}
		deltaY += UrkundeConstants.POINTS_BETWEEN_PARAGRAPHS;
		deltaY = fontCalculator.berechneDeltaY(text, deltaY, UrkundeConstants.SIZE_TEXT_NORMAL);

		font = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL);
		linePrinter.printTextCenter(content, text, font, deltaY);

		deltaY += UrkundeConstants.POINTS_BETWEEN_PARAGRAPHS;

		List<String> lines = new LineWrapper(UrkundeConstants.TEILNEHMER_SIZES_DESCENDING).wrapp(urkundeInhalt.getTeilnehmername());
		int fontSize = new FontCalulator().maximaleFontSizeTeilnehmername(urkundeInhalt.getTeilnehmername());
		font = UrkundeConstants.getFontBlack(fontSize);
		for (int i = 0; i < lines.size(); i++) {
			final String line = lines.get(i);
			deltaY = fontCalculator.berechneDeltaY(line, deltaY, fontSize);
			linePrinter.printTextCenter(content, line, font, deltaY);
			// deltaY += UrkundeConstants.POINTS_BETWEEN_ROWS;
		}

		font = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_NORMAL);
		text = urkundeInhalt.getKlassenname();
		deltaY = fontCalculator.berechneDeltaY(text, deltaY, UrkundeConstants.SIZE_TEXT_NORMAL);
		linePrinter.printTextCenter(content, text, font, deltaY);

		fontSize = UrkundeConstants.SIZE_TEXT_NORMAL;
		text = urkundeInhalt.getSchulname();
		if (!StringUtils.isBlank(text)) {
			font = UrkundeConstants.getFontBlack(fontSize);
			lines = new LineWrapper(UrkundeConstants.SCHULNAME_SIZES_DESCENDING).wrapp(text);
			deltaY = fontCalculator.berechneDeltaY(text, deltaY, fontSize);
			for (int i = 0; i < lines.size(); i++) {
				final String line = lines.get(i);
				// deltaY = fontCalculator.berechneDeltaY(line, deltaY, fontSize);
				linePrinter.printTextCenter(content, line, font, deltaY);
				deltaY += UrkundeConstants.POINTS_BETWEEN_ROWS;
			}
		} else {
			// FIXME
			deltaY += UrkundeConstants.POINTS_OHNE_SCHULE;
		}

		deltaY += UrkundeConstants.POINTS_BETWEEN_PARAGRAPHS;
		return deltaY;
	}
}
