//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.util.List;

import de.egladil.mkv.persistence.utils.LengthTester;

/**
 * LineWrapper
 */
public class LineWrapper {

	private final Integer[] orderedFontSizes;

	private final LengthTester lengthTester;

	/**
	 * LineWrapper
	 */
	public LineWrapper(final Integer[] orderedFontSizes) {
		this.orderedFontSizes = orderedFontSizes;
		this.lengthTester = new LengthTester();
	}

	/**
	 * Bricht den gegebenen Namen um, wenn zu lang
	 *
	 * @param name
	 * @return List
	 */
	public final List<String> wrapp(final String name) {
//		lengthTester.checkTooLongAndThrow(name, orderedFontSizes[orderedFontSizes.length - 1]);
//		LineWrapStrategy strategy = null;
//		if (!lengthTester.needsWrapping(name, orderedFontSizes)) {
//			strategy = new InvariantWrapStrategy();
//		} else {
//			strategy = new SplittingWrapStrategy(orderedFontSizes);
//		}
//		return strategy.breakLines(name);
		return new InvariantWrapStrategy().breakLines(name);
	}
}
