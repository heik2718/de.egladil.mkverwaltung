//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;

/**
 * LoesungszettelComparator
 */
public class LoesungszettelComparator implements Comparator<ILoesungszettel> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(ILoesungszettel arg0, ILoesungszettel arg1) {
		return arg0.getPunkte() - arg1.getPunkte();
	}

}
