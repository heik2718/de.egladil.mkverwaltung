//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;

/**
 * TeilnehmerUndPunkteNameComparator
 */
public class TeilnehmerUndPunkteNameComparator implements Comparator<TeilnehmerUndPunkteLoesungszettelAdapter> {

	@Override
	public int compare(final TeilnehmerUndPunkteLoesungszettelAdapter arg0, final TeilnehmerUndPunkteLoesungszettelAdapter arg1) {

		final String ersterName = arg0.getName();
		final String zweiterName = arg1.getName();

		if (!ersterName.equals(zweiterName)) {
			return ersterName.compareToIgnoreCase(zweiterName);
		}
		final String ersterZusatz = arg0.getZusatz();
		final String zweiterZusatz = arg1.getZusatz();
		if (ersterZusatz == null && zweiterZusatz == null) {
			return 0;
		}
		if (ersterZusatz == null) {
			return -1;
		}
		if (zweiterZusatz == null) {
			return 1;
		}

		return ersterZusatz.compareToIgnoreCase(zweiterZusatz);
	}
}
