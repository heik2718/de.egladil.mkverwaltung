//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.render;

import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfContentByte;

import de.egladil.mkv.auswertungen.urkunden.inhalt.UrkundeInhalt;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.utils.UrkundeConstants;

/**
 * FooterRenderer rendert Datum und Unterzeichner
 */
public class FooterRenderer implements UrkundeAbschnittRenderer {

	private final UrkundeLinePrinter linePrinter;

	/**
	 * FooterRenderer
	 */
	public FooterRenderer() {
		linePrinter = new UrkundeLinePrinter();
	}

	@Override
	public int printAbschnittAndShiftVerticalPosition(final PdfContentByte content, final UrkundeInhalt urkundeInhalt,
		final int differenceFromTopPositionPoints) throws DocumentException, IOException {

		final Font font = UrkundeConstants.getFontBlack(UrkundeConstants.SIZE_TEXT_SMALL);
		int deltaY = UrkundeConstants.Y_TOP - UrkundeConstants.Y_BOTTOM;
		linePrinter.printTextLeft(content, urkundeInhalt.getDatum(), font, deltaY);

		deltaY += UrkundeConstants.POINTS_BETWEEN_ROWS;
		if (Sprache.en == urkundeInhalt.getSprache()) {
			linePrinter.printText(content, "(jury)", font, UrkundeConstants.X_RIGHT, deltaY);
		} else {
			linePrinter.printText(content, "(Jury)", font, UrkundeConstants.X_RIGHT, deltaY);
		}

		return deltaY;
	}
}
