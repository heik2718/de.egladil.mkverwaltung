//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik;

import java.util.List;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * IVerteilungGeneratorerstellt die Gesamtpunktverteilung.
 */
public interface IVerteilungGenerator {

	/**
	 * Erzeugt für die gegebenen Lösungszettel der gegebenen Klassenstufe die Daten der Gesamtpunktverteilung.
	 *
	 * @param loesungszettel List darf nicht null sein.
	 * @param klassenstufe Klassenstufe darf nicht null sein.
	 * @param jahr String das Wettbewerbsjahr
	 * @return GesamtpunktverteilungDaten
	 * @throws NullPointerException wenn einer der Parameter null ist.
	 */
	GesamtpunktverteilungDaten berechneGesamtpunktverteilungDaten(List<ILoesungszettel> loesungszettel, Klassenstufe klassenstufe,
		String jahr);

	/**
	 * Berechnet den Median über die gegebenen Lösungszettel.
	 *
	 * @param loesungszettel List
	 * @return String auf 3 Nachkommastellen gerundet mit Komma (kein Dezimalpunkt).
	 */
	String berechneMedian(List<ILoesungszettel> loesungszettel);
}
