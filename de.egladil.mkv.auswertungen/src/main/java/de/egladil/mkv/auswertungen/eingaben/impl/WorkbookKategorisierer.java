//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

import java.util.Arrays;
import java.util.List;

import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;

/**
 * Der WorkbookKategorisierer kategorisiert ein gegebenes Workbook.
 */
public class WorkbookKategorisierer {

	private static final List<String> EINDEUTIGE_ANTWORTBUCHSTABEN = Arrays.asList(new String[] { "A", "a", "B", "b", "C", "c" });

	private static final List<String> EINDEUTIGE_WERTUNGSBUCHSTABEN = Arrays.asList(new String[] { "R", "r", "F", "f" });

	/**
	 * Ermittelt aus der gegebenen row die WorkbookKategorie.
	 *
	 * @param row MKAuswertungRow
	 * @return WorkbookKategorie
	 */
	WorkbookKategorie errateKategorie(MKAuswertungRow row) {
		String nutzereingaben = row.toNutzereingabe();
		for (String antwortbuchstabe : EINDEUTIGE_ANTWORTBUCHSTABEN) {
			if (nutzereingaben.contains(antwortbuchstabe)) {
				return WorkbookKategorie.ANTWORTBUCHSTABEN;
			}
		}
		for (String wertungsbuchstabe : EINDEUTIGE_WERTUNGSBUCHSTABEN) {
			if (nutzereingaben.contains(wertungsbuchstabe)) {
				return WorkbookKategorie.WERTUNGSBUCHSTABEN;
			}
		}
		return WorkbookKategorie.NICHT_ENTSCHEIDBAR;
	}

	/**
	 * Ermittelt für das gegebene Workbook die Kategorie.
	 *
	 * @param workbook MKAuswertungWorkbook
	 * @return WorkbookKategorie
	 */
	public WorkbookKategorie errateKategorie(MKAuswertungWorkbook workbook) {
		for (MKAuswertungRow row : workbook.getRows()) {
			WorkbookKategorie kategorie = errateKategorie(row);
			if (kategorie != WorkbookKategorie.NICHT_ENTSCHEIDBAR) {
				return kategorie;
			}
		}
		return WorkbookKategorie.NICHT_ENTSCHEIDBAR;
	}
}
