//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.urkunden.inhalt;

import java.io.InputStream;

import com.itextpdf.text.BaseColor;

import de.egladil.mkv.persistence.utils.UrkundenmotivProvider;

/**
 * UrkundenmotivMitText stellt den UrkundenmotivProvider und die allen Urkunden für diese Schule gemeinsamen Texte zur
 * Verfügung.
 */
public final class UrkundenmotivMitText {

	private final String wettbewerbsjahr;

	private final UrkundenmotivProvider motivProvider;

	private final String datum;

	/**
	 * UrkundenmotivMitText
	 */
	UrkundenmotivMitText(final String wettbewerbsjahr, final String schulname, final UrkundenmotivProvider motivProvider,
		final String datum) {
		this.wettbewerbsjahr = wettbewerbsjahr;
		this.motivProvider = motivProvider;
		this.datum = datum;
	}

	public final String getWettbewerbsjahr() {
		return wettbewerbsjahr;
	}

	public InputStream getBackgroundImage() {
		return motivProvider.getBackgroundImage();
	}

	public BaseColor getHeadlineColor() {
		return motivProvider.getHeadlineColor();
	}

	public final String getDatum() {
		return datum;
	}
}
