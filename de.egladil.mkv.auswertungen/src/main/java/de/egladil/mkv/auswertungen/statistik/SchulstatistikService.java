//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik;

import java.util.Optional;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * SchulstatistikService
 */
public interface SchulstatistikService {

	/**
	 * Löscht den gegebenen Auswertungdownload. Exceptions werden ins log geschrieben.
	 *
	 * @param benutzerUuid String die UUId des Benutzers
	 * @param download AuswertungDownload
	 */
	void deleteDownloadQietly(String benutzerUuid, AuswertungDownload download);

	/**
	 *
	 * @param downloadcode String
	 * @return Optional
	 */
	Optional<AuswertungDownload> findDownload(String downloadcode);

	/**
	 * Generiert die Schulstatistik für eine Rootgruppe. Diese gibt es nur im aktuellen Jahr.
	 *
	 * @param benutzerUuid String die uuid des Benutzers fürs LOG
	 * @param kuerzelRootgruppe String das kuerzel der Auswertungsgruppe (Schule)
	 * @return Optional ein downloadcode für auswertungdownloads
	 */
	Optional<String> generiereStatistikAktuellesJahr(String benutzerUuid, String kuerzelRootgruppe);

	/**
	 * Generiert die Schulstatistik für die gegebene Teilnahme.
	 *
	 * @param benutzerUuid String fürs LOG
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param schulname String der Name der Schule.
	 * @return String einen downloadcode für auswertungdownloads
	 * @throws PreconditionFailedException falls es keine Lösungszettel gibt.
	 */
	Optional<String> generiereSchulstatistik(String benutzerUuid, TeilnahmeIdentifier teilnahmeIdentifier, String schulname)
		throws PreconditionFailedException;

}
