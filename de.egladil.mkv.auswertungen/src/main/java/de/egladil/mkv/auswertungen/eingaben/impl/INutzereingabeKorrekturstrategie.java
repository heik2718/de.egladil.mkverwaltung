//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben.impl;

/**
 * INutzereingabeKorrekturstrategie
 */
public interface INutzereingabeKorrekturstrategie {

	/**
	 * Je nach Implementierung wird die gegebene Nutzereingabe in eine bereinigte Nutzereingabe umgewandelt.
	 *
	 * @param nutzereingabe String kommaseparierte Benutzereingabe oder einzelnes Zeichen. Darf nicht null sein.
	 * @return String kommaseparierte korrigierte Benutzereingabe.
	 */
	String korrigiere(String nutzereingabe);
}
