//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.domain.RohpunktItem;
import de.egladil.mkv.auswertungen.urkunden.UebersichtFontProvider;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * PunkteMitAnzahlPDFGenerator
 */
public class PunkteMitAnzahlPDFGenerator {

	private final UebersichtFontProvider fontProvider = new UebersichtFontProvider();

	private final PdfCellRenderer cellRendererRight = new PdfCellRightTextRenderer();

	private final PdfCellRenderer cellRendererCenter = new PdfCellCenteredTextRenderer();

	/**
	 * Generiert eine Tabelle mit den Aufgabenergebnissen.
	 *
	 * @param aufgabenergebnisse
	 * @return
	 */
	public byte[] generiere(final GesamtpunktverteilungDaten daten, final String klassenname) {

		final Document doc = new Document(PageSize.A4);
		final int numCols = 2;

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

			PdfWriter.getInstance(doc, out);
			doc.open();

			final PdfPTable table = new PdfPTable(numCols);

			final Font font = fontProvider.getFontBold();
			final PdfPCell cell = cellRendererCenter.createCell(font, "Punktverteilung " + klassenname, numCols);
			table.addCell(cell);

			addHeaders(table);

			for (final RohpunktItem rpi : daten.getRohpunktItemsSorted()) {
				addRow(table, rpi);
			}

			doc.add(table);
			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}

	private void addHeaders(final PdfPTable table) {
		final Font font = fontProvider.getFontBold();

		final String[] headings = new String[] { "Punkte", "Anzahl Kinder" };

		for (final String text : headings) {
			final PdfPCell cell = cellRendererCenter.createCell(font, text, 1);
			table.addCell(cell);
		}
	}

	private void addRow(final PdfPTable table, final RohpunktItem item) {
		final Font font = fontProvider.getFontNormal();
		final String punkteText =  item.getPunkteText();

		PdfPCell cell = cellRendererRight.createCell(font, punkteText, 1);
		table.addCell(cell);

		cell = cellRendererRight.createCell(font, String.valueOf(item.getAnzahl()), 1);
		table.addCell(cell);
	}
}
