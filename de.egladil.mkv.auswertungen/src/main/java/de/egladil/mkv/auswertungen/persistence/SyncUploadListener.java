//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.persistence;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.email.service.IMailservice;
import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;

/**
 * SyncUploadListener
 */
@Singleton
public class SyncUploadListener implements IUploadListener {

	private final ILoesungszettelDao loesungszettelDao;

	private final IWettbewerbsloesungDao wettbewerbsloesungDao;

	private final IUploadDao uploadDao;

	private final IUploadInfoDao uploadInfoDao;

	private final IMailservice mailService;

	private final AuswertungstabelleFileWriter fileWriter;

	/**
	 * Erzeugt eine Instanz von SyncUploadListener
	 */
	@Inject
	public SyncUploadListener(final ILoesungszettelDao loesungszettelDao, final IWettbewerbsloesungDao wettbewerbsloesungDao,
		final IUploadDao uploadDao, final IUploadInfoDao uploadInfoDao, final IMailservice mailService, final AuswertungstabelleFileWriter fileWriter) {
		super();
		this.loesungszettelDao = loesungszettelDao;
		this.wettbewerbsloesungDao = wettbewerbsloesungDao;
		this.uploadDao = uploadDao;
		this.uploadInfoDao = uploadInfoDao;
		this.mailService = mailService;
		this.fileWriter = fileWriter;
	}

	@Override
	public void processUpload(final Long uploadId, final String uploadDirPath) {
		final UploadProcessor uploadProcessor = new UploadProcessor(loesungszettelDao, wettbewerbsloesungDao, uploadDao,
			uploadInfoDao, mailService, fileWriter);
		uploadProcessor.processUploadAndExceptions(uploadId, uploadDirPath);
	}

}
