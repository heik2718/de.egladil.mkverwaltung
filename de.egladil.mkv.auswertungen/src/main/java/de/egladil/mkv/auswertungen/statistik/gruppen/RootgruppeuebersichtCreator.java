//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

import java.util.List;

import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkte;
import de.egladil.mkv.auswertungen.urkunden.inhalt.TeilnehmerUndPunkteLoesungszettelAdapter;

/**
 * Erzeugt aus einer Liste von TeilnehmerUndPunkte-Objekten ein Rootgruppeuebersicht-Objekt.
 */
public class RootgruppeuebersichtCreator {

	/**
	 * @param alleTeilnehmer List
	 * @return Rootgruppeuebersicht
	 */
	public Rootgruppeuebersicht createSchuluebersicht(final List<TeilnehmerUndPunkte> alleTeilnehmer, final String schulname) {

		final Rootgruppeuebersicht rootgruppeuebersicht = new Rootgruppeuebersicht(schulname);
		for (final TeilnehmerUndPunkte tup : alleTeilnehmer) {
			final Klassenschluessel klassenschluessel = new Klassenschluessel(tup.getKlassenstufe(), tup.getKlassenname());
			rootgruppeuebersicht.putSchuelerin(klassenschluessel, new TeilnehmerUndPunkteLoesungszettelAdapter(tup));
		}
		return rootgruppeuebersicht;
	}
}
