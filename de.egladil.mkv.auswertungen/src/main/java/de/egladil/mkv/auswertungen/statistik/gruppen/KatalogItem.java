//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

/**
 * KatalogItem ist ein fachliches Schlüsselelement für Gruppierungen.
 */
public class KatalogItem {

	private final Katalogart katalogart;

	private final String kuerzel;

	private final String name;

	/**
	 * KatalogItem
	 */
	public KatalogItem(final Katalogart katalogart, final String kuerzel, final String name) {
		this.katalogart = katalogart;
		this.kuerzel = kuerzel;
		this.name = name;
	}

	public final Katalogart getKatalogart() {
		return katalogart;
	}

	public final String getKuerzel() {
		return kuerzel;
	}

	public final String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((katalogart == null) ? 0 : katalogart.hashCode());
		result = prime * result + ((kuerzel == null) ? 0 : kuerzel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final KatalogItem other = (KatalogItem) obj;
		if (katalogart != other.katalogart) {
			return false;
		}
		if (kuerzel == null) {
			if (other.kuerzel != null) {
				return false;
			}
		} else if (!kuerzel.equals(other.kuerzel)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("KatalogItem [katalogart=");
		builder.append(katalogart);
		builder.append(", kuerzel=");
		builder.append(kuerzel);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}
}
