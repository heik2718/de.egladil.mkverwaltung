//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.gruppen;

/**
 * Katalogart
 */
public enum Katalogart {

	LAND,
	ORT,
	SCHULE

}
