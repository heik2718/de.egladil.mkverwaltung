//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.files.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.ODFNotOfficeXmlFileException;
import org.apache.poi.openxml4j.exceptions.OLE2NotOfficeXmlFileException;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.domain.MKAuswertungCell;
import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.MKAuswertungWorkbook;
import de.egladil.mkv.auswertungen.domain.MKCellAddress;
import de.egladil.mkv.auswertungen.exceptions.UnprocessableAuswertungException;
import de.egladil.mkv.auswertungen.files.IAuswertungStreamReader;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.exceptions.MKAuswertungenException;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AbstractAuswertungExcelStreamReader arbeitet auf dem Workbook.
 */
public abstract class AbstractAuswertungExcelStreamReader implements IAuswertungStreamReader {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractAuswertungExcelStreamReader.class);

	@Override
	public MKAuswertungWorkbook readAuswertung(final InputStream in, final int maxLengthExtracted)
		throws IOException, MKAuswertungenException, UnprocessableAuswertungException {
		try (Workbook workbook = getWorkbook(in)) {
			final MKAuswertungWorkbook result = new MKAuswertungWorkbook();
			final Iterator<Sheet> iter = workbook.sheetIterator();
			int index = 0;
			while (iter.hasNext()) {
				final Sheet sheet = iter.next();
				if (!POIAssistant.isEmptySheet(sheet)) {
					final List<MKAuswertungRow> rows = generateTable(sheet, index++);
					if (!rows.isEmpty()) {
						result.addAll(rows);
					}
				}
			}
			LOG.debug("MKAuswertungWorkbook mit {} Zeile(n) generiert.", result.size());
			return result;
		} catch (OfficeXmlFileException | OLE2NotOfficeXmlFileException | ODFNotOfficeXmlFileException e) {
			throw new UnprocessableAuswertungException("Auswertung vom falschen Typ: " + e.getMessage(), e);
		}
	}

	/**
	 * Holt das Workbook raus.
	 *
	 * @param in InputStream darf nicht null sein.
	 * @return Workbook;
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	protected abstract Workbook getWorkbook(InputStream in) throws IllegalArgumentException, IOException, OfficeXmlFileException,
		OLE2NotOfficeXmlFileException, ODFNotOfficeXmlFileException;

	/**
	 * Aus dem Sheet wird eine verwertbare MKAuswertungTable gebastelt.
	 *
	 * @param sheet
	 * @param index
	 * @return MKAuswertungTable
	 * @throws UnprocessableAuswertungException
	 */
	List<MKAuswertungRow> generateTable(final Sheet sheet, final int index) throws UnprocessableAuswertungException {
		final Klassenstufe klassenstufe = POIAssistant.getKlasse(sheet);
		if (klassenstufe == null) {
			throw new MKAuswertungenException("An dieser Stelle darf die Klassenstufe nicht null sein");
		}
		final CellAddress startAddress = POIAssistant.findAddressWithStringContents(sheet, STARTADDRESS_MARKER);
		final Iterator<Row> iter = sheet.rowIterator();
		final List<MKAuswertungRow> rows = new ArrayList<>();
		int i = index;
		while (iter.hasNext()) {
			final Row row = iter.next();
			if (POIAssistant.isDatenRow(row)) {
				try {
					final MKAuswertungRow mkRow = generateRow(row, startAddress.getColumn(), klassenstufe, i++);
					if (mkRow != null) {
						rows.add(mkRow);
					}
				} catch (final NullPointerException e) {
					LOG.error("Fehlerhafte Zeile: {}", row.toString());
					throw new UnprocessableAuswertungException("NPE bei row " + i + ": " + e.getMessage());
				}

			}
		}
		LOG.debug("{} Zeilen generiert.", rows.size());
		return rows;
	}

	/**
	 * Generiert eine MKAuswertungRow.
	 *
	 * @param row Row
	 * @param startAddress CellAddress
	 * @param klassenstufe Klassenstufe
	 * @param index int
	 * @return MKAuswertungRow oder null.
	 * @throws NullPointerException, wenn in einer Zelle einer Zeile etwas falsches drinsteht.
	 */
	private MKAuswertungRow generateRow(final Row row, final int anzahlNamensfelder, final Klassenstufe klassenstufe, final int index) {
		final MKAuswertungRow result = new MKAuswertungRow(index, klassenstufe, anzahlNamensfelder);
		final Iterator<Cell> iter = row.cellIterator();
		int maxCol = 0;
		switch (klassenstufe) {
		case IKID:
			maxCol = anzahlNamensfelder + 12;
			break;
		case EINS:
			maxCol = anzahlNamensfelder + 24;
			break;
		case ZWEI:
			maxCol = anzahlNamensfelder + 30;
			break;
		default:
			throw new MKVException("noch nicht eingebundene Klassenstufe " + klassenstufe + " gefunden.");
		}
		int cellIndex = 0;
		int actCol = 0;
		while (iter.hasNext() && actCol < maxCol) {
			final Cell cell = iter.next();
			actCol = cell.getAddress().getColumn();
			if (POIAssistant.isTextCell(cell)) {
				final MKAuswertungCell zelle = generateMKCell(cell);
				try {
					result.addCell(cellIndex, zelle);
				} catch (final IllegalArgumentException e) {
					final String message = "Fehler in Zelle " + cell.getAddress() + ": " + e.getMessage();
					throw new UnprocessableAuswertungException(message, e);
				}
				cellIndex++;
			}
		}
		checkRow(result);
		if (result.isEmpty()) {
			return null;
		}
		return result;
	}

	private void checkRow(final MKAuswertungRow row) throws UnprocessableAuswertungException {
		if (row != null) {
			int index = 0;
			for (final MKAuswertungCell cell : row.showCells()) {
				if (cell == null) {
					final String message = "UnprocessableAuswertung: Zeile " + row.getIndex() + " enthält ungültige Eingaben."
						+ index;
					throw new UnprocessableAuswertungException(message);
				}
				index++;
			}
		}
	}

	/**
	 * TODO
	 *
	 * @param cell
	 * @return
	 */
	private MKAuswertungCell generateMKCell(final Cell cell) {
		final MKAuswertungCell zelle = new MKAuswertungCell(new MKCellAddress(cell.getRowIndex(), cell.getColumnIndex()),
			POIAssistant.getCellValue(cell));
		return zelle;
	}
}
