//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.impl;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.statistik.compare.LoesungszettelComparator;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;

/**
 * BerechneMedianCommand berechnet den Median der Punkte.
 */
public class BerechneMedianCommand {

	private static final Logger LOG = LoggerFactory.getLogger(BerechneMedianCommand.class);

	/**
	 * @param alleLoesungszettel
	 * @return double
	 * @throws NullPointerException wenn Parameter null
	 * @throws UnsupportedOperationException wenn alleLoesungszettel leer.
	 */
	public double berechne(List<ILoesungszettel> alleLoesungszettel) {
		if (alleLoesungszettel == null) {
			throw new NullPointerException("Parameter alleLoesungszettel");
		}
		if (alleLoesungszettel.isEmpty()) {
			throw new UnsupportedOperationException("alleLoesungszettel darf nicht leer sein");
		}

		Collections.sort(alleLoesungszettel, new LoesungszettelComparator());
		LOG.debug("kleinster Wert: {}, größter Wert: {}", alleLoesungszettel.get(0),
			alleLoesungszettel.get(alleLoesungszettel.size() - 1));

		int anzahl = alleLoesungszettel.size();
		double result = 0;

		if (anzahl % 2 == 0) {
			int erstes = alleLoesungszettel.get(alleLoesungszettel.size() / 2 - 1).getPunkte();
			int zweites = alleLoesungszettel.get(alleLoesungszettel.size() / 2).getPunkte();

			LOG.debug("Median zwischen {} und {}", erstes, zweites);
			result = Double.valueOf((erstes + zweites)) / 200;
		} else {
			int index = Math.floorDiv(anzahl, 2);
			LOG.debug("Median bei index {}", index);
			result = Double.valueOf(alleLoesungszettel.get(index).getPunkte()) / 100;
		}

		return result;
	}
}
