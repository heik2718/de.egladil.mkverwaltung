//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.statistik.compare;

import java.util.Comparator;

import de.egladil.mkv.auswertungen.domain.Punktintervall;

/**
 * PunktintervallDescendingSorter sortiert die Punktintervalle absteigend.
 */
public class PunktintervallDescendingSorter implements Comparator<Punktintervall> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Punktintervall arg0, Punktintervall arg1) {
		return arg0.compareTo(arg1) * (-1);
	}
}
