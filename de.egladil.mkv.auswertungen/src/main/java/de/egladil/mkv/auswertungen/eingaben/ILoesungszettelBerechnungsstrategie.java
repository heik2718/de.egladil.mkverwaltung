//=====================================================
// Projekt: de.egladil.mkv.auswertungen
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.eingaben;

import de.egladil.mkv.auswertungen.domain.MKAuswertungRow;
import de.egladil.mkv.auswertungen.domain.WorkbookKategorie;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;

/**
 * ILoesungszettelBerechnungsstrategie ist ein Algorithmus, der aus einer MKAuswertungRow die Rohdaten eines
 * Lösungszettels erzeugt.
 */
public interface ILoesungszettelBerechnungsstrategie {

	/**
	 * Berechnet aus einer MKAuswertungRow die Rohdaten eines Loesungszettels.
	 *
	 * @param auswertungRow MKAuswertungRow darf nicht null sein
	 * @return LoesungszettelRohdaten
	 * @throws NullPointerException wenn auswertungRow null
	 */
	LoesungszettelRohdaten berechneRohdaten(MKAuswertungRow auswertungRow);

	/**
	 * Gibt die zugehörige WorkbokkKategorie zurück. Das erleichtert vor allem Tests.
	 *
	 * @return WorkbookKategorie
	 */
	WorkbookKategorie getCorrespondingWorkbookKategorie();
}
