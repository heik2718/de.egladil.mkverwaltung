//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.auswertungen.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * MKAuswertungRow ist eine Zeile in einer Minikänguru-Auswertunstabelle, die zu einer Wertung gehört.
 */
public class MKAuswertungRow implements Comparable<MKAuswertungRow> {

	private int index;

	private MKAuswertungCell[] cells;

	private final int anzahlNamenfelder;

	private final Klassenstufe klassenstufe;

	public MKAuswertungRow(int index, Klassenstufe klassenstufe, int anzahlNamenfelder) {
		this.index = index;
		this.anzahlNamenfelder = anzahlNamenfelder;
		this.klassenstufe = klassenstufe;
		cells = new MKAuswertungCell[anzahlNamenfelder + klassenstufe.getAnzahlAufgaben()];
	}

	public void addCell(int index, MKAuswertungCell cell) {
		if (index < 0 || index >= cells.length) {
			throw new IllegalArgumentException (
				"index muss kleiner als " + cells.length + " und größer als 0 sein. [index=" + index + "]");
		}
		cells[index] = cell;
	}

	/**
	 * Gibt den Inhalt der Zelle mit Index index zurück.
	 *
	 * @param index int muss zwischen 0 und cells.length liegen.
	 * @return Optional<String>
	 * @throws IndexOutOfBoundsException
	 */
	public Optional<String> getCellContent(int index) throws IllegalArgumentException {
		if (index < 0 || index >= cells.length) {
			throw new IndexOutOfBoundsException(
				"index muss kleiner als " + cells.length + " und größer als 0 sein. [index=" + index + "]");
		}
		return cells[index].getContents();
	}

	/**
	 *
	 * @return
	 * @throws NullPointerException
	 */
	public boolean isEmpty() {
		MKAuswertungCell[] wertungszellen = toWertungsrelevanteZellen();
		for (int i = 0; i < wertungszellen.length; i++) {
			final MKAuswertungCell mkCell = wertungszellen[i];
			if (mkCell != null && !mkCell.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MKAuswertungRow other = (MKAuswertungRow) obj;
		if (index != other.index)
			return false;
		return true;
	}

	public String prettyPrint() {
		StringBuffer sb = new StringBuffer();
		String num = "" + index;
		num = StringUtils.leftPad(num, 3);
		sb.append(num);
		sb.append(" : ");
		for (MKAuswertungCell zelle : cells) {
			if (zelle == null) {
				sb.append("-");
			} else {
				sb.append(zelle.getContents().orElse("null"));
			}
			sb.append(" ");
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		List<String> cellsStrings = new ArrayList<>();
		for (MKAuswertungCell cell : cells) {
			cellsStrings.add(cell.toString());
		}
		return PrettyStringUtils.collectionToString(cellsStrings, ",");
	}

	/**
	 * Gibt eine Kopie aller Zellen zurück.<br>
	 * <br>
	 * <strong>Achtung: </strong> Es können auch null- Elemente enthalten sein.
	 *
	 * @return MKAuswertungCell[]
	 */
	public MKAuswertungCell[] showCells() {
		return cells.clone();
	}

	/**
	 * Gibt nur das Array der für die Auswertung relevanten Zellen zurück.<br>
	 * <br>
	 * <strong>Achtung: </strong> Es können auch null- Elemente enthalten sein.
	 *
	 * @return MKAuswertungCell[]
	 */
	MKAuswertungCell[] toWertungsrelevanteZellen() {
		MKAuswertungCell[] result = new MKAuswertungCell[klassenstufe.getAnzahlAufgaben()];
		for (int index = result.length - 1; index >= 0; index--) {
			result[index] = cells[index + anzahlNamenfelder];
		}
		return result;
	}

	/**
	 * Gibt die Nutzereingabe kommasepariert zurück. Das ist nur bei Uploads interessant, da online keine Tipfehler
	 * möglich sind.
	 *
	 * @throws NullPointerException
	 */
	public String toNutzereingabe() {
		List<String> cellsStrings = new ArrayList<>();
		int index = 0;
		for (MKAuswertungCell cell : toWertungsrelevanteZellen()) {
			if (cell == null) {
				String msg = "Fehler in Zeile " + index + ": Zelle " + index + " ist null";
				throw new NullPointerException(msg);
			}
			cellsStrings.add(cell.toString());
			index++;
		}
		return PrettyStringUtils.collectionToString(cellsStrings, ",");
	}

	/**
	 * Liefert die Membervariable klassenstufe
	 *
	 * @return die Membervariable klassenstufe
	 */
	public Klassenstufe getKlasse() {
		return klassenstufe;
	}

	/**
	 * Liefert die Membervariable anzahlNamenfelder
	 *
	 * @return die Membervariable anzahlNamenfelder
	 */
	public int getAnzahlNamenfelder() {
		return anzahlNamenfelder;
	}

	public int getIndex() {
		return index;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MKAuswertungRow arg0) {
		return this.index - arg0.getIndex();
	}
}
