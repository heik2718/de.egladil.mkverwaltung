import { Injectable } from '@angular/core';
import { FilesSelected, UploadFilesStatus } from './file-upload.model';
import { FileUploadConfig } from './file-upload-config';

@Injectable()
export class UploadUtilsService {

  constructor() { }


  public verifyFiles(files: FileList, config: FileUploadConfig): FilesSelected {
    const filesArray = Array.from(files);

    const maxFilesCount = config.maxFilesCount;
    const totalFilesSize = config.totalFilesSize;
    const acceptExtensions = config.acceptExtensions;

    if (filesArray.length > maxFilesCount) {
      return <FilesSelected>{
        status: UploadFilesStatus.STATUS_MAX_FILES_COUNT_EXCEED,
        files: filesArray
      };
    }

    const filesWithExceedSize = filesArray.filter((file: File) => file.size > config.maxFileSize);
    if (filesWithExceedSize.length) {
      return <FilesSelected>{
        status: UploadFilesStatus.STATUS_MAX_FILE_SIZE_EXCEED,
        files: filesWithExceedSize
      };
    }

    let filesSize = 0;
    filesArray.forEach((file: File) => filesSize += file.size);
    if (filesSize > totalFilesSize) {
      return <FilesSelected>{
        status: UploadFilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED,
        files: filesArray
      };
    }

    const filesNotMatchExtensions = filesArray.filter((file: File) => {
      const extensionsList = (acceptExtensions as string)
        .split(',')
        .map(extension => extension.slice(1))
        .join('|');

      const regexp = this.getRegExp(extensionsList);

      return !regexp.test(file.name);
    });

    if (filesNotMatchExtensions.length) {
      return <FilesSelected>{
        status: UploadFilesStatus.STATUS_NOT_MATCH_EXTENSIONS,
        files: filesNotMatchExtensions
      };
    }

    return <FilesSelected>{
      status: UploadFilesStatus.STATUS_SUCCESS,
      files: filesArray
    };
  }

  private getRegExp(extensions: string): RegExp {
    return new RegExp(`(.*?)\.(${extensions})$`);
  }
}


