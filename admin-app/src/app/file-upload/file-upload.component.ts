import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, DoCheck, EventEmitter, Output, Input } from '@angular/core';
import { FilesSelected } from './file-upload.model';
import { UploadUtilsService } from './upload-utils.service';
import { FileUploadConfig } from './file-upload-config';

@Component({
  selector: 'admin-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileUploadComponent implements OnInit, DoCheck {

  @Input()
  fileUplodConfig: FileUploadConfig;

  @Output()
  filesSelect: EventEmitter<FilesSelected> = new EventEmitter<FilesSelected>();



  constructor(private changeDetector: ChangeDetectorRef, private uploadUtils: UploadUtilsService) { }

  ngOnInit() {
  }

  ngDoCheck() {
    this.changeDetector.detectChanges();
  }


  public onChange(files: FileList): void {
    if (!files.length) {
      return;
    }

    this.filesSelect.emit(
      this.uploadUtils.verifyFiles(files, this.fileUplodConfig)
    );
  }
}


