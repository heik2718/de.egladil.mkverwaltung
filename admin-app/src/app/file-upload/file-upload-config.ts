export interface FileUploadConfig {
  acceptExtensions?: string[] | string;
  maxFilesCount?: number;
  maxFileSize?: number;
  totalFilesSize?: number;
}

export const fileUploadDefaultConfig: FileUploadConfig = {
  acceptExtensions: '*',
  maxFilesCount: Infinity,
  maxFileSize: Infinity,
  totalFilesSize: Infinity
};

