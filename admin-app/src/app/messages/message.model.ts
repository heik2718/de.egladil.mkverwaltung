
export const INFO = 'INFO';
export const WARN = 'WARN';
export const ERROR = 'ERROR';

export class Message {

  level: string;
  message: string;

  constructor(obj: any = {}) {
    this.level = obj && obj.level || null;
    this.message = obj && obj.message || null;
  }
}
