import {Action} from 'redux';

import {Message} from './message.model';
import * as MessageAction from './messages.actions';

export interface MessageState {
  currentMessage: Message;
}

const initialState: MessageState = {currentMessage: null};

export const MessagesReducer =

  function(state: MessageState = initialState, action: Action): MessageState {
    switch (action.type) {
      case MessageAction.SET_CURRENT_MESSAGE:
        const message: Message = (<MessageAction.SetCurrentMessageAction>action).message;
        return {
          currentMessage: message
        };
      case MessageAction.CLEAR_MESSAGE:
        return {
          currentMessage: null
        };
      default:
        return state;
    }
  };


export const getMessageState = (state): MessageState => state.currentMessage;
