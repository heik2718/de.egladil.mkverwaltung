import {Action, ActionCreator} from 'redux';
import {Message} from './message.model';

export const SET_CURRENT_MESSAGE = '[Messages] Set Current';
export const CLEAR_MESSAGE = '[Messages] clear';

export interface SetCurrentMessageAction extends Action {
  message: Message;
}

export interface ClearCurrentMessageAction extends Action {
  message: Message;
}

export const setCurrentMessage: ActionCreator<SetCurrentMessageAction> =
  (theMessage) => ({
    type: SET_CURRENT_MESSAGE,
    message: theMessage
  });

export const clearCurrentMessage: ActionCreator<ClearCurrentMessageAction> =
  (theMessage) => ({
    type: CLEAR_MESSAGE,
    message: theMessage
  });
