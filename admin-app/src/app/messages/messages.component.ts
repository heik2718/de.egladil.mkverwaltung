import { Component, OnInit, OnDestroy, Injectable, Inject } from '@angular/core';

// Infratructure
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';


import { AppStore } from '../app.store';
import { AppState } from '../app.reducer';
import { Message, INFO, WARN, ERROR } from '../messages/message.model';
import { CLEAR_MESSAGE } from '../messages/messages.actions';


@Component({
  selector: 'admin-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, OnDestroy {

  errorMessage: string;
  hasError: boolean;
  warningMessage: string;
  hasWarning: boolean;
  infoMessage: string;
  hasInfo: boolean;

  private _message: Message;

  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>, private _logger: LogService) {
    this._logger.debug('MessagesComponent created');
  }

  ngOnInit() {

    this._logger.debug('init messages component');

    this._unsubscribe = this._store.subscribe(() => {
      this._message = this._store.getState().message.currentMessage;
      this._reset();
      if (this._message !== null) {
        this._logger.debug(this._message.message);
        switch (this._message.level) {
          case INFO:
            if (this._message.message !== undefined) {
              if (this._message.message.length > 0) {
                this.infoMessage = this._message.message;
                this.hasInfo = true;
              } else {
                this._logger.debug('leere Infomessage bekommen');
              }
            }
            break;
          case WARN:
            this.warningMessage = this._message.message;
            this.hasWarning = true;
            break;
          case ERROR:
            this.errorMessage = this._message.message;
            this.hasError = true;
            break;
          default:
            break;
        }
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  clearMessage() {
    const action = {
      type: CLEAR_MESSAGE
    };
    this._store.dispatch(action);

  }

  private _reset(): void {
    this.errorMessage = null;
    this.hasError = false;
    this.warningMessage = null;
    this.hasWarning = false;
    this.infoMessage = null;
    this.hasInfo = false;
  }

}
