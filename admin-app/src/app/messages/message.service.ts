/**
 * Globaler Service, um Messages zu dispatchen
 */
import { Inject } from '@angular/core';
import { Store } from 'redux';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { clearCurrentMessage, setCurrentMessage } from '../messages/messages.actions';
import { Message } from '../messages/message.model';
import { SessionService } from '../login/session.service';



@Injectable()
export class MessageService {

  constructor(@Inject(AppStore) private _store: Store<AppState>,
    private _router: Router,
    private _sessionService: SessionService
  ) { }

  clearMessage(): void {
    this._store.dispatch(clearCurrentMessage(null));
  }

  setMessage(message: Message): void {
    this._store.dispatch(setCurrentMessage(message));
    if (message.message.indexOf('Timeout') > -1) {
      this._sessionService.ping();
      this._router.navigate(['/landing']);
    }
  }
}
