import { InjectionToken } from '@angular/core';
import { createStore, Store, compose, StoreEnhancer } from 'redux';
import { environment } from '../environments/environment';

import { AppState, default as reducer } from './app.reducer';

/**
 * chrome dev tools aktivieren, aber nur im nicht-Prod-Modus.
 */
const devtools: StoreEnhancer<AppState> =
  window['devToolsExtension'] && !environment.production ?
    window['devToolsExtension']() : f => f;

export const AppStore = new InjectionToken('App.store');

export function createAppStore(): Store<AppState> {
  return createStore<AppState>(
    reducer,
    compose(devtools)
  );
}

export const appStoreProviders = [
  { provide: AppStore, useFactory: createAppStore }
];


