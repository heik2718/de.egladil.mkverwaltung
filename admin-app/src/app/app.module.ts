// Modules
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HewiNgLibModule } from 'hewi-ng-lib';


// shared
import { environment } from '../environments/environment';
import { AppRoutingModule, appRouting } from './app-routing.module';
import { DownloadDelegate } from './shared/download.delegate';
import { FilterPipe } from './shared/filter.pipe';
import { LoggedInGuard } from './shared/loggedIn.guard';
import { MessageService } from './messages/message.service';
import { PagerService } from './shared/pager.service';

// Components and Services
import { AppComponent } from './app.component';
import { AufgabeErgebnisComponent } from './statistik/gesamtuebersicht/aufgabeErgebnis.component';
import { AuswertungsgruppeComponent } from './auswertungsgruppen/auswertungsgruppe.component';
import { AuswertungsgruppenListComponent } from './auswertungsgruppen/auswertungsgruppenList.component';
import { BenutzerListComponent } from './benutzer/benutzerList.component';
import { BenutzerOverviewComponent } from './benutzer/benutzerOverview.component';
import { BenutzerService } from './benutzer/benutzer.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorInterceptorProvider } from './shared/error.interceptor';
import { AuswertungstabellenComponent } from './file/auswertungstabellen.component';
import { GesamtpunktedatenComponent } from './statistik/gesamtuebersicht/gesamtpunktedaten.component';
import { GesamtpunktverteilungComponent } from './statistik/gesamtuebersicht/gesamtpunktverteilung.component';
import { KatalogeComponent } from './kataloge/kataloge.component';
import { KontextComponent } from './kontext/kontext.component';
import { LaenderListComponent } from './kataloge/laender/laenderList.component';
import { LaenderService } from './kataloge/laender/laender.service';
import { LandOverviewComponent } from './kataloge/laender/landOverview.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { MailComponent } from './mail/mail.component';
import { MessagesComponent } from './messages/messages.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NotFoundComponent } from './shared/not-found.component';
import { OrteListComponent } from './kataloge/orte/orteList.component';
import { OrtOverviewComponent } from './kataloge/orte/ortOverview.component';
import { PunktklasseComponent } from './statistik/gesamtuebersicht/punktklasse.component';
import { PunktverteilungComponent } from './statistik/gesamtuebersicht/punktverteilung.component';
import { RohpunktComponent } from './statistik/gesamtuebersicht/rohpunkt.component';
import { RootgruppeDetailsComponent } from './rootgruppe-details/rootgruppe-details.component';
import { SchulenListComponent } from './kataloge/schulen/schulenList.component';
import { SchuleOverviewComponent } from './kataloge/schulen/schuleOverview.component';
import { ShowErrorComponent } from './shared/showError.component';
import { StatistikComponent } from './statistik/statistik.component';
import { TeilnahmegruppeComponent } from './statistik/teilnahmegruppe.component';
import { TeilnahmegruppenListComponent } from './statistik/teilnahmegruppenlist.component';
import { TokenInterceptorProvider } from './shared/token.interceptor';
import { WettbewerbsjahrComponent } from './statistik/wettbewerbsjahr.component';

import { appStoreProviders } from './app.store';
import { auswertungsgruppenServiceInjectables } from './auswertungsgruppen/auswertungsgruppen.service';
import { sessionServiceInjectables } from './login/session.service';
import { OrteService } from './kataloge/orte/orte.service';
import { SchulenService } from './kataloge/schulen/schulen.service';
import { StatistikService } from './statistik/statistik.service';
import { AuswertungsstabelleDetailsComponent } from './file/auswertungsstabelle-details.component';
import { AuswertungstabellenService } from './file/auswertungstabellen.service';
import { AuswertungstabelleBackgroundDirective } from './file/auswertungstabelle-background.directive';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { UploadUtilsService } from './file-upload/upload-utils.service';
import { GlobalErrorHandler } from './error/error-handler.service';

@NgModule({
  declarations: [
    AppComponent,
    AufgabeErgebnisComponent,
    AuswertungsgruppenListComponent,
    AuswertungsgruppeComponent,
    BenutzerListComponent,
    BenutzerOverviewComponent,
    DashboardComponent,
    FilterPipe,
    GesamtpunktedatenComponent,
    GesamtpunktverteilungComponent,
    KatalogeComponent,
    KontextComponent,
    LaenderListComponent,
    LandOverviewComponent,
    LandingComponent,
    LoginComponent,
    MailComponent,
    MessagesComponent,
    NavbarComponent,
    NotFoundComponent,
    PunktklasseComponent,
    PunktverteilungComponent,
    RootgruppeDetailsComponent,
    RohpunktComponent,
    SchulenListComponent,
    SchuleOverviewComponent,
    ShowErrorComponent,
    StatistikComponent,
    TeilnahmegruppeComponent,
    TeilnahmegruppenListComponent,
    WettbewerbsjahrComponent,
    OrtOverviewComponent,
    OrteListComponent,
    AuswertungstabellenComponent,
    AuswertungsstabelleDetailsComponent,
    AuswertungstabelleBackgroundDirective,
    FileUploadComponent
  ],
  imports: [
    appRouting,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    HewiNgLibModule,
    NgbModule,
    NgbCollapseModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    appStoreProviders,
    AuswertungstabellenService,
    BenutzerService,
    ErrorInterceptorProvider,
    DownloadDelegate,
    LaenderService,
    LoggedInGuard,
    MessageService,
    OrteService,
    PagerService,
    auswertungsgruppenServiceInjectables,
    sessionServiceInjectables,
    TokenInterceptorProvider,
    SchulenService,
    StatistikService,
    UploadUtilsService,
    // GlobalErrorHandler,
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
  ],
  bootstrap: [AppComponent]
})


export class AppModule { }


