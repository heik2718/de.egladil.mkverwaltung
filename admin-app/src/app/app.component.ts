import {Component, Inject, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppState} from './app.reducer';
import {AppStore} from './app.store';
import {SessionToken} from './login/session.model';
import {SessionService} from './login/session.service';
import {setKontext} from './kontext/kontext.actions';
import {setCurrentSession} from './login/session.actions';


@Component({
  selector: 'admin-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'MKV-Admin-App';

  version = environment.version;
  envName = environment.envName;
  showEnv = !environment.production;
  api = environment.apiUrl;

  csrfToken: string;
  showCsrf = !environment.production;

  private _loggedIn: boolean;

  private _unsubscribe: Unsubscribe;

  @ViewChild('navbarToggler', { static: true }) navbarToggler: ElementRef;

  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private _router: Router
    , private _userService: SessionService
    , private _logger: LogService) {
  }

  ngOnInit() {
    this._logger.debug('App started');
    this.csrfToken = '';


    this._unsubscribe = this._store.subscribe(() => {
      this._logger.debug('state changed');
      this._readState();
    });

    // ping
    this._checkSession();
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  isLoggedIn(): boolean {
    return this._loggedIn;
  }

  gotoLogin(): void {
    this.collapseNav();
    this._router.navigate(['/login']);
  }

  logout() {
    this.collapseNav();
    this._router.navigate(['/landing']);
  }


  collapseNav() {
    if (this.navBarTogglerIsVisible()) {
      this._logger.debug('visible');
      this._logger.debug(JSON.stringify(this.navbarToggler.nativeElement));
      this.navbarToggler.nativeElement.click();
    }
  }


  private _checkSession() {
    this._logger.debug('this.csrfToken=' + this.csrfToken);
    if (!this.csrfToken || this.csrfToken.length === 0) {
      // resp is of type `HttpResponse<PingMessage>`
      this._userService.ping()
        .subscribe(resp => {
          // display its headers
          const keys = resp.headers.keys();
          const headers = keys.map(key =>
            this._logger.debug(key + ': ' + resp.headers.get(key))
          );
          const body = resp.body;
          const kontext = body.payload;
          const user = new SessionToken({'xsrfToken': kontext.xsrfToken});
          kontext.xsrfToken = '';
          this._store.dispatch(setCurrentSession(user));
          this._store.dispatch(setKontext(kontext));
        });

      this._userService.ping();
    }
  }

  private navBarTogglerIsVisible() {
    this._logger.debug('navBarTogglerIsVisible');
    return this.navbarToggler.nativeElement.offsetParent !== null;
  }


  private _readState(): void {
    const state = this._store.getState();
    const user = state.session.sessionToken;
    this._logger.debug('inside readState()');
    if (user !== null) {
      this.csrfToken = user.xsrfToken;
      this._loggedIn = user.isLoggedIn();
    } else {
      this._loggedIn = false;
      this.csrfToken = '';
    }
  }
}
