import { TestBed, inject } from '@angular/core/testing';

import { AuswertungstabellenService } from './auswertungstabellen.service';

describe('AuswertungstabellenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuswertungstabellenService]
    });
  });

  it('should be created', inject([AuswertungstabellenService], (service: AuswertungstabellenService) => {
    expect(service).toBeTruthy();
  }));
});
