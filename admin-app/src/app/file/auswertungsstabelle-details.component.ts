import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Auswertungstabelle } from './+state/file.model';
import { AuswertungstabellenService } from './auswertungstabellen.service';
import { MessageService } from '../messages/message.service';
import { FileUploadConfig } from '../file-upload/file-upload-config';
import { FilesSelected, UploadFilesStatus } from '../file-upload/file-upload.model';

@Component({
  selector: 'admin-auswertungsstabelle-details',
  templateUrl: './auswertungsstabelle-details.component.html',
  styleUrls: ['./auswertungsstabelle-details.component.css']
})
export class AuswertungsstabelleDetailsComponent implements OnInit {

  @Input() tabelle: Auswertungstabelle;

  @Output()
  tabelleSelect: EventEmitter<Auswertungstabelle> = new EventEmitter<Auswertungstabelle>();

  selectedFilesForUpload: any;

  hasError: boolean;

  errorMsg: string;

  uploading: boolean;

  acceptExtensions: string;

  fileUplodConfig: FileUploadConfig;

  constructor(private service: AuswertungstabellenService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.fileUplodConfig = {
      acceptExtensions: 'ods,xls,xlsx',
      maxFilesCount: 1,
      maxFileSize: 5120000,
      totalFilesSize: 10120000
    };
    this.tabelle.selected = false;
  }



  getTeilnahmeart(): string {
    if (this.tabelle.dateiInfo.teilnahmeart === 'S') {
      return 'Schule';
    }
    if (this.tabelle.dateiInfo.teilnahmeart === 'P') {
      return 'privat';
    }
  }

  downloadFile(): void {

    this.service.saveFile(this.tabelle.dateiInfo.dateiname);

  }

  public filesSelect(selectedFiles: FilesSelected): void {

    this.messageService.clearMessage();

    if (selectedFiles.status !== UploadFilesStatus.STATUS_SUCCESS) {
      this.hasError = true;
      switch (selectedFiles.status) {
        case UploadFilesStatus.STATUS_MAX_FILES_COUNT_EXCEED: this.errorMsg = 'zu viele'; break;
        case UploadFilesStatus.STATUS_MAX_FILE_SIZE_EXCEED: this.errorMsg = 'zu zu groß'; break;
        case UploadFilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED: this.errorMsg = 'zusammen zu groß'; break;
        case UploadFilesStatus.STATUS_NOT_MATCH_EXTENSIONS: this.errorMsg = 'nur xls, xlsx oder ods'; break;
      }

      this.selectedFilesForUpload = selectedFiles.status;
      return;
    }
    //    this.selectedFiles = Array.from(selectedFiles.files).map(file => file.name);
    this.selectedFilesForUpload = selectedFiles;
    this.uploading = true;
    this.service.korrigiereUpload(this.selectedFilesForUpload, this.tabelle.upload);
    this.uploading = false;
  }

  public showUploadBtn(): boolean {
    return !this.uploading && 'FEHLER' === this.tabelle.dateiInfo.uploadStatus;
  }

  toggleSelected() {
    this.tabelle.selected = !this.tabelle.selected;
    this.tabelleSelect.emit(this.tabelle);
  }
}

