import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Auswertungstabelle } from './+state/file.model';
import { AuswertungstabellenService } from './auswertungstabellen.service';
import { AppStore } from '../app.store';
import { AppState } from '../app.reducer';
import { Store, Unsubscribe } from 'redux';
import { clearAuswertungstabellen } from './+state/auswertungstabellen.actions';

@Component({
  selector: 'admin-auswertungstabellen',
  templateUrl: './auswertungstabellen.component.html',
  styleUrls: ['./auswertungstabellen.component.css']
})
export class AuswertungstabellenComponent implements OnInit, OnDestroy {

  auswertungstabellen: Auswertungstabelle[];

  selectedAuswertungstabellen: Auswertungstabelle[];

  loading: boolean;

  markiertText = '';

  private unsubscribe: Unsubscribe;

  constructor(@Inject(AppStore) private _store: Store<AppState>,
    private auswertungstabellenService: AuswertungstabellenService) { }

  ngOnInit() {

    this.loading = true;
    this.selectedAuswertungstabellen = [];

    this.unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this.auswertungstabellen = state.auswertungstabellen.auswertungstabellen;
      this.selectedAuswertungstabellen = [];
      this.loading = false;
      this.markiertText = this.getMarkiertText();
    });

    this.auswertungstabellenService.loadAuswertungstabellen();
  }

  ngOnDestroy() {
    this._store.dispatch(clearAuswertungstabellen([]));
    this.unsubscribe();
  }

  onTabelleSelected(tabelle: Auswertungstabelle) {
    if (tabelle === undefined) {
      return;
    }
    const _gruppe = this.auswertungstabellen.find(g => g.dateiInfo.dateiname === tabelle.dateiInfo.dateiname) as Auswertungstabelle;
    if (_gruppe !== undefined) {
      if (_gruppe.selected) {
        this.selectedAuswertungstabellen.push(_gruppe);
      } else {
        for (let i = 0; i < this.selectedAuswertungstabellen.length; i++) {
          if (this.selectedAuswertungstabellen[i].dateiInfo.dateiname === _gruppe.dateiInfo.dateiname) {
            this.selectedAuswertungstabellen.splice(i, 1);
            break;
          }
        }
      }
      this.markiertText = this.getMarkiertText();
    }
  }

  private getMarkiertText(): string {
    return this.selectedAuswertungstabellen.length + ' von ' + this.auswertungstabellen.length + ' markiert';
  }

  moveSelectedFilesOnServer(): void {

    if (this.selectedAuswertungstabellen.length > 0) {
      const payload = [];
      for (let i = 0; i < this.selectedAuswertungstabellen.length; i++) {
        const tabelle = this.selectedAuswertungstabellen[i];
        payload.push(tabelle.dateiInfo.dateiname);
      }

      this.auswertungstabellenService.moveFiles(payload);
    }
  }
}
