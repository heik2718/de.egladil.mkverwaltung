import { Injectable, Inject } from '@angular/core';
import { Store } from 'redux';

// own app
import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { ResponsePayload } from '../shared/responsePayload.model';

// infrastructure
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { LogService } from 'hewi-ng-lib';
import { Upload, Auswertungstabelle } from './+state/file.model';
import { loadAuswertungstabellen } from './+state/auswertungstabellen.actions';
import { DownloadDelegate } from '../shared/download.delegate';
import { MessageService } from '../messages/message.service';
import { FilesSelected } from '../file-upload/file-upload.model';
import { WARN, Message } from '../messages/message.model';

const BASE_URL = environment.apiUrl;

@Injectable()
export class AuswertungstabellenService {

  constructor(@Inject(AppStore) private _store: Store<AppState>,
    private messageService: MessageService,
    private _http: HttpClient,
    private _downloadDelegate: DownloadDelegate,
    private _logger: LogService) { }

  loadAuswertungstabellen(): void {

    const url = BASE_URL + '/uploads/contents';

    this._http.get(url)
      .subscribe((response: ResponsePayload<Auswertungstabelle[]>) => {
        this._logger.debug('AuswertungstabellenService: response ist da');
        const _auswertungstabellen = response.payload;
        this._store.dispatch(loadAuswertungstabellen(_auswertungstabellen));
      });
  }

  saveFile(dateiname: string) {
    this._logger.debug('saveFile ' + dateiname);
    const url = BASE_URL + '/uploads/' + dateiname;

    this._http.get(url, { responseType: 'blob' })
      .subscribe((res: Blob) => {
        const headerKeys = res.size;
        this._logger.debug(JSON.stringify(headerKeys));
        this._downloadDelegate.saveToFileSystem(res, dateiname);
      });
  }

  moveFiles(dateinamen: string[]): void {
    const url = BASE_URL + '/uploads/dateien';

    this._http.request<ResponsePayload<Auswertungstabelle[]>>('DELETE', url, { body: dateinamen })
      .subscribe(
        (data) => {
          const _auswertungstabellen = data.payload;
          this._store.dispatch(loadAuswertungstabellen(_auswertungstabellen));
          const _message: Message = data.apiMessage;
          this.messageService.setMessage(_message);
        },
        (_error) => {
          const actualState = this._store.getState().auswertungstabellen.auswertungstabellen;
          for (let i = 0; i < actualState.length; i++) {
            const tabelle = actualState[i];
            tabelle.selected = false;
          }
          this._store.dispatch(loadAuswertungstabellen(actualState));
        });
  }

  korrigiereUpload(selectedFiles: FilesSelected, upload: Upload) {
    const files = selectedFiles.files;
    this._logger.debug('jetzt file ' + files[0].name + ' hochladen');

    const url = BASE_URL + '/uploads/' + upload.jahr + '/' + upload.teilnahmeart
      + '/' + upload.kuerzel + '/' + upload.checksumme;
    const promise = this.makeFileRequest(files[0], url, 'PUT');
    promise.then(
      (data: ResponsePayload<Auswertungstabelle[]>) => {
        // data ist das, womit in makeFileRequest... onreadystatechange reslove aufgerufen wird
        if (data !== undefined) {
          const _auswertungstabellen = data.payload;
          this._store.dispatch(loadAuswertungstabellen(_auswertungstabellen));
          this.messageService.setMessage({
            level: data.apiMessage.level,
            message: data.apiMessage.message
          });
        }

      },
    ).catch((data: ResponsePayload<Auswertungstabelle[]>) => {
      // data ist das, womit in makeFileRequest... onreadystatechange reject aufgerufen wird
      this.messageService.setMessage({
        level: data.apiMessage.level,
        message: data.apiMessage.message
      });
    });
  }

  uploadForUser(selectedFiles: FilesSelected, benutzerUuid: string) {
    const files = selectedFiles.files;
    this._logger.debug('jetzt file ' + files[0].name + ' hochladen');

    const url = BASE_URL + '/uploads/' + benutzerUuid;
    const promise = this.makeFileRequest(files[0], url, 'POST');
    promise.then(
      (data: ResponsePayload<Auswertungstabelle[]>) => {
        // data ist das, womit in makeFileRequest... onreadystatechange reslove aufgerufen wird
        if (data !== undefined) {
          const _auswertungstabellen = data.payload;
          this._store.dispatch(loadAuswertungstabellen(_auswertungstabellen));
          this.messageService.setMessage({
            level: data.apiMessage.level,
            message: data.apiMessage.message
          });
        }
      },
    ).catch((data: ResponsePayload<Auswertungstabelle[]>) => {
      // data ist das, womit in makeFileRequest... onreadystatechange reject aufgerufen wird
      this.messageService.setMessage({
        level: data.apiMessage.level,
        message: data.apiMessage.message
      });
    });
  }

  private makeFileRequest(file: File, url: string, method: string): Promise<any> {

    const promise = new Promise((resolve, reject) => {

      const formData: any = new FormData();

      formData.append('file', file, file.name);

      const xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          const message = xhr.response.text;
          console.log(message);
          if (xhr.readyState === 4) {
            if (xhr.status >= 200 && xhr.status < 400) {
              resolve(JSON.parse(xhr.response));
              // der Parameter in reslove ist ein ResponsePayload und wird über promise.then() in die callback-function durchgereicht.

            } else {
              reject(JSON.parse(xhr.response));
            }
          }
        }
      };

      xhr.ontimeout = function () {
        const msg = {
          'level': WARN,
          'message': 'Timeout'
        } as Message;

        const responsePayload = {
          apiMessage: msg,
          payload: []
        } as ResponsePayload<Auswertungstabelle[]>;

        console.log('keine Ahnung, was ich damit anfangen soll: ' + JSON.stringify(responsePayload));
      };

      xhr.upload.addEventListener('loadstart', this.progressStartListener, false);
      xhr.upload.addEventListener('progress', this.progressListener, false);
      xhr.upload.addEventListener('load', this.transferCompleteListener, false);

      // const url = BASE_URL + '/uploads' + '/' + upload.jahr + '/' + upload.teilnahmeart
      //   + '/' + upload.kuerzel + '/' + upload.checksumme;

      xhr.open(method, url, true);

      const _user = this._store.getState().session.sessionToken;
      // // Hier keinen Content-Type- Header setzen. Das macht xhr automatisch korrekt!
      // //      xhr.setRequestHeader('Content-Type', 'multipart/form-data');
      xhr.setRequestHeader('Authorization', 'Bearer ' + _user.accessToken);
      xhr.setRequestHeader('X-XSRF-TOKEN', _user.xsrfToken);
      xhr.setRequestHeader('Accept', 'application/json, text/plain, */*');
      xhr.send(formData);
    });

    return promise;
  }




  // #############################################################################
  // die folgenden Listener verarbeiten lokale Ereignisse auf Browser-Ebene
  private progressListener(evt: ProgressEvent): void {
    // dieses event misst, wie lange der Browser lokal braucht, um die Datei einzulesen. Ist daher nur geeignet,
    // wenn man wirklich große Dateien einlesen will
    if (evt.lengthComputable) {
      const percentComplete = (evt.loaded / evt.total) * 100;
      // this._logger.debug('geladen: ' + percentComplete);
    }
  }

  private progressStartListener(_evt: ProgressEvent): void {
    // this._logger.debug('progress started');
  }

  private transferCompleteListener(_evt: ProgressEvent): void {
    // this._logger.debug('transfer completed');
  }
}
