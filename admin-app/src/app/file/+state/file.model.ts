

export class DateiInfo {
    dateiname: string;
    teilnahmeart: string;
    teilnahmekuerzel: string;
    uploadStatus: string;
    groesse: string;

    constructor(obj: any = {}) {
        this.dateiname = obj && obj.dateiname || null;
        this.teilnahmeart = obj && obj.teilnahmeart || null;
        this.teilnahmekuerzel = obj && obj.teilnahmekuerzel || null;
        this.uploadStatus = obj && obj.uploadStatus || null;
        this.groesse = obj && obj.groesse || null;
    }
}

export class Upload {
    kuerzel: string;
    teilnahmeart: string;
    mimetype: string;
    dateiname: string;
    jahr: string;
    checksumme: string;
    uploadStatus: string;

    constructor(obj: any = {}) {
        this.dateiname = obj && obj.dateiname || null;
        this.teilnahmeart = obj && obj.teilnahmeart || null;
        this.kuerzel = obj && obj.kuerzel || null;
        this.uploadStatus = obj && obj.uploadStatus || null;
        this.jahr = obj && obj.jahr || null;
        this.checksumme = obj && obj.checksumme || null;
    }
}

export interface Auswertungstabelle {
    dateiInfo: DateiInfo;
    upload: Upload;
    selected: boolean;
}


