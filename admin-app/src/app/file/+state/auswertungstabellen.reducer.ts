import { Action } from 'redux';

import { Auswertungstabelle } from './file.model';
import { AuswertungstabellenState, initialAuswertungstabellenState } from './auswertungstabellen.state';
import * as AuswertungstabellenAction from './auswertungstabellen.actions';

export const AuswertungstabellenReducer =

    function (state: AuswertungstabellenState = initialAuswertungstabellenState, action: Action): AuswertungstabellenState {
        switch (action.type) {
            case AuswertungstabellenAction.AUSWERTUNGSTABELLEN_LOAD:
                // tslint:disable-next-line:max-line-length
                const _auswertungstabellen: Auswertungstabelle[] = (<AuswertungstabellenAction.AuswertungstabellenLoadAction>action).payload;
                return {
                    auswertungstabellen: _auswertungstabellen
                };
            case AuswertungstabellenAction.AUSWERTUNGSTABELLEN_CLEAR:
                return initialAuswertungstabellenState;
            default:
                return state;
        }
    };

