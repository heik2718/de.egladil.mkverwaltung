import { Auswertungstabelle } from './file.model';

export interface AuswertungstabellenState {

    auswertungstabellen: Auswertungstabelle[];

}

export const initialAuswertungstabellenState: AuswertungstabellenState = { auswertungstabellen: [] };


