import { Action, ActionCreator } from 'redux';
import { Auswertungstabelle } from './file.model';

export const AUSWERTUNGSTABELLEN_LOAD = '[auswertungstabellen] load';
export const AUSWERTUNGSTABELLEN_CLEAR = '[auswertungstabellen] clear';

export interface AuswertungstabellenLoadAction extends Action {
    payload: Auswertungstabelle[];
}

export interface AuswertungstabellenClearAction extends Action {
    payload: Auswertungstabelle[];
}

export interface AuswertungstabelleUpdateAction extends Action {
    payload: Auswertungstabelle;
}

export const loadAuswertungstabellen: ActionCreator<AuswertungstabellenLoadAction> =
  (tabellen) => ({
    type: AUSWERTUNGSTABELLEN_LOAD,
    payload: tabellen
  });

export const clearAuswertungstabellen: ActionCreator<AuswertungstabellenClearAction> =
  (_tabellen) => ({
    type: AUSWERTUNGSTABELLEN_CLEAR,
    payload: []
  });


