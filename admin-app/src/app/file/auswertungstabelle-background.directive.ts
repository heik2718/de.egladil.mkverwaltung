import { Directive, Input, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { getBackgroundColorByUploadStatus } from '../shared/utils';

@Directive({
  selector: '[adminAuswertungstabelleBackground]'
})
export class AuswertungstabelleBackgroundDirective implements AfterViewInit {

   @Input()
  uploadStatus: string;

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngAfterViewInit(): void {
    const color = getBackgroundColorByUploadStatus(this.uploadStatus);
    // tslint:disable-next-line:max-line-length
    color == null ? this.renderer.removeStyle(this.el.nativeElement, 'backgroundColor') : this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', color);
  }

}
