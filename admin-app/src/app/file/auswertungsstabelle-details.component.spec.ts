import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuswertungsstabelleDetailsComponent } from './auswertungsstabelle-details.component';

describe('AuswertungsstabelleDetailsComponent', () => {
  let component: AuswertungsstabelleDetailsComponent;
  let fixture: ComponentFixture<AuswertungsstabelleDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuswertungsstabelleDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuswertungsstabelleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
