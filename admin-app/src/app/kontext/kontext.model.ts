
/**
 * Kontext für MKV erhalten mittels Ping
 */
export class Kontext {
  wettbewerbsjahr?: string;
  gueltigkeitEinmalpasswort?: number; // Anzahl Minuten, die ein Einmalpasswort gültig ist
  maxExtractedBytes?: number;
  startAnmeldungText?: string;
  downloadFreigegebenPrivat?: boolean;
  datumFreigabePrivat?: string; // Datum der Freigabe der Unterlagen für Privatanmeldungen
  downloadFreigegebenLehrer?: boolean;
  datumFreigabeLehrer?: string; // Datum der Freigabe der Unterlagen für Lehrer
  neuanmeldungFreigegeben?: boolean;
  uploadDisabled?: boolean;
  wettbewerbBeendet?: boolean;
  xsrfToken?: string;
  ankuendigungWartungsmeldung?: boolean;
  wartungsmeldungText?: string;

  constructor(data: any = {}) {
    this.wettbewerbsjahr = data.wettbewerbsjahr || 'jahr fehlt';
    this.gueltigkeitEinmalpasswort = data.gueltigkeitEinmalpasswort;
    this.maxExtractedBytes = data && data.maxExtractedBytes || 0;
    this.startAnmeldungText = data.startAnmeldungText || '';
    this.downloadFreigegebenPrivat = data.downloadFreigegebenPrivat || false;
    this.datumFreigabePrivat = data.datumFreigabePrivat || null;
    this.downloadFreigegebenLehrer = data.downloadFreigegebenLehrer || false;
    this.datumFreigabeLehrer = data.datumFreigabeLehrer || null;
    this.neuanmeldungFreigegeben = data.neuanmeldungFreigegeben || false;
    this.uploadDisabled = data.uploadDisabled || true;
    this.xsrfToken = data.xsrfToken;
    this.ankuendigungWartungsmeldung = data.ankuendigungWartungsmeldung || false;
    this.wartungsmeldungText = data.wartungsmeldungText || '';
  }
}

