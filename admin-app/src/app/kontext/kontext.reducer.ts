import {Action} from 'redux';

import {Kontext} from './kontext.model';
import * as KontextAction from './kontext.actions';

export interface KontextState {
  kontext: Kontext;
}

const initialState: KontextState = {kontext: null};

export const KontextReducer =

  function(state: KontextState = initialState, action: Action): KontextState {
    switch (action.type) {
      case KontextAction.SET_KONTEXT:
        const theKontext: Kontext = (<KontextAction.KontextAction>action).kontext;
        return {
          kontext: theKontext
        };
      case KontextAction.CLEAR_KONTEXT:
        return {
          kontext: null
        };
      default:
        return state;
    }
  };


export const getKontextState = (state): KontextState => state.kontext;

