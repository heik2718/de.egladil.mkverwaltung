import {Action, ActionCreator} from 'redux';
import {Kontext} from './kontext.model';

export const SET_KONTEXT = '[Kontext] Set';
export const CLEAR_KONTEXT = '[Kontext] Clear';

export interface KontextAction extends Action {
  kontext: Kontext;
}

export const setKontext: ActionCreator<KontextAction> =
  (theKontext) => ({
    type: SET_KONTEXT,
    kontext: theKontext
  });


export const clearKontext: ActionCreator<KontextAction> =
  (theKontext) => ({
    type: CLEAR_KONTEXT,
    kontext: theKontext
  });

