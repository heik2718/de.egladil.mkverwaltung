import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';

import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { Kontext } from '../kontext/kontext.model';

@Component({
  selector: 'admin-kontext',
  templateUrl: './kontext.component.html'
})
export class KontextComponent implements OnInit, OnDestroy {

  kontext: Kontext;

  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>, private _logger: LogService) {
    this._logger.debug('KontextComponent created');
    this._reset();
  }

  ngOnInit() {

    this._logger.debug('KontextComponent ngOnInit');
    this._readState();

    this._unsubscribe = this._store.subscribe(() => {
      this._readState();
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  getText(flag: boolean): string {
    return flag ? 'ja' : 'nein';
  }

  private _readState(): void {
    this._logger.debug('KontextComponent: state changed');
    const state = this._store.getState();
    const k = state.kontext.kontext;
    if (k !== null) {
      this.kontext = k;
    } else {
      this._reset();
    }
  }

  private _reset(): void {
    this.kontext = null;
  }
}
