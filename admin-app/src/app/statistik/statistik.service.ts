import { Injectable, Inject } from '@angular/core';
import { Store } from 'redux';

// own app
import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { ResponsePayload } from '../shared/responsePayload.model';
import { Teilnahmegruppe, Teilnahmejahr, Gesamtpunktverteilung } from './+state/statistik.model';
import { loadTeilnahmegruppen, loadTeilnahmejahre, loadGesamtpunktverteilung } from './+state/statistik.actions';

// infrastructure
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { LogService } from 'hewi-ng-lib';

const BASE_URL = environment.apiUrl;

@Injectable()
export class StatistikService {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _http: HttpClient,
    private _logger: LogService) { }


  loadJahre(): void {
    const _url = BASE_URL + '/auswertungen/jahre';

    const _future$ = this._http.get(_url);
    _future$.subscribe((response: ResponsePayload<Teilnahmejahr[]>) => {
      const _payload = response.payload;
      this._logger.debug(JSON.stringify(_payload));
      this._store.dispatch(loadTeilnahmejahre(_payload));
    });
  }

  loadUebersichtGruppen(teilnahmejahr: Teilnahmejahr): void {
    const _url = BASE_URL + '/auswertungen/' + teilnahmejahr.jahr + '/gruppen';

    const _future$ = this._http.get(_url);
    _future$.subscribe((response: ResponsePayload<Teilnahmegruppe[]>) => {
      const _payload = response.payload;
      this._store.dispatch(loadTeilnahmegruppen(_payload));
    });
  }

  loadGesamtpunktverteilung(teilnahmejahr: Teilnahmejahr): void {
    const _url = BASE_URL + '/auswertungen/gesamtpunktverteilungen/' + teilnahmejahr.jahr;

    const _future$ = this._http.get(_url);
    _future$.subscribe((response: ResponsePayload<Gesamtpunktverteilung>) => {
      const _payload = response.payload;

      //      this._logger.debug(JSON.stringify(_payload));

      const _vert = new Gesamtpunktverteilung(_payload);
      this._store.dispatch(loadGesamtpunktverteilung(_vert));
    });
  }

  loadPunktverteilungenGruppen(teilnahmejahr: Teilnahmejahr, kuerzelliste: string): void {
    const _url = BASE_URL + '/auswertungen/gesamtpunktverteilungen/' + teilnahmejahr.jahr + '/gruppen';
    const _params = new HttpParams().set('k', kuerzelliste);

    const _future$ = this._http.get(_url, { params: _params });
    _future$.subscribe((response: ResponsePayload<Gesamtpunktverteilung>) => {
      const _payload = response.payload;
      const _vert = new Gesamtpunktverteilung(_payload);
      this._store.dispatch(loadGesamtpunktverteilung(_vert));
    });
  }
}



