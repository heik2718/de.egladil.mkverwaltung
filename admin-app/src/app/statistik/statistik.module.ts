import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatistikRoutingModule } from './statistik-routing.module';

@NgModule({
  imports: [
    CommonModule,
    StatistikRoutingModule
  ],
  declarations: []
})
export class StatistikModule { }
