import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';
import {sumAnzahlSchulen, sumAnzahlKinder, getAnzahlPrivatteilnahmen, getKuerzelliste} from '../shared/utils';

import {StatistikService} from './statistik.service';
import {Teilnahmegruppe, Teilnahmejahr} from './+state/statistik.model';
import {clearTeilnahmegruppen} from './+state/statistik.actions';

@Component({
  selector: 'admin-laenderuebersicht',
  templateUrl: './teilnahmegruppenlist.component.html',
  styleUrls: ['./statistikStyles.css']
})
export class TeilnahmegruppenListComponent implements OnInit, OnDestroy {

  teilnahmegruppen: Teilnahmegruppe[];
  jahr: string;
  anzahlSchulen: number;
  anzahlKinder: number;
  anzahlPrivatteilnahmen: number;

  selectedGruppen: Teilnahmegruppe[];

  private _unsubscribe: Unsubscribe;

  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private _statistikService: StatistikService
    , private _route: ActivatedRoute
    , private _router: Router
    , private _logger: LogService) {

    this.teilnahmegruppen = [];
    this.jahr = '';
    this.anzahlKinder = 0;
    this.anzahlSchulen = 0;
    this.anzahlPrivatteilnahmen = 0;

  }

  ngOnInit() {

    const routeSubscription = this._route.params
      .subscribe(params => {
        this._logger.debug('route.params=' + JSON.stringify(params));
        const id = (params['jahr'] || '');
        if (id !== '' && id !== 'new') {
          const _stored = this._store.getState().teilnahmegruppen.teilnahmegruppen as Teilnahmegruppe[];
          if (_stored === null || _stored.length === 0 || _stored[0].jahr !== id) {
            const _teilnahmejahr = {'jahr': id};
            this._statistikService.loadUebersichtGruppen(_teilnahmejahr);
          } else {
            this.teilnahmegruppen = _stored;
          }
        }
      });

    this.selectedGruppen = [];
    this._unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this.teilnahmegruppen = state.teilnahmegruppen.teilnahmegruppen;
      this.anzahlSchulen = sumAnzahlSchulen(this.teilnahmegruppen);
      this.anzahlKinder = sumAnzahlKinder(this.teilnahmegruppen);
      this.anzahlPrivatteilnahmen = getAnzahlPrivatteilnahmen(this.teilnahmegruppen);

      this._logger.debug('Anzahlen');
      if (this.teilnahmegruppen.length > 0) {
        this.jahr = this.teilnahmegruppen[0].jahr;
      }
    });
  }

  onGruppeSelected(kuerzel: string) {
    const _gruppe = this.teilnahmegruppen.find(g => g.kuerzel === kuerzel) as Teilnahmegruppe;
    if (_gruppe !== undefined) {
      if (_gruppe.selected) {
        this.selectedGruppen.push(_gruppe);
        this._logger.debug('Gruppe ' + kuerzel + ' selected');
      } else {
        for (let i = 0; i < this.selectedGruppen.length; i++) {
          if (this.selectedGruppen[i].kuerzel === _gruppe.kuerzel) {
            this.selectedGruppen.splice(i, 1);
            this._logger.debug('Gruppe ' + kuerzel + ' deselected');
            break;
          }
        }
      }
    }
  }

  ngOnDestroy() {
    this._unsubscribe();
    this._store.dispatch(clearTeilnahmegruppen([]));
  }

  gewaehlteAuswerten(): void {
    this._logger.debug('Statistik für ' + this.selectedGruppen.length + ' Gruppe(n)');
    if (this.selectedGruppen.length > 0) {
      const _teilnahmejahr = {'jahr': this.jahr} as Teilnahmejahr;
      const _kuerzel = getKuerzelliste(this.selectedGruppen);
      this._statistikService.loadPunktverteilungenGruppen(_teilnahmejahr, _kuerzel);
      this._router.navigate(['/statistik/punktverteilung']);
    }
  }


  cancel(): void {
    this._router.navigate(['/statistik']);
  }
}


