import { Component, OnInit, Inject, Input } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Store, Unsubscribe } from 'redux';

import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';

import { Teilnahmejahr } from './+state/statistik.model';
import { selectWettbewerbsjahr, unselectWettbewerbsjahr } from './+state/statistik.actions';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'admin-jahr',
  templateUrl: './wettbewerbsjahr.component.html',
  styleUrls: ['./statistikStyles.css']
})
export class WettbewerbsjahrComponent implements OnInit {

  @Input() teilnahmejahr: Teilnahmejahr;
  private _styles: {};

  constructor (private _router: Router
    , private _route: ActivatedRoute
    , @Inject(AppStore) private _store: Store<AppState>
    , private _logger: LogService) { }

  ngOnInit() {
  }

  gotoGruppenuebersicht(): void {
    this._store.dispatch(selectWettbewerbsjahr(this.teilnahmejahr));
    this._router.navigate(['/statistik/uebersicht/', this.teilnahmejahr.jahr], { relativeTo: this._route });
  }

  getGesamtpunktverteilung(): void {
    this._store.dispatch(selectWettbewerbsjahr(this.teilnahmejahr));
    this._router.navigate(['/statistik/gesamtpunktverteilung/', this.teilnahmejahr.jahr], { relativeTo: this._route });
  }
}



