import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';

import { AppConstants } from '../shared/app.constants';
import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';

import { Teilnahmegruppe } from './+state/statistik.model';

@Component({
  selector: 'admin-teilnahmegruppe',
  templateUrl: './teilnahmegruppe.component.html',
  styleUrls: ['./statistikStyles.css']
})
export class TeilnahmegruppeComponent implements OnInit {

  @Input() teilnahmegruppe: Teilnahmegruppe;

  @Output()
  gruppeSelect: EventEmitter<string> = new EventEmitter<string>();

  selected: boolean;

  private _styles: {};

  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _logger: LogService) { }

  ngOnInit() {
    this.selected = false;
    const _color = this._calculateBackgroundColor();
    this._styles = {
      'background-color': _color
    };
  }

  toggleSelected(): void {
    this.selected = !this.selected;
    this.teilnahmegruppe.selected = this.selected;

    const _color = this._calculateBackgroundColor();
    this._styles = {
      'background-color': _color
    };

    this.gruppeSelect.emit(this.teilnahmegruppe.kuerzel);
  }

  getStyles() {
    return this._styles;
  }

  private _calculateBackgroundColor(): string {
    if (this.teilnahmegruppe.selected) {
      return AppConstants.backgroundColors.INPUT_VALID;
    } else {
      return AppConstants.backgroundColors.INPUT_NEUTRAL;
    }
  }
}


