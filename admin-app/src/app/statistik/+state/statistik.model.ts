import {HateoasPayload} from '../../shared/responsePayload.model';

export const FILTERART_LAND = 'LAND';
export const FILTERART_PRIVAT = 'PRIVAT';

export class Teilnahmenfilter {
  name: string;
  filterart: string;
  kuerzelliste: string[];

  constructor(data: any = {}) {
    this.name = data && data.name || '';
    this.filterart = data && data.filterart || '';
    this.kuerzelliste = data && data.kuerzelliste || [];
  }
}

export const initialTeilnahmenfilter: Teilnahmenfilter = {'name': '', 'filterart': '', 'kuerzelliste': []};

export class Teilnahmegruppe {
  filter: Teilnahmenfilter;
  jahr: string;
  kuerzel: string;
  aggregiert: boolean;
  anzahlAnmeldungen?: number;
  anzahlKinder?: number;
  teilnahmejahre?: string[];
  hateoasPayload: HateoasPayload;
  selected: boolean;

  constructor(data: any = {}) {
    this.filter = data && data.filter || initialTeilnahmenfilter;
    this.jahr = data && data.jahr || '';
    this.kuerzel = data && data.kuerzel || '';
    this.aggregiert = data && data.aggregiert || false;
    this.anzahlAnmeldungen = data && data.anzahlAnmeldungen || 0;
    this.anzahlKinder = data && data.anzahlKinder || 0;
    this.teilnahmejahre = data && data.teilnahmejahre || [];
    this.hateoasPayload = data && data.hateoasPayload || null;
    this.selected = false;
  }
}

export class Teilnahmejahr {
  jahr?: string;
  hateoasPayload?: HateoasPayload;

  constructor(data: any = {}) {
    this.jahr = data && data.jahr || '';
    this.hateoasPayload = data && data.hateoasPayload || null;
  }
}

export class Gesamtpunktverteilung {
  name: string;
  jahr: string;
  daten: GesamtpunktverteilungDaten[];

  constructor(data: any = {}) {
    this.name = data && data.name || 'Titel fehlt';
    this.jahr = data && data.jahr || '';
    this.daten = data && data.daten || [];
  }
}


export class GesamtpunktverteilungDaten {
  klassenstufe: string;
  anzahlTeilnehmer: number;
  median: string;
  gesamtpunktverteilungItems: GesamtpunktverteilungItem[];
  aufgabeErgebnisItems: AufgabeErgebnisItem[];
  rohpunktItems: RohpunktItem[];

  constructor(data: any = {}) {
    this.klassenstufe = data && data.klassenstufe || '';
    this.anzahlTeilnehmer = data && data.anzahlTeilnehmer || 0;
    this.median = data && data.median || '';
    this.gesamtpunktverteilungItems = data && data.gesamtpunktverteilungItems || [];
    this.aufgabeErgebnisItems = data && data.aufgabeErgebnisItems || [];
    this.rohpunktItems = data && data.rohpunktItems || [];
  }
}

/**
 * Anzahlen innerhalb der festen Punktklassen
 */
export class GesamtpunktverteilungItem {
  intervall: string;
  anzahlImIntervall: string;
  intervallPR: string;

  constructor(data: any = {}) {
    this.intervall = data && data.intervall || '';
    this.anzahlImIntervall = data && data.anzahlImIntervall || '';
    this.intervallPR = data && data.intervallPR || '';
  }
}

/**
 * für die Aufgeben-Ergebnis-Ansicht
 */
export class AufgabeErgebnisItem {
  nummer: string;
  anteilRichtigGeloest: string;
  anteilFalschGeloest: string;
  anteilNichtGeloest: string;
  zuLeicht: boolean;
  zuSchwer: boolean;

  constructor(data: any = {}) {
    this.nummer = data && data.nummer || '';
    this.anteilRichtigGeloest = data && data.anteilRichtigGeloest || '';
    this.anteilFalschGeloest = data && data.anteilFalschGeloest || '';
    this.anteilNichtGeloest = data && data.anteilNichtGeloest || '';
    this.zuLeicht = data && data.zuLeicht || false;
    this.zuSchwer = data && data.zuSchwer || false;
  }

}

/**
 * Für die Prozenzränge
 */
export class RohpunktItem {
  punkte: string;
  anzahl: string;
  prozentrang: string;

  constructor(data: any = {}) {
    this.punkte = data && data.punkte || '';
    this.anzahl = data && data.anzahl || '';
    this.prozentrang = data && data.prozentrang || '';
  }
}




