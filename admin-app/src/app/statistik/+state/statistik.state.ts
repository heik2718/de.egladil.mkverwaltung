import {Teilnahmegruppe, Teilnahmejahr, Gesamtpunktverteilung} from './statistik.model';
import * as StatistikState from './statistik.state';

export interface TeilnahmegruppenState {
  teilnahmegruppen: Teilnahmegruppe[];
}

export const initialTeilnahmegruppenState: TeilnahmegruppenState = {teilnahmegruppen: []};

export interface TeilnahmejahreState {
  jahre: Teilnahmejahr[];
}

export const initialTeilnahmejahreState: TeilnahmejahreState = {jahre: []};


export interface GewaehltesWettbewerbsjahrState {
  jahr: Teilnahmejahr;
}

export interface GesamtpunktverteilungState {
  gesamtpunktverteilung: Gesamtpunktverteilung;
}

export const initialGesamtpunktverteilungState: StatistikState.GesamtpunktverteilungState
  = {gesamtpunktverteilung: new Gesamtpunktverteilung({})};






