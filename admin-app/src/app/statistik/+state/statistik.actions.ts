import {Action, ActionCreator} from 'redux';
import {Teilnahmegruppe, Teilnahmejahr, Gesamtpunktverteilung} from './statistik.model';

export const TEILNAHMEGRUPPEN_LOAD = '[teilnahmegruppen] load';
export const TEILNAHMEGRUPPEN_CLEAR = '[teilnahmegruppen] clear';

export const TEILNAHMEJAHRE_LOAD = '[teilnahmejahre] load';
export const TEILNAHMEJAHRE_CLEAR = '[teilnahmejahre] clear';

export const WETTBEWERBSJAHR_SELECT = '[wettbewerbsjahr] select';
export const WETTBEWERBSJAHR_CLEAR = '[wettbewerbsjahr] clear';

export const GESAMTPUNKTVERTEILUNG_LOAD = '[gesamtpunktverteilung] load';
export const GESAMTPUNKTVERTEILUNG_CLEAR = '[gesamtpunktverteilung] clear';


// Teilnahmegruppen
export interface TeilnahmegruppenLoadAction extends Action {
  payload: Teilnahmegruppe[];
}

export interface TeilnahmegruppenClearAction extends Action {
  payload: Teilnahmegruppe[];
}

export const loadTeilnahmegruppen: ActionCreator<TeilnahmegruppenLoadAction> =
  (g) => ({
    type: TEILNAHMEGRUPPEN_LOAD,
    payload: g
  });

export const clearTeilnahmegruppen: ActionCreator<TeilnahmegruppenClearAction> =
  (g) => ({
    type: TEILNAHMEGRUPPEN_CLEAR,
    payload: []
  });

// Teilnahmejahre
export interface TeilnahmejahreLoadAction extends Action {
  payload: Teilnahmejahr[];
}

export interface TeilnahmejahreClearAction extends Action {
  payload: Teilnahmejahr[];
}

export const loadTeilnahmejahre: ActionCreator<TeilnahmejahreLoadAction> =
  (g) => ({
    type: TEILNAHMEJAHRE_LOAD,
    payload: g
  });

export const clearTeilnahmejahre: ActionCreator<TeilnahmejahreClearAction> =
  (g) => ({
    type: TEILNAHMEJAHRE_CLEAR,
    payload: []
  });


// Wettbewerbsjahr
export interface WettbewerbsjahrSelectAction extends Action {
  payload: Teilnahmejahr;
}

export interface WettbewerbsjahrClearAction extends Action {
  payload: Teilnahmejahr[];
}

export const selectWettbewerbsjahr: ActionCreator<WettbewerbsjahrSelectAction> =
  (g) => ({
    type: WETTBEWERBSJAHR_SELECT,
    payload: g
  });

export const unselectWettbewerbsjahr: ActionCreator<WettbewerbsjahrClearAction> =
  (g) => ({
    type: WETTBEWERBSJAHR_CLEAR,
    payload: []
  });

// Gesamtpunktverteilung
// Teilnahmejahre
export interface GessamtpunktverteilungLoadAction extends Action {
  payload: Gesamtpunktverteilung;
}

export interface GessamtpunktverteilungClearAction extends Action {
  payload: Gesamtpunktverteilung;
}

export const loadGesamtpunktverteilung: ActionCreator<GessamtpunktverteilungLoadAction> =
  (g) => ({
    type: GESAMTPUNKTVERTEILUNG_LOAD,
    payload: g
  });

export const clearGesamtpunktverteilung: ActionCreator<GessamtpunktverteilungClearAction> =
  (g) => ({
    type: GESAMTPUNKTVERTEILUNG_CLEAR,
    payload: new Gesamtpunktverteilung({})
  });



