import {Action} from 'redux';

import {Teilnahmegruppe, Teilnahmejahr, GesamtpunktverteilungDaten, Gesamtpunktverteilung} from './statistik.model';
import * as StatistikState from './statistik.state';
import * as StatistikAction from './statistik.actions';

export const TeilnahmegruppenReducer =
  function(state: StatistikState.TeilnahmegruppenState = StatistikState.initialTeilnahmegruppenState, action: Action):
    StatistikState.TeilnahmegruppenState {

    switch (action.type) {
      case (StatistikAction.TEILNAHMEGRUPPEN_LOAD):
        const _payload: Teilnahmegruppe[] = (<StatistikAction.TeilnahmegruppenLoadAction>action).payload;
        return {
          teilnahmegruppen: _payload
        };
      case (StatistikAction.TEILNAHMEGRUPPEN_CLEAR): return StatistikState.initialTeilnahmegruppenState;
      default: return state;
    }
  };

export const TeilnahmejahreReducer =
  function(state: StatistikState.TeilnahmejahreState = StatistikState.initialTeilnahmejahreState, action: Action):
    StatistikState.TeilnahmejahreState {

    switch (action.type) {
      case (StatistikAction.TEILNAHMEJAHRE_LOAD):
        const _payload: Teilnahmejahr[] = (<StatistikAction.TeilnahmejahreLoadAction>action).payload;
        return {
          jahre: _payload
        };
      case (StatistikAction.TEILNAHMEJAHRE_CLEAR): return StatistikState.initialTeilnahmejahreState;
      default: return state;
    }
  };

export const GewaehltesWettbewerbsjahrReducer =
  function(state: StatistikState.GewaehltesWettbewerbsjahrState = null, action: Action):
    StatistikState.GewaehltesWettbewerbsjahrState {

    switch (action.type) {
      case (StatistikAction.WETTBEWERBSJAHR_SELECT):
        const _payload: Teilnahmejahr = (<StatistikAction.WettbewerbsjahrSelectAction>action).payload;
        return {
          jahr: _payload
        };
      case (StatistikAction.WETTBEWERBSJAHR_CLEAR): return null;
      default: return state;
    }
  };

export const GesamtpunktverteilungReducer =
  function(state: StatistikState.GesamtpunktverteilungState = null, action: Action):
    StatistikState.GesamtpunktverteilungState {

    switch (action.type) {
      case (StatistikAction.GESAMTPUNKTVERTEILUNG_LOAD):
        const _payload: Gesamtpunktverteilung = (<StatistikAction.GessamtpunktverteilungLoadAction>action).payload;
        return {
          gesamtpunktverteilung: _payload
        };
      case (StatistikAction.GESAMTPUNKTVERTEILUNG_CLEAR): return null;
      default: return state;
    }
  };




