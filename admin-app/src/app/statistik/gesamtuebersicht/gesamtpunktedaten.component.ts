import { Component, OnInit, Inject, Input } from '@angular/core';
import { LogService } from 'hewi-ng-lib';

import { GesamtpunktverteilungDaten } from '../+state/statistik.model';

@Component({
  selector: 'admin-gesamtpunkte-daten',
  templateUrl: './gesamtpunktedaten.component.html'
})
export class GesamtpunktedatenComponent implements OnInit {

  @Input() daten: GesamtpunktverteilungDaten;

  showPunktverteilung: boolean;
  showAufgabenItems: boolean;
  showRohpunktItems: boolean;


  constructor (private _logger: LogService) { }

  ngOnInit() {
    this.showPunktverteilung = false;
    this.showAufgabenItems = false;
    this.showRohpunktItems = false;
  }

  togglePunktverteilung(): void {
    this.showPunktverteilung = !this.showPunktverteilung;
  }

  toggleAufgabenItem(): void {
    this.showAufgabenItems = !this.showAufgabenItems;
  }

  toggleRohpunktitems(): void {
    this.showRohpunktItems = !this.showRohpunktItems;
  }

  collapsAll(): void {
    this.showAufgabenItems = false;
    this.showPunktverteilung = false;
    this.showRohpunktItems = false;
  }

  expandAll(): void {
    this.showAufgabenItems = true;
    this.showPunktverteilung = true;
    this.showRohpunktItems = true;
  }

  isAnyExpanded(): boolean {
    return this.showAufgabenItems || this.showPunktverteilung || this.showRohpunktItems;
  }

  isAllCollapsed(): boolean {
    return !this.showAufgabenItems && !this.showPunktverteilung && !this.showRohpunktItems;
  }
}



