import { Component, OnInit, Inject, Input } from '@angular/core';
import { LogService } from 'hewi-ng-lib';

import { GesamtpunktverteilungItem } from '../+state/statistik.model';

@Component({
  selector: 'admin-punktklasse-item',
  templateUrl: './punktklasse.component.html'
})
export class PunktklasseComponent implements OnInit {

  @Input() item: GesamtpunktverteilungItem;
  private _styles: {};

  constructor (private _logger: LogService) { }

  ngOnInit() {
  }
}


