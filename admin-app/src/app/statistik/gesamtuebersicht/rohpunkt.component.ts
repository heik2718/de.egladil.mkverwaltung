import {Component, OnInit, Inject, Input} from '@angular/core';

import {RohpunktItem} from '../+state/statistik.model';

@Component({
  selector: 'admin-rohpunkt',
  templateUrl: './rohpunkt.component.html'
})
export class RohpunktComponent implements OnInit {

  @Input() item: RohpunktItem;
  private _styles: {};

  constructor() {}

  ngOnInit() {
  }
}


