import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';

import { clearGesamtpunktverteilung } from '../+state/statistik.actions';
import { AppState } from '../../app.reducer';
import { AppStore } from '../../app.store';
import { MessageService } from '../../messages/message.service';
import { Gesamtpunktverteilung, Teilnahmejahr, GesamtpunktverteilungDaten } from '../+state/statistik.model';
import { gesamtzahlKinder } from '../../shared/utils';

/**
 * Ist die Komponente für die Punktverteilung einer Gruppe von Ländern o.ä.
 */
@Component({
  selector: 'admin-punktverteilung',
  templateUrl: './punktverteilung.component.html',
})
export class PunktverteilungComponent implements OnInit, OnDestroy {

  daten: GesamtpunktverteilungDaten[];
  teilnahmejahr: Teilnahmejahr;
  anzahlKinder: number;
  title: string;

  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _router: Router
    , private _messageService: MessageService
    , private _logger: LogService) {

    this.title = 'Titel nicht vom Server';
  }

  ngOnInit() {

    this.anzahlKinder = 0;

    this._logger.debug('ngOnInit start');

    this._unsubscribe = this._store.subscribe(() => {
      const _state = this._store.getState();
      this.teilnahmejahr = _state.gewaehltesWettbewerbsjahr === null ? null : _state.gewaehltesWettbewerbsjahr.jahr;
      if (this.teilnahmejahr !== null) {
        this.title = 'Titel nicht vom Server (' + this.teilnahmejahr.jahr + ')';
      }

      if (_state.gesamtpunktverteilung !== null) {
        this._logger.debug('Daten vorhanden');
        const _verteilung = _state.gesamtpunktverteilung.gesamtpunktverteilung as Gesamtpunktverteilung;
        this.daten = _verteilung.daten;
        this.title = _verteilung.name;
        if (this.daten.length > 0) {
          this.anzahlKinder = gesamtzahlKinder(this.daten);
        }
      } else {
        this.title = 'noch keine Daten vorhanden';
      }
    });

    this._logger.debug('ngOnInit ende');
  }

  ngOnDestroy() {
    this._logger.debug('nach unsubscribe');
  }

  cancel(): void {
    this._messageService.clearMessage();
    this._store.dispatch(clearGesamtpunktverteilung());
    this._router.navigate(['/statistik']);
  }
}


