import { Component, OnInit, Inject, Input } from '@angular/core';
import { LogService } from 'hewi-ng-lib';

import { AppConstants } from '../../shared/app.constants';

import { AufgabeErgebnisItem } from '../+state/statistik.model';

@Component({
  selector: 'admin-aufgabe-ergebnis',
  templateUrl: './aufgabeErgebnis.component.html'
})
export class AufgabeErgebnisComponent implements OnInit {

  @Input() item: AufgabeErgebnisItem;
  private _styles: {};

  constructor (private _logger: LogService) { }

  ngOnInit() {
    const _color = this._calculateBackgroundColor();
    this._styles = {
      'background-color': _color
    };
  }

  private _calculateBackgroundColor(): string {
    if (this.item.zuLeicht) {
      this._logger.debug('zuLeicht');
      return AppConstants.backgroundColors.ZU_LEICHT;
    }
    if (this.item.zuSchwer) {
      this._logger.debug('zuSchwer');
      return AppConstants.backgroundColors.ZU_SCHWER;
    }
    return AppConstants.backgroundColors.INPUT_NEUTRAL;
  }

  getStyles() {
    return this._styles;
  }
}




