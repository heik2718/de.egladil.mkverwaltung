import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';

import { clearGesamtpunktverteilung } from '../+state/statistik.actions';
import { AppState } from '../../app.reducer';
import { AppStore } from '../../app.store';
import { MessageService } from '../../messages/message.service';
import { StatistikService } from '../statistik.service';
import { Gesamtpunktverteilung, Teilnahmejahr, GesamtpunktverteilungDaten } from '../+state/statistik.model';
import { gesamtzahlKinder } from '../../shared/utils';

@Component({
  selector: 'admin-gesamtpunktverteilung',
  templateUrl: './gesamtpunktverteilung.component.html',
})
export class GesamtpunktverteilungComponent implements OnInit {

  daten: GesamtpunktverteilungDaten[];
  teilnahmejahr: Teilnahmejahr;
  anzahlKinder: number;
  title: string;

  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _router: Router
    , private _route: ActivatedRoute
    , private _messageService: MessageService
    , private _statistikService: StatistikService
    , private _logger: LogService) { }

  ngOnInit() {

    this._logger.debug('GesamtpunktverteilungDaten.ngOnInit()');

    this.title = 'Gesamtpunktverteilung';

    const routeSubscription = this._route.params
      .subscribe(params => {
        this._logger.debug('route.params=' + JSON.stringify(params));
        const id = (params['jahr'] || '');
        if (id && id !== '') {
          const _state = this._store.getState();
          if (_state.gesamtpunktverteilung === null || _state.gesamtpunktverteilung.gesamtpunktverteilung.jahr !== id) {
            this._logger.debug('haben noch keine oder die falsche gesamtpunktverteilung');
            this._statistikService.loadGesamtpunktverteilung(new Teilnahmejahr({ jahr: id }));
          } else {
            this.daten = _state.gesamtpunktverteilung.gesamtpunktverteilung.daten;
            this.title = _state.gesamtpunktverteilung.gesamtpunktverteilung.name;
          }
        }
      });

    this._unsubscribe = this._store.subscribe(() => {
      const _state = this._store.getState();
      this.teilnahmejahr = _state.gewaehltesWettbewerbsjahr === null ? null : _state.gewaehltesWettbewerbsjahr.jahr;
      if (_state.gesamtpunktverteilung !== null) {
        this.daten = _state.gesamtpunktverteilung.gesamtpunktverteilung.daten;
        this.anzahlKinder = gesamtzahlKinder(this.daten);
      }
    });
  }

  cancel(): void {
    this._messageService.clearMessage();
    this._store.dispatch(clearGesamtpunktverteilung());
    this._router.navigate(['/statistik']);
  }
}


