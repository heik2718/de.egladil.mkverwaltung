import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';

import {StatistikService} from './statistik.service';
import {Teilnahmejahr} from './+state/statistik.model';

@Component({
  selector: 'admin-statistik',
  templateUrl: './statistik.component.html',
})
export class StatistikComponent implements OnInit {

  jahre: Teilnahmejahr[];
  private _unsubscribe: Unsubscribe;

  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private _statistikService: StatistikService
    , private _logger: LogService) {}

  ngOnInit() {

    this._logger.debug('StatistikComponent.ngOnInit()');
    this.jahre = [];

    this._unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this.jahre = state.teilnahmejahre.jahre;
      this._checkJahre();
    });

    this._checkJahre();
  }

  private _checkJahre() {
    if (this.jahre.length === 0) {
      this._statistikService.loadJahre();
    }
  }
}


