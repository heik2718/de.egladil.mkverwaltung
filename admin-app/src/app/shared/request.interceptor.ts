import {Injectable, Inject} from '@angular/core';
import {HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent} from '@angular/common/http';
import {HttpHeaders, HttpInterceptor, HTTP_INTERCEPTORS} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Store} from 'redux';


import {AppStore} from '../app.store';
import {AppState} from '../app.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor( @Inject(AppStore) private _store: Store<AppState>, private _logger: LogService) {}


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const user = this._store.getState().session.sessionToken;

    this._logger.debug('intercept http request ' + JSON.stringify(user));

    const modifiedReq = request.clone({
      headers: this._addHeaders(request.headers)
    });

    return next.handle(modifiedReq);

//    return next.handle(modifiedReq).do((event: HttpEvent<any>) => {}, (err: any) => {
//      if (err instanceof HttpErrorResponse) {
//        // do error handling here
//      }
//    });
  }

  private _addHeaders(headers: HttpHeaders): HttpHeaders {
    const _user = this._store.getState().session.sessionToken;

    if (_user === null || _user === undefined) {
      return headers;
    }

    if (!_user.isLoggedIn()) {
      const _headers = headers.append('Content-Type', 'application/json; charset=utf-8')
        .append('X-XSRF-TOKEN', _user.xsrfToken);
      return _headers;
    } else {
      const _headers = headers.append('Content-Type', 'application/json; charset=utf-8')
        .append('X-XSRF-TOKEN', _user.xsrfToken).append('Authorization', 'Bearer ' + _user.accessToken);
      return _headers;
    }
  }
}

/**
 * Provider POJO for the interceptor
 */
export const RequestInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: RequestInterceptor,
  multi: true,
};

