import { Injectable, Inject } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Store } from 'redux';


import { AppStore } from '../app.store';
import { AppState } from '../app.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor (@Inject(AppStore) private _store: Store<AppState>, private _logger: LogService) { }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const sessionToken = this._store.getState().session.sessionToken;

    this._logger.debug('intercept http request ' + JSON.stringify(sessionToken));

    const modifiedReq = request.clone({
      headers: this._addHeaders(request.headers)
    });

    return next.handle(modifiedReq);
  }

  private _addHeaders(headers: HttpHeaders): HttpHeaders {
    const _sessionToken = this._store.getState().session.sessionToken;

    if (_sessionToken === null || _sessionToken === undefined) {
      return headers;
    }

    if (!_sessionToken.isLoggedIn()) {
      const _headers = headers.append('Content-Type', 'application/json; charset=utf-8')
        .append('X-XSRF-TOKEN', _sessionToken.xsrfToken);
      return _headers;
    } else {
      const _headers = headers.append('Content-Type', 'application/json; charset=utf-8')
        .append('X-XSRF-TOKEN', _sessionToken.xsrfToken).append('Authorization', 'Bearer ' + _sessionToken.accessToken);
      return _headers;
    }
  }
}

/**
 * Provider POJO for the interceptor
 */
export const TokenInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptor,
  multi: true,
};

