
import {Message} from '../messages/message.model';

export const ACTION_GET = 'GET';
export const ACTION_CREATE = 'POST';
export const ACTION_UPDATE = 'PUT';
export const ACTION_DELETE = 'DELETE';


export class HateoasLink {
  method: string;
  url: string;
  mediatype: string;

  constructor(data: any = {}) {
    this.method = data && data.method || null;
    this.url = data && data.url || null;
    this.mediatype = data && data.mediatype || null;
  }
}

export class HateoasPayload {
  id: string;
  url: string;
  kontext: string;
  links: HateoasLink[];

  constructor(data: any = {}) {
    this.id = data && data.id || null;
    this.url = data && data.url || null;
    this.links = data.links || [];
  }
}


export class ResponsePayload<T> {

  apiMessage: Message;
  payload: T;

  constructor(obj: any) {
    this.apiMessage = obj && obj.apiMessage || null;
    this.payload = obj && obj.payload || null;
  }
}




