import { Component, Input, Optional } from '@angular/core';
import { NgForm, FormGroupDirective, FormGroup } from '@angular/forms';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'admin-show-error',
  template: `
		<div *ngIf="errorMessages" class="alert alert-danger" aria-describedby="error">
			<div *ngFor="let errorMessage of errorMessages">
				{{errorMessage}}
			</div>
		</div>`

})

export class ShowErrorComponent {

  @Input() path;
  @Input() text = '';

  constructor (@Optional() private ngForm: NgForm, @Optional() private formGroup: FormGroupDirective, private logger: LogService) { }

  get errorMessages(): string[] {
    let form: FormGroup;

    if (this.ngForm) {
      form = this.ngForm.form;
    } else {
      form = this.formGroup.form;
    }
    const messages = [];
    const control = form.get(this.path);
    if (!control || !(control.touched) || !control.errors) {
      return null;
    }
    for (const code in control.errors) {
      if (control.errors.hasOwnProperty(code)) {
        const error = control.errors[code];
        let message = '';
        switch (code) {
          case 'requiredTrue':
            message = `Bitte stimmen Sie zu.`;
            break;
          case 'required':
            if (this.text === 'chckb') {
              message = `Bitte stimmen Sie zu.`;
            } else {
              message = `${this.text} ist ein Pflichtfeld`;
            }
            break;
          case 'minlength':
            message = `${this.text} muss mindestens ${error.requiredLength} Zeichen enthalten`;
            break;
          case 'maxlength':
            message = `${this.text} darf maximal ${error.requiredLength} Zeichen enthalten`;
            break;
          case 'invalidEMail':
            message = `Bitte geben Sie eine gültige E-Mail Adresse an.`;
            break;
          case 'invalidPassword':
            message = `Passwort-Regeln: mindestens 8 höchstens 20 Zeichen, mindestens ein Buchstabe, mindestens eine Ziffer,
 kein Leerzeichen`;
            break;
          case 'invalidEinmalPassword':
            message = `Das Einmalpasswort enthält ungültige Zeichen.`;
            break;
        }

        messages.push(message);
      }
    }

    //    this.logger.debug(messages);
    return messages;
  }

}

