import {Injectable, Inject} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Store} from 'redux';

import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor( @Inject(AppStore) private _store: Store<AppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

    const user = this._store.getState().session.sessionToken;
    if (user && user.isLoggedIn()) {
      return true;
    }
    this.router.navigate(['/login'], {
      queryParams: {
        return: state.url
      }
    });
    return false;
  }
}
