import {Injectable} from '@angular/core';
import {saveAs} from 'file-saver/FileSaver';

@Injectable()
export class DownloadDelegate {

  constructor() {}

  saveToFileSystem(blob: Blob, filename: string) {
    saveAs(blob, filename);
  }
}

