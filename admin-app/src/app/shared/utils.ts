import {Teilnahmegruppe, GesamtpunktverteilungDaten, FILTERART_PRIVAT} from '../statistik/+state/statistik.model';

export function sumAnzahlSchulen(teilnahmegruppen: Teilnahmegruppe[]): number {
  const result = teilnahmegruppen.filter(g => g.filter.filterart !== FILTERART_PRIVAT)
    .map(g => g.anzahlAnmeldungen).reduce((prev, next) => prev + next);
  return result;
}

export function sumAnzahlKinder(teilnahmegruppen: Teilnahmegruppe[]): number {
  const result = teilnahmegruppen.filter(g => !g.aggregiert).map(g => g.anzahlKinder).reduce((prev, next) => prev + next);
  return result;
}

export function getAnzahlPrivatteilnahmen(teilnahmegruppen: Teilnahmegruppe[]): number {
  console.log('filtern privat');
  const _gruppen = teilnahmegruppen.filter(g => g.filter.filterart === FILTERART_PRIVAT) as Teilnahmegruppe[];
  if (_gruppen.length > 0) {
    return _gruppen[0].anzahlAnmeldungen;
  }
  return 0;
}

export function getKuerzelliste(teilnahmegruppen: Teilnahmegruppe[]): string {
  console.log('wandeln in kuerzelliste um');
  const _kuerzel = teilnahmegruppen.map(g => g.kuerzel).join();
  return _kuerzel;
}


export function gesamtzahlKinder(daten: GesamtpunktverteilungDaten[]): number {
  const result = daten.map(g => g.anzahlTeilnehmer).reduce((prev, next) => prev + next);
  return result;
}

export function getBackgroundColorByUploadStatus(status: string) {
    switch (status) {
        case 'FERTIG':
            return 'HoneyDew'; // helles grün
        case 'NEU':
            return 'OldLace'; // helles beige
        case 'LEER':
            return '#ffd280'; // helles orange
        case 'FEHLER':
            return '#ffa600'; // dunkleres orange
        case 'KORRIGIERT':
            return 'LightYellow';
    }

    return 'white';
}




