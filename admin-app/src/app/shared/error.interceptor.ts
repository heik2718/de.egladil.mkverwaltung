import { Inject } from '@angular/core';
import { Store } from 'redux';

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';
import 'rxjs/add/operator/catch';

import { AppStore } from '../app.store';
import { AppState } from '../app.reducer';
import { Message, ERROR, WARN } from '../messages/message.model';
import { MessageService } from '../messages/message.service';

import { LogService } from 'hewi-ng-lib';

/**
 * Intercepts the HTTP responses, and in case that an error/exception is thrown, handles it
 * and extract the relevant information of it.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _messageService: MessageService,
    private _logger: LogService) { }

  /**
   * Intercepts an outgoing HTTP request, executes it and handles any error that could be triggered in execution.
   * @see HttpInterceptor
   * @param req the outgoing HTTP request
   * @param next a HTTP request handler
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req)
      .catch(errorResponse => {
        let message: Message;
        let errMsg: string;
        let status: number;

        if (errorResponse instanceof HttpErrorResponse) {
          this._logger.debug('HttpErrorResponse');
          const _errorResponse: HttpErrorResponse = errorResponse;
          message = _errorResponse.error.apiMessage;

          if (message !== undefined) {
            this._messageService.setMessage(message);
            return _throw(message.message);
          }
          status = errorResponse.status;
        } else {
          this._logger.debug('else: ' + JSON.stringify(errorResponse));
          errMsg = errorResponse.message ? errorResponse.message : 'ClientError';
          message = new Message({ 'level': ERROR, 'message': 'ClientError' });
        }

        this._logger.debug(errMsg);
        if (status) {
          switch (status) {
            case 0:
            case 503:
              message = new Message({ 'level': ERROR, 'message': 'Der Server ist nicht verfügbar' });
              break;
            case 401:
            case 403:
              message = new Message({ 'level': ERROR, 'message': 'Das hat leider nicht geklappt' });
              break;
            case 408:
              message = new Message({ 'level': WARN, 'message': 'Timeout: bitte neu anmelden' });
              break;
            case 900:
              message = new Message({ 'level': WARN, 'message': 'Diesen Eintrag gibt es schon' });
              break;
            case 400:
            case 409:
            case 412:
            case 902:
            case 904:
            case 500:
              message = new Message({ 'level': ERROR, 'message': 'Es ist ein unerwarteter Fehler aufgetreten: ' + status });
              break;
            default: message = new Message({ 'level': ERROR, 'message': 'Unerwarteter Statuscode: ' + status });
              break;

          }
        }

        this._messageService.setMessage(message);
        return _throw(errMsg);
      });
  }
}

/**
 * Provider POJO for the interceptor
 */
export const ErrorInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: ErrorInterceptor,
  multi: true,
};

