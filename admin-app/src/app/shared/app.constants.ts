
export const ACTIVE = 'ACTIVE';
export const INACTIVE = 'INACTIVE';
export const PENDING = 'PENDING';

export class AppConstants {

  public static colors = {
    'DARK_GRAY': '#555753',
    'WHITE': '#ffffff'
  };

  public static backgroundColors = {
    'INPUT_INVALID': '#fffae6', // hellgelb
    'INPUT_VALID': '#eefbe9',  // hellgrün
    'INPUT_NEUTRAL': '#ffffff', // weiß
    'INPUT_MISSING': '#d3d7cf', // grau
    'ZU_LEICHT': '#fffae6', // hellgelb
    'ZU_SCHWER': '#ffd699', // hellorange
    'WEISS': '#ffffff', // weiß
    'GRAU': '#d3d7cf', // grau
    'HELLGRUEN': '#eefbe9',  // hellgrün
    'HELLORANGE': '#ffd699',
    'HELLGELB': '#fffae6', // hellgelb
  };
}


