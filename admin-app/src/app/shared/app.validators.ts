/**
 * Validatoren für alles Mögliche
 */

import {FormGroup, FormControl, AbstractControl} from '@angular/forms';

export function emailValidator(control): {
  [key: string]: any
} {
  const re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

  if (!control.value || control.value === '' || re.test(control.value)) {
    return null;
  } else {
    return {'invalidEMail': true};
  }
}

export function passwordValidator(control): {
  [key: string]: any
} {
  const re = /(?=[^A-ZÄÖÜa-zäöüß]*[A-ZÄÖÜa-zäöüß])(?=[^\d]*[\d])[A-Za-z0-9ÄÖÜäöüß !&quot;#\$%&amp;'\(\)\*\+,\-\.\/:&lt;=&gt;\?@\[\]\^\\_`'{|}~ ]{8,100}/;

  if (!control.value || control.value === '' || re.test(control.value)) {
    return null;
  } else {
    return {'invalidPassword': true};
  }
}

export function allValuesBlankValidator(values: string[]): {
  [key: string]: any
} {
  const _valuesFiltered = values.filter((value: string) => value && value.trim().length > 0);
  return _valuesFiltered.length === 0 ? {'allBlank': true} : null;
}

export function allValuesNoneBlankValidator(strs: string[]): {
  [key: string]: any
} {
  let anz = 0;
  for (let i = 0; i < strs.length; i++) {
    const v = strs[i];
    if (v && v.trim().length > 0){
      anz++;
    }
  }
  return anz === strs.length ? {'allNoneBlank': true} : null;
}

export function validateAllFormFields(formGroup: FormGroup): void {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({onlySelf: true});
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
}

export function resetForm(formGroup: FormGroup): void {
  formGroup.reset();
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    control.setErrors(null);
  });
}


