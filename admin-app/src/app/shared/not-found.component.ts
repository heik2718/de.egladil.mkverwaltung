import {Component} from '@angular/core';

@Component({
  selector: 'admin-not-found',
  templateUrl: 'not-found.component.html',
})
export class NotFoundComponent {
}
