import {Injectable, Inject} from '@angular/core';
import { LogService } from 'hewi-ng-lib';


@Injectable()
export class PagerService {

  constructor(private _logger: LogService) {}

  getPager(totalItems: number, currentPage: number = 1, itemsPerPage: number = 10) {
    // calculate total pages
    let totalPages = 1;
    if (itemsPerPage > 0) {
      totalPages = Math.ceil(totalItems / itemsPerPage);
    }

    this._logger.debug('totalItems=' + totalItems + ', pageSize=' + itemsPerPage + ', totalPages=' + totalPages);

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = Math.min(startIndex + itemsPerPage - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    const pages = this._range(startPage, endPage, totalPages);

    // return object with all pager properties required by the view
    const _result = {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: itemsPerPage,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };

    this._logger.debug(JSON.stringify(_result));
    return _result;
  }

  private _range(_startPage: number, _endPage: number, _maxPages: number): any {
    this._logger.debug('_startPage=' + _startPage + ', _endPage=' + _endPage + ', _maxPages=' + _maxPages);
    const _result = [];
    const _last = Math.min(_endPage, _maxPages);
    for (let i = 0; i < _last; i++) {
      const index = _startPage + i;
      if (index <= _last) {
        _result.push(index);
      }
    }
    this._logger.debug(JSON.stringify(_result));
    return _result;
  }
}

