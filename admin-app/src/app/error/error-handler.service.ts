import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LogService, MessagesService } from 'hewi-ng-lib';
import { LogPublishersService } from '../logger/log-publishers.service';
import { environment } from '../../environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    private logService: LogService;

    constructor (private injector: Injector) {

        // ErrorHandler wird vor allen anderen Injectables instanziiert,
        // so dass man benötigte Services nicht im Konstruktor injekten kann !!!

        if (this.logService === undefined || this.logService === null) {

            const logPublishersService = this.injector.get(LogPublishersService);
            this.logService = this.injector.get(LogService);

            this.logService.initLevel(environment.loglevel);
            this.logService.registerPublishers(logPublishersService.publishers);
            this.logService.info('logging initialisiert: loglevel=' + environment.loglevel);

        }
    }


    handleError(error: any): void {

        let message = 'admin-app: unerwarteter Fehler aufgetreten: ';

        if (error && error.message) {
            message += error.message;
        }

        this.logService.error(message);

        if (error instanceof HttpErrorResponse) {
            this.logService.warn(message + ' (das sollte nicht vorkommen, da doese Errors von einem derr Services behandelt werden)');
        } else {
            this.logService.error(message);
        }

        this.injector.get(MessagesService).error(message);
    }
}

