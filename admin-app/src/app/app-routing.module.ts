import { NgModule, OnDestroy } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { AppComponent } from './app.component';
import { AuswertungstabellenComponent } from './file/auswertungstabellen.component';
import { BenutzerListComponent } from './benutzer/benutzerList.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GesamtpunktverteilungComponent } from './statistik/gesamtuebersicht/gesamtpunktverteilung.component';
import { KatalogeComponent } from './kataloge/kataloge.component';
import { KontextComponent } from './kontext/kontext.component';
import { LaenderListComponent } from './kataloge/laender/laenderList.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { MailComponent } from './mail/mail.component';
import { NotFoundComponent } from './shared/not-found.component';
import { OrteListComponent } from './kataloge/orte/orteList.component';
import { PunktverteilungComponent } from './statistik/gesamtuebersicht/punktverteilung.component';
import { RootgruppeDetailsComponent } from './rootgruppe-details/rootgruppe-details.component';
import { AuswertungsgruppenListComponent } from './auswertungsgruppen/auswertungsgruppenList.component';
import { SchulenListComponent } from './kataloge/schulen/schulenList.component';
import { StatistikComponent } from './statistik/statistik.component';
import { TeilnahmegruppenListComponent } from './statistik/teilnahmegruppenlist.component';


import { LoggedInGuard } from './shared/loggedIn.guard';

const appRoutes: Routes = [
  { path: 'landing', component: LandingComponent }
  , { path: 'login', component: LoginComponent }
  , { path: 'auswertungstabellen', component: AuswertungstabellenComponent, canActivate: [LoggedInGuard] }
  , { path: 'benutzer', component: BenutzerListComponent, canActivate: [LoggedInGuard] }
  , { path: 'dashboard', component: DashboardComponent, canActivate: [LoggedInGuard] }
  , { path: 'kataloge', component: KatalogeComponent, canActivate: [LoggedInGuard] }
  , { path: 'kataloge/schulen', component: SchulenListComponent, canActivate: [LoggedInGuard] }
  , { path: 'kataloge/orte', component: OrteListComponent, canActivate: [LoggedInGuard] }
  , { path: 'kataloge/laender', component: LaenderListComponent, canActivate: [LoggedInGuard] }
  , { path: 'kontext', component: KontextComponent, canActivate: [LoggedInGuard] }
  , { path: 'mail', component: MailComponent, canActivate: [LoggedInGuard] }
  , { path: 'schulen', component: AuswertungsgruppenListComponent, canActivate: [LoggedInGuard] }
  , { path: 'schulen/auswertungsgruppe/:kuerzel', component: RootgruppeDetailsComponent, canActivate: [LoggedInGuard] }
  , { path: 'statistik', component: StatistikComponent, canActivate: [LoggedInGuard] }
  , { path: 'statistik/uebersicht/:jahr', component: TeilnahmegruppenListComponent, canActivate: [LoggedInGuard] }
  , { path: 'statistik/gesamtpunktverteilung/:jahr', component: GesamtpunktverteilungComponent, canActivate: [LoggedInGuard] }
  , { path: 'statistik/punktverteilung', component: PunktverteilungComponent, canActivate: [LoggedInGuard] }
  /** Redirect Konfigurationen **/
  , { path: '', redirectTo: '/landing', pathMatch: 'full' }
  , { path: '**', component: NotFoundComponent }, // immer als letztes konfigurieren - erste Route die matched wird angesteuert

];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const appRouting = RouterModule.forRoot(appRoutes, { useHash: true });

export const routingComponents = [AppComponent
  , LoginComponent
];

export const routingProviders = [LoggedInGuard];
