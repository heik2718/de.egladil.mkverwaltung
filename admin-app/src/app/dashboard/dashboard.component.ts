import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _router: Router) {}

  ngOnInit() {
  }

  gotoStatistik(): void {
    this._router.navigate(['/statistik']);
  }

  gotoAuswertungsgruppen(): void {
    this._router.navigate(['/schulen']);
  }

  gotoKataloge(): void {
    this._router.navigate(['/kataloge']);
  }

  gotoBenutzer(): void {
    this._router.navigate(['/benutzer']);
  }

  gotoMail(): void {
    this._router.navigate(['/mail']);
  }

  gotoUploads(): void {
    this._router.navigate(['/auswertungstabellen']);
  }

  gotoKontext(): void {
    this._router.navigate(['/kontext']);
  }

}
