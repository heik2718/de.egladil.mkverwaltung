import {Component, OnInit, OnDestroy, Inject, ViewChild} from '@angular/core';
import {NgbCollapse} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';
import {SessionService} from '../login/session.service';

@Component({
  selector: 'admin-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit, OnDestroy {

  public isCollapsed: boolean;
  public loggedIn: boolean;

  private _unsubscribe: Unsubscribe;

  @ViewChild(NgbCollapse, { static: true }) navbarToggler: NgbCollapse;


  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private _router: Router
    , private _userService: SessionService
    , private _logger: LogService) {
    this.loggedIn = false;
  }

  ngOnInit() {
    this.isCollapsed = true;

    this._unsubscribe = this._store.subscribe(() => {
      this._logger.debug('NavbarComponent: state changed');
      const state = this._store.getState();
      const user = state.session.sessionToken;
      if (user && user.accessToken !== null) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  collapseNav() {
    if (this._navBarTogglerIsVisible()) {
      this._logger.debug('visible');
      this.isCollapsed = true;
    }
  }

  gotoLogin(): void {
    this.isCollapsed = true;
    this._router.navigate(['/login']);
  }

  logout(): void {
    this._logger.debug('jetzt ausloggen');
    this.collapseNav();
    this._userService.logout();
  }

  private _navBarTogglerIsVisible() {
    if (this.navbarToggler === null || this.navbarToggler === undefined) {
      return false;
    }
    return true;
  }
}
