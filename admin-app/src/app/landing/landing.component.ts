import {Component, Inject, OnInit, OnDestroy} from '@angular/core';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';
import {Kontext} from '../kontext/kontext.model';
import {SessionToken} from '../login/session.model';

@Component({
  selector: 'admin-landing',
  templateUrl: './landing.component.html'
})
export class LandingComponent implements OnInit, OnDestroy {

  public wettbewerbsjahr: string;


  private _csrfToken: string;
  private _unsubscribe: Unsubscribe;

  constructor( @Inject(AppStore) private _store: Store<AppState>, private _logger: LogService) {
    this._reset();
  }

  ngOnInit() {

    this._readState();

    this._unsubscribe = this._store.subscribe(() => {
      this._logger.debug('state changed');
      this._readState();
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  private _readState(): void {
    const state = this._store.getState();
    const k = state.kontext.kontext;
    if (k !== null) {
      this.wettbewerbsjahr = k.wettbewerbsjahr;
      this._csrfToken = k.xsrfToken;
    } else {
      this._reset();
    }
  }

  private _reset(): void {
    this.wettbewerbsjahr = '';
    this._csrfToken = '';
  }
}
