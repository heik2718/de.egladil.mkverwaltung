import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MailRoutingModule } from './mail-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MailRoutingModule
  ],
  declarations: []
})
export class MailModule { }
