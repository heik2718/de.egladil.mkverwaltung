import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, RouterStateSnapshot} from '@angular/router';

import {LaenderService} from './laender/laender.service';


@Component({
  selector: 'admin-kataloge',
  templateUrl: './kataloge.component.html',
})
export class KatalogeComponent implements OnInit {

  constructor(private _router: Router
    , private _route: ActivatedRoute
    , private _laenderService: LaenderService
  ) {}


  ngOnInit() {
  }


  gotoSchulen(): void {
    this._router.navigate(['schulen'], {relativeTo: this._route});
  }

  gotoOrte(): void {
    this._router.navigate(['orte'], {relativeTo: this._route});
  }

  gotoLaender(): void {
    this._laenderService.loadLaender();
    this._router.navigate(['laender'], {relativeTo: this._route});
  }
}


