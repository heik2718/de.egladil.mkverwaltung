
export const ROOT = 0;
export const LAND = 1;
export const ORT = 2;
export const SCHULE = 3;

export class OrtSuchePayload {
  landkuerzel: string;
  name: string;

  constructor(data: any = {}) {
    this.landkuerzel = data && data.landkuerzel || null;
    this.name = data && data.name || null;
  }
}



