import { Component, OnInit, OnDestroy, Inject, SimpleChanges, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';

import { Store, Unsubscribe } from 'redux';
import { Subject } from 'rxjs/Subject';
import { LogService } from 'hewi-ng-lib';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';

import { AppState } from '../../app.reducer';
import { AppStore } from '../../app.store';
import { ERROR } from '../../messages/message.model';
import { MessageService } from '../../messages/message.service';

import { Land } from './+state/laender.model';
import { LaenderService } from './laender.service';
import { clearLaender } from './+state/laender.actions';
import { validateAllFormFields, resetForm, allValuesBlankValidator } from '../../shared/app.validators';


@Component({
  selector: 'admin-laender',
  templateUrl: './laenderList.component.html',
  styleUrls: ['./laenderList.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class LaenderListComponent implements OnInit, OnDestroy {

  landAnlegenForm: FormGroup;
  kuerzel: AbstractControl;
  name: AbstractControl;

  anlegenFormVisible: boolean;
  cancelled: boolean;
  formSubmitAttempt: boolean;


  laender: Land[];

  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _messageService: MessageService
    , private _laenderService: LaenderService
    , private _logger: LogService) {

    this.landAnlegenForm = _fb.group({
      'kuerzel': ['', [Validators.required, Validators.maxLength(5)]],
      'name': ['', [Validators.required, Validators.maxLength(100)]]
    });

    this.kuerzel = this.landAnlegenForm.controls['kuerzel'];
    this.name = this.landAnlegenForm.controls['name'];
    this.laender = [];
    this.anlegenFormVisible = false;
    this.cancelled = false;
    this.formSubmitAttempt = false;
  }


  ngOnInit() {

    this.laender = [];

    this._unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this._logger.debug('state.laender changed');
      this.laender = state.laender.laender;
      this.laender = this._sortLaender(this.laender);
    });

    if (this._store.getState().laender.laender.length === 0) {
      this._laenderService.loadLaender();
    }
  }

  ngOnDestroy() {
    this._messageService.clearMessage();
    this._unsubscribe();
  }

  leeren(): void {
    this._store.dispatch(clearLaender());
    this._messageService.clearMessage();
  }

  onSubmit(value: any): void {
    const _name = value.name && value.name.trim() || null;
    const _kuerzel = value.kuerzel && value.kuerzel.trim() || null;

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.landAnlegenForm.valid) {
      this._laenderService.landAnlegen(_kuerzel, _name);
      this.cancelled = false;
    } else {
      validateAllFormFields(this.landAnlegenForm);
    }
  }

  cancelLandAnlegen(): void {
    this.cancelled = true;
    this.formSubmitAttempt = false;
    resetForm(this.landAnlegenForm);
    this.anlegenFormVisible = false;
    this._messageService.clearMessage();
  }

  neuesLand(): void {
    this.anlegenFormVisible = true;
  }

  private _sortLaender(laender: Land[]): Land[] {
    let _sorted: Land[];
    _sorted = laender.slice(0);
    _sorted.sort((arg0, arg1): number => {
      if (arg0.name < arg1.name) {
        return -1;
      }
      if (arg0.name > arg1.name) {
        return 1;
      }
      return 0;
    });
    return _sorted;
  }
}






