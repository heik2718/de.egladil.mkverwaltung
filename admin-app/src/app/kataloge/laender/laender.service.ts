import { Injectable, Inject } from '@angular/core';
import { Store } from 'redux';

// own app
import { AppState } from '../../app.reducer';
import { AppStore } from '../../app.store';
import { ResponsePayload } from '../../shared/responsePayload.model';
import { Land } from './+state/laender.model';
import { addLand, loadLaender, LandAddAction, LAND_ADD } from './+state/laender.actions';

// infrastructure
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Message } from '../../messages/message.model';
import { MessageService } from '../../messages/message.service';
import { LogService } from 'hewi-ng-lib';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { catchError, retry } from 'rxjs/operators';

const BASE_URL = environment.apiUrl;

@Injectable()
export class LaenderService {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _http: HttpClient,
    private _messageService: MessageService,
    private _logger: LogService) { }


  loadLaender(): void {

    const _url = BASE_URL + '/laender';

    const _future$ = this._http.get(_url);

    _future$.subscribe((response: ResponsePayload<Land[]>) => {
      const _laender = response.payload;
      this._logger.debug('Anzahl Länder: ' + _laender.length);
      this._store.dispatch(loadLaender(_laender));
    });
  }

  landAnlegen(kuerzel: string, name: string): void {
    this._logger.debug('start login');
    const url = BASE_URL + '/laender';

    const _payload = { 'kuerzel': kuerzel.toUpperCase(), 'name': name };

    this._http.post<ResponsePayload<Land>>(url, _payload, {}).subscribe(
      (data) => {
        this._logger.debug(JSON.stringify(data));
        const _message: Message = data.apiMessage;
        this._messageService.setMessage(_message);

        const _land = new Land(data.payload);
        this._store.dispatch(addLand(_land));
      });
  }
}



