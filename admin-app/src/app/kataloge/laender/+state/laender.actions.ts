import {Action, ActionCreator} from 'redux';
import {Land} from './laender.model';

export const LAENDER_LOAD = '[laender] load';
export const LAENDER_CLEAR = '[laender] clear';
export const LAND_ADD = '[lander] add';
export const LAND_UPDATE = '[laender] update';

export interface LaenderLoadAction extends Action {
  payload: Land[];
}

export interface LaenderClearAction extends Action {
  payload: Land[];
}

export interface LandAddAction extends Action {
  payload: Land;
}

export interface LandUpdateAction extends Action {
  payload: Land;
}

export const loadLaender: ActionCreator<LaenderLoadAction> =
  (g) => ({
    type: LAENDER_LOAD,
    payload: g
  });

export const clearLaender: ActionCreator<LaenderClearAction> =
  (g) => ({
    type: LAENDER_CLEAR,
    payload: []
  });

export const addLand: ActionCreator<LandAddAction> =
  (g) => ({
    type: LAND_UPDATE,
    payload: g
  });

export const updateLand: ActionCreator<LandUpdateAction> =
  (g) => ({
    type: LAND_UPDATE,
    payload: g
  });

