import {HateoasPayload} from '../../../shared/responsePayload.model';

export class Land {
  kuerzel: string;
  name: string;
  anzahlOrte: number;
  teilnahmejahre: string[];
  hateoasPayload: HateoasPayload;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.name = data && data.name || null;
    this.anzahlOrte = data && data.anzahlOrte || 0;
    this.teilnahmejahre = data && data.teilnahmejahre || [];
    this.hateoasPayload = data && data.hateoasPayload || null;
  }
}

export class LandTeilnahme {
  jahr: string;
  anzahlSchulen: number;
  anzahlKinder: number;
  hateoasPayload: HateoasPayload;

  constructor(data: any = {}) {
    this.jahr = data && data.jahr || '';
    this.anzahlSchulen = data && data.anzahlSchulen || 0;
    this.anzahlKinder = data && data.anzahlKinder || 0;
    this.hateoasPayload = data && data.hateoasPayload || null;
  }
}



