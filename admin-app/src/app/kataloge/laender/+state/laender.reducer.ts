import {Action} from 'redux';

import {Land} from './laender.model';
import {LaenderState, initialLaenderState} from './laender.state';
import * as LaenderAction from './laender.actions';

export const LaenderReducer = function(state: LaenderState = initialLaenderState, action: Action): LaenderState {

  switch (action.type) {
    case (LaenderAction.LAENDER_LOAD):
      const _laender: Land[] = (<LaenderAction.LaenderLoadAction>action).payload;
      return {
        laender: _laender
      };
    case (LaenderAction.LAND_ADD):
      const _neuesLand: Land = (<LaenderAction.LandAddAction>action).payload;
      console.log(JSON.stringify(_neuesLand));
      return {
        laender: [_neuesLand, ...state.laender]
      };
    case (LaenderAction.LAND_UPDATE):
      const _geaendertesLand: Land = (<LaenderAction.LandUpdateAction>action).payload;
      const _geaenderteLaender: Land[] = state.laender.map(land => {
        if (land.kuerzel !== _geaendertesLand.kuerzel) {
          return land;
        }
        return _geaendertesLand;
      });

      return {
        laender: _geaenderteLaender
      };
    case (LaenderAction.LAENDER_CLEAR): return initialLaenderState;
    default: return state;
  }
};

