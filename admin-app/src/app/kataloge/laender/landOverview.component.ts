import { Component, OnInit, Inject, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';

import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';

import { MessageService } from '../../messages/message.service';
import { OrteService } from '../orte/orte.service';
import { AppStore } from '../../app.store';
import { AppState } from '../../app.reducer';
import { Land } from './+state/laender.model';
import { validateAllFormFields, resetForm, allValuesBlankValidator } from '../../shared/app.validators';


@Component({
  selector: 'admin-land-overview',
  templateUrl: './landOverview.component.html'
})
export class LandOverviewComponent implements OnInit, OnDestroy {

  @Input() land: Land;
  private _unsubscribe: Unsubscribe;

  ortAnlegenForm: FormGroup;
  name: AbstractControl;

  anlegenFormVisible: boolean;
  cancelled: boolean;
  formSubmitAttempt: boolean;



  private _styles: {};

  constructor (private _router: Router
    , private _route: ActivatedRoute
    , @Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _orteService: OrteService
    , private _messageService: MessageService
    , private _logger: LogService) {

    this.ortAnlegenForm = _fb.group({
      'name': ['', [Validators.required, Validators.maxLength(100)]]
    });

    this.name = this.ortAnlegenForm.controls['name'];
    this.formSubmitAttempt = false;
    this.anlegenFormVisible = false;
    this.cancelled = false;
  }

  ngOnInit() {
    this._unsubscribe = this._store.subscribe(() => {
      const _state = this._store.getState();

      const _laender = this._store.getState().laender.laender;
      const _gefiltert = _laender.filter((l: Land) => l.kuerzel === this.land.kuerzel);
      if (_gefiltert.length === 1) {
        this.land = _gefiltert[0];
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  loadTeilnahmen(i): void {
    const _jahr = this.land.teilnahmejahre[i];
    this._logger.debug(this.land.kuerzel + ': Teilnahmen zu Jahr ' + _jahr + ' laden');
  }

  onSubmit(value: any): void {
    const _name = value.name && value.name.trim() || null;

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.ortAnlegenForm.valid) {
      this._orteService.ortAnlegen(this.land, _name);
      this.cancelled = false;
    } else {
      validateAllFormFields(this.ortAnlegenForm);
    }
  }

  toggleNeuerOrt(): void {
    this._messageService.clearMessage();
    this.formSubmitAttempt = false;

    if (this.anlegenFormVisible) {
      this._cancelOrtAnlegen();
    } else {
      this._neuerOrt();
    }
  }

  private _cancelOrtAnlegen(): void {
    this._messageService.clearMessage();
    this.anlegenFormVisible = false;
    this.cancelled = true;
  }

  private _neuerOrt(): void {
    this._messageService.clearMessage();
    this.anlegenFormVisible = true;
    this.formSubmitAttempt = false;
    this.cancelled = false;
  }
}

