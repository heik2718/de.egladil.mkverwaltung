import { Injectable, Inject } from '@angular/core';
import { Store } from 'redux';

// own app
import { AppState } from '../../app.reducer';
import { AppStore } from '../../app.store';
import { ResponsePayload } from '../../shared/responsePayload.model';

// infrastructure
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Ort } from './+state/orte.model';
import { Land } from '../laender/+state/laender.model';
import { addOrt, loadOrte } from './+state/orte.actions';
import { updateLand } from '../laender/+state/laender.actions';
import { Message } from '../../messages/message.model';
import { MessageService } from '../../messages/message.service';

import { LogService } from 'hewi-ng-lib';

const BASE_URL = environment.apiUrl;

@Injectable()
export class OrteService {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _http: HttpClient,
    private _messageService: MessageService,
    private _logger: LogService) { }


  findOrte(name: string, land: string): void {

    const _url = BASE_URL + '/orte';

    let query = {};
    if (name && name.length > 0 && land && land.length > 0) {
      query = {
        'name': name,
        'land': land
      };
    } else {
      if (name && name.length > 0) {
        query = {
          'name': name
        };
      } else {
        query = {
          'land': land
        };
      }
    }

    const _params = new HttpParams({
      fromObject: query
    });

    this._logger.debug(JSON.stringify(_params));

    const _future$ = this._http.get(_url, { params: _params });

    _future$.subscribe((response: ResponsePayload<Ort[]>) => {
      const _orte = response.payload;
      this._store.dispatch(loadOrte(_orte));
    });
  }

  ortAnlegen(land: Land, name: string): void {
    this._logger.debug('start login');
    const url = BASE_URL + '/orte';

    const _payload = { 'landkuerzel': land.kuerzel.toUpperCase(), 'name': name };

    this._http.post<ResponsePayload<Ort>>(url, _payload, {}).subscribe(
      (data) => {
        this._logger.debug(JSON.stringify(data));
        const _message: Message = data.apiMessage;
        this._messageService.setMessage(_message);

        const _ort = new Ort(data.payload);
        this._store.dispatch(addOrt(_ort));

        land.anzahlOrte++;
        this._store.dispatch(updateLand(land));
      });
  }
}


