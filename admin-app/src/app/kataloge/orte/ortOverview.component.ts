import { Component, OnInit, Inject, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';

import { AppStore } from '../../app.store';
import { AppState } from '../../app.reducer';
import { Ort } from './+state/orte.model';
import { Schule } from '../schulen/+state/schulen.model';
import { validateAllFormFields, resetForm, allValuesBlankValidator } from '../../shared/app.validators';

import { MessageService } from '../../messages/message.service';
import { SchulenService } from '../schulen/schulen.service';

@Component({
  selector: 'admin-ort-overview',
  templateUrl: './ortOverview.component.html'
})
export class OrtOverviewComponent implements OnInit, OnDestroy {

  @Input() ort: Ort;

  private _styles: {};
  private _unsubscribe: Unsubscribe;


  schuleAnlegenForm: FormGroup;
  name: AbstractControl;
  strasse: AbstractControl;
  url: AbstractControl;

  anlegenFormVisible: boolean;
  formSubmitAttempt: boolean;
  cancelled: boolean;

  constructor (private _router: Router
    , private _route: ActivatedRoute
    , @Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _schulenService: SchulenService
    , private _messageService: MessageService
    , private _logger: LogService) {

    this.schuleAnlegenForm = _fb.group({
      'name': ['', [Validators.required, Validators.maxLength(100)]],
      'strasse': ['', [Validators.required, Validators.maxLength(100)]],
      'url': ['', [Validators.maxLength(2000)]]
    });

    this.name = this.schuleAnlegenForm.controls['name'];
    this.strasse = this.schuleAnlegenForm.controls['strasse'];
    this.url = this.schuleAnlegenForm.controls['url'];

    this.anlegenFormVisible = false;
    this.formSubmitAttempt = false;
    this.cancelled = false;
  }

  ngOnInit() {

    this.anlegenFormVisible = false;
    this.formSubmitAttempt = false;
    this.cancelled = false;

    this._unsubscribe = this._store.subscribe(() => {
      const _state = this._store.getState();

      const _orte = this._store.getState().orte.orte;
      const _gefiltert = _orte.filter((o: Ort) => o.kuerzel === this.ort.kuerzel);
      if (_gefiltert.length === 1) {
        this.ort = _gefiltert[0];
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  onSubmit(value: any): void {
    const _name = value.name && value.name.trim() || null;
    const _strasse = value.strasse && value.strasse.trim() || null;
    const _url = value.url && value.url.trim() || null;

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.schuleAnlegenForm.valid) {
      this._schulenService.schuleAnlegen(this.ort, _name, _strasse, _url);
      this.cancelled = false;
    } else {
      validateAllFormFields(this.schuleAnlegenForm);
    }
  }

  private _cancelSchuleAnlegen(): void {
    this.anlegenFormVisible = false;
    this.cancelled = true;
  }

  toggleNeueSchule(): void {
    this._messageService.clearMessage();
    this.formSubmitAttempt = false;

    if (this.anlegenFormVisible) {
      this._cancelSchuleAnlegen();
    } else {
      this._neueSchule();
    }
  }

  private _neueSchule(): void {
    this.anlegenFormVisible = true;
    this.cancelled = false;
  }

  neuerOrt(): void {
    this._messageService.clearMessage();
    this.anlegenFormVisible = true;
    this.formSubmitAttempt = false;
    this.cancelled = false;
  }
}
