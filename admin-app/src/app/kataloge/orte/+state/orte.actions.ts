import {Action, ActionCreator} from 'redux';
import {Ort} from './orte.model';


export const ORTE_LOAD = '[orte] load';
export const ORTE_CLEAR = '[orte] clear';
export const ORT_ADD = '[orte] add';
export const ORT_UPDATE = '[orte] update';

export interface OrteLoadAction extends Action {
  payload: Ort[];
}

export interface OrteClearAction extends Action {
  payload: Ort[];
}

export interface OrtAddAction extends Action {
  ort: Ort;
}

export interface OrtUpdateAction extends Action {
  ort: Ort;
}

export const loadOrte: ActionCreator<OrteLoadAction> =
  (g) => ({
    type: ORTE_LOAD,
    payload: g
  });

export const clearOrte: ActionCreator<OrteClearAction> =
  (g) => ({
    type: ORTE_CLEAR,
    payload: []
  });


export const updateOrt: ActionCreator<OrtUpdateAction> =
  (g) => ({
    type: ORT_UPDATE,
    ort: g
  });

export const addOrt: ActionCreator<OrtAddAction> =
  (g) => ({
    type: ORT_ADD,
    ort: g
  });
