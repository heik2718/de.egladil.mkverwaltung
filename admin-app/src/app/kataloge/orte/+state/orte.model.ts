import {HateoasPayload} from '../../../shared/responsePayload.model';


export class OrtLage {
  landkuerzel: string;
  land: string;

  constructor(data: any = {}) {
    this.landkuerzel = data && data.landkuerzel || null;
    this.land = data && data.land || null;
  }
}

export class Ort {
  kuerzel: string;
  name: string;
  lage: OrtLage;
  anzahlSchulen: number;
  hateoasPayload: HateoasPayload;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.name = data && data.name || null;
    this.lage = data && data.lage || null;
    this.anzahlSchulen = data && data.anzahlSchulen || 0;
    this.hateoasPayload = data && data.hateoasPayload || null;
  }
}



