import {Action} from 'redux';

import {Ort} from './orte.model';
import {OrteState, initialOrteState} from './orte.state';
import * as OrteAction from './orte.actions';

export const OrteReducer = function(state: OrteState = initialOrteState, action: Action): OrteState {

  switch (action.type) {
    case (OrteAction.ORTE_LOAD):
      const _orte: Ort[] = (<OrteAction.OrteLoadAction>action).payload;
      return {
        orte: _orte
      };
    case (OrteAction.ORT_ADD):
      const _neuerOrt: Ort = (<OrteAction.OrtAddAction>action).ort;
      console.log(JSON.stringify(_neuerOrt));
      return {
        orte: [_neuerOrt, ...state.orte]
      };
    case (OrteAction.ORT_UPDATE):
      const _geaenderterOrt = (<OrteAction.OrtUpdateAction>action).ort;
      const _geaenderteOrte: Ort[] = state.orte.map(ort => {
        if (ort.kuerzel !== _geaenderterOrt.kuerzel) {
          return ort;
        }
        return _geaenderterOrt;
      });
      return {
        orte: _geaenderteOrte
      };
    case (OrteAction.ORTE_CLEAR): return initialOrteState;
    default: return state;
  }
};



