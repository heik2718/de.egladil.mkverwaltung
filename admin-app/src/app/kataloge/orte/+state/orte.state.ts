import {Ort} from './orte.model';

export interface OrteState {
  orte: Ort[];
}

export const initialOrteState: OrteState = {orte: []};

