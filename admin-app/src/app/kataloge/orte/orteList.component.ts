import {Component, OnInit, OnDestroy, Inject, SimpleChanges, ChangeDetectionStrategy, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {Store, Unsubscribe} from 'redux';
import {Subject} from 'rxjs/Subject';
import { LogService } from 'hewi-ng-lib';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {Subscription} from 'rxjs/Subscription';

import {AppState} from '../../app.reducer';
import {AppStore} from '../../app.store';
import {ERROR} from '../../messages/message.model';
import {MessageService} from '../../messages/message.service';
import {PagerService} from '../../shared/pager.service';
import {Ort} from './+state/orte.model';
import {clearOrte} from './+state/orte.actions';
import {OrteService} from './orte.service';
import {validateAllFormFields, resetForm, allValuesBlankValidator} from '../../shared/app.validators';

@Component({
  selector: 'admin-orte',
  templateUrl: './orteList.component.html',
  styleUrls: ['./orteList.component.css']
})
export class OrteListComponent implements OnInit, OnDestroy {

  ortsucheForm: FormGroup;
  ortname: AbstractControl;
  landname: AbstractControl;

  orte: Ort[];

  formSubmitAttempt: boolean;
  cancelled: boolean;

  pager: any;
  private _numberPages: number;
  private _suchparameter: {};

  private _unsubscribe: Unsubscribe;

  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _messageService: MessageService
    , private _pagerService: PagerService
    , private _orteService: OrteService
    , private _logger: LogService) {

    this.ortsucheForm = _fb.group({
      'ortname': ['', [Validators.required, Validators.maxLength(100)]],
      'landname': ['', [Validators.maxLength(100)]]
    });

    this.ortname = this.ortsucheForm.controls['ortname'];
    this.landname = this.ortsucheForm.controls['landname'];

    this.pager = {};
    this._numberPages = 10;

  }

  ngOnInit() {
    this.formSubmitAttempt = false;
    this.cancelled = true;

    this._unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this.orte = state.orte.orte;
    });
  }

  ngOnDestroy() {
    this._messageService.clearMessage();
    this._unsubscribe();
  }

  onSubmit(value: any): void {
    const _name = value.ortname && value.ortname.trim() || null;
    const _land = value.landname && value.landname.trim() || null;

    if (_name === null && _land === null) {
      if (!this.cancelled) {
        this._messageService.setMessage({level: ERROR, message: 'mindestens ein Suchparameter erforderlich.'});
      }
      return;
    }

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.ortsucheForm.valid) {
      this._orteService.findOrte(_name, _land);
      this.cancelled = false;
    } else {
      validateAllFormFields(this.ortsucheForm);
    }
  }


  cancel(): void {
    this.cancelled = true;
    this.formSubmitAttempt = false;
    resetForm(this.ortsucheForm);
    this._store.dispatch(clearOrte());
    this._messageService.clearMessage();
  }
}
