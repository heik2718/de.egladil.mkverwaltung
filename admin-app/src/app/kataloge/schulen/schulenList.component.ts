import {Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy} from '@angular/core';
import {FormBuilder, FormGroup, AbstractControl, Validators} from '@angular/forms';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {AppState} from '../../app.reducer';
import {AppStore} from '../../app.store';
import {ERROR} from '../../messages/message.model';
import {MessageService} from '../../messages/message.service';
import {PagerService} from '../../shared/pager.service';
import {Schule} from './+state/schulen.model';
import {clearSchulen} from './+state/schulen.actions';
import {SchulenService} from './schulen.service';
import {validateAllFormFields, resetForm} from '../../shared/app.validators';


@Component({
  selector: 'admin-schulen',
  templateUrl: './schulenList.component.html',
  styleUrls: ['./schulenList.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class SchulenListComponent implements OnInit, OnDestroy {


  schulsucheForm: FormGroup;
  schulname: AbstractControl;
  ortname: AbstractControl;

  schulen: Schule[];

  formSubmitAttempt: boolean;
  cancelled: boolean;

  private _unsubscribe: Unsubscribe;

  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _messageService: MessageService
    , private _pagerService: PagerService
    , private _schulenService: SchulenService
    , private _logger: LogService) {

    this.schulsucheForm = this._fb.group({
      'schulname': ['', [Validators.maxLength(100)]],
      'ortname': ['', [Validators.maxLength(100)]]
    });

    this.schulname = this.schulsucheForm.controls['schulname'];
    this.ortname = this.schulsucheForm.controls['ortname'];

  }


  ngOnInit() {

    this.formSubmitAttempt = false;
    this.cancelled = true;

    this._unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this.schulen = state.schulen.schulen;
    });
  }

  ngOnDestroy() {
    this._messageService.clearMessage();
    this._unsubscribe();
  }

  onSubmit(value: any): void {
    const _name = value.schulname && value.schulname.trim() || null;
    const _ort = value.ortname && value.ortname.trim() || null;

    if (_name === null && _ort === null) {
      if (!this.cancelled) {
        this._messageService.setMessage({level: ERROR, message: 'mindestens ein Suchparameter erforderlich.'});
      }
      return;
    }

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.schulsucheForm.valid) {
      this._schulenService.findSchulen(_name, _ort);
      this.cancelled = false;
    } else {
      validateAllFormFields(this.schulsucheForm);
    }
  }

  cancel(): void {
    this.cancelled = true;
    this.formSubmitAttempt = false;
    resetForm(this.schulsucheForm);
    this._store.dispatch(clearSchulen());
    this._messageService.clearMessage();
  }
}



