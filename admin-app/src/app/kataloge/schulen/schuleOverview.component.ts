import {Component, OnInit, Inject, Input, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router, RouterStateSnapshot} from '@angular/router';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppStore} from '../../app.store';
import {AppState} from '../../app.reducer';
import {AppConstants} from '../../shared/app.constants';
import {Schule} from './+state/schulen.model';
import {validateAllFormFields, resetForm, allValuesBlankValidator} from '../../shared/app.validators';

import {MessageService} from '../../messages/message.service';
import {SchulenService} from '../schulen/schulen.service';


@Component({
  selector: 'admin-schule-overview',
  templateUrl: './schuleOverview.component.html'
})
export class SchuleOverviewComponent implements OnInit, OnDestroy {

  @Input() schule: Schule;
  private _styles: {};
  private _unsubscribe: Unsubscribe;

  schuleAendernForm: FormGroup;
  name: AbstractControl;
  strasse: AbstractControl;
  url: AbstractControl;

  aendernFormVisible: boolean;
  formSubmitAttempt: boolean;
  cancelled: boolean;


  constructor(private _router: Router
    , private _route: ActivatedRoute
    , @Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _messageService: MessageService
    , private _schulenService: SchulenService
    , private _logger: LogService) {


    this.schuleAendernForm = _fb.group({
      'name': ['', [Validators.required, Validators.maxLength(100)]],
      'strasse': ['', [Validators.required, Validators.maxLength(100)]],
      'url': ['', [Validators.maxLength(2000)]]
    });

    this.name = this.schuleAendernForm.controls['name'];
    this.strasse = this.schuleAendernForm.controls['strasse'];
    this.url = this.schuleAendernForm.controls['url'];

    this.aendernFormVisible = false;
    this.formSubmitAttempt = false;
    this.cancelled = false;
  }

  ngOnInit() {

    const _color = this._calculateBackgroundColor();
    this._styles = {
      'background-color': _color
    };

    this._unsubscribe = this._store.subscribe(() => {
      const _state = this._store.getState();

      this._logger.debug('inside _unsubscribe');
      const _schulen = this._store.getState().schulen.schulen;
      const _gefiltert = _schulen.filter((o: Schule) => o.kuerzel === this.schule.kuerzel);
      if (_gefiltert.length === 1) {
        this.schule = _gefiltert[0];
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribe();
  }


  toggleSchuleAendern(): void {

    this._messageService.clearMessage();

    if (this.aendernFormVisible) {
      this._cancelSchuleAendern();
    } else {
      this._schuleAendern();
    }
  }

  private _schuleAendern(): void {
    this.aendernFormVisible = true;
    this.formSubmitAttempt = false;
    this.cancelled = false;
    this._initAendernForm();
  }


  private _initAendernForm(): void {
    const value = {
      'name': this.schule.name
      , 'strasse': this.schule.strasse
      , 'url': this.schule.url && this.schule.url || ''
    };

    this.schuleAendernForm.setValue(value);
  }

  private _cancelSchuleAendern(): void {
    this.aendernFormVisible = false;
    this.cancelled = true;
  }


  detailsAnzeigen(): void {
    this._logger.debug('Details kommen noch');
  }

  getStyles() {
    return this._styles;
  }


  onSubmit(value: any): void {
    const _name = value.name && value.name.trim() || null;
    const _strasse = value.strasse && value.strasse.trim() || null;
    const _url = value.url && value.url.trim() || null;

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.schuleAendernForm.valid) {
      this._schulenService.schuleAendern(this.schule.kuerzel, _name, _strasse, _url);
      this.cancelled = false;
    } else {
      validateAllFormFields(this.schuleAendernForm);
    }
  }


  private _calculateBackgroundColor(): string {
    if (this.schule.anzahlTeilnahmen > 0) {
      return AppConstants.backgroundColors.HELLGRUEN;
    }
    return AppConstants.backgroundColors.INPUT_NEUTRAL;
  }
}

