import { Injectable, Inject } from '@angular/core';
import { Store } from 'redux';

// own app
import { AppState } from '../../app.reducer';
import { AppStore } from '../../app.store';
import { ResponsePayload } from '../../shared/responsePayload.model';

// infrastructure
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

import { Ort } from '../orte/+state/orte.model';
import { updateOrt } from '../orte/+state/orte.actions';
import { Schule } from './+state/schulen.model';
import { loadSchulen, addSchule, updateSchule } from './+state/schulen.actions';
import { Message } from '../../messages/message.model';
import { MessageService } from '../../messages/message.service';

import { LogService } from 'hewi-ng-lib';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { catchError, retry } from 'rxjs/operators';

const BASE_URL = environment.apiUrl;

@Injectable()
export class SchulenService {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _http: HttpClient,
    private _messageService: MessageService,
    private _logger: LogService) { }


  findSchulen(name: string, ort: string): void {

    const _url = BASE_URL + '/schulen';

    let query = {};
    if (name && name.length > 0 && ort && ort.length > 0) {
      query = {
        'name': name,
        'ort': ort
      };
    } else {
      if (name && name.length > 0) {
        query = {
          'name': name
        };
      } else {
        query = {
          'ort': ort
        };
      }
    }

    const _params = new HttpParams({
      fromObject: query
    });

    const _future$ = this._http.get(_url, { params: _params });

    _future$.subscribe((response: ResponsePayload<Schule[]>) => {
      const _schulen = response.payload;
      this._store.dispatch(loadSchulen(_schulen));
    });
  }

  schuleAnlegen(ort: Ort, name: string, strasse: string, website: string): void {
    this._logger.debug('start login');
    const url = BASE_URL + '/schulen';

    const _payload = {
      'landkuerzel': ort.lage.landkuerzel.toUpperCase(),
      'ortkuerzel': ort.kuerzel.toUpperCase(),
      'name': name,
      'strasse': strasse,
      'url': website
    };

    this._http.post<ResponsePayload<Schule>>(url, _payload, {}).subscribe(
      (data) => {
        this._logger.debug(JSON.stringify(data));
        const _message: Message = data.apiMessage;
        this._messageService.setMessage(_message);

        const _schule = new Schule(data.payload);
        this._store.dispatch(addSchule(_schule));

        ort.anzahlSchulen++;
        this._store.dispatch(updateOrt(ort));
      });
  }

  schuleAendern(kuerzel: string, name: string, strasse: string, website: string): void {
    this._logger.debug('start login');
    const url = BASE_URL + '/schulen/' + kuerzel;

    const _payload = {
      'name': name,
      'strasse': strasse,
      'url': website
    };

    this._http.put<ResponsePayload<Schule>>(url, _payload, {}).subscribe(
      (data) => {
        this._logger.debug(JSON.stringify(data));
        const _message: Message = data.apiMessage;
        this._messageService.setMessage(_message);

        const _schule = new Schule(data.payload);
        this._logger.debug(JSON.stringify(_schule));
        this._store.dispatch(updateSchule(_schule));
      });
  }
}


