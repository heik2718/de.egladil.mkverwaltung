import {Schule} from './schulen.model';

export interface SchulenState {
  schulen: Schule[];
}

export const initialSchulenState: SchulenState = {schulen: []};

