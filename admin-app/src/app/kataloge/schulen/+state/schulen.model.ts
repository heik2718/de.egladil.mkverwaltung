import {HateoasPayload} from '../../../shared/responsePayload.model';


export class SchuleLage {
  landkuerzel: string;
  land: string;
  ortkuerzel: string;
  ort: string;

  constructor(data: any = {}) {
    this.landkuerzel = data && data.landkuerzel || null;
    this.land = data && data.land || null;
    this.ortkuerzel = data && data.ortkuerzel || null;
    this.ort = data && data.ort || null;
  }
}

export class VereinbarungAdv {
  schulkuerzel: string; // ist die ID
  schulname: string;
  strasse: string;
  hausnummer: string;
  ort: string;
  plz: string;
  laendercode: string;
  zugestimmtAm: string;
  zugestimmtDurch: string;
  versionsnummer: string; // ID zum Vereinbarungstext
}

export class Schule {
  kuerzel: string;
  name: string;
  strasse: string;
  url: string;
  schultyp: string;
  lage: SchuleLage;
  anzahlTeilnahmen: number;
  hateoasPayload: HateoasPayload;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.name = data && data.name || null;
    this.strasse = data && data.strasse || null;
    this.url = data && data.url || null;
    this.schultyp = data && data.schultyp || null;
    this.lage = data && data.lage || null;
    this.anzahlTeilnahmen = data && data.anzahlTeilnahmen || 0;
    this.hateoasPayload = data && data.hateoasPayload || null;
  }
}




