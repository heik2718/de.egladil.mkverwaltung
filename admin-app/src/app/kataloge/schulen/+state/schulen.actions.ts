import {Action, ActionCreator} from 'redux';
import {Schule} from './schulen.model';


export const SCHULEN_LOAD = '[schulen] load';
export const SCHULEN_CLEAR = '[schulen] clear';
export const SCHULE_ADD = '[schule] add';
export const SCHULE_UPDATE = '[schule] update';

export interface SchulenLoadAction extends Action {
  payload: Schule[];
}

export interface SchulenClearAction extends Action {
  payload: Schule[];
}

export interface SchuleAddAction extends Action {
  schule: Schule;
}

export interface SchuleUpdateAction extends Action {
  schule: Schule;
}


export const loadSchulen: ActionCreator<SchulenLoadAction> =
  (g) => ({
    type: SCHULEN_LOAD,
    payload: g
  });

export const clearSchulen: ActionCreator<SchulenClearAction> =
  (g) => ({
    type: SCHULEN_CLEAR,
    payload: []
  });


export const addSchule: ActionCreator<SchuleAddAction> =
  (g) => ({
    type: SCHULE_ADD,
    schule: g
  });

export const updateSchule: ActionCreator<SchuleUpdateAction> =
  (g) => ({
    type: SCHULE_UPDATE,
    schule: g
  });
