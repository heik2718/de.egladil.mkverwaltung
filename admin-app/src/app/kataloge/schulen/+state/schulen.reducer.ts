import {Action} from 'redux';

import {Schule} from './schulen.model';
import {SchulenState, initialSchulenState} from './schulen.state';
import * as SchulenAction from './schulen.actions';

export const SchulenReducer = function(state: SchulenState = initialSchulenState, action: Action): SchulenState {

  switch (action.type) {
    case (SchulenAction.SCHULEN_LOAD):
      const _schulen: Schule[] = (<SchulenAction.SchulenLoadAction>action).payload;
      return {
        schulen: _schulen
      };
   case (SchulenAction.SCHULE_ADD):
      const _schule: Schule = (<SchulenAction.SchuleAddAction>action).schule;
      console.log(JSON.stringify(_schule));
      return {
        schulen: [_schule, ...state.schulen]
      };
    case (SchulenAction.SCHULE_UPDATE):
      const _geaenderteSchule = (<SchulenAction.SchuleUpdateAction>action).schule;
      const _geaenderteSchulen: Schule[] = state.schulen.map(schule => {
        if (schule.kuerzel !== _geaenderteSchule.kuerzel) {
          return schule;
        }
        console.log('schule ' + schule.kuerzel + ' ausgetauscht');
        return _geaenderteSchule;
      });
      return {
        schulen: _geaenderteSchulen
      };
    case (SchulenAction.SCHULEN_CLEAR): return initialSchulenState;
    default: return state;
  }
};



