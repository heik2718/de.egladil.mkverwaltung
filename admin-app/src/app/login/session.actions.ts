import {Action, ActionCreator} from 'redux';
import {SessionToken} from './session.model';

/**
 * UserActions specifies action creators concerning Users
 */
export const SESSION_SET_CURRENT = '[Session] Set Current';
export const RESET_SESSION = '[Session] reset';

export interface SetCurrentSessionAction extends Action {
  sessionToken: SessionToken;
}

export const setCurrentSession: ActionCreator<SetCurrentSessionAction> =
  (user) => ({
    type: SESSION_SET_CURRENT,
    sessionToken: user
  });

export interface ResetSessionAction extends Action {
  payload: null;
}

export const resetCurrentSession: ActionCreator<ResetSessionAction> =
  (user) => ({
    type: RESET_SESSION,
    payload: user
  });


