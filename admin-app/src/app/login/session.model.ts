
export class SessionToken {
  accessToken: string;
  xsrfToken: string;

  constructor(obj: any = {}) {
    this.accessToken = obj && obj.accessToken || null;
    this.xsrfToken = obj && obj.xsrfToken || null;
  }

  isLoggedIn(): boolean {
    return this.accessToken !== null;
  }
}


export interface LoginCredentials {
  username: string;
  password: string;
  kleber?: string;
}


