import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from 'redux';

// own app
import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { Kontext } from '../kontext/kontext.model';
import { ResponsePayload } from '../shared/responsePayload.model';
import { setCurrentSession, resetCurrentSession } from '../login/session.actions';
import { setKontext } from '../kontext/kontext.actions';
import { SessionToken, LoginCredentials } from './session.model';


// infrastructure
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { LogService } from 'hewi-ng-lib';
import { Observable } from 'rxjs/Observable';

const BASE_URL = environment.apiUrl;

@Injectable()
export class SessionService {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _http: HttpClient,
    private _router: Router,
    private _logger: LogService) { }

  ping(): Observable<HttpResponse<ResponsePayload<Kontext>>> {

    return this._http
      .get<ResponsePayload<Kontext>>(BASE_URL + '/ping/start', { observe: 'response' });
  }

  login(credentials: LoginCredentials): void {
    this._logger.debug('start login');
    const url = BASE_URL + '/konten/admin';


    this._http.post<ResponsePayload<SessionToken>>(url, credentials, {}).subscribe(
      (data) => {
        this._logger.debug(JSON.stringify(data));
        const user = new SessionToken(data.payload);
        this._store.dispatch(resetCurrentSession(user));
        this._store.dispatch(setCurrentSession(user));
      });
  }

  logout(): void {

    this._logger.debug('start logout');
    const url = BASE_URL + '/konten/logout';

    this._http.post<ResponsePayload<Kontext>>(url, null, {}).subscribe(
      (data) => {
        this._logger.debug(JSON.stringify(data));
        const payload = data.payload;
        const user = new SessionToken({ 'xsrfToken': payload.xsrfToken });
        this._store.dispatch(resetCurrentSession(user));
        this._store.dispatch(setCurrentSession(user));
        this._store.dispatch(setKontext(payload));
        this._router.navigate(['/landing']);
      });
  }
}

export const sessionServiceInjectables: Array<any> = [
  SessionService
];
