import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';
import { Store, Unsubscribe } from 'redux';

import { LogService } from 'hewi-ng-lib';


// eigene Teile
import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { Kontext } from '../kontext/kontext.model';
import { MessageService } from '../messages/message.service';
import { LoginCredentials, SessionToken } from '../login/session.model';
import { SessionService } from '../login/session.service';
import { emailValidator, passwordValidator, validateAllFormFields } from '../shared/app.validators';



@Component({
  selector: 'admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {


  loginForm: FormGroup;
  username: AbstractControl;
  password: AbstractControl;
  kleber: AbstractControl;

  formSubmitAttempt: boolean;
  private _redirectUrl: string;

  private _loginCredentials: LoginCredentials;
  private _unsubscribe: Unsubscribe;



  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _messageService: MessageService
    , private _router: Router
    , private _route: ActivatedRoute
    , private _userService: SessionService
    , private _logger: LogService
  ) {

    this.loginForm = _fb.group({
      'username': ['', [Validators.required, Validators.maxLength(255)]],
      'password': ['', [Validators.required, passwordValidator]],
      'kleber': ['']
    });

    this.username = this.loginForm.controls['username'];
    this.password = this.loginForm.controls['password'];
    this.kleber = this.loginForm.controls['kleber'];
    this._redirectUrl = '';

  }

  ngOnInit() {

    this._logger.debug('mal anhalten zum gucken');
    this.formSubmitAttempt = false;

    this._route.queryParams
      .subscribe((key) => {
        // beabsichtigte route aus den queryParams merken
        this._redirectUrl = key['return'] || '/dashboard';
        this._logger.debug('redirect to ' + this._redirectUrl);
      });

    this._unsubscribe = this._store.subscribe(() => {
      if (this.formSubmitAttempt) {
        const state = this._store.getState();
        const user = state.session.sessionToken;
        if (user.isLoggedIn()) {
          // nach erfolgreichem Login dorthin navigieren. (klappt nur mit absoluten Urls)
          this._router.navigateByUrl(this._redirectUrl);
        }
      }
    });
  }

  ngOnDestroy() {
    this._messageService.clearMessage();
    this._unsubscribe();
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this._logger.debug('you submitted value:' + value);
    if (this.loginForm.valid) {
      this._loginCredentials = Object.assign({}, value) as LoginCredentials;
      this._logger.debug('loginCredentials: ' + JSON.stringify(this._loginCredentials));
      this._userService.login(this._loginCredentials);
    } else {
      validateAllFormFields(this.loginForm);
    }
  }

  cancel(): void {
    this._router.navigate(['/landing']);
  }
}


