import {Action} from 'redux';

import {SessionToken} from './session.model';
import * as SessionActions from './session.actions';

export interface SessionState {
  sessionToken: SessionToken;
}

const initialState: SessionState = {sessionToken: null};

export const SessionReducer =

  function(state: SessionState = initialState, action: Action): SessionState {
    switch (action.type) {
      case SessionActions.SESSION_SET_CURRENT:
        const _sessionToken: SessionToken = (<SessionActions.SetCurrentSessionAction>action).sessionToken;
        return {
          sessionToken: _sessionToken
        };
      default:
        return state;
    }
  };



