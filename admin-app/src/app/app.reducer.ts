/* tslint:disable:typedef */

import { Reducer, combineReducers, AnyAction } from 'redux';

import { AuswertungsgruppenState } from './auswertungsgruppen/+state/auswertungsgruppen.state';
import { AuswertungsgruppenReducer } from './auswertungsgruppen/+state/auswertungsgruppen.reducer';
import { AuswertungstabellenState } from './file/+state/auswertungstabellen.state';
import { AuswertungstabellenReducer } from './file/+state/auswertungstabellen.reducer';
import { BenutzerReducer } from './benutzer/+state/benutzer.reducer';
import { BenutzerState } from './benutzer/+state/benutzer.state';
import { KontextState, KontextReducer } from './kontext/kontext.reducer';
import { LaenderReducer } from './kataloge/laender/+state/laender.reducer';
import { LaenderState } from './kataloge/laender/+state/laender.state';
import { MessageState, MessagesReducer } from './messages/message.reducer';
import { OrteReducer } from './kataloge/orte/+state/orte.reducer';
import { OrteState } from './kataloge/orte/+state/orte.state';
import { RootgruppeReducer, RootgruppeState } from './rootgruppe-details/rootgruppe.state';
import { SessionState, SessionReducer } from './login/session.reducer';
import { RESET_SESSION } from './login/session.actions';
import { SchulenReducer } from './kataloge/schulen/+state/schulen.reducer';
import { SchulenState } from './kataloge/schulen/+state/schulen.state';
import { TeilnahmegruppenReducer, TeilnahmejahreReducer } from './statistik/+state/statistik.reducer';
import { GewaehltesWettbewerbsjahrReducer, GesamtpunktverteilungReducer } from './statistik/+state/statistik.reducer';
import { TeilnahmegruppenState, TeilnahmejahreState } from './statistik/+state/statistik.state';
import { GewaehltesWettbewerbsjahrState, GesamtpunktverteilungState } from './statistik/+state/statistik.state';

export * from './login/session.reducer';

export interface AppState {
  auswertungsgruppen: AuswertungsgruppenState;
  auswertungstabellen: AuswertungstabellenState;
  benutzer: BenutzerState;
  gewaehlteRootgruppe: RootgruppeState;
  kontext: KontextState;
  laender: LaenderState;
  message: MessageState;
  orte: OrteState;
  session: SessionState;
  schulen: SchulenState;
  teilnahmegruppen: TeilnahmegruppenState;
  teilnahmejahre: TeilnahmejahreState;
  gewaehltesWettbewerbsjahr: GewaehltesWettbewerbsjahrState;
  gesamtpunktverteilung: GesamtpunktverteilungState;
}


const appReducer: Reducer<AppState> = combineReducers<AppState>({
  auswertungsgruppen: AuswertungsgruppenReducer,
  auswertungstabellen: AuswertungstabellenReducer,
  benutzer: BenutzerReducer,
  gewaehlteRootgruppe: RootgruppeReducer,
  kontext: KontextReducer,
  laender: LaenderReducer,
  orte: OrteReducer,
  session: SessionReducer,
  message: MessagesReducer,
  schulen: SchulenReducer,
  teilnahmegruppen: TeilnahmegruppenReducer,
  teilnahmejahre: TeilnahmejahreReducer,
  gewaehltesWettbewerbsjahr: GewaehltesWettbewerbsjahrReducer,
  gesamtpunktverteilung: GesamtpunktverteilungReducer,
});

const initialState = appReducer({} as AppState, {} as AnyAction);

const rootReducer = (state, action) => {
  if (action.type === RESET_SESSION) {
    return initialState;
  }
  return appReducer(state, action);
};

export default appReducer;


