import { Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Store, Unsubscribe } from 'redux';
import { Subject } from 'rxjs/Subject';
import { LogService } from 'hewi-ng-lib';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { Kontext } from '../kontext/kontext.model';
import { Rootgruppe, Suchmodus, SUCHMODUS_LAND, SUCHMODUS_ORT, SUCHMODUS_SCHULE } from './+state/auswertungsgruppen.model';
import { loadAuswertungsgruppen, filterAuswertungsgruppen } from './+state/auswertungsgruppen.actions';
import { AuswertungsgruppenService } from './auswertungsgruppen.service';

@Component({
  selector: 'admin-auswertungsgruppen',
  templateUrl: './auswertungsgruppenList.component.html',
  styleUrls: ['./auswertungsgruppenList.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class AuswertungsgruppenListComponent implements OnInit, OnDestroy {

  suchmodusForm: FormGroup;
  suchmodusControl: AbstractControl;


  schulen: Rootgruppe[];
  gefilterteSchulen: Rootgruppe[];

  gefiltert: boolean;
  suchstring: string;

  selectedSuchmodusName: string;
  selectedSuchmodus: Suchmodus;
  providedSuchmodus: Suchmodus[];

  private _suchstringChanged$: Subject<string> = new Subject<string>();

  private _kontext: Kontext;
  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _schulenService: AuswertungsgruppenService
    , private _logger: LogService) {
    this._logger.debug('AuswertungsgruppenListComponent created');
    this.schulen = [];
    this.gefilterteSchulen = [];

    this.providedSuchmodus = [];
    this.providedSuchmodus.push(SUCHMODUS_SCHULE);
    this.providedSuchmodus.push(SUCHMODUS_ORT);
    this.providedSuchmodus.push(SUCHMODUS_LAND);

    this.suchmodusForm = this._fb.group({
      'suchmodus': ['']
    });

    this.suchmodusControl = this.suchmodusForm.controls['suchmodus'];

    this._suchstringChanged$
      .debounceTime(500) // wait 500ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe(model => {
        this.suchstring = model;
        if (!this.selectedSuchmodus) {
          this.selectedSuchmodus = SUCHMODUS_SCHULE;
          this.selectedSuchmodusName = SUCHMODUS_SCHULE.name;
        }
        this._filternMitSuchstring();
      });
  }

  ngOnInit() {

    this.suchstring = '';
    this.selectedSuchmodusName = '';
    this._readState();


    this._unsubscribe = this._store.subscribe(() => {
      this._readState();
    });

    if (this.schulen.length === 0 && this._kontext.neuanmeldungFreigegeben) {
      this._loadSchulen();
    }

    this.gefiltert = false;
  }

  ngOnDestroy() {
    this._unsubscribe();
  }

  onSuchmodusSelected() {
    const value = this.providedSuchmodus.find(ks => ks.name === this.selectedSuchmodusName);
    if (value) {
      this.selectedSuchmodus = value;
    } else {
      this.selectedSuchmodus = null;
    }
  }

  suchstringChanged(text: string) {
    this._suchstringChanged$.next(text);
  }


  private _filternMitSuchstring(): void {
    let _gefiltert = [];
    let _filtern = false;

    if (this._isStringNotBlank(this.suchstring)) {
      _filtern = true;
      const _vergl = this._getVergleichsstring(this.suchstring);
      this.gefiltert = true;

      switch (this.selectedSuchmodus) {
        case (SUCHMODUS_SCHULE): _gefiltert = this._getGefiltertSchule(_vergl); break;
        case (SUCHMODUS_ORT): _gefiltert = this._getGefiltertOrt(_vergl); break;
        case (SUCHMODUS_LAND): _gefiltert = this._getGefiltertLand(_vergl); break;
        default: _filtern = false; this.gefiltert = false; break;
      }
    }
    if (_filtern) {
      this._store.dispatch(filterAuswertungsgruppen({ schulen: this.schulen, gefiltert: _gefiltert }));
    } else {
      this._clearFilterOhneSuchmodus();
    }
  }

  private _isStringNotBlank(str: string): boolean {
    if (!str) {
      return false;
    }
    return str.trim().length > 0;
  }

  private _getVergleichsstring(str: string): string {
    return str.trim().toLocaleLowerCase();
  }

  private _getGefiltertSchule(suchstring: string): Rootgruppe[] {
    const _result = this.schulen.filter((g: Rootgruppe) => g.schulteilnahme.schule.toLowerCase().includes(suchstring));
    this._logger.debug('Anzahl: ' + _result.length);
    return _result;
  }

  private _getGefiltertOrt(suchstring: string): Rootgruppe[] {
    return this.schulen.filter((g: Rootgruppe) => g.schulteilnahme.ort.toLowerCase().startsWith(suchstring));
  }

  private _getGefiltertLand(suchstring: string): Rootgruppe[] {
    return this.schulen.filter((g: Rootgruppe) => g.schulteilnahme.land.toLowerCase().startsWith(suchstring));
  }


  clearFilter(): void {
    this.selectedSuchmodus = null;
    this.selectedSuchmodusName = null;
    this._clearFilterOhneSuchmodus();
  }

  private _clearFilterOhneSuchmodus(): void {
    this.gefiltert = false;
    this.suchstring = '';
    this._store.dispatch(filterAuswertungsgruppen({ schulen: this.schulen, gefiltert: this.schulen }));
  }

  showGeaenderte(): void {
    this.gefiltert = true;
    const _filtered = this.schulen
      .filter((g: Rootgruppe) => g.nameDiffersFromKatalog);
    this._store.dispatch(filterAuswertungsgruppen({ schulen: this.schulen, gefiltert: _filtered }));
  }

  showAuswertbare(): void {
    this.gefiltert = true;
    const _filtered = this.schulen
      .filter((g: Rootgruppe) => g.anzahlLoesungszettel > 0);
    this._store.dispatch(filterAuswertungsgruppen({ schulen: this.schulen, gefiltert: _filtered }));
  }

  showClearButton(): boolean {
    if (this.selectedSuchmodus || this.selectedSuchmodusName && this.selectedSuchmodusName.trim().length > 0) {
      return true;
    }
    return false;
  }

  refresh(): void {
    this.gefiltert = false;
    this._loadSchulen();
  }


  private _loadSchulen(): void {
    this._schulenService.load(this._kontext.wettbewerbsjahr)
      .subscribe(resp => {
        const body = resp.body;
        if (body.payload) {
          this.schulen = body.payload;
          this.gefilterteSchulen = body.payload;
          // initialize to page 1
          this._store.dispatch(loadAuswertungsgruppen(this.schulen));
        }
      });
  }

  private _readState(): void {
    this._logger.debug('SchulenListComponent: state changed');
    const _state = this._store.getState();
    this._kontext = _state.kontext.kontext;
    this.schulen = _state.auswertungsgruppen.rootgruppen;
    this.gefilterteSchulen = _state.auswertungsgruppen.gefiltert;
  }
}

