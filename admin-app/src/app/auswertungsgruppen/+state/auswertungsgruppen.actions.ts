import {Action, ActionCreator} from 'redux';
import {Rootgruppe} from './auswertungsgruppen.model';

export const AUSWERTUNGSGRUPPEN_LOAD = '[auswertungsgruppen] load';
export const AUSWERTUNGSGRUPPEN_FILTER = '[auswertungsgruppen] filter';
export const AUSWERTUNGSGRUPPEN_CLEAR = '[auswertungsgruppen] clear';
export const AUSWERTUNGSGRUPPEN_UPDATE = '[auswertungsgruppen] update';

export interface AuswertungsgruppenLoadAction extends Action {
  payload: Rootgruppe[];
}

export interface AuswertungsgruppenFilterAction extends Action {
  schulen: Rootgruppe[];
  gefiltert: Rootgruppe[];
}

export interface AuswertungsgruppenClearAction extends Action {
  payload: Rootgruppe[];
}

export interface AuswertungsgruppenUpdateAction extends Action {
  payload: Rootgruppe;
}

export const loadAuswertungsgruppen: ActionCreator<AuswertungsgruppenLoadAction> =
  (g) => ({
    type: AUSWERTUNGSGRUPPEN_LOAD,
    payload: g
  });

export const clearAuswertungsgruppen: ActionCreator<AuswertungsgruppenClearAction> =
  (g) => ({
    type: AUSWERTUNGSGRUPPEN_LOAD,
    payload: []
  });

export const filterAuswertungsgruppen: ActionCreator<AuswertungsgruppenFilterAction> =
  (g: any = {}) => ({
    type: AUSWERTUNGSGRUPPEN_FILTER,
    schulen: g.schulen,
    gefiltert: g.gefiltert
  });


