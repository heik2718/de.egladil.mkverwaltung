import {HateoasPayload} from '../../shared/responsePayload.model';

export const STATISTIK_LOESUNGSZETTEL = 'STATISTIK_LOESUNGSZETTEL';
export const STATISTIK_SCHULE = 'STATISTIK_SCHULE';
export const DOWNLOADART_URKUNDE = 'URKUNDE';

export class Auswertungsgruppe {

  kuerzel: string;
  name: string;
  geaendertDurch?: string;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.name = data && data.name || null;
    this.geaendertDurch = data && data.geaendertDurch || null;
  }
}

export class DownloadCode {
  downloadCode: string;
  teilnahmeart: string;
  jahr: string;
  kuerzel; string;
  downloadArt: string;

  constructor(data: any = {}) {
    this.downloadCode = data && data.downloadCode || null;
    this.teilnahmeart = data && data.teilnahmeart || null;
    this.jahr = data && data.jahr || null;
    this.kuerzel = data && data.kuerzel || null;
    this.downloadArt = data && data.downloadArt || null;
  }
}

export class Schulteilnahme {
  kuerzel: string;
  jahr: string;
  landkuerzel: string;
  land: string;
  ortkuerzel: string;
  ort: string;
  schulkuerzel: string;
  schule: string;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.jahr = data && data.jahr || null;
    this.landkuerzel = data && data.landkuerzel || null;
    this.land = data && data.land || null;
    this.ortkuerzel = data && data.ortkuerzel || null;
    this.ort = data && data.ort || null;
    this.schulkuerzel = data && data.schulkuerzel || null;
    this.schule = data && data.schule || null;
  }
}

export class Rootgruppe {
  rootgruppe: Auswertungsgruppe;
  schulteilnahme: Schulteilnahme;
  nameDiffersFromKatalog: boolean;
  anzahlLoesungszettel: number;
  auswertungsupload: boolean;
  teilnahmejahre: string[];

  constructor(data: any = {}) {
    this.rootgruppe = data && data.rootgruppe || null;
    this.schulteilnahme = data && data.schulteilnahme || null;
    this.nameDiffersFromKatalog = data && data.nameDiffersFromKatalog || false;
    this.anzahlLoesungszettel = data && data.anzahlLoesungszettel || 0;
    this.auswertungsupload = data && data.auswertungsupload || true;
    this.teilnahmejahre = data && data.teilnahmejahre || [];
  }
}

export interface Suchmodus {
  name: string;
  label: string;
}

export const SUCHMODUS_SCHULE = {name: 'SCHULE', label: 'Schulen'} as Suchmodus;
export const SUCHMODUS_ORT = {name: 'ORT', label: 'Orte'} as Suchmodus;
export const SUCHMODUS_LAND = {name: 'LAND', label: 'Länder'} as Suchmodus;



