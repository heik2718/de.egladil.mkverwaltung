import {Rootgruppe} from './auswertungsgruppen.model';

export interface AuswertungsgruppenState {
  rootgruppen: Rootgruppe[];
  gefiltert: Rootgruppe[];
}

export const initialAuswertungsgruppenState: AuswertungsgruppenState = {rootgruppen: [], gefiltert: []};

