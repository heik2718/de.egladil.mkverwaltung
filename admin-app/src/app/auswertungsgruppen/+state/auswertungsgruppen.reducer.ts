import {Action} from 'redux';

import {Rootgruppe} from './auswertungsgruppen.model';
import {AuswertungsgruppenState, initialAuswertungsgruppenState} from './auswertungsgruppen.state';
import * as AuswertungsgruppenAction from './auswertungsgruppen.actions';

export const AuswertungsgruppenReducer =

  function(state: AuswertungsgruppenState = initialAuswertungsgruppenState, action: Action): AuswertungsgruppenState {
    switch (action.type) {
      case AuswertungsgruppenAction.AUSWERTUNGSGRUPPEN_LOAD:
        const _rootgruppen: Rootgruppe[] = (<AuswertungsgruppenAction.AuswertungsgruppenLoadAction>action).payload;
        return {
          rootgruppen: _rootgruppen,
          gefiltert: _rootgruppen
        };
      case AuswertungsgruppenAction.AUSWERTUNGSGRUPPEN_FILTER:
//        console.log('FILTER_SCHULEN');
        const _alle: Rootgruppe[] = (<AuswertungsgruppenAction.AuswertungsgruppenFilterAction>action).schulen;
        const _gefiltert: Rootgruppe[] = (<AuswertungsgruppenAction.AuswertungsgruppenFilterAction>action).gefiltert;
        return {
          rootgruppen: _alle,
          gefiltert: _gefiltert
        };
      case AuswertungsgruppenAction.AUSWERTUNGSGRUPPEN_CLEAR:
        return initialAuswertungsgruppenState;
      default:
        return state;
    }
  };

