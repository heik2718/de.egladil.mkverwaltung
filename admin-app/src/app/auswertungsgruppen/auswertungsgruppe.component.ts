import { Component, OnInit, Inject, Input } from '@angular/core';
import { Rootgruppe } from './+state/auswertungsgruppen.model';
import { AppConstants } from '../shared/app.constants';

@Component({
  selector: 'admin-auswertungsgruppe',
  templateUrl: './auswertungsgruppe.component.html',
  styleUrls: ['./auswertungsgruppe.component.css']
})
export class AuswertungsgruppeComponent implements OnInit {

  @Input() rootgruppe: Rootgruppe;
  private _styles: {};



  constructor() { }

  ngOnInit() {
    const _color = this._calculateBackgroundColor();
    this._styles = {
      'background-color': _color
    };
  }

  private _calculateBackgroundColor(): string {
    if (this.rootgruppe.schulteilnahme === null) {
      return AppConstants.backgroundColors.INPUT_MISSING;
    }
    if (this.rootgruppe.auswertungsupload) {
      return AppConstants.backgroundColors.HELLORANGE;
    }
    if (this.rootgruppe.anzahlLoesungszettel > 0) {
      return AppConstants.backgroundColors.INPUT_VALID;
    }
    return AppConstants.backgroundColors.INPUT_NEUTRAL;
  }

  getStyles() {
    return this._styles;
  }

  toggleTeilnahme(jahr: number) {

  }
}


