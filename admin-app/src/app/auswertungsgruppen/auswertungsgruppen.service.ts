import { Injectable, Inject } from '@angular/core';
import { Store } from 'redux';

// own app
import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { DownloadDelegate } from '../shared/download.delegate';
import { ResponsePayload } from '../shared/responsePayload.model';
import { Rootgruppe, DownloadCode } from './+state/auswertungsgruppen.model';

// infrastructure
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { LogService } from 'hewi-ng-lib';
import { Observable } from 'rxjs/Observable';

const BASE_URL = environment.apiUrl;

@Injectable()
export class AuswertungsgruppenService {

  constructor (@Inject(AppStore) private _store: Store<AppState>,
    private _downloadDelegate: DownloadDelegate,
    private _http: HttpClient,
    private _logger: LogService) { }

  load(jahr: string): Observable<HttpResponse<ResponsePayload<Rootgruppe[]>>> {

    const url = BASE_URL + '/auswertungsgruppen/rootgruppen/' + jahr;

    return this._http
      .get<ResponsePayload<Rootgruppe[]>>(url, { observe: 'response' });
  }

  erzeugeAuswertung(kuerzel: string): Observable<HttpResponse<ResponsePayload<DownloadCode>>> {

    const url = BASE_URL + '/auswertungsgruppen/rootgruppen/' + kuerzel + '/schulstatistik';
    this._logger.debug('call ' + url);

    return this._http
      .get<ResponsePayload<DownloadCode>>(url, { observe: 'response' });
  }

  saveFile(downloadcode: string) {
    this._logger.debug('saveFile ' + downloadcode);
    const url = BASE_URL + '/auswertungsgruppen/rootgruppen/schuluebersichten/' + downloadcode;
    const $blob = this._http.get(url, { responseType: 'blob' });
    const filename = 'auswertung-minikaenguru-' + downloadcode + '.pdf';

    $blob.subscribe((res: Blob) => {
      const headerKeys = res.size;
      this._logger.debug(JSON.stringify(headerKeys));
      this._downloadDelegate.saveToFileSystem(res, filename);
    }, (error) => {
      this._logger.debug('error beim download der Auswertung mit downloadcode ' + downloadcode);
    });
  }
}

export const auswertungsgruppenServiceInjectables: Array<any> = [
  AuswertungsgruppenService
];

