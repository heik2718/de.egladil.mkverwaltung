import {Action, ActionCreator} from 'redux';
import {Rootgruppe} from '../auswertungsgruppen/+state/auswertungsgruppen.model';

export const SET_ROOTGRUPPE = '[Rootgruppe] Set';
export const CLEAR_ROOTGRUPPE = '[Rootgruppe] Clear';

export interface RootgruppeAction extends Action {
  rootgruppe: Rootgruppe;
}

export const setRootgruppe: ActionCreator<RootgruppeAction> =
  (theRootgruppe) => ({
    type: SET_ROOTGRUPPE,
    rootgruppe: theRootgruppe
  });

export const clearRootgruppe: ActionCreator<RootgruppeAction> =
  (theRootgruppe) => ({
    type: CLEAR_ROOTGRUPPE,
    rootgruppe: theRootgruppe
  });



