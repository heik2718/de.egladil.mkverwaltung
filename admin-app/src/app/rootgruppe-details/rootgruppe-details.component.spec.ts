import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootgruppeDetailsComponent } from './rootgruppe-details.component';

describe('RootgruppeDetailsComponent', () => {
  let component: RootgruppeDetailsComponent;
  let fixture: ComponentFixture<RootgruppeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootgruppeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootgruppeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
