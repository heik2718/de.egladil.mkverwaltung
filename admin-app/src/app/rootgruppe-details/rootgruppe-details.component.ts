import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Store, Unsubscribe} from 'redux';
import { LogService } from 'hewi-ng-lib';

import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';
import {Rootgruppe, DownloadCode} from '../auswertungsgruppen/+state/auswertungsgruppen.model';
import * as RootgruppeAction from '../rootgruppe-details/rootgruppe.actions';
import {AuswertungsgruppenService} from '../auswertungsgruppen/auswertungsgruppen.service';

@Component({
  selector: 'admin-rootgruppe-details',
  templateUrl: './rootgruppe-details.component.html',
  styleUrls: ['./rootgruppe-details.component.css']
})
export class RootgruppeDetailsComponent implements OnInit, OnDestroy {

  auswertungsgruppe: Rootgruppe;
  downloadCode: string;
  auswertenDisabled: boolean;
  showDownloadLink: boolean;

  private _unsubscribeStore: Unsubscribe;

  constructor( @Inject(AppStore) private _store: Store<AppState>
    , private router: Router
    , private route: ActivatedRoute
    , private _schulenService: AuswertungsgruppenService
    , private _logger: LogService
  ) {

  }

  ngOnInit() {

    this.showDownloadLink = false;
    this.auswertenDisabled = true;

    const routeSubscription = this.route.params
      .subscribe(params => {
        this._logger.debug('route.params=' + JSON.stringify(params));
        const id = (params['kuerzel'] || '');
        if (id !== '' && id !== 'new') {
          const _stored = this._store.getState().gewaehlteRootgruppe.rootgruppe as Rootgruppe;
          if (_stored === null || _stored.rootgruppe.kuerzel !== id) {
            this.router.navigate(['/schulen']);
          } else {
            this.auswertungsgruppe = _stored;
            this.auswertenDisabled = this.auswertungsgruppe.anzahlLoesungszettel === 0;
          }
        }
      });

    this._unsubscribeStore = this._store.subscribe(() => {
      this._readState();
    });
  }

  ngOnDestroy() {
    if (this._unsubscribeStore) {
      this._unsubscribeStore();
    }
  }

  erstelleAuswertung(): void {
    this._schulenService.erzeugeAuswertung(this.auswertungsgruppe.rootgruppe.kuerzel)
      .subscribe(resp => {
        const body = resp.body;
        if (body.payload) {
          this._logger.debug('payload=' + body.payload);
          const _payload = body.payload as DownloadCode;
          if (_payload) {
            this.downloadCode = _payload.downloadCode;
            if (this.downloadCode !== null && this.downloadCode !== undefined && this.downloadCode.length > 0) {
              this.showDownloadLink = true;
            } else {
              this.showDownloadLink = false;
            }
          } else {
            this.downloadCode = '';
            this.showDownloadLink = false;
          }
        }
      });
  }

  downloadResult(): void {
    if (this.showDownloadLink) {
      this._schulenService.saveFile(this.downloadCode);
      this.showDownloadLink = false;
    }
  }

  cancel(): void {
    this._store.dispatch(RootgruppeAction.clearRootgruppe());
    this.router.navigate(['/schulen']);
  }

  private _readState(): void {
    this.auswertungsgruppe = this._store.getState().gewaehlteRootgruppe.rootgruppe;
  }
}
