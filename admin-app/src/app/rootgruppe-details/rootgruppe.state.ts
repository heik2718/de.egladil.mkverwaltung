import {Action} from 'redux';

import {Rootgruppe} from '../auswertungsgruppen/+state/auswertungsgruppen.model';
import * as RootgruppeAction from './rootgruppe.actions';



export interface RootgruppeState {
  rootgruppe: Rootgruppe;
}

export const initialRootgruppeState: RootgruppeState = {rootgruppe: null};

export const RootgruppeReducer =

  function(state: RootgruppeState = initialRootgruppeState, action: Action): RootgruppeState {

    switch (action.type) {
      case RootgruppeAction.SET_ROOTGRUPPE:
        const theRootgruppe: Rootgruppe = (<RootgruppeAction.RootgruppeAction>action).rootgruppe;
        return {
          rootgruppe: theRootgruppe
        };
      case RootgruppeAction.CLEAR_ROOTGRUPPE:
        return initialRootgruppeState;
      default: return state;
    }
  };


