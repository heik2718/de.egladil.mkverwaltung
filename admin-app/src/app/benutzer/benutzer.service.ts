import {Injectable, Inject} from '@angular/core';
import {Store} from 'redux';

// own app
import {AppState} from '../app.reducer';
import {AppStore} from '../app.store';
import {ResponsePayload} from '../shared/responsePayload.model';

// infrastructure
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Benutzer} from './+state/benutzer.model';
import {loadBenutzer} from './+state/benutzer.actions';

import { LogService } from 'hewi-ng-lib';

const BASE_URL = environment.apiUrl;

@Injectable()
export class BenutzerService {

  constructor( @Inject(AppStore) private _store: Store<AppState>,
    private _http: HttpClient,
    private _logger: LogService) {}


  findBenutzer(modus: string, suchstring: string): void {

    const _url = BASE_URL + '/benutzer';

    const _query = {
      'modus': modus,
      'param': suchstring
    };

    const _params = new HttpParams({
      fromObject: _query
    });

    const _future$ = this._http.get(_url, {params: _params});

    _future$.subscribe((response: ResponsePayload<Benutzer[]>) => {
      this._logger.debug('response ist da');
      const _benutzer = response.payload;
      this._store.dispatch(loadBenutzer(_benutzer));
    });
  }
}


