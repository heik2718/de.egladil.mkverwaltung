import { Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { Store, Unsubscribe } from 'redux';
import { LogService } from 'hewi-ng-lib';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { AppState } from '../app.reducer';
import { AppStore } from '../app.store';
import { ERROR } from '../messages/message.model';
import { MessageService } from '../messages/message.service';
import { Benutzer, BENUTZERSUCHMODUS_EMAIL, BENUTZERSUCHMODUS_NAME, BENUTZERSUCHMODUS_ID } from './+state/benutzer.model';
import { clearBenutzer } from './+state/benutzer.actions';
import { BenutzerService } from './benutzer.service';
import { validateAllFormFields, resetForm, allValuesNoneBlankValidator } from '../shared/app.validators';


@Component({
  selector: 'admin-benutzer',
  templateUrl: './benutzerList.component.html',
  styleUrls: ['./benutzerList.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class BenutzerListComponent implements OnInit, OnDestroy {


  benutzersucheForm: FormGroup;
  email: AbstractControl;
  name: AbstractControl;

  uuid: AbstractControl;

  alleBenutzer: Benutzer[];

  formSubmitAttempt: boolean;
  btnSubmitDisabled: boolean;
  cancelled: boolean;

  private _unsubscribe: Unsubscribe;

  constructor (@Inject(AppStore) private _store: Store<AppState>
    , private _fb: FormBuilder
    , private _messageService: MessageService
    , private _benutzerService: BenutzerService
    , private _logger: LogService) {

    this.benutzersucheForm = this._fb.group({
      'uuid': ['', [Validators.maxLength(36)]],
      'email': ['', [Validators.maxLength(100)]],
      'name': ['', [Validators.maxLength(100)]]
    }, { validator: allValuesNoneBlankValidator });

    this.uuid = this.benutzersucheForm.controls['uuid'];
    this.email = this.benutzersucheForm.controls['email'];
    this.name = this.benutzersucheForm.controls['name'];

  }


  ngOnInit() {
    this.formSubmitAttempt = false;
    this.cancelled = true;
    this.btnSubmitDisabled = false;

    this._unsubscribe = this._store.subscribe(() => {
      const state = this._store.getState();
      this.alleBenutzer = state.benutzer.benutzer;
    });
  }

  ngOnDestroy() {
    this._messageService.clearMessage();
    this._unsubscribe();
  }

  onSubmit(value: any): void {
    const _email = value.email && value.email.trim() || null;
    const _name = value.name && value.name.trim() || null;
    const _uuid = value.uuid && value.uuid.trim() || null;

    if (_email === null && _name === null && _uuid === null) {
      if (!this.cancelled) {
        this._messageService.setMessage({ level: ERROR, message: 'mindestens ein Suchparameter erforderlich.' });
      }
      return;
    }

    this.formSubmitAttempt = true;
    this._messageService.clearMessage();
    this._logger.debug('you submitted value:' + value);
    if (this.benutzersucheForm.valid) {
      if (_email !== null && _email.length > 0) {
        this._benutzerService.findBenutzer(BENUTZERSUCHMODUS_EMAIL, _email);
      } else {
        if (_name != null && _name.length > 0) {
          this._benutzerService.findBenutzer(BENUTZERSUCHMODUS_NAME, _name);
        } else {
          if (_uuid != null && _uuid.length > 0) {
            this._benutzerService.findBenutzer(BENUTZERSUCHMODUS_ID, _uuid);
          }
        }
      }

      this.cancelled = false;
    } else {
      validateAllFormFields(this.benutzersucheForm);
    }
  }

  checkSubmitDisabled(): boolean {
    if (!this.formSubmitAttempt && !this.benutzersucheForm.valid) {
      return true;
    }
    //    const _email = this.benutzersucheForm.value.email && this.benutzersucheForm.value.email.trim() || null;
    //    const _name = this.benutzersucheForm.value.name && this.benutzersucheForm.value.name.trim() || null;
    //    if (_email === null && _name === null || _email.length > 0 && _name.length > 0) {
    //      return true;
    //    }
    return false;
  }

  cancel(): void {
    this.cancelled = true;
    this.formSubmitAttempt = false;
    resetForm(this.benutzersucheForm);
    this._store.dispatch(clearBenutzer());
    this._messageService.clearMessage();
  }
}



