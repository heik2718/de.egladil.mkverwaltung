import { Component, OnInit, Input } from '@angular/core';
import { AppConstants } from '../shared/app.constants';
import { Benutzer } from './+state/benutzer.model';
import { AuswertungstabellenService } from '../file/auswertungstabellen.service';
import { MessageService } from '../messages/message.service';
import { UploadFilesStatus, FilesSelected } from '../file-upload/file-upload.model';
import { FileUploadConfig } from '../file-upload/file-upload-config';



@Component({
  selector: 'admin-benutzer-overview',
  templateUrl: './benutzerOverview.component.html'
})
export class BenutzerOverviewComponent implements OnInit {

  @Input() benutzer: Benutzer;
  private _styles: {};

  schuleVisible: boolean;

  uploading: boolean;

  btnUploadVisible: boolean;

  selectedFilesForUpload: any;

  private hasError: boolean;

  private errorMsg: string;

  fileUplodConfig: FileUploadConfig;

  constructor(private service: AuswertungstabellenService,
    private messageService: MessageService) { }

  ngOnInit() {
    const _color = this._calculateBackgroundColor();
    this._styles = {
      'background-color': _color
    };
    this.fileUplodConfig = {
      acceptExtensions: 'ods,xls,xlsx',
      maxFilesCount: 1,
      maxFileSize: 5120000,
      totalFilesSize: 10120000
    };
    this.schuleVisible = false;
    this.uploading = false;
    this.hasError = false;
    this.errorMsg = '';

    this.btnUploadVisible = this.benutzer.erweiterteKontodaten.aktuelleTeilnahme &&
      this.benutzer.erweiterteKontodaten.aktuelleTeilnahme.anzahlTeilnehmer === 0;
  }

  private _calculateBackgroundColor(): string {
    if (this.benutzer.basisdaten.rolle === 'MKV_LEHRER') {
      return AppConstants.backgroundColors.INPUT_VALID;
    } else {
      return AppConstants.backgroundColors.ZU_LEICHT;
    }
  }

  getStyles() {
    return this._styles;
  }

  toggleSchule(): void {
    this.schuleVisible = !this.schuleVisible;
  }

  public filesSelect(selectedFiles: FilesSelected): void {

    this.messageService.clearMessage();

    if (selectedFiles.status !== UploadFilesStatus.STATUS_SUCCESS) {
      this.hasError = true;
      switch (selectedFiles.status) {
        case UploadFilesStatus.STATUS_MAX_FILES_COUNT_EXCEED: this.errorMsg = 'zu viele'; break;
        case UploadFilesStatus.STATUS_MAX_FILE_SIZE_EXCEED: this.errorMsg = 'zu zu groß'; break;
        case UploadFilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED: this.errorMsg = 'zusammen zu groß'; break;
        case UploadFilesStatus.STATUS_NOT_MATCH_EXTENSIONS: this.errorMsg = 'nur xls, xlsx oder ods'; break;
      }

      this.selectedFilesForUpload = selectedFiles.status;
      return;
    }
    //    this.selectedFiles = Array.from(selectedFiles.files).map(file => file.name);
    this.selectedFilesForUpload = selectedFiles;
    this.uploading = true;
    this.service.uploadForUser(this.selectedFilesForUpload, this.benutzer.geschuetzteKontodaten.uuid);
    this.uploading = false;
  }

}

