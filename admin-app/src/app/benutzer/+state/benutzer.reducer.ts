import {Action} from 'redux';

import {Benutzer} from './benutzer.model';
import {BenutzerState, initialBenutzerState} from './benutzer.state';

import * as BenutzerAction from './benutzer.actions';

export const BenutzerReducer = function(state: BenutzerState = initialBenutzerState, action: Action): BenutzerState {

  switch (action.type) {
    case (BenutzerAction.BENUTZER_LOAD):
      const _benutzer: Benutzer[] = (<BenutzerAction.BenutzerLoadAction>action).payload;
      return {
        benutzer: _benutzer
      };
    case (BenutzerAction.BENUTZER_CLEAR): return initialBenutzerState;
    default: return state;
  }
};


