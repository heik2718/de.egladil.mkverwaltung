import { HateoasPayload } from '../../shared/responsePayload.model';
import { Schule } from '../../kataloge/schulen/+state/schulen.model';

export const BENUTZERSUCHMODUS_EMAIL = 'EMAIL';
export const BENUTZERSUCHMODUS_NAME = 'NAME';
export const BENUTZERSUCHMODUS_ID = 'ID';

export class Teilnahme {

  aktuelle: boolean;
  anzahlLoesungszettel: number;
  anzahlTeilnehmer: number;
  anzahlUploads: number;

    constructor(data: any = {}) {
    this.aktuelle = data && data.aktuelle || false;
    this.anzahlLoesungszettel = data && data.anzahlLoesungszettel || 0;
    this.anzahlTeilnehmer = data && data.anzahlTeilnehmer || 0;
    this.anzahlUploads = data && data.anzahlUploads || 0;
  }

}

export class PersonBasisdaten {
  vorname?: string;
  nachname?: string;
  email?: string;
  rolle?: string;

  constructor(data: any = {}) {
    this.vorname = data && data.vorname || null;
    this.nachname = data && data.nachname || null;
    this.email = data && data.email || null;
    this.rolle = data && data.rolle || null;
  }

}

export class ErweiterteKontodaten {
  mailbenachrichtigung?: boolean;
  schule?: Schule;
  anzahlTeilnahmen: number;
  aktuelleTeilnahme?: Teilnahme;
  advVereinbarungVorhanden: boolean;

  constructor(data: any = {}) {
    this.mailbenachrichtigung = data && data.mailbenachrichtigung || false;
    this.schule = data && data.schule || null;
    this.anzahlTeilnahmen = data && data.anzahlTeilnahmen || 0;
    this.aktuelleTeilnahme = data && data.aktuelleTeilnahme || null;
    this.advVereinbarungVorhanden = data && data.advVereinbarungVorhanden || false;
  }
}

export class GeschuetzteKontodaten {
  uuid: string;
  lastAccess: string;
  datumGeaendert: string;
  aktiviert: boolean;
  gesperrt: boolean;
  anonym: boolean;

  constructor(data: any = {}) {
    this.uuid = data && data.uuid || null;
    this.lastAccess = data && data.lastAccess || null;
    this.datumGeaendert = data && data.datumGeaendert || null;
    this.aktiviert = data && data.aktiviert || true;
    this.gesperrt = data && data.gesperrt || false;
    this.anonym = data && data.annym || false;
  }
}

export class Benutzer {
  hateoasPayload?: HateoasPayload;
  basisdaten: PersonBasisdaten;
  erweiterteKontodaten: ErweiterteKontodaten;
  geschuetzteKontodaten: GeschuetzteKontodaten;

  constructor(data: any = {}) {
    this.hateoasPayload = data && data.hateoasPayload || null;
    this.basisdaten = data && data.basisdaten || null;
    this.erweiterteKontodaten = data && data.erweiterteKontodaten || null;
    this.geschuetzteKontodaten = data && data.geschuetzteKontodaten || true;
  }


}



