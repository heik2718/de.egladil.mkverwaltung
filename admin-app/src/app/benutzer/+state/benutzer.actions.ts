import {Action, ActionCreator} from 'redux';
import {Benutzer} from './benutzer.model';

export const BENUTZER_LOAD = '[benutzer] load';
export const BENUTZER_CLEAR = '[benutzer] clear';
export const BENUTZER_UPDATE = '[benutzer] update';

export interface BenutzerLoadAction extends Action {
  payload: Benutzer[];
}

export interface BenutzerClearAction extends Action {
  payload: Benutzer[];
}


export const loadBenutzer: ActionCreator<BenutzerLoadAction> =
  (g) => ({
    type: BENUTZER_LOAD,
    payload: g
  });

export const clearBenutzer: ActionCreator<BenutzerClearAction> =
  (g) => ({
    type: BENUTZER_CLEAR,
    payload: []
  });




