export const environment = {
  production: true,
  version: '2.0.0',
  envName: 'Production',
  apiUrl: 'https://mathe-jung-alt.de/mkvadmin',
  isDebugMode: false,
  consoleLogActive: false,
  serverLogActive: true,
  loglevel: 4
};
