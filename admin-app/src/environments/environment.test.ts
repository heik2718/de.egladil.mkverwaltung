export const environment = {
  production: false,
  version: '2.0.0-QS',
  envName: 'Test',
  apiUrl: 'http://192.168.10.173/mkvadmin',
  isDebugMode: true,
  consoleLogActive: true,
  serverLogActive: true,
  loglevel: 2
};
