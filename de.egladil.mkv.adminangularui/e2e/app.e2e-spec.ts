import { De.Egladil.Mkv.AdminangularuiPage } from './app.po';

describe('de.egladil.mkv.adminangularui App', () => {
  let page: De.Egladil.Mkv.AdminangularuiPage;

  beforeEach(() => {
    page = new De.Egladil.Mkv.AdminangularuiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
