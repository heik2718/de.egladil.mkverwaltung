export class Antwortzeile {
	nummer: string;
	pos1: boolean;
	pos2: boolean;
	pos3: boolean;
	pos4: boolean;
	pos5: boolean;
	skipped: boolean;

	constructor(nummer: string) {
		this.nummer = nummer;
		this.pos1 = false;
		this.pos2 = false;
		this.pos3 = false;
		this.pos4 = false;
		this.pos5 = false;
		this.skipped = false;
	}

	clicked(event) {
		console.log(this.nummer + '-' + event.target.id);
	}
}
