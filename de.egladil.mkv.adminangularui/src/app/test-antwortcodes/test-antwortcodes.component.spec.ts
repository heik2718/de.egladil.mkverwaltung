import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestAntwortcodesComponent } from './test-antwortcodes.component';

describe('TestAntwortcodesComponent', () => {
  let component: TestAntwortcodesComponent;
  let fixture: ComponentFixture<TestAntwortcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestAntwortcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAntwortcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
