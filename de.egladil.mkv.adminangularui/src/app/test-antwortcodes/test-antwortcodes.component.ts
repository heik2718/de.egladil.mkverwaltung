import {Component, OnInit} from '@angular/core';
import {Antwortzeile} from '../antwortzeile';

@Component({
  selector: 'antwortcodes',
  templateUrl: './test-antwortcodes.component.html'
})
export class TestAntwortcodesComponent {
  zeilen: Antwortzeile[];
  kind: string;
  klasse: string;

  constructor() {
    this.zeilen = [
      new Antwortzeile('A-1'),
      new Antwortzeile('A-2'),
      new Antwortzeile('A-3'),
      new Antwortzeile('A-4'),
      new Antwortzeile('A-5'),
      new Antwortzeile('B-1'),
      new Antwortzeile('B-2'),
      new Antwortzeile('B-3'),
      new Antwortzeile('B-4'),
      new Antwortzeile('B-5'),
      new Antwortzeile('C-1'),
      new Antwortzeile('C-2'),
      new Antwortzeile('C-3'),
      new Antwortzeile('C-4'),
      new Antwortzeile('C-5')
    ];
    this.kind = 'Jonny Calonny';
    this.klasse = '1';
  }
}
