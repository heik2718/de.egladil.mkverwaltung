import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'login-form',
  templateUrl: 'login-form.component.html'
})
export class LoginFormComponent {
	model = new User('','','');
	submitted = false;

	onSubmit() {
		this.submitted = true;
	}
}
