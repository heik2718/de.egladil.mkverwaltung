import { Component } from '@angular/core';

// Backtick (`): html kann in mehreren Zeilen einegeben werden, um es lesbarer zu machen.

@Component({
  selector: 'app-root',
  template: `<h1>{{title}}</h1>
             <!--<login-form></login-form>-->
             <antwortcodes></antwortcodes>`
})
export class AppComponent {
  title = 'MKV Admin App';
}
