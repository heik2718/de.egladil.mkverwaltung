import { Component, OnInit, Input } from '@angular/core';
import { Antwortzeile } from './antwortzeile';


@Component({
	  selector: 'antwzeile',
	  templateUrl: './antwortzeile.component.html'
	})

export class AntwortzeileComponent {
	@Input('zeile') antwort : Antwortzeile;
}
