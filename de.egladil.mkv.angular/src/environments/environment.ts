// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=test --base-href /mkv/ --output-hashing=bundles` then `environment.test.ts` will be used instead.
// `ng build --env=prod --base-href /mkv/ --output-hashing=bundles` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`. --aot
export const environment = {
  production: false,
  version: '5.0.0-DEV',
  envName: 'Development',
  apiUrl: 'http://localhost:9200/mkvapi',
  consoleLogActive: true,
  serverLogActive: true,
  loglevel: 2
};
