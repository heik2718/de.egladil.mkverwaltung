export const environment = {
  production: false,
  version: '5.0.0-QS',
  envName: 'Test',
  apiUrl: 'http://192.168.10.173/mkvapi',
  isDebugMode: true,
  consoleLogActive: true,
  serverLogActive: true,
  loglevel: 1
};
