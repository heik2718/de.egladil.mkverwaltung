export const environment = {
  production: true,
  version: '5.0.0',
  envName: 'Production',
  apiUrl: 'https://mathe-jung-alt.de/mkvapi',
  isDebugMode: false,
  consoleLogActive: false,
  serverLogActive: true,
  loglevel: 4
};
