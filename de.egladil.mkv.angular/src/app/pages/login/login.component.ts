import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, AbstractControl, Validators} from '@angular/forms';
import {mkvEmailValidator, mkvPasswordValidator, validateAllFormFields} from '../../validators/app.validators';
import {STATIC_ZUGANGSDATEN} from '../../state/actions';
import {AuthenticationTokenStore} from '../../state/store/authenticationToken.store';
import {StaticContentStore} from '../../state/store/staticContent.store';
import {MessageService} from '../../services/message.service';
import {SessionService} from '../../services/session.service';
import {LoginCredentials} from '../../models/clientPayload';
import {SessionToken} from '../../models/serverResponse';
import {AppConstants} from '../../core/app.constants';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-login-form',
  templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  username: AbstractControl;
  password: AbstractControl;
  kleber: AbstractControl;

  private loginCredentials: LoginCredentials;
  private subscriptions: Subscription;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;

  formSubmitAttempt: boolean;
  private redirectToUrl: string;

  private authenticationToken$: Observable<SessionToken>;
  private staticContent$: Observable<string>;

  constructor(private fb: FormBuilder
    , private router: Router
    , private messageService: MessageService
    , private sessionService: SessionService
    , private authTokenStore: AuthenticationTokenStore
    , private staticContentStore: StaticContentStore
    , private logger: LogService) {

    this.loginForm = fb.group({
      'username': ['', [Validators.required, mkvEmailValidator]],
      'password': ['', [Validators.required, mkvPasswordValidator]],
      'kleber': ['']
    });

    this.username = this.loginForm.controls['username'];
    this.password = this.loginForm.controls['password'];
    this.kleber = this.loginForm.controls['kleber'];

    this.subscriptions = new Subscription();
    this.authenticationToken$ = this.authTokenStore.stateSubject.asObservable();
    this.staticContent$ = this.staticContentStore.stateSubject.asObservable();

    this.infoPasswordExpanded = false;
    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;
  }

  ngOnInit() {
    this.formSubmitAttempt = false;

    const redirectSubsrciption = this.staticContent$
      .subscribe((redirectUrl: string) => {
        this.redirectToUrl = redirectUrl;
      });

    const authSubscription = this.authenticationToken$
      .subscribe(
      (authToken: SessionToken) => {
        if (authToken.accessToken) {
          if (this.redirectToUrl && this.redirectToUrl.length > 0) {
            const wohin = [];
            wohin.push(this.redirectToUrl);
            this.router.navigate(wohin);
          } else {
            this.router.navigate(['/dashboard']);
          }
        }
      });

    this.subscriptions.add(redirectSubsrciption);
    this.subscriptions.add(authSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.logger.debug('LoginComponent destroyed');
    this.messageService.clearMessage();
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    if (this.loginForm.valid) {
      this.loginCredentials = Object.assign({}, value) as LoginCredentials;
      this.logger.debug('loginCredentials: ' + JSON.stringify(this.loginCredentials));
      this.sessionService.login(this.loginCredentials);
    } else {
      validateAllFormFields(this.loginForm);
    }
  }

  togglePwdInfo(): void {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  gotoZugangsdaten(): void {
    const action = {
      type: STATIC_ZUGANGSDATEN
    };
    this.staticContentStore.dispatch(action);
    this.router.navigate(['/staticcontent']);
  }

  gotoOrderTempPwd(): void {
    this.router.navigate(['/passwort/temp/order']);
  }

  cancel(): void {
    this.router.navigate(['/landing']);
  }
}