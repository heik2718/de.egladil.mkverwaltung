import { Component, Input, OnInit, OnDestroy, AfterContentInit } from '@angular/core';


import { AuswertungService } from '../../../services/auswertung.service';
import { Teilnahme, TeilnahmeIdentifier } from '../../../models/user';
import { DownloadCode } from '../../../models/auswertung';
import { DownloadcodeStore } from '../../../state/store/downloadcode.store';
import { GlobalSettingsStore } from '../../../state/store/globalSettings.store';
import { STATISTIK_LOESUNGSZETTEL } from '../../../models/progress';
import { GlobalSettings } from '../../../models/serverResponse';
import { MessageService } from '../../../services/message.service';
import { LogService } from 'hewi-ng-lib';


import { Subscription ,  Observable } from 'rxjs';

@Component({
  selector: 'mkv-teilnahme',
  templateUrl: './teilnahme.component.html'
})
export class TeilnahmeComponent implements OnInit, OnDestroy, AfterContentInit {

  @Input() teilnahme: Teilnahme;

  wettbewerbsjahr: string;

  downloadCode: string;
  warnungText: string;
  showWarnungText: boolean;
  showDownloadLink: boolean;
  processing: boolean;
  hatStatistik: boolean;

  private _subscription: Subscription;

  private aktuelleTeilnahme$: Observable<Teilnahme>;
  private settings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private _downloadCode$: Observable<DownloadCode>;

  constructor(private globalSettingsStore: GlobalSettingsStore
    , private auswertungService: AuswertungService
    , private downloadcodeStore: DownloadcodeStore
    , private messageService: MessageService
    , private logger: LogService
  ) {
    this._subscription = new Subscription();

    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this._downloadCode$ = this.downloadcodeStore.stateSubject.asObservable();
  }

  ngOnInit() {

    this.showDownloadLink = false;
    this.downloadCode = '';


    const settingsSubscription = this.settings$.subscribe((settings: GlobalSettings) => {
      if (settings) {
        this.wettbewerbsjahr = settings.wettbewerbsjahr;
      }
    });

    const downloadCodeSubscription = this._downloadCode$
      .subscribe(
        (code: DownloadCode) => {
          if (!code || code.downloadArt !== STATISTIK_LOESUNGSZETTEL) {
            return;
          }
          this.showDownloadLink = false;
          if (code.jahr === this.teilnahme.teilnahmeIdentifier.jahr) {
            this.downloadCode = code.downloadCode;
            this.processing = false;
            if (this.downloadCode === null || this.downloadCode === undefined) {
              //
            } else {
              this.showDownloadLink = this.downloadCode.length > 0;
            }
          }
        });

    this._subscription.add(settingsSubscription);
    this._subscription.add(downloadCodeSubscription);
  }

  ngAfterContentInit() {
    this.hatStatistik = this.teilnahme.teilnahmeIdentifier.teilnahmeart === 'S' && this.teilnahme.anzahlLoesungszettel > 0;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  erstelleSchulstatistik(): void {
    this.showDownloadLink = true;
    this.processing = true;
    const tIdentifier = this.teilnahme.teilnahmeIdentifier;
    if (tIdentifier) {
      this.auswertungService.erstelleStatistik(tIdentifier);
    }
  }

  downloadResult(): void {
    if (this.showDownloadLink) {
      const tIdentifier = this.teilnahme.teilnahmeIdentifier;
      if (tIdentifier) {
        this.auswertungService.saveFile(tIdentifier, this.downloadCode);
        this.showDownloadLink = false;
        this.messageService.clearMessage();
      }
    }
  }

  showUploads(): boolean {
    if (this.teilnahme) {
      if (this.teilnahme.teilnahmeIdentifier.teilnahmeart === 'P') {
        return false;
      }
      if (this.wettbewerbsjahr !== this.teilnahme.teilnahmeIdentifier.jahr) {
        return false;
      }
      return this.teilnahme.anzahlUploads > 0;
    }
    return false;
  }

  showAnzahlKinder(): boolean {
    if (this.wettbewerbsjahr === this.teilnahme.teilnahmeIdentifier.jahr) {
      return this.teilnahme.anzahlUploads === 0;
    }
    return false;
  }


  showSchule(): boolean {
    return this.teilnahme.schule !== undefined && this.teilnahme.schule !== null;
  }

  showKollegen(): boolean {
    return this.teilnahme.kollegen && this.teilnahme.kollegen.length > 0;
  }
}

