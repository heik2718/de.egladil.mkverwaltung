import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { User, Teilnahme } from '../../../models/user';
import { OpportunityStateMap, OpportunityState } from '../../../models/opportunityState';
import { GlobalSettings } from '../../../models/serverResponse';
import { UserStore } from '../../../state/store/user.store';
import { TeilnahmenStore } from '../../../state/store/teilnahmen.store';
import { GlobalSettingsStore } from '../../../state/store/globalSettings.store';
import { UserService } from '../../../services/user.service';
import { Subscription, Observable } from 'rxjs';
import { AuswertungService } from '../../../services/auswertung.service';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './teilnahmelist.component.html',
  providers: []
})
export class TeilnahmelistComponent implements OnInit, OnDestroy {


  title: string;
  originalTitle: string;
  user: User;

  teilnahmen: Teilnahme[];

  anmeldenState: OpportunityState;

  private _subscription: Subscription;

  @Output() onTeilnahmeSelected: EventEmitter<Teilnahme>;

  private settings$: Observable<GlobalSettings>;
  private user$: Observable<User>;
  private _opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService
  private _teilnahmen$: Observable<Teilnahme[]>;

  selectedTeilnahme: Teilnahme;

  constructor(private titleService: Title
    , private teilnahmenStore: TeilnahmenStore
    , private auswertungService: AuswertungService
    , private userStore: UserStore
    , private userService: UserService
    , private settingsStore: GlobalSettingsStore
    , private logger: LogService) {

    this.title = '';
    this._subscription = new Subscription();

    this.user$ = this.userStore.stateSubject.asObservable();
    this.settings$ = this.settingsStore.stateSubject.asObservable();
    this._opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this._teilnahmen$ = this.teilnahmenStore.stateSubject.asObservable();

    this.teilnahmen = [];

    this.onTeilnahmeSelected = new EventEmitter<Teilnahme>();
  }


  ngOnInit() {
    this.originalTitle = this.titleService.getTitle();
    if (this.title) {
      this.titleService.setTitle(this.title);
    }

    const settingsSubscription = this.settings$
      .subscribe(
        (settings: GlobalSettings) => {
          this.title = 'Teilnahmen (aktuelles Wettbewerbsjahr ' + settings.wettbewerbsjahr + ')';
        });

    const teilnahmenSubscription = this._teilnahmen$
      .subscribe(
        (alle: Teilnahme[]) => {
          this.teilnahmen = alle;
        });

    const userSubscription = this.user$
      .subscribe(
        (user: User) => {
          this.logger.debug('user bekommen');
          this.user = user;
          if (this.user.basisdaten && this.teilnahmenStore.getState().length === 0) {
            this.auswertungService.loadTeilnahmen();
          }
        });

    const opportunityStateSubscription = this._opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
      });


    this._subscription.add(teilnahmenSubscription);
    this._subscription.add(settingsSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(opportunityStateSubscription);


  }

  ngOnDestroy() {
    this.titleService.setTitle(this.originalTitle);
    this._subscription.unsubscribe();
    this.teilnahmen = [];
  }

  clicked(teilnahme: Teilnahme) {
    this.selectedTeilnahme = teilnahme;
    this.onTeilnahmeSelected.emit(teilnahme);
  }

  isSelected(teilnahme: Teilnahme): boolean {
    if (this.selectedTeilnahme && teilnahme) {
      return this.selectedTeilnahme.teilnahmeIdentifier.kuerzel === teilnahme.teilnahmeIdentifier.kuerzel;
    }
    return false;
  }
}

