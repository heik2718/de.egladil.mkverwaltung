import {Component, OnInit, OnDestroy} from '@angular/core';
import {AppConstants} from '../../../core/app.constants';
import {UserStore} from '../../../state/store/user.store';
import {GlobalSettingsStore} from '../../../state/store/globalSettings.store';
import {GlobalSettings} from '../../../models/serverResponse';
import {MKVAction, CLEAR} from '../../../state/actions';
import {OpportunityStateMap, OpportunityState} from '../../../models/opportunityState';
import {OPPORTUNITY_ANMELDEN, OPPORTUNITY_AUSWERTUNG, OPPORTUNITY_DOWNLOAD, OPPORTUNITY_EXCELUPLOAD, OPPORTUNITY_PROFIL, OPPORTUNITY_TEILNAHMEN} from '../../../models/opportunityState';
import {ANMELDEN, AUSWERTUNG, DOWNLOAD, EXCELUPLOAD, TEILNEHMERUPLOAD, PROFIL, TEILNAHMEN, OPPORTUNITY_TEILNEHMERUPLOAD} from '../../../models/opportunityState';
import {User} from '../../../models/user';
import {UserDelegate} from '../../../shared/user.delegate';
import {Wettbewerbsanmeldung} from '../../../models/clientPayload';
import {AnmeldungService} from '../../../services/anmeldung.service';
import {UserService} from '../../../services/user.service';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';


@Component({
  selector: 'mkv-anmelden',
  templateUrl: './anmeldung.component.html',
})

export class AnmeldungComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;
  private user$: Observable<User>;
  private user: User;
  globalSettings: GlobalSettings;


  anmelden: OpportunityState;

  private opportunityStateMap: OpportunityStateMap;

  private settings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService

  constructor(private userStore: UserStore
    , private userService: UserService
    , private userDelegate: UserDelegate
    , private globalSettingsStore: GlobalSettingsStore
    , private teilnahmeService: AnmeldungService
    , private logger: LogService) {

    this.logger.debug('AnmeldungComponent constructor');

    this.subscriptions = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this.opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
  }

  ngOnInit() {

    const opportunityStateSubscription = this.opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmelden = map.ANMELDEN;
        this.logger.debug(this.anmelden.text);
      });

    const userSubscription = this.user$.subscribe((u: User) => {
      if (u) {
        this.user = new User(u);
      }
    });

    const settingsSubscription = this.settings$.subscribe((settings: GlobalSettings) => {
      if (settings) {
        this.globalSettings = settings;
      }
    });

    this.subscriptions.add(opportunityStateSubscription);
    this.subscriptions.add(userSubscription);
    this.subscriptions.add(settingsSubscription);

    this.logger.debug('AnmeldungComponent initialized');

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  submitAnmeldung(): void {
    const schulkuerzel = this.user.erweiterteKontodaten && this.user.erweiterteKontodaten.schule && this.user.erweiterteKontodaten.schule.kuerzel || null;
    const payload = {'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this.user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this.user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel} as Wettbewerbsanmeldung;
    this.teilnahmeService.anmelden(payload);
  }
}