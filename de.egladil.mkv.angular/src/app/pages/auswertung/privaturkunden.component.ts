import {Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {Farbschema, Teilnehmer, KLASSENSTUFE_1, KLASSENSTUFE_2, TeilnehmerUndFarbschema, DownloadCode} from '../../models/auswertung';
import {DOWNLOADART_URKUNDE} from '../../models/progress';
import {User, Teilnahme} from '../../models/user';

import {AuswertungService} from '../../services/auswertung.service';
import {MessageService} from '../../services/message.service';
import {UserService} from '../../services/user.service';

import {ActionFactory, TeilnehmerUrkundenAction, CLEAR} from '../../state/actions';
import {DownloadcodeStore} from '../../state/store/downloadcode.store';
import {FarbschemaStore} from '../../state/store/farbschema.store';
import {PrivaturkundenauftragStore} from '../../state/store/privaturkundenauftrag.store';
import {UserStore} from '../../state/store/user.store';

import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-urkunden',
  templateUrl: './privaturkunden.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class PrivaturkundenComponent implements OnInit, OnDestroy {

  urkundenForm: FormGroup;
  farbschema: AbstractControl;
  nurKaengurusprung: AbstractControl;

  selectedFarbschema: Farbschema;
  selectedFarbschemaName: string;
  btnSubmitDisabled: boolean;
  formSubmitAttempt: boolean;

  farben: Farbschema[];

  teilnehmerUrkundenauftrag: TeilnehmerUndFarbschema;
  downloadCode: string;
  showDownloadLink: boolean;
  processing: boolean;
  showLehrerwarnung: boolean;

  header: string;
  processingText: string;
  btnTextDrucken: string;

  private _subscription: Subscription;
  private _urkundenauftrag$: Observable<TeilnehmerUndFarbschema>;
  private _downloadCode$: Observable<DownloadCode>;
  private _farbschemas$: Observable<Farbschema[]>;
  private _user$: Observable<User>;

  constructor(private fb: FormBuilder
    , r: ActivatedRoute
    , private router: Router
    , private messageService: MessageService
    , private auswertungService: AuswertungService
    , private urkundenauftragStore: PrivaturkundenauftragStore
    , private downloadcodeStore: DownloadcodeStore
    , private farbschemaStore: FarbschemaStore
    , private userStore: UserStore
    , private userService: UserService
    , private logger: LogService) {

    this.urkundenForm = fb.group({
      'farbschema': ['', [Validators.required]],
      'nurKaengurusprung': [false]
    });

    this.farbschema = this.urkundenForm.controls['farbschema'];
    this._subscription = new Subscription();
    this._urkundenauftrag$ = this.urkundenauftragStore.stateSubject.asObservable();
    this._downloadCode$ = this.downloadcodeStore.stateSubject.asObservable();
    this._farbschemas$ = this.farbschemaStore.stateSubject.asObservable();
    this._user$ = this.userStore.stateSubject.asObservable();
    this.farben = [];
  }

  ngOnInit() {
    this.logger.debug('ngOnInit');
    this.showDownloadLink = false;
    this.processing = false;
    this.showLehrerwarnung = false;
    this.header = 'Urkunden drucken';
    this.processingText = 'Die Urkunden werden erstellt...';
    this.btnTextDrucken = 'Urkunden drucken';

    const farbschemasSubscription = this._farbschemas$.subscribe((schemas: Farbschema[]) => {
      for (let i = 0; i < schemas.length; i++) {
        const fs = schemas[i];
        fs.imageData = 'data:image/png;base64,' + fs.thumbnail;
        this.farben.push(fs);
      }
    });

    const userSubscription = this._user$
      .subscribe(
      (u: User) => {
        if (u.basisdaten && u.basisdaten.rolle === 'MKV_LEHRER') {
          this.showLehrerwarnung = true;
          this.header = 'Urkunde für ein Kind korrigieren';
          this.processingText = 'Die Urkunde wird erstellt...';
          this.btnTextDrucken = 'Urkunde korrigieren';
        }
      });

    const urkundenauftragSubscription = this._urkundenauftrag$
      .subscribe(
      (a: TeilnehmerUndFarbschema) => {
        this.logger.debug('urkundenauftragSubscription');
        this.teilnehmerUrkundenauftrag = a;
        if (this.teilnehmerUrkundenauftrag === undefined || this.teilnehmerUrkundenauftrag === null) {
          this.logger.debug('kein Urkundenauftrag, also navigate');
          this.router.navigate(['/teilnehmer']);
        }
      });

    const downloadCodeSubscription = this._downloadCode$
      .subscribe(
      (code: DownloadCode) => {
        if (!code || code.downloadArt !== DOWNLOADART_URKUNDE) {
          return;
        }
        this.downloadCode = code.downloadCode;
        this.processing = false;
        if (this.downloadCode === null || this.downloadCode === undefined) {
          this.showDownloadLink = false;
        } else {
          this.showDownloadLink = this.downloadCode.length > 0;
        }
        this.logger.debug('showDownloadLink=' + this.showDownloadLink);
      });

    this._subscription.add(farbschemasSubscription);
    this._subscription.add(urkundenauftragSubscription);
    this._subscription.add(downloadCodeSubscription);
    this._subscription.add(userSubscription);

    if (this.teilnehmerUrkundenauftrag === undefined || this.teilnehmerUrkundenauftrag === null) {
      this.logger.debug('kein Urkundenauftrag, also navigate');
      this.router.navigate(['/teilnehmer']);
    }
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this.urkundenauftragStore.dispatch(ActionFactory.teilnehmerUrkundenAction(CLEAR, this.teilnehmerUrkundenauftrag));
    this.downloadcodeStore.dispatch(ActionFactory.replaceStringAction(CLEAR, null));
    this.messageService.clearMessage();
  }

  onFarbeSelected() {
    this.logger.debug('farbe: ' + this.selectedFarbschemaName);
    const value = this.farben.find(ks => ks.name === this.selectedFarbschemaName);
    if (value) {
      this.selectedFarbschema = value;
    } else {
      this.selectedFarbschema = null;
    }
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    this.teilnehmerUrkundenauftrag.farbschemaName = this.selectedFarbschema.name;
    const tIdentifier = this.userService.getIdentifierAktuelleTeilnahme();
    const kaengurusprung = value.nurKaengurusprung;
    if (tIdentifier) {
      this.processing = true;
      if (kaengurusprung) {
        this.auswertungService.druckeTeilnehmerurkundenKaengurusprung(tIdentifier, this.teilnehmerUrkundenauftrag);
      } else {
        this.auswertungService.druckeTeilnehmerurkunden(tIdentifier, this.teilnehmerUrkundenauftrag);
      }
    }
    if (tIdentifier) {
      this.processing = true;

    }

    // hier service anbinden
  }

  cancel(): void {
    this.router.navigate(['/teilnehmer']);
  }

  downloadResult(): void {
    if (this.showDownloadLink) {
      const tIdentifier = this.userService.getIdentifierAktuelleTeilnahme();
      if (tIdentifier) {
        this.auswertungService.saveFile(tIdentifier, this.downloadCode);
        this.showDownloadLink = false;
        this.messageService.clearMessage();
      }
    }
  }

  isSubmitDisabled(): boolean {
    if (this.btnSubmitDisabled || !this.urkundenForm.valid) {
      return true;
    }
    if (this.processing) {
      return true;
    }
    if (this.teilnehmerUrkundenauftrag === null || this.teilnehmerUrkundenauftrag === undefined) {
      return true;
    }
    if (this.userService.getIdentifierAktuelleTeilnahme() === null) {
      this.logger.warn('aktuelle Teilnahme nicht korrekt initialisiert.');
      return false;
    }
    return false;
  }
}
