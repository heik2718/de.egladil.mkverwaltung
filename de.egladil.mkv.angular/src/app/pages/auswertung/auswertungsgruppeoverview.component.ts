import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {AppConstants} from '../../core/app.constants';

import {Auswertungsgruppe} from '../../models/auswertung';
import {TeilnahmeIdentifier} from '../../models/user';

import {MKVAction, ActionFactory, TEILNEHMER_GELADEN, ADD} from '../../state/actions';
import {AuswertungsgruppeStore} from '../../state/store/auswertungsgruppe.store';
import {TeilnehmerArrayStore} from '../../state/store/teilnehmerArray.store';

import {AuswertungsgruppeService} from '../../services/auswertungsgruppe.service';

import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-auswertungsgruppeoverview',
  templateUrl: './auswertungsgruppeoverview.component.html'
})
export class AuswertungsgruppeoverviewComponent implements OnInit {

  @Input() theGruppe: Auswertungsgruppe;

  showDialog: boolean;
  showWarnung: boolean;

  warnung: string;
  private myStyles: {};

  constructor(private router: Router
    , private route: ActivatedRoute
    , private auswertungsgruppeService: AuswertungsgruppeService
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private teilnehmerArrayStore: TeilnehmerArrayStore
    , private logger: LogService) {
  }

  ngOnInit() {
    this.showWarnung = false;
    this.warnung = '';
    const color = this._calculateBackgroundColor();
    this.myStyles = {
      'background-color': color
    };
  }

  edit(): void {
    this.logger.debug('jetzt zu Auswertungsgruppe edit/kuerzel wechseln');
    this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, this.theGruppe));
    this.router.navigate(['edit', this.theGruppe.kuerzel], {relativeTo: this.route});
  }

  gotoKinder(): void {
    this.logger.debug('alle Teilnehmer dieser Gruppe setzen, dann navigieren');
    if (!this.theGruppe.teilnehmer) {
      this.theGruppe.teilnehmer = [];
    }
    this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction('ADD', this.theGruppe));
    this.teilnehmerArrayStore.dispatch(ActionFactory.teilnehmerArrayAction(TEILNEHMER_GELADEN, this.theGruppe.teilnehmer, null));
    this.router.navigate(['auswertungsgruppe', this.theGruppe.kuerzel], {relativeTo: this.route});
  }

  delete(): void {
    const tIdentifier = {
      'teilnahmeart': this.theGruppe.teilnahmeart,
      'kuerzel': this.theGruppe.teilnahmekuerzel,
      'jahr': this.theGruppe.jahr
    } as TeilnahmeIdentifier;

    this.auswertungsgruppeService.loeschen(tIdentifier, this.theGruppe.kuerzel);
  }

  hatTeilnehmer(): boolean {
    return this.theGruppe.anzahlKinder > 0;
  }

  private _calculateBackgroundColor(): string {
    if (this.theGruppe === null || this.theGruppe === undefined) {
      return AppConstants.backgroundColors.INPUT_NEUTRAL;
    }
    if (this.hatTeilnehmer()) {
      if (this.theGruppe.anzahlKinder !== this.theGruppe.anzahlLoesungszettel) {
        this.showWarnung = true;
        this.warnung = 'erfasste Antworten: ' + this.theGruppe.anzahlLoesungszettel + ' von ' + this.theGruppe.anzahlKinder;
        return AppConstants.backgroundColors.INPUT_INVALID;
      }
      return AppConstants.backgroundColors.INPUT_NEUTRAL;
    }
    this.showWarnung = true;
    this.warnung = 'noch keine Kinder erfasst';
    return AppConstants.backgroundColors.INPUT_INVALID;
  }

  getMyStyles() {
    return this.myStyles;
  }

}
