import {Component, OnInit, OnDestroy, ChangeDetectionStrategy} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators, NgForm} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';

import {AppConstants} from '../../core/app.constants';

import {AuswertungsgruppePayload} from '../../models/clientPayload';
import {Klassenstufe, Teilnehmer, KLASSENSTUFE_1, KLASSENSTUFE_2, Auswertungsgruppe, findKlassenstufeAuswertungsgruppe, KLASSENSTUFE_I} from '../../models/auswertung';
import {TeilnahmeIdentifier} from '../../models/user';

import {MKVAction, ActionFactory, CLEAR, ADD} from '../../state/actions';

import {APIMessage, INFO, WARN, ERROR, NULL} from '../../models/serverResponse';

import {AktuelleTeilnahmeStore} from '../../state/store/aktuelleTeilnahme.store';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {AuswertungsgruppeStore} from '../../state/store/auswertungsgruppe.store';
import {RootgruppeStore} from '../../state/store/rootgruppe.store';

import {AuswertungsgruppeService} from '../../services/auswertungsgruppe.service';
import {MessageService} from '../../services/message.service';
import {TeilnahmeDelegate} from '../../shared/teilnahme.delegate';



import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';


@Component({
  selector: 'mkv-edit-auswertungsgruppe',
  templateUrl: './edit-auswertungsgruppe.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class EditAuswertungsgruppeComponent implements OnInit, OnDestroy {

  klasseForm: FormGroup;
  klassenstufe: AbstractControl;
  name: AbstractControl;
  formTitle: string;

  selectedKlassenstufe: Klassenstufe;
  selectedKlassenstufeName: string;

  klassenstufeFixed: boolean;
  btnSubmitDisabled: boolean;
  formSubmitAttempt: boolean;
  saved: boolean;
  istRootgruppe: boolean;

  nameLabel: string;
  namePlaceholder: string;

  tooltipName: string;
  infoNameExpanded: boolean;

  klassenstufen: Klassenstufe[];


  private _rootgruppe: Auswertungsgruppe;
  private _neueGruppe: boolean;
  auswertungsgruppe: Auswertungsgruppe;

  private _rootgruppe$: Observable<Auswertungsgruppe>;
  private _auswertungsgruppe$: Observable<Auswertungsgruppe>;
  private _message$: Observable<APIMessage>;

  private _subscription: Subscription;

  constructor(private fb: FormBuilder
    , private router: Router
    , private route: ActivatedRoute
    , private auswertungsgruppeService: AuswertungsgruppeService
    , private messageStore: ApiMessageStore
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private rootgruppeStore: RootgruppeStore
    , private messageService: MessageService
    , private teilnahmeDelegate: TeilnahmeDelegate
    , private logger: LogService) {



    this.klasseForm = fb.group({
      'klassenstufe': [null],
      'name': ['', [Validators.required, Validators.maxLength(110)]]
    });

    this.klassenstufe = this.klasseForm.controls['klassenstufe'];
    this.name = this.klasseForm.controls['name'];

    this.klassenstufen = [];
    this.klassenstufen.push(KLASSENSTUFE_1);
    this.klassenstufen.push(KLASSENSTUFE_2);
    this.klassenstufen.push(KLASSENSTUFE_I);

    this._rootgruppe$ = this.rootgruppeStore.stateSubject.asObservable();
    this._auswertungsgruppe$ = this.auswertungsgruppeStore.stateSubject.asObservable();
    this._message$ = this.messageStore.stateSubject.asObservable();

    this._subscription = new Subscription();

  }

  ngOnInit() {
    this.infoNameExpanded = false;
    this.tooltipName = AppConstants.tooltips.AUSWERTUNGSGRUPPE_NAME;
    this.btnSubmitDisabled = true;
    this.selectedKlassenstufeName = '';
    this.messageService.clearMessage();

    const routeSubscription = this.route.params
      .subscribe(params => {
        this.logger.debug('route.params=' + JSON.stringify(params));
        const id = (params['kuerzel'] || '');
        if (!this.rootgruppeStore.getState()) {
          this.router.navigate(['/auswertungsgruppen']);
        } else {
          this._rootgruppe = this.rootgruppeStore.getState();
        }
        if (id === 'new') {

          if (this._rootgruppe === null || this._rootgruppe === undefined) {
            this.router.navigate(['/auswertungsgruppen']);
          }

          this.formTitle = 'Klasse eintragen';
          this.nameLabel = 'Klassenname*';
          this.namePlaceholder = 'Klassenname';
          this.istRootgruppe = false;
          this._neueGruppe = true;

          const data = {
            'teilnahmeart': this._rootgruppe.teilnahmeart,
            'teilnahmekuerzel': this._rootgruppe.teilnahmekuerzel,
            'jahr': this._rootgruppe.jahr,
            'name': '',
            'parent': this._rootgruppe,
          };
          const neueGruppe = new Auswertungsgruppe(data);
          this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, neueGruppe));
        } else {
          const gruppe = this.teilnahmeDelegate.findAuswertungsgruppe(this._rootgruppe, id);
          if (!gruppe) {
            this.logger.debug('storedGruppe undefined: kuerzel=' + id);
            this.router.navigate(['/auswertungsgruppen']);
          } else {
            this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, gruppe));
          }
          this._neueGruppe = false;
        }
      });

    const auswertungsgruppeSubscription = this._auswertungsgruppe$.subscribe((g: Auswertungsgruppe) => {
      this.logger.debug('auswertungsgruppeSubscription');
      if (g === null) {
        return;
      }
      if (this.auswertungsgruppe) {
        this.logger.debug('auswertungsgruppe ausgetauscht');
      }
      if (this._rootgruppe.kuerzel === g.kuerzel) {
        this.nameLabel = 'Schulname*';
        this.namePlaceholder = 'Schulname';
        this.istRootgruppe = true;
        this._rootgruppe = g;
      } else {
        if (g.klassenstufe) {
          this.klassenstufeFixed = true;
        } else {
          this.klassenstufeFixed = false;
        }
        this.nameLabel = 'Klassenname*';
        this.namePlaceholder = 'Klassenname';
        this.istRootgruppe = false;
      }
      this.btnSubmitDisabled = true;
      this.auswertungsgruppe = g;
      this._initForm();
    });

    const messageSubscription = this._message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
        this.btnSubmitDisabled = false;
      } else {
        if (this.formSubmitAttempt = true && apiMsg.message.length > 0) {
          this.router.navigate(['/auswertungsgruppen']);
        }
      }
    });

    this._subscription.add(auswertungsgruppeSubscription);
    this._subscription.add(messageSubscription);
  }

  ngOnDestroy() {
    this.logger.debug('destroy EditAuswertungsgruppeComponent');
    this._subscription.unsubscribe();
    this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(CLEAR, null));
  }

  toggleNameInfo(): void {
    this.infoNameExpanded = !this.infoNameExpanded;
  }

  isSubmitDisabled(): boolean {
    if (this.btnSubmitDisabled || !this.klasseForm.valid) {
      //      this.logger.debug('submitDisabled 1');
      return true;
    }
    if (!this.istRootgruppe && !this.selectedKlassenstufe) {
      //      this.logger.debug('submitDisabled 2');
      return true;
    }
    if (!this._rootgruppe) {
      //      this.logger.debug('submitDisabled 3');
      return false;
    }
    //    this.logger.debug('submitDisabled 4');
    return false;
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
//    this.messageService.clearMessage();

    const payload = {
      'nameKlassenstufe': this.selectedKlassenstufe ? this.selectedKlassenstufe.name : null,
      'name': value.name
    } as AuswertungsgruppePayload;

    const tIdentifier = {
      'teilnahmeart': this._rootgruppe.teilnahmeart,
      'kuerzel': this._rootgruppe.teilnahmekuerzel,
      'jahr': this._rootgruppe.jahr
    } as TeilnahmeIdentifier;

    if (this._neueGruppe) {
      this.auswertungsgruppeService.anlegen(tIdentifier, this._rootgruppe.kuerzel, payload);
    } else {
      this.auswertungsgruppeService.aendern(tIdentifier, this.auswertungsgruppe.kuerzel, payload);
    }

    this.btnSubmitDisabled = true;
    this.saved = true;
  }

  onNameChanged(e): void {
    const name = this.klasseForm.value.name;
    if (this.auswertungsgruppe.name === null || name.trim() !== this.auswertungsgruppe.name.trim()) {
      this.btnSubmitDisabled = false;
    } else {
      this.btnSubmitDisabled = true;
    }
  }

  cancel(): void {
    this.router.navigate(['/auswertungsgruppen']);
  }

  onKlassenstufeSelected() {
    const value = this.klassenstufen.find(ks => ks.name === this.selectedKlassenstufeName);
    this.logger.debug('value=' + value);
    if (value) {
      this.selectedKlassenstufe = value;
      if (this.auswertungsgruppe !== undefined) {
        this.auswertungsgruppe.klassenstufe = value;
      }
    } else {
      this.selectedKlassenstufe = null;
      if (this.auswertungsgruppe !== undefined) {
        this.auswertungsgruppe.klassenstufe = null;
      }
    }
  }

  private _initForm() {
    const value = {
      'klassenstufe': null, 'name': null
    };

    if (this.auswertungsgruppe) {
      this.logger.debug(JSON.stringify(this.auswertungsgruppe));
      value.name = this.auswertungsgruppe.name;
      if (!this.istRootgruppe) {
        if (this.auswertungsgruppe.klassenstufe) {
          this.selectedKlassenstufe = findKlassenstufeAuswertungsgruppe(this.klassenstufen, this.auswertungsgruppe);
          this.klassenstufeFixed = true;
          value.klassenstufe = this.selectedKlassenstufe;
        }
      }
    }
    this.logger.debug(JSON.stringify(this.selectedKlassenstufe));
    this.logger.debug(JSON.stringify(value));
    this.klasseForm.setValue(value);
    this.selectedKlassenstufe = value.klassenstufe;
  }
}
