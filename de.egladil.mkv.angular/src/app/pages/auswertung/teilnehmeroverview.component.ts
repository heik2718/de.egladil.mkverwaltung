import {AppConstants} from '../../core/app.constants';
import {Component, Input, OnInit, OnDestroy, HostBinding, EventEmitter, ChangeDetectionStrategy, Output, Inject} from '@angular/core';
import {ActivatedRoute, Router, RouterStateSnapshot} from '@angular/router';
import { DOCUMENT } from "@angular/common";

import {Teilnehmer, TeilnehmerUndFarbschema, Auswertungsgruppe} from '../../models/auswertung';
import {User} from '../../models/user';

import {AuswertungService} from '../../services/auswertung.service';
import {TeilnehmerService} from '../../services/teilnehmer.service';
import {UserDelegate} from '../../shared/user.delegate';

import {ActionFactory, TeilnehmerUrkundenAction, ADD, AuswertungsgruppeAction} from '../../state/actions';
import {AuswertungsgruppeStore} from '../../state/store/auswertungsgruppe.store';
import {FarbschemaStore} from '../../state/store/farbschema.store';
import {PrivaturkundenauftragStore} from '../../state/store/privaturkundenauftrag.store';
import {UserStore} from '../../state/store/user.store';

import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';


@Component({
  selector: 'mkv-teilnehmeroverview',
  templateUrl: './teilnehmeroverview.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class TeilnehmeroverviewComponent implements OnInit, OnDestroy {

  @Input() theTeilnehmer: Teilnehmer;

  showDialog: boolean;
  infoUrkundeExpanded: boolean;
  tooltipUrkunde: string;

  showInfoUrkunde: boolean;
  showKlassenstufe: boolean;

  private myStyles: {};

  private _user: User;

  private _subscription: Subscription;
  private _user$: Observable<User>;

  constructor(private router: Router
    , private route: ActivatedRoute
    , @Inject(DOCUMENT) private document: Document
    , private auswertungService: AuswertungService
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private farbschemaStore: FarbschemaStore
    , private teilnehmerservice: TeilnehmerService
    , private urkundenauftragStore: PrivaturkundenauftragStore
    , private userDelegate: UserDelegate
    , private userStore: UserStore
    , private logger: LogService) {

    this._subscription = new Subscription();

    this._user$ = this.userStore.stateSubject.asObservable();

    this.tooltipUrkunde = AppConstants.tooltips.TEILNEHMERURKUNDE_INFO;

  }

  ngOnInit() {
    this.logger.debug('ngOnInit');
    this.showDialog = false;
    this.infoUrkundeExpanded = false;
    this.showInfoUrkunde = false;
    const color = this._calculateBackgroundColor();
    this.myStyles = {
      'background-color': color
    };

    const userSubscription = this._user$
      .subscribe(
      (user: User) => {
        this._user = user;
        if (this._user.basisdaten) {
          this.showInfoUrkunde = this.userDelegate.isLehrer(this._user);
          this.showKlassenstufe = this.userDelegate.isPrivat(this._user);
        }
      });

    this._subscription.add(userSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  edit(): void {
    this.logger.debug('teilnehmerKuerzel=' + this.theTeilnehmer.kuerzel);
    if (this.userDelegate.isLehrer(this._user)) {
      this.router.navigate(['auswertungsgruppen/auswertungsgruppe/teilnehmer/edit/' + this.theTeilnehmer.kuerzel]);
    } else {
      this.router.navigate(['edit', this.theTeilnehmer.kuerzel], {relativeTo: this.route});
    }
  }

  openPrintDialog(): void {
    if (this.farbschemaStore.getState().length === 0) {
      this.auswertungService.loadFarbschemas();
    }
    const auftrag = {
      'teilnehmer': [this.theTeilnehmer]
    } as TeilnehmerUndFarbschema;
    this.urkundenauftragStore.dispatch(ActionFactory.teilnehmerUrkundenAction(ADD, auftrag));
    this.router.navigate(['/teilnehmer/urkunden']);
  }

  hatLoesungszettel(): boolean {
    if (this.theTeilnehmer.loesungszettelKuerzel === null || this.theTeilnehmer.loesungszettelKuerzel === undefined || this.theTeilnehmer.loesungszettelKuerzel.length === 0) {
      return false;
    }
    //    this.logger.debug('Teilnehmer hat loesungszettel');
    return true;
  }

  deleteTeilnehmer(): void {
    if (this.theTeilnehmer.loesungszettelKuerzel === null || this.theTeilnehmer.loesungszettelKuerzel === undefined) {
      this._delete();
    } else {
      this.showDialog = true;
      //      this.document.body.scrollTo(0, this.document.body.scrollHeight);
      const element = this.document.activeElement;
      this.document.body.scrollTo(0, 2000);
    }
  }

  loeschenConfirmed(): void {
    this.showDialog = false;
    this._delete();
  }

  private _delete(): void {
    this.teilnehmerservice.einenTeilnehmerLoeschen(this.theTeilnehmer);
  }

  private _calculateBackgroundColor(): string {
    if (this.theTeilnehmer === null || this.theTeilnehmer === undefined) {
      return AppConstants.backgroundColors.INPUT_NEUTRAL;
    }
    if (this.hatLoesungszettel()) {
      return AppConstants.backgroundColors.INPUT_NEUTRAL;
    }
    return AppConstants.backgroundColors.INPUT_INVALID;
  }

  getMyStyles() {
    return this.myStyles;
  }

  toggleInfoUrkunde(): void {
    this.infoUrkundeExpanded = !this.infoUrkundeExpanded;
  };
}