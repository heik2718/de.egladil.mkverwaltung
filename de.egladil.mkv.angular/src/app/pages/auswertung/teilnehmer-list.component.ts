import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { DOCUMENT } from "@angular/common";

import {Teilnehmer, TeilnehmerUndFarbschema} from '../../models/auswertung';
import {getTeilnehmerToPrint, createInitialTeilnehmer} from '../../models/auswertung';
import {Wettbewerbsanmeldung} from '../../models/clientPayload';
import {OpportunityStateMap, OpportunityState} from '../../models/opportunityState';
import {APIMessage, GlobalSettings} from '../../models/serverResponse';
import {User, Teilnahme} from '../../models/user';

import {AnmeldungService} from '../../services/anmeldung.service';
import {AuswertungService} from '../../services/auswertung.service';
import {MessageService} from '../../services/message.service';
import {TeilnehmerService} from '../../services/teilnehmer.service';
import {UserDelegate} from '../../shared/user.delegate';
import {UserService} from '../../services/user.service';


import {ActionFactory, ADD, TEILNEHMER_SELECT} from '../../state/actions';
import {AktuelleTeilnahmeStore} from '../../state/store/aktuelleTeilnahme.store';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {GlobalSettingsStore} from '../../state/store/globalSettings.store';
import {FarbschemaStore} from '../../state/store/farbschema.store';
import {TeilnehmerStore} from '../../state/store/teilnehmer.store';
import {TeilnehmerArrayStore} from '../../state/store/teilnehmerArray.store';
import {PrivaturkundenauftragStore} from '../../state/store/privaturkundenauftrag.store';
import {UserStore} from '../../state/store/user.store';

import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './teilnehmer-list.component.html',
  providers: []
})
export class TeilnehmerListComponent implements OnInit, OnDestroy {

  private _user: User;
  private _teilnahme: Teilnahme;
  globalSettings: GlobalSettings;
  alleTeilnehmer: Teilnehmer[];
  anzahlTeilnehmer: number;


  title: string;

  anmeldenState: OpportunityState;
  auswertungState: OpportunityState;

  kannDrucken: boolean;
  warnungText: string;
  showWarnungText: boolean;

  private _opportunityStateMap: OpportunityStateMap;

  private _subscription: Subscription;

  private _settings$: Observable<GlobalSettings>;
  private _user$: Observable<User>;
  private _aktuelleTeilnahme$: Observable<Teilnahme>;
  private _alleTeilnehmer$: Observable<Teilnehmer[]>;
  private _opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService
  private _message$: Observable<APIMessage>;


  constructor(private router: Router
    , private route: ActivatedRoute
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private anmeldungService: AnmeldungService
    , private auswertungService: AuswertungService
    , private farbschemaStore: FarbschemaStore
    , private messageStore: ApiMessageStore
    , private userDelegate: UserDelegate
    , private userService: UserService
    , private userStore: UserStore
    , private settingsStore: GlobalSettingsStore
    , private teilnehmerService: TeilnehmerService
    , private teilnehmerStore: TeilnehmerStore
    , private teilnehmerArrayStore: TeilnehmerArrayStore
    , private urkundenauftragStore: PrivaturkundenauftragStore
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {

    this._subscription = new Subscription();

    this._user$ = this.userStore.stateSubject.asObservable();
    this._settings$ = this.settingsStore.stateSubject.asObservable();
    this._opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this._aktuelleTeilnahme$ = this.aktuelleTeilnahmeStore.stateSubject.asObservable();
    this._message$ = this.messageStore.stateSubject.asObservable();
    this._alleTeilnehmer$ = this.teilnehmerArrayStore.items.asObservable();
  }

  ngOnInit() {

    this.anzahlTeilnehmer = 0;
    this.title = 'Kinder erfassen';
    this.kannDrucken = false;
    this.warnungText = '';

    const opportunityStateSubscription = this._opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.auswertungState = map.AUSWERTUNG;
      });

    const settingsSubscription = this._settings$
      .subscribe(
      (settings: GlobalSettings) => {
        this.globalSettings = settings;
      });

    const teilnahmeSubscription = this._aktuelleTeilnahme$.subscribe(
      (t: Teilnahme) => {
        this._teilnahme = t;
        if (this._teilnahme !== null && this._teilnahme !== undefined) {
          if (this._teilnahme.teilnahmeIdentifier.teilnahmeart === 'P') {
            this.title = 'Kinder erfassen und Urkunden generieren';
          }
          this._loadTeilnehmer();
        }
      });

    const teilnehmerArraySubscription = this._alleTeilnehmer$.subscribe(
      (theTeilnehmer: Teilnehmer[]) => {
        this.alleTeilnehmer = theTeilnehmer;
        this.anzahlTeilnehmer = this.alleTeilnehmer.length;
        this._updateKannDrucken();
      });

    const userSubscription = this._user$
      .subscribe(
      (user: User) => {
        this.logger.debug('user bekommen');
        this._user = user;
        if (this.userDelegate.isLehrer(this._user)) {
          this.router.navigate(['auswertungsgruppen']);
        }
      });

    const messageSubscription = this._message$.subscribe((apiMsg: APIMessage) => {
      this.document.body.scrollTop = 0;
    });

    this._subscription.add(settingsSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(teilnahmeSubscription);
    this._subscription.add(teilnehmerArraySubscription);
    this._subscription.add(opportunityStateSubscription);
    this._subscription.add(messageSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  submitAnmeldung(): void {
    const schulkuerzel = this._user.erweiterteKontodaten.schule ? this._user.erweiterteKontodaten.schule.kuerzel : null;
    const payload = {'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this._user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this._user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel} as Wettbewerbsanmeldung;
    this.anmeldungService.anmelden(payload);
  }

  gotoNeuesKind() {
    const _teilnehmer = createInitialTeilnehmer(this._teilnahme.teilnahmeIdentifier);
    this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_SELECT, _teilnehmer));
    this.router.navigate(['edit', 'new'], {relativeTo: this.route});
  }

  gotoPrint(): void {
    const fertigeTeilnehmer = getTeilnehmerToPrint(this.alleTeilnehmer);
    const auftrag = {
      'teilnehmer': fertigeTeilnehmer
    } as TeilnehmerUndFarbschema;
    this.urkundenauftragStore.dispatch(ActionFactory.teilnehmerUrkundenAction(ADD, auftrag));

    if (this.farbschemaStore.getState().length === 0) {
      this.auswertungService.loadFarbschemas();
    }

    this.router.navigate(['teilnehmer/urkunden']);
  }

  private _updateKannDrucken(): void {
    if (this.globalSettings.downloadFreigegebenPrivat === false) {
      this.kannDrucken = false;
      this.showWarnungText = false;
      return;
    }
    this.kannDrucken = false;
    this.showWarnungText = false;
    if (this.alleTeilnehmer) {
      const fertigeTeilnehmer = getTeilnehmerToPrint(this.alleTeilnehmer);
      this.logger.debug(fertigeTeilnehmer.length + ' von ' + this.alleTeilnehmer.length + ' fertig');
      if (fertigeTeilnehmer.length > 0) {
        this.kannDrucken = true;
        if (fertigeTeilnehmer.length < this.alleTeilnehmer.length) {
          this.warnungText = 'Urkunden werden nur für die Kinder gedruckt, für die Antworten erfasst sind.';
          this.showWarnungText = true;
        } else {
          this.warnungText = '';
        }
      } else {
        this.warnungText = 'Bitte erfassen Sie Antworten um Urkunden zu drucken.';
        this.showWarnungText = true;
      }
    }
    this.logger.debug(this.warnungText);
  }

  _loadTeilnehmer() {
    if (this._teilnahme === undefined || this._teilnahme === null) {
      return;
    }
    this.teilnehmerService.laden(this._teilnahme.teilnahmeIdentifier);
  }
}