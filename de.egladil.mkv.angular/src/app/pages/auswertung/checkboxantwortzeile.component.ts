import {AppConstants} from '../../core/app.constants';
import {Component, Input} from '@angular/core';

import {Checkboxantwortzeile, Antwortcheckbox, getAnzahlChecked} from '../../models/auswertung';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-checkboxantwortzeile',
  templateUrl: './checkboxantwortzeile.component.html'
})
export class CheckboxantwortzeileComponent {

  @Input() zeile: Checkboxantwortzeile;
  valid: boolean;

  constructor(private logger: LogService) {
    this.valid = true;
    this.logger.debug('CheckboxantwortzeileComponent created');
  }

  calculateBackgroundColor(): string {
    if (this.zeile === null || this.zeile === undefined) {
      this.valid = true;
      this.logger.debug('zeile not initialized');
      return AppConstants.backgroundColors.INPUT_MISSING;
    }
//    this.logger.debug(JSON.stringify(this.zeile));

    const anzahlChecked = getAnzahlChecked(this.zeile);
    this.valid = true;
    switch (anzahlChecked) {
      case 0:
        return AppConstants.backgroundColors.INPUT_MISSING;
      case 1:
        return AppConstants.backgroundColors.INPUT_VALID;
      default:
        this.valid = false;
        return AppConstants.backgroundColors.INPUT_INVALID;
    }
  }
}
