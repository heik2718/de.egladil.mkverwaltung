import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from "@angular/common";

import { AppConstants } from '../../core/app.constants';

import { Klassenstufe, DownloadCode } from '../../models/auswertung';
import { Auswertungsgruppe, SchuleUrkundenauftrag } from '../../models/auswertung';
import { Wettbewerbsanmeldung } from '../../models/clientPayload';
import { OpportunityStateMap, OpportunityState } from '../../models/opportunityState';
import { STATISTIK_SCHULE } from '../../models/progress';
import { APIMessage, GlobalSettings } from '../../models/serverResponse';
import { User, Teilnahme } from '../../models/user';

import { AnmeldungService } from '../../services/anmeldung.service';
import { AuswertungService } from '../../services/auswertung.service';
import { AuswertungsgruppeService } from '../../services/auswertungsgruppe.service';
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';

import { UserDelegate } from '../../shared/user.delegate';

import { ActionFactory, ADD } from '../../state/actions';
import { AktuelleTeilnahmeStore } from '../../state/store/aktuelleTeilnahme.store';
import { ApiMessageStore } from '../../state/store/apiMessage.store';
import { AuswertungsgruppeStore } from '../../state/store/auswertungsgruppe.store';
import { DownloadcodeStore } from '../../state/store/downloadcode.store';
import { FarbschemaStore } from '../../state/store/farbschema.store';
import { GlobalSettingsStore } from '../../state/store/globalSettings.store';
import { RootgruppeStore } from '../../state/store/rootgruppe.store';
import { SchuleurkundenauftragStore } from '../../state/store/schuleurkundenauftrag.store';
import { UserStore } from '../../state/store/user.store';

import { Subscription ,  Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-auswertungsgruppe',
  templateUrl: './auswertungsgruppe.component.html'
})
export class AuswertungsgruppeComponent implements OnInit, OnDestroy {

  private _user: User;
  private _teilnahme: Teilnahme;

  rootgruppe: Auswertungsgruppe;

  globalSettings: GlobalSettings;

  anmeldenState: OpportunityState;
  auswertungState: OpportunityState;

  selectedKlassenstufe: Klassenstufe;
  selectedKlassenstufeName: string;

  downloadCode: string;
  tooltipSchulname: string;
  infoSchulnameExpanded: boolean;
  kannDrucken: boolean;
  warnungText: string;
  showWarnungText: boolean;
  showDownloadLink: boolean;
  processing: boolean;

  private _subscription: Subscription;

  private _settings$: Observable<GlobalSettings>;
  private _downloadCode$: Observable<DownloadCode>;
  private _user$: Observable<User>;
  private _aktuelleTeilnahme$: Observable<Teilnahme>;
  private _opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService
  private _message$: Observable<APIMessage>;
  private _rootgruppe$: Observable<Auswertungsgruppe>;


  constructor(private router: Router
    , private route: ActivatedRoute
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private anmeldungService: AnmeldungService
    , private auswertungService: AuswertungService
    , private auswertungsgruppeService: AuswertungsgruppeService
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private downloadcodeStore: DownloadcodeStore
    , private farbschemaStore: FarbschemaStore
    , private messageService: MessageService
    , private messageStore: ApiMessageStore
    , private rootgruppeStore: RootgruppeStore
    , private settingsStore: GlobalSettingsStore
    , private schuleurkundenauftragStore: SchuleurkundenauftragStore
    , private userService: UserService
    , private userDelegate: UserDelegate
    , private userStore: UserStore
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {

    this._subscription = new Subscription();

    this._user$ = this.userStore.stateSubject.asObservable();
    this._settings$ = this.settingsStore.stateSubject.asObservable();
    this._opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this._aktuelleTeilnahme$ = this.aktuelleTeilnahmeStore.stateSubject.asObservable();
    this._rootgruppe$ = this.rootgruppeStore.stateSubject.asObservable();
    this._message$ = this.messageStore.stateSubject.asObservable();
    this._downloadCode$ = this.downloadcodeStore.stateSubject.asObservable();

  }

  ngOnInit() {
    this.infoSchulnameExpanded = false;
    this.tooltipSchulname = AppConstants.tooltips.AUSWERTUNGSGRUPPE_NAME;
    this.warnungText = '';
    this.showDownloadLink = false;
    this.processing = false;

    const opportunityStateSubscription = this._opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.auswertungState = map.AUSWERTUNG;
      });

    const settingsSubscription = this._settings$
      .subscribe(
        (settings: GlobalSettings) => {
          this.globalSettings = settings;
        });

    const teilnahmeSubscription = this._aktuelleTeilnahme$.subscribe(
      (t: Teilnahme) => {
        this._teilnahme = t;
      });

    const rootgruppeSubscription = this._rootgruppe$.subscribe(
      (g: Auswertungsgruppe) => {
        if (g !== undefined && g !== null) {
          this.logger.debug('rootgruppe gefunden: ' + g.name);
          this.rootgruppe = g;
          this._updateKannDrucken();
        }
      });

    const downloadCodeSubscription = this._downloadCode$
      .subscribe(
        (code: DownloadCode) => {
          if (!code || code.downloadArt !== STATISTIK_SCHULE) {
            return;
          }
          this.downloadCode = code.downloadCode;
          this.processing = false;
          if (this.downloadCode === null || this.downloadCode === undefined) {
            this.showDownloadLink = false;
          } else {
            this.showDownloadLink = this.downloadCode.length > 0;
          }
          this.logger.debug('showDownloadLink=' + this.showDownloadLink);
        });

    const userSubscription = this._user$
      .subscribe(
        (user: User) => {
          this.logger.debug('user bekommen');
          this._user = user;
          if (this._user.basisdaten) {
            if (this.userDelegate.isPrivat(user)) {
              this.router.navigate(['/teilnehmer']);
            } else {
              this._loadRootgruppe();
            }
          }
        });

    const messageSubscription = this._message$.subscribe(() => {
      this.document.body.scrollTop = 0;
    });

    this._opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this._subscription.add(opportunityStateSubscription);
    this._subscription.add(settingsSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(teilnahmeSubscription);
    this._subscription.add(rootgruppeSubscription);
    this._subscription.add(messageSubscription);
    this._subscription.add(downloadCodeSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  toggleSchulnameInfo(): void {
    this.infoSchulnameExpanded = !this.infoSchulnameExpanded;
  };

  edit(): void {
    this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, this.rootgruppe));
    this.router.navigate(['edit', this.rootgruppe.kuerzel], { relativeTo: this.route });
  }

  submitAnmeldung(): void {
    const schulkuerzel = this._user.erweiterteKontodaten.schule ? this._user.erweiterteKontodaten.schule.kuerzel : null;
    const payload = {
      'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this._user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this._user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel
    } as Wettbewerbsanmeldung;
    this.anmeldungService.anmelden(payload);
  }

  gotoPrint(): void {
    this.logger.debug('jetzt drucken vorbereiten');
    const auftrag = {
      'kuerzelRootgruppe': this.rootgruppe.kuerzel
    } as SchuleUrkundenauftrag;
    this.schuleurkundenauftragStore.dispatch(ActionFactory.schuleUrkundenAction(ADD, auftrag));

    if (this.farbschemaStore.getState().length === 0) {
      this.auswertungService.loadFarbschemas();
    }

    this.router.navigate(['auswertungsgruppen/urkunden']);

  }

  erstelleSchulstatistik(): void {
    this.showDownloadLink = true;
    this.processing = true;
    this.auswertungService.erstelleStatistikAktuellesJahr(this.rootgruppe.kuerzel);
  }

  downloadResult(): void {
    if (this.showDownloadLink) {
      const tIdentifier = this.userService.getIdentifierAktuelleTeilnahme();
      if (tIdentifier) {
        this.auswertungService.saveFile(tIdentifier, this.downloadCode);
        this.showDownloadLink = false;
        this.messageService.clearMessage();
      }
    }
  }

  private _updateKannDrucken(): void {
    if (this.globalSettings.downloadFreigegebenLehrer === false) {
      this.kannDrucken = false;
      this.showWarnungText = false;
      return;
    }
    this.kannDrucken = false;
    this.showWarnungText = false;
    if (this.auswertungState.disabled) {
      this.kannDrucken = false;
      this.showWarnungText = false;
    } else {
      if (this.rootgruppe) {
        if (this.rootgruppe.anzahlKinder > 0) {
          this.kannDrucken = this.rootgruppe.anzahlLoesungszettel > 0;
          if (this.rootgruppe.anzahlKinder > this.rootgruppe.anzahlLoesungszettel) {
            this.warnungText = 'Bei der Auswertung werden nur die Kinder berücksichtigt, für die Antworten erfasst sind. In den gelb markierten Klassen fehlen noch Antworten.';
            this.showWarnungText = true;
          }
        } else {
          this.warnungText = 'Bitte erfassen Sie Klassen, um die Urkunden zu drucken';
          this.showWarnungText = true;
          this.kannDrucken = false;
        }
      }
    }
  }

  private _loadRootgruppe() {
    if (this._teilnahme === undefined || this._teilnahme === null) {
      return;
    }
    this.auswertungsgruppeService.rootgruppeLaden(this._teilnahme.teilnahmeIdentifier);
  }
}
