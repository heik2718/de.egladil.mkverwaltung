import { AppConstants } from '../../core/app.constants';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Klassenstufe, Antwortbuchstabe, KLASSENSTUFE_1, Teilnehmer, antwortbuchstabeValid, createAntwortbuchstabe } from '../../models/auswertung';
import { TeilnehmerStore } from '../../state/store/teilnehmer.store';
import { ActionFactory, ANTWORTBUCHSTABE_GEAENDERT, AntwortbuchstabenAction, TEILNEHMER_GEAENDERT } from '../../state/actions';
import { LogService } from 'hewi-ng-lib';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'mkv-antwortbuchstabe',
  templateUrl: './antwortbuchstabe.component.html'
})
export class AntwortbuchstabeComponent implements OnInit, OnDestroy {

  @Input() eingabe: string;
  @Input() index: number;
  @Input() rowindex: number;

  _teilnehmer$: Observable<Teilnehmer>;
  _subscription: Subscription;

  placeholder: string;
  arrayIndex: number;

  private _teilnehmer: Teilnehmer;


  private _colIndex: number;

  constructor(private logger: LogService
    , private teilnehmerStore: TeilnehmerStore) {
    this.logger.debug('AntowrtbuchstabeComponent');
    this._teilnehmer$ = this.teilnehmerStore.stateSubject.asObservable();
    this._subscription = new Subscription();
  }

  ngOnInit() {
    const teilnehmerSubscription = this._teilnehmer$.subscribe(
      (t: Teilnehmer) => {
        if (t === null) {
          return;
        }
        this._teilnehmer = t;
        if (this._teilnehmer !== null) {
          this.arrayIndex = this.rowindex * this._teilnehmer.klassenstufe.anzahlSpalten + this.index;
          //          this.logger.debug('arrayIndex=' + this.arrayIndex);
          if (this._teilnehmer.antworten !== null && this._teilnehmer.antworten[this.arrayIndex] !== undefined) {
            this.eingabe = this._teilnehmer.antworten[this.arrayIndex].buchstabe;
          } else {
            this.eingabe = '';
          }
        } else {
          this.arrayIndex = -1;
        }
        this._initPlaceholder();
      });

    this._subscription.add(teilnehmerSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  onKey(event: KeyboardEvent) {
    this.eingabe = this.eingabe.toUpperCase();
    const antwortbuchstabe = createAntwortbuchstabe(this.eingabe, this.arrayIndex);
    const antworten = this._teilnehmer.antworten;
    antworten[this.arrayIndex] = antwortbuchstabe;
    this._teilnehmer.antworten = antworten;
    this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_GEAENDERT, this._teilnehmer));
  }

  calculateBackgroundColor(): string {
    if (this.eingabe === null || this.eingabe === undefined || this.eingabe.length === 0) {
      return AppConstants.backgroundColors.INPUT_NEUTRAL;
    }
    const eingabeUpper = this.eingabe.toUpperCase();
    const valid = antwortbuchstabeValid(eingabeUpper);
    if (valid) {
      return AppConstants.backgroundColors.INPUT_VALID;
    } else {
      return AppConstants.backgroundColors.INPUT_INVALID;
    }
  }

  calculateBorderColor(): string {
    if (this.eingabe === null || this.eingabe === undefined || this.eingabe.length === 0) {
      return '#cccccc';
    }
    const eingabeUpper = this.eingabe.toUpperCase();
    const valid = antwortbuchstabeValid(eingabeUpper);
    if (valid) {
      return '#cccccc';
    } else {
      return '#a94442';
    }
  }

  private _initPlaceholder() {
    this._colIndex = this.index + 1;
    this.placeholder = '';
    switch (this.rowindex) {
      case 0:
        this.placeholder = 'A-' + this._colIndex;
        break;
      case 1:
        this.placeholder = 'B-' + this._colIndex;
        break;
      default:
        this.placeholder = 'C-' + this._colIndex;
        break;
    }
  }
}
