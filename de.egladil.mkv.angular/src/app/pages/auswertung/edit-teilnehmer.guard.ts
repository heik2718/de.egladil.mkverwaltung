import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate} from '@angular/router';
import {Observable} from 'rxjs';
import {EditTeilnehmerComponent} from './edit-teilnehmer.component';

@Injectable()
export class EditTeilnehmerGuard implements CanDeactivate<EditTeilnehmerComponent> {

  canDeactivate(component: EditTeilnehmerComponent,
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot): Observable<boolean> | boolean {
    return component.canDeactivate();
  }
}
