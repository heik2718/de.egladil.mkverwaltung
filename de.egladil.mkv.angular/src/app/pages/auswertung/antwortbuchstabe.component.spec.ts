import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AntwortbuchstabeComponent} from './antwortbuchstabe.component';

describe('AntwortbuchstabeComponent', () => {
  let component: AntwortbuchstabeComponent;
  let fixture: ComponentFixture<AntwortbuchstabeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AntwortbuchstabeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AntwortbuchstabeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//  it('should be created', () => {
//    expect(component).toBeTruthy();
//  });
});
