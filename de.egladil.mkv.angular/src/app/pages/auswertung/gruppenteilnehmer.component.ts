import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from "@angular/common";

import { Teilnehmer, createInitialTeilnehmerForKlasse } from '../../models/auswertung';
import { Auswertungsgruppe, findAuswertungsgruppe } from '../../models/auswertung';
import { Wettbewerbsanmeldung } from '../../models/clientPayload';
import { OpportunityStateMap, OpportunityState} from '../../models/opportunityState';
import { APIMessage, GlobalSettings } from '../../models/serverResponse';
import { User, Teilnahme } from '../../models/user';

import { AnmeldungService } from '../../services/anmeldung.service';
import { MessageService } from '../../services/message.service';
import { TeilnehmerService } from '../../services/teilnehmer.service';
import { UserDelegate } from '../../shared/user.delegate';
import { UserService } from '../../services/user.service';


import { ActionFactory, ADD, TEILNEHMER_SELECT } from '../../state/actions';
import { ApiMessageStore } from '../../state/store/apiMessage.store';
import { GlobalSettingsStore } from '../../state/store/globalSettings.store';
import { RootgruppeStore } from '../../state/store/rootgruppe.store';
import { TeilnehmerStore } from '../../state/store/teilnehmer.store';
import { TeilnehmerArrayStore } from '../../state/store/teilnehmerArray.store';
import { UserStore } from '../../state/store/user.store';

import { Subscription ,  Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';


@Component({
  selector: 'mkv-gruppenteilnehmer',
  templateUrl: './gruppenteilnehmer.component.html'
})
export class GruppenteilnehmerComponent implements OnInit, OnDestroy {

  private _user: User;
  private _loesungenVollstaendig: boolean;

  globalSettings: GlobalSettings;
  alleTeilnehmer: Teilnehmer[];
  anzahlTeilnehmer: number;
  title: string;

  anmeldenState: OpportunityState;
  auswertungState: OpportunityState;

  auswertungsgruppe: Auswertungsgruppe;
  private _subscription: Subscription;

  private _settings$: Observable<GlobalSettings>;
  private _user$: Observable<User>;
  private _aktuelleTeilnahme$: Observable<Teilnahme>;
  private _opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService
  private _message$: Observable<APIMessage>;
  private _alleTeilnehmer$: Observable<Teilnehmer[]>;

  constructor(private router: Router
    , private route: ActivatedRoute
    , private anmeldungService: AnmeldungService
    , private messageService: MessageService
    , private messageStore: ApiMessageStore
    , private userDelegate: UserDelegate
    , private userService: UserService
    , private userStore: UserStore
    , private settingsStore: GlobalSettingsStore
    , private rootgruppeStore: RootgruppeStore
    , private teilnehmerService: TeilnehmerService
    , private teilnehmerStore: TeilnehmerStore
    , private teilnehmerArrayStore: TeilnehmerArrayStore
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {

    this.title = 'Kinder erfassen';
    this._subscription = new Subscription();

    this._user$ = this.userStore.stateSubject.asObservable();
    this._settings$ = this.settingsStore.stateSubject.asObservable();
    this._opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this._message$ = this.messageStore.stateSubject.asObservable();
    this._alleTeilnehmer$ = this.teilnehmerArrayStore.items.asObservable();

  }

  ngOnInit() {

    const routeSubscription = this.route.params
      .subscribe(params => {
        this.logger.debug('route.params=' + JSON.stringify(params));

        const rootgruppe = this.rootgruppeStore.getState();
        if (rootgruppe === null) {
          this.router.navigate(['/auswertungsgruppen']);
        }
        const id = (params['kuerzel'] || '');
        if (id !== 'new' && id !== '') {
          this.auswertungsgruppe = findAuswertungsgruppe(rootgruppe, id);
          this._checkAndLoadTeilnehmer();
        }
      });

    const opportunityStateSubscription = this._opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.auswertungState = map.AUSWERTUNG;
      });

    const settingsSubscription = this._settings$
      .subscribe(
        (settings: GlobalSettings) => {
          this.globalSettings = settings;
        });

    const teilnehmerArraySubscription = this._alleTeilnehmer$.subscribe(
      (theTeilnehmer: Teilnehmer[]) => {
        this.alleTeilnehmer = theTeilnehmer;
        this.anzahlTeilnehmer = this.alleTeilnehmer.length;
        this._updateLoesungenVollstaendig();
      });

    const userSubscription = this._user$
      .subscribe(
        (user: User) => {
          this.logger.debug('user bekommen');
          this._user = user;
          if (this._user.basisdaten) {
            if (this.userDelegate.isPrivat(this._user)) {
              this.router.navigate(['teilnehmer']);
            }
          }
        });

    const messageSubscription = this._message$.subscribe((apiMsg: APIMessage) => {
      this.document.body.scrollTop = 0;
    });

    this._subscription.add(settingsSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(teilnehmerArraySubscription);
    this._subscription.add(routeSubscription);
    this._subscription.add(opportunityStateSubscription);
    this._subscription.add(messageSubscription);

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  gotoNeuesKind() {
    const teilnehmer = createInitialTeilnehmerForKlasse(this.auswertungsgruppe);
    this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_SELECT, teilnehmer));
    this.router.navigate(['/auswertungsgruppen/auswertungsgruppe/teilnehmer/edit/new']);
  }

  submitAnmeldung(): void {
    const schulkuerzel = this._user.erweiterteKontodaten.schule ? this._user.erweiterteKontodaten.schule.kuerzel : null;
    const payload = {
      'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this._user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this._user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel
    } as Wettbewerbsanmeldung;
    this.anmeldungService.anmelden(payload);
  }

  cancel(): void {
    this.messageService.clearMessage();
    this.router.navigate(['/auswertungsgruppen']);
  }

  showWarnung(): boolean {
    if (this.globalSettings.downloadFreigegebenLehrer === false) {
      return false;
    }
    return !this.auswertungState.disabled && !this._loesungenVollstaendig;
  }

  private _updateLoesungenVollstaendig(): void {
    this._loesungenVollstaendig = this.auswertungsgruppe.anzahlKinder === this.auswertungsgruppe.anzahlLoesungszettel;
  }

  _checkAndLoadTeilnehmer() {
    if (this.auswertungsgruppe === null || this.auswertungsgruppe === undefined) {
      return;
    }
    if (this.auswertungsgruppe.teilnehmer !== null && this.auswertungsgruppe.teilnehmer !== undefined && this.auswertungsgruppe.teilnehmer.length === 0) {
      this.teilnehmerService.zuAuswertunggruppeLaden(this.auswertungsgruppe);
    }
  }
}
