import {ViewChild, Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators, NgForm, ReactiveFormsModule} from '@angular/forms';
import { DOCUMENT } from "@angular/common";

import {AppConstants} from '../../core/app.constants';
import {environment} from '../../../environments/environment';

import {Klassenstufe, Teilnehmer, KLASSENSTUFE_1, KLASSENSTUFE_2, KLASSENSTUFE_I, Matrixantwortzeile, Antwortbuchstabe, Auswertungsgruppe} from '../../models/auswertung';
import {Sprache, SPRACHE_DE, SPRACHE_EN} from '../../models/auswertung';
import {createInitialTeilnehmer, findKlassenstufeTeilnehmer, createAntwortbuchstabe, createInitialAntwortbuchstaben, antwortbuchstabeValid} from '../../models/auswertung';
import {findKlassenstufeAuswertungsgruppe, EINGABEMODUS_DEFAULT, EINGABEMODUS_SCHNELL, Checkboxantwortzeile, isAntwortzettelLeer} from '../../models/auswertung';
import {mapToAntwortbuchstaben, createInitialCheckboxAntwortzeilen, findSpracheTeilnehmer} from '../../models/auswertung';
import {Teilnahme} from '../../models/user';
import {APIMessage, INFO, WARN, ERROR, GlobalSettings} from '../../models/serverResponse';
import {User, TeilnahmeIdentifier, createTeilnahmeIdentifier} from '../../models/user';

import {MessageService} from '../../services/message.service';
import {TeilnehmerService} from '../../services/teilnehmer.service';

import {LOAD_ANTWORTBUCHSTABEN, TEILNEHMER_SELECT, TEILNEHMER_RELEASE, ActionFactory, RESET_ANTWORTBUCHTABEN, TEILNEHMER_GEAENDERT} from '../../state/actions';
import {AktuelleTeilnahmeStore} from '../../state/store/aktuelleTeilnahme.store';
import {AuswertungsgruppeStore} from '../../state/store/auswertungsgruppe.store';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {GlobalSettingsStore} from '../../state/store/globalSettings.store';
import {TeilnehmerArrayStore} from '../../state/store/teilnehmerArray.store';
import {TeilnehmerStore} from '../../state/store/teilnehmer.store';
import {UserStore} from '../../state/store/user.store';

import {UserDelegate} from '../../shared/user.delegate';

import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-edit-teilnehmer',
  templateUrl: './edit-teilnehmer.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class EditTeilnehmerComponent implements OnInit, OnDestroy {

  //  @ViewChild(NgForm) teilnehmerForm: FormGroup;
  teilnehmerForm: FormGroup;
  vorname: AbstractControl;
  nachname: AbstractControl;
  zusatz: AbstractControl;
  klassenstufe: AbstractControl;
  formTitle: string;

  selectedKlassenstufe: Klassenstufe;
  selectedKlassenstufeName: string;
  btnSubmitDisabled: boolean;
  formSubmitAttempt: boolean;

  selectedSprache: Sprache;
  selectedSpracheKuerzel: string;

  antwortcode: string;
  // showAntwortcode = !environment.production;
  showAntwortcode = false;


  infoZusatzExpanded: boolean;
  tooltipZusatz: string;
  infoEingabemodusExpanded: boolean;

  tooltipDefaulteingabe: string;
  tooltipSchnelleingabe: string;
  tooltipAntwortenLoeschen: string;
  textBtnDefaulteingabe: string;
  textBtnSchnelleingabe: string;
  textBtnAntwortenLoeschen: string;

  auswertungsgruppe: Auswertungsgruppe;
  teilnehmer: Teilnehmer;
  klassenstufen: Klassenstufe[];
  sprachen: Sprache[];

  matrixantwortzeilen: Matrixantwortzeile[];
  checkboxAntwortzeilen: Checkboxantwortzeile[];

  aktuelleTeilnahme: Teilnahme;

  klassenstufeFixed: boolean;

  checkboxEingabeEnabled: boolean;
  matrixEingabeEnabled: boolean;
  antwortbereichExpanded: boolean;
  showDialog: boolean;
  nameKindSicherheitsabfrage: string;

  private _speichernUndSchliessen: boolean;

  saved = false;
  //  fertig = false;
  private _formInitializing: boolean;
  //  private _matrixeingabeStarted: boolean;
  private _settings: GlobalSettings;

  private _subscription: Subscription;

  private user: User;
  private _user$: Observable<User>;
  private _aktuelleTeilnahme$: Observable<Teilnahme>;
  private _alleTeilnehmer$: Observable<Teilnehmer[]>;
  private _message$: Observable<APIMessage>;
  private _teilnehmer$: Observable<Teilnehmer>;
  private _auswertungsgruppe$: Observable<Auswertungsgruppe>;
  private _settings$: Observable<GlobalSettings>;

  constructor(private fb: FormBuilder
    , private router: Router
    , private route: ActivatedRoute
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private messageService: MessageService
    , private messageStore: ApiMessageStore
    , private settingsStore: GlobalSettingsStore
    , private teilnehmerArrayStore: TeilnehmerArrayStore
    , private teilnehmerService: TeilnehmerService
    , private teilnehmerStore: TeilnehmerStore
    , private userStore: UserStore
    , private userDelegate: UserDelegate
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {

    this.logger.debug('EditTeilnehmerComponent create');
    this._aktuelleTeilnahme$ = aktuelleTeilnahmeStore.stateSubject.asObservable();
    this._alleTeilnehmer$ = teilnehmerArrayStore.items.asObservable();
    this._message$ = this.messageStore.stateSubject.asObservable();
    this._teilnehmer$ = this.teilnehmerStore.stateSubject.asObservable();
    this._user$ = this.userStore.stateSubject.asObservable();
    this._auswertungsgruppe$ = this.auswertungsgruppeStore.stateSubject.asObservable();
    this._settings$ = this.settingsStore.stateSubject.asObservable();

    this.teilnehmerForm = fb.group({
      'klassenstufe': [new FormControl(null), [Validators.required]],
      'vorname': ['', [Validators.required, Validators.maxLength(55)]],
      'nachname': ['', [Validators.maxLength(55)]],
      'zusatz': ['', [Validators.maxLength(55)]],
      'sprache': [new FormControl(null), [Validators.required]]
    });

    this.vorname = this.teilnehmerForm.controls['vorname'];
    this.nachname = this.teilnehmerForm.controls['nachname'];
    this.zusatz = this.teilnehmerForm.controls['zusatz'];

    this.tooltipZusatz = AppConstants.tooltips.TEILNEHMER_ZUSATZ;
    this.tooltipDefaulteingabe = AppConstants.tooltips.ANTWORTEN_DEFAULTEINGABE;
    this.tooltipSchnelleingabe = AppConstants.tooltips.ANTWORTEN_SCHNELLEINGABE;
    this.tooltipAntwortenLoeschen = AppConstants.tooltips.ANTWORTEN_LOESCHEN;
    this.textBtnDefaulteingabe = AppConstants.buttontexte.BTN_ANTWORTEN_DEFAULTEINGABE;
    this.textBtnSchnelleingabe = AppConstants.buttontexte.BTN_ANTWORTEN_SCHNELLEINGABE;
    this.textBtnAntwortenLoeschen = AppConstants.buttontexte.BTN_ANTWORTEN_LOESCHEN;
    this.klassenstufen = [];
    this.klassenstufen.push(KLASSENSTUFE_1);
    this.klassenstufen.push(KLASSENSTUFE_2);
    this.klassenstufen.push(KLASSENSTUFE_I);

    this.sprachen = [];
    this.sprachen.push(SPRACHE_DE);
    this.sprachen.push(SPRACHE_EN);

    this._subscription = new Subscription();
  }

  ngOnInit() {
    this._speichernUndSchliessen = false;
    this.showDialog = false;
    this.nameKindSicherheitsabfrage = 'das Kind';
    this._formInitializing = false;
    this.antwortcode = '';
    this.klassenstufeFixed = false;

    this.logger.debug('init EditTeilnehmerComponent');

    this.btnSubmitDisabled = false;
    this.formSubmitAttempt = false;
    this.matrixantwortzeilen = [];
    this.checkboxAntwortzeilen = [];
    this.infoZusatzExpanded = false;
    this.antwortbereichExpanded = false;
    this.infoEingabemodusExpanded = false;
    this.messageService.clearMessage();

    const routeSubscription = this.route.params
      .subscribe(params => {
        this.logger.debug('route.params=' + JSON.stringify(params));
        const id = (params['kuerzel'] || '');
        if (id !== '' && id !== 'new') {
          this.teilnehmer = this.teilnehmerArrayStore.getTeilnehmer(id);
          if (this.teilnehmer && this.teilnehmer.kuerzel) {
            this._setSelectedKlassenstufe(this.teilnehmer.klassenstufe);
            this._setSelectedSprache(this.teilnehmer.sprache);
            this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_SELECT, this.teilnehmer));
            this._initForm();
            this.formTitle = 'Kind ändern';
          } else {
            this.logger.debug('thisTeilnehmer undefined: kuerzel=' + id);
            this.router.navigate(['/dashboard']);
          }
        } else {
          this.formTitle = 'Kind eintragen';
          const u = this.userStore.getState();
          if (this.userDelegate.isLehrer(u)) {
            const g = this.auswertungsgruppeStore.getState();
            if (g === undefined || g === null) {
              this.router.navigate(['/auswertungsgruppen']);
            }
          }
          this._setSelectedKlassenstufe(null);
          this._setSelectedSprache(SPRACHE_DE);
        }
      });

    const userSubscription = this._user$.subscribe((u: User) => {
      if (u) {
        this.user = new User(u);
      }
    });

    const settingsSubscription = this._settings$
      .subscribe(
      (settings: GlobalSettings) => {
        this._settings = settings;
      });


    const auswertungsgruppeSubscription = this._auswertungsgruppe$.subscribe((g: Auswertungsgruppe) => {
      this.auswertungsgruppe = g;
    });

    const aktTeilnahmeSubscription = this._aktuelleTeilnahme$.subscribe(
      (teilnahme: Teilnahme) => {
        this.logger.debug('teilnahme bekommen');
        this.aktuelleTeilnahme = teilnahme;
        if (this.teilnehmer === undefined) {
          this.teilnehmer = createInitialTeilnehmer(this.aktuelleTeilnahme.teilnahmeIdentifier);
        }
        this._initMatrixantwortzeilen(this.teilnehmer);
      });

    const teilnehmerSubscription = this._teilnehmer$.subscribe(
      (t: Teilnehmer) => {
        if (t === null) {
          return;
        }
        this.teilnehmer = t;
        if (this.teilnehmer.eingabemodus === EINGABEMODUS_DEFAULT) {
          this.matrixEingabeEnabled = false;
          this.checkboxEingabeEnabled = true;
          this._initCheckboxAntwortzeilen(this.teilnehmer);
        }
        if (this.teilnehmer.eingabemodus === EINGABEMODUS_SCHNELL) {
          this.checkboxEingabeEnabled = false;
          this.matrixEingabeEnabled = true;
          this._initMatrixantwortzeilen(this.teilnehmer);
        }
        this.antwortcode = '';
        if (this.teilnehmer.antworten !== undefined && this.teilnehmer.antworten !== null) {
          const items = this.teilnehmer.antworten;
          for (let i = 0; i < items.length; i++) {
            this.antwortcode += items[i].buchstabe;
          }
        }
        this._initForm();
      });

    const messageSubscription = this._message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
        this.btnSubmitDisabled = false;
        this.checkboxEingabeEnabled = false;
        this.matrixEingabeEnabled = false;
      } else {
        if (this.formSubmitAttempt = true && this._speichernUndSchliessen && apiMsg.message.length > 0) {
          if (this.aktuelleTeilnahme.teilnahmeIdentifier.teilnahmeart === 'S') {
            const path = '/auswertungsgruppen/auswertungsgruppe/' + this.auswertungsgruppe.kuerzel;
            this.router.navigate([path]);
          } else {
            this.router.navigate(['/teilnehmer']);
          }
        }
      }
    });

    this.messageService.clearMessage();

    this._subscription.add(messageSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(teilnehmerSubscription);
    this._subscription.add(aktTeilnahmeSubscription);
    this._subscription.add(routeSubscription);
    this._subscription.add(settingsSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  hatLoesungszettel(): boolean {
    if (this.teilnehmer.loesungszettelKuerzel === null || this.teilnehmer.loesungszettelKuerzel === undefined || this.teilnehmer.loesungszettelKuerzel.length === 0) {
      return false;
    }
    return true;
  }

  /**
   * wird durch den EditTeilnehmerGuard aufgerufen
   */
  canDeactivate(): boolean {
    if (this.saved || !this.teilnehmerForm.dirty) {
      return true;
    }
    return window.confirm(`Ihr Formular besitzt ungespeicherte Änderungen. Möchten Sie die Seite wirklich verlassen?`);
  }

  toggleZusatzInfo(): void {
    this.infoZusatzExpanded = !this.infoZusatzExpanded;
  };

  // momentan nicht verwendet
  toggleInfoEingabemodus(): void {
    this.infoEingabemodusExpanded = !this.infoEingabemodusExpanded;
  }

  antwortenLoeschen(): void {
    //    this.messageService.clearMessage();
    this.checkboxEingabeEnabled = false;
    this.matrixEingabeEnabled = false;
    this.btnSubmitDisabled = false;
    this.checkboxAntwortzeilen = null;
    this.teilnehmer.antworten = null;
    this.teilnehmer.eingabemodus = null;
    this.antwortbereichExpanded = false;
    this._speichernUndSchliessen = false;
    this._submit();
    this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_GEAENDERT, this.teilnehmer));
  }

  selectDefaultantworten(): void {
    //    this.messageService.clearMessage();
    this.checkboxEingabeEnabled = true;
    this.matrixEingabeEnabled = false;
    this.btnSubmitDisabled = false;
    this._initCheckboxAntwortzeilen(this.teilnehmer);
    this.teilnehmer.eingabemodus = EINGABEMODUS_DEFAULT;
    this.antwortbereichExpanded = true;
    this.infoEingabemodusExpanded = false;
    //    this.fertig = false;
  };

  selectSchnelleingabe(): void {
    //    this.messageService.clearMessage();
    //    this.antwortbereichExpanded = true;
    //    this.checkboxEingabeEnabled = false;
    //    this.btnSubmitDisabled = false;
    //    this.infoEingabemodusExpanded = false;
    //    if (this.teilnehmer.antworten === null || this.teilnehmer.antworten === undefined || this.teilnehmer.antworten.length === 0) {
    //      this.teilnehmer.vorname = this.teilnehmerForm.get('vorname').value;
    //      this.teilnehmer.nachname = this.teilnehmerForm.get('nachname').value;
    //      this.teilnehmer.zusatz = this.teilnehmerForm.get('zusatz').value;
    //      this.teilnehmer.klassenstufe = this.selectedKlassenstufe;
    //      this.teilnehmer.antworten = createInitialAntwortbuchstaben(this.selectedKlassenstufe);
    //      this.teilnehmer.eingabemodus = EINGABEMODUS_SCHNELL;
    //      this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_GEAENDERT, this.teilnehmer));
    //      this.textBtnToggleAntworten = 'Antworten ausblenden';
    //    }
  };

  onKlassenstufeSelected() {
    if (this._formInitializing) {
      return;
    }
    const value = this.klassenstufen.find(ks => ks.name === this.selectedKlassenstufeName);
    if (value) {
      this._setSelectedKlassenstufe(value);
      if (this.teilnehmer !== undefined) {
        if (this.teilnehmer.klassenstufe !== value) {
          const eingaben = this.teilnehmerForm.value;
          this.teilnehmer.vorname = eingaben && eingaben.vorname || null;
          this.teilnehmer.nachname = eingaben && eingaben.nachname || null;
          this.teilnehmer.zusatz = eingaben && eingaben.zusatz || null;
          this.teilnehmer.klassenstufe = this.selectedKlassenstufe;
          this.teilnehmer.antworten = createInitialAntwortbuchstaben(value);
          this._initMatrixantwortzeilen(this.teilnehmer);
          this._setSelectedSprache(this.teilnehmer.sprache);
        }
        this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_GEAENDERT, this.teilnehmer));
      }
    } else {
      this._setSelectedKlassenstufe(null);
    }
  }

  onSpracheSelected() {
    if (this._formInitializing) {
      return;
    }
    const value = this.sprachen.find(sp => sp.kuerzel === this.selectedSpracheKuerzel);
    if (value) {
      this._setSelectedSprache(value);
      if (this.teilnehmer !== undefined) {
        if (this.teilnehmer.sprache !== value) {
          const eingaben = this.teilnehmerForm.value;
          this.teilnehmer.vorname = eingaben && eingaben.vorname || null;
          this.teilnehmer.nachname = eingaben && eingaben.nachname || null;
          this.teilnehmer.zusatz = eingaben && eingaben.zusatz || null;
          this.teilnehmer.klassenstufe = this.selectedKlassenstufe;
          this.teilnehmer.sprache = this.selectedSprache;
        }
      }
    }
  }

  onSubmit(value: any): void {
    this.logger.debug('you submitted value:' + value);
    this.teilnehmer.vorname = value.vorname;
    this.teilnehmer.nachname = value.nachname;
    this.teilnehmer.zusatz = value.zusatz;
    this._speichernUndSchliessen = true;
    if (this.checkboxEingabeEnabled && isAntwortzettelLeer(this.checkboxAntwortzeilen)) {
      this.nameKindSicherheitsabfrage = this.teilnehmer.vorname;
      this.showDialog = true;
    } else {
      this._submit();
    }
  }

  leererAntwortzettelConfirmed(): void {
    this.showDialog = false;
    this._submit();
  }

  leererAntwortzettelDenied(): void {
    this.showDialog = false;
  }

  private _submit() {
    this.formSubmitAttempt = true;
    if (this.auswertungsgruppe !== undefined && this.auswertungsgruppe !== null) {
      this.teilnehmer.auswertungsgruppe = this.auswertungsgruppe;
    }

    const checkboxZeilen = this.checkboxAntwortzeilen !== null && this.checkboxAntwortzeilen !== undefined && this.checkboxAntwortzeilen.length > 0;

    if (this.teilnehmer.eingabemodus === EINGABEMODUS_DEFAULT && this.checkboxEingabeEnabled || checkboxZeilen) {
      this.teilnehmer.antworten = mapToAntwortbuchstaben(this.checkboxAntwortzeilen);
    }

    const anzahlEingaben = this._getAnzahlEingaben();
    if (anzahlEingaben === 0) {
      this.teilnehmer.antworten = [];
      this.teilnehmer.eingabemodus = null;
    }

    this.teilnehmerService.speichern(this.teilnehmer);

    this.btnSubmitDisabled = true;
    this.saved = true;
    this.document.body.scrollTop = 0;
    this.antwortbereichExpanded = false;
    //    this.fertig = true;
  }

  cancel(): void {
    this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_RELEASE, this.teilnehmer));
    this.messageService.clearMessage();
    if (this.userDelegate.isLehrer(this.user)) {
      this.router.navigate(['/auswertungsgruppen/auswertungsgruppe/' + this.auswertungsgruppe.kuerzel]);
    } else {
      this.router.navigate(['/teilnehmer']);
    }
  }

  isSubmitDisabled(): boolean {
    //    const antwValid = this.antwortenValid();
    //    if (this.btnSubmitDisabled || !this.teilnehmerForm.valid || !antwValid || !this._checkboxenValid()) {
    if (this.btnSubmitDisabled || !this.teilnehmerForm.valid) {
      return true;
    }
    return false;
  }

  isShowPiktogrammKlasse1(): boolean {
    if (this.matrixEingabeEnabled && this.selectedKlassenstufe && this.selectedKlassenstufe.name === 'EINS') {
      return true;
    }
    return false;
  }

  isShowPiktogrammKlasse2(): boolean {
    if (this.matrixEingabeEnabled && this.selectedKlassenstufe && this.selectedKlassenstufe.name === 'ZWEI') {
      return true;
    }
    return false;
  }

  isEingabemodusSelected(): boolean {
    if (this.teilnehmer.eingabemodus === undefined || this.teilnehmer.eingabemodus === null) {
      return false;
    }
    return true;
  }

  kannAntwortenEintragen(): boolean {
    if (this.userDelegate.isLehrer(this.user)) {
      if (this._settings.downloadFreigegebenLehrer === false) {
        return false;
      }
    } else {
      if (this._settings.downloadFreigegebenPrivat === false) {
        return false;
      }
    }
    if (this.selectedKlassenstufe === null || this.selectedKlassenstufe === undefined) {
      return false;
    }
    return true;
  }

  //  antwortenValid(): boolean {
  //    if (this.teilnehmer === undefined || this.teilnehmer === null) {
  //      //      this.logger.debug('antwortenValid 1');
  //      return true;
  //    }
  //    const antworten = this.teilnehmer.antworten;
  //    if (antworten === undefined) {
  //      //      this.logger.debug('antwortenValid 2');
  //      return true;
  //    }
  //    const anzahlEingaben = this._getAnzahlEingaben();
  //    if (anzahlEingaben === 0) {
  //      //      this.logger.debug('antwortenValid 3');
  //      return true;
  //    }
  //    for (let i = 0; i < antworten.length; i++) {
  //      if (!antwortbuchstabeValid(antworten[i].buchstabe)) {
  //        //        this.logger.debug('antwortenValid 4: index = ' + i);
  //        return false;
  //      }
  //    }
  //    this.logger.debug('antwortenValid 5');
  //    return true;
  //  }

  //  _checkboxenValid(): boolean {
  //    return isCheckboxenValid(this.checkboxAntwortzeilen);
  //  }

  private _getAnzahlEingaben(): number {
    if (this.teilnehmer === undefined || this.teilnehmer === null || this.teilnehmer.antworten === null) {
      return 0;
    }
    const antworten = this.teilnehmer.antworten;
    let anzahlEingaben = 0;
    for (let i = 0; i < antworten.length; i++) {
      if (antworten[i].buchstabe.length > 0) {
        anzahlEingaben++;
      }
    }
    return anzahlEingaben;
  }

  private _initForm() {
    this._formInitializing = true;
    if (this.teilnehmer !== undefined) {
      this.logger.debug(JSON.stringify(this.teilnehmer));
      if (this.teilnehmer.kuerzel === null || this.teilnehmer.kuerzel === undefined) {
        if (this.auswertungsgruppe) {
          this._setSelectedKlassenstufe(findKlassenstufeAuswertungsgruppe(this.klassenstufen, this.auswertungsgruppe));
        }
      } else {
        this._setSelectedKlassenstufe(findKlassenstufeTeilnehmer(this.klassenstufen, this.teilnehmer));
      }
      this._setSelectedSprache(this.teilnehmer.sprache);
    } else {
      if (this.auswertungsgruppe) {
        this._setSelectedKlassenstufe(findKlassenstufeAuswertungsgruppe(this.klassenstufen, this.auswertungsgruppe));
      }
      this._setSelectedSprache(SPRACHE_DE);
    }
    this.logger.debug(JSON.stringify(this.selectedKlassenstufe));
    const value = {
      'klassenstufe': this.selectedKlassenstufe
      , 'vorname': this.teilnehmer.vorname
      , 'nachname': this.teilnehmer.nachname !== null ? this.teilnehmer.nachname : ''
      , 'zusatz': this.teilnehmer.zusatz !== null ? this.teilnehmer.zusatz : ''
      , 'sprache': this.selectedSprache
    };
    this.logger.debug(JSON.stringify(value));
    this.teilnehmerForm.setValue(value);
    this._formInitializing = false;
  }

  private _initMatrixantwortzeilen(teilnehmer: Teilnehmer): void {
    if (!this.selectedKlassenstufe) {
      return;
    }
    const anzahl = this.selectedKlassenstufe.anzahlSpalten;
    for (let index = 0; index < 3; index++) {
      const eingaben = [];
      for (let j = 0; j < anzahl; j++) {
        if (teilnehmer !== null && teilnehmer.antworten !== null && teilnehmer.antworten !== undefined && teilnehmer.antworten.length === 3 * anzahl) {
          const m = index * 4 + j;
          const b = teilnehmer.antworten[m].buchstabe;
          if (b !== undefined && b !== null) {
            eingaben[j] = b;
          } else {
            eingaben[j] = '';
          }
        } else {
          eingaben[j] = '';
        }
      }
      this.matrixantwortzeilen[index] = {
        'eingaben': eingaben,
        'index': index
      };
    }
    this.antwortbereichExpanded = true;
  }

  private _initCheckboxAntwortzeilen(teilnehmer: Teilnehmer): void {
    if (teilnehmer === null || teilnehmer === undefined) {
      return;
    }
    if (this.checkboxAntwortzeilen === undefined || this.checkboxAntwortzeilen === null || this.checkboxAntwortzeilen.length === 0) {
      this.checkboxAntwortzeilen = createInitialCheckboxAntwortzeilen(this.selectedKlassenstufe);
    }

    const antw = teilnehmer.antworten;
    if (antw === null || antw === undefined || antw.length === 0 || teilnehmer.eingabemodus === EINGABEMODUS_SCHNELL) {
      return;
    }
    for (let i = 0; i < antw.length; i++) {
      const b = antw[i].buchstabe;
      switch (b) {
        case 'A': this.checkboxAntwortzeilen[i].zellen[0].check();
          break;
        case 'B': this.checkboxAntwortzeilen[i].zellen[1].check();
          break;
        case 'C': this.checkboxAntwortzeilen[i].zellen[2].check();
          break;
        case 'D': this.checkboxAntwortzeilen[i].zellen[3].check();
          break;
        case 'E': this.checkboxAntwortzeilen[i].zellen[4].check();
          break;
        default: this.logger.debug('unerwarteter Antwortbuchstabe ' + b);
          break;
      }
    }
    this.antwortbereichExpanded = true;
  }

  private _setSelectedSprache(sprache: Sprache): void {
//    this._formInitializing = true;
    this.selectedSprache = sprache;
    if (sprache) {
      this.selectedSpracheKuerzel = sprache.kuerzel;
    } else {
      this.selectedSpracheKuerzel = '';
    }
//    this._formInitializing = false;
  }

  private _setSelectedKlassenstufe(klassenstufe: Klassenstufe): void {
//    this._formInitializing = true;
    this.selectedKlassenstufe = klassenstufe;
    if (klassenstufe) {
      this.selectedKlassenstufeName = klassenstufe.name;
      this.klassenstufeFixed = true;
    } else {
      this.selectedKlassenstufeName = '';
    }
//    this._formInitializing = false;
  }
}