import { Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Farbschema, Auswertungsgruppe, SchuleUrkundenauftrag, getAuswertungsgruppenToPrint, DownloadCode } from '../../models/auswertung';
import { DOWNLOADART_URKUNDE } from '../../models/progress';
import { User, Teilnahme } from '../../models/user';

import { AuswertungService } from '../../services/auswertung.service';
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';

import { ActionFactory, SchuleUrkundenAction, CLEAR } from '../../state/actions';
import { DownloadcodeStore } from '../../state/store/downloadcode.store';
import { RootgruppeStore } from '../../state/store/rootgruppe.store';
import { SchuleurkundenauftragStore } from '../../state/store/schuleurkundenauftrag.store';
import { FarbschemaStore } from '../../state/store/farbschema.store';

import { Subscription ,  Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-schulurkunden',
  templateUrl: './schulurkunden.component.html'
})
export class SchulurkundenComponent implements OnInit, OnDestroy {

  urkundenForm: FormGroup;
  farbschema: AbstractControl;
  nurKaengurusprung: AbstractControl;

  selectedFarbschema: Farbschema;
  selectedFarbschemaName: string;
  btnSubmitDisabled: boolean;
  formSubmitAttempt: boolean;

  farben: Farbschema[];
  rootgruppe: Auswertungsgruppe;

  urkundenauftrag: SchuleUrkundenauftrag;
  downloadCode: string;
  showDownloadLink: boolean;
  processing: boolean;
  klassen: Auswertungsgruppe[];

  showChkNurKaenguru: boolean;


  private _subscription: Subscription;
  private _user$: Observable<User>;
  private _urkundenauftrag$: Observable<SchuleUrkundenauftrag>;
  private _downloadCode$: Observable<DownloadCode>;
  private _rootgruppe$: Observable<Auswertungsgruppe>;
  private _farbschemas$: Observable<Farbschema[]>;

  constructor(private fb: FormBuilder
    , r: ActivatedRoute
    , private router: Router
    , private farschemaStore: FarbschemaStore
    , private messageService: MessageService
    , private auswertungService: AuswertungService
    , private rootgruppeStore: RootgruppeStore
    , private urkundenauftragStore: SchuleurkundenauftragStore
    , private downloadcodeStore: DownloadcodeStore
    , private userService: UserService
    , private logger: LogService) {

    this.urkundenForm = fb.group({
      'farbschema': ['', [Validators.required]],
      'nurKaengurusprung': [false]
    });

    this.farbschema = this.urkundenForm.controls['farbschema'];
    this.nurKaengurusprung = this.urkundenForm.controls['nurKaengurusprung'];

    this._subscription = new Subscription();
    this._urkundenauftrag$ = this.urkundenauftragStore.stateSubject.asObservable();
    this._downloadCode$ = this.downloadcodeStore.stateSubject.asObservable();
    this._rootgruppe$ = this.rootgruppeStore.stateSubject.asObservable();
    this._farbschemas$ = this.farschemaStore.stateSubject.asObservable();
    this.farben = [];

  }

  ngOnInit() {

    this.messageService.clearMessage();
    this.showDownloadLink = false;
    this.processing = false;

    const farbschemasSubscription = this._farbschemas$.subscribe((schemas: Farbschema[]) => {
      for (let i = 0; i < schemas.length; i++) {
        const fs = schemas[i];
        fs.imageData = 'data:image/png;base64,' + fs.thumbnail;
        this.farben.push(fs);
      }
    });

    const rootgruppeSubscription = this._rootgruppe$.subscribe(
      (g: Auswertungsgruppe) => {
        if (g !== undefined && g !== null) {
          this.logger.debug('rootgruppe gefunden: ' + g.name);
          this.rootgruppe = g;
          this.klassen = getAuswertungsgruppenToPrint(this.rootgruppe.auswertungsgruppen);
        }
      });

    const urkundenauftragSubscription = this._urkundenauftrag$
      .subscribe(
        (a: SchuleUrkundenauftrag) => {
          this.logger.debug('urkundenauftragSubscription');
          this.urkundenauftrag = a;
          this._checkAndNavigate(this.urkundenauftrag);
        });

    const downloadCodeSubscription = this._downloadCode$
      .subscribe(
        (code: DownloadCode) => {
          if (!code || code.downloadArt !== DOWNLOADART_URKUNDE) {
            return;
          }
          this.downloadCode = code.downloadCode;
          this.processing = false;
          if (this.downloadCode === null || this.downloadCode === undefined) {
            this.showDownloadLink = false;
          } else {
            this.showDownloadLink = this.downloadCode.length > 0;
          }
          this.logger.debug('showDownloadLink=' + this.showDownloadLink);
        });

    this._subscription.add(farbschemasSubscription);
    this._subscription.add(urkundenauftragSubscription);
    this._subscription.add(downloadCodeSubscription);

    this._checkAndNavigate(this.urkundenauftrag);

    let anzKinder = 0;
    for (let i = 0; i < this.klassen.length; i++) {
      const gr = this.klassen[i];
      if (gr && gr.anzahlKinder !== undefined) {
        anzKinder += gr.anzahlKinder;
      }
    }
    this.showChkNurKaenguru = anzKinder > 1;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this.urkundenauftragStore.dispatch(ActionFactory.schuleUrkundenAction(CLEAR, this.urkundenauftrag));
    this.downloadcodeStore.dispatch(ActionFactory.replaceStringAction(CLEAR, null));
    this.messageService.clearMessage();
  }

  onFarbeSelected() {
    this.logger.debug('farbe: ' + this.selectedFarbschemaName);
    const value = this.farben.find(ks => ks.name === this.selectedFarbschemaName);
    if (value) {
      this.selectedFarbschema = value;
    } else {
      this.selectedFarbschema = null;
    }
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    this.urkundenauftrag.farbschemaName = this.selectedFarbschema.name;
    const tIdentifier = this.userService.getIdentifierAktuelleTeilnahme();
    const kaengurusprung = value.nurKaengurusprung;
    if (tIdentifier) {
      this.processing = true;
      if (kaengurusprung) {
        this.auswertungService.druckeSchulurkundenKaengurusprung(tIdentifier, this.urkundenauftrag);
      } else {
        this.auswertungService.druckeSchulurkunden(tIdentifier, this.urkundenauftrag);
      }
    }
  }

  cancel(): void {
    this.router.navigate(['/auswertungsgruppen']);
  }

  downloadResult(): void {
    if (this.showDownloadLink) {
      const tIdentifier = this.userService.getIdentifierAktuelleTeilnahme();
      if (tIdentifier) {
        this.auswertungService.saveFile(tIdentifier, this.downloadCode);
        this.showDownloadLink = false;
        this.messageService.clearMessage();
      }
    }
  }

  isSubmitDisabled(): boolean {
    if (this.btnSubmitDisabled || !this.urkundenForm.valid) {
      return true;
    }
    if (this.processing) {
      return true;
    }
    if (this.urkundenauftrag === null || this.urkundenauftrag === undefined) {
      return true;
    }
    if (this.userService.getIdentifierAktuelleTeilnahme() === null) {
      this.logger.warn('aktuelle Teilnahme nicht korrekt initialisiert.');
      return false;
    }
    return false;
  }

  private _checkAndNavigate(auftrag: SchuleUrkundenauftrag): void {
    if (auftrag === undefined || auftrag === null) {
      this.logger.debug('kein Urkundenauftrag, also navigate');
      this.router.navigate(['/auswertungsgruppen']);
    }
  }

}
