import { Component, Input, OnInit } from '@angular/core';
import { Matrixantwortzeile, Klassenstufe, Teilnehmer } from '../../models/auswertung';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-matrixantwortzeile',
  templateUrl: './matrixantwortzeile.component.html'
})
export class MatrixantwortzeileComponent implements OnInit {

  @Input() zeile: Matrixantwortzeile;

  style: string;

  constructor(private logger: LogService) { }

  ngOnInit() {
    this.logger.debug('zeile: ' + JSON.stringify(this.zeile));

    const anzahl = this.zeile.eingaben.length;

    if (anzahl === 4) {
      this.style = 'col-xs-4 col-sm-4 col-md-4';
    }
    if (anzahl === 5) {
      this.style = 'col-xs-5 col-sm-5 col-md-5';
    }
  }
}
