import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserStore } from '../../state/store/user.store';
import { GlobalSettingsStore } from '../../state/store/globalSettings.store';
import { GlobalSettings } from '../../models/serverResponse';
import { FarbschemaStore } from '../../state/store/farbschema.store';
import { Wettbewerbsanmeldung } from '../../models/clientPayload';
import { Schule } from '../../models/kataloge';
import { User } from '../../models/user';
import { OpportunityStateMap, OpportunityState } from '../../models/opportunityState';
import { AnmeldungService } from '../../services/anmeldung.service';
import { AuswertungService } from '../../services/auswertung.service';
import { UserService } from '../../services/user.service';
import { UserDelegate } from '../../shared/user.delegate';
import { Subscription, Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-dashboard',
  templateUrl: './dashboard.component.html',
})

export class DashboardComponent implements OnInit, OnDestroy {

  private _subscription: Subscription;
  private user$: Observable<User>;

  private user: User;
  schule: Schule;
  globalSettings: GlobalSettings;


  anmeldenState: OpportunityState;
  teilnahmenState: OpportunityState;
  downloadState: OpportunityState;
  auswertungState: OpportunityState;
  uploadExcelState: OpportunityState;
  profilState: OpportunityState;

  private opportunityStateMap: OpportunityStateMap;

  private settings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService

  constructor(private router: Router
    , private userStore: UserStore
    , private userService: UserService
    , private userDelegate: UserDelegate
    , private globalSettingsStore: GlobalSettingsStore
    , private teilnahmeService: AnmeldungService
    , private auswertungService: AuswertungService
    , private farbschemaStore: FarbschemaStore
    , private logger: LogService) {

    this._subscription = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this.opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
  }

  ngOnInit() {

    const opportunityStateSubscription = this.opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.teilnahmenState = map.TEILNAHMEN;
        this.downloadState = map.DOWNLOAD;
        this.auswertungState = map.AUSWERTUNG;
        this.uploadExcelState = map.EXCELUPLOAD;
        this.profilState = map.PROFIL;
      });

    const userSubscription = this.user$.subscribe((u: User) => {
      if (u) {
        this.user = new User(u);
        this.schule = this.user && this.user.erweiterteKontodaten && this.user.erweiterteKontodaten.schule || null;
      }
    });

    const settingsSubscription = this.settings$.subscribe((settings: GlobalSettings) => {
      if (settings) {
        this.globalSettings = settings;
      }
    });

    this._subscription.add(opportunityStateSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(settingsSubscription);

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  submitAnmeldung(): void {
    const schulkuerzel = this.user.erweiterteKontodaten.schule ? this.user.erweiterteKontodaten.schule.kuerzel : null;
    const payload = {
      'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this.user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this.user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel
    } as Wettbewerbsanmeldung;
    this.teilnahmeService.anmelden(payload);
  }

  showBtnAdVAbschliessen(): boolean {
    if (!this.user) {
      return false;
    }
    if (this.userDelegate.isPrivat(this.user)) {
      return false;
    }
    return !this.user.erweiterteKontodaten.advVereinbarungVorhanden;
  }

  showBtnAdVDownload(): boolean {
    if (!this.user) {
      return false;
    }
    if (this.userDelegate.isPrivat(this.user)) {
      return false;
    }
    return this.user.erweiterteKontodaten.advVereinbarungVorhanden;
  }

  gotoTeilnahmen() {
    this.logger.debug('jetzt zu Teilnahmen wechseln');
    this.auswertungService.loadTeilnahmen();
    this.router.navigate(['/teilnahmen']);
  }

  gotoProfil() {
    this.router.navigate(['/profil']);
  }

  gotoDownload() {
    if (this.farbschemaStore.getState().length === 0) {
      this.auswertungService.loadFarbschemas();
    }

    this.router.navigate(['/download']);
  }

  gotoUpload() {
    this.router.navigate(['/upload']);
  }

  gotoAuswertung() {
    if (this.userDelegate.isLehrer(this.user)) {
      this.router.navigate(['/auswertungsgruppen']);
    } else {
      this.router.navigate(['/teilnehmer']);
    }
  }

  gotoAuftragsvereinbarung(): void {
    if (this.userDelegate.isPrivat(this.user)) {
      return;
    }
    this.router.navigate(['/adv']);
  }

  anzahlTeilnahmen(): number {
    if (this.user.erweiterteKontodaten) {
      return this.user.erweiterteKontodaten.anzahlTeilnahmen;
    }
    return 0;
  }
}

