import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { GlobalSettingsStore } from '../../state/store/globalSettings.store';
import { AuthenticationTokenStore } from '../../state/store/authenticationToken.store';
import { ActionFactory, DESELECT } from '../../state/actions';
import { LAND, ORT, SCHULE } from '../../models/kataloge';
import { KatalogItemStore } from '../../state/store/katalogItem.store';
import { GlobalSettings } from '../../models/serverResponse';
import { SessionToken } from '../../models/serverResponse';
import { Subscription, Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';


@Component({
  templateUrl: './landig.component.html',
  providers: []
})
export class LandingComponent implements OnInit, OnDestroy {

  title: string;
  jahr: string;
  startAnmeldungText: string;
  originalTitle: string;

  private subscriptions: Subscription;


  /**
   * private Observable Streams (Referenz auf PingStore)
   */
  private globalSettings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private authenticationToken$: Observable<SessionToken>;

  constructor(r: ActivatedRoute
    , private router: Router
    , private titleService: Title
    , private settingsStore: GlobalSettingsStore
    , private authTokenStore: AuthenticationTokenStore
    , private katalogItemStore: KatalogItemStore
    , private logger: LogService) {

    this.title = '';
    this.jahr = '';
    this.subscriptions = new Subscription();

    /**
	   * neue Observables, auf die Subjects im Store
	   */
    this.globalSettings$ = this.settingsStore.stateSubject.asObservable();
    this.authenticationToken$ = this.authTokenStore.stateSubject.asObservable();
  }


  ngOnInit() {
    this.originalTitle = this.titleService.getTitle();

    if (this.title) {
      this.titleService.setTitle(this.title);
    }
    this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, SCHULE, null));
    this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, ORT, null));
    this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, LAND, null));

    /**
     * subscribe to the kontext Subject of the Store
     */
    const globalSettingsSubscription = this.globalSettings$
      .subscribe(
        (settings: GlobalSettings) => {
          this.jahr = settings.wettbewerbsjahr;
        });

    const authSubscription = this.authenticationToken$
      .subscribe(
        (authToken: SessionToken) => {
          if (authToken.accessToken) {
            this.router.navigate(['/teilnahmen']);
          }
        });


    this.subscriptions.add(globalSettingsSubscription);
    this.subscriptions.add(authSubscription);
  }

  ngOnDestroy() {
    this.titleService.setTitle(this.originalTitle);
    this.subscriptions.unsubscribe();
    this.logger.debug('LandingComponent destroyed');
  }

  gotoLogin() {
    this.router.navigate(['/login']);
  }

  gotoChangeTempPwd() {
    this.router.navigate(['/passwort/temp/change']);
  }

  gotoRegisterPrivat() {
    this.router.navigate(['/registrierungen/privat']);
  }

  gotoRegisterLehrer() {
    this.router.navigate(['/registrierungen/lehrer']);
  }
}
