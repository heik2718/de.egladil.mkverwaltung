import { Component, Input } from '@angular/core';
import { DateiInfo } from '../../models/serverResponse';

@Component({
  selector: 'mkv-datei',
  templateUrl: './dateiInfo.component.html'
})
export class DateiInfoComponent {

  @Input() dateiInfo: DateiInfo;

  constructor() { }
}

