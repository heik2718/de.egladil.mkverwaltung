import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {OpportunityStateMap, OpportunityState, ANMELDEN, DOWNLOAD} from '../../models/opportunityState';
import {User} from '../../models/user';
import {Farbschema} from '../../models/auswertung';
import {FarbschemaStore} from '../../state/store/farbschema.store';
import {UserDelegate} from '../../shared/user.delegate';
import {UserStore} from '../../state/store/user.store';
import {DateiInfo} from '../../models/serverResponse';
import {DateiInfoStore} from '../../state/store/dateiInfo.store';
import {DownloadService} from '../../services/download.service';
import {GlobalSettingsStore} from '../../state/store/globalSettings.store';
import {GlobalSettings} from '../../models/serverResponse';
import {Wettbewerbsanmeldung} from '../../models/clientPayload';
import {AnmeldungService} from '../../services/anmeldung.service';
import {UserService} from '../../services/user.service';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './download.component.html',
})

export class DownloadComponent implements OnInit, OnDestroy {

  private isLehrer: boolean;
  private dateien: DateiInfo[];
  private user: User;
  globalSettings: GlobalSettings;

  anmeldenState: OpportunityState;
  downloadState: OpportunityState;
  farbschema: Farbschema[];

  private opportunityStateMap: OpportunityStateMap;

  private subscriptions: Subscription;
  private user$: Observable<User>;
  dateiInfos$: Observable<DateiInfo[]>;
  private settings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService
  private farbschemas$: Observable<Farbschema[]>;



  constructor(r: ActivatedRoute
    , private router: Router
    , private farbschemaStore: FarbschemaStore
    , private userService: UserService
    , private userStore: UserStore
    , private userDelegate: UserDelegate
    , private dateiInfoStore: DateiInfoStore
    , private downloadService: DownloadService
    , private globalSettingsStore: GlobalSettingsStore
    , private teilnahmeService: AnmeldungService
    , private logger: LogService) {

    this.subscriptions = new Subscription();

    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.dateiInfos$ = this.dateiInfoStore.stateSubject.asObservable();
    this.opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this.farbschemas$ = this.farbschemaStore.stateSubject.asObservable();
  }

  ngOnInit() {

    const opportunityStateSubscription = this.opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.downloadState = map.DOWNLOAD;
        if (!this.downloadState.disabled) {
          this.downloadService.getFileList();
        }
      });

    const settingsSubscription = this.settings$.subscribe((settings: GlobalSettings) => {
      if (settings) {
        this.globalSettings = settings;
      }
    });

    const farbschemasSubscription = this.farbschemas$.subscribe((schemas: Farbschema[]) => {
      this.farbschema = schemas;
      for (let i = 0; i < this.farbschema.length; i++) {
        const fs = this.farbschema[i];
        fs.imageData = 'data:image/png;base64,' + fs.thumbnail;
      }
    });

    const userSubscription = this.user$
      .subscribe(
      (u: User) => {
        this.isLehrer = this.userDelegate.isLehrer(u);
        this.user = u;
      });

    const dateiInfosSubscription = this.dateiInfos$
      .subscribe(
      (infos: DateiInfo[]) => {
        if (infos) {
          this.dateien = infos;
        }
      });

    this.subscriptions.add(opportunityStateSubscription);
    this.subscriptions.add(userSubscription);
    this.subscriptions.add(settingsSubscription);
    this.subscriptions.add(dateiInfosSubscription);
    this.subscriptions.add(farbschemasSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  download(index: number): void {
    const dateiname = this.dateien[index].dateiname;
    this.logger.debug('jetzt ' + dateiname + ' runterladen');
    this.downloadService.saveFile(dateiname);
  }

  submitAnmeldung(): void {
    const schulkuerzel = this.user.erweiterteKontodaten && this.user.erweiterteKontodaten.schule && this.user.erweiterteKontodaten.schule.kuerzel || null;
    const payload = {
      'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this.user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this.user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel
    } as Wettbewerbsanmeldung;
    this.teilnahmeService.anmelden(payload);
  }
}

