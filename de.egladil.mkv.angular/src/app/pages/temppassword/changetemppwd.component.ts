import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {mkvEmailValidator, mkvEinmalPasswordValidator, mkvPasswordValidator, mkvPasswortPasswortNeuValidator, validateAllFormFields} from '../../validators/app.validators';
import {APIMessage, INFO, WARN, ERROR} from '../../models/serverResponse';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {MessageService} from '../../services/message.service';
import {TempPasswordService} from '../../services/temppwd.service';
import {TempPwdAendernCredentials} from '../../models/clientPayload';
import {AppConstants} from '../../core/app.constants';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-changetemppwd-form',
  templateUrl: './changetemppwd.component.html',
})

export class ChangeTempPwdComponent implements OnInit, OnDestroy {

  changeTempPwdForm: FormGroup;
  email: AbstractControl;
  passwort: AbstractControl;
  passwortNeu: AbstractControl;
  passwortNeuWdh: AbstractControl;
  kleber: AbstractControl;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;
  infoEmailExpanded: boolean;
  tooltipMailadresse: string;
  infoTempPwdExpanded: boolean;
  tooltipTempPwd: string;

  formSubmitAttempt: boolean;
  btnSubmitDisabled: boolean;

  private subscriptions: Subscription;
  private message$: Observable<APIMessage>;


  constructor(fb: FormBuilder
    , r: ActivatedRoute
    , private router: Router
    , private messageService: MessageService
    , private temppwdService: TempPasswordService
    , private messageStore: ApiMessageStore
    , private logger: LogService) {
    this.changeTempPwdForm = fb.group({
      'email': ['', [Validators.required, mkvEmailValidator]],
      'passwort': ['', [Validators.required, mkvEinmalPasswordValidator]],
      'passwortNeu': ['', [Validators.required, mkvPasswordValidator]],
      'passwortNeuWdh': ['', [Validators.required, mkvPasswordValidator]],
      'kleber': ['']
    }, {validator: mkvPasswortPasswortNeuValidator});

    this.email = this.changeTempPwdForm.controls['email'];
    this.passwort = this.changeTempPwdForm.controls['passwort'];
    this.passwortNeu = this.changeTempPwdForm.controls['passwortNeu'];
    this.passwortNeuWdh = this.changeTempPwdForm.controls['passwortNeuWdh'];
    this.kleber = this.changeTempPwdForm.controls['kleber'];

    this.infoEmailExpanded = false;
    this.infoPasswordExpanded = false;
    this.infoTempPwdExpanded = false;

    this.tooltipMailadresse = AppConstants.tooltips.MAILADRESSE_REGISTRIERT;
    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;
    this.tooltipTempPwd = AppConstants.tooltips.TEMPPWD;

    this.subscriptions = new Subscription();
    this.message$ = this.messageStore.stateSubject.asObservable();
  }

  ngOnInit() {
    this.formSubmitAttempt = false;
    this.btnSubmitDisabled = false;

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
        this.btnSubmitDisabled = false;
      }
      if (apiMsg.level === INFO) {
        this.btnSubmitDisabled = true;
      }
    });

    this.subscriptions.add(messageSubscription);
  }

  ngOnDestroy() {
    this.logger.debug('ChangeTempPwdComponent destroyed');
    this.messageService.clearMessage();
    this.subscriptions.unsubscribe();
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    if (this.changeTempPwdForm.valid) {
      const credentials = Object.assign({}, value) as TempPwdAendernCredentials;
      credentials.type = 'changeTempPwd';
      this.temppwdService.changePassword(credentials);
    } else {
      validateAllFormFields(this.changeTempPwdForm);
    }
  }

  togglePwdInfo = function() {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  toggleTempPwdInfo = function() {
    this.infoTempPwdExpanded = !this.infoTempPwdExpanded;
  };

  toggleEmailInfo = function() {
    this.infoEmailExpanded = !this.infoEmailExpanded;
  };


  cancel() {
    this.router.navigate(['/landing']);
  }
}