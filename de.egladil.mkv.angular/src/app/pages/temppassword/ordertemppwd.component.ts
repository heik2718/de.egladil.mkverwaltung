import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';
import { mkvEmailValidator, validateAllFormFields } from '../../validators/app.validators';
import { APIMessage, INFO, WARN, ERROR } from '../../models/serverResponse';
import { ApiMessageStore } from '../../state/store/apiMessage.store';
import { MessageService } from '../../services/message.service';
import { TempPasswordService } from '../../services/temppwd.service';
import { OrderTempCredentials } from '../../models/clientPayload';
import { AppConstants } from '../../core/app.constants';
import { Subscription, Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-ordertemppwd-form',
  templateUrl: './ordertemppwd.component.html',
})

export class OrderTempPwdComponent implements OnInit, OnDestroy {

  orderTempPwdForm: FormGroup;
  email: AbstractControl;
  kleber: AbstractControl;

  infoEmailExpanded: boolean;
  tooltipMailadresse: string;

  formSubmitAttempt: boolean;
  btnSubmitDisabled: boolean;

  private subscriptions: Subscription;
  private message$: Observable<APIMessage>;

  constructor(fb: FormBuilder
    , r: ActivatedRoute
    , private router: Router
    , private messageService: MessageService
    , private temppwdService: TempPasswordService
    , private messageStore: ApiMessageStore
    , private logger: LogService) {
    this.orderTempPwdForm = fb.group({
      'email': ['', [Validators.required, mkvEmailValidator]],
      'kleber': ['']
    });

    this.email = this.orderTempPwdForm.controls['email'];
    this.kleber = this.orderTempPwdForm.controls['kleber'];

    this.infoEmailExpanded = false;
    this.tooltipMailadresse = AppConstants.tooltips.MAILADRESSE_REGISTRIERT;

    this.subscriptions = new Subscription();
    this.message$ = this.messageStore.stateSubject.asObservable();
  }

  ngOnInit() {
    this.formSubmitAttempt = false;

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
        this.btnSubmitDisabled = false;
      }
      if (apiMsg.level === INFO) {
        this.btnSubmitDisabled = true;
      }
    });

    this.subscriptions.add(messageSubscription);
  }

  ngOnDestroy() {
    this.logger.debug('OrderTempPwdComponent destroyed');
    this.messageService.clearMessage();
    this.subscriptions.unsubscribe();
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    if (this.orderTempPwdForm.valid) {
      const credentials = Object.assign({}, value) as OrderTempCredentials;
      this.temppwdService.orderPassword(credentials);
    } else {
      validateAllFormFields(this.orderTempPwdForm);
    }
  }

  toggleEmailInfo = function () {
    this.infoEmailExpanded = !this.infoEmailExpanded;
  };

  isSubmited = function (): boolean {
    return this.temppwdServiceCalled;
  }

  cancel() {
    this.router.navigate(['/landing']);
  }

}