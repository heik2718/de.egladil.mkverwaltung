import { Injectable } from '@angular/core';
import { FileUploadConfig, FileVericationResult } from 'app/models/files';

@Injectable({
  providedIn: 'root'
})
export class UploadVerificationService {

  constructor() { }


  verifyUpload(files: File[], fileUploadConfig: FileUploadConfig): FileVericationResult {

    let fileCount = 0;
    let totalFileSize = 0;

    files.forEach(file => {

      fileCount++;
      totalFileSize += file.size;

      if (fileUploadConfig.maxEachFileSize && file.size / 1024 > fileUploadConfig.maxEachFileSize) {
        return <FileVericationResult>{
          verificationSuccess: false,
          verificationStatus: 'MAX_EACH_FILE_SIZE_EXCEED'
        };
      }

    });

    if (fileCount > fileUploadConfig.maxFileCount) {
      return <FileVericationResult>{
          verificationSuccess: false,
          verificationStatus: 'MAX_FILES_COUNT_EXCEED'
        };
    }

    if (totalFileSize > fileUploadConfig.maxTotalFileSize) {

      return <FileVericationResult>{
          verificationSuccess: false,
          verificationStatus: 'MAX_TOTAL_FILE_SIZE_EXCEED'
        };
    }

    return <FileVericationResult>{
      verificationSuccess: true,
      verificationStatus: 'VERIFIED'
    };
  }
}
