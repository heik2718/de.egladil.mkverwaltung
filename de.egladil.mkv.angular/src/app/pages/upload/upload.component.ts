import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants } from '../../core/app.constants';

import { Teilnehmer } from '../../models/auswertung';
import { APIMessage } from '../../models/serverResponse';
import { GlobalSettings } from '../../models/serverResponse';
import { User, Teilnahme } from '../../models/user';
import { Wettbewerbsanmeldung } from '../../models/clientPayload';

import { AktuelleTeilnahmeStore } from '../../state/store/aktuelleTeilnahme.store';
import { ApiMessageStore } from '../../state/store/apiMessage.store';
import { GlobalSettingsStore } from '../../state/store/globalSettings.store';
import { OpportunityStateMap, OpportunityState } from '../../models/opportunityState';
import { TeilnehmerArrayStore } from '../../state/store/teilnehmerArray.store';
import { UserStore } from '../../state/store/user.store';

import { AnmeldungService } from '../../services/anmeldung.service';
import { MessageService } from '../../services/message.service';
import { UploadService } from '../../services/upload.service';
import { UserDelegate } from '../../shared/user.delegate';
import { UserService } from '../../services/user.service';

import { Subscription, Observable, forkJoin } from 'rxjs';
import { UploadVerificationService } from './upload-verification.service';
import { FileUploadConfig, FileVericationResult } from 'app/models/files';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './upload.component.html',
})

export class UploadComponent implements OnInit, OnDestroy {


  @ViewChild('file', { static: false }) file;

  // public files: Set<File> = new Set();

  public selectedFile: File;

  progress: any;
  hasError: boolean;
  errorMsg: string;
  uploading: boolean;
  globalSettings: GlobalSettings;
  private user: User;
  aktuelleTeilnahme: Teilnahme;
  anzahlTeilnehmer: number;

  anmeldenState: OpportunityState;
  auswertungState: OpportunityState;
  excelUploadState: OpportunityState;

  uploadDisabledText: string;
  uploadSuccessful = false;


  private _subscription: Subscription;
  private settings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService
  private _alleTeilnehmer$: Observable<Teilnehmer[]>;
  private _aktuelleTeilnahme$: Observable<Teilnahme>;
  private user$: Observable<User>;
  private message$: Observable<APIMessage>;

  private filesConfig: FileUploadConfig = {
    maxFileCount: 4,
    maxEachFileSize: 614400,
    maxTotalFileSize: 2457600
  };


  constructor(private userStore: UserStore
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private userService: UserService
    , private userDelegate: UserDelegate
    , private uploadService: UploadService
    , private messageStore: ApiMessageStore
    , private globalSettingsStore: GlobalSettingsStore
    , private teilnahmeService: AnmeldungService
    , private teilnehmerArrayStore: TeilnehmerArrayStore
    , private messageService: MessageService
    , private uploadVerificationService: UploadVerificationService
    , private router: Router
    , private logger: LogService) {

    this._subscription = new Subscription();

    this._aktuelleTeilnahme$ = this.aktuelleTeilnahmeStore.stateSubject.asObservable();
    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.message$ = this.messageStore.stateSubject.asObservable();
    this.opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();
    this._alleTeilnehmer$ = this.teilnehmerArrayStore.items.asObservable();

  }

  ngOnInit() {
    this.hasError = false;
    this.uploading = false;
    this.anzahlTeilnehmer = 0;

    const opportunityStateSubscription = this.opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.auswertungState = map.AUSWERTUNG;
        this.excelUploadState = map.EXCELUPLOAD;
        this.uploadDisabledText = this.excelUploadState.text;
        this.logger.debug('update opportunity state');
      });

    const settingsSubscription = this.settings$.subscribe((settings: GlobalSettings) => {
      if (settings) {
        this.globalSettings = settings;
      }
    });

    const teilnahmeSubscription = this._aktuelleTeilnahme$.subscribe(
      (t: Teilnahme) => {
        this.aktuelleTeilnahme = t;
      });

    const userSubscription = this.user$.subscribe((u: User) => {
      if (u) {
        this.user = u;
      }
    });

    const messageSubscription = this.message$.subscribe(() => {
      this.uploading = false;
    });

    const teilnehmerArraySubscription = this._alleTeilnehmer$.subscribe(
      (theTeilnehmer: Teilnehmer[]) => {
        this.anzahlTeilnehmer = theTeilnehmer.length;
        this.uploadDisabledText = AppConstants.texte.KEIN_UPLOAD_WEGEN_URKUNDEN;
      });

    this._subscription.add(opportunityStateSubscription);
    this._subscription.add(settingsSubscription);
    this._subscription.add(messageSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(teilnahmeSubscription);
    this._subscription.add(teilnehmerArraySubscription);

    this.messageService.clearMessage();

  }

  onFilesAdded() {

    const files: { [key: string]: File } = this.file.nativeElement.files;

    const fileArray: File[] = [];

    for (const key in files) {
      if (!isNaN(parseInt(key, 0))) {
        fileArray.push(files[key]);
      }
    }

    const verificationResult: FileVericationResult = this.uploadVerificationService.verifyUpload(fileArray, this.filesConfig)

    if (!verificationResult.verificationSuccess) {

      this.hasError = true;
      switch (verificationResult.verificationStatus) {
        case 'MAX_FILES_COUNT_EXCEED':
          this.errorMsg = 'Bitte höchstens ' + this.filesConfig.maxFileCount + ' Dateien auswählen';
          break;
        case 'MAX_EACH_FILE_SIZE_EXCEED':
          this.errorMsg = 'Bitte keine Datei auswählen, die größer als ' + (this.filesConfig.maxEachFileSize / 1024) + ' Kilobyte groß ist';
          break;
        case 'MAX_TOTAL_FILE_SIZE_EXCEED':
          this.errorMsg = 'Die gewählten Dateien übersteigen die erlaubte Maximalgröße von ' + (this.filesConfig.maxTotalFileSize / 1024) + ' Kilobyte';
          break;
      }

      return;
    }

    // fileArray.forEach(file => {
    //   this.files.add(file);
    // })
    this.selectedFile = fileArray[0];
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  addFiles() {
    this.file.nativeElement.click();
  }


  uploadFiles() {

    if (this.uploadSuccessful) {
      return;
    }

    this.uploading = true;

    const promise = this.uploadService.uploadFile(this.selectedFile);

    promise.then(
      data => {
        if (data !== undefined) {
          this.messageService.handleMessage(data);
        }
        this.uploading = false;
        // this.files.clear();
        this.selectedFile = undefined;
      }
    ).catch(
      data => {
        if (data !== undefined) {
          this.messageService.handleMessage(data);
        }
        // this.files.clear();
        this.selectedFile = undefined;
      }
    );

  }

  submitAnmeldung(): void {
    const schulkuerzel = this.user.erweiterteKontodaten && this.user.erweiterteKontodaten.schule && this.user.erweiterteKontodaten.schule.kuerzel || null;
    const payload = {
      'jahr': this.globalSettings.wettbewerbsjahr
      , 'benachrichtigen': this.user.erweiterteKontodaten.mailbenachrichtigung
      , 'rolle': this.user.basisdaten.rolle
      , 'schulkuerzel': schulkuerzel
    } as Wettbewerbsanmeldung;
    this.teilnahmeService.anmelden(payload);
  }

  gotoAuswertung() {
    if (this.userDelegate.isLehrer(this.user)) {
      this.router.navigate(['/auswertungsgruppen']);
    } else {
      this.router.navigate(['/teilnehmer']);
    }
  }

  cancel() {
    this.router.navigate(['/dashboard']);
  }
}

