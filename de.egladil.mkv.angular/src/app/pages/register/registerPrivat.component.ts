import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { Privatregistrierung, REGISTRIERUNG_PRIVAT } from '../../models/clientPayload';
import { APIMessage, WARN, ERROR } from '../../models/serverResponse';
import { mkvEmailValidator, mkvPasswordValidator, mkvPasswortPasswortWiederholtValidator } from '../../validators/app.validators';
import { MessageService } from '../../services/message.service';
import { ApiMessageStore } from '../../state/store/apiMessage.store';
import { RegistrationService } from '../../services/registration.service';
import { GlobalSettings } from '../../models/serverResponse';
import { GlobalSettingsStore } from '../../state/store/globalSettings.store';
import { AppConstants } from '../../core/app.constants';
import { Subscription, Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './registerPrivat.component.html',
})

export class RegisterPrivatComponent implements OnInit, OnDestroy {

  registerPForm: FormGroup;
  vorname: AbstractControl;
  nachname: AbstractControl;
  email: AbstractControl;
  passwort: AbstractControl;
  passwortWdh: AbstractControl;
  gleichAnmelden: AbstractControl;
  automatischBenachrichtigen: AbstractControl;
  agbGelesen: AbstractControl;
  kleber: AbstractControl;

  private requestPayload: Privatregistrierung;
  private _subscription: Subscription;

  infoEmailExpanded: boolean;
  tooltipEmail: string;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;

  private submitDisabled: boolean;

  wettbewerbsjahr: string;
  neuanmeldungFreigegeben: boolean;

  textChkNewsletter: string;

  private settings$: Observable<GlobalSettings>;
  private registrierungCache$: Observable<Privatregistrierung>;
  private message$: Observable<APIMessage>;

  constructor(private fb: FormBuilder
    , private router: Router
    , private messageService: MessageService
    , private registrationService: RegistrationService
    , private globalSettingsStore: GlobalSettingsStore
    , private messageStore: ApiMessageStore
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {
    this.registerPForm = fb.group({
      'vorname': ['', [Validators.required, Validators.maxLength(100)]],
      'nachname': ['', [Validators.required, Validators.maxLength(100)]],
      'email': ['', [Validators.required, mkvEmailValidator]],
      'passwort': ['', [Validators.required, mkvPasswordValidator]],
      'passwortWdh': ['', [Validators.required, mkvPasswordValidator]],
      'gleichAnmelden': [true, []],
      'automatischBenachrichtigen': [false, []],
      'agbGelesen': [false, [Validators.requiredTrue]],
      'kleber': ['']
    }, { validator: mkvPasswortPasswortWiederholtValidator });

    this.vorname = this.registerPForm.controls['vorname'];
    this.nachname = this.registerPForm.controls['nachname'];
    this.email = this.registerPForm.controls['email'];
    this.passwort = this.registerPForm.controls['passwort'];
    this.passwortWdh = this.registerPForm.controls['passwortWdh'];
    this.gleichAnmelden = this.registerPForm.controls['gleichAnmelden'];
    this.automatischBenachrichtigen = this.registerPForm.controls['automatischBenachrichtigen'];
    this.agbGelesen = this.registerPForm.controls['agbGelesen'];
    this.kleber = this.registerPForm.controls['kleber'];

    this._subscription = new Subscription();
    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this.message$ = this.messageStore.stateSubject.asObservable();

    this.infoPasswordExpanded = false;
    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;
    this.infoEmailExpanded = false;
    this.tooltipEmail = AppConstants.tooltips.MAILADRESSE_REGISTRIERUNG;
    this.textChkNewsletter = AppConstants.buttontexte.CHK_NEWSLETTER;

    this.wettbewerbsjahr = '';
  }

  ngOnInit() {
    this.submitDisabled = false;

    const settingsSubscription = this.settings$
      .subscribe(
        (globalSettings: GlobalSettings) => {
          this.wettbewerbsjahr = globalSettings.wettbewerbsjahr;
          this.neuanmeldungFreigegeben = globalSettings.neuanmeldungFreigegeben;
        });

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.submitDisabled = false;
      }
    });

    this._subscription.add(settingsSubscription);
    this._subscription.add(messageSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this.logger.debug('LoginComponent destroyed');
    this.messageService.clearMessage();
  }

  onSubmit(value: any): void {
    this.submitDisabled = true;
    this.logger.debug('you submitted value:' + value);
    this.requestPayload = Object.assign({}, value) as Privatregistrierung;
    this.requestPayload.type = REGISTRIERUNG_PRIVAT;
    if (!this.neuanmeldungFreigegeben) {
      this.requestPayload.gleichAnmelden = false;
    }
    this.logger.debug('loginCredentials: ' + JSON.stringify(this.requestPayload));
    this.registrationService.registerPrivat(this.requestPayload);
  }

  toggleEmailInfo(): void {
    this.infoEmailExpanded = !this.infoEmailExpanded;
  };

  togglePwdInfo(): void {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  isSubmitDisabled(): boolean {
    return this.submitDisabled;
  }

  cancel(): void {
    this.router.navigate(['/landing']);
  }
}