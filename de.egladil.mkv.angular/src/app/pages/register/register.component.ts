import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  templateUrl: './register.component.html',
})

export class RegisterComponent {



  constructor(private router: Router) {
  }

  gotoRegisterLehrer() {
    this.router.navigate(['/registrierungen/lehrer']);
  }

  gotoRegisterPrivat(): void {
    this.router.navigate(['/registrierungen/privat']);
  }
}