import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import { DOCUMENT } from "@angular/common";

import {Lehrerregistrierung, REGISTRIERUNG_LEHRER} from '../../models/clientPayload';
import {KatalogItem, KatalogItemMap, LAND, ORT, SCHULE} from '../../models/kataloge';
import {APIMessage, WARN, ERROR, GlobalSettings} from '../../models/serverResponse';

import {mkvEmailValidator, mkvPasswordValidator, mkvPasswortPasswortWiederholtValidator} from '../../validators/app.validators';
import {AppConstants} from '../../core/app.constants';

import {MessageService} from '../../services/message.service';

import {ActionFactory, STATIC_KONTAKT, STATIC_AGB, CACHE, CLEAR_KATALOG, CLEAR, DESELECT, MKVAction} from '../../state/actions';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {GlobalSettingsStore} from '../../state/store/globalSettings.store';
import {KatalogStore} from '../../state/store/katalog.store';
import {KatalogItemStore} from '../../state/store/katalogItem.store';
import {StaticContentStore} from '../../state/store/staticContent.store';
import {RegistrationService} from '../../services/registration.service';

import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './registerLehrer.component.html',
})

export class RegisterLehrerComponent implements OnInit, OnDestroy {

  registerLForm: FormGroup;
  vorname: AbstractControl;
  nachname: AbstractControl;
  email: AbstractControl;
  passwort: AbstractControl;
  passwortWdh: AbstractControl;
  gleichAnmelden: AbstractControl;
  automatischBenachrichtigen: AbstractControl;
  agbGelesen: AbstractControl;
  kleber: AbstractControl;

  textChkNewsletter: string;

  selectedSchule: KatalogItem;


  private requestPayload: Lehrerregistrierung;
  private subscriptions: Subscription;

  infoEmailExpanded: boolean;
  tooltipEmail: string;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;

  infoNameKollegenExpanded: boolean;
  tooltipNameKollegen: string;

  private submitDisabled: boolean;

  wettbewerbsjahr: string;
  neuanmeldungFreigegeben: boolean;

  private settings$: Observable<GlobalSettings>;
  private registrierungCache$: Observable<Lehrerregistrierung>;
  private message$: Observable<APIMessage>;
  private katalogItems$: Observable<KatalogItemMap>;

  constructor(private fb: FormBuilder
    , private router: Router
    , private messageService: MessageService
    , private registrationService: RegistrationService
    , private globalSettingsStore: GlobalSettingsStore
    , private katalogStore: KatalogStore
    , private katalogItemStore: KatalogItemStore
    , private staticContentStore: StaticContentStore
    , private messageStore: ApiMessageStore
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {


    this.registerLForm = fb.group({
      'vorname': ['', [Validators.required, Validators.maxLength(100)]],
      'nachname': ['', [Validators.required, Validators.maxLength(100)]],
      'email': ['', [Validators.required, mkvEmailValidator]],
      'passwort': ['', [Validators.required, mkvPasswordValidator]],
      'passwortWdh': ['', [Validators.required, mkvPasswordValidator]],
      'gleichAnmelden': [true, []],
      'automatischBenachrichtigen': [false, []],
      'agbGelesen': [false, [Validators.requiredTrue]],
      'kleber': ['']
    }, {validator: mkvPasswortPasswortWiederholtValidator});

    this.vorname = this.registerLForm.controls['vorname'];
    this.nachname = this.registerLForm.controls['nachname'];
    this.email = this.registerLForm.controls['email'];
    this.passwort = this.registerLForm.controls['passwort'];
    this.passwortWdh = this.registerLForm.controls['passwortWdh'];
    this.gleichAnmelden = this.registerLForm.controls['gleichAnmelden'];
    this.automatischBenachrichtigen = this.registerLForm.controls['automatischBenachrichtigen'];
    this.agbGelesen = this.registerLForm.controls['agbGelesen'];
    this.kleber = this.registerLForm.controls['kleber'];

    this.subscriptions = new Subscription();
    this.settings$ = this.globalSettingsStore.stateSubject.asObservable();
    this.katalogItems$ = this.katalogItemStore.stateSubject.asObservable();
    this.message$ = this.messageStore.stateSubject.asObservable();

    this.infoPasswordExpanded = false;
    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;
    this.infoEmailExpanded = false;
    this.tooltipEmail = AppConstants.tooltips.MAILADRESSE_REGISTRIERUNG;
    this.infoNameKollegenExpanded = false;
    this.tooltipNameKollegen = AppConstants.tooltips.NAME_KOLLEGEN;
    this.textChkNewsletter = AppConstants.buttontexte.CHK_NEWSLETTER;

    this.wettbewerbsjahr = '';
  }

  ngOnInit() {
    this.submitDisabled = false;

    const settingsSubscription = this.settings$
      .subscribe(
      (globalSettings: GlobalSettings) => {
        this.wettbewerbsjahr = globalSettings.wettbewerbsjahr;
        this.neuanmeldungFreigegeben = globalSettings.neuanmeldungFreigegeben;
      });

    const katalogItemsSubscription = this.katalogItems$
      .subscribe((map: KatalogItemMap) => {
        if (map.land !== null && map.land.kuerzel === 'LEER') {
          this.submitDisabled = true;
        }
        if (map.ort !== null && map.ort.kuerzel === 'LEER') {
          this.submitDisabled = true;
        }
        this.selectedSchule = map.schule;
        if (this.selectedSchule !== null) {
          this.logger.debug('Schule ' + this.selectedSchule.kuerzel + ' selected');
          if (this.selectedSchule.kuerzel === 'LEER') {
            this.submitDisabled = true;
          } else {
            this.submitDisabled = false;
          }
        } else {
          this.submitDisabled = true;
        }
      });

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.submitDisabled = false;
      }
      this.document.body.scrollTop = 0;
    });

    this.subscriptions.add(settingsSubscription);
    this.subscriptions.add(katalogItemsSubscription);
    this.subscriptions.add(messageSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.logger.debug('RegisterLehrerComponent destroyed');
    this.messageService.clearMessage();
  }

  onSubmit(value: any): void {
    this.submitDisabled = true;
    this.logger.debug('you submitted value:' + value);
    this.requestPayload = Object.assign({}, value) as Lehrerregistrierung;
    this.requestPayload.type = REGISTRIERUNG_LEHRER;
    this.requestPayload.schulkuerzel = this.selectedSchule.kuerzel;
    if (!this.neuanmeldungFreigegeben) {
      this.requestPayload.gleichAnmelden = false;
    }
    this.logger.debug('loginCredentials: ' + JSON.stringify(this.requestPayload));
    this.registrationService.registerLehrer(this.requestPayload);
  }


  toggleEmailInfo(): void {
    this.infoEmailExpanded = !this.infoEmailExpanded;
  };

  togglePwdInfo(): void {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  toggleNameKollegenInfo(): void {
    this.infoNameKollegenExpanded = !this.infoNameKollegenExpanded;
  };

  isSubmitDisabled(): boolean {
    return this.submitDisabled;
  }

  cancel(): void {
    const action = {'type': CLEAR} as MKVAction;
    this.katalogItemStore.dispatch(action);
    this.katalogStore.dispatch(action);
    this.router.navigate(['/landing']);
  }
}