import {Component} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {ProfilService} from '../../services/profil.service';
import {ActivatedRoute, Router} from '@angular/router';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-konto-loeschen',
  templateUrl: './kontoLoeschen.component.html',
})

export class KontoLoeschenComponent {

  kontoLoeschenForm: FormGroup;
  einverstanden: AbstractControl;

  constructor(private fb: FormBuilder
    , r: ActivatedRoute
    , private profilService: ProfilService
    , private router: Router
    , private logger: LogService) {

    this.kontoLoeschenForm = fb.group({
      'einverstanden': [false, [Validators.requiredTrue]]
    });

    this.einverstanden = this.kontoLoeschenForm.controls['einverstanden'];

  }

  onSubmit(value: any): void {
    this.logger.debug('you submitted value:' + value);
    this.profilService.kontoLoeschen(value.einverstanden);
  }

  cancel() {
    this.router.navigate(['/profil']);
  }
}

