import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MKVAction, CLEAR } from '../../state/actions';
import { User } from '../../models/user';
import { UserDelegate } from '../../shared/user.delegate';
import { UserStore } from '../../state/store/user.store';
import { KatalogItemStore } from '../../state/store/katalogItem.store';
import { Subscription ,  Observable } from 'rxjs';

@Component({
  templateUrl: './profil.component.html',
})

export class ProfilComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;
  private user$: Observable<User>;
  private user: User;

  constructor(private router: Router
    , private userStore: UserStore
    , private userDelegate: UserDelegate
    , private katalogItemStore: KatalogItemStore) {

    this.subscriptions = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
  }

  ngOnInit() {
    const userSubscription = this.user$.subscribe((u: User) => {
      if (u) {
        this.user = new User(u);
      }
    });

    this.subscriptions.add(userSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  gotoSchulzuordnung() {
    const action = { 'type': CLEAR } as MKVAction;
    this.katalogItemStore.dispatch(action);
    this.router.navigate(['/profil/schulzuordnung']);
  }

  gotoKontoLoeschen() {
    this.router.navigate(['/profil/kontoloeschung']);
  }

  showSchulzuordnung(): boolean {

    if (this.userDelegate.isPrivat(this.user)) {
      return false;
    }
    return this.user.erweiterteKontodaten && this.user.erweiterteKontodaten.schulwechselMoeglich;
  }

  gotoNameAendern(): void {
    this.router.navigate(['/profil/name']);
  }

  gotoPasswortAendern(): void {
    this.router.navigate(['/profil/passwort']);
  }

  gotoEmailAendern(): void {
    this.router.navigate(['/profil/email']);
  }
}

