import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {mkvPasswordValidator, mkvPasswortPasswortNeuValidator, validateAllFormFields} from '../../validators/app.validators';
import {APIMessage, WARN, ERROR, INFO} from '../../models/serverResponse';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {MessageService} from '../../services/message.service';
import {ProfilService} from '../../services/profil.service';
import {User} from '../../models/user';
import {UserStore} from '../../state/store/user.store';
import {PasswortAendernPayload} from '../../models/clientPayload';
import {AppConstants} from '../../core/app.constants';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-profil-passwort',
  templateUrl: './profil.passwort.component.html',
})

export class ProfilPasswortComponent implements OnInit, OnDestroy {

  changePasswortForm: FormGroup;
  passwort: AbstractControl;
  passwortNeu: AbstractControl;
  passwortNeuWdh: AbstractControl;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;

  formSubmitAttempt: boolean;
  btnSubmitDisabled: boolean;

  private email: string;

  private subscriptions: Subscription;
  private message$: Observable<APIMessage>;
  private user$: Observable<User>;

  private payload: PasswortAendernPayload;


  constructor(fb: FormBuilder
    , r: ActivatedRoute
    , private router: Router
    , private messageService: MessageService
    , private profilService: ProfilService
    , private messageStore: ApiMessageStore
    , private userStore: UserStore
    , private logger: LogService) {
    this.changePasswortForm = fb.group({
      'passwort': ['', [Validators.required, mkvPasswordValidator]],
      'passwortNeu': ['', [Validators.required, mkvPasswordValidator]],
      'passwortNeuWdh': ['', [Validators.required, mkvPasswordValidator]]
    }, {validator: mkvPasswortPasswortNeuValidator});

    this.passwort = this.changePasswortForm.controls['passwort'];
    this.passwortNeu = this.changePasswortForm.controls['passwortNeu'];
    this.passwortNeuWdh = this.changePasswortForm.controls['passwortNeuWdh'];

    this.infoPasswordExpanded = false;

    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;

    this.subscriptions = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.message$ = this.messageStore.stateSubject.asObservable();
  }

  ngOnInit() {
    this.formSubmitAttempt = false;
    this.btnSubmitDisabled = false;

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
      }
      if (apiMsg.level === INFO) {
        this.btnSubmitDisabled = true;
      }
    });

    const userSubscription = this.user$
      .subscribe(
      (user: User) => {
        this.email = user.basisdaten.email;
        this.formSubmitAttempt = false;
      });

    this.subscriptions.add(userSubscription);

    this.subscriptions.add(messageSubscription);
  }

  ngOnDestroy() {
    this.logger.debug('ChangeTempPwdComponent destroyed');
    this.messageService.clearMessage();
    this.subscriptions.unsubscribe();
  }

  triggerErrorsInForm(): void {
    validateAllFormFields(this.changePasswortForm);
    mkvPasswortPasswortNeuValidator(this.changePasswortForm);
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    if (this.changePasswortForm.valid) {
      const payload = Object.assign({}, value) as PasswortAendernPayload;
      payload.type = 'changePwd';
      payload.email = this.email;
      this.profilService.passwortAendern(payload);
    } else {
      validateAllFormFields(this.changePasswortForm);
    }
  }

  togglePwdInfo = function() {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  cancel() {
    this.router.navigate(['/profil']);
  }
}