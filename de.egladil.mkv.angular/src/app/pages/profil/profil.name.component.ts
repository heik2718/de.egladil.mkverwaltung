import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {ProfilService} from '../../services/profil.service';
import {APIMessage, INFO, WARN, ERROR} from '../../models/serverResponse';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {MessageService} from '../../services/message.service';
import {ActionFactory} from '../../state/actions';
import {PersonAendernPayload} from '../../models/clientPayload';
import {User} from '../../models/user';
import {UserStore} from '../../state/store/user.store';
import {validateAllFormFields} from '../../validators/app.validators';
import {AppConstants} from '../../core/app.constants';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-profil-name',
  templateUrl: './profil.name.component.html',
})

export class ProfilNameComponent implements OnInit, OnDestroy {

  nameAendernForm: FormGroup;
  vorname: AbstractControl;
  nachname: AbstractControl;

  private _payload: PersonAendernPayload;
  private _subscription: Subscription;
  private _user: User;

  private user$: Observable<User>;
  private message$: Observable<APIMessage>;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;

  formSubmitAttempt: boolean;
  btnSubmitDisabled: boolean;


  constructor(private fb: FormBuilder
    , r: ActivatedRoute
    , private profilService: ProfilService
    , private messageService: MessageService
    , private userStore: UserStore
    , private messageStore: ApiMessageStore
    , private router: Router
    , private logger: LogService) {

    this.nameAendernForm = fb.group({
      'vorname': ['', [Validators.required, Validators.maxLength(100)]],
      'nachname': ['', [Validators.required, Validators.maxLength(100)]]
    });

    this.vorname = this.nameAendernForm.controls['vorname'];
    this.nachname = this.nameAendernForm.controls['nachname'];

    this._subscription = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.message$ = this.messageStore.stateSubject.asObservable();

    this.infoPasswordExpanded = false;
    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;
  }

  ngOnInit() {
    this.formSubmitAttempt = false;
    this.btnSubmitDisabled = false;

    const userSubscription = this.user$
      .subscribe(
      (theUser: User) => {
        this._user = theUser;
        this.vorname.setValue(this._user.basisdaten.vorname);
        this.nachname.setValue(this._user.basisdaten.nachname);
        this.formSubmitAttempt = false;
        this.logger.debug('vorname.value=' + this.vorname.value + ', nachname.value=' + this.nachname.value)
      });

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
        this.btnSubmitDisabled = false;
      }
      if (apiMsg.level === INFO) {
        this.btnSubmitDisabled = true;
      }
    });

    this._subscription.add(messageSubscription);
    this._subscription.add(userSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this.logger.debug('ProfilEmailComponent destroyed');
    this.messageService.clearMessage();
  }

  togglePwdInfo(): void {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  showValidationErrors(): void {
    validateAllFormFields(this.nameAendernForm);
    if (this.vorname.value === this._user.basisdaten.vorname && this.nachname.value === this._user.basisdaten.nachname) {
      const apiMessage = new APIMessage(ERROR, 'Bitte ändern Sie die Eingabe im Feld Vorname oder im Feld Nachname.');
      this.messageStore.dispatch(ActionFactory.apiMessageAction(apiMessage));
    }
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    if (!this.nameAendernForm.pristine && this.nameAendernForm.valid) {
      this._payload = Object.assign({}, value) as PersonAendernPayload;
      this._payload.username = this._user.basisdaten.email;
      this._payload.rolle = this._user.basisdaten.rolle;
      this.logger.debug('loginCredentials: ' + JSON.stringify(this._payload));
      this.profilService.personAendern(this._payload);
    } else {
      this.logger.debug('validierungsfehler');
      this.showValidationErrors();
    }
  }

  cancel(): void {
    this.messageService.clearMessage();
    this.router.navigate(['/profil']);
  }
}

