import {Component, OnInit, OnDestroy, Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {KatalogItem, KatalogItemMap} from '../../models/kataloge';
import {KatalogItemStore} from '../../state/store/katalogItem.store';
import {GlobalSettings} from '../../models/serverResponse';
import {GlobalSettingsStore} from '../../state/store/globalSettings.store';
import {MessageService} from '../../services/message.service';
import {ProfilService} from '../../services/profil.service';
import {User} from '../../models/user';
import {UserStore} from '../../state/store/user.store';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-schulzuordnung',
  templateUrl: './schulzuordnung.component.html',
})

@Injectable()
export class SchulzuordnungComponent implements OnInit, OnDestroy {

  neueSchuleForm: FormGroup;
  einverstanden: AbstractControl;
  wettbewerbsjahr: string;
  selectedSchule: KatalogItem;
  selectedLandName: string;
  user: User;

  private submitDisabled: boolean;

  private subscriptions: Subscription;

  private user$: Observable<User>;
  private globalSettings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private katalogItems$: Observable<KatalogItemMap>;


  constructor(private fb: FormBuilder
    , r: ActivatedRoute
    , private settingsStore: GlobalSettingsStore
    , private userStore: UserStore
    , private katalogItemStore: KatalogItemStore
    , private messageService: MessageService
    , private profilService: ProfilService
    , private router: Router
    , private logger: LogService) {

    this.neueSchuleForm = fb.group({
      'einverstanden': [false, []]
    });

    this.einverstanden = this.neueSchuleForm.controls['einverstanden'];
    this.wettbewerbsjahr = '';
    this.submitDisabled = false;

    this.subscriptions = new Subscription();

    /**
     * neue Observables, auf die Subjects im Store
     */
    this.katalogItems$ = this.katalogItemStore.stateSubject.asObservable();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.globalSettings$ = this.settingsStore.stateSubject.asObservable();
  }

  ngOnInit() {

    this.submitDisabled = false;

    const userSubscription = this.user$.subscribe((u: User) => {
      if (u) {
        this.user = new User(u);
      }
    });

    const globalSettingsSubscription = this.globalSettings$
      .subscribe(
      (settings: GlobalSettings) => {
        this.wettbewerbsjahr = settings.wettbewerbsjahr;
      });

    const katalogItemsSubscription = this.katalogItems$
      .subscribe((map: KatalogItemMap) => {
        this.selectedSchule = map.schule;
      });

    this.subscriptions.add(katalogItemsSubscription);
    this.subscriptions.add(userSubscription);
    this.subscriptions.add(globalSettingsSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  isSubmitDisabled(): boolean {
    if (this.submitDisabled) {
      return true;
    }
    if (!this.user || !this.user.erweiterteKontodaten || !this.user.erweiterteKontodaten.schule) {
      return true;
    }
    if (!this.selectedSchule) {
      return true;
    }
    return false;
  }

  onSubmit(value: any): void {
    this.submitDisabled = true;
    this.logger.debug('you submitted value:' + value);
    const payload = {'kuerzelAlt': this.user.erweiterteKontodaten.schule.kuerzel, 'kuerzelNeu': this.selectedSchule.kuerzel, 'anmelden': value.einverstanden};
    this.profilService.schuleWechseln(payload);
  }

  cancel() {
    this.messageService.clearMessage();
    this.router.navigate(['/profil']);
  }
}

