import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {ProfilService} from '../../services/profil.service';
import {APIMessage, INFO, WARN, ERROR} from '../../models/serverResponse';
import {ApiMessageStore} from '../../state/store/apiMessage.store';
import {MessageService} from '../../services/message.service';
import {MailadresseAendernPayload} from '../../models/clientPayload';
import {User} from '../../models/user';
import {UserStore} from '../../state/store/user.store';
import {mkvEmailValidator, mkvPasswordValidator, mkvEmailEmailNeuValidator, validateAllFormFields} from '../../validators/app.validators';
import {AppConstants} from '../../core/app.constants';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-profil-email',
  templateUrl: './profil.email.component.html',
})

export class ProfilEmailComponent implements OnInit, OnDestroy {

  emailAendernForm: FormGroup;
  username: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;

  private mailadressePayload: MailadresseAendernPayload;
  private subscriptions: Subscription;

  private user$: Observable<User>;
  private message$: Observable<APIMessage>;

  infoPasswordExpanded: boolean;
  tooltipPasswort: string;
  private usernameValue: string;

  formSubmitAttempt: boolean;
  btnSubmitDisabled: boolean;

  constructor(private fb: FormBuilder
    , r: ActivatedRoute
    , private profilService: ProfilService
    , private messageService: MessageService
    , private userStore: UserStore
    , private messageStore: ApiMessageStore
    , private router: Router
    , private logger: LogService) {

    this.emailAendernForm = fb.group({
      'username': ['', [Validators.required, mkvEmailValidator]],
      'email': ['', [Validators.required, mkvEmailValidator]],
      'password': ['', [Validators.required, mkvPasswordValidator]]
    }, {validator: mkvEmailEmailNeuValidator});

    this.username = this.emailAendernForm.controls['username'];
    this.email = this.emailAendernForm.controls['email'];
    this.password = this.emailAendernForm.controls['password'];
    this.usernameValue = '';

    this.subscriptions = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.message$ = this.messageStore.stateSubject.asObservable();

    this.infoPasswordExpanded = false;
    this.tooltipPasswort = AppConstants.tooltips.PASSWORTREGELN;
  }

  ngOnInit() {
    this.formSubmitAttempt = false;
    this.btnSubmitDisabled = false;

    const userSubscription = this.user$
      .subscribe(
      (user: User) => {
        this.usernameValue = user.basisdaten.email;
        this.username.setValue(user.basisdaten.email);
        this.email.setValue(user.basisdaten.email);
        this.formSubmitAttempt = false;
        this.logger.debug('username.value=' + this.username.value + ', email.value=' + this.email.value)
      });

    const messageSubscription = this.message$.subscribe((apiMsg: APIMessage) => {
      if (apiMsg.level === WARN || apiMsg.level === ERROR) {
        this.formSubmitAttempt = false;
        this.btnSubmitDisabled = false;
      }
      if (apiMsg.level === INFO) {
        this.btnSubmitDisabled = true;
      }
    });
    this.subscriptions.add(messageSubscription);
    this.subscriptions.add(userSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.logger.debug('ProfilEmailComponent destroyed');
    this.messageService.clearMessage();
  }

  togglePwdInfo(): void {
    this.infoPasswordExpanded = !this.infoPasswordExpanded;
  };

  triggerErrorsInForm(): void {
    this.email.markAsTouched({onlySelf: true});
    this.password.markAsTouched({onlySelf: true});
    mkvEmailEmailNeuValidator(this.emailAendernForm);
  }

  onSubmit(value: any): void {
    this.formSubmitAttempt = true;
    this.logger.debug('you submitted value:' + value);
    if (this.emailAendernForm.valid) {
      this.mailadressePayload = Object.assign({}, value) as MailadresseAendernPayload;
      this.mailadressePayload.username = this.usernameValue;
      this.logger.debug('loginCredentials: ' + JSON.stringify(this.mailadressePayload));
      this.profilService.emailAendern(this.mailadressePayload);
    } else {
      this.logger.debug('validierungsfehler');
      this.triggerErrorsInForm();
    }
  }

  cancel(): void {
    this.messageService.clearMessage();
    this.router.navigate(['/profil']);
  }
}

