import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {StringAction, REDIRECTED_FROM_URL} from '../state/actions';
import {StaticContentStore} from '../state/store/staticContent.store';
import {SessionToken} from '../models/serverResponse';
import {AuthenticationTokenStore} from '../state/store/authenticationToken.store';

import {Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private logger: LogService
    , private authTokenStore: AuthenticationTokenStore
    , private router: Router
    , private staticContentStore: StaticContentStore) {
    this.logger.debug('LoginGuard created');
  }

  canActivate(routeSnapshot: ActivatedRouteSnapshot, routerSnapshot: RouterStateSnapshot): Observable<boolean> | boolean {

    const _token = this.authTokenStore.getState() as SessionToken;
    if (!_token || !_token.accessToken) {
      const url = encodeURI(routerSnapshot.url);

      const action = {} as StringAction;
      action.type = REDIRECTED_FROM_URL;
      action.str = '' + url;
      this.logger.debug('LoginGuard: url=' + JSON.stringify(action));
      this.staticContentStore.dispatch(action);

      this.router.navigate(['/login'], {queryParams: {redirect: url}});
    }
    return true;
  }
}