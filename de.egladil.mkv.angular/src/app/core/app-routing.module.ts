import {NgModule, OnDestroy} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LandingComponent} from '../pages/landing/landing.component';
import {LoginComponent} from '../pages/login/login.component';
import {OrderTempPwdComponent} from '../pages/temppassword/ordertemppwd.component';
import {ChangeTempPwdComponent} from '../pages/temppassword/changetemppwd.component';
import {TeilnahmelistComponent} from '../pages/teilnahmen/teilnahmeliste/teilnahmelist.component';
import {DashboardComponent} from '../pages/dashboard/dashboard.component';
import {ProfilComponent} from '../pages/profil/profil.component';
import {SchulzuordnungComponent} from '../pages/profil/schulzuordnung.component';
import {KontoLoeschenComponent} from '../pages/profil/kontoLoeschen.component';
import {DownloadComponent} from '../pages/download/download.component';
import {UploadComponent} from '../pages/upload/upload.component';
import {LoginGuard} from './login.guard';
import {NotFoundComponent} from '../pages/not-found/not-found.component';
import {NeueSchuleComponent} from '../shared/neueSchule.component';
import {RegisterPrivatComponent} from '../pages/register/registerPrivat.component';
import {StaticContentComponent} from '../shared/staticContent.component';
import {RegisterComponent} from '../pages/register/register.component';
import {RegisterLehrerComponent} from '../pages/register/registerLehrer.component';
import {ProfilEmailComponent} from '../pages/profil/profil.email.component';
import {ProfilNameComponent} from '../pages/profil/profil.name.component';
import {ProfilPasswortComponent} from '../pages/profil/profil.passwort.component';
import {TeilnehmerListComponent} from '../pages/auswertung/teilnehmer-list.component';
import {GruppenteilnehmerComponent} from '../pages/auswertung/gruppenteilnehmer.component';
import {AnmeldungComponent} from '../pages/teilnahmen/anmeldung/anmeldung.component';
import {EditTeilnehmerComponent} from '../pages/auswertung/edit-teilnehmer.component';
import {EditTeilnehmerGuard} from '../pages/auswertung/edit-teilnehmer.guard';
import {PrivaturkundenComponent} from '../pages/auswertung/privaturkunden.component';
import {SchulurkundenComponent} from '../pages/auswertung/schulurkunden.component';
import {AuswertungsgruppeComponent} from '../pages/auswertung/auswertungsgruppe.component';
import {EditAuswertungsgruppeComponent} from '../pages/auswertung/edit-auswertungsgruppe.component';
import {AdvComponent} from '../adv/adv.component';
// import {teilnehmerRoutes, teilnehmerRoutingComponents, teilnehmerRoutingProviders} from '../pages / teilnahmen / auswertung / teilnehmer.routing';


const appRoutes: Routes = [
  {path: 'landing', component: LandingComponent},
  {path: 'passwort/temp/order', component: OrderTempPwdComponent, data: {title: 'Passwort vergessen'}},
  {path: 'passwort/temp/change', component: ChangeTempPwdComponent, data: {title: 'Einmalpasswort ändern'}},
  {path: 'registrierungen', component: RegisterComponent},
  {path: 'registrierungen/lehrer', component: RegisterLehrerComponent},
  {path: 'registrierungen/privat', component: RegisterPrivatComponent},
  {path: 'schule', component: NeueSchuleComponent, data: {title: 'neue Schule'}},
  {path: 'dashboard', canActivate: [LoginGuard], component: DashboardComponent},
  {path: 'teilnahmen', canActivate: [LoginGuard], component: TeilnahmelistComponent, data: {title: 'Teilnahmen'}},
  {path: 'profil', canActivate: [LoginGuard], component: ProfilComponent},
  {path: 'profil/email', canActivate: [LoginGuard], component: ProfilEmailComponent},
  {path: 'profil/name', canActivate: [LoginGuard], component: ProfilNameComponent},
  {path: 'profil/passwort', canActivate: [LoginGuard], component: ProfilPasswortComponent},
  {path: 'download', canActivate: [LoginGuard], component: DownloadComponent},
  {path: 'upload', canActivate: [LoginGuard], component: UploadComponent},
  {path: 'anmeldung', canActivate: [LoginGuard], component: AnmeldungComponent},
  {path: 'adv', canActivate: [LoginGuard], component: AdvComponent},
  //  {path: 'auswertung', canActivate: [LoginGuard], component: TeilnehmerListComponent, children: teilnehmerRoutes},

  // Auswertung
  {path: 'auswertungsgruppen', canActivate: [LoginGuard], component: AuswertungsgruppeComponent},
  {path: 'auswertungsgruppen/auswertungsgruppe/:kuerzel', canActivate: [LoginGuard], component: GruppenteilnehmerComponent},
  {path: 'auswertungsgruppen/auswertungsgruppe/teilnehmer/edit/new', canActivate: [LoginGuard], component: EditTeilnehmerComponent},
  {path: 'auswertungsgruppen/auswertungsgruppe/teilnehmer/edit/:kuerzel', canActivate: [LoginGuard], component: EditTeilnehmerComponent},
  {path: 'auswertungsgruppen/edit/:kuerzel', canActivate: [LoginGuard], component: EditAuswertungsgruppeComponent},
  {path: 'auswertungsgruppen/edit/new', canActivate: [LoginGuard], component: EditAuswertungsgruppeComponent},
  {path: 'auswertungsgruppen/urkunden', canActivate: [LoginGuard], component: SchulurkundenComponent},

  // Auswertung mit Teilnehmern
  {path: 'teilnehmer', canActivate: [LoginGuard], component: TeilnehmerListComponent},
  {path: 'teilnehmer/edit/:kuerzel', canActivate: [LoginGuard], canDeactivate: [EditTeilnehmerGuard], component: EditTeilnehmerComponent},
  {path: 'teilnehmer/edit/new', canActivate: [LoginGuard], canDeactivate: [EditTeilnehmerGuard], component: EditTeilnehmerComponent},

  {path: 'teilnehmer/urkunden', canActivate: [LoginGuard], component: PrivaturkundenComponent},

  {path: 'profil/schulzuordnung', canActivate: [LoginGuard], component: SchulzuordnungComponent},
  {path: 'profil/kontoloeschung', canActivate: [LoginGuard], component: KontoLoeschenComponent},
  {path: 'login', component: LoginComponent, data: {title: 'Login'}},
  {path: 'staticcontent', component: StaticContentComponent},
  /** Redirect Konfigurationen **/
  {path: '', redirectTo: '/landing', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}, // immer als letztes konfigurieren - erste Route die matched wird angesteuert
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const appRouting = RouterModule.forRoot(appRoutes, {useHash: true});

export const routingComponents = [LandingComponent
  , OrderTempPwdComponent
  , ChangeTempPwdComponent
  , RegisterComponent
  , RegisterLehrerComponent
  , RegisterPrivatComponent
  , NeueSchuleComponent
  , DashboardComponent
  , TeilnahmelistComponent
  , ProfilComponent
  , ProfilEmailComponent
  , ProfilNameComponent
  , ProfilPasswortComponent
  , DownloadComponent
  , UploadComponent
  , SchulzuordnungComponent
  , KontoLoeschenComponent
  , NotFoundComponent
  //  , ...teilnehmerRoutingComponents
];

export const routingProviders = [LoginGuard, EditTeilnehmerGuard];
