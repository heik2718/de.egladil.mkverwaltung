
export const ACTIVE = 'ACTIVE';
export const INACTIVE = 'INACTIVE';
export const PENDING = 'PENDING';

export class AppConstants {

  public static tooltips = {
    'PASSWORTREGELN': 'mindestens 8 Zeichen, höchstens 100 Zeichen, mindestens ein Buchstabe, mindestens eine Ziffer, keine Leerzeichen am Anfang und am Ende'
    + 'erlaubte Sonderzeichen sind !"#$%&)(*+,-./:;<=>?@][^_`\'{|}~',
    'MAILADRESSE_REGISTRIERT': 'Tragen Sie hier bitte die Mailadresse ein, mit der Sie sich registriert haben.',
    'MAILADRESSE_REGISTRIERUNG': 'Mit dieser Mailadresse werden Sie sich später einloggen.',
    'TEMPPWD': 'Tragen Sie hier bitte das Einmalpasswort ein, das Ihnen per Mail gesendet wurde.',
    'NAME_KOLLEGEN': 'Vor- und Nachname werden den Kollegen und Kolleginnen der gleichen Schule angezeigt.',
    'NEWSLETTER_ACTIVATE': 'Sie werden informiert, wenn die Anmeldung zum neuen Wettbewerb begonnen hat oder Unterlagen verfügbar sind.',
    'NEWSLETTER_DEACTIVATE': 'Sie erhalten keine automatischen Mails, wenn die Anmeldung zum neuen Wettbewerb begonnen hat oder Unterlagen verfügbar sind.',
    'TEILNEHMER_ZUSATZ': 'Dieser Wert erscheint nicht auf der Urkunde. Sie können damit Kinder mit gleichen Namen unterscheidbar machen.',
    'ANTWORTEN_DEFAULTEINGABE': 'Standardeingabe: zu jeder Aufgabe das angekreuzte Kästchen markieren.',
    'ANTWORTEN_SCHNELLEINGABE': 'Schnelleingabe (erfahrene Benutzer): Eingabe der Lösungsbuchstaben wie beim Känguru-Wettbewerb.',
    'ANTWORTEN_LOESCHEN': 'alle bisherigen Antworten löschen und speichern',
    'AUSWERTUNGSGRUPPE_NAME': 'Dieser Name erscheint auf der Urkunde',
    'TEILNEHMERURKUNDE_INFO': 'gedacht zum Korrigieren einzelner Urkunden - zum Druck aller Urkunden über Menüpunkt Auswertung/Urkunden gehen'
  };

  public static buttontexte = {
    'BTN_NEWSLETTER_ACTIVATE': 'aktivieren',
    'BTN_NEWSLETTER_DEACTIVATE': 'deaktivieren',
    'BTN_ANTWORTEN_DEFAULTEINGABE': 'Antworten erfassen',
    'BTN_ANTWORTEN_SCHNELLEINGABE': 'Antworten Schnelleingabe',
    'BTN_ANTWORTEN_LOESCHEN': 'Antworten löschen und speichern',
    'CHK_NEWSLETTER': `Ich willige ein, dass mich H.Winkelvoß über den Wettbewerb betreffende Änderungen per E-Mail informiert. Dies ist z.B. die Freischaltung von
          Wettbewerbsunterlagen, die Fertigstellung der Gesamtauswertung, der Beginn des Anmeldezeitraums oder eine Erinnerung an das Hochladen der Schulauswertung.
          Meine Daten werden ausschließlich zu diesem Zweck genutzt. Eine Weitergabe an Dritte erfolgt nicht. Ich kann die
          Einwilligung jederzeit per E-Mail an minikaenguru@egladil.de oder durch Änderung meines Profils widerrufen.`
  };

  public static texte = {
    //    'ANMELDUNG_PENDING': 'Der Anmeldezeitraum hat noch nicht begonnen',
    'PROFIL_DESCR_LEHRER': 'Name, Mailadresse, Passwort, Mailbenachrichtigung oder Schulzuordnung ändern; Konto löschen',
    'PROFIL_DESCR_PRIVAT': 'Name, Mailadresse, Passwort oder Mailbenachrichtigung ändern; Konto löschen',
    'WETTBEWRB_RUHT': 'Der Wettbewerb hat noch nicht begonnen.',
    'KEIN_DOWNLOAD_OHNE_TEILNAHME': 'Sie können keine Unterlagen herunterladen, da Sie nicht zum aktuellen Wettbewerb angemeldet sind.',
    'AUSWERTUNG_DESCRIPTION': 'Antworten Ihrer Kinder erfassen und alle Urkunden drucken',
    'KEINE_URKUNDEN_WEGEN_UPLOADS': `Sie können leider keine Urkunden generieren lassen, da Sie bereits Excel/Open Office- Dateien hochgeladen haben.`,
    'KEIN_UPLOAD_OHNE_TEILNAHME': 'Sie können keine Auswertungstabellen hochladen, da Sie nicht zum aktuellen Wettbewerb angemeldet sind.',
    'KEIN_UPLOAD_WEGEN_URKUNDEN': 'Sie können keine Auswertungstabellen hochladen, da Sie die Auswertung bereits hier im Programm begonnen haben.',
    'UPLOAD_EXCEL_DESCRIPTION': 'Achtung: ohne Urkunden!',
    'KEINE_AUSWERTUNG_OHNE_TEILNAHME': 'Sie können keine Urkunden erstellen, da Sie nicht zum aktuellen Wettbewerb angemeldet sind.',
  };

  public static colors = {
    'DARK_GRAY': '#555753',
    'WHITE': '#ffffff'
  };

  public static backgroundColors = {
    'INPUT_INVALID': '#fffae6',
    'INPUT_VALID': '#eefbe9',
    'INPUT_NEUTRAL': '#ffffff',
    'INPUT_MISSING': '#d3d7cf'
  };
}


