
import { filter } from 'rxjs/operators';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { UserStore } from '../state/store/user.store';
import { UserDelegate } from '../shared/user.delegate';
import { GlobalSettings } from '../models/serverResponse';
import { User } from '../models/user';
import { SessionToken } from '../models/serverResponse';
import { ActionFactory, STATIC_KONTAKT, STATIC_AGB } from '../state/actions';
import { KatalogStore } from '../state/store/katalog.store';
import { StaticContentStore } from '../state/store/staticContent.store';
import { KatalogService } from '../services/katalog.service';
import { SessionService } from '../services/session.service';

import { LogService } from 'hewi-ng-lib';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
// tslint:disable-next-line:import-blacklist
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'mkv-root',
  templateUrl: './app.component.html',
  providers: []
})
export class AppComponent implements OnInit, OnDestroy {

  defaultTitle: string;
  title = 'Minikänguru';
  version = environment.version;
  envName = environment.envName;
  showEnv = !environment.production;
  api = environment.apiUrl;
  xsrfToken: string;
  showCsrf = !environment.production;
  loggedIn: boolean;
  downloadEnabled: boolean;
  uploadEnabled: boolean;
  auswertungEnabled: boolean;
  ankuendigungWartungsmeldung: boolean;
  wartungsmeldung: string;


  response$: Observable<Response>;

  private _user: User;

  private subscriptions: Subscription;

  private settings$: Observable<GlobalSettings>; // wird durch den store gefüllt
  private authToken$: Observable<SessionToken>; // wird durch den store gefüllt
  private _user$: Observable<User>;

  @ViewChild('navbarToggler', { static: false }) navbarToggler: ElementRef;

  constructor(private settingsStore: GlobalSettingsStore
    , private authenticationTokenStore: AuthenticationTokenStore
    , private katalogStore: KatalogStore
    , private kataloService: KatalogService
    , private logger: LogService
    , private sessionService: SessionService
    , private staticContentStore: StaticContentStore
    , private userStore: UserStore
    , private userDelegate: UserDelegate
    , private router: Router
    , private activatedRoute: ActivatedRoute
    , private titleService: Title
    //    , private staticContentService: StaticContentService
  ) {

    this.subscriptions = new Subscription();
    this.loggedIn = false;
    this.auswertungEnabled = false;


    /**
     * neue Observables, auf die Subjects im Store
     */
    this.settings$ = this.settingsStore.stateSubject.asObservable();
    this.authToken$ = this.authenticationTokenStore.stateSubject.asObservable();
    this._user$ = this.userStore.stateSubject.asObservable();

    this.logger.debug('AppComponent created');
  }

  ngOnInit() {
    this.ankuendigungWartungsmeldung = false;
    this.wartungsmeldung = '';
    this.defaultTitle = this.titleService.getTitle();
    this.router.events.pipe(
      //      .do(event => console.log(event))
      filter(event => event instanceof NavigationEnd))
      .subscribe(event => {
        //        console.log(event);
        this.setBrowserTitle();
      });

    /**
     * subscribe to the kontext Subject of the Store
     */
    const settingsSubscription = this.settings$
      .subscribe(
        (settings: GlobalSettings) => {
          this.xsrfToken = settings.xsrfToken;
          const authToken = { 'xsrfToken': this.xsrfToken };
          this.authenticationTokenStore.dispatch(ActionFactory.authenticationTokenAction(authToken));
          this.logger.debug('nach sync des AuthTokens');
          this.title = 'Minikänguru ' + settings.wettbewerbsjahr;
          this.downloadEnabled = settings.downloadFreigegebenLehrer || settings.downloadFreigegebenPrivat;
          this.uploadEnabled = !settings.uploadDisabled;
          this.auswertungEnabled = !settings.uploadDisabled;
          this.ankuendigungWartungsmeldung = settings.ankuendigungWartungsmeldung;
          this.wartungsmeldung = settings.wartungsmeldungText;
          this.checkLaender();
        });

    const authTokenSubscription = this.authToken$
      .subscribe(
        (authToken: SessionToken) => {
          this.loggedIn = authToken.accessToken !== null && authToken.accessToken !== undefined;
        });

    const userSubscription = this._user$
      .subscribe(
        (user: User) => {
          this.logger.debug('user bekommen');
          this._user = user;
        });

    this.subscriptions.add(settingsSubscription);
    this.subscriptions.add(authTokenSubscription);
    this.subscriptions.add(userSubscription);


    /**
     * Triggern http-Request. Resultat wird über die subscriptions aus dem MKVStore eingespeist
     */
    this.checkSession();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.logger.debug('AppComponent destroyed');
  }

  navBarTogglerIsVisible() {
    this.logger.debug('navBarTogglerIsVisible');
    return this.navbarToggler.nativeElement.offsetParent !== null;
  }

  collapseNav() {
    if (this.navBarTogglerIsVisible()) {
      this.logger.debug('visible');
      this.logger.debug(JSON.stringify(this.navbarToggler.nativeElement));
      this.navbarToggler.nativeElement.click();
    }
  }

  setBrowserTitle() {
    let title = this.defaultTitle;
    let route = this.activatedRoute;
    while (route.firstChild) {
      route = route.firstChild;
      title = route.snapshot.data['title'] || title;
    }
    this.titleService.setTitle(title);
  }

  isLoggedIn(): boolean {
    return this.loggedIn;
  }

  gotoSignIn(): void {
    this.collapseNav();
    this.router.navigate(['/registrierungen']);
  }

  gotoLogin(): void {
    this.collapseNav();
    this.router.navigate(['/login']);
  }

  goHome(): void {
    //    this.collapseNav();
    if (this.isLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {
      this.router.navigate(['/landing']);
    }
  }

  logout() {
    this.sessionService.logout();
    this.collapseNav();
    this.router.navigate(['/landing']);
  }

  checkSession() {
    this.logger.debug('this.xsrfToken=' + this.xsrfToken);
    if (!this.xsrfToken || this.xsrfToken.length === 0) {
      this.sessionService.ping();
    }
  }

  checkLaender() {
    const laender = this.katalogStore.getState().land;
    if (!laender || laender.length === 0) {
      this.kataloService.loadLaender();
    }
  }

  gotoAgb(): void {
    const action = {
      type: STATIC_AGB
    };
    this.staticContentStore.dispatch(action);
    this.router.navigate(['/staticcontent']);
  }

  gotoKontakt(): void {
    const action = {
      type: STATIC_KONTAKT
    };
    this.staticContentStore.dispatch(action);
    this.router.navigate(['/staticcontent']);
  }

  isLinkTeilnahmelistEnabled(): boolean {
    if (this._user === undefined) {
      return true;
    }
    return !this.userDelegate.isLehrer(this._user);
  }

  isLinkAuswertungsgruppeEnabled(): boolean {
    return this.userDelegate.isLehrer(this._user);
  }

  isAdvEnabled(): boolean {
    return this.userDelegate.isLehrer(this._user);
  }
}
