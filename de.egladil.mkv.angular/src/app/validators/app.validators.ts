import {FormGroup, FormControl, AbstractControl} from '@angular/forms';
import {SchulzuordnungAendernPayload} from '../models/clientPayload';

export function mkvEmailValidator(control): {
  [key: string]: any
} {
  const re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

  if (!control.value || control.value === '' || re.test(control.value)) {
    return null;
  } else {
    return {'invalidEMail': true};
  }
}

export function mkvPasswordValidator(control): {
  [key: string]: any
} {
  const re = /(?=[^A-ZÄÖÜa-zäöüß]*[A-ZÄÖÜa-zäöüß])(?=[^\d]*[\d])[A-Za-z0-9ÄÖÜäöüß !&quot;#\$%&amp;'\(\)\*\+,\-\.\/:&lt;=&gt;\?@\[\]\^\\_`'{|}~ ]{8,100}/;

  if (!control.value || control.value === '' || re.test(control.value)) {
    return null;
  } else {
    return {'invalidPassword': true};
  }
}

export function mkvEinmalPasswordValidator(control): {
  [key: string]: any
} {
  const re = /[a-zA-Z0-9\-]*/;

  if (!control.value || control.value === '' || re.test(control.value)) {
    return null;
  } else {
    return {'invalidEinmalPassword': true};
  }
}

function hasPasswordPasswordWdhError(passwort1: string, passwort2: string): boolean {
  if (passwort1 !== undefined && passwort2 !== undefined && passwort1 !== '' && passwort2 !== '' && passwort1 !== passwort2) {
    return true;
  }
  return false;
}

function hasAttributeAttributeWdhEqualError(attr1: string, attr2: string): boolean {
  if (attr1 === undefined && attr2 === undefined) {
    return true;
  }
  if (attr1 === '' && attr2 === '') {
    return true
  }
  if (attr1 !== undefined && attr1 !== '' && attr2 !== undefined && attr2 !== '' && attr1 === attr2) {
    return true
  }
  return false;
}

export function mkvPasswortPasswortNeuValidator(formGroup: AbstractControl): {
  [key: string]: any
} {
  const passwort1Control = formGroup.get('passwortNeu');
  const passwort2Control = formGroup.get('passwortNeuWdh');

  if (!passwort1Control || !passwort2Control) {
    return null;
  }
  const passwort1 = passwort1Control.value;
  const passwort2 = passwort2Control.value;

  if (hasPasswordPasswordWdhError(passwort1, passwort2)) {
    return {'passwortNeuPasswortNeuWdh': true};
  }
}

export function mkvPasswortPasswortWiederholtValidator(formGroup: AbstractControl): {
  [key: string]: any
} {
  const passwort1Control = formGroup.get('passwort');
  const passwort2Control = formGroup.get('passwortWdh');

  if (!passwort1Control || !passwort2Control) {
    return null;
  }
  const passwort = passwort1Control.value;
  const passwortWdh = passwort2Control.value;
  if (hasPasswordPasswordWdhError(passwort, passwortWdh)) {
    return {'passwortNeuPasswortNeuWdh': true};
  }
}

export function mkvEmailEmailNeuValidator(formGroup: AbstractControl): {
  [key: string]: any
} {
  const email1Control = formGroup.get('username');
  const email2Control = formGroup.get('email');

  if (!email1Control || !email2Control) {
    return null;
  }
  const attr1 = email1Control.value;
  const attr2 = email2Control.value;

  if (hasAttributeAttributeWdhEqualError(attr1, attr2)) {
    return {'attributeAttributeWdhEqual': true};
  }
}

export function schulzuordnungAendernValidator(payload: SchulzuordnungAendernPayload): {[key: string]: any} {
  if (!payload || payload.kuerzelAlt && payload.kuerzelAlt.length === 8 && payload.kuerzelAlt && payload.kuerzelNeu.length === 8) {
    return null
  }
  if (!payload.kuerzelAlt || payload.kuerzelAlt.length !== 8) {
    return {'alteSchuleRequired': true};
  }
  if (!payload.kuerzelNeu || payload.kuerzelNeu.length !== 8) {
    return {'neueSchuleRequired': true};
  }
  return null;
}

export function validateAllFormFields(formGroup: FormGroup): void {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({onlySelf: true});
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
}
