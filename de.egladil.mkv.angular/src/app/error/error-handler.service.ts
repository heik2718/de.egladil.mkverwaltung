import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LogService, MessagesService } from 'hewi-ng-lib';
import { environment } from 'environments/environment';
import { LogPublishersService } from '../logger/log-publishers.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserStore } from 'app/state/store/user.store';


@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

	private logService: LogService;

	constructor(private injector: Injector) {

		// ErrorHandler wird vor allen anderen Injectables instanziiert,
		// so dass man benötigte Services nicht im Konstruktor injekten kann !!!

		const logPublishersService = this.injector.get(LogPublishersService);
		this.logService = this.injector.get(LogService);

		this.logService.initLevel(environment.loglevel);
		this.logService.registerPublishers(logPublishersService.publishers);
		this.logService.info('logging initialized: loglevel=' + environment.loglevel);
	}


	handleError(error: any): void {

		let message = 'Checklistenapp: unerwarteter Fehler aufgetreten: ';

		if (error && error.message) {
			message += error.message;
		}

		// try sending an Error-Log to the Server
		const userStore = this.injector.get(UserStore);
		const user = userStore.getState();


		let userRef = '';

		if (user !== undefined && user.basisdaten) {
			userRef = user.basisdaten.email;
		} else {
			userRef = 'anonymous';
		}



		this.logService.error(message + ' (userRef=' + userRef + ')');

		if (error instanceof HttpErrorResponse) {
			this.logService.warn( message + ' (das sollte nicht vorkommen, da doese Errors von einem derr Services behandelt werden)');
		} else {
			this.logService.error(message + ' (userRef=' + userRef + ')');
		}

		this.injector.get(MessagesService).error(message);
	}
}