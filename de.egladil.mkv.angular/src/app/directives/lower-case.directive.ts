import {HostListener, HostBinding, Directive} from '@angular/core';
@Directive({
  selector: '[mkvLowerCase]'
})

export class LowerCaseDirective {
  @HostBinding() value = '';
  @HostListener('change', ['$event']) onChange($event) {
    this.value = $event.target.value.toLowerCase();
  }
}