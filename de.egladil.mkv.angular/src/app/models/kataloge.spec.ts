import {TreeNode, Schule} from './kataloge';

describe('TreeNode', () => {

  it('constructor TreeNode of PublicLand', () => {
    const land = {'name': 'Berlin', 'kuerzel': 'DE-BE', 'kinder': [{'name':'Berlin','kuerzel':'U4901QPO','kinder':[]}]};

    const treeNode = new TreeNode(land);

    expect(treeNode.kuerzel).toEqual('DE-BE');
    expect(treeNode.name).toEqual('Berlin');
    expect(treeNode.kinder.length).toEqual(0);

    treeNode.addAllKinder(land.kinder);

    expect(treeNode.kinder.length).toEqual(1);
    const ort = treeNode.kinder[0];
    expect(ort.kuerzel).toEqual('U4901QPO');
    expect(ort.name).toEqual('Berlin');
    expect(ort.kinder.length).toEqual(0);
  });
});