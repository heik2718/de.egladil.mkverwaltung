export const STATISTIK_LOESUNGSZETTEL = 'STATISTIK_LOESUNGSZETTEL';
export const STATISTIK_SCHULE = 'STATISTIK_SCHULE';
export const DOWNLOADART_URKUNDE = 'URKUNDE';
export const DOWNLOADART_ADV = '[downloadart] ADV';
export const DOWNLOADART_STATISTIK = 'STATISTIK';

export class Progress {
  kontext: string;
  running: boolean;

  constructor(theKontext: string, isRunning: boolean) {
    this.kontext = theKontext;
    this.running = isRunning;
  }
}


