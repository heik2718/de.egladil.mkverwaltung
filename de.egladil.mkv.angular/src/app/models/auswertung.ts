import {TeilnahmeIdentifier, createTeilnahmeIdentifier} from './user';
export const KLASSENSTUFE_1 = {name: 'EINS', label: 'Klasse 1', anzahlAufgaben: 12, anzahlSpalten: 4} as Klassenstufe;
export const KLASSENSTUFE_2 = {name: 'ZWEI', label: 'Klasse 2', anzahlAufgaben: 15, anzahlSpalten: 5} as Klassenstufe;
export const KLASSENSTUFE_I = {name: 'IKID', label: 'Inklusion', anzahlAufgaben: 6, anzahlSpalten: 3} as Klassenstufe;
export const EINGABEMODUS_DEFAULT = 'CHECKBOX';
export const EINGABEMODUS_SCHNELL = 'MATRIX';
export const SPRACHE_DE = {kuerzel: 'de', label: 'deutsch'};
export const SPRACHE_EN = {kuerzel: 'en', label: 'englisch'};




export interface Farbschema {
  name: string;
  label: string;
  thumbnail: string;
  imageData?: string;
}

export class DownloadCode {
  downloadCode: string;
  teilnahmeart: string;
  jahr: string;
  kuerzel; string;
  downloadArt: string;

  constructor(data: any = {}) {
    this.downloadCode = data && data.downloadCode || null;
    this.teilnahmeart = data && data.teilnahmeart || null;
    this.jahr = data && data.jahr || null;
    this.kuerzel = data && data.kuerzel || null;
    this.downloadArt = data && data.downloadArt || null;
  }
}


export interface Klassenstufe {
  name: string;
  label: string;
  anzahlAufgaben: number;
  anzahlSpalten: number;
}

export interface Sprache {
  kuerzel: string;
  label: string;
}

export interface Antwortbuchstabe {
  buchstabe: string;
  code: number;
  index: number;
  valid: boolean;
}

export interface Matrixantwortzeile {
  eingaben: string[];
  index: number;
}

export class Antwortcheckbox {
  parent: Checkboxantwortzeile;
  nummer: number;
  checked: boolean;
  color: string;

  constructor(parent: Checkboxantwortzeile, nummer: number) {
    this.parent = parent;
    this.nummer = nummer;
    this.checked = false;
    this.color = '#ffffff';
  }

  clicked(event) {
    this.parent.clicked(this.nummer);
  }

  toggleChecked() {
    if (this.color === '#ffffff') {
      this.color = '#555753';
    } else {
      this.color = '#ffffff';
    }
    this.checked = !this.checked;
  }

  check() {
    this.checked = true;
    this.color = '#555753';
  }

  uncheck() {
    this.checked = false;
    this.color = '#ffffff';
  }
}

export class Checkboxantwortzeile {
  index: number;
  nummer: string;
  zellen: Antwortcheckbox[];

  constructor(nummer: string, index: number, klassenstufe: Klassenstufe) {
    this.index = index;
    this.nummer = nummer;

    this.zellen = [];
    if (klassenstufe === KLASSENSTUFE_I) {
      this.zellen[0] = new Antwortcheckbox(this, 1);
      this.zellen[1] = new Antwortcheckbox(this, 2);
      this.zellen[2] = new Antwortcheckbox(this, 3);
    } else {
      this.zellen[0] = new Antwortcheckbox(this, 1);
      this.zellen[1] = new Antwortcheckbox(this, 2);
      this.zellen[2] = new Antwortcheckbox(this, 3);
      this.zellen[3] = new Antwortcheckbox(this, 4);
      this.zellen[4] = new Antwortcheckbox(this, 5);
    }
  }

  clicked(nummer: number) {
    for (let i = 0; i < this.zellen.length; i++) {
      const zelle = this.zellen[i];
      if (zelle.nummer === nummer) {
        zelle.toggleChecked();
      } else {
        zelle.uncheck();
      }
    }
  }
}

class Teilnehmer {
  kuerzel: string;
  teilnahmeart: string;
  teilnahmekuerzel: string;
  jahr: string;
  klassenstufe: Klassenstufe;
  vorname: string;
  nachname?: string;
  zusatz?: string;
  auswertungsgruppe?: Auswertungsgruppe;
  loesungszettelKuerzel?: string;
  punkte?: string;
  kaengurusprung?: number;
  antworten?: Antwortbuchstabe[];
  eingabemodus?: string;
  sprache?: Sprache;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.teilnahmeart = data && data.teilnahmeart || null;
    this.teilnahmekuerzel = data && data.teilnahmekuerzel || null;
    this.jahr = data && data.jahr || null;
    this.klassenstufe = data && data.klassenstufe || null;
    this.vorname = data && data.vorname || null;
    this.nachname = data && data.nachname || null;
    this.zusatz = data && data.zusatz || null;
    this.auswertungsgruppe = data && data.auswertungsgruppe || null;
    this.punkte = data && data.punkte || null;
    this.kaengurusprung = data && data.kaengurusprung || null;
    this.antworten = data && data.antworten || null;
    this.eingabemodus = data && data.eingabemodus || null;
    this.sprache = data && data.sprache || null;
  }
}

export interface TeilnehmerUndLoadStatus {
  teilnehmer: Teilnehmer[];
  geladen: boolean;
}

class Auswertungsgruppe {
  kuerzel: string;
  teilnahmeart: string;
  teilnahmekuerzel: string;
  jahr: string;
  klassenstufe?: Klassenstufe;
  name: string;
  anzahlKlassen?: number;
  anzahlKinder?: number;
  anzahlLoesungszettel?: number;
  farbschema?: Farbschema;
  parent?: Auswertungsgruppe;
  auswertungsgruppen?: Auswertungsgruppe[];
  teilnehmer?: Teilnehmer[];

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.jahr = data && data.jahr || null;
    this.teilnahmeart = data && data.teilnahmeart || null;
    this.auswertungsgruppen = data && data.teilnahmekuerzel || null;
    this.name = data && data.name || null;
    this.parent = data && data.parent || null;
    this.klassenstufe = data && data.klassenstufe || null;
    this.farbschema = data && data.farbschema || null;
    this.auswertungsgruppen = data && data.auswertungsgruppen || [];
    this.teilnehmer = data && data.teilnehmer || [];
  }
}

interface TeilnehmerUndFarbschema {
  farbschemaName?: string;
  teilnehmer: Teilnehmer[];
}

interface SchuleUrkundenauftrag {
  kuerzelRootgruppe: string;
  farbschemaName?: string;
}

export function createInitialTeilnehmerForKlasse(auswertungsgruppe: Auswertungsgruppe): Teilnehmer {
  //  console.log('before return');
  const _tIdentifier = createTeilnahmeIdentifier(auswertungsgruppe.teilnahmeart, auswertungsgruppe.teilnahmekuerzel, auswertungsgruppe.jahr);
  const teilnehmer = createInitialTeilnehmer(_tIdentifier);
  teilnehmer.klassenstufe = auswertungsgruppe.klassenstufe;
  return teilnehmer;
}

export function createInitialTeilnehmer(teilnahmeIdentifier: TeilnahmeIdentifier): Teilnehmer {
  //  console.log('before return');
  return {
    teilnahmeart: teilnahmeIdentifier.teilnahmeart,
    teilnahmekuerzel: teilnahmeIdentifier.kuerzel,
    jahr: teilnahmeIdentifier.jahr,
    vorname: '',
    nachname: null,
    zusatz: null,
    antworten: null,
    sprache: SPRACHE_DE
  } as Teilnehmer;
}

export function findKlassenstufeTeilnehmer(klassenstufen: Klassenstufe[], teilnehmer: Teilnehmer): Klassenstufe {
  //    console.log('findKlassenstufe: ' + JSON.stringify(klassenstufen) + ', ' + JSON.stringify(teilnehmer));
  const k = klassenstufen.find(ks => ks.name === teilnehmer.klassenstufe.name);

  return k !== undefined ? k : null;
}

export function findKlassenstufeAuswertungsgruppe(klassenstufen: Klassenstufe[], auswertungsgruppe: Auswertungsgruppe): Klassenstufe {
  //  console.log('findKlassenstufe: ' + JSON.stringify(klassenstufen) + ', ' + JSON.stringify(auswertungsgruppe));
  if (!auswertungsgruppe.klassenstufe) {
    return null;
  }
  const k = klassenstufen.find(ks => ks.name === auswertungsgruppe.klassenstufe.name);
  return k !== undefined ? k : null;
}

export function findAuswertungsgruppe(rootgruppe: Auswertungsgruppe, kuerzel: string): Auswertungsgruppe {
  if (kuerzel && rootgruppe) {
    const result = rootgruppe.auswertungsgruppen.find(g => g.kuerzel === kuerzel);
    return result !== undefined ? result : null;
  }
  return null;
}

export function findSpracheTeilnehmer(sprachen: Sprache[], teilnehmer: Teilnehmer): Sprache {
  //    console.log('findKlassenstufe: ' + JSON.stringify(klassenstufen) + ', ' + JSON.stringify(teilnehmer));
  const k = sprachen.find(sp => sp.kuerzel === (teilnehmer.sprache ? teilnehmer.sprache.kuerzel : null));

  return k !== undefined ? k : SPRACHE_DE;
}

export function antwortbuchstabeValid(wert: string): boolean {
  //  console.log('auswertung.ts: antwortbuchstabeValid');
  if (wert === undefined || wert === null || wert.length !== 1) {
    return false;
  }
  const validValues = 'abcdenABCDEN-012345';
  const index = validValues.indexOf(wert);
  return index > -1;
}

export function createInitialAntwortbuchstaben(klassenstufe: Klassenstufe): Antwortbuchstabe[] {
  const result = [];
  for (let i = 0; i < klassenstufe.anzahlAufgaben; i++) {
    result.push({
      'buchstabe': '',
      'code': -1,
      'index': i,
      'valid': false
    } as Antwortbuchstabe);
  }
  return result;
}

export function createAntwortbuchstabe(value: string, arrayIndex: number): Antwortbuchstabe {
  //  console.log('createAntwortbuchstabe.ts: antwortbuchstabeValid');
  const valid = antwortbuchstabeValid(value);
  const upper = value.toUpperCase();
  let buchstabe;
  let code;
  if (valid) {
    if ('ABCDEN-'.indexOf(upper) > -1) {
      buchstabe = upper;
      switch (buchstabe) {
        case 'A':
          code = 1; break;
        case 'B':
          code = 2; break;
        case 'C':
          code = 3; break;
        case 'D':
          code = 4; break;
        case 'E':
          code = 5; break;
        case 'N':
          code = 0; break;
        case '-':
          code = 0;
          buchstabe = 'N';
          break;
        default: break;
      }
    }
    if ('012345'.indexOf(upper) > -1) {
      code = Number(upper);
      switch (code) {
        case 0:
          buchstabe = 'N';
          break;
        case 1:
          buchstabe = 'A';
          break;
        case 2:
          buchstabe = 'B';
          break;
        case 3:
          buchstabe = 'C';
          break;
        case 4:
          buchstabe = 'D';
          break;
        case 5:
          buchstabe = 'E';
          break;
        default: break;
      }
    }
  } else {
    buchstabe = value;
    code = -1;
  }
  const antwortbuchstabe = {
    'buchstabe': buchstabe,
    'code': code,
    'index': arrayIndex,
    'valid': valid
  } as Antwortbuchstabe;
  return antwortbuchstabe;
}


//export function isCheckboxenValid(zeilen: Checkboxantwortzeile[]): boolean {
//  if (zeilen === null || zeilen === undefined || zeilen.length === 0) {
//    return true;
//  }
//  for (let i = 0; i < zeilen.length; i++) {
//    const zeile = zeilen[i];
//    const anzahlChecked = getAnzahlChecked(zeile);
//    if (anzahlChecked > 1) {
//      return false;
//    }
//
//  }
//  return true;
//}

export function mapToAntwortbuchstaben(zeilen: Checkboxantwortzeile[]): Antwortbuchstabe[] {
  const result = [];
  for (let i = 0; i < zeilen.length; i++) {
    const zeile = zeilen[i];
    const nummerChecked = _getNummerCheckedZelle(zeile);
    result[i] = createAntwortbuchstabe('' + nummerChecked, i);

  }
  return result;
}

export function getAnzahlChecked(zeile: Checkboxantwortzeile): number {
  let result = 0;
  for (let i = 0; i < zeile.zellen.length; i++) {
    const zelle = zeile.zellen[i];
    if (zelle.checked) {
      result++;
    }
  }
  return result;
}

function _getNummerCheckedZelle(zeile: Checkboxantwortzeile): number {
  for (let i = 0; i < zeile.zellen.length; i++) {
    if (zeile.zellen[i].checked) {
      return zeile.zellen[i].nummer;
    }
  }
  return 0;
}

function _getZelleMitNummer(zeile: Checkboxantwortzeile, nummer: number): Antwortcheckbox {
  for (let i = 0; zeile.zellen.length; i++) {
    const zelle = zeile.zellen[i];
    if (zelle.nummer === nummer) {
      return zelle;
    }
  }
  return null;
}

export function createInitialCheckboxAntwortzeilen(klassenstufe: Klassenstufe): Checkboxantwortzeile[] {
  const checkboxAntwortzeilen = [];
  if (klassenstufe === KLASSENSTUFE_1) {
    checkboxAntwortzeilen[0] = new Checkboxantwortzeile('A-1', 0, klassenstufe);
    checkboxAntwortzeilen[1] = new Checkboxantwortzeile('A-2', 1, klassenstufe);
    checkboxAntwortzeilen[2] = new Checkboxantwortzeile('A-3', 2, klassenstufe);
    checkboxAntwortzeilen[3] = new Checkboxantwortzeile('A-4', 3, klassenstufe);
    checkboxAntwortzeilen[4] = new Checkboxantwortzeile('B-1', 4, klassenstufe);
    checkboxAntwortzeilen[5] = new Checkboxantwortzeile('B-2', 5, klassenstufe);
    checkboxAntwortzeilen[6] = new Checkboxantwortzeile('B-3', 6, klassenstufe);
    checkboxAntwortzeilen[7] = new Checkboxantwortzeile('B-4', 7, klassenstufe);
    checkboxAntwortzeilen[8] = new Checkboxantwortzeile('C-1', 8, klassenstufe);
    checkboxAntwortzeilen[9] = new Checkboxantwortzeile('C-2', 9, klassenstufe);
    checkboxAntwortzeilen[10] = new Checkboxantwortzeile('C-3', 10, klassenstufe);
    checkboxAntwortzeilen[11] = new Checkboxantwortzeile('C-4', 11, klassenstufe);

    return checkboxAntwortzeilen;
  }

  if (klassenstufe === KLASSENSTUFE_2) {
    checkboxAntwortzeilen[0] = new Checkboxantwortzeile('A-1', 0, klassenstufe);
    checkboxAntwortzeilen[1] = new Checkboxantwortzeile('A-2', 1, klassenstufe);
    checkboxAntwortzeilen[2] = new Checkboxantwortzeile('A-3', 2, klassenstufe);
    checkboxAntwortzeilen[3] = new Checkboxantwortzeile('A-4', 3, klassenstufe);
    checkboxAntwortzeilen[4] = new Checkboxantwortzeile('A-5', 4, klassenstufe);
    checkboxAntwortzeilen[5] = new Checkboxantwortzeile('B-1', 5, klassenstufe);
    checkboxAntwortzeilen[6] = new Checkboxantwortzeile('B-2', 6, klassenstufe);
    checkboxAntwortzeilen[7] = new Checkboxantwortzeile('B-3', 7, klassenstufe);
    checkboxAntwortzeilen[8] = new Checkboxantwortzeile('B-4', 8, klassenstufe);
    checkboxAntwortzeilen[9] = new Checkboxantwortzeile('B-5', 9, klassenstufe);
    checkboxAntwortzeilen[10] = new Checkboxantwortzeile('C-1', 10, klassenstufe);
    checkboxAntwortzeilen[11] = new Checkboxantwortzeile('C-2', 11, klassenstufe);
    checkboxAntwortzeilen[12] = new Checkboxantwortzeile('C-3', 12, klassenstufe);
    checkboxAntwortzeilen[13] = new Checkboxantwortzeile('C-4', 13, klassenstufe);
    checkboxAntwortzeilen[14] = new Checkboxantwortzeile('C-5', 14, klassenstufe);

    return checkboxAntwortzeilen;
  }

  checkboxAntwortzeilen[0] = new Checkboxantwortzeile('A-1', 0, klassenstufe);
  checkboxAntwortzeilen[1] = new Checkboxantwortzeile('A-2', 1, klassenstufe);
  checkboxAntwortzeilen[2] = new Checkboxantwortzeile('B-1', 2, klassenstufe);
  checkboxAntwortzeilen[3] = new Checkboxantwortzeile('B-2', 3, klassenstufe);
  checkboxAntwortzeilen[4] = new Checkboxantwortzeile('C-1', 4, klassenstufe);
  checkboxAntwortzeilen[5] = new Checkboxantwortzeile('C-2', 5, klassenstufe);

  return checkboxAntwortzeilen;
}

export function getTeilnehmerToPrint(alleTeilnehmer: Teilnehmer[]): Teilnehmer[] {
  const teilnehmerFiltered = alleTeilnehmer.filter((t: Teilnehmer) =>
    t.loesungszettelKuerzel !== undefined && t.loesungszettelKuerzel !== null && t.loesungszettelKuerzel.length > 0
  );
  //  console.log('anzahl fertige=' + teilnehmerFiltered.length);
  return teilnehmerFiltered;
}

export function getAuswertungsgruppenToPrint(alleAuswertungsgruppen: Auswertungsgruppe[]): Auswertungsgruppe[] {
  const gruppenFiltered = alleAuswertungsgruppen.filter((g: Auswertungsgruppe) =>
    g.anzahlLoesungszettel > 0
  );
  //  console.log('hier mal stop');
  return gruppenFiltered;
}

export function isAntwortzettelLeer(zeilen: Checkboxantwortzeile[]): boolean {
  if (zeilen !== undefined && zeilen !== null) {
    for (let i = 0; i < zeilen.length; i++) {
      const zeile = zeilen[i];
      for (let j = 0; j < zeile.zellen.length; j++) {
        const zelle = zeile.zellen[j];
        if (zelle.checked) {
          return false;
        }
      }
    }
    return true;
  }
  return false;
}

export {TeilnehmerUndFarbschema, SchuleUrkundenauftrag, Auswertungsgruppe, Teilnehmer};







