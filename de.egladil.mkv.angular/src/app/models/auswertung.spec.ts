import {Klassenstufe, KLASSENSTUFE_1, KLASSENSTUFE_2, Teilnehmer, findKlassenstufe, createAntwortbuchstabe, antwortbuchstabeValid, getTeilnehmerToPrint} from './auswertung';

describe('Klassenstufen', () => {

  it('findet Klassenstufe in array', () => {
    const klassenstufen = [];
    klassenstufen.push(KLASSENSTUFE_1);
    klassenstufen.push(KLASSENSTUFE_2);

    const teilnehmer = {
      'kuerzel': 'sgdgqi',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ghdgdigw',
      'jahr': '2018',
      'klassenstufe': KLASSENSTUFE_2,
      'vorname': 'Horst'
    } as Teilnehmer;

    const result = findKlassenstufe(klassenstufen, teilnehmer);
    expect(result.label).toEqual('Klasse 2');
  });

  it('findet Klassenstufe nicht', () => {
    const klassenstufen = [];
    klassenstufen.push(KLASSENSTUFE_1);
    klassenstufen.push(KLASSENSTUFE_2);

    const unbekannte = {name: 'DREI', label: 'Klasse 3'} as Klassenstufe;

    const teilnehmer = {
      'kuerzel': 'sgdgqi',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ghdgdigw',
      'jahr': '2018',
      'klassenstufe': unbekannte,
      'vorname': 'Horst'
    } as Teilnehmer;

    const result = findKlassenstufe(klassenstufen, teilnehmer);
    expect(result).toBe(null);
  });
});

describe('Anntwortbuchstaben', () => {

  it('berechnet valid korrekt', () => {
    const eingaben = ['a', 'b', 'c', 'd', 'e', 'n', 'A', 'B', 'C', 'D', 'E', 'N', '-', '0', '1', '2', '3', '4', '5'];

    for (let i = 0; i < eingaben.length; i++) {
      const valid = antwortbuchstabeValid(eingaben[i]);
      expect(valid).toBeTruthy();
    }
  });

  it('berechnet not valid korrekt', () => {
    const eingaben = ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'ab', '', '6', '7', '8', '9'];

    for (let i = 0; i < eingaben.length; i++) {
      const valid = antwortbuchstabeValid(eingaben[i]);
      expect(valid).toBeFalsy();
    }
  });

  it('berechnet antwortbuchstabe korrekt', () => {
    const eingaben = ['a', 'b', 'c', 'd', 'e', 'n', 'A', 'B', 'C', 'D', 'E', 'N', '-', '0', '1', '2', '3', '4', '5'];

    for (let i = 0; i < eingaben.length; i++) {
      const antwortbuchstabe = createAntwortbuchstabe(eingaben[i], i);
      expect(antwortbuchstabe.index).toBe(i);
      expect(antwortbuchstabe.valid).toBeTruthy();
      switch (i) {
        case 0:
        case 6:
        case 14:
          expect(antwortbuchstabe.buchstabe).toBe('A');
          expect(antwortbuchstabe.code).toBe(1);
          break;
        case 1:
        case 7:
        case 15:
          expect(antwortbuchstabe.buchstabe).toBe('B');
          expect(antwortbuchstabe.code).toBe(2);
          break;
        case 2:
        case 8:
        case 16:
          expect(antwortbuchstabe.buchstabe).toBe('C');
          expect(antwortbuchstabe.code).toBe(3);
          break;
        case 3:
        case 9:
        case 17:
          expect(antwortbuchstabe.buchstabe).toBe('D');
          expect(antwortbuchstabe.code).toBe(4);
          break;
        case 4:
        case 10:
        case 18:
          expect(antwortbuchstabe.buchstabe).toBe('E');
          expect(antwortbuchstabe.code).toBe(5);
          break;
        case 5:
        case 11:
        case 12:
        case 13:
          expect(antwortbuchstabe.buchstabe).toBe('N');
          expect(antwortbuchstabe.code).toBe(0);
          break;
        default: break;
      }
    }

  });

  it('berechnet invalid antwortbuchstabe korrekt', () => {
    const eingaben = ['r', 'ab', '6'];

    for (let i = 0; i < eingaben.length; i++) {
      const antwortbuchstabe = createAntwortbuchstabe(eingaben[i], i);
      expect(antwortbuchstabe.index).toBe(i);
      expect(antwortbuchstabe.valid).toBeFalsy();
      switch (i) {
        case 0:
          expect(antwortbuchstabe.buchstabe).toBe('r');
          expect(antwortbuchstabe.code).toBe(-1);
          break;
        case 1:
          expect(antwortbuchstabe.buchstabe).toBe('ab');
          expect(antwortbuchstabe.code).toBe(-1);
          break;
        case 2:
          expect(antwortbuchstabe.buchstabe).toBe('6');
          expect(antwortbuchstabe.code).toBe(-1);
          break;
        default: break;
      }
    }
  });
});

describe('Teilnehmerfilter', () => {
  it('getTeilnehmerToPrint findet die richtigen Teilnehmer', () => {
    const alleTeilnehmer = [];

    alleTeilnehmer.push({
      'kuerzel': 'TRTTFGHG',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ABCDEFGH',
      'jahr': '2018',
      'klassenstufe': KLASSENSTUFE_1,
      'vorname': 'Rudolph'
    } as Teilnehmer);

    alleTeilnehmer.push({
      'kuerzel': 'KJHZGTD5',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ABCDEFGH',
      'jahr': '2018',
      'klassenstufe': KLASSENSTUFE_1,
      'vorname': 'Luise',
      'loesungszettelKuerzel': 'G'
    } as Teilnehmer);

    alleTeilnehmer.push({
      'kuerzel': 'HZTFGRE4',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ABCDEFGH',
      'jahr': '2018',
      'klassenstufe': KLASSENSTUFE_1,
      'vorname': 'Luise',
      'loesungszettelKuerzel': ''
    } as Teilnehmer);

     alleTeilnehmer.push({
      'kuerzel': 'JHZTRF65',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ABCDEFGH',
      'jahr': '2018',
      'klassenstufe': KLASSENSTUFE_1,
      'vorname': 'Luise',
      'loesungszettelKuerzel': null
    } as Teilnehmer);

    const teilnehmer = {
      'kuerzel': 'sgdgqi',
      'teilnahmeart': 'P',
      'teilnahmekuerzel': 'ghdgdigw',
      'jahr': '2018',
      'klassenstufe': KLASSENSTUFE_2,
      'vorname': 'Horst'
    } as Teilnehmer;

    const result = getTeilnehmerToPrint(alleTeilnehmer);
    expect(result.length).toEqual(1);
  });
});