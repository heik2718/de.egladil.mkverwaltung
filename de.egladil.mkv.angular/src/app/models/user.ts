import { Schule } from './kataloge';
import { HateoasPayload } from './serverResponse';
import { ACTIVE, INACTIVE, PENDING } from '../core/app.constants';

export class Anmeldungsstatus {
  status: string;
  message: string;
  tooltip: string;
  cssClass: string;

  constructor(data: any = {}) {
    this.status = data.status;
    this.message = data.message;
    this.tooltip = data.tooltip;
    this.cssClass = data.cssClass;
  }
}

/**
 * Wettbewerbsanmeldungsarten
 */
const ANMELDUNG_AKTIV = new Anmeldungsstatus({
  'status': ACTIVE,
  'message': 'angemeldet',
  'tooltip': 'Sie sind zum aktuellen Wettbewerb angemeldet.',
  'cssClass': ['infoLabel', 'label', 'label-success']
});

const ANMELDUNG_INAKTIV = new Anmeldungsstatus({
  'status': INACTIVE,
  'message': 'nicht angemeldet',
  'tooltip': 'Sie sind noch nicht zum aktuellen Wettbewerb angemeldet.',
  'cssClass': ['infoLabel', 'label', 'label-warning']
});

const ANMELDUNG_PENDING = new Anmeldungsstatus({
  'status': PENDING,
  'message': 'nicht angemeldet',
  'tooltip': 'Der Anmeldezeitraum hat noch nicht begonnen.',
  'cssClass': ['infoLabel', 'label', 'label-default']
});



export interface Teilnehmer {
  kuerzel: string;
  teilnahmeart: string;
  teilnahmekuerzel: string;
  jahr: string;
  klassenstufe: string;
  klassenstufeLabel: string;
  vorname?: string;
  nachname?: string;
  zusatz?: string;
  loesungszettelkuerzel?: string;
}

export interface TeilnahmeIdentifier {
  kuerzel: string;
  jahr: string;
  teilnahmeart: string;
}


/**
 *
 */
export class Teilnahme {
  teilnahmeIdentifier: TeilnahmeIdentifier;
  aktuelle: boolean;
  schule?: Schule;
  anzahlUploads: number;
  anzahlTeilnehmer: number;
  anzahlLoesungszettel: number;
  kollegen?: string[];

  constructor(data: any = {}) {
    this.teilnahmeIdentifier = data && data.teilnahmeIdentifier || null;
    this.aktuelle = data && data.aktuelle || false;
    this.schule = data && data.schule || null;
    this.anzahlUploads = data && data.anzahlUploads || 0;
    this.anzahlTeilnehmer = data && data.anzahlTeilnehmer || 0;
    this.anzahlLoesungszettel = data && data.anzahlLoesungszettel || 0;
    this.kollegen = data && data.kollegen || null;
  }
}

/**
 *
 */
export interface Upload {
  teilnahmekuerzel: string;
  jahr: string;
  teilnahmeart: string;
}

interface PersonBasisdaten {
  vorname?: string;
  nachname?: string;
  email?: string;
  rolle?: string;
}

interface ErweiterteKontodaten {
  mailbenachrichtigung?: boolean;
  schule?: Schule;
  anzahlTeilnahmen: number;
  aktuelleTeilnahme?: Teilnahme;
  advVereinbarungVorhanden: boolean;
  schulwechselMoeglich: boolean;
}

export class User {

  hateoasPayload: HateoasPayload;
  basisdaten: PersonBasisdaten;
  erweiterteKontodaten?: ErweiterteKontodaten;

  constructor(data: any = {}) {
    this.hateoasPayload = data && data.hateoasPayload || null;
    this.basisdaten = data && data.basisdaten || null;
    this.erweiterteKontodaten = data && data.erweiterteKontodaten || null;
  }
}

const initialUser = new User({});

function createTeilnahmeIdentifier(pTeilnahmeart: string, pTeilnahmekuerzel: string, pJahr: string): TeilnahmeIdentifier {
  return {
    kuerzel: pTeilnahmekuerzel,
    jahr: pJahr,
    teilnahmeart: pTeilnahmeart
  } as TeilnahmeIdentifier;
}




export { initialUser, ANMELDUNG_AKTIV, ANMELDUNG_INAKTIV, ANMELDUNG_PENDING, createTeilnahmeIdentifier };


