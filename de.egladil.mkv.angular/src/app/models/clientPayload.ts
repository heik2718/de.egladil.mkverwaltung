/**
 * Enthält alls Objekte, die als Payload an den Server gesendet werden.
 */
export const REGISTRIERUNG_PRIVAT = 'privat';
export const REGISTRIERUNG_LEHRER = 'lehrer';


export interface LoginCredentials {
  username: string;
  password: string;
  kleber?: string;
}

export interface OrderTempCredentials {
  email: string;
  kleber?: string;
}

export interface TempPwdAendernCredentials {
  email: string;
  passwortNeu: string;
  passwortNeuWdh: string;
  tempPassword: string;
  type: string;
  kleber?: string;
}

export interface SchuleAnschriftKatalogantrag {
  type: string;
  email: string;
  landName?: string;
  name: string;
  plz: string;
  ort: string;
  strasse?: string;
  hausnummer?: string;
  kleber?: string;
}

export interface SchulzuordnungAendernPayload {
  kuerzelAlt: string;
  kuerzelNeu: string;
  anmelden: boolean;
}

export interface Privatregistrierung {
  type: string;
  vorname: string;
  nachname: string;
  email: string;
  passwort: string;
  passwortWdh: string;
  agbGelesen: boolean;
  automatischBenachrichtigen: boolean;
  gleichAnmelden: boolean;
  kleber?: string;
}

export interface Lehrerregistrierung extends Privatregistrierung {
  landkuerzel: string;
  ortkuerzel: string;
  schulkuerzel: string;
}

export interface Wettbewerbsanmeldung {
  jahr: string;
  benachrichtigen: boolean;
  rolle: string;
  schulkuerzel?: string;
}

export interface MailadresseAendernPayload {
  username: string;
  password: string;
  email: string;
  kleber?: string

}

export interface PersonAendernPayload {
  username: string;
  vorname: string;
  nachname: string;
  rolle: string;
  kleber?: string
}

export interface StatusPayload {
  neuerStatus: boolean;
  rolle: string;
}

export interface PasswortAendernPayload {
  type: string;
  email: string;
  passwort: string;
  passwortNeu: string;
  passwortNeuWdh: string;
  kleber?: string;
}

export interface TeilnehmerPayload {
  nameKlassenstufe: string;
  vorname: string;
  nachname?: string;
  zusatz?: string;
  loesungszettelkuerzel?: string;
  auswertungsgruppekuerzel?: string;
  apiversion: string;
}

export interface AuswertungsgruppePayload {
  nameKlassenstufe?: string;
  name: string;
}

export class AdvVereinbarungAntrag {
  schulkuerzel: string;
  schulname: string;
  laendercode: string;
  plz: string;
  ort: string;
  strasse: string;
  hausnummer: string;

  constructor(data: any = {}) {
    this.schulkuerzel = data && data.schulkuerzel || null;
    this.schulname = data && data.schulname || null;
    this.laendercode = data && data.laendercode || null;
    this.plz = data && data.plz || null;
    this.ort = data && data.ort || null;
    this.strasse = data && data.strasse || null;
    this.hausnummer = data && data.hausnummer || null;
  }

}




