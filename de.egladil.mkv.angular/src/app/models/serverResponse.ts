/**
 * APIMessage-Levels
 */
export const NULL = 'NULL';
export const INFO = 'INFO';
export const WARN = 'WARN';
export const ERROR = 'ERROR';
export const HATEOAS_KONTEXT_ADV = '[Hateoaskontext] adv';

/**
 * Objekt, das den statuscode eines http-Responses ergänzt.
 */
export class APIMessage {
  level: string;
  message?: string;

  constructor(level: string, message: string) {
    this.level = level;
    this.message = message;
  }
}

export class NullResponsePayload {}

export class SessionToken {
  accessToken?: string;
  xsrfToken: string;

  constructor(data: any = {}) {
    this.accessToken = data && data.accessToken || null;
    this.xsrfToken = data && data.xsrfToken || null;
  }
}
/**
 * Generischer API-Response bestehend aus einer APIMessage und einem Payload, das eins der Domain-Objekte ist.
 */
export class APIResponsePayload<T> {
  apiMessage: APIMessage;
  sessionToken: SessionToken;
  payload?: T;

  constructor(data: any = {}) {
    this.apiMessage = data && data.apiMessage || '';
    this.sessionToken = data && data.sessionToken || null;
    this.payload = data && data.payload || null;
    //    console.log(data.apiMessage);
  }
}


export interface InvalidProperty {
  name?: string;
  message?: string;
}

export class ConstraintViolationMessage {
  globalMessage?: string;
  crossValidationMessage?: string;
  invalidProperties?: InvalidProperty[];

  constructor(data: any = {}) {
    this.globalMessage = data.globalMessage;
    this.crossValidationMessage = data.crossValidationMessage;
    this.invalidProperties = data.invalidProperties;
  }
}



/**
 * Settings für MKV erhalten mittels Ping
 */
export class GlobalSettings {
  wettbewerbsjahr?: string;
  gueltigkeitEinmalpasswort?: number; // Anzahl Minuten, die ein Einmalpasswort gültig ist
  startAnmeldungText?: string;
  downloadFreigegebenPrivat?: boolean;
  datumFreigabePrivat?: string; // Datum der Freigabe der Unterlagen für Privatanmeldungen
  downloadFreigegebenLehrer?: boolean;
  datumFreigabeLehrer?: string; // Datum der Freigabe der Unterlagen für Lehrer
  neuanmeldungFreigegeben?: boolean;
  uploadDisabled?: boolean;
  wettbewerbBeendet?: boolean;
  xsrfToken?: string;
  ankuendigungWartungsmeldung?: boolean;
  wartungsmeldungText?: string;
  staticHtmlInfos?: DateiInfo[];

  constructor(data: any = {}) {
    this.wettbewerbsjahr = data.wettbewerbsjahr || 'jahr fehlt';
    this.gueltigkeitEinmalpasswort = data.gueltigkeitEinmalpasswort;
    this.startAnmeldungText = data.startAnmeldungText || '';
    this.downloadFreigegebenPrivat = data.downloadFreigegebenPrivat || false;
    this.datumFreigabePrivat = data.datumFreigabePrivat || null;
    this.downloadFreigegebenLehrer = data.downloadFreigegebenLehrer || false;
    this.datumFreigabeLehrer = data.datumFreigabeLehrer || null;
    this.neuanmeldungFreigegeben = data.neuanmeldungFreigegeben || false;
    this.uploadDisabled = data.uploadDisabled || true;
    this.xsrfToken = data.xsrfToken;
    this.ankuendigungWartungsmeldung = data.ankuendigungWartungsmeldung || false;
    this.wartungsmeldungText = data.wartungsmeldungText || '';
  }
}

export interface DateiInfo {
  kategorie: string; // aufgaben oder hilfsmittel
  dateiname: string;
  beschreibung: string;
  groesse: string;
  dateiUrl: string;
}

export class HateoasLink {
  method: string;
  url: string;
  mediatype: string;

  constructor(data: any = {}) {
    this.method = data && data.method || null;
    this.url = data && data.url || null;
    this.mediatype = data && data.mediatype || null;
  }
}

export class HateoasPayload {
  id: string;
  url: string;
  kontext: string;
  links: HateoasLink[];

  constructor(data: any = {}) {
    this.id = data && data.id || null;
    this.url = data && data.url || null;
    this.links = data.links || [];
  }
}


