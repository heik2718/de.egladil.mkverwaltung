export const LAND = 'LAND';
export const ORT = 'ORT';
export const SCHULE = 'SCHULE';

export class TreeNode {
  level: number;
  kuerzel: string;
  name: string;
  kinder: TreeNode[];

  constructor(data: any = {}) {
    if (data) {
      this.level = data.level;
      this.kuerzel = data.kuerzel;
      this.name = data.name;
      this.kinder = [];
    }
  }

  addAllKinder(data: any = []): void {
    data.forEach((item, index) => {
      const kind = new TreeNode(item);
      this.kinder.push(kind);
    });
  }
}

export class KatalogItem {
  level: number;
  kuerzel: string;
  name: string;
  kinder?: KatalogItem[];

  constructor(data: any = {}) {
    this.level = data.level;
    this.kuerzel = data.kuerzel;
    this.name = data.name;
    this.kinder = data.kinder !== undefined ? data.kinder : [];
  }
}

/**
 * dient dazu, den Schulkatalog zu storen.
 */
export interface KatalogMap {
  land: KatalogItem[];
  ort: KatalogItem[];
  schule: KatalogItem[];
}

/**
 * dient dazu, einzelne KatalogItems zu storen.
 */
export interface KatalogItemMap {
  land?: KatalogItem;
  ort?: KatalogItem;
  schule?: KatalogItem;
}

export class SchuleLage {
  landkuerzel: string;
  land: string;
  ortkuerzel: string;
  ort: string;

  constructor(data: any = {}) {
    this.landkuerzel = data && data.landkuerzel || null;
    this.land = data && data.land || null;
    this.ortkuerzel = data && data.ortkuerzel || null;
    this.ort = data && data.ort || null;
  }
}


export class Schule {
  kuerzel?: string;
  name?: string;
  lage?: SchuleLage;

  constructor(data: any = {}) {
    this.kuerzel = data && data.kuerzel || null;
    this.name = data && data.name || null;
    this.lage = data && data.lage || null;
  }
}

