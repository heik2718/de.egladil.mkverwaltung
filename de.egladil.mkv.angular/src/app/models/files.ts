
export type FileVerificationStatus = 'MAX_FILES_COUNT_EXCEED' | 'MAX_EACH_FILE_SIZE_EXCEED' | 'MAX_TOTAL_FILE_SIZE_EXCEED' | 'VERIFIED';

export interface FileUploadConfig {
	maxFileCount: number;
	maxEachFileSize?: number;
	maxTotalFileSize?: number;
}

export interface FileVericationResult {
    verificationSuccess: boolean;
    verificationStatus: FileVerificationStatus;
}

