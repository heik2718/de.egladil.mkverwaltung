
export const ANMELDEN = 'ANMELDEN';
export const TEILNAHMEN = 'TEILNAHMEN';
export const DOWNLOAD = 'DOWNLOAD';
export const AUSWERTUNG = 'AUSWERUNG';
export const TEILNEHMERUPLOAD = 'TEILNEHMERUPLOAD';
export const EXCELUPLOAD = 'EXCELUPLOAD';
export const PROFIL = 'PROFIL';


export interface OpportunityState {
  opportunity: string;
  visible: boolean;
  disabled: boolean;
  text: string;
}

export interface OpportunityStateMap {
  ANMELDEN: OpportunityState;
  TEILNAHMEN: OpportunityState;
  DOWNLOAD: OpportunityState;
  AUSWERTUNG: OpportunityState;
  TEILNEHMERUPLOAD: OpportunityState;
  EXCELUPLOAD: OpportunityState;
  PROFIL: OpportunityState;
}


export const OPPORTUNITY_ANMELDEN = {
  'opportunity': ANMELDEN,
  'visible': true,
  'disabled': true,
  'text': 'zum Thema anmelden'
} as OpportunityState;

export const OPPORTUNITY_TEILNAHMEN = {
  'opportunity': TEILNAHMEN,
  'visible': true,
  'disabled': false,
  'text': ''
} as OpportunityState;

export const OPPORTUNITY_DOWNLOAD = {
  'opportunity': DOWNLOAD,
  'visible': true,
  'disabled': true,
  'text': 'zum Thema download'
} as OpportunityState;

export const OPPORTUNITY_AUSWERTUNG = {
  'opportunity': AUSWERTUNG,
  'visible': true,
  'disabled': true,
  'text': 'zum Thema Auswertung'
} as OpportunityState;

export const OPPORTUNITY_TEILNEHMERUPLOAD = {
  'opportunity': TEILNEHMERUPLOAD,
  'visible': false,
  'disabled': true,
  'text': 'zum Thema teilnehmerupload'
} as OpportunityState;

export const OPPORTUNITY_EXCELUPLOAD = {
  'opportunity': EXCELUPLOAD,
  'visible': true,
  'disabled': true,
  'text': 'Zum Thema Excelupload'
} as OpportunityState;

export const OPPORTUNITY_PROFIL = {
  'opportunity': PROFIL,
  'visible': true,
  'disabled': false,
  'text': 'Zum Thema Profil'
} as OpportunityState;

