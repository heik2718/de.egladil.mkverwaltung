import {Injectable} from '@angular/core';
import {Store} from '../store';
import {StaticContentReducer} from '../reducers/staticContent.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class StaticContentStore extends Store<string> {

  constructor(protected logger: LogService) {
    super(logger);
    this._state = '';
    this._reducer = new StaticContentReducer();
  }
}