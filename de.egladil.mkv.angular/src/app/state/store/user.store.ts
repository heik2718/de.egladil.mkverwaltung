import { Injectable } from '@angular/core';
import { Store } from '../store';
import { User } from '../../models/user';
import { UserReducer } from '../reducers/user.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class UserStore extends Store<User> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('UserStore created');
    this._state = {} as User;
    this._reducer = new UserReducer(logger);
  }
}
