import { Injectable } from '@angular/core';
import { Store } from '../store';
import { Lehrerregistrierung, REGISTRIERUNG_LEHRER } from '../../models/clientPayload';
import { LehrerregistrierungReducer } from '../reducers/lehrerregistrierung.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class LehrerregistrierungStore extends Store<Lehrerregistrierung> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('LehrerregistrierungStore created');
    this._state = {} as Lehrerregistrierung;
    this._state.type = REGISTRIERUNG_LEHRER;
    this._reducer = new LehrerregistrierungReducer(logger);
  }
}