import {Injectable} from '@angular/core';
import {Store} from '../store';

import {DownloadCode} from '../../models/auswertung';

import {DownloadcodeReducer} from '../reducers/downloadcode.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class DownloadcodeStore extends Store<DownloadCode> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('DownloadcodeStore created');
    this._state = null;
    this._reducer = new DownloadcodeReducer();
  }
}