import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import {Teilnahme} from '../../models/user'
import {AktuelleTeilnameAction} from '../../state/actions';
import {AktuelleTeilnahmeReducer} from '../reducers/aktuelleTeilnahme.reducer';
import {ReplaySubject} from 'rxjs';

@Injectable()
export class AktuelleTeilnahmeStore {

  private _state: Teilnahme;
  stateSubject: ReplaySubject<Teilnahme>;
  private _reducer: AktuelleTeilnahmeReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<Teilnahme>(1);
    this._state = {} as Teilnahme;
    this._reducer = new AktuelleTeilnahmeReducer();
  }

  getState(): Teilnahme {
    return this._state;
  }

  dispatch(action: AktuelleTeilnameAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}