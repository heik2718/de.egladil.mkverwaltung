import { Injectable } from '@angular/core';
import { Store } from '../store';
import { SchuleurkundenauftragReducer } from '../reducers/schuleurkundenauftragReducer';
import { SchuleUrkundenauftrag } from '../../models/auswertung';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class SchuleurkundenauftragStore extends Store<SchuleUrkundenauftrag> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('GlobalSettingsStore created');
    this._state = null;
    this._reducer = new SchuleurkundenauftragReducer(logger);
  }
}