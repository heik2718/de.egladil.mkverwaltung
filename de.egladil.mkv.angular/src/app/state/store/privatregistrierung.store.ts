import { Injectable } from '@angular/core';
import { Store } from '../store';
import { Privatregistrierung, REGISTRIERUNG_PRIVAT } from '../../models/clientPayload';
import { PrivatregistrierungReducer } from '../reducers/privatregistrierung.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class PrivatregistrierungStore extends Store<Privatregistrierung> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('PrivatregistrierungStore created');
    this._state = {} as Privatregistrierung;
    this._state.type = REGISTRIERUNG_PRIVAT;
    this._reducer = new PrivatregistrierungReducer(logger);
  }
}