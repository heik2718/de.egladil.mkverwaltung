import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Store } from '../store';
import { AuswertungsgruppeReducer } from '../reducers/auswertungsgruppe.reducer';
import { Auswertungsgruppe } from '../../models/auswertung';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class AuswertungsgruppeStore extends Store<Auswertungsgruppe> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('AuswertungsgruppeStore created');
    this._state = null;
    this._reducer = new AuswertungsgruppeReducer(logger);
  }
}