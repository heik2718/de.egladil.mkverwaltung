import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import {Teilnahme} from '../../models/user';
import {TeilnahmenAction} from '../../state/actions';
import {TeilnahmenReducer} from '../reducers/teilnahmen.reducer';
import {ReplaySubject} from 'rxjs';

@Injectable()
export class TeilnahmenStore {

  private _state: Teilnahme[];
  stateSubject: ReplaySubject<Teilnahme[]>;
  private _reducer: TeilnahmenReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<Teilnahme[]>(1);
    this._state = [];
    this._reducer = new TeilnahmenReducer();
  }

  getState(): Teilnahme[] {
    return this._state;
  }

  dispatch(action: TeilnahmenAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}

