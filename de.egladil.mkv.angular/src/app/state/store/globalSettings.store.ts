import { Injectable } from '@angular/core';
import { Store } from '../store';
import { GlobalSettings } from '../../models/serverResponse';
import { GlobalSettingsReducer } from '../reducers/globalSettings.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class GlobalSettingsStore extends Store<GlobalSettings> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('GlobalSettingsStore created');
    this._state = new GlobalSettings({});
    this._reducer = new GlobalSettingsReducer(logger);
  }
}