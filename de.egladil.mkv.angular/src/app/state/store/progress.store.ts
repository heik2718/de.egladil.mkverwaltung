import { Injectable } from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import { Progress } from '../../models/progress';
import { ProgressAction } from '../../state/actions';
import { ProgressReducer } from '../reducers/progress.reducer';


import { ReplaySubject } from 'rxjs';

@Injectable()
export class ProgressStore {

  private _state: Progress;
  stateSubject: ReplaySubject<Progress>;
  private _reducer: ProgressReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<Progress>(1);
    this._state = {} as Progress;
    this._reducer = new ProgressReducer();
  }

  getState(): Progress {
    return this._state;
  }

  dispatch(action: ProgressAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}

