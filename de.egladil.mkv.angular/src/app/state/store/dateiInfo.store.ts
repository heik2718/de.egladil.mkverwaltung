import { Injectable } from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import { DateiInfo } from '../../models/serverResponse'
import { DateiInfoAction } from '../../state/actions';
import { DateiInfoReducer } from '../reducers/dateiInfo.reducer';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class DateiInfoStore {

  private _state: DateiInfo[];
  stateSubject: ReplaySubject<DateiInfo[]>;
  private _reducer: DateiInfoReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<DateiInfo[]>(1);
    this._state = [];
    this._reducer = new DateiInfoReducer();
  }

  getState(): DateiInfo[] {
    return this._state;
  }

  dispatch(action: DateiInfoAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}