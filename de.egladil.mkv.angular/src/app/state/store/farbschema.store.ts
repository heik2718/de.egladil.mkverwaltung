import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import {Farbschema} from '../../models/auswertung'
import {FarbschemaAction} from '../../state/actions';
import {FarbschemaReducer} from '../reducers/farbschema.reducer';
import {ReplaySubject} from 'rxjs';

@Injectable()
export class FarbschemaStore {

  private _state: Farbschema[];
  stateSubject: ReplaySubject<Farbschema[]>;
  private _reducer: FarbschemaReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<Farbschema[]>(1);
    this._state = [];
    this._reducer = new FarbschemaReducer();
  }

  getState(): Farbschema[] {
    return this._state;
  }

  dispatch(action: FarbschemaAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}

