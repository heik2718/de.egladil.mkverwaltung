import { Injectable } from '@angular/core';
import { Store } from '../store';
import { LogService } from 'hewi-ng-lib';
import { KatalogMap } from '../../models/kataloge';
import { KatalogMapReducer } from '../reducers/katalogmap.reducer';

@Injectable()
export class KatalogStore extends Store<KatalogMap> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('KatalogStore created');
    this._state = { 'land': [], 'ort': [], 'schule': [] } as KatalogMap;
    this._reducer = new KatalogMapReducer();
  }
}