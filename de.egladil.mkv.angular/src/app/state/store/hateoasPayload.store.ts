import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';

import {HateoasPayload} from '../../models/serverResponse';
import {HateoasMKVAction} from '../actions';
import {HateoasPayloadReducer} from '../reducers/hateoasPayload.reducer';


import {ReplaySubject} from 'rxjs';

@Injectable()
export class HateoasPayloadStore {

  private _state: HateoasPayload;
  stateSubject: ReplaySubject<HateoasPayload>;
  private _reducer: HateoasPayloadReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<HateoasPayload>(1);
    this._state = {} as HateoasPayload;
    this._reducer = new HateoasPayloadReducer();
  }

  getState(): HateoasPayload {
    return this._state;
  }

  dispatch(action: HateoasMKVAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}

