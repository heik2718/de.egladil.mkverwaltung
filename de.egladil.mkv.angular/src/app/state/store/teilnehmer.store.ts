import { Injectable } from '@angular/core';
import { Store } from '../store';
import { TeilehmerReducer } from '../reducers/teilnehmer.reducer';
import { Teilnehmer, Antwortbuchstabe } from '../../models/auswertung';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class TeilnehmerStore extends Store<Teilnehmer> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('TeilnehmerStore created');
    this._state = null;
    this._reducer = new TeilehmerReducer(logger);
  }


  getAntwortbuchstabe(index: number): Antwortbuchstabe {
    const t = this._state.antworten.filter(_antwort => _antwort.index === index)[0];
    return <Antwortbuchstabe>(Object.assign({}, t));
  }
}