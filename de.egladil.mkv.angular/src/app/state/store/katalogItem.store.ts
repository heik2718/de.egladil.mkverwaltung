import {Injectable} from '@angular/core';
import {Store} from '../store';
import { LogService } from 'hewi-ng-lib';
import {KatalogItemMap} from '../../models/kataloge';
import {KatalogItemReducer} from '../reducers/katalogitem.reducer';

@Injectable()
export class KatalogItemStore extends Store<KatalogItemMap> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('KatalogItemStore created');
    this._state = {'land': null, 'ort': null, 'schule': null};
    this._reducer = new KatalogItemReducer();
  }
}