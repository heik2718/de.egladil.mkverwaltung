import { Injectable } from '@angular/core';
import { Store } from '../store';
import { StaticHtmlReducer } from '../reducers/staticHtml.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class StaticHtmlStore extends Store<string> {

  constructor(protected logger: LogService) {
    super(logger);
    this._state = '';
    this._reducer = new StaticHtmlReducer();
  }
}