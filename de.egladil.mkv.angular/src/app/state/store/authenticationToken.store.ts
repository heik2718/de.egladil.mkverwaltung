import { Injectable } from '@angular/core';
import { Store } from '../store';
import { SessionToken } from '../../models/serverResponse';
import { AuthenticationTokenReducer } from '../reducers/authenticationToken.reducer';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class AuthenticationTokenStore extends Store<SessionToken> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('AuthenticationTokenStore created');
    this._state = { xsrfToken: null, accessToken: null } as SessionToken;
    this._reducer = new AuthenticationTokenReducer();
  }

  isAuthenticated() {
    const authToken = (<SessionToken>this.getState());
    this.logger.debug('authToken=' + authToken);
    return authToken !== null && authToken !== undefined;
  }
}