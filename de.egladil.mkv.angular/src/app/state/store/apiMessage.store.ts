import { Injectable } from '@angular/core';
import { Store } from '../store';
import { MessageReducer } from '../reducers/message.reducer';
import { LogService } from 'hewi-ng-lib';
import { APIMessage, INFO } from '../../models/serverResponse';

@Injectable()
export class ApiMessageStore extends Store<APIMessage> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('ApiMessageStore created');
    this._state = new APIMessage(INFO, null);
    this._reducer = new MessageReducer();
  }
}