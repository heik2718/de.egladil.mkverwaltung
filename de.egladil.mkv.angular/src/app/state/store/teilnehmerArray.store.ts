import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Teilnehmer } from '../../models/auswertung';
import { TeilnehmerStore } from '../../state/store/teilnehmer.store';
import { TEILNEHMER_ERFASST, TEILNEHMER_GEAENDERT, TEILNEHMER_GELADEN, TEILNEHMER_GELOESCHT, CLEAR, TeilnehmerArrayAction, ActionFactory } from '../actions';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class TeilnehmerArrayStore {

  private teilnehmer: Teilnehmer[] = [];
  items = new ReplaySubject<Teilnehmer[]>();

  constructor(private logger: LogService, private teilnehmerStore: TeilnehmerStore) { }

  dispatch(action: TeilnehmerArrayAction) {
    //    this.logger.debug('dispatched ' + JSON.stringify(action));
    this.teilnehmer = this._reduce(this.teilnehmer, action);
    this.items.next(this.teilnehmer);
  }

  _reduce(teilnehmer: Teilnehmer[], action: TeilnehmerArrayAction): Teilnehmer[] {
    switch (action.type) {
      case TEILNEHMER_ERFASST:
        const neuer = action.alleTeilnehmer[0];
        this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_GEAENDERT, neuer));
        return [...teilnehmer, neuer];
      case TEILNEHMER_GEAENDERT:
        return teilnehmer.map(t => {
          const editedTeilnehmer = action.alleTeilnehmer[0];
          if (t.kuerzel !== editedTeilnehmer.kuerzel) {
            return t;
          }
          this.teilnehmerStore.dispatch(ActionFactory.selectedTeilnehmerAction(TEILNEHMER_GEAENDERT, editedTeilnehmer));
          return editedTeilnehmer;
        });
      case TEILNEHMER_GELOESCHT:
        if (action.kuerzel !== null) {
          return teilnehmer.filter(t => t.kuerzel !== action.kuerzel);
        }
        return [];
      case TEILNEHMER_GELADEN:
        return action.alleTeilnehmer;
      case CLEAR:
        return [];
      default: return teilnehmer;
    }
  }

  getTeilnehmer(kuerzel: string): Teilnehmer {
    const t = this.teilnehmer.filter(_teilnehmer => _teilnehmer.kuerzel === kuerzel)[0];
    return <Teilnehmer>(Object.assign({}, t));
  }
}