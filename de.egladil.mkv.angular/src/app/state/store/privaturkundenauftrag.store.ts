import {Injectable} from '@angular/core';
import {Store} from '../store';
import {PrivaturkundenauftragReducer} from '../reducers/privaturkundenauftragReducer';
import {TeilnehmerUndFarbschema} from '../../models/auswertung';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class PrivaturkundenauftragStore extends Store<TeilnehmerUndFarbschema> {

  constructor(protected logger: LogService) {
    super(logger);
    this.logger.debug('GlobalSettingsStore created');
    this._state = null;
    this._reducer = new PrivaturkundenauftragReducer(logger);
  }
}