import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import {Schule} from '../../models/kataloge';
import {SchuleAction} from '../../state/actions';
import {SchuleReducer} from '../reducers/schule.reducer';


import {ReplaySubject} from 'rxjs';

@Injectable()
export class SchuleStore {

  private _state: Schule;
  stateSubject: ReplaySubject<Schule>;
  private _reducer: SchuleReducer;

  constructor(protected logger: LogService) {
    this.logger.debug('DateiInfoStore created');
    this.stateSubject = new ReplaySubject<Schule>(1);
    this._state = {} as Schule;
    this._reducer = new SchuleReducer();
  }

  getState(): Schule {
    return this._state;
  }

  dispatch(action: SchuleAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}

