import {APIMessage} from '../../models/serverResponse';
import {Reducer} from '../store';
import {MKVAction, StringAction, ADD, CLEAR} from '../actions';

export class StaticHtmlReducer implements Reducer<string> {

  reduce(state: string, action: MKVAction): string {
    switch (action.type) {
      case ADD:
        const act = (<StringAction>action);
        return act.str;
      case CLEAR:
        return '';
      default:
        return state;
    }
  }
}
