import {DateiInfo} from '../../models/serverResponse';
import {Reducer} from '../store';
import {DateiInfoAction, LOAD_KATALOG} from '../actions';

export class DateiInfoReducer implements Reducer<DateiInfo[]> {

  reduce(state: DateiInfo[], action: DateiInfoAction): DateiInfo[] {
//    console.log('inside DateiInfoReducer.reduce');
    switch (action.type) {
      case LOAD_KATALOG:
        return [...action.payload];
      default:
        return state;
    }
  }
}
