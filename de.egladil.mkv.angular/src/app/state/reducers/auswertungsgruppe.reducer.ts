import { Auswertungsgruppe } from '../../models/auswertung';
import { Reducer } from '../store';
import { MKVAction, LOAD, ADD, CLEAR, AuswertungsgruppeAction } from '../actions';
import { LogService } from 'hewi-ng-lib';

export class AuswertungsgruppeReducer implements Reducer<Auswertungsgruppe> {

  constructor(private logger: LogService) { }

  reduce(state: Auswertungsgruppe, action: MKVAction): Auswertungsgruppe {
    this.logger.debug('AuswertungsgruppeReducer');
    const theGruppe = (<AuswertungsgruppeAction>action).gruppe;
    switch (action.type) {
      case ADD:
      case LOAD:
        return Object.assign({}, state, theGruppe);
      case CLEAR:
        return null;
      default:
        return state;
    }
  }
}
