import {GlobalSettings} from '../../models/serverResponse';
import {Store, Reducer} from '../store';
import {AUTH, MKVAction, PingGlobalSettingsAction, AuthenticationTokenAction, PING, STATIC_HTML, DateiInfoAction} from '../actions';
import { LogService } from 'hewi-ng-lib';


export class GlobalSettingsReducer implements Reducer<GlobalSettings> {

  constructor(private logger: LogService) {}

  reduce(state: GlobalSettings, action: MKVAction): GlobalSettings {
    switch (action.type) {
      case PING:
        this.logger.debug('reduce ' + JSON.stringify(action));
        return Object.assign({}, state, (<PingGlobalSettingsAction>action).globalSettings);
      case STATIC_HTML:
        const dateiinfos = (<DateiInfoAction>action).payload;
        return Object.assign({}, state, {staticHtmlInfos: dateiinfos && dateiinfos || []});
      case AUTH:
        const token = (<AuthenticationTokenAction>action).token;
        return Object.assign({}, state, {xsrfToken: token.xsrfToken || null});
      default:
        return state;
    }
  }
}
