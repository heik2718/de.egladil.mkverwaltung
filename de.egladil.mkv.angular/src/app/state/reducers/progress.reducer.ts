import {Progress} from '../../models/progress';
import {ProgressAction, ADD, CLEAR} from '../actions';
import {Reducer} from '../store';

export class ProgressReducer implements Reducer<Progress> {

  reduce(state: Progress, action: ProgressAction): Progress {
    switch (action.type) {
      case ADD:
        state = action.progress;
        return state;
      case CLEAR:
        state = null;
        break;
      default:
        return state;
    }
  }
}
