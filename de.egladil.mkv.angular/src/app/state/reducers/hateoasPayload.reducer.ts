import {HateoasPayload} from '../../models/serverResponse';
import {HateoasMKVAction, ADD, CLEAR} from '../actions';
import {Reducer} from '../store';

export class HateoasPayloadReducer implements Reducer<HateoasPayload> {

  reduce(state: HateoasPayload, action: HateoasMKVAction): HateoasPayload {
    switch (action.type) {
      case ADD:
        state = action.payload;
        return state;
      case CLEAR:
        state = null;
        break;
      default:
        return state;
    }
  }
}
