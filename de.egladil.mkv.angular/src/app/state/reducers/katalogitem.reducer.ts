import {KatalogItem, KatalogItemMap, LAND, ORT, SCHULE} from '../../models/kataloge';
import {Reducer} from '../store';
import {MKVAction, SELECT, DESELECT, CACHE, CLEAR, KatalogItemAction, CacheSchulauswahlAction} from '../actions';


export class KatalogItemReducer implements Reducer<KatalogItemMap> {

  reduce(state: KatalogItemMap, action: MKVAction): KatalogItemMap {
//    console.log(JSON.stringify(action));
    switch (action.type) {
      case SELECT:
        return this.selectItem(state, (<KatalogItemAction>action).item, (<KatalogItemAction>action).key);
      case DESELECT:
        return this.deslectItem(state, (<KatalogItemAction>action).item, (<KatalogItemAction>action).key);
      case CACHE:
//        console.log('cache');
        return (<CacheSchulauswahlAction>action).payload;
      case CLEAR:
        return {'land': null, 'ort': null, 'schule': null} as KatalogItemMap;
      default: return state;
    }
  }

  private selectItem(theState: KatalogItemMap, item: KatalogItem, key: string): KatalogItemMap {
    switch (key) {
      case LAND:
        return {
          'land': item,
          'ort': null,
          'schule': null
        }
      case ORT:
        return {
          'land': theState.land,
          'ort': item,
          'schule': null
        }
      case SCHULE:
        return {
          'land': theState.land,
          'ort': theState.ort,
          'schule': item
        }
      default: return theState;
    }
  }

  private deslectItem(theState: KatalogItemMap, item: KatalogItem, key: string): KatalogItemMap {
//    console.log('deselect');
    switch (key) {
      case LAND:
        return {
          'land': null,
          'ort': null,
          'schule': null
        }
      case ORT:
        return {
          'land': theState.land,
          'ort': null,
          'schule': null
        }
      case SCHULE:
        return {
          'land': theState.land,
          'ort': theState.ort,
          'schule': null
        }
      default: return theState;
    }
  }
}