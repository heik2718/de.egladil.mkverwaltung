import { TeilnehmerUndFarbschema } from '../../models/auswertung';
import { Reducer } from '../store';
import { MKVAction, TeilnehmerUrkundenAction, ADD, CLEAR } from '../actions';
import { LogService } from 'hewi-ng-lib';

export class PrivaturkundenauftragReducer implements Reducer<TeilnehmerUndFarbschema> {

  constructor(private logger: LogService) { }

  reduce(state: TeilnehmerUndFarbschema, action: MKVAction): TeilnehmerUndFarbschema {
    this.logger.debug('TeilnehmerUrkundenauftrag');
    const auftrag = (<TeilnehmerUrkundenAction>action).auftrag;
    switch (action.type) {
      case ADD:
        return Object.assign({}, state, auftrag);
      case CLEAR:
        return null;
      default:
        return state;
    }
  }
}
