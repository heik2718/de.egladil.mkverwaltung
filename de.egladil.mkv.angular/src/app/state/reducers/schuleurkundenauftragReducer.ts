import { SchuleUrkundenauftrag } from '../../models/auswertung';
import { Reducer } from '../store';
import { MKVAction, SchuleUrkundenAction, ADD, CLEAR } from '../actions';
import { LogService } from 'hewi-ng-lib';

export class SchuleurkundenauftragReducer implements Reducer<SchuleUrkundenauftrag> {

  constructor(private logger: LogService) { }

  reduce(state: SchuleUrkundenauftrag, action: MKVAction): SchuleUrkundenauftrag {
    this.logger.debug('TeilnehmerUrkundenauftrag');
    const auftrag = (<SchuleUrkundenAction>action).auftrag;
    switch (action.type) {
      case ADD:
        return Object.assign({}, state, auftrag);
      case CLEAR:
        return null;
      default:
        return state;
    }
  }
}
