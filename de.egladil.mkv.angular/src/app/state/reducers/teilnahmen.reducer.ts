import {Reducer} from '../store';
import {Teilnahme} from '../../models/user';
import {TeilnahmenAction, LOAD, ADD, CLEAR} from '../actions';

export class TeilnahmenReducer implements Reducer<Teilnahme[]> {

  reduce(state: Teilnahme[], action: TeilnahmenAction): Teilnahme[] {
    switch (action.type) {
      case LOAD:
        return [...action.payload];
       case ADD:
        return [action.payload[0], ...state];
       case CLEAR:
           return [];
      default:
        return state;
    }
  }
}
