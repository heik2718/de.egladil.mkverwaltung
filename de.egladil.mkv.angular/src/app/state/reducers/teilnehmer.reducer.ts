import { Teilnehmer } from '../../models/auswertung';
import { Reducer } from '../store';
import { MKVAction, SelectedTeilnehmerAction, TEILNEHMER_SELECT, TEILNEHMER_RELEASE, TEILNEHMER_GEAENDERT } from '../actions';
import { LogService } from 'hewi-ng-lib';

export class TeilehmerReducer implements Reducer<Teilnehmer> {

  constructor(private logger: LogService) { }

  reduce(state: Teilnehmer, action: MKVAction): Teilnehmer {
    this.logger.debug('TeilnehmerReducer');
    const teilnehmer = (<SelectedTeilnehmerAction>action).theTeilnehmer;
    switch (action.type) {
      case TEILNEHMER_SELECT:
      case TEILNEHMER_GEAENDERT:
        return teilnehmer;
      case TEILNEHMER_RELEASE:
        return null;
      default:
        return state;
    }
  }
}
