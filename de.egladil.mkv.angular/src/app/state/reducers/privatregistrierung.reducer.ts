import { Reducer } from '../store';
import { Privatregistrierung, REGISTRIERUNG_PRIVAT } from '../../models/clientPayload';
import { MKVAction, CachePrivatregistrierungAction, CLEAR, CACHE } from '../actions';
import { LogService } from 'hewi-ng-lib';


export class PrivatregistrierungReducer implements Reducer<Privatregistrierung> {

  constructor(private logger: LogService) { }

  reduce(state: Privatregistrierung, action: MKVAction): Privatregistrierung {
    switch (action.type) {
      case CACHE:
        this.logger.debug('reduce ' + JSON.stringify(action));
        return Object.assign({}, state, (<CachePrivatregistrierungAction>action).payload);
      case CLEAR:
        const reg = {} as Privatregistrierung;
        reg.type = REGISTRIERUNG_PRIVAT;
        return reg;
      default:
        return state;
    }
  }
}
