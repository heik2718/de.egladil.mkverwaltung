import {APIMessage} from '../../models/serverResponse';
import {Reducer} from '../store';
import {MKVAction, SetAPIMessageAction, MESSAGE, CLEAR} from '../actions';

export class MessageReducer implements Reducer<APIMessage> {

  reduce(state: APIMessage, action: MKVAction): APIMessage {
    switch (action.type) {
      case MESSAGE:
        return Object.assign({}, state, (<SetAPIMessageAction>action).message);
      case CLEAR:
        return {
          level: 'NULL',
          message: ''
        };
      default:
        return state;
    }
  }
}
