import {Schule} from '../../models/kataloge';
import {SchuleAction, LOAD, CLEAR} from '../actions';
import {Reducer} from '../store';

export class SchuleReducer implements Reducer<Schule> {

  reduce(state: Schule, action: SchuleAction): Schule {
    switch (action.type) {
      case LOAD:
        state = action.schule;
        return state;
      case CLEAR:
        state = null;
        break;
      default:
        return state;
    }
  }
}
