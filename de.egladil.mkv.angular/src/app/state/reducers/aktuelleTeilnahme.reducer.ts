import {Teilnahme} from '../../models/user';
import {AktuelleTeilnameAction, LOAD, CLEAR, ADD, UPDATE} from '../actions';
import {Reducer} from '../store';

export class AktuelleTeilnahmeReducer implements Reducer<Teilnahme> {

  reduce(state: Teilnahme, action: AktuelleTeilnameAction): Teilnahme {
    //    console.log('inside DateiInfoReducer.reduce');
    switch (action.type) {
      case ADD:
      case LOAD:
      case UPDATE:
        //        console.log('LOGINLOGOUT');
        state = action.teilnahme;
        return state;
      case CLEAR:
        state = null;
        break;
      default:
        return state;
    }
  }
}
