import {APIMessage} from '../../models/serverResponse';
import {Reducer} from '../store';
import {MKVAction, DownloadCodeAction, ADD, CLEAR} from '../actions';
import {DownloadCode} from '../../models/auswertung';

export class DownloadcodeReducer implements Reducer<DownloadCode> {

  reduce(state: DownloadCode, action: DownloadCodeAction): DownloadCode {
    switch (action.type) {
      case ADD:
        return action.downloadCode;
      case CLEAR:
        return null;
      default:
        return state;
    }
  }
}
