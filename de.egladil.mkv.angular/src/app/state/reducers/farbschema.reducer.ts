import {Farbschema} from '../../models/auswertung';
import {Reducer} from '../store';
import {FarbschemaAction, LOAD} from '../actions';

export class FarbschemaReducer implements Reducer<Farbschema[]> {

  reduce(state: Farbschema[], action: FarbschemaAction): Farbschema[] {
    switch (action.type) {
      case LOAD:
        return [...action.payload];
      default:
        return state;
    }
  }
}
