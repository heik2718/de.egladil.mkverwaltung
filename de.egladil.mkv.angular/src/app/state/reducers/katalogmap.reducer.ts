import {KatalogItem, KatalogMap, LAND, ORT, SCHULE} from '../../models/kataloge';
import {Reducer} from '../store';
import {MKVAction, LOAD_KATALOG, CLEAR_KATALOG, DESELECT, CACHE, CLEAR, CacheKatalogAction, KatalogAction} from '../actions';


export class KatalogMapReducer implements Reducer<KatalogMap> {

  reduce(state: KatalogMap, action: MKVAction): KatalogMap {
    //    console.log('reduce KatalogMap');
    switch (action.type) {
      case LOAD_KATALOG:
        return this.loadKatalog(state, (<KatalogAction>action).items, (<KatalogAction>action).key);
      case CLEAR_KATALOG:
        return this.clearKatalog(state, (<KatalogAction>action).items, (<KatalogAction>action).key);
      case CACHE:
//        console.log('cache');
        return (<CacheKatalogAction>action).payload;
      case CLEAR:
        return {'land': state.land, 'ort': [], 'schule': []} as KatalogMap;
      default: return state;
    }
  }

  private loadKatalog(theState: KatalogMap, items: KatalogItem[], key: string): KatalogMap {
    switch (key) {
      case LAND:
        return {
          'land': items,
          'ort': [],
          'schule': []
        }
      case ORT:
        return {
          'land': theState.land,
          'ort': items,
          'schule': []
        }
      case SCHULE:
        return {
          'land': theState.land,
          'ort': theState.ort,
          'schule': items
        }
      default: return theState;
    }
  }

  private clearKatalog(theState: KatalogMap, items: KatalogItem[], key: string): KatalogMap {
    switch (key) {
      case LAND:
        return {
          'land': [],
          'ort': [],
          'schule': []
        }
      case ORT:
        return {
          'land': theState.land,
          'ort': [],
          'schule': []
        }
      case SCHULE:
        return {
          'land': theState.land,
          'ort': theState.ort,
          'schule': []
        }
      default: return theState;
    }
  }
}