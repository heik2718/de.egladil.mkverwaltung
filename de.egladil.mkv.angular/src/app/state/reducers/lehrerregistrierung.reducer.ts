import { Reducer } from '../store';
import { Lehrerregistrierung, REGISTRIERUNG_LEHRER } from '../../models/clientPayload';
import { MKVAction, CachePrivatregistrierungAction, CLEAR, CACHE } from '../actions';
import { LogService } from 'hewi-ng-lib';


export class LehrerregistrierungReducer implements Reducer<Lehrerregistrierung> {

  constructor(private logger: LogService) { }

  reduce(state: Lehrerregistrierung, action: MKVAction): Lehrerregistrierung {
    switch (action.type) {
      case CACHE:
        this.logger.debug('reduce ' + JSON.stringify(action));
        return Object.assign({}, state, (<CachePrivatregistrierungAction>action).payload);
      case CLEAR:
        const reg = {} as Lehrerregistrierung;
        reg.type = REGISTRIERUNG_LEHRER;
        return reg;
      default:
        return state;
    }
  }
}
