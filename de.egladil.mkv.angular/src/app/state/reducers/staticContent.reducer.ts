import {APIMessage} from '../../models/serverResponse';
import {Reducer} from '../store';
import {MKVAction, StringAction, STATIC_AGB, STATIC_KONTAKT, STATIC_ZUGANGSDATEN, CLEAR, REDIRECTED_FROM_URL, STATIC_HTML} from '../actions';

export class StaticContentReducer implements Reducer<string> {

  reduce(state: string, action: MKVAction): string {
    switch (action.type) {
      case STATIC_AGB:
      case STATIC_KONTAKT:
      case STATIC_ZUGANGSDATEN:
        return action.type;
      case REDIRECTED_FROM_URL:
      case STATIC_HTML:
        const act = (<StringAction>action);
        return act.str;
      case CLEAR:
        return null;
      default:
        return state;
    }
  }
}
