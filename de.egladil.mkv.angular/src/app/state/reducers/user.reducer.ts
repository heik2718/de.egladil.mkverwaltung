import { User, initialUser } from '../../models/user';
import { Reducer } from '../store';
import { MKVAction, AuthUserAction, LOGIN, LOGOUT, PERSON_NAME, PERSON_NEWSLETTER, UPDATE_AKT_TEILNAHME, UPDATE } from '../actions';
import { LogService } from 'hewi-ng-lib';

export class UserReducer implements Reducer<User> {

  constructor(private logger: LogService) { }

  reduce(state: User, action: MKVAction): User {
    this.logger.debug('UserReducer');
    const fromServer = (<AuthUserAction>action).user;
    switch (action.type) {
      case LOGIN:
        const result = Object.assign({}, state, fromServer);
        return result;
      case PERSON_NAME:
        const neuerUser = Object.assign({}, new User({}), state);
        neuerUser.basisdaten.vorname = fromServer.basisdaten.vorname;
        neuerUser.basisdaten.nachname = fromServer.basisdaten.nachname;
        return neuerUser;
      case PERSON_NEWSLETTER:
        const neuNewsletter = Object.assign({}, new User({}), state);
        neuNewsletter.erweiterteKontodaten.mailbenachrichtigung = fromServer.erweiterteKontodaten.mailbenachrichtigung;
        return neuNewsletter;
      case UPDATE_AKT_TEILNAHME:
        const neuTeilnahmen = Object.assign({}, new User({}), state);
        neuTeilnahmen.erweiterteKontodaten.aktuelleTeilnahme = fromServer.erweiterteKontodaten.aktuelleTeilnahme;
        return neuTeilnahmen;
      case UPDATE:
        return fromServer;
      case LOGOUT:
        return initialUser;
      default:
        return state;
    }
  }
}
