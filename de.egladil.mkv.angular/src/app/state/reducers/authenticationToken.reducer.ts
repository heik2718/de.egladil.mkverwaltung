import {SessionToken} from '../../models/serverResponse';
import {Reducer} from '../store';
import {MKVAction, AuthenticationTokenAction, AUTH} from '../actions';

export class AuthenticationTokenReducer implements Reducer<SessionToken> {

  reduce(state: SessionToken, action: MKVAction): SessionToken {
    switch (action.type) {
      case AUTH:
        return Object.assign({}, state, (<AuthenticationTokenAction>action).token);
      default:
        return state;
    }
  }
}
