import { MKVAction } from './actions';
import { LogService } from 'hewi-ng-lib';
import { ReplaySubject } from 'rxjs';

export class Store<T> {

  protected _state: T;
  protected _reducer: Reducer<T>;

  stateSubject: ReplaySubject<T>;

  constructor(protected logger: LogService) {
    this.stateSubject = new ReplaySubject<T>(1);
  }

  getState(): T {
    return this._state;
  }

  dispatch(action: MKVAction): void {
    this._state = this._reducer.reduce(this._state, action);
    this.stateSubject.next(this._state);
  }
}

export interface Reducer<T> {
  reduce(state: T, action: MKVAction): T;
}

