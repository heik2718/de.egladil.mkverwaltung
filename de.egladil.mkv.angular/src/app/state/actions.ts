import {Teilnehmer, Antwortbuchstabe, TeilnehmerUndFarbschema, Auswertungsgruppe, SchuleUrkundenauftrag, Farbschema} from '../models/auswertung';
import {Schule} from '../models/kataloge';
import {User, Teilnahme} from '../models/user';
import {GlobalSettings, APIMessage, HateoasPayload} from '../models/serverResponse';
import {Privatregistrierung, Lehrerregistrierung} from '../models/clientPayload';
import {SessionToken} from '../models/serverResponse';
import {KatalogItem, KatalogItemMap, KatalogMap} from '../models/kataloge';
import {DateiInfo} from '../models/serverResponse';
import {DownloadCode} from '../models/auswertung';
import {Progress} from '../models/progress';

export const PING = 'PING';
export const AUTH = 'AUTH';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const PERSON_NAME = 'PERSON_NAME';
export const PERSON_NEWSLETTER = 'PERSON_NEWSLTTER';
export const MESSAGE = 'MESSAGE';
export const CLEAR = 'CLEAR';
export const LOAD = 'LOAD';
export const ADD = 'ADD';
export const UPDATE = 'UPDATE';
export const LOAD_KATALOG = 'LOAD_KATALOG';
export const CLEAR_KATALOG = 'CLEAR_KATALOG';
export const SELECT = 'SELECT';
export const DESELECT = 'DESELECT';
export const STATIC_KONTAKT = 'static contents kontakt';
export const STATIC_AGB = 'static contents agb';
export const STATIC_HTML = 'static contents html';
export const REDIRECTED_FROM_URL = 'redirected from url';
export const STATIC_ZUGANGSDATEN = 'static contents zugangsdaten';
export const CACHE = 'cache';
export const START = 'start';
export const STOP = 'stop';
//export const EXCEL_UPLOAD = 'excel hochgeladen';
export const TEILNEHMER_ERFASST = 'Teilnehmer erfasst';
export const TEILNEHMER_GEAENDERT = 'Teilnehmer geändert';
export const TEILNEHMER_GELADEN = 'Teilnehmer geladen';
export const TEILNEHMER_GELOESCHT = 'Teilnehmer gelöscht';
export const UPDATE_AKT_TEILNAHME = 'aktuelle Teilnahme aktualisiert';
export const AUSWERTUNGSGRUPPE_ERFASST = 'Auswertungsgruppe erfasst';
export const AUSWERTUNGSGRUPPE_GEAENDERT = 'Auswertungsgruppe geändert';
export const ANTWORTBUCHSTABE_EINGEGEBEN = 'Antwortbuchstabe angelegt';
export const ANTWORTBUCHSTABE_GEAENDERT = 'Antwortbuchstabe geändert';
export const RESET_ANTWORTBUCHTABEN = 'reset Antwortbuchstaben'
export const LOAD_ANTWORTBUCHSTABEN = 'load Antwortbuchstaben';
export const TEILNEHMER_SELECT = 'select Teilnehmer';
export const TEILNEHMER_RELEASE = 'release Teilnehmer';


export interface MKVAction {
  type: string;
}

export interface AktuelleTeilnameAction extends MKVAction {
  teilnahme: Teilnahme;
}

export interface SchuleAction extends MKVAction {
  schule: Schule;
}

export interface ProgressAction extends MKVAction {
  progress: Progress;
}

export interface HateoasMKVAction extends MKVAction {
  payload: HateoasPayload;
}

export interface DownloadCodeAction extends MKVAction {
  downloadCode: DownloadCode;
}

export interface StringAction extends MKVAction {
  str: string;
}

export interface PingGlobalSettingsAction extends MKVAction {
  globalSettings: GlobalSettings;
}

export interface CachePrivatregistrierungAction extends MKVAction {
  payload: Privatregistrierung;
}

export interface AuthUserAction extends MKVAction {
  user: User;
}

export interface SetAPIMessageAction extends MKVAction {
  message: APIMessage;
}

export interface AuthenticationTokenAction extends MKVAction {
  token: SessionToken;
}

export interface CacheSchulauswahlAction extends MKVAction {
  payload: KatalogItemMap;
}

export interface DateiInfoAction extends MKVAction {
  payload: DateiInfo[];
}

export interface FarbschemaAction extends MKVAction {
  payload: Farbschema[];
}

export interface TeilnahmenAction extends MKVAction {
  payload: Teilnahme[];
}

export interface CacheKatalogAction extends MKVAction {
  payload: KatalogMap;
}

export interface KatalogAction extends MKVAction {
  key: string;
  items: KatalogItem[];
}

export interface KatalogItemAction extends MKVAction {
  key: string;
  item: KatalogItem;
}

export interface ReplaceStringAction extends MKVAction {
  wert: string;
}

export interface TeilnehmerArrayAction extends MKVAction {
  alleTeilnehmer: Teilnehmer[];
  kuerzel?: string;
  geladen: boolean;
}

export interface SelectedTeilnehmerAction extends MKVAction {
  theTeilnehmer: Teilnehmer;
}

export interface AntwortbuchstabenAction extends MKVAction {
  alleEingaben: Antwortbuchstabe[];
}

export interface TeilnehmerUrkundenAction extends MKVAction {
  auftrag: TeilnehmerUndFarbschema;
}

export interface SchuleUrkundenAction extends MKVAction {
  auftrag: SchuleUrkundenauftrag;
}

export interface AuswertungsgruppeAction extends MKVAction {
  gruppe: Auswertungsgruppe;
}

/**
 * Generiert die einzelnen Actions
 */
export class ActionFactory {

  static actionOhnePayload(actionType: string) {
    return {
      type: actionType
    } as MKVAction;
  }

  static pingAction(settings: GlobalSettings) {
    return {
      type: PING,
      globalSettings: settings
    };
  }

  static cacheSchulauswahlAction(selectedItems: KatalogItemMap) {
    return {
      type: CACHE,
      payload: selectedItems
    };
  }

  static cacheKatalogeAction(kataloge: KatalogMap) {
    return {
      type: CACHE,
      payload: kataloge
    };
  }

  static cachePrivatregistrierungAction(registrierung: Privatregistrierung) {
    return {
      type: CACHE,
      payload: registrierung
    };
  }

  static cacheLehrerregistrierungAction(registrierung: Lehrerregistrierung) {
    return {
      type: CACHE,
      payload: registrierung
    };
  }

  static loginUserAction(user: User) {
    return {
      type: LOGIN,
      user: user
    };
  }


  static updateUserAction(user: User) {
    return {
      type: UPDATE,
      user: user
    };
  }

  static logoutUserAction() {
    return {
      type: LOGOUT
    };
  }

  static apiMessageAction(message: APIMessage) {
    return {
      type: MESSAGE,
      message: message
    };
  }

  static authenticationTokenAction(token: SessionToken) {
    return {
      type: AUTH,
      token: token
    };
  }

  static replaceStringAction(actionType: string, value: string) {
    return {
      type: actionType,
      str: value
    };
  }

  /**
   * theKey ist dabei LAND, ORT oder SCHULE und die Action dient dazu, im store das Element mit dem key auszutauschen
   */
  static katalogItemAction(actionType: string, theKey: string, theItem: KatalogItem) {
    return {
      type: actionType,
      key: theKey,
      item: theItem
    };
  }

  /**
 * theKey ist dabei LAND, ORT oder SCHULE und die Action dient dazu, im store das Element mit dem key auszutauschen
 */
  static katalogAction(actionType: string, theKey: string, theItems: KatalogItem[]) {
    return {
      'type': actionType,
      'key': theKey,
      'items': theItems
    };
  }

  static dateiInfoAction(actionType: string, theItems: DateiInfo[]) {
    return {
      'type': actionType,
      'payload': theItems
    } as DateiInfoAction;
  }

  static teilnehmerArrayAction(actionType: string, theTeilnehmer: Teilnehmer[], kuerzel: string) {
    return {
      'type': actionType,
      'alleTeilnehmer': theTeilnehmer,
      'kuerzel': kuerzel
    } as TeilnehmerArrayAction;
  }

  static antwortbuchstabenAction(actionType: string, eingabe: Antwortbuchstabe[]) {
    return {
      'type': actionType,
      'alleEingaben': eingabe
    } as AntwortbuchstabenAction;
  }

  static selectedTeilnehmerAction(actionType: string, teilnehmer: Teilnehmer) {
    return {
      type: actionType,
      theTeilnehmer: teilnehmer
    } as SelectedTeilnehmerAction;
  }

  static teilnehmerUrkundenAction(actionType: string, auftrag: TeilnehmerUndFarbschema) {
    return {
      'type': actionType,
      'auftrag': auftrag
    } as TeilnehmerUrkundenAction;
  }

  static schuleUrkundenAction(actionType: string, auftrag: SchuleUrkundenauftrag) {
    return {
      'type': actionType,
      'auftrag': auftrag
    } as SchuleUrkundenAction;
  }

  static auswertungsgruppeAction(actionType: string, gruppe: Auswertungsgruppe): AuswertungsgruppeAction {
    return {
      'type': actionType,
      'gruppe': gruppe
    } as AuswertungsgruppeAction;
  }

  static addDownloadCodeAction(value: DownloadCode) {
    return {
      'type': ADD,
      'downloadCode': value
    };
  }

  static clearDownloadCodeAction() {
    return {
      'type': CLEAR
    };
  }

  static loadSchuleAction(theSchule: Schule) {
    return {
      'type': LOAD,
      'schule': theSchule
    };
  }

  static clearAction() {
    return {
      'type': CLEAR
    };
  }

}
