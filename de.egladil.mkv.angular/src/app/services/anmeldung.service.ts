
import { tap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { Wettbewerbsanmeldung } from '../models/clientPayload';
import { APIResponsePayload } from '../models/serverResponse';
import { User, Teilnahme } from '../models/user';
import { SessionToken } from '../models/serverResponse';

import { MessageService } from './message.service';

import { ActionFactory, ADD, AktuelleTeilnameAction, TeilnahmenAction } from '../state/actions';
import { AktuelleTeilnahmeStore } from '../state/store/aktuelleTeilnahme.store';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { UserStore } from '../state/store/user.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { TeilnahmenStore } from '../state/store/teilnahmen.store';

import { LogService } from 'hewi-ng-lib';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';





const BASE_URL = environment.apiUrl + '/teilnahmen';


@Injectable()
export class AnmeldungService {

  /**
 * inject http und Store
 */
  constructor(private http: HttpClient
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private messageService: MessageService
    , private settingsStore: GlobalSettingsStore
    , private userStore: UserStore
    , private authTokenStore: AuthenticationTokenStore
    , private teilnahmenStore: TeilnahmenStore
    , private logger: LogService) {
  }

  anmelden(payload: Wettbewerbsanmeldung): void {
    this.logger.debug('Parameter: ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.post(BASE_URL + '/teilnahme', payload).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          const user = data.payload;
          this.logger.debug(JSON.stringify(user));

          const teilnahme = user.erweiterteKontodaten && user.erweiterteKontodaten.aktuelleTeilnahme || null;
          if (teilnahme !== null) {
            const teilnahmeAction = {
              'type': ADD,
              'teilnahme': teilnahme
            } as AktuelleTeilnameAction;
            this.aktuelleTeilnahmeStore.dispatch(teilnahmeAction);

            const _neue = [] as Teilnahme[];
            _neue.push(teilnahme);
            const teilnahmenAction = {
              'type': ADD,
              'payload': _neue
            } as TeilnahmenAction;

            this.teilnahmenStore.dispatch(teilnahmenAction);
          }

          const userAction = ActionFactory.loginUserAction(user);
          this.userStore.dispatch(userAction);
          this.settingsStore.dispatch(userAction);


          const authToken = { xsrfToken: data.sessionToken.xsrfToken, accessToken: data.sessionToken.accessToken } as SessionToken;
          const authAction = ActionFactory.authenticationTokenAction(authToken);
          this.authTokenStore.dispatch(authAction);

          const msg = data.apiMessage;
          this.messageService.handleMessage(msg);

        },
        (error) => {
          this.logger.debug('neue Teilnahme error');
          this.messageService.handleError(error);
        });
  }
}



