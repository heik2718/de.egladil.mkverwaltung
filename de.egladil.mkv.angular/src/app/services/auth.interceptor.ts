import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationTokenStore } from 'app/state/store/authenticationToken.store';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

	constructor(private authTokenStore: AuthenticationTokenStore) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		const authToken = this.authTokenStore.getState();

		if (authToken.xsrfToken && authToken.accessToken) {
			const cloned = req.clone({
				headers: req.headers.append('Content-Type', 'application/json; charset=utf-8').append('X-XSRF-TOKEN', authToken.xsrfToken).append('Authorization', 'Bearer ' + authToken.accessToken)
			});

			return next.handle(cloned);
		}

		if (authToken.xsrfToken) {
			const cloned = req.clone({
				headers: req.headers.append('Content-Type', 'application/json; charset=utf-8').append('X-XSRF-TOKEN', authToken.xsrfToken)
			});
			return next.handle(cloned);
		}

		const cloned = req.clone();
		return next.handle(cloned);
	}
}

