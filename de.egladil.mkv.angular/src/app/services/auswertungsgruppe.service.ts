
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { APIResponsePayload } from '../models/serverResponse';

import { Auswertungsgruppe } from '../models/auswertung';
import { AuswertungsgruppePayload } from '../models/clientPayload';
import { TeilnahmeIdentifier } from '../models/user';

import { ActionFactory, ADD, LOAD, CLEAR } from '../state/actions';
import { AuswertungsgruppeStore } from '../state/store/auswertungsgruppe.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { RootgruppeStore } from '../state/store/rootgruppe.store';


import { MessageService } from '../services/message.service';
import { TeilnahmeDelegate } from '../shared/teilnahme.delegate';

import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl + '/teilnahmen';
// /teilnahmen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen


@Injectable()
export class AuswertungsgruppeService {

  /**
 * inject http und Store
 */
  constructor(private http: HttpClient
    , private messageService: MessageService
    , private rootgruppeStore: RootgruppeStore
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private authTokenStore: AuthenticationTokenStore
    , private teilnahmeDelegate: TeilnahmeDelegate
    , private logger: LogService) {
  }

  rootgruppeLaden(tIdentifier: TeilnahmeIdentifier): void {
    this.logger.debug('laden: Parameter: ' + JSON.stringify(tIdentifier));
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/auswertungsgruppen';
    this.http.get(url).pipe(
      map(data => new APIResponsePayload<Auswertungsgruppe>(data)))
      .subscribe(
        (data: APIResponsePayload<Auswertungsgruppe>) => {
          const auswertungsgruppe = data.payload;
          const action = ActionFactory.auswertungsgruppeAction(LOAD, auswertungsgruppe);
          this.rootgruppeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Teilnehmer laden error');
          this.messageService.handleError(error)
        });
  }

  anlegen(tIdentifier: TeilnahmeIdentifier, kuerzelRootgruppe: string, payload: AuswertungsgruppePayload): void {
    this.logger.debug('laden: Parameter: ' + JSON.stringify(tIdentifier));
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/auswertungsgruppen/' + kuerzelRootgruppe;

    this.http.post(url, payload).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<Auswertungsgruppe>(data)))
      .subscribe(
        (data: APIResponsePayload<Auswertungsgruppe>) => {
          const auswertungsgruppe = data.payload;
          this.logger.debug('anlegen: ' + JSON.stringify(auswertungsgruppe));
          const action = ActionFactory.auswertungsgruppeAction(ADD, auswertungsgruppe);
          this.auswertungsgruppeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Auswertungsgruppe anlegen error');
          this.messageService.handleError(error);
        });
  }

  aendern(tIdentifier: TeilnahmeIdentifier, kuerzel: string, payload: AuswertungsgruppePayload): void {
    this.logger.debug('laden: Parameter: ' + JSON.stringify(tIdentifier));
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/auswertungsgruppen/' + kuerzel;

    this.http.put(url, payload).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<Auswertungsgruppe>(data)))
      .subscribe(
        (data: APIResponsePayload<Auswertungsgruppe>) => {
          const auswertungsgruppe = data.payload;
          // das ist die Root-Gruppe
          const geaenderteGruppe = this.teilnahmeDelegate.findAuswertungsgruppe(auswertungsgruppe, kuerzel);
          if (geaenderteGruppe !== null) {
            this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, geaenderteGruppe));
          } else {
            this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(CLEAR, null));
          }
          this.rootgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, auswertungsgruppe));
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Auswertungsgruppe anlegen error');
          this.messageService.handleError(error);
        });
  }

  loeschen(tIdentifier: TeilnahmeIdentifier, kuerzel: string): void {
    this.logger.debug('laden: Parameter: ' + JSON.stringify(tIdentifier));
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/auswertungsgruppen/' + kuerzel;

    this.http.delete(url).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<Auswertungsgruppe>(data)))
      .subscribe(
        (data: APIResponsePayload<Auswertungsgruppe>) => {
          const auswertungsgruppe = data.payload;
          // das ist die Root-Gruppe
          this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(CLEAR, null));
          this.rootgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(ADD, auswertungsgruppe));
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Auswertungsgruppe loeschen error');
          this.messageService.handleError(error);
        });
  }
}

