
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { ActionFactory, LOAD_KATALOG } from '../state/actions';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { APIResponsePayload, DateiInfo } from '../models/serverResponse';
import { DateiInfoStore } from '../state/store/dateiInfo.store';
import { DownloadDelegate } from '../shared/download.delegate';
import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl + '/dateien';


@Injectable()
export class DownloadService {

  constructor(private http: HttpClient
    , private messageService: MessageService
    , private dateiInfoStore: DateiInfoStore
    , private authTokenStore: AuthenticationTokenStore
    , private downloadDelegate: DownloadDelegate
    , private logger: LogService) {
  }

  getFileList(): void {
    this.logger.debug('start getFileList');
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL).pipe(
      map(data => new APIResponsePayload<DateiInfo[]>(data)),
      tap(items => this.logger.debug(JSON.stringify(items))))
      .subscribe((dateiInfos: APIResponsePayload<DateiInfo[]>) => {
        const action = ActionFactory.dateiInfoAction(LOAD_KATALOG, dateiInfos.payload);
        this.dateiInfoStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('error beim Laden der Dateiliste');
          this.messageService.handleError(error);
        });
  }

  saveFile(dateiname: string) {
    this.logger.debug('saveFile ' + dateiname);
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL + '/' + dateiname, { observe: 'response', responseType: 'blob' })
      .subscribe(response => {
        this.downloadDelegate.saveToFileSystem(response);
      }, (error) => {
        this.logger.debug('error beim download der datei' + dateiname);
        this.messageService.handleError(error);
      });
  }
}