
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

import { APIResponsePayload } from '../models/serverResponse';
import { SchuleAnschriftKatalogantrag } from '../models/clientPayload';
import { MessageService } from './message.service';
import { ApiMessageStore } from '../state/store/apiMessage.store';
import { KatalogItem, LAND, ORT, SCHULE, Schule } from '../models/kataloge';
import { ActionFactory, LOAD_KATALOG } from '../state/actions';
import { KatalogStore } from '../state/store/katalog.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { APIMessage } from '../models/serverResponse';
import { SchuleStore } from '../state/store/schule.store';
import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl;

@Injectable()
export class KatalogService {

  constructor(private http: HttpClient
    , private messageService: MessageService
    , private apiMessageStore: ApiMessageStore
    , private katalogStore: KatalogStore
    , private authTokenStore: AuthenticationTokenStore
    , private settingsStore: GlobalSettingsStore
    , private schuleStore: SchuleStore
    , private logger: LogService) {
  }

  loadLaender(): void {
    this.logger.debug('start loadLaender');
    const _xsrfToken = this.settingsStore.getState().xsrfToken;
    if (!_xsrfToken) {
      this.logger.debug('noch kein xsrfToken-Token erhalten');
      return;
    }
    const laenderArray = this.katalogStore.getState().land;
    this.logger.debug(laenderArray ? 'Anzahl Länder: ' + laenderArray.length : 'laender nicht geladen');
    if (laenderArray && laenderArray.length > 0) {
      this.logger.debug('Länder schon geladen');
      return;
    }

    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL + '/laender', { observe: 'body' }).pipe(
      map(data => <KatalogItem[]>(data)),
      tap(items => this.logger.debug(JSON.stringify(items))))
      .subscribe((laender: KatalogItem[]) => {
        // workaround wegen Browser-Bugs bei dropdown list: https://github.com/angular/angular/issues/10010
        // setzen als erstes Item ein leeres, damit eine Auswahl durch den user provoziert wird.
        const alleLaender = [];
        alleLaender[0] = new KatalogItem({
          'level': 0,
          'kuerzel': 'LEER',
          'name': '',
          'kinder': []
        });
        let index = 1;
        for (let i = 0; i < laender.length; i++) {
          alleLaender[index] = laender[i];
          index++;
        }
        const action = ActionFactory.katalogAction(LOAD_KATALOG, LAND, alleLaender);
        this.katalogStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }

  loadOrte(land: KatalogItem): void {
    if (land.kuerzel === 'LEER') {
      const orte = [];
      orte[0] = new KatalogItem({
        'level': 1,
        'kuerzel': 'LEER',
        'name': '',
        'kinder': []
      });
      const action = ActionFactory.katalogAction(LOAD_KATALOG, ORT, orte);
      this.katalogStore.dispatch(action);
      return;
    }
    this.logger.debug('start loadOrte zu land ' + land.name);

    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL + '/laender/' + land.kuerzel, { observe: 'body' }).pipe(
      map(data => <KatalogItem>(data)))
      .subscribe((parent: KatalogItem) => {
        // workaround wegen Browser-Bugs bei dropdown list: https://github.com/angular/angular/issues/10010
        // setzen als erstes Item ein leeres, damit eine Auswahl durch den user provoziert wird.
        const orte = [];
        orte[0] = new KatalogItem({
          'level': 1,
          'kuerzel': 'LEER',
          'name': '',
          'kinder': []
        });
        let index = 1;
        for (let i = 0; i < parent.kinder.length; i++) {
          orte[index] = parent.kinder[i];
          index++;
        }
        const action = ActionFactory.katalogAction(LOAD_KATALOG, ORT, orte);
        this.katalogStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }

  loadSchulen(land: KatalogItem, ort: KatalogItem): void {
    if (land.kuerzel === 'LEER' || ort.kuerzel === 'LEER') {
      const schulen = [];
      schulen[0] = new KatalogItem({
        'level': 1,
        'kuerzel': 'LEER',
        'name': '',
        'kinder': []
      });
      const action = ActionFactory.katalogAction(LOAD_KATALOG, SCHULE, schulen);
      this.katalogStore.dispatch(action);
      return;
    }
    this.logger.debug('start loadSchulen zu land ' + land.name + ' und ort ' + ort.name);

    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL + '/laender/' + land.kuerzel + '/' + ort.kuerzel, { observe: 'body' }).pipe(
      map(data => <KatalogItem>(data)),
      tap(items => this.logger.debug(JSON.stringify(items))))
      .subscribe((parent: KatalogItem) => {
        // workaround wegen Browser-Bugs bei dropdown list: https://github.com/angular/angular/issues/10010
        // setzen als erstes Item ein leeres, damit eine Auswahl durch den user provoziert wird.
        const schulen = [];
        schulen[0] = new KatalogItem({
          'level': 1,
          'kuerzel': 'LEER',
          'name': '',
          'kinder': []
        });
        let index = 1;
        for (let i = 0; i < parent.kinder.length; i++) {
          schulen[index] = parent.kinder[i];
          index++;
        }

        const action = ActionFactory.katalogAction(LOAD_KATALOG, SCHULE, schulen);
        this.katalogStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }

  neueSchule(landkuerzel: string, payload: SchuleAnschriftKatalogantrag): void {
    this.logger.debug('start neuer Schulantrag ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.post(BASE_URL + '/laender/' + landkuerzel + '/schulantrag', payload).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<APIMessage>(data)))
      .subscribe((apiResponse: APIResponsePayload<APIMessage>) => {
        const action = ActionFactory.apiMessageAction(apiResponse.apiMessage);
        this.apiMessageStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }

  loadSchule(schulkuerzel: string): void {
    // const _authHeaders = this.authTokenStore.getHeaders();
    this.http.get(BASE_URL + '/schulen/' + schulkuerzel).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<Schule>(data)))
      .subscribe((apiResponse: APIResponsePayload<Schule>) => {
        const action = ActionFactory.loadSchuleAction(apiResponse.payload);
        this.schuleStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }
}

