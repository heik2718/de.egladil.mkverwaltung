
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

import { LoginCredentials } from '../models/clientPayload';
import { LAND, ORT, SCHULE } from '../models/kataloge';
import { APIResponsePayload, GlobalSettings } from '../models/serverResponse';
import { User } from '../models/user';
import { SessionToken } from '../models/serverResponse';

import { MessageService } from './message.service';

import { DESELECT, CLEAR, TEILNEHMER_RELEASE, ADD } from '../state/actions';
import { ActionFactory, MKVAction, StringAction, SelectedTeilnehmerAction, TeilnehmerUrkundenAction, AktuelleTeilnameAction, TeilnahmenAction } from '../state/actions';
import { AktuelleTeilnahmeStore } from '../state/store/aktuelleTeilnahme.store';
import { AuswertungsgruppeStore } from '../state/store/auswertungsgruppe.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { DownloadcodeStore } from '../state/store/downloadcode.store';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { KatalogStore } from '../state/store/katalog.store';
import { KatalogItemStore } from '../state/store/katalogItem.store';
import { RootgruppeStore } from '../state/store/rootgruppe.store';
import { StaticContentStore } from '../state/store/staticContent.store';
import { TeilnahmenStore } from '../state/store/teilnahmen.store';
import { TeilnehmerStore } from '../state/store/teilnehmer.store';
import { TeilnehmerArrayStore } from '../state/store/teilnehmerArray.store';
import { PrivaturkundenauftragStore } from '../state/store/privaturkundenauftrag.store';
import { UserStore } from '../state/store/user.store';
import { StaticContentService } from '../services/staticContent.service';

import { Router } from '@angular/router';
import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';





const BASE_URL = environment.apiUrl;


@Injectable()
export class SessionService {


	/**
	 * inject http und Store
	 */
  constructor(private http: HttpClient
    , private messageService: MessageService
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private auswertungsgruppeStore: AuswertungsgruppeStore
    , private authTokenStore: AuthenticationTokenStore
    , private downloadcodeStore: DownloadcodeStore
    , private globalSettingsStore: GlobalSettingsStore
    , private katalogStore: KatalogStore
    , private katalogItemStore: KatalogItemStore
    , private rootgruppeStore: RootgruppeStore
    , private staticContentService: StaticContentService
    , private staticContentStore: StaticContentStore
    , private teilnahmenStore: TeilnahmenStore
    , private teilnehmerStore: TeilnehmerStore
    , private teilnehmerArrrayStore: TeilnehmerArrayStore
    , private teilnehmerUrkundenauftraStore: PrivaturkundenauftragStore
    , private userStore: UserStore
    , private router: Router
    , private logger: LogService) {

  }

  ping(): void {
    const _xsrfToken = this.globalSettingsStore.getState().xsrfToken;
    this.logger.debug('start ping: ' + _xsrfToken);
    if (_xsrfToken && _xsrfToken.length > 0) {
      this.logger.debug('xsrfToken vorhanden => return');
      return;
    }
    this.http.get(BASE_URL + '/session/kontext', {observe: 'body'}).pipe(
     // mit dem umgewandelten Objekt den Kontruktor füttern. Ergebnis ist ein initialisierter Kontext
      map(data => new APIResponsePayload<GlobalSettings>(data)))
      .subscribe(
        (data: APIResponsePayload<GlobalSettings>) => {
          this._refreshAnonymousSession(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }

  login(credentials: LoginCredentials): void {
    this.logger.debug('start login');
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.post(BASE_URL + '/session', credentials, { observe: 'body' }).pipe(
      //      .do(data => this.logger.debug(JSON.stringify(data)))
      // mit dem umgewandelten Objekt den Kontruktor füttern. Ergebnis ist ein initialisierter User
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          //        const aktuelleTeilnahme = this.userDelegate.findTeilnahmeZuJahr(data.payload, wettvewerbsjahr);
          const aktuelleTeilnahme = data.payload.erweiterteKontodaten.aktuelleTeilnahme;
          if (aktuelleTeilnahme !== null) {
            const teilnahmeAction = {
              'type': ADD,
              'teilnahme': aktuelleTeilnahme
            } as AktuelleTeilnameAction;
            this.aktuelleTeilnahmeStore.dispatch(teilnahmeAction);
          }

          const _user = data.payload as User;

          const userAction = ActionFactory.loginUserAction(_user);
          this.userStore.dispatch(userAction);

          const authToken = { xsrfToken: data.sessionToken.xsrfToken, accessToken: data.sessionToken.accessToken } as SessionToken;
          const authAction = ActionFactory.authenticationTokenAction(authToken);
          this.globalSettingsStore.dispatch(authAction);
          this.authTokenStore.dispatch(authAction);

          if (_user.basisdaten.rolle === 'MKV_LEHRER') {
            this.staticContentService.loadStaticHtmlFileList();
          }
        },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }

  logout(): void {
    // const authHeaders = this.authTokenStore.getHeaders();
    // if (authHeaders.get('Authorization')) {

      this._clearState();

      this.http.delete(BASE_URL + '/session', { observe: 'body' }).pipe(
        map(data => new APIResponsePayload<GlobalSettings>(data)))
        .subscribe((data: APIResponsePayload<GlobalSettings>) => {
          this._refreshAnonymousSession(data);
          this.router.navigate(['/landing']);
        },
          (error) => this.messageService.handleError(error)
        );
    // }
  }

  private _refreshAnonymousSession(data: APIResponsePayload<GlobalSettings>) {
    const settingsAction = ActionFactory.pingAction(data.payload);
    this.globalSettingsStore.dispatch(settingsAction);
    const authToken = { xsrfToken: data.payload.xsrfToken, accessToken: null } as SessionToken;
    const authAction = ActionFactory.authenticationTokenAction(authToken);
    this.authTokenStore.dispatch(authAction);
  }

  private _clearState(): void {
    this.aktuelleTeilnahmeStore.dispatch({ 'type': ADD, 'teilnahme': null } as AktuelleTeilnameAction);
    this.auswertungsgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(CLEAR, null));
    this.downloadcodeStore.dispatch({ 'type': CLEAR, 'str': null } as StringAction);
    this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, SCHULE, null));
    this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, ORT, null));
    this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, LAND, null));
    this.katalogStore.dispatch(ActionFactory.katalogAction(CLEAR, SCHULE, []));
    this.katalogStore.dispatch(ActionFactory.katalogAction(CLEAR, ORT, []));
    this.katalogStore.dispatch(ActionFactory.katalogAction(CLEAR, LAND, []));
    this.rootgruppeStore.dispatch(ActionFactory.auswertungsgruppeAction(CLEAR, null));
    this.staticContentStore.dispatch({ 'type': CLEAR } as MKVAction);
    this.teilnehmerArrrayStore.dispatch(ActionFactory.teilnehmerArrayAction(CLEAR, [], null));
    this.teilnehmerStore.dispatch({ 'type': TEILNEHMER_RELEASE, 'theTeilnehmer': null } as SelectedTeilnehmerAction);
    this.teilnahmenStore.dispatch({ 'type': CLEAR, 'payload': [] } as TeilnahmenAction);
    this.teilnehmerUrkundenauftraStore.dispatch({ 'type': CLEAR, 'auftrag': null } as TeilnehmerUrkundenAction);
    this.userStore.dispatch(ActionFactory.logoutUserAction());
    this.messageService.clearMessage();
    this.logger.debug('ende von _clearState()');
  }
}