
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { APIResponsePayload, NullResponsePayload } from '../models/serverResponse';
import { MessageService } from './message.service';
import { OrderTempCredentials, TempPwdAendernCredentials } from '../models/clientPayload';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl;


@Injectable()
export class TempPasswordService {


	/**
	 * inject http und Store
	 */
  constructor(private http: HttpClient
    , private messageService: MessageService
    , private authTokenStore: AuthenticationTokenStore
    , private logger: LogService) {

  }

  orderPassword(credentials: OrderTempCredentials): void {
    this.logger.debug('start orderPassword');
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.post(BASE_URL + '/temppwd', credentials, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<NullResponsePayload>(data)))
      .subscribe(
        (data: APIResponsePayload<NullResponsePayload>) => {
          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }

  changePassword(credentials: TempPwdAendernCredentials): void {
    this.logger.debug('start changePassword');
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.put(BASE_URL + '/temppwd', credentials, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<NullResponsePayload>(data)))
      .subscribe(
        (data: APIResponsePayload<NullResponsePayload>) => {
          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }
}