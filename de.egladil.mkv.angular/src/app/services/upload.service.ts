
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { APIMessage } from 'app/models/serverResponse';
import { MessageService } from './message.service';
import { AuthenticationTokenStore } from 'app/state/store/authenticationToken.store';

const URL = environment.apiUrl + '/dateien/datei';


@Injectable()
export class UploadService {

  constructor(private authTokenStore: AuthenticationTokenStore) { }



  public uploadFile(file: File) {

    const promise = this.makeFileRequest(file, 'POST');

    return promise;
  }

  // public uploadFiles(files: Set<File>): { [key: string]: { progress: Observable<number> } } {

  //   const status: { [key: string]: { progress: Observable<number> } } = {};

  //   // const authHeaders = this.authTokenStore.getHeadersUpload();

  //   files.forEach(file => {

  //     const formData: FormData = new FormData();
  //     formData.append('file', file, file.name);

  //     const req = new HttpRequest('POST', URL, formData, {
  //       headers: new HttpHeaders({'Content-Type': 'multipart/form-data'}),
  //       reportProgress: true
  //     });

  //     const progress = new Subject<number>();

  //     this.http.request(req).subscribe(event => {

	// 			if (event.type === HttpEventType.UploadProgress) {

	// 				// calculate the progress percentage
	// 				const percentDone = Math.round(100 * event.loaded / event.total);

	// 				// pass the percentage into the progress-stream
	// 				progress.next(percentDone);
	// 			} else if (event.type === HttpEventType.Response) {
	// 				// Close the progress-stream if we get an answer form the API
	// 				// The upload is complete
	// 				progress.complete();
	// 			}
	// 		});

  //     status[file.name] = {
	// 			progress: progress.asObservable()
	// 		};
  //   });

  //   return status;
  // }

  private makeFileRequest(file: File, method: string): Promise<any> {

    const promise = new Promise((resolve, reject) => {

      const formData: any = new FormData();

      formData.append('file', file, file.name);

      const xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          const message = xhr.response.text;
          console.log(message);
          if (xhr.readyState === 4) {
            if (xhr.status >= 200 && xhr.status < 400) {
              resolve(JSON.parse(xhr.response));
              // der Parameter in reslove ist ein ResponsePayload und wird über promise.then() in die callback-function durchgereicht.

            } else {
              reject(JSON.parse(xhr.response));
            }
          }
        }
      };

      xhr.ontimeout = function () {
        const msg = {
          'level': 'WARN',
          'message': 'Timeout'
        } as APIMessage;

        const responsePayload = {
          apiMessage: msg,
          payload: null
        };

        console.log('keine Ahnung, was ich damit anfangen soll: ' + JSON.stringify(responsePayload));
      };

      xhr.upload.addEventListener('loadstart', this.progressStartListener, false);
      xhr.upload.addEventListener('progress', this.progressListener, false);
      xhr.upload.addEventListener('load', this.transferCompleteListener, false);

      // const url = BASE_URL + '/uploads' + '/' + upload.jahr + '/' + upload.teilnahmeart
      //   + '/' + upload.kuerzel + '/' + upload.checksumme;

      xhr.open(method, URL, true);

      const authToken = this.authTokenStore.getState();
      // // Hier keinen Content-Type- Header setzen. Das macht xhr automatisch korrekt!
      // //      xhr.setRequestHeader('Content-Type', 'multipart/form-data');
      xhr.setRequestHeader('Authorization', 'Bearer ' + authToken.accessToken);
      xhr.setRequestHeader('X-XSRF-TOKEN', authToken.xsrfToken);
      xhr.setRequestHeader('Accept', 'application/json, text/plain, */*');
      xhr.send(formData);
    });

    return promise;
  }




  // #############################################################################
  // die folgenden Listener verarbeiten lokale Ereignisse auf Browser-Ebene
  private progressListener(evt: ProgressEvent): void {
    // dieses event misst, wie lange der Browser lokal braucht, um die Datei einzulesen. Ist daher nur geeignet,
    // wenn man wirklich große Dateien einlesen will
    if (evt.lengthComputable) {
      const percentComplete = (evt.loaded / evt.total) * 100;
      // this._logger.debug('geladen: ' + percentComplete);
    }
  }

  private progressStartListener(_evt: ProgressEvent): void {
    // this._logger.debug('progress started');
  }

  private transferCompleteListener(_evt: ProgressEvent): void {
    // this._logger.debug('transfer completed');
  }


}



