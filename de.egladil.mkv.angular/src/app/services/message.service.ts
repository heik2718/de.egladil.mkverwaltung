import { Injectable } from '@angular/core';
import { APIMessage } from '../models/serverResponse';
import { ApiMessageStore } from '../state/store/apiMessage.store';
import { ActionFactory, CLEAR } from '../state/actions';
import { LogService } from 'hewi-ng-lib';


@Injectable()
export class MessageService {

	/**
	 * Store
	 */
  constructor(private store: ApiMessageStore, private logger: LogService) { }

  handleError(error: any = {}): void {
    this.logger.debug(JSON.stringify(error));
    let payload = {};
    let action = null;
    switch (error.status) {
      case 0:
        this.logger.debug('server is down');
        const apiMessage = {
          level: 'ERROR',
          message: 'Der Service steht wegen eines Fehlers nicht zur Verfügung. Bitte senden Sie eine Mail.'
        };
        action = ActionFactory.apiMessageAction(apiMessage);
        break;
      case 500:
        action = ActionFactory.apiMessageAction({
          level: 'ERROR',
          message: 'Es ist ein Serverfehler aufgetreten. Bitte senden Sie eine Mail.'
        });
        break;
      //      case 400:
      //        const apiResponse = new APIResponsePayload<NullResponsePayload>(JSON.parse(error._body));
      //        action = ActionFactory.apiMessageAction(apiResponse.apiMessage);
      //        break;
      case 403:
        action = ActionFactory.apiMessageAction({
          level: 'ERROR',
          message: 'Bitte löschen Sie die Eingaben im Feld Kleber.'
        });
        break;
      case 404:
        action = ActionFactory.apiMessageAction({
          level: 'ERROR',
          message: 'Die Datei existiert nicht oder nicht mehr.'
        });
        break;
      case 408:
        action = ActionFactory.apiMessageAction({
          level: 'WARN',
          message: 'Sie waren zu lange inaktiv. Bitte laden Sie die Seite neu (F5)'
        });
        break;
      case 400:
      case 401:
      case 402:
      case 409: // concurrent modification
      case 412: // precondition failed
      case 900: // duplicate entry
      case 902: // validation error
      case 904: // not successful
        payload = JSON.parse(error._body);
        action = this.errorToAction(payload);
        break;
      default:
        this.logger.error('Servererror status=' + error.status);
        action = ActionFactory.apiMessageAction({
          level: 'ERROR',
          message: 'Es ist ein unerwarteter Fehler aufgetreten. Bitte senden Sie eine Mail.'
        });
        break;
    }
    this.store.dispatch(action);
  }

  errorToAction(payload: any = {}): any {
    let action = {};
    this.logger.debug(JSON.stringify(payload));
    if (payload.apiMessage) {
      action = ActionFactory.apiMessageAction(payload.apiMessage as APIMessage);
    } else {
      // FIXME das ist noch etwas blöd, da APIMessage und CrossValidationMessage nicht zusammenpassen.
      // Kann aber so gemacht werden, solange die Validierung im Client dafür sorgt, dass kein invalides Request-Payload gesendet wird.
      // Muss aufgeräumt werden!!!!
      if (payload.crossValidationMessage) {
        const apiMessage = {
          level: 'ERROR',
          message: payload.crossValidationMessage
        };
        action = ActionFactory.apiMessageAction(apiMessage);
      } else {
        action = ActionFactory.apiMessageAction(payload as APIMessage);
      }
    }
    return action;
  }


  handleMessage(message: any = {}): void {
    if (message !== undefined) {
      const theMessage = this._toAPIMessage(message);
      if (theMessage !== null && theMessage.message.length > 0) {

        if (theMessage.level === 'INFO') {
          window.scrollTo(0, 0);
        }
        this.store.dispatch(ActionFactory.apiMessageAction(theMessage));
      }
    }
  }

  private _toAPIMessage(message: any = {}): APIMessage {
    if (message.apiMessage) {
      return message.apiMessage as APIMessage;
    }
    if (message.message && message.level) {
      return new APIMessage(message.level, message.message);
    }
    return null;
  }

  /**
   * convenience-Methode, damit die Aufrufen nicht darüber nachdenken müssen, welches Event sie erzeugen sollten.
   */
  clearMessage(): void {
    const action = {
      type: CLEAR
    };
    this.store.dispatch(action);
  }
}