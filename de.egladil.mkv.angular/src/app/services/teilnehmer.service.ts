
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Teilnehmer, Auswertungsgruppe } from '../models/auswertung';
import { User, TeilnahmeIdentifier } from '../models/user';
import { APIResponsePayload, NullResponsePayload, WARN } from '../models/serverResponse';
import { Teilnahme } from '../models/user';

import { MessageService } from './message.service';
import { TeilnahmeDelegate } from '../shared/teilnahme.delegate';

import { ActionFactory, AuthUserAction, UPDATE, TEILNEHMER_ERFASST, TEILNEHMER_GEAENDERT, TEILNEHMER_GELADEN } from '../state/actions';
import { TEILNEHMER_GELOESCHT, AktuelleTeilnameAction, UPDATE_AKT_TEILNAHME } from '../state/actions';

import { AktuelleTeilnahmeStore } from '../state/store/aktuelleTeilnahme.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { UserStore } from '../state/store/user.store';
import { TeilnehmerArrayStore } from '../state/store/teilnehmerArray.store';

import { LogService } from 'hewi-ng-lib';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const BASE_URL = environment.apiUrl + '/teilnahmen';
// /teilnahmen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/teilnehmer


@Injectable()
export class TeilnehmerService {

  /**
 * inject http und Store
 */
  constructor(private http: HttpClient
    , private messageService: MessageService
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private teilnehmerArrayStore: TeilnehmerArrayStore
    , private authTokenStore: AuthenticationTokenStore
    , private userStore: UserStore
    , private teilnahmeDelegate: TeilnahmeDelegate
    , private logger: LogService) {
  }

  laden(tIdentifier: TeilnahmeIdentifier): void {
    this.logger.debug('laden: Parameter: ' + JSON.stringify(tIdentifier));
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/teilnehmer';
    this.http.get(url, { observe: 'body' }).pipe(
      //      .do(data => this.logger.debug(JSON.stringify(data)))
      map(data => new APIResponsePayload<Teilnehmer[]>(data)))
      .subscribe(
        (data: APIResponsePayload<Teilnehmer[]>) => {
          const teilnehmerArray = data.payload;
          const action = ActionFactory.teilnehmerArrayAction(TEILNEHMER_GELADEN, teilnehmerArray, null);
          this.teilnehmerArrayStore.dispatch(action);

          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Teilnehmer laden error');
          this.messageService.handleError(error);
        });
  }

  zuAuswertunggruppeLaden(auswertungsgruppe: Auswertungsgruppe): void {
    const tIdentifier = {
      'teilnahmeart': auswertungsgruppe.teilnahmeart,
      'kuerzel': auswertungsgruppe.teilnahmekuerzel,
      'jahr': auswertungsgruppe.jahr
    } as TeilnahmeIdentifier;

    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/auswertungsgruppen/' + auswertungsgruppe.kuerzel + '/teilnehmer';
    this.http.get(url, { observe: 'body' }).pipe(
      //      .do(data => this.logger.debug(JSON.stringify(data)))
      map(data => new APIResponsePayload<Teilnehmer[]>(data)))
      .subscribe(
        (data: APIResponsePayload<Teilnehmer[]>) => {
          const teilnehmerArray = data.payload;
          const action = ActionFactory.teilnehmerArrayAction(TEILNEHMER_GELADEN, teilnehmerArray, null);
          this.teilnehmerArrayStore.dispatch(action);

          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Teilnehmer laden error');
          this.messageService.handleError(error);
        });
  }

  speichern(teilnehmer: Teilnehmer): void {
    this.logger.debug('speichern: Parameter: ' + JSON.stringify(teilnehmer));
    const tIdentifier = {
      'teilnahmeart': teilnehmer.teilnahmeart,
      'kuerzel': teilnehmer.teilnahmekuerzel,
      'jahr': teilnehmer.jahr
    } as TeilnahmeIdentifier;

    const path = this.teilnahmeDelegate.getPathPrefix(BASE_URL, tIdentifier) + '/teilnehmer';
    // const authHeaders = this.authTokenStore.getHeaders();

    if (teilnehmer.kuerzel !== null && teilnehmer.kuerzel !== undefined && teilnehmer.kuerzel !== '') {
      this._aendern(path, teilnehmer);
    } else {
      this._anlegen(path, teilnehmer);
    }
  }

  einenTeilnehmerLoeschen(teilnehmer: Teilnehmer): void {
    this.logger.debug('loeschen: Parameter: ' + JSON.stringify(teilnehmer));
    const path = BASE_URL + '/teilnehmer/' + teilnehmer.kuerzel;
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.delete(path, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<NullResponsePayload>(data)))
      .subscribe(
        (data: APIResponsePayload<NullResponsePayload>) => {
          const action = ActionFactory.teilnehmerArrayAction(TEILNEHMER_GELOESCHT, [], teilnehmer.kuerzel);
          this.teilnehmerArrayStore.dispatch(action);
          this._updateAnzahlKinder(-1);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('einen Teilnehmer loeschen error');
          this.messageService.handleError(error);
        });
  }

  teilnehmergruppeLoeschen(teilnahmeIdentifier: TeilnahmeIdentifier): void {
    this.logger.debug('loeschen: Parameter: ' + JSON.stringify(teilnahmeIdentifier));

    const path = this.teilnahmeDelegate.getPathPrefix(BASE_URL, teilnahmeIdentifier) + '/teilnehmer';
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.delete(path, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<NullResponsePayload>(data)))
      .subscribe(
        (data: APIResponsePayload<NullResponsePayload>) => {
          const action = ActionFactory.teilnehmerArrayAction(TEILNEHMER_GELOESCHT, [], null);
          this.teilnehmerArrayStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('einen Teilnehmer loeschen error');
          this.messageService.handleError(error);
        });
  }

  private _anlegen(path: string, teilnehmer: Teilnehmer): void {
    this.http.post(path, teilnehmer, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<Teilnehmer>(data)))
      .subscribe(
        (data: APIResponsePayload<Teilnehmer>) => {
          const teilnehmerArray = [];
          teilnehmerArray.push(data.payload);
          const action = ActionFactory.teilnehmerArrayAction(TEILNEHMER_ERFASST, teilnehmerArray, teilnehmer.kuerzel);
          this.teilnehmerArrayStore.dispatch(action);
          this._updateAnzahlKinder(1);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Teilnehmer anlegen error');
          this.messageService.handleError(error);
        });
  }

  private _aendern(path: string, teilnehmer: Teilnehmer): void {
    this.http.put(path, teilnehmer).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<Teilnehmer>(data)))
      .subscribe(
        (data: APIResponsePayload<Teilnehmer>) => {
          const teilnehmerArray = [];
          teilnehmerArray.push(data.payload);
          const action = ActionFactory.teilnehmerArrayAction(TEILNEHMER_GEAENDERT, teilnehmerArray, teilnehmer.kuerzel);
          this.teilnehmerArrayStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Teilnehmer anlegen error');
          this.messageService.handleError(error);
        });
  }

  private _updateAnzahlKinder(signedAnzahl: number): void {
    const aktUser = this.userStore.getState();
    const aktuelleTeilnahme = this.aktuelleTeilnahmeStore.getState();
    const anzahlVorher = aktuelleTeilnahme.anzahlTeilnehmer;
    const neueTeilnahme = new Teilnahme(aktuelleTeilnahme);
    const neuerUser = new User(aktUser);

    if (neuerUser.erweiterteKontodaten && neuerUser.erweiterteKontodaten.aktuelleTeilnahme) {
      neueTeilnahme.anzahlTeilnehmer = neuerUser.erweiterteKontodaten.aktuelleTeilnahme.anzahlTeilnehmer;
    } else {
      neueTeilnahme.anzahlTeilnehmer = 0;
    }

    const aktUserAction = {
      'type': UPDATE_AKT_TEILNAHME,
      'user': neuerUser
    } as AuthUserAction;

    const aktTeilnahmeAction = {
      'type': UPDATE,
      'teilnahme': aktuelleTeilnahme
    } as AktuelleTeilnameAction;

    this.userStore.dispatch(aktUserAction);
    this.aktuelleTeilnahmeStore.dispatch(aktTeilnahmeAction);
  }
}




