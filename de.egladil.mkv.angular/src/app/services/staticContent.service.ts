
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LogService } from 'hewi-ng-lib';

import { ActionFactory, ADD, STATIC_HTML } from '../state/actions';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { StaticHtmlStore } from '../state/store/staticHtml.store';
import { APIResponsePayload, DateiInfo } from '../models/serverResponse';

import { MessageService } from './message.service';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl;

@Injectable()
export class StaticContentService {

  constructor(private http: HttpClient
    , private messageService: MessageService
    , private authTokenStore: AuthenticationTokenStore
    , private _sanitizer: DomSanitizer
    , private _globalSettingsStore: GlobalSettingsStore
    , private _staticHtmlStore: StaticHtmlStore
    , private logger: LogService) {
  }

  loadStaticHtmlFileList(): void {

    // const _authHeaders = this.authTokenStore.getHeaders();
    this.http.get(BASE_URL + '/htmlcontent').pipe(
      map(data => new APIResponsePayload<DateiInfo[]>(data)))
      .subscribe((dateiInfos: APIResponsePayload<DateiInfo[]>) => {
        const action = ActionFactory.dateiInfoAction(STATIC_HTML, dateiInfos.payload);
        this._globalSettingsStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('error beim Laden der Dateiliste');
          this.messageService.handleError(error);
        });
  }


  getStaticContent(url: string): void {
    const _url = BASE_URL + '/' + url;
    this.logger.debug('requesting static content from ' + _url);

    // const authHeaders = this.authTokenStore.getHeadersHtmlType();

    this.http.get(_url).subscribe(body => {
      const _content = this._sanitizer.sanitize(SecurityContext.HTML, body);
      const _action = ActionFactory.replaceStringAction(ADD, _content);
      this._staticHtmlStore.dispatch(_action);
    },
      (error) => {
        this.logger.debug('login error');
        this.messageService.handleError(error);
      });
  }
}

