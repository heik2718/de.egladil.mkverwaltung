
import {tap, map} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

import { TeilnehmerUndFarbschema, SchuleUrkundenauftrag, DownloadCode, Farbschema } from '../models/auswertung';
import { TeilnahmeIdentifier, Teilnahme } from '../models/user';

import { DownloadDelegate } from '../shared/download.delegate';
import { MessageService } from './message.service';
import { TeilnahmeDelegate } from '../shared/teilnahme.delegate';

import { ActionFactory, LOAD, FarbschemaAction, TeilnahmenAction } from '../state/actions';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { DownloadcodeStore } from '../state/store/downloadcode.store';
import { FarbschemaStore } from '../state/store/farbschema.store';
import { TeilnahmenStore } from '../state/store/teilnahmen.store';
import { SessionToken } from '../models/serverResponse';

import { APIResponsePayload } from '../models/serverResponse';

import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';





const BASE_URL = environment.apiUrl;


@Injectable()
export class AuswertungService {

  constructor(private http: HttpClient
    , private messageService: MessageService
    , private downloadcodeStore: DownloadcodeStore
    , private farbschemaStore: FarbschemaStore
    , private authTokenStore: AuthenticationTokenStore
    , private downloadDelegate: DownloadDelegate
    , private teilnahmeDelegate: TeilnahmeDelegate
    , private teilnahmenStore: TeilnahmenStore
    , private logger: LogService) {
  }

  druckeTeilnehmerurkunden(tIdentifier: TeilnahmeIdentifier, auftrag: TeilnehmerUndFarbschema) {
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL + '/auswertungen', tIdentifier) + '/urkunden';

    this.logger.debug('Drucke Urkunden ' + url + ': ' + auftrag.teilnehmer.length);

    this.http.post(url, this._getPayload(auftrag)).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<DownloadCode>(data)), )
      .subscribe(
        (data: APIResponsePayload<DownloadCode>) => {
          const action = ActionFactory.addDownloadCodeAction(data.payload);
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Urkunden generieren error');
          const action = ActionFactory.clearDownloadCodeAction();
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleError(error);
        });
  }

  druckeTeilnehmerurkundenKaengurusprung(tIdentifier: TeilnahmeIdentifier, auftrag: TeilnehmerUndFarbschema) {
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL + '/auswertungen', tIdentifier) + '/jumpcerts';

    this.http.post(url, this._getPayload(auftrag)).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<DownloadCode>(data)), )
      .subscribe(
        (data: APIResponsePayload<DownloadCode>) => {
          const action = ActionFactory.addDownloadCodeAction(data.payload);
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Urkunden generieren error');
          const action = ActionFactory.clearDownloadCodeAction();
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleError(error);
        });
  }

  druckeSchulurkunden(tIdentifier: TeilnahmeIdentifier, auftrag: SchuleUrkundenauftrag) {
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL + '/auswertungen', tIdentifier) + '/auswertungsgruppe/urkunden';

    this.http.post(url, auftrag).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<DownloadCode>(data)), )
      .subscribe(
        (data: APIResponsePayload<DownloadCode>) => {
          const action = ActionFactory.addDownloadCodeAction(data.payload);
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          const action = ActionFactory.clearDownloadCodeAction();
          this.downloadcodeStore.dispatch(action);
          this.logger.debug('Urkunden generieren error');
          this.messageService.handleError(error);
        });
  }

  druckeSchulurkundenKaengurusprung(tIdentifier: TeilnahmeIdentifier, auftrag: SchuleUrkundenauftrag) {
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL + '/auswertungen', tIdentifier) + '/auswertungsgruppe/jumpcerts';

    this.http.post(url, auftrag).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<DownloadCode>(data)), )
      .subscribe(
        (data: APIResponsePayload<DownloadCode>) => {
          const action = ActionFactory.addDownloadCodeAction(data.payload);
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          const action = ActionFactory.clearDownloadCodeAction();
          this.downloadcodeStore.dispatch(action);
          this.logger.debug('Urkunden generieren error');
          this.messageService.handleError(error);
        });
  }

  erstelleStatistikAktuellesJahr(kuerzel: string) {
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = BASE_URL + '/teilnahmen/rootgruppen/' + kuerzel + '/schulstatistik';
    this.logger.debug(url);

    this.http.get(url).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<DownloadCode>(data)), )
      .subscribe(
        (data: APIResponsePayload<DownloadCode>) => {
          const action = ActionFactory.addDownloadCodeAction(data.payload);
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Schulstatistik generieren error');
          const action = ActionFactory.clearDownloadCodeAction();
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleError(error);
        });
  }

  erstelleStatistik(tIdentifier: TeilnahmeIdentifier) {
    // const authHeaders = this.authTokenStore.getHeaders();

    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL + '/teilnahmen', tIdentifier) + '/schulstatistik';
    this.logger.debug(url);

    this.http.get(url).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<DownloadCode>(data)), )
      .subscribe(
        (data: APIResponsePayload<DownloadCode>) => {
          const action = ActionFactory.addDownloadCodeAction(data.payload);
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('Schulstatistik generieren error');
          const action = ActionFactory.clearDownloadCodeAction();
          this.downloadcodeStore.dispatch(action);
          this.messageService.handleError(error);
        });
  }

  private _getPayload(auftrag: TeilnehmerUndFarbschema): any {
    const alleKuerzel = [];
    for (let i = 0; i < auftrag.teilnehmer.length; i++) {
      alleKuerzel.push(auftrag.teilnehmer[i].kuerzel);
    }
    const payload = {
      'farbschemaName': auftrag.farbschemaName,
      'teilnehmerKuerzel': alleKuerzel
    };
    return payload;
  }

  saveFile(tIdentifier: TeilnahmeIdentifier, downloadcode: string) {
    this.logger.debug('saveFile ' + downloadcode);
    // const authHeaders = this.authTokenStore.getHeaders();
    const url = this.teilnahmeDelegate.getPathPrefix(BASE_URL + '/auswertungen', tIdentifier) + '/auswertung/' + downloadcode;

    this.http.get(url, { observe: 'response', responseType: 'blob' })
      .subscribe(response => {
        this.downloadDelegate.saveToFileSystem(response);
      }, (error) => {
        this.logger.debug('error beim download der Auswertung mit downloadcode ' + downloadcode);
        this.messageService.handleError(error);
      });
  }

  loadFarbschemas(): void {
    this.logger.debug('start getFileList');
    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL + '/farbschemas').pipe(
     map(data => new APIResponsePayload<Farbschema[]>(data)), )
      .subscribe((apiResponse: APIResponsePayload<Farbschema[]>) => {

        const action = {
          type: LOAD,
          payload: apiResponse.payload
        } as FarbschemaAction;

        this.farbschemaStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('error beim Laden der Farbschemas');
          this.messageService.handleError(error);
        });
  }

  loadTeilnahmen(): void {
    this.logger.debug('start getFileList');

    const authToken = (<SessionToken>this.authTokenStore.getState());
    if (!authToken.accessToken) {
      return;
    }

    // const authHeaders = this.authTokenStore.getHeaders();

    this.http.get(BASE_URL + '/teilnahmen').pipe(
      map(data => new APIResponsePayload<Teilnahme[]>(data)), )
      .subscribe((apiResponse: APIResponsePayload<Teilnahme[]>) => {

        const action = {
          type: LOAD,
          payload: apiResponse.payload
        } as TeilnahmenAction;

        this.teilnahmenStore.dispatch(action);
      },
        (error) => {
          this.logger.debug('error beim Laden der Teilnahmen');
          this.messageService.handleError(error);
        });
  }
}

