
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { APIResponsePayload, NullResponsePayload } from '../models/serverResponse';
import { SchulzuordnungAendernPayload, MailadresseAendernPayload, PersonAendernPayload, StatusPayload, PasswortAendernPayload } from '../models/clientPayload';
import { MessageService } from './message.service';
import { User } from '../models/user';
import { SessionToken } from '../models/serverResponse';
import { ActionFactory, PERSON_NAME, PERSON_NEWSLETTER } from '../state/actions';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { KatalogService } from '../services/katalog.service';
import { UserStore } from '../state/store/user.store';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { SessionService } from '../services/session.service';
import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl + '/mitglieder';


@Injectable()
export class ProfilService {

  /**
 * inject http und Store
 */
  constructor(private http: HttpClient
    , private messageService: MessageService
    , private katalogService: KatalogService
    , private sessionService: SessionService
    , private settingsStore: GlobalSettingsStore
    , private userStore: UserStore
    , private authTokenStore: AuthenticationTokenStore
    , private logger: LogService) {
  }

  kontoLoeschen(loeschen: boolean): void {
    this.logger.debug('Parameter: ' + loeschen);
    // const authHeaders = this.authTokenStore.getHeaders();
    const theBody = { 'neuerStatus': loeschen, 'rolle': this.userStore.getState().basisdaten.rolle };
    // if (authHeaders.get('Authorization')) {
      this.http.request('DELETE', BASE_URL + '/mitglied', { observe: 'body', body: theBody }).pipe(
        tap(data => this.logger.debug(JSON.stringify(data))),
        map(data => new APIResponsePayload<NullResponsePayload>(data)))
        .subscribe(
          (data: APIResponsePayload<NullResponsePayload>) => {
            this.messageService.handleMessage(data);
            this.sessionService.logout();
          },
          (error) => this.messageService.handleError(error)
        );
    // }
  }

  emailAendern(payload: MailadresseAendernPayload): void {
    this.logger.debug('Parameter: ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.put(BASE_URL + '/mitglied/email', payload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          const userAction = ActionFactory.loginUserAction(data.payload);
          this.userStore.dispatch(userAction);
          this.settingsStore.dispatch(userAction);

          const authToken = { xsrfToken: data.sessionToken.xsrfToken, accessToken: data.sessionToken.accessToken } as SessionToken;
          const authAction = ActionFactory.authenticationTokenAction(authToken);
          this.authTokenStore.dispatch(authAction);
          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }

  passwortAendern(payload: PasswortAendernPayload): void {
    this.logger.debug('Parameter: ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.put(BASE_URL + '/mitglied/passwort', payload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          const userAction = ActionFactory.loginUserAction(data.payload);
          this.userStore.dispatch(userAction);
          this.settingsStore.dispatch(userAction);

          const authToken = { xsrfToken: data.sessionToken.xsrfToken, accessToken: data.sessionToken.accessToken } as SessionToken;
          const authAction = ActionFactory.authenticationTokenAction(authToken);
          this.authTokenStore.dispatch(authAction);
          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }



  personAendern(payload: PersonAendernPayload): void {
    this.logger.debug('Parameter: ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.put(BASE_URL + '/mitglied/name', payload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          const userAction = {
            type: PERSON_NAME,
            user: data.payload
          };
          this.userStore.dispatch(userAction);
          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }

  newsletterAendern(payload: StatusPayload): void {
    this.logger.debug('Parameter: ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.put(BASE_URL + '/mitglied/newsletter', payload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          const userAction = {
            type: PERSON_NEWSLETTER,
            user: data.payload
          };
          this.userStore.dispatch(userAction);
          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }

  schuleWechseln(payload: SchulzuordnungAendernPayload): void {
    this.logger.debug('Parameter: ' + JSON.stringify(payload));
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.put(BASE_URL + '/lehrer/schule', payload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<User>(data)))
      .subscribe(
        (data: APIResponsePayload<User>) => {
          const userAction = ActionFactory.loginUserAction(data.payload);
          this.userStore.dispatch(userAction);
          this.settingsStore.dispatch(userAction);

          this.katalogService.loadSchule(data.payload.erweiterteKontodaten.schule.kuerzel);

          const authToken = { xsrfToken: data.sessionToken.xsrfToken, accessToken: data.sessionToken.accessToken } as SessionToken;
          const authAction = ActionFactory.authenticationTokenAction(authToken);
          this.authTokenStore.dispatch(authAction);

          this.messageService.handleMessage(data);
        },
        (error) => this.messageService.handleError(error)
      );
  }
}