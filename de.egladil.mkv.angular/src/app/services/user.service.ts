import { Injectable } from '@angular/core';
import { INACTIVE, PENDING, AppConstants } from '../core/app.constants';
import { TeilnahmeIdentifier } from '../models/user';
import { User, Teilnahme, Anmeldungsstatus, ANMELDUNG_AKTIV, ANMELDUNG_INAKTIV, ANMELDUNG_PENDING } from '../models/user';
import { OpportunityStateMap, OpportunityState } from '../models/opportunityState';
import { OPPORTUNITY_ANMELDEN, OPPORTUNITY_AUSWERTUNG, OPPORTUNITY_DOWNLOAD, OPPORTUNITY_EXCELUPLOAD, OPPORTUNITY_PROFIL, OPPORTUNITY_TEILNAHMEN } from '../models/opportunityState';
import { OPPORTUNITY_TEILNEHMERUPLOAD } from '../models/opportunityState';
import { GlobalSettings } from '../models/serverResponse';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { AktuelleTeilnahmeStore } from '../state/store/aktuelleTeilnahme.store';
import { UserStore } from '../state/store/user.store';
import { UserDelegate } from '../shared/user.delegate';
import { LogService } from 'hewi-ng-lib';
import { ReplaySubject, Observable, Subscription } from 'rxjs';

/**
 * Stellt Businesslogik im Zusammenhang mit einem angemeldeten Benutzer zur Verfügung.
 */
@Injectable()
export class UserService {

  statusAnmeldungSubject: ReplaySubject<Anmeldungsstatus>;
  opportunityStateMapSubject: ReplaySubject<OpportunityStateMap>;

  private settings$: Observable<GlobalSettings>;
  private user$: Observable<User>;

  private _user: User;
  private _aktuelleTeilnahme: Teilnahme;
  private _settings: GlobalSettings;
  private _statusAnmeldung: Anmeldungsstatus;
  private _aktuelleTeilnahme$: Observable<Teilnahme>;
  private _opportunityStateMap: OpportunityStateMap;

  private subscriptions: Subscription;

  constructor(private settingsStore: GlobalSettingsStore
    , private userStore: UserStore
    , private aktuelleTeilnahmeStore: AktuelleTeilnahmeStore
    , private userDelegate: UserDelegate
    , private logger: LogService) {

    this.logger.debug('StoreObserver new instance');
    this.statusAnmeldungSubject = new ReplaySubject<Anmeldungsstatus>(1);
    this.opportunityStateMapSubject = new ReplaySubject<OpportunityStateMap>(1);
    this._aktuelleTeilnahme$ = this.aktuelleTeilnahmeStore.stateSubject.asObservable();

    this.user$ = this.userStore.stateSubject.asObservable();
    this.settings$ = this.settingsStore.stateSubject.asObservable();

    this.subscriptions = new Subscription();
    this._init();
  }

  private _init() {
    this._opportunityStateMap = {
      ANMELDEN: OPPORTUNITY_ANMELDEN,
      TEILNAHMEN: OPPORTUNITY_TEILNAHMEN,
      DOWNLOAD: OPPORTUNITY_DOWNLOAD,
      AUSWERTUNG: OPPORTUNITY_AUSWERTUNG,
      TEILNEHMERUPLOAD: OPPORTUNITY_TEILNEHMERUPLOAD,
      EXCELUPLOAD: OPPORTUNITY_EXCELUPLOAD,
      PROFIL: OPPORTUNITY_PROFIL
    } as OpportunityStateMap;


    const settingsSubscription = this.settings$
      .subscribe(
        (settings: GlobalSettings) => {
          this.logger.debug('settingsSubscription');
          this._settings = settings;
          this._statusAnmeldung = this._determineStatusAnmeldung();
          this.statusAnmeldungSubject.next(this._statusAnmeldung);
        });

    const teilnahmeSubscription = this._aktuelleTeilnahme$.subscribe(
      (t: Teilnahme) => {
        this.logger.debug('teilnahme erhalten');
        this._aktuelleTeilnahme = t;
        this._opportunityStateMap = this._reduceOpportunityStateMap();
        this.opportunityStateMapSubject.next(this._opportunityStateMap);
      });

    const userSubscription = this.user$
      .subscribe(
        (user: User) => {
          this.logger.debug('userSubscription');
          this._user = user;
          this._statusAnmeldung = this._determineStatusAnmeldung();
          this.statusAnmeldungSubject.next(this._statusAnmeldung);
          this._opportunityStateMap = this._reduceOpportunityStateMap();
          this.opportunityStateMapSubject.next(this._opportunityStateMap);
        });


    this.subscriptions.add(settingsSubscription);
    this.subscriptions.add(userSubscription);
    this.subscriptions.add(teilnahmeSubscription);
  }

  private _reduceNeuanmeldung(): OpportunityState {
    const state = OPPORTUNITY_ANMELDEN;
    if (this._statusAnmeldung.status === PENDING) {
      state.visible = true;
      state.disabled = true;
      state.text = this._settings.startAnmeldungText;
      return state;
    }
    if (this._statusAnmeldung.status === INACTIVE) {
      state.visible = true;
      state.disabled = false;
      state.text = '';
      return state;
    }
    state.visible = false;
    state.disabled = true;
    state.text = '';
    return state;
  }

  private _reduceTeilnahmen(): OpportunityState {
    return OPPORTUNITY_TEILNAHMEN;
  }

  private _reduceDownload(): OpportunityState {
    const state = OPPORTUNITY_DOWNLOAD;
    if (this._statusAnmeldung.status === PENDING) {
      state.visible = true;
      state.disabled = true;
      state.text = AppConstants.texte.WETTBEWRB_RUHT;
      return state;
    }
    if (!this.userDelegate.isDownloadFreigegeben(this._user, this._settings)) {
      state.disabled = true;
      state.text = this.userDelegate.getTextFreigabeUnterlagen(this._user, this._settings);
      return state;
    }
    if (this._statusAnmeldung.status === INACTIVE) {
      state.disabled = true;
      state.text = AppConstants.texte.KEIN_DOWNLOAD_OHNE_TEILNAHME;
      return state;
    }
    state.disabled = false;
    state.text = '';
    return state;
  }

  private _reduceAuswertung(): OpportunityState {
    const state = OPPORTUNITY_AUSWERTUNG;
    this.logger.debug('_reduceAuswertung start');
    // if (!this.userDelegate.isDownloadFreigegeben(this._user, this._settings) || this._statusAnmeldung.status === PENDING) {
    if (this._settings.wettbewerbBeendet) {
      state.disabled = true;
      state.text = AppConstants.texte.WETTBEWRB_RUHT;
      return state;
    }
    if (this._statusAnmeldung.status === INACTIVE) {
      state.disabled = true;
      state.text = AppConstants.texte.KEINE_AUSWERTUNG_OHNE_TEILNAHME;
      return state;
    }
    if (this._aktuelleTeilnahme && this._aktuelleTeilnahme.anzahlUploads > 0) {
      state.disabled = true;
      state.text = AppConstants.texte.KEINE_URKUNDEN_WEGEN_UPLOADS;
      return state;
    }
    state.disabled = false;
    state.text = AppConstants.texte.AUSWERTUNG_DESCRIPTION;
    return state;
  }

  private _reduceExcelUpload(): OpportunityState {
    const state = OPPORTUNITY_EXCELUPLOAD;
    if (!this.userDelegate.isDownloadFreigegeben(this._user, this._settings) || this._statusAnmeldung.status === PENDING) {
      state.disabled = true;
      state.text = AppConstants.texte.WETTBEWRB_RUHT;
      this.logger.debug('kein Excelupload weil Wettbewerb ruht');
      return state;
    }
    if (this._statusAnmeldung.status === INACTIVE) {
      state.disabled = true;
      state.text = AppConstants.texte.KEIN_UPLOAD_OHNE_TEILNAHME;
      this.logger.debug('kein Excelupload da nicht angemeldet');
      return state;
    }
    if (this._aktuelleTeilnahme && this._aktuelleTeilnahme.anzahlUploads === 0 && this._aktuelleTeilnahme.anzahlLoesungszettel > 0) {
      state.disabled = true;
      state.text = AppConstants.texte.KEIN_UPLOAD_WEGEN_URKUNDEN;
      this.logger.debug('kein Excelupload wegen online-teilnehmern');
      return state;
    }
    state.disabled = false;
    state.text = AppConstants.texte.UPLOAD_EXCEL_DESCRIPTION;
    this.logger.debug('Excelupload möglich');
    return state;
  }

  private _reduceProfil(): OpportunityState {
    const state = OPPORTUNITY_PROFIL;
    if (this.userDelegate.isLehrer(this._user)) {
      state.text = AppConstants.texte.PROFIL_DESCR_LEHRER;
    } else {
      state.text = AppConstants.texte.PROFIL_DESCR_PRIVAT;
    }
    return state;
  }

  private _reduceOpportunityStateMap(): OpportunityStateMap {
    const neuanmeldung = this._reduceNeuanmeldung();
    const teilnahmen = this._reduceTeilnahmen();
    const download = this._reduceDownload();
    const auswertung = this._reduceAuswertung();
    const excelUpload = this._reduceExcelUpload();
    const profil = this._reduceProfil();

    const result = {
      ANMELDEN: neuanmeldung,
      TEILNAHMEN: teilnahmen,
      DOWNLOAD: download,
      AUSWERTUNG: auswertung,
      EXCELUPLOAD: excelUpload,
      PROFIL: profil
    } as OpportunityStateMap;

    return result;
  }

  private _determineStatusAnmeldung(): Anmeldungsstatus {
    this.logger.debug('_determineStatusAnmeldung');
    if (this._aktuelleTeilnahme && this._aktuelleTeilnahme !== undefined) {
      return ANMELDUNG_AKTIV;
    }
    if (this._settings && this._settings.neuanmeldungFreigegeben) {
      return ANMELDUNG_INAKTIV;
    }
    return ANMELDUNG_PENDING;
  }

  getAktuelleTeilnahme(): Teilnahme {
    return this._aktuelleTeilnahme;
  }

  getIdentifierAktuelleTeilnahme(): TeilnahmeIdentifier {
    if (this._aktuelleTeilnahme) {
      return this._aktuelleTeilnahme.teilnahmeIdentifier;
    }
    return null;
  }
}
