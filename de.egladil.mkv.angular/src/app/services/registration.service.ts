
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { APIResponsePayload } from '../models/serverResponse';
import { MessageService } from './message.service';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { Privatregistrierung, Lehrerregistrierung } from '../models/clientPayload';
import { APIMessage } from '../models/serverResponse';
import { LogService } from 'hewi-ng-lib';
import { HttpClient } from '@angular/common/http';

const BASE_URL = environment.apiUrl;


@Injectable()
export class RegistrationService {


	/**
	 * inject http und Store
	 */
  constructor(private http: HttpClient
    , private messageService: MessageService
    , private authTokenStore: AuthenticationTokenStore
    , private logger: LogService) {

  }

  registerPrivat(requestPayload: Privatregistrierung): void {
    this.logger.debug('start register privat');
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.post(BASE_URL + '/registrierungen/privat', requestPayload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      // mit dem umgewandelten Objekt den Kontruktor füttern. Ergebnis ist ein initialisierter User
      map(data => new APIResponsePayload<APIMessage>(data)))
      .subscribe(
        (data: APIResponsePayload<APIMessage>) => {
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        });
  }

  registerLehrer(requestPayload: Lehrerregistrierung): void {
    this.logger.debug('start register privat');
    // const authHeaders = this.authTokenStore.getHeaders();
    this.http.post(BASE_URL + '/registrierungen/lehrer', requestPayload, { observe: 'body' }).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      // mit dem umgewandelten Objekt den Kontruktor füttern. Ergebnis ist ein initialisierter User
      map(data => new APIResponsePayload<APIMessage>(data)))
      .subscribe(
        (data: APIResponsePayload<APIMessage>) => {
          this.messageService.handleMessage(data);
        },
        (error) => {
          this.logger.debug('login error');
          this.messageService.handleError(error);
        }
      );
  }
}