
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { LogService } from 'hewi-ng-lib';

import { HateoasMKVAction, ADD, CLEAR, ActionFactory } from '../state/actions';
import { AdvVereinbarungAntrag } from '../models/clientPayload';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { APIResponsePayload, HateoasPayload, HATEOAS_KONTEXT_ADV } from '../models/serverResponse';
import { DownloadDelegate } from '../shared/download.delegate';
import { HateoasPayloadStore } from '../state/store/hateoasPayload.store';
import { Progress, DOWNLOADART_ADV } from '../models/progress';
import { ProgressStore } from '../state/store/progress.store';
import { UserStore } from '../state/store/user.store';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';





const BASE_URL = environment.apiUrl + '/adv/vereinbarungen';


@Injectable()
export class AdvService {


  constructor(private http: HttpClient
    , private _authTokenStore: AuthenticationTokenStore
    , private _downloadDelegate: DownloadDelegate
    , private _hateoasPayloadStore: HateoasPayloadStore
    , private _messageService: MessageService
    , private _progressStore: ProgressStore
    , private _userStore: UserStore
    , private logger: LogService) {
  }

  createAdvAntrag(user: User, advAntrag: AdvVereinbarungAntrag): void {

    // const authHeaders = this._authTokenStore.getHeaders();

    // this.http.post(BASE_URL, advAntrag, { headers: authHeaders }).pipe(
    this.http.post(BASE_URL, advAntrag).pipe(
      tap(data => this.logger.debug(JSON.stringify(data))),
      map(data => new APIResponsePayload<HateoasPayload>(data)))
      .subscribe(
        (data: APIResponsePayload<HateoasPayload>) => {
          const _payload = data.payload;
          _payload.kontext = HATEOAS_KONTEXT_ADV;
          const action = {
            type: ADD,
            payload: _payload
          } as HateoasMKVAction;
          this._hateoasPayloadStore.dispatch(action);

          const _user = Object.assign({}, new User({}), user);
          _user.erweiterteKontodaten.advVereinbarungVorhanden = true;
          const userAction = ActionFactory.updateUserAction(_user);
          this._userStore.dispatch(userAction);

          this._messageService.handleMessage(data);
        },
        (error) => {
          const action = {
            type: CLEAR,
            payload: null
          } as HateoasMKVAction;
          this._hateoasPayloadStore.dispatch(action);
          this.logger.debug('AdvVereinbarung erzeugen error');
          this._messageService.handleError(error);
        });
  }

  druckeAdvVereinbarung(schulkuerzel: string) {
    this.logger.debug('druckeAdvVereinbarung ' + schulkuerzel);

    this._setProgress(true);

    // const authHeaders = this._authTokenStore.getHeaders();
    const url = BASE_URL + '/' + schulkuerzel;

    // Observable<HttpResponse<Blob>>
    this.http.get(url, {observe: 'response', responseType: 'blob' }).subscribe(
      response => {
        this._downloadDelegate.saveToFileSystem(response);
        this._setProgress(false);
      }, (error) => {
        this.logger.debug('error beim download der Auswertung mit downloadcode ' + schulkuerzel);
        this._messageService.handleError(error);
        this._setProgress(false);
      });
  }

  private _setProgress(running: boolean): void {
    this._progressStore.dispatch({
      type: ADD,
      progress: new Progress(DOWNLOADART_ADV, running)
    });
  }
}
