import {Component, OnInit, OnDestroy, Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {Schule, KatalogItem, KatalogMap, KatalogItemMap, LAND, ORT, SCHULE} from '../models/kataloge';

import {ActionFactory, CLEAR_KATALOG, SELECT, DESELECT, STATIC_AGB, STATIC_KONTAKT, CLEAR, MKVAction, KatalogItemAction} from '../state/actions';
import {KatalogStore} from '../state/store/katalog.store';
import {KatalogItemStore} from '../state/store/katalogItem.store';

import {KatalogService} from '../services/katalog.service';

import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';
import {environment} from '../../environments/environment';



@Component({
  selector: 'mkv-schulauswahl',
  templateUrl: './schulauswahl.component.html',
  providers: []
})

@Injectable()
export class SchulauswahlComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;
  private laender: KatalogItem[];
  orte: KatalogItem[];
  schulen: KatalogItem[];

  selectedLand: KatalogItem;
  selectedLandKuerzel: string;

  selectedOrt: KatalogItem;
  selectedOrtKuerzel: string;
  selectOrtDisabled: boolean;

  selectedSchule: KatalogItem;
  selectedSchuleKuerzel: string;
  selectSchuleDisabled: boolean

  private kataloge$: Observable<KatalogMap>;
  private katalogItems$: Observable<KatalogItemMap>;

  private orte$: Observable<KatalogItem[]>;
  private schulen$: Observable<KatalogItem[]>;
  private staticContent$: Observable<string>;



  showSelection = !environment.production;
  private selectDisabled: boolean;


  constructor(private router: Router
    , private katalogStore: KatalogStore
    , private katalogItemStore: KatalogItemStore
    , private katalogService: KatalogService
    , private logger: LogService) {

    this.subscriptions = new Subscription();
    this.kataloge$ = this.katalogStore.stateSubject.asObservable();
    this.katalogItems$ = this.katalogItemStore.stateSubject.asObservable();

    this.logger.debug('SchulauswahlComponent created');
  }

  ngOnInit() {
    this.selectedLand = null;
    this.selectedLandKuerzel = null;
    this.selectedOrtKuerzel = null;
    this.selectedOrt = null;
    this.selectedSchuleKuerzel = null;
    this.selectedSchule = null;
    this.selectDisabled = false;
    this.selectOrtDisabled = true;
    this.selectSchuleDisabled = true;

    const katalogeSubscription = this.kataloge$
      .subscribe((map: KatalogMap) => {
        this.orte = map.ort;
        this.schulen = map.schule;

        if (this.orte && this.orte.length > 0) {
          this.selectOrtDisabled = false;
        }
        if (this.schulen && this.schulen.length > 0) {
          this.selectSchuleDisabled = false;
        }
      });

    const katalogItemsSubscription = this.katalogItems$
      .subscribe((map: KatalogItemMap) => {
        const oldLand = this.selectedLand;
        this.selectedLand = map.land;
        this.selectedOrt = map.ort;
        this.selectedSchule = map.schule;
        if (map.land !== null && map.land !== oldLand) {
          this.selectedLandKuerzel = this.selectedLand.kuerzel;
          this.selectedOrt = null;
          this.selectedSchule = null;
          this.selectOrtDisabled = true;
          this.selectSchuleDisabled = true;
          this.katalogService.loadOrte(this.selectedLand);

        }
      });

    this.subscriptions.add(katalogeSubscription);
    this.subscriptions.add(katalogItemsSubscription);

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  selectLand(value): void {
    this.logger.debug('land selected');
  }

  onOrtSelected() {
    if (this.selectDisabled) {
      return;
    }
    const value = this.orte.find(ort => ort.kuerzel === this.selectedOrtKuerzel);
    if (this.selectedLand && value !== undefined) {
      this.selectedOrt = value;
      if (this.selectedOrt.kuerzel !== 'LEER' && this.selectedOrt.kinder.length === 0) {
        this.katalogService.loadSchulen(this.selectedLand, value);
      }
      this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(SELECT, ORT, value));
    } else {
      const action = {type: CLEAR_KATALOG, key: ORT};
      this.katalogStore.dispatch(action);
      this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, ORT, null));
    }
    this.logger.debug('Ort selected - katalogItemStore ' + JSON.stringify(this.katalogItemStore.getState()));
  }

  onSchuleSelected() {
    if (this.selectDisabled) {
      return;
    }
    const value = this.schulen.find(schule => schule.kuerzel === this.selectedSchuleKuerzel);
    if (this.selectedSchuleKuerzel && this.selectedOrt && value) {
      this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(SELECT, SCHULE, value));
    } else {
      this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, SCHULE, null));
    }
    this.logger.debug('Schule selected - katalogItemStore ' + JSON.stringify(this.katalogItemStore.getState()));
  }

  gotoNeueSchule() {
    const action = {'type': CLEAR} as MKVAction;
    this.katalogItemStore.dispatch(action);
    this.katalogStore.dispatch(action);
    this.router.navigate(['/schule']);
  }
}