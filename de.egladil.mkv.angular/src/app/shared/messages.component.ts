import {Component, OnInit, OnDestroy, Injectable} from '@angular/core';
import {ApiMessageStore} from '../state/store/apiMessage.store';
import {APIMessage, INFO, WARN, ERROR} from '../models/serverResponse';
import {MessageService} from '../services/message.service';
import {ActionFactory, CLEAR} from '../state/actions';
import {Subscription, Observable, Observer} from 'rxjs';
import { LogService } from 'hewi-ng-lib';



@Component({
  selector: 'mkv-msg',
  templateUrl: './messages.component.html',
  providers: []
})

/**
 * Die MessagesComponent zeigt eine INFO-, WARN- oder ERROR-Meldung an. Getriggert wird sie über den apiMessage.store, an dem sich
 * die MessageComponent registriert hat. Dieser wird seinerseits durch den messageService aktualisiert.
 */
@Injectable()
export class MessagesComponent implements OnInit, OnDestroy {

  errorMessage: string;
  hasError: boolean;
  warningMessage: string;
  hasWarning: boolean;
  infoMessage: string;
  hasInfo: boolean;

  private subscriptions: Subscription;

	/**
	 * private Observable Streams (Referenz auf PingStore)
	 */
  private apiMessage$: Observable<APIMessage>; // wird durch den store gefüllt

  constructor(private store: ApiMessageStore, private messageService: MessageService, private logger: LogService) {
    this.subscriptions = new Subscription();
    this.apiMessage$ = this.store.stateSubject.asObservable();
  }

  ngOnInit() {


    /**
 * subscribe to the messageState Subject of the Store
 */
    const messageSubscription = this.apiMessage$
      .subscribe(
      (message) => {
        this.logger.debug('message.level=' + message.level);
        this.reset();
        switch (message.level) {
          case INFO:
            if (message.message !== null && message.message !== undefined) {
              if (message.message.length > 0) {
              this.infoMessage = message.message;
              this.hasInfo = true;
              } else {
                this.logger.debug('leere Infomessage bekommen');
              }
            }
            break;
          case WARN:
            this.warningMessage = message.message;
            this.hasWarning = true;
            break;
          case ERROR:
            this.errorMessage = message.message;
            this.hasError = true;
            break;
          default:
            break;
        }
      });


    this.subscriptions.add(messageSubscription);

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  clearMessage() {
    this.messageService.clearMessage();
  }

  private reset(): void {
    this.errorMessage = null;
    this.hasError = false;
    this.warningMessage = null;
    this.hasWarning = false;
    this.infoMessage = null;
    this.hasInfo = false;
  }

}