import { Component, Input, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { AdvService } from '../services/adv.service';
import { MessageService } from '../services/message.service';
import { Progress, DOWNLOADART_ADV } from '../models/progress';
import { ProgressStore } from '../state/store/progress.store';
import { Schule } from '../models/kataloge';


@Component({
  selector: 'mkv-adv-download',
  templateUrl: './download-adv.component.html'
})

export class DownloadAdvComponent implements OnInit, OnDestroy {

  @Input() schule: Schule;

  processing: boolean;

  private _subscription: Subscription;
  private _progress$: Observable<Progress>;


  constructor(private _advService: AdvService
    , private _messageService: MessageService
    , private _progressStore: ProgressStore) {

    this._subscription = new Subscription();
    this._progress$ = this._progressStore.stateSubject.asObservable();
  }

  ngOnInit() {

    const progressSubscripton = this._progress$.subscribe((payload: Progress) => {
      if (payload && payload.kontext === DOWNLOADART_ADV && payload.running) {
        this.processing = payload.running;
      } else {
        this.processing = false;
      }
    });

    this._subscription.add(progressSubscripton);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._messageService.clearMessage();
  }

  advDrucken(): void {
    const schulkuerzel = this.schule.kuerzel;
    if (schulkuerzel) {
      this._advService.druckeAdvVereinbarung(schulkuerzel);
    }
  }
}