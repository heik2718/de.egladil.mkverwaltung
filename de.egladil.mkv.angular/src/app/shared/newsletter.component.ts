import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfilService } from '../services/profil.service';
import { StatusPayload } from '../models/clientPayload';
import { User } from '../models/user';
import { UserStore } from '../state/store/user.store';
import { AppConstants } from '../core/app.constants';
import { Subscription, Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-newsletter',
  templateUrl: './newsletter.component.html',
})

export class NewsletterComponent implements OnInit, OnDestroy {

  btnTextMailstatus: string;
  cssClass: string;

  textNewsletter: string;

  private subscriptions: Subscription;
  private alterStatus: boolean;
  private rolle: string;

  private user$: Observable<User>;

  constructor(r: ActivatedRoute
    , private userStore: UserStore
    , private profilService: ProfilService
    , private logger: LogService) {

    this.subscriptions = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();

  }

  ngOnInit() {
    const userSubscription = this.user$
      .subscribe(
        (theUser: User) => {
          this.logger.debug('user erhalten');
          this.alterStatus = theUser.erweiterteKontodaten && theUser.erweiterteKontodaten.mailbenachrichtigung || false;
          this.rolle = theUser.basisdaten && theUser.basisdaten.rolle || null;
          this.btnTextMailstatus = this.alterStatus ? AppConstants.buttontexte.BTN_NEWSLETTER_DEACTIVATE : AppConstants.buttontexte.BTN_NEWSLETTER_ACTIVATE;
          this.cssClass = this.alterStatus ? 'btn-primary' : 'btn-warning';
          if (theUser.erweiterteKontodaten && theUser.erweiterteKontodaten.mailbenachrichtigung) {
            this.textNewsletter = '';
          } else {
            this.textNewsletter = AppConstants.buttontexte.CHK_NEWSLETTER;
          }
        });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  newletterAendern(): void {
    const neuerStatus = this.alterStatus ? false : true;
    if (!neuerStatus) {
      this.textNewsletter = AppConstants.buttontexte.CHK_NEWSLETTER;
    } else {
      this.textNewsletter = '';
    }
    this.logger.debug('alter Status=' + this.alterStatus + ', neuer Status=' + neuerStatus);
    const payload = {} as StatusPayload;
    payload.neuerStatus = neuerStatus;
    payload.rolle = this.rolle;
    this.profilService.newsletterAendern(payload);
  }
}

