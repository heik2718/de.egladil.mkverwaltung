import {Component, OnInit, OnDestroy} from '@angular/core';
import {UserStore} from '../state/store/user.store';
import {User, Anmeldungsstatus} from '../models/user';
import {UserDelegate} from '../shared/user.delegate';
import {UserService} from '../services/user.service';
import {Subscription, Observable} from 'rxjs';
import { LogService } from 'hewi-ng-lib';



@Component({
  selector: 'mkv-user',
  templateUrl: './userdetails.component.html',
  providers: []
})

export class UserdetailsComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;
  private user$: Observable<User>;
  private statusAnmeldung$: Observable<Anmeldungsstatus>;

  user: User;
  statusAnmeldung: Anmeldungsstatus;

  constructor(private userStore: UserStore
    , private userDelegate: UserDelegate
    , private userService: UserService
    , private logger: LogService) {

    this.subscriptions = new Subscription();
    this.user$ = this.userStore.stateSubject.asObservable();
    this.statusAnmeldung$ = this.userService.statusAnmeldungSubject.asObservable();
  }

  ngOnInit() {
    const userSubscription = this.user$.subscribe((u: User) => {
      if (u) {
        this.user = u;
      }
    });


    const statusAnmeldungSubscription = this.statusAnmeldung$
      .subscribe(
      (value: Anmeldungsstatus) => {
        this.statusAnmeldung = value;
        this.logger.debug(JSON.stringify(this.statusAnmeldung));
      });

    this.subscriptions.add(userSubscription);
    this.subscriptions.add(statusAnmeldungSubscription);

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  showSchulzuordnung(): boolean {
    return this.userDelegate.isLehrer(this.user);
  }
}