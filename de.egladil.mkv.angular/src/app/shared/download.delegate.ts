import { Injectable } from '@angular/core';
import { LogService } from 'hewi-ng-lib';
import { HttpResponse } from '@angular/common/http';
import * as fileSaver from 'file-saver'; // npm i --save file-saver

@Injectable()
export class DownloadDelegate {

  constructor(private logger: LogService) { }

  saveToFileSystem(response: HttpResponse<Blob>) {
    const contentDispositionHeader: string = response.headers.get('Content-Disposition');
    const contentType: string = response.headers.get('Content-Type');
    const parts: string[] = contentDispositionHeader.split(';');
    this.logger.debug(JSON.stringify(parts));
    let filename = parts[1].split('=')[1];
    this.logger.debug('dateiname=' + filename);
    filename = filename.replace('"', '');
    filename = filename.replace('"', '');
    const blob = new Blob([response.body], { type: contentType });
    fileSaver.saveAs(blob, filename);
  }
}
