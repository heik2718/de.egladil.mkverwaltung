import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Location, DOCUMENT } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';

import { ActionFactory, SELECT } from '../state/actions';
import { APIMessage, INFO, WARN, ERROR } from '../models/serverResponse';
import { ApiMessageStore } from '../state/store/apiMessage.store';
import { mkvEmailValidator } from '../validators/app.validators';
import { SchuleAnschriftKatalogantrag } from '../models/clientPayload';
import { KatalogStore } from '../state/store/katalog.store';
import { KatalogItemStore } from '../state/store/katalogItem.store';
import { KatalogItem, KatalogMap, KatalogItemMap, LAND } from '../models/kataloge';
import { AuthenticationTokenStore } from '../state/store/authenticationToken.store';
import { KatalogService } from '../services/katalog.service';
import { MessageService } from '../services/message.service';
import { Subscription ,  Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './neueSchule.component.html',
})

export class NeueSchuleComponent implements OnInit, OnDestroy {

  neueSchuleForm: FormGroup;
  email: AbstractControl;
  nameLand: AbstractControl;
  name: AbstractControl;
  plz: AbstractControl;
  ort: AbstractControl;
  strasse: AbstractControl;
  hausnummer: AbstractControl;
  kleber: AbstractControl;

  selectedLand: KatalogItem;

  showNameLand: boolean;
  submitDisabled: boolean;
  cancelRequested: boolean;

  private katalogantrag: SchuleAnschriftKatalogantrag;
  private subscriptions: Subscription;

  private katalogItems$: Observable<KatalogItemMap>;
  private apiMessage$: Observable<APIMessage>;


  constructor(private fb: FormBuilder
    , private katalogStore: KatalogStore
    , private katalogItemStore: KatalogItemStore
    , private authTokenStore: AuthenticationTokenStore
    , private katalogService: KatalogService
    , private messageService: MessageService
    , private messageStore: ApiMessageStore
    , private router: Router
    , private location: Location
    , @Inject(DOCUMENT) private document: Document
    , private logger: LogService) {



    this.neueSchuleForm = fb.group({
      'email': ['', [Validators.required, mkvEmailValidator]],
      'nameLand': ['', [Validators.maxLength(100)]],
      'name': ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      'plz': ['', [Validators.required, Validators.maxLength(10)]],
      'ort': ['', [Validators.required, Validators.maxLength(100)]],
      'strasse': ['', [Validators.maxLength(89)]],
      'hausnummer': ['', [Validators.maxLength(10)]],
      'kleber': ['']
    });

    this.showNameLand = false;
    this.cancelRequested = false;

    this.email = this.neueSchuleForm.controls['email'];
    this.nameLand = this.neueSchuleForm.controls['nameLand'];
    this.name = this.neueSchuleForm.controls['name'];
    this.plz = this.neueSchuleForm.controls['plz'];
    this.ort = this.neueSchuleForm.controls['ort'];
    this.strasse = this.neueSchuleForm.controls['strasse'];
    this.hausnummer = this.neueSchuleForm.controls['hausnummer'];
    this.kleber = this.neueSchuleForm.controls['kleber'];

    this.subscriptions = new Subscription();
    this.katalogItems$ = this.katalogItemStore.stateSubject.asObservable();
    this.apiMessage$ = this.messageStore.stateSubject.asObservable();

    this.logger.debug('NeueSchuleComponent created');

  }

  ngOnInit() {
    this.selectedLand = null;
    this.submitDisabled = false;

    const katalogItemsSubscription = this.katalogItems$
      .subscribe((map: KatalogItemMap) => {
        this.selectedLand = map.land;
      });

    const apiMessageSubscription = this.apiMessage$
      .subscribe((apiMessage: APIMessage) => {
        if (apiMessage.level === INFO) {
          this.submitDisabled = true;
        } else {
          this.submitDisabled = false;
        }
      });

    this.subscriptions.add(katalogItemsSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  isSubmitDisabled(): boolean {
    return this.submitDisabled || this.selectedLand === null && (this.nameLand.value === null || this.nameLand.value === '');
  }

  toggleNameLand(): void {
    this.showNameLand = !this.showNameLand;
  }

  onSubmit(value: any): void {
    if (this.cancelRequested) {
      return;
    }
    this.submitDisabled = true;
    this.logger.debug('you submitted value:' + value);
    const payload = value as SchuleAnschriftKatalogantrag;

    const antrag = {
      'type': 'anschrift',
      'email': payload.email ? payload.email.trim() : '',
      'nameLand': payload.landName ? payload.landName.trim() : '',
      'name': payload.name ? payload.name.trim() : '',
      'plz': payload.plz ? payload.plz.trim() : '',
      'ort': payload.ort ? payload.ort.trim() : '',
      'strasse': payload.strasse ? payload.strasse.trim() : '',
      'hausnummer': payload.hausnummer ? payload.hausnummer.trim() : '',
      'kleber': payload.kleber
    };

    payload.type = 'anschrift';
    const landKuerzel = this.selectedLand ? this.selectedLand.kuerzel : 'UNBEK';
    this.katalogService.neueSchule(landKuerzel, antrag);
    this.document.body.scrollTop = 0;
  }

  cancel() {
    this.cancelRequested = true;
    this.messageService.clearMessage();
    this.location.back(); // <-- go back to previous location on cancel
  }
}

