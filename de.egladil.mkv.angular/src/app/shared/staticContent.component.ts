import {Component, OnInit, OnDestroy, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {STATIC_AGB, STATIC_KONTAKT, STATIC_ZUGANGSDATEN, CLEAR, ActionFactory} from '../state/actions';
import {StaticContentStore} from '../state/store/staticContent.store';
import {Subscription, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import { LogService } from 'hewi-ng-lib';

@Component({
  templateUrl: './staticContent.component.html'
})

@Injectable()
export class StaticContentComponent implements OnInit, OnDestroy {

  kontaktEnabled: boolean;
  agbEnabled: boolean;
  zugangsdatenEnabled: boolean;
  titel: string;

  private staticContent$: Observable<string>;
  private subscriptions: Subscription;

  constructor(private staticContentStore: StaticContentStore
    , private router: Router
    , private location: Location
    , private logger: LogService) {

    this.subscriptions = new Subscription();
    this.staticContent$ = staticContentStore.stateSubject.asObservable();
  }


  ngOnInit() {
    this.reset();
    this.logger.debug('init static content');

    const staticContentSubscription = this.staticContent$
      .subscribe(
      (message: string) => {
        switch (message) {
          case STATIC_AGB:
            this.reset();
            this.titel = 'Datenschutzerklärung';
            this.agbEnabled = true;
            break;
          case STATIC_KONTAKT:
            this.reset();
            this.titel = 'Impressum';
            this.kontaktEnabled = true;
            break;
          case STATIC_ZUGANGSDATEN:
            this.reset();
            this.titel = 'Zugangsdaten vergessen?'
            this.zugangsdatenEnabled = true;
            break;
          default:
            this.reset();
            this.logger.debug('kein Content ausgewählt');
            this.router.navigate(['/landing']);
            break;
        }
      });

    this.subscriptions.add(staticContentSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  cancel(): void {
    const action = {
      type: CLEAR
    };

    this.staticContentStore.dispatch(action);
    this.location.back(); // <-- go back to previous location on cancel
  }

  private reset(): void {
    this.titel = '';
    this.agbEnabled = false;
    this.kontaktEnabled = false;
    this.zugangsdatenEnabled = false;
  }
}


