import { Component, OnInit, OnDestroy } from '@angular/core';

import { AppConstants } from '../core/app.constants';

import { OpportunityStateMap, OpportunityState, ANMELDEN, AUSWERTUNG } from '../models/opportunityState';

import { UserService } from '../services/user.service';


import { Subscription, Observable } from 'rxjs';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-urkundenwarnung',
  templateUrl: './urkundenwarnung.component.html'
})
export class UrkundenwarnungComponent implements OnInit, OnDestroy {

  anmeldenState: OpportunityState;
  auswertungState: OpportunityState;

  private _opportunityStateMap$: Observable<OpportunityStateMap>; // aus userService

  private _subscription: Subscription;

  constructor(private userService: UserService
    , private logger: LogService) {

    this._subscription = new Subscription();

    this._opportunityStateMap$ = this.userService.opportunityStateMapSubject.asObservable();


  }

  ngOnInit() {
    const opportunityStateSubscription = this._opportunityStateMap$.subscribe(
      (map: OpportunityStateMap) => {
        this.anmeldenState = map.ANMELDEN;
        this.auswertungState = map.AUSWERTUNG;
      });

    this._subscription.add(opportunityStateSubscription);

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  showLinkUrkunden(): boolean {
    const result = this.auswertungState.text === AppConstants.texte.KEINE_URKUNDEN_WEGEN_UPLOADS;
    return result;
  }
}
