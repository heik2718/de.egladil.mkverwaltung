import {User, Teilnahme} from '../models/user';
import {GlobalSettings} from '../models/serverResponse';
import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class UserDelegate {

  private ROLLE_LEHRER = 'MKV_LEHRER';
  private ROLLE_PRIVAT = 'MKV_PRIVAT';
  private callCount: number;

  constructor(private logger: LogService) {
    this.callCount = 0;
  }

  isLehrer(user: User): boolean {
    this.callCount++;
    if (!user) {
      return false;
    }
    return user && user.basisdaten && user.basisdaten.rolle === this.ROLLE_LEHRER || false;
  }

  isPrivat(user: User): boolean {
    this.callCount++;
    if (!user) {
      return false;
    }
    return user && user.basisdaten && user.basisdaten.rolle === this.ROLLE_PRIVAT || false;
  }

  isDownloadFreigegeben(user: User, settings: GlobalSettings): boolean {
    if (!user || !settings) {
      return false;
    }

    if (this.isLehrer(user)) {
      return settings.downloadFreigegebenLehrer;
    } else {
      return settings.downloadFreigegebenPrivat;
    }
  }

  getTextFreigabeUnterlagen(user: User, settings: GlobalSettings): string {
    if (!user || !settings) {
      return '';
    }
    let datum = '';
    if (this.isLehrer(user)) {
      datum = settings.datumFreigabeLehrer;
    } else {
      datum = settings.datumFreigabePrivat;
    }
    return 'Die Unterlagen werden am ' + datum + ' freigegeben.';
  }
}


