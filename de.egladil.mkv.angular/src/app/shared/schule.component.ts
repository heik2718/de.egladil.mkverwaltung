import { Component, Input, HostBinding } from '@angular/core';
import { Schule } from '../models/kataloge';


@Component({
	  selector: 'mkv-schule',
	  templateUrl: './schule.component.html'
})

export class SchuleComponent {

	@Input() schule: Schule;


	constructor() {}

}