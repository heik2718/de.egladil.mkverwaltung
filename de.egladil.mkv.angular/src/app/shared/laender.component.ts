import {Component, OnInit, OnDestroy, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ActionFactory, CLEAR_KATALOG, SELECT, DESELECT, STATIC_AGB, STATIC_KONTAKT, MKVAction, KatalogItemAction} from '../state/actions';
import {FormBuilder, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import {KatalogStore} from '../state/store/katalog.store';
import {KatalogItemStore} from '../state/store/katalogItem.store';
import {Schule, KatalogItem, KatalogMap, KatalogItemMap, LAND, ORT, SCHULE} from '../models/kataloge';
import {KatalogService} from '../services/katalog.service';
import {StaticContentStore} from '../state/store/staticContent.store';
import {Subscription, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-laender',
  templateUrl: './laender.component.html',
  providers: []
})

@Injectable()
export class LaenderComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;
  laender: KatalogItem[];
  land: AbstractControl;

  selectedLand: KatalogItem;
  selectedLandKuerzel: string;


  //  private selectedOrt: KatalogItem;
  //  private selectedSchule: KatalogItem;

  private kataloge$: Observable<KatalogMap>;
  private katalogItems$: Observable<KatalogItemMap>;

  private laender$: Observable<KatalogItem[]>;
  private staticContent$: Observable<string>;

  showSelection = !environment.production;

  private selectDisabled: boolean;

  constructor(private router: Router
    , private fb: FormBuilder
    , private katalogStore: KatalogStore
    , private katalogItemStore: KatalogItemStore
    , private katalogService: KatalogService
    , private staticContentStore: StaticContentStore
    , private logger: LogService) {

    this.land = new FormControl(null, [Validators.required]);
    this.subscriptions = new Subscription();
    this.kataloge$ = this.katalogStore.stateSubject.asObservable();
    this.katalogItems$ = this.katalogItemStore.stateSubject.asObservable();
    this.staticContent$ = this.staticContentStore.stateSubject.asObservable();
    this.logger.debug('LaenderComponent created');
  }

  ngOnInit() {
    this.selectedLand = null;
    this.selectedLandKuerzel = null;
    this.selectDisabled = false;

    const staticContentSubscription = this.staticContent$
      .subscribe((content: string) => {
        if (STATIC_AGB === content || STATIC_KONTAKT === content) {
          this.selectDisabled = true;
        }
      });

    const katalogeSubscription = this.kataloge$
      .subscribe((map: KatalogMap) => {
        this.laender = map.land;
      });

    const katalogItemsSubscription = this.katalogItems$
      .subscribe((map: KatalogItemMap) => {
        this.selectedLand = map.land;
        if (this.selectedLand) {
          this.selectedLandKuerzel = this.selectedLand.kuerzel;
        }
      });

    this.subscriptions.add(katalogeSubscription);
    this.subscriptions.add(katalogItemsSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  selectLand(value): void {
    this.logger.debug('land selected');
  }

  onLandSelected() {
    if (this.selectDisabled || !this.laender) {
      return;
    }
    const value = this.laender.find(land => land.kuerzel === this.selectedLandKuerzel);
    if (value !== undefined) {
      this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(SELECT, LAND, value));
      if (value.kuerzel !== 'LEER') {
        this.katalogService.loadOrte(value);
      } else {
        const action = {type: CLEAR_KATALOG, key: ORT};
        this.katalogStore.dispatch(action);
        this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, ORT, null));
      }
    } else {
      this.katalogItemStore.dispatch(ActionFactory.katalogItemAction(DESELECT, LAND, null));
    }
    this.logger.debug('Land selected - katalogItemStore ' + JSON.stringify(this.katalogItemStore.getState()));
  }
}