
import {Auswertungsgruppe} from '../models/auswertung';
import {TeilnahmeIdentifier} from '../models/user';
import {Injectable} from '@angular/core';
import { LogService } from 'hewi-ng-lib';

@Injectable()
export class TeilnahmeDelegate {

  constructor(private logger: LogService) {
  }


  getPathPrefix(baseUrl: string, tIdentifier: TeilnahmeIdentifier): string {
    this.logger.debug(JSON.stringify(tIdentifier));
    return baseUrl + '/' + tIdentifier.jahr + '/' + tIdentifier.teilnahmeart + '/' + tIdentifier.kuerzel;
  }

  findAuswertungsgruppe(auswertungsgruppe: Auswertungsgruppe, kuerzel: string): Auswertungsgruppe {
    if (!auswertungsgruppe) {
      return null;
    }
    if (kuerzel === auswertungsgruppe.kuerzel) {
      return auswertungsgruppe;
    }
    if (auswertungsgruppe.auswertungsgruppen) {
      const value = auswertungsgruppe.auswertungsgruppen.find(g => g.kuerzel === kuerzel);
      return value;
    } return null;
  }
}