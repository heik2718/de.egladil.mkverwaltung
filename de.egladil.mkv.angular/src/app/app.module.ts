// modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HewiNgLibModule } from 'hewi-ng-lib';


// shared
import { environment } from '../environments/environment';
import { appRouting } from './core/app-routing.module';
import { LoginGuard } from './core/login.guard';
import { UserDelegate } from './shared/user.delegate';
import { NeueSchuleComponent } from './shared/neueSchule.component';
import { DownloadDelegate } from './shared/download.delegate';
import { TeilnahmeDelegate } from './shared/teilnahme.delegate';

// services
import { AdvService } from './services/adv.service';
import { AnmeldungService } from './services/anmeldung.service';
import { AuswertungService } from './services/auswertung.service';
import { AuswertungsgruppeService } from './services/auswertungsgruppe.service';
import { DownloadService } from './services/download.service';
import { KatalogService } from './services/katalog.service';
import { MessageService } from './services/message.service';
import { ProfilService } from './services/profil.service';
import { RegistrationService } from './services/registration.service';
import { SessionService } from './services/session.service';
import { StaticContentService } from './services/staticContent.service';
import { TeilnehmerService } from './services/teilnehmer.service';
import { TempPasswordService } from './services/temppwd.service';
import { UploadService } from './services/upload.service';

// state
import { UserService } from './services/user.service';
import { ApiMessageStore } from './state/store/apiMessage.store';
import { AuthenticationTokenStore } from './state/store/authenticationToken.store';
import { GlobalSettingsStore } from './state/store/globalSettings.store';
import { UserStore } from './state/store/user.store';
import { KatalogStore } from './state/store/katalog.store';
import { KatalogItemStore } from './state/store/katalogItem.store';
import { PrivatregistrierungStore } from './state/store/privatregistrierung.store';
import { LehrerregistrierungStore } from './state/store/lehrerregistrierung.store';
import { StaticContentStore } from './state/store/staticContent.store';
import { DateiInfoStore } from './state//store/dateiInfo.store';
import { AktuelleTeilnahmeStore } from './state/store/aktuelleTeilnahme.store';
import { TeilnehmerArrayStore } from './state/store/teilnehmerArray.store';
import { TeilnehmerStore } from './state/store/teilnehmer.store';
import { PrivaturkundenauftragStore } from './state/store/privaturkundenauftrag.store';
import { DownloadcodeStore } from './state/store/downloadcode.store';
import { AuswertungsgruppeStore } from './state/store/auswertungsgruppe.store';
import { RootgruppeStore } from './state/store/rootgruppe.store';
import { SchuleStore } from './state/store/schule.store';
import { SchuleurkundenauftragStore } from './state/store/schuleurkundenauftrag.store';
import { StaticHtmlStore } from './state/store/staticHtml.store';
import { HateoasPayloadStore } from './state/store/hateoasPayload.store';
import { ProgressStore } from './state/store/progress.store';
import { FarbschemaStore } from './state/store/farbschema.store';
import { TeilnahmenStore } from './state/store/teilnahmen.store';


// components
import { AnmeldungComponent } from './pages/teilnahmen/anmeldung/anmeldung.component';
import { AntwortbuchstabeComponent } from './pages/auswertung/antwortbuchstabe.component';
import { AppComponent } from './core/app.component';
import { AuswertungsgruppeComponent } from './pages/auswertung/auswertungsgruppe.component';
import { AuswertungsgruppeoverviewComponent } from './pages/auswertung/auswertungsgruppeoverview.component';
import { ChangeTempPwdComponent } from './pages/temppassword/changetemppwd.component';
import { CheckboxantwortzeileComponent } from './pages/auswertung/checkboxantwortzeile.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DateiInfoComponent } from './pages/download/dateiInfo.component';
import { DialogComponent } from './dialog/dialog.component';
import { DownloadComponent } from './pages/download/download.component';
import { DownloadAdvComponent } from './shared/download-adv.component';
import { EditTeilnehmerComponent } from './pages/auswertung/edit-teilnehmer.component';
import { EditTeilnehmerGuard } from './pages/auswertung/edit-teilnehmer.guard';
import { GruppenteilnehmerComponent } from './pages/auswertung/gruppenteilnehmer.component';
import { KontoLoeschenComponent } from './pages/profil/kontoLoeschen.component';
import { LaenderComponent } from './shared/laender.component';
import { LandingComponent } from './pages/landing/landing.component';
import { LoginComponent } from './pages/login/login.component';
import { MatrixantwortzeileComponent } from './pages/auswertung/matrixantwortzeile.component';
import { MessagesComponent } from './shared/messages.component';
import { EditAuswertungsgruppeComponent } from './pages/auswertung/edit-auswertungsgruppe.component';
import { NewsletterComponent } from './shared/newsletter.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { OrderTempPwdComponent } from './pages/temppassword/ordertemppwd.component';
import { ProfilComponent } from './pages/profil/profil.component';
import { ProfilEmailComponent } from './pages/profil/profil.email.component';
import { ProfilNameComponent } from './pages/profil/profil.name.component';
import { ProfilPasswortComponent } from './pages/profil/profil.passwort.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegisterLehrerComponent } from './pages/register/registerLehrer.component';
import { RegisterPrivatComponent } from './pages/register/registerPrivat.component';
import { ShowErrorComponent } from './shared/showError.component';
import { SchulauswahlComponent } from './shared/schulauswahl.component';
import { SchuleComponent } from './shared/schule.component';
import { SchulurkundenComponent } from './pages/auswertung/schulurkunden.component';
import { SchulzuordnungComponent } from './pages/profil/schulzuordnung.component';
import { StaticContentComponent } from './shared/staticContent.component';
import { TeilnahmeComponent } from './pages/teilnahmen/teilnahmeliste/teilnahme.component';
import { TeilnahmelistComponent } from './pages/teilnahmen/teilnahmeliste/teilnahmelist.component';
import { TeilnehmerListComponent } from './pages/auswertung/teilnehmer-list.component';
import { TeilnehmeroverviewComponent } from './pages/auswertung/teilnehmeroverview.component';
import { UserdetailsComponent } from './shared/userdetails.component';
import { UploadComponent } from './pages/upload/upload.component';
import { PrivaturkundenComponent } from './pages/auswertung/privaturkunden.component';
import { UrkundenwarnungComponent } from './shared/urkundenwarnung.component';

// Directives
import { LowerCaseDirective } from './directives/lower-case.directive';

// Validators

import { AdvComponent } from './adv/adv.component';
import { AuthInterceptor } from './services/auth.interceptor';
import { GlobalErrorHandler } from './error/error-handler.service';

@NgModule({
  declarations: [
    AppComponent,
    AnmeldungComponent,
    AntwortbuchstabeComponent,
    MatrixantwortzeileComponent,
    AuswertungsgruppeComponent,
    AuswertungsgruppeoverviewComponent,
    ChangeTempPwdComponent,
    CheckboxantwortzeileComponent,
    DashboardComponent,
    DateiInfoComponent,
    DialogComponent,
    DownloadComponent,
    DownloadAdvComponent,
    EditTeilnehmerComponent,
    KontoLoeschenComponent,
    LaenderComponent,
    LandingComponent,
    LoginComponent,
    LowerCaseDirective,
    MessagesComponent,
    NeueSchuleComponent,
    EditAuswertungsgruppeComponent,
    NewsletterComponent,
    NotFoundComponent,
    OrderTempPwdComponent,
    ProfilComponent,
    ProfilEmailComponent,
    ProfilNameComponent,
    ProfilPasswortComponent,
    RegisterComponent,
    RegisterLehrerComponent,
    RegisterPrivatComponent,
    SchulauswahlComponent,
    SchuleComponent,
    SchulzuordnungComponent,
    ShowErrorComponent,
    StaticContentComponent,
    TeilnahmelistComponent,
    TeilnahmeComponent,
    UploadComponent,
    PrivaturkundenComponent,
    UrkundenwarnungComponent,
    UserdetailsComponent,
    TeilnehmerListComponent,
    TeilnehmeroverviewComponent,
    GruppenteilnehmerComponent,
    SchulurkundenComponent,
    AdvComponent
  ],
  imports: [
    appRouting,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    HewiNgLibModule
  ],
  providers: [
    AdvService,
    AktuelleTeilnahmeStore,
    AnmeldungService,
    ApiMessageStore,
    AuswertungService,
    AuswertungsgruppeService,
    AuswertungsgruppeStore,
    AuthenticationTokenStore,
    DateiInfoStore,
    DownloadcodeStore,
    DownloadDelegate,
    DownloadService,
    EditTeilnehmerGuard,
    FarbschemaStore,
    GlobalSettingsStore,
    HateoasPayloadStore,
    KatalogItemStore,
    KatalogService,
    KatalogStore,
    LehrerregistrierungStore,
    LoginGuard,
    MessageService,
    PrivatregistrierungStore,
    ProfilService,
    ProgressStore,
    RegistrationService,
    RootgruppeStore,
    SessionService,
    SchuleStore,
    SchuleurkundenauftragStore,
    StaticContentService,
    StaticHtmlStore,
    StaticContentStore,
    TeilnehmerArrayStore,
    TeilnehmerService,
    TempPasswordService,
    UserDelegate,
    UserService,
    UserStore,
    UploadService,
    TeilnahmenStore,
    TeilnehmerStore,
    PrivaturkundenauftragStore,
    TeilnahmeDelegate,
    GlobalErrorHandler,
		{ provide: ErrorHandler, useClass: GlobalErrorHandler },
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		},

  ],
  bootstrap: [AppComponent]
})



export class AppModule { }
