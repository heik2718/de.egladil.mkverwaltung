import { Component, OnInit, Input, Output, OnChanges, EventEmitter, Inject } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { DOCUMENT } from "@angular/common";
import { LogService } from 'hewi-ng-lib';

@Component({
  selector: 'mkv-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale3d(.3, .3, .3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
      ])
    ])
  ]
})
export class DialogComponent implements OnInit {
  @Input() closable = false;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(@Inject(DOCUMENT) private document: Document, private logger: LogService) {
    this.logger.debug('Dialog created');
  }

  ngOnInit() {
    //    this.logger.debug('offsetTop=' + this.document.body.offsetTop + ', offsetHeight' + this.document.body.offsetHeight);
    //    this.document.body.scrollTo(0, this.document.body.offsetHeight);
  }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }
}