import { Component, OnInit, OnDestroy, SecurityContext, Inject } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';
import { DOCUMENT } from "@angular/common";
import { Subscription, Observable } from 'rxjs';





import { AdvService } from '../services/adv.service';
import { APIResponsePayload, APIMessage, GlobalSettings, DateiInfo, HateoasPayload, HATEOAS_KONTEXT_ADV } from '../models/serverResponse';
import { HateoasMKVAction, CLEAR } from '../state/actions';
import { AdvVereinbarungAntrag } from '../models/clientPayload';
import { GlobalSettingsStore } from '../state/store/globalSettings.store';
import { HateoasPayloadStore } from '../state/store/hateoasPayload.store';
import { KatalogService } from '../services/katalog.service';
import { MessageService } from '../services/message.service';
//import {Progress, DOWNLOADART_ADV} from '../models/progress';
//import {ProgressStore} from '../state/store/progress.store';
import { StaticHtmlStore } from '../state/store/staticHtml.store';
import { Schule } from '../models/kataloge';
import { SchuleStore } from '../state/store/schule.store';
import { StaticContentService } from '../services/staticContent.service';
import { User } from '../models/user';
import { UserStore } from '../state/store/user.store';
import { LogService } from 'hewi-ng-lib';



@Component({
  selector: 'mkv-adv',
  templateUrl: './adv.component.html',
  styleUrls: ['./adv.component.css']
})
export class AdvComponent implements OnInit, OnDestroy {

  advForm: FormGroup;
  schulname: AbstractControl;
  laendercode: AbstractControl;
  plz: AbstractControl;
  ort: AbstractControl;
  strasse: AbstractControl;
  hausnummer: AbstractControl;

  private _subscription: Subscription;
  private _user$: Observable<User>;
  private _user: User;

  private _schule$: Observable<Schule>;
  schule: Schule;

  private _content$: Observable<string>;
  content: string;

  private _settings$: Observable<GlobalSettings>;
  private _settings: GlobalSettings;

  private _hateoasPayload$: Observable<HateoasPayload>;
  private _hateoasPayload;

  //  private _progress$: Observable<Progress>;

  private _advTextGeladen: boolean;

  submitDisabled: boolean;
  cancelRequested: boolean;
  showDownloadLink: boolean;
  processing: boolean;


  constructor(private fb: FormBuilder
    , private _advService: AdvService
    , private _hateoasPayloadStore: HateoasPayloadStore
    , private _katalogService: KatalogService
    , private _globalSettingsStore: GlobalSettingsStore
    , private _messageService: MessageService
    //    , private _progressStore: ProgressStore
    , private _schuleStore: SchuleStore
    , private _staticContentService: StaticContentService
    , private _staticHtmlStore: StaticHtmlStore
    , private _userStore: UserStore
    , private _sanitizer: DomSanitizer
    , private _router: Router
    , @Inject(DOCUMENT) private document: Document
    , private _logger: LogService) {

    this._subscription = new Subscription();
    this._settings$ = this._globalSettingsStore.stateSubject.asObservable();
    this._user$ = this._userStore.stateSubject.asObservable();
    this._content$ = this._staticHtmlStore.stateSubject.asObservable();
    this._schule$ = this._schuleStore.stateSubject.asObservable();
    this._hateoasPayload$ = this._hateoasPayloadStore.stateSubject.asObservable();
    //    this._progress$ = this._progressStore.stateSubject.asObservable();

    this.advForm = fb.group({
      'schulname': ['', [Validators.required, Validators.maxLength(100)]],
      'laendercode': ['', [Validators.required, Validators.maxLength(2)]],
      'plz': ['', [Validators.required, Validators.maxLength(10)]],
      'ort': ['', [Validators.required, Validators.maxLength(100)]],
      'strasse': ['', [Validators.required, Validators.maxLength(200)]],
      'hausnummer': ['', [Validators.required, Validators.maxLength(10)]]
    });

    this.schulname = this.advForm.controls['schulname'];
    this.laendercode = this.advForm.controls['laendercode'];
    this.plz = this.advForm.controls['plz'];
    this.ort = this.advForm.controls['ort'];
    this.strasse = this.advForm.controls['strasse'];
    this.hausnummer = this.advForm.controls['hausnummer'];
  }

  ngOnInit() {

    this.submitDisabled = false;
    this.content = '<p>Dieser Text ist nicht vom Server</p>';
    this._advTextGeladen = false;
    this.showDownloadLink = false;

    const settingsSubscription = this._settings$.subscribe((theSettings: GlobalSettings) => {
      this._settings = theSettings;
      if (!this._advTextGeladen && this._settings.staticHtmlInfos) {
        this._loadAdvText();
      }
    });

    const hateoasSubscripton = this._hateoasPayload$.subscribe((payload: HateoasPayload) => {
      if (payload && payload.kontext === HATEOAS_KONTEXT_ADV && payload.id) {
        this.showDownloadLink = true;
      } else {
        this.showDownloadLink = false;
      }
    });

    const schuleSubscription = this._schule$.subscribe((s: Schule) => {
      this.schule = s;
      this._initForm();
    });

    const staticContentSubscription = this._content$.subscribe((c: string) => {
      this.content = c;
    });

    const userSubscription = this._user$.subscribe((u: User) => {
      if (u) {
        this._user = new User(u);

        if (this._user.basisdaten.rolle === 'MKV_LEHRER') {
          if (this._user.erweiterteKontodaten.advVereinbarungVorhanden) {
            this.content = '<p>Ihre Schule hat mit mir bereits eine Vereinbarung zur Auftragsverarbeitung abgeschlossen</p>';
            this.showDownloadLink = true;
          } else {
            this.showDownloadLink = false;
            if (!this.schule) {
              this._katalogService.loadSchule(this._user.erweiterteKontodaten.schule.kuerzel);
            }
            if (!this._advTextGeladen && this._settings.staticHtmlInfos) {
              this._loadAdvText();
            } else {
              this._staticContentService.loadStaticHtmlFileList();
            }
          }
        }
      }
    });



    this._subscription.add(settingsSubscription);
    this._subscription.add(schuleSubscription);
    this._subscription.add(userSubscription);
    this._subscription.add(staticContentSubscription);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._messageService.clearMessage();
  }

  getSchule(): Schule {
    if (this.schule) {
      return this.schule;
    }
    return this._user.erweiterteKontodaten.schule;
  }

  private _loadAdvText(): void {
    this._logger.debug(JSON.stringify(this._settings.staticHtmlInfos));
    const dieInfos = this._settings.staticHtmlInfos.filter((dateiInfo: DateiInfo) => dateiInfo.dateiname.startsWith('adv-vereinbarung'));
    if (dieInfos && dieInfos.length > 0) {
      const url = dieInfos[0].dateiUrl;
      this._logger.debug('request url ' + url);
      this._staticContentService.getStaticContent(url);
    }
    this._advTextGeladen = true;
  }



  onSubmit(value: any): void {
    if (this.cancelRequested) {
      return;
    }
    this.submitDisabled = true;
    const payload = value as AdvVereinbarungAntrag;

    const antrag = {
      'schulkuerzel': this.getSchule().kuerzel,
      'schulname': payload.schulname ? payload.schulname.trim() : '',
      'laendercode': payload.laendercode ? payload.laendercode.trim() : '',
      'plz': payload.plz ? payload.plz.trim() : '',
      'ort': payload.ort ? payload.ort.trim() : '',
      'strasse': payload.strasse ? payload.strasse.trim() : '',
      'hausnummer': payload.hausnummer ? payload.hausnummer.trim() : ''
    };

    payload.schulkuerzel = this.schule.kuerzel;
    this._advService.createAdvAntrag(this._user, antrag);
    this.document.body.scrollTop = 0;
  }

  cancel() {
    this.cancelRequested = true;
    const action = {
      type: CLEAR,
      payload: null
    } as HateoasMKVAction;
    this._hateoasPayloadStore.dispatch(action);
    this._messageService.clearMessage();
    this._router.navigate(['/dashboard']);
  }

  private _initForm() {

    // Für Bundesländer nur die beiden ersten Zeichen :)
    let lc = this.schule.lage.landkuerzel;
    if (lc.length > 2) {
      lc = lc.substr(0, 2);
    }

    const value = {
      'schulname': this.schule.name
      , 'laendercode': lc
      , 'plz': ''
      , 'ort': this.schule.lage.ort
      , 'strasse': ''
      , 'hausnummer': ''
    };
    this.advForm.setValue(value);
  }
}




