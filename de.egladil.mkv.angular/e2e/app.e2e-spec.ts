import { MkvangularPage } from './app.po';

describe('mkvangular App', () => {
  let page: MkvangularPage;

  beforeEach(() => {
    page = new MkvangularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('mkv works!');
  });
});
