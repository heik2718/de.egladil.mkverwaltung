import { browser, by, element } from 'protractor';

export class MkvangularPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mkv-root h1')).getText();
  }
}
