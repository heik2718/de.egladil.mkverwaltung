//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.importapp.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;

/**
 * UnbekanntZuordnungStrategieIT
 */
public class UnbekanntZuordnungStrategieIT extends AbstractGuiceIT {

	@Inject
	private ISchulteilnahmeDao schulteilnahmeDao;

	@Inject
	private ISchuleReadOnlyDao schuleReadOnlyDao;

	private UnbekanntZuordnungStrategie strategie;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		strategie = new UnbekanntZuordnungStrategie(schulteilnahmeDao, schuleReadOnlyDao);
	}

	@Test
	void getOrCreateTeilnahmeDEKeineTeilnahmeVorhanden() {
		// Arrange
		final String dateiname = "unbekannt.cvs";
		final String wettbewerbsjahr = "2016";

		// Act
		final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = strategie.getOrCreateTeilnahme(dateiname, wettbewerbsjahr);

		// Assert
		assertNotNull(teilnahmeIdentifierProvider);
		assertTrue(teilnahmeIdentifierProvider instanceof Schulteilnahme);
		final Schulteilnahme teilnahme = (Schulteilnahme) teilnahmeIdentifierProvider;
		assertNull(teilnahme.getId());
		assertEquals("N2MQLMGD", teilnahme.getKuerzel());
	}
}
