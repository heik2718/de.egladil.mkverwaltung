//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * NFRTransformStrategyTest
 */
public class NFRTransformStrategyTest {

	@Test
	void getNutzereingabeIstIdentitaet() {
		// Arrange
		final String eingabe = "asggsasag";

		// Act + Assert
		assertEquals(eingabe, new NFRTransformStrategy().getNutzereingabe(eingabe));
	}

	@Test
	void getWertungscodeEntferntSemikolons() {
		// Arrange
		final String eingabe = "asgg;sasag";
		final String expected = "asggsasag";

		// Act + Assert
		assertEquals(expected, new NFRTransformStrategy().getWertungscode(eingabe));
	}

}
