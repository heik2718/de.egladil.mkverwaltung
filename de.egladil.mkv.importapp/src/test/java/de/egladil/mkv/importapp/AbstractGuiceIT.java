//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp;

import org.junit.jupiter.api.BeforeEach;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.importapp.config.AppInjector;
import de.egladil.mkv.persistence.config.KontextReader;

/**
 * AbstractGuiceIT
 */
public abstract class AbstractGuiceIT {

	@BeforeEach
	public void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		final Injector injector = Guice.createInjector(new AppInjector(OsUtils.getDevConfigRoot()));
		injector.injectMembers(this);
	}
}
