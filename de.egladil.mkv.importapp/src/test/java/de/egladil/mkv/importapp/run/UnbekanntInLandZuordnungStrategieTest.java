//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;

/**
 * UnbekanntInLandZuordnungStrategieTest
 */
public class UnbekanntInLandZuordnungStrategieTest {

	private UnbekanntInLandZuordnungStrategie strategie;

	@BeforeEach
	void setUp() {
		strategie = new UnbekanntInLandZuordnungStrategie(Mockito.mock(ISchulteilnahmeDao.class),
			Mockito.mock(ISchuleReadOnlyDao.class));
	}

	@Test
	void getLandkuerzelSchweiz() {
		// Arrange
		final String dateiname = "ch_03.csv";
		final String expected = "CH";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelNrw() {
		// Arrange
		final String dateiname = "nrw_03.csv";
		final String expected = "de-NW";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelNW() {
		// Arrange
		final String dateiname = "nw_03.csv";
		final String expected = "de-NW";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelRlp() {
		// Arrange
		final String dateiname = "rlp_03.csv";
		final String expected = "de-RP";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelRp() {
		// Arrange
		final String dateiname = "rp_03.csv";
		final String expected = "de-RP";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelLsa() {
		// Arrange
		final String dateiname = "lsa_03.csv";
		final String expected = "de-ST";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelST() {
		// Arrange
		final String dateiname = "st_03.csv";
		final String expected = "de-ST";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getLandkuerzelEindeutig() {
		// Arrange
		final String dateiname = "ni_03.csv";
		final String expected = "de-NI";

		// Act
		final String actual = strategie.getLandkuerzel(dateiname);

		// Assert
		assertEquals(expected, actual);
	}
}
