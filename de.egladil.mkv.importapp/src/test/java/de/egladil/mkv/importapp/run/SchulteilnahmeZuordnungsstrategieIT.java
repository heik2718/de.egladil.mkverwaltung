//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.importapp.AbstractGuiceIT;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;

/**
 * SchulteilnahmeZuordnungsstrategieIT
 */
public class SchulteilnahmeZuordnungsstrategieIT extends AbstractGuiceIT {

	@Inject
	private ISchulteilnahmeDao schulteilnahmeDao;

	private SchulteilnahmeZuordnungsstrategie strategie;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		strategie = new SchulteilnahmeZuordnungsstrategie(schulteilnahmeDao);
	}

	@Test
	void getOrCreateTeilnahmeTeilnahmeVorhanden() {
		// Arrange
		final String dateiname = "9PDDS51Q_grundschule.cvs";
		final String wettbewerbsjahr = "2018";

		// Act
		final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = strategie.getOrCreateTeilnahme(dateiname, wettbewerbsjahr);

		// Assert
		assertNotNull(teilnahmeIdentifierProvider);
		assertTrue(teilnahmeIdentifierProvider instanceof Schulteilnahme);
		final Schulteilnahme teilnahme = (Schulteilnahme) teilnahmeIdentifierProvider;
		assertNotNull(teilnahme.getId());
	}

	@Test
	void getOrCreateTeilnahmeKeineTeilnahmeVorhanden() {
		// Arrange
		final String dateiname = "9PDDS51Q_grundschule.cvs";
		final String wettbewerbsjahr = "1970";

		// Act
		final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = strategie.getOrCreateTeilnahme(dateiname, wettbewerbsjahr);

		// Assert
		assertNotNull(teilnahmeIdentifierProvider);
		assertTrue(teilnahmeIdentifierProvider instanceof Schulteilnahme);
		final Schulteilnahme teilnahme = (Schulteilnahme) teilnahmeIdentifierProvider;
		assertNull(teilnahme.getId());
	}

}
