//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.google.inject.Inject;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.mkv.importapp.AbstractGuiceIT;
import de.egladil.mkv.importapp.config.ImportAppConfiguration;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;

/**
 * PrivatteilnahmeZuordnungsstrategieIT
 */
public class PrivatteilnahmeZuordnungsstrategieIT extends AbstractGuiceIT {

	@Inject
	private IPrivatkontoDao privatkontoDao;

	private PrivatteilnahmeZuordnungsstrategie strategie;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		final IEgladilConfiguration configuration = Mockito.mock(ImportAppConfiguration.class);
		Mockito.when(configuration.getProperty(ImportAppConfiguration.ImportAppConfigurationKey.uuidPrivatkonto.toString()))
			.thenReturn("bf01e395-7d12-4e53-8a7c-2d595ae7c263");

		strategie = new PrivatteilnahmeZuordnungsstrategie(configuration, privatkontoDao);
	}

	@Test
	void getOrCreateTeilnahmeTeilnahmeVorhanden() {
		// Arrange
		final String dateiname = "einzelteilnahmen.cvs";
		final String wettbewerbsjahr = "2018";

		// Act
		final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = strategie.getOrCreateTeilnahme(dateiname, wettbewerbsjahr);

		// Assert
		assertNotNull(teilnahmeIdentifierProvider);
		assertTrue(teilnahmeIdentifierProvider instanceof Privatteilnahme);
		final Privatteilnahme privatteilnahme = (Privatteilnahme) teilnahmeIdentifierProvider;
		assertNotNull(privatteilnahme.getId());
	}

	@Test
	void getOrCreateTeilnahmeKeineTeilnahmeVorhanden() {
		// Arrange
		final String dateiname = "einzelteilnahmen.cvs";
		final String wettbewerbsjahr = "1970";

		// Act
		final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = strategie.getOrCreateTeilnahme(dateiname, wettbewerbsjahr);

		// Assert
		assertNotNull(teilnahmeIdentifierProvider);
		assertTrue(teilnahmeIdentifierProvider instanceof Privatteilnahme);
		final Privatteilnahme privatteilnahme = (Privatteilnahme) teilnahmeIdentifierProvider;
		assertNull(privatteilnahme.getId());
	}
}
