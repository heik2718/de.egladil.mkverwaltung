//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * NumbersTransformStrategyTest
 */
public class NumbersTransformStrategyTest {

	@Test
	void getNutzereingabeKlappt() {
		// Arrange
		final String eingabe = "-0,75;3,00;3;0;0,00;-1;-1,00;4;4,00;-1,25;5;5,00";
		final String expected = "f;r;r;n;n;f;f;r;r;f;r;r";

		// Act + Assert
		assertEquals(expected, new NumbersTransformStrategy().getNutzereingabe(eingabe));
	}

	@Test
	void getWertungscodeKlappt() {
		// Arrange
		final String eingabe = "-0,75;3,00;3;0;0,00;-1;-1,00;4;4,00;-1,25;5;5,00";
		final String expected = "frrnnffrrfrr";

		// Act + Assert
		assertEquals(expected, new NumbersTransformStrategy().getWertungscode(eingabe));
	}

}
