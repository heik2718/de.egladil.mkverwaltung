//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.mkv.importapp.config.ImportAppConfiguration;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;

/**
 * SingleImportProcessorTest
 */
public class SingleImportProcessorTest {

	private SingleImportProcessor processor;

	private IEgladilConfiguration configuration;

	@BeforeEach
	void setUp() {
		configuration = new ImportAppConfiguration();
		configuration.init("/home/heike/git/konfigurationen/mkverwaltung/configAll");

		final IPrivatkontoDao privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		final IPrivatteilnahmeDao privatteilnahmeDao = Mockito.mock(IPrivatteilnahmeDao.class);
		final ISchulteilnahmeDao schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		final ISchuleReadOnlyDao schuleReadOnlyDao = Mockito.mock(ISchuleReadOnlyDao.class);
		final ILoesungszettelDao loesungszettelDao = Mockito.mock(ILoesungszettelDao.class);
		processor = new SingleImportProcessor(privatkontoDao, privatteilnahmeDao, schulteilnahmeDao, schuleReadOnlyDao,
			loesungszettelDao);
	}

	@Test
	void chooseStrategieKonkreteSchule() {
		// Arrange
		final String dateiname = "LP6EZKUU_punktverteilung_a_h_francke_schule_buchholz.csv";

		// Act
		final TeilnahmeZuordnungsstrategie strategie = processor.waehleStrategie(dateiname, configuration);

		// Assert
		assertTrue(strategie instanceof SchulteilnahmeZuordnungsstrategie);
	}

	@Test
	void chooseStrategiePrivat() {
		// Arrange
		final String dateiname = "einzelstarter.csv";

		// Act
		final TeilnahmeZuordnungsstrategie strategie = processor.waehleStrategie(dateiname, configuration);

		// Assert
		assertTrue(strategie instanceof PrivatteilnahmeZuordnungsstrategie);
	}

	@Test
	void chooseStrategieUnbekanntOhneUnterstrich() {
		// Arrange
		final String dateiname = "unbekannt.csv";

		// Act
		final TeilnahmeZuordnungsstrategie strategie = processor.waehleStrategie(dateiname, configuration);

		// Assert
		assertTrue(strategie instanceof UnbekanntZuordnungStrategie);
	}

	@Test
	void chooseStrategieUnbekanntMitUnterstrich() {
		// Arrange
		final String dateiname = "unbekannt_01.csv";

		// Act
		final TeilnahmeZuordnungsstrategie strategie = processor.waehleStrategie(dateiname, configuration);

		// Assert
		assertTrue(strategie instanceof UnbekanntZuordnungStrategie);
	}

	@Test
	void chooseStrategieUnbekanntInLand() {
		// Arrange
		final String dateiname = "nrw_08.csv";

		// Act
		final TeilnahmeZuordnungsstrategie strategie = processor.waehleStrategie(dateiname, configuration);

		// Assert
		assertTrue(strategie instanceof UnbekanntInLandZuordnungStrategie);
	}

	@Test
	void chooseStrategieKeinTreffer() {
		// Arrange
		final String dateiname = "bladiblubb.csv";

		// Act
		final TeilnahmeZuordnungsstrategie strategie = processor.waehleStrategie(dateiname, configuration);

		// Assert
		assertNull(strategie);
	}
}
