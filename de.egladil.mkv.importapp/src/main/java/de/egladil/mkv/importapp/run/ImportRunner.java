//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.file.VerzeichnisAuflistenCommand;

/**
 * ImportRunner
 */
@Singleton
public class ImportRunner {

	private static final Logger LOG = LoggerFactory.getLogger(ImportRunner.class);

	private String pathSourceDir;

	private final SingleImportProcessor importProcessor;

	/**
	 * ImportRunner
	 */
	@Inject
	public ImportRunner(final SingleImportProcessor importProcessor) {
		this.importProcessor = importProcessor;
	}

	public final void setPathSourceDir(final String pathSourceDir) {
		this.pathSourceDir = pathSourceDir;
	}

	/**
	 * Importiert die Daten aus allen csv-Dateien im source-Verzeichnis zur gegebenen Klassenstufe und zum gegebenen
	 * Jahr.
	 *
	 * @param klassenstufe Klassenstufe.
	 * @param jahr String
	 */
	public void importAll(final Klassenstufe klassenstufe, final String jahr, final IEgladilConfiguration configuration) {
		final VerzeichnisAuflistenCommand verzeichnisAuflistenCmd = new VerzeichnisAuflistenCommand();
		final List<File> allFiles = verzeichnisAuflistenCmd.listFilesAndLogIOException(pathSourceDir);
		LOG.info("importieren Daten aus {} Datei(en) zu Klassenstufe {}, Wettbewerbsjahr {}", allFiles.size(), klassenstufe, jahr);

		for (final File file : allFiles) {
			importProcessor.importFile(file, klassenstufe, jahr, configuration);
		}
	}
}
