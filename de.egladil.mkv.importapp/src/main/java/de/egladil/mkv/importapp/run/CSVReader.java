//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;

import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * CSVReader
 */
public class CSVReader {


	/**
	 * Wandelt eine einzelne CSV-Datei in die ;-separierten Strings um.
	 *
	 * @param pFile File eine CSV-Datei.
	 * @return List die Zeilen.
	 */
	public List<String> readLines(final File pFile) throws MKVException {
		if (pFile == null) {
			throw new IllegalArgumentException("Der Parameter darf nicht null sein.");
		}
		if (!pFile.exists() || !pFile.canRead()) {
			final String msg = "Die datei [" + pFile.getAbsolutePath() + "] existiert nicht oder ist nicht lesbar.";
			throw new MKVException(msg);
		}
		FileReader fr = null;
		try {
			fr = new FileReader(pFile);
			return IOUtils.readLines(fr);
		} catch (final IOException e) {
			final String msg = "Fehler beim Lesen [file=" + pFile.getAbsolutePath() + "]: " + e.getMessage();
			throw new MKVException(msg, e);
		} finally {
			IOUtils.closeQuietly(fr);
		}
	}

}
