//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.util.List;

import com.google.common.base.Splitter;

import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;

/**
 * SchulteilnahmeZuordnungsstrategie
 */
public class SchulteilnahmeZuordnungsstrategie extends AbstractSchulteilnahmeZuordnungStrategie {

	private static final Splitter UNDERLINE_SPLITTER = Splitter.on('_').trimResults();

	/**
	 * SchulteilnahmeZuordnungsstrategie
	 */
	public SchulteilnahmeZuordnungsstrategie(final ISchulteilnahmeDao schulteilnahmeDao) {
		super(schulteilnahmeDao);
	}

	@Override
	public TeilnahmeIdentifierProvider getOrCreateTeilnahme(final String dateiname, final String wettbewerbsjahr) {

		final List<String> tokens = UNDERLINE_SPLITTER.splitToList(dateiname);
		final String schulkuerzel = tokens.get(0);

		return super.findOrCreateSchulteilnahme(schulkuerzel, wettbewerbsjahr);
	}
}
