//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Splitter;

import de.egladil.common.persistence.utils.PrettyStringUtils;

/**
 * NumbersTransformStrategy
 */
public class NumbersTransformStrategy implements TransformToWertungscodeStrategy {

	private static final Splitter SEMIKOLON_SPLITTER = Splitter.on(';');

	@Override
	public String getWertungscode(final String csvLine) {
		final String nutzereingabe = this.getNutzereingabe(csvLine);
		return StringUtils.remove(nutzereingabe, ";");
	}

	@Override
	public String getNutzereingabe(final String csvLine) {
		final List<String> transformedValues = getTokens(csvLine);
		return PrettyStringUtils.collectionToString(transformedValues, ";");
	}

	private List<String> getTokens(final String csvLine) {
		final List<String> tokens = SEMIKOLON_SPLITTER.splitToList(csvLine);
		final List<String> transformedValues = new ArrayList<>(tokens.size());
		for (final String token : tokens) {
			final String intToken = StringUtils.remove(token, ",");
			final Integer d = Integer.valueOf(intToken);
			if (d < 0) {
				transformedValues.add("f");
			}
			if (d == 0) {
				transformedValues.add("n");
			}
			if (d > 0) {
				transformedValues.add("r");
			}
		}
		return transformedValues;
	}
}
