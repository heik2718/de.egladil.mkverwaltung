//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.util.Optional;

import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * UnbekanntZuordnungStrategie ordnet einen Lösungszettel der Schule Unbekannt im Ort Unbekannt Land de zu.
 */
public class UnbekanntZuordnungStrategie extends AbstractSchulteilnahmeZuordnungStrategie {

	private static final String LANDKUERZEL = "de";

	private final ISchuleReadOnlyDao schuleReadOnlyDao;

	/**
	 * UnbekanntZuordnungStrategie
	 */
	public UnbekanntZuordnungStrategie(final ISchulteilnahmeDao schulteilnahmeDao, final ISchuleReadOnlyDao schuleReadOnlyDao) {
		super(schulteilnahmeDao);
		this.schuleReadOnlyDao = schuleReadOnlyDao;
	}

	@Override
	public TeilnahmeIdentifierProvider getOrCreateTeilnahme(final String dateiname, final String wettbewerbsjahr) {

		final Optional<SchuleReadOnly> optSchule = schuleReadOnlyDao.findUnbekannteSchuleImLand(LANDKUERZEL);
		if (!optSchule.isPresent()) {
			throw new MKVException("Import von Datei " + dateiname + " nicht möglich: keine Schule Unbekannt vorhanden :/");
		}

		final SchuleReadOnly schuleReadOnly = optSchule.get();
		return super.findOrCreateSchulteilnahme(schuleReadOnly.getKuerzel(), wettbewerbsjahr);
	}
}
