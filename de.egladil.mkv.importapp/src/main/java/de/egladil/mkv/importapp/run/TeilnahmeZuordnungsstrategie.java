//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;

/**
 * TeilnahmeZuordnungsstrategie
 */
public interface TeilnahmeZuordnungsstrategie {

	/**
	 * Sucht oder erzeugt eine Schulteilnahme oder eine Privatteilnahme und gibt diese als TeilnahmeIdentifierProvider
	 * zurück. Falls neu, ist sie nicht persistiert.
	 *
	 * @param dateiname String Name der Datei mit dem Wertungscode
	 * @param wettbewerbsjahr String das Wettbewerbsjahr
	 * @return TeilnahmeIdentifier
	 */
	TeilnahmeIdentifierProvider getOrCreateTeilnahme(String dateiname, String wettbewerbsjahr);
}
