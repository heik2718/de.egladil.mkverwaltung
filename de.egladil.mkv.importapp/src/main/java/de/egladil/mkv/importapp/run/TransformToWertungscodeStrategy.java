//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

/**
 * TransformToWertungscodeStrategy
 */
public interface TransformToWertungscodeStrategy {

	/**
	 * Transformiert die Zeile in einen nfr-String ohne Semikolons.
	 *
	 * @param csvLine String die ;- Separierte Zeile aus einem CSC-File.
	 * @return String
	 */
	String getWertungscode(String csvLine);

	/**
	 * Gibt die Nutzereingabe zurück.
	 *
	 * @param csvLine String
	 * @return String
	 */
	String getNutzereingabe(String csvLine);

	/**
	 * Entscheidet anhand der gegebenen Zeile, ob es sich um n,f,r handelt oder um Doubles.
	 *
	 * @param firstLine
	 * @return TransformToWertungscodeStrategy
	 */
	static TransformToWertungscodeStrategy chooseStrategy(final String firstLine) {

		if (firstLine.contains("n") || firstLine.contains("f") || firstLine.contains("r")) {
			return new NFRTransformStrategy();
		}

		return new NumbersTransformStrategy();
	}
}
