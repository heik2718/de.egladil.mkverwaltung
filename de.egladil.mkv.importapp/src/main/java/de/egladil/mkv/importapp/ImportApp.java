//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.mkv.importapp.config.AppInjector;
import de.egladil.mkv.importapp.config.ImportAppConfiguration;
import de.egladil.mkv.importapp.run.ImportRunner;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;

/**
 * ImportApp importiert aus einem gegebenen Verzeichnis die Daten aus CSV-Dateien in die Datenbank.
 */
public class ImportApp {

	private static final Logger LOG = LoggerFactory.getLogger(ImportApp.class);

	@Parameter(names = { "-c", "--config" }, description = "path to config dir", required = true)
	private String pathConfigRoot;

	@Parameter(names = { "-s", "--source" }, description = "path to source dir", required = true)
	private String pathSourceDir;

	@Parameter(names = { "-k", "--klasse" }, description = "Klassentufe (EINS / ZWEI)", required = true)
	private String klassenstufeStr;

	@Parameter(names = { "-j", "--jahr" }, description = "Jahr", required = true)
	private String jahr;

	@Parameter(names = { "-h", "--help" }, help = true)
	private boolean help;

	private Klassenstufe klassenstufe;

	private ImportRunner importRunner;

	public static void main(final String[] args) {

		final ImportApp application = new ImportApp();

		try {
			new JCommander(application, args);

			if (application.help) {
				application.printUsage();
				System.exit(0);
			}
			LOG.info(application.toString());
			application.configure();
			application.start();
			LOG.info("fertisch :) ");
			System.exit(0);
		} catch (final ParameterException e) {
			LOG.error(e.getMessage());
			application.printUsage();
			System.exit(1);
		} catch (final Exception e) {
			LOG.error(e.getClass().getSimpleName() + ": " + e.getMessage(), e);
			System.exit(1);
		}

		System.out.println("Hello World!");
	}

	private void configure() {
		klassenstufe = Klassenstufe.valueOf(klassenstufeStr);
		final Injector injector = Guice.createInjector(new AppInjector(pathConfigRoot));
		importRunner = injector.getInstance(ImportRunner.class);
		importRunner.setPathSourceDir(pathSourceDir);
	}

	private void start() {
		final ImportAppConfiguration configuration = new ImportAppConfiguration();
		configuration.init(pathConfigRoot);
		importRunner.importAll(klassenstufe, jahr, configuration);
	}

	private void printUsage() {
		final StringBuffer sb = new StringBuffer();
		sb.append("Usage: <main class> [options]\n");
		sb.append("   Options:\n");
		sb.append("     * -c, --config\n");
		sb.append("          Pfad zum Konfigirationsverzeichnis\n");
		sb.append("     * -s, --source\n");
		sb.append("          Pfad zum Verzeichnis mit den Auswertungen\n");
		sb.append("     * -k, --klasse\n");
		sb.append("          Klassenstufe [EINS | ZWEI]\n");
		sb.append("     * -j, --jahr\n");
		sb.append("          Jahr der Auswertung\n");
		sb.append("       -h, --help\n");
		sb.append("          help = Optionen anzeigen\n");
		sb.append("\n");
		sb.append("Beispiele:\n");
		sb.append(
			"java -jar importApp.jar --config /home/heike/git/konfigurationen/mkverwaltung/configAll --source /media/veracrypt1/ag_arbeit/minikaenguru/_punktverteilungen/2010/csv --klasse ZWEI --jahr 2010\n");
		sb.append(
			"java -jar importApp.jar -c /home/heike/git/konfigurationen/mkverwaltung/configAll -s /media/veracrypt1/ag_arbeit/minikaenguru/_punktverteilungen/2010/csv -k ZWEI -j 2010\n");

		System.out.println(sb.toString());
	}
}
