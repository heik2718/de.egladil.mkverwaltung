//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.util.Optional;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.mkv.importapp.config.ImportAppConfiguration;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * PrivatteilnahmeZuordnungsstrategie
 */
public class PrivatteilnahmeZuordnungsstrategie implements TeilnahmeZuordnungsstrategie {

	private final IEgladilConfiguration configuration;

	private final IPrivatkontoDao privatkontoDao;

	/**
	 * PrivatteilnahmeZuordnungsstrategie
	 */
	public PrivatteilnahmeZuordnungsstrategie(final IEgladilConfiguration configuration, final IPrivatkontoDao privatkontoDao) {
		this.configuration = configuration;
		this.privatkontoDao = privatkontoDao;
	}

	@Override
	public TeilnahmeIdentifierProvider getOrCreateTeilnahme(final String dateiname, final String wettbewerbsjahr) {

		final String uuidPrivatkonto = configuration
			.getProperty(ImportAppConfiguration.ImportAppConfigurationKey.uuidPrivatkonto.toString());

		final Optional<Privatkonto> opt = privatkontoDao.findByUUID(uuidPrivatkonto);
		if (!opt.isPresent()) {
			throw new MKVException("Privatkonto mit [UUID=" + uuidPrivatkonto + "] nicht gefunden");
		}
		final Privatkonto privatkonto = opt.get();
		final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = privatkonto.getTeilnahmeZuJahr(wettbewerbsjahr);
		if (teilnahmeIdentifierProvider != null) {
			return teilnahmeIdentifierProvider;
		}

		final Privatteilnahme neueTeilnahme = new Privatteilnahme(wettbewerbsjahr, new KuerzelGenerator().generateDefaultKuerzel());
		privatkonto.addTeilnahme(neueTeilnahme);
		privatkontoDao.persist(privatkonto);
		return neueTeilnahme;
	}
}
