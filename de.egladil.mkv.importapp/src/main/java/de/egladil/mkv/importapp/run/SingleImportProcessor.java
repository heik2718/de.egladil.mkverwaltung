//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.mkv.auswertungen.VerarbeitetesWorkbook;
import de.egladil.mkv.auswertungen.eingaben.LoesungszettelRohdatenProvider;
import de.egladil.mkv.importapp.domain.ImportStatus;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * SingleImportProcessor
 */
@Singleton
public class SingleImportProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(SingleImportProcessor.class);

	private static final String DATEINAME_PRIVAT = "einzelstarter.csv";

	private static final String UNBEKANNT_MARKER = "unbekannt";

	private static final Splitter UNDERLINE_SPLITTER = Splitter.on("_");

	private final IPrivatkontoDao privatkontoDao;

	private final IPrivatteilnahmeDao privatteilnahmeDao;

	private final ISchulteilnahmeDao schulteilnahmeDao;

	private final ISchuleReadOnlyDao schuleReadOnlyDao;

	private final ILoesungszettelDao loesungszettelDao;

	/**
	 * SingleImportProcessor
	 */
	@Inject
	public SingleImportProcessor(final IPrivatkontoDao privatkontoDao, final IPrivatteilnahmeDao privatteilnahmeDao,
		final ISchulteilnahmeDao schulteilnahmeDao, final ISchuleReadOnlyDao schuleReadOnlyDao,
		final ILoesungszettelDao loesungszettelDao) {
		this.privatkontoDao = privatkontoDao;
		this.privatteilnahmeDao = privatteilnahmeDao;
		this.schulteilnahmeDao = schulteilnahmeDao;
		this.schuleReadOnlyDao = schuleReadOnlyDao;
		this.loesungszettelDao = loesungszettelDao;
	}

	/**
	 * Importiert die Daten der gegebenen Datei.
	 *
	 * @param file File die Datei mit den Daten
	 * @param jahr Sring das Jahr
	 * @param klassenstufe Klassenstufe.
	 * @param configuration IEgladilConfiguration
	 */
	@Transactional
	public void importFile(final File file, final Klassenstufe klassenstufe, final String jahr,
		final IEgladilConfiguration configuration) {

		final String dateiname = file.getName();

		final List<String> lines = new CSVReader().readLines(file);
		if (lines.size() > 0) {
			final TeilnahmeZuordnungsstrategie teilnahmeZuordnugsstrategie = waehleStrategie(dateiname, configuration);

			if (teilnahmeZuordnugsstrategie == null) {
				LOG.error("existiert keine Zuordnungsstrategie für {}", file.getAbsolutePath());
			} else {

				try {

					LOG.info("importieren Daten aus {}, teilnahmeZuordnugsstrategie = {}", dateiname,
						teilnahmeZuordnugsstrategie.getClass().getSimpleName());

					final TeilnahmeIdentifierProvider teilnahmeIdentifierProvider = teilnahmeZuordnugsstrategie
						.getOrCreateTeilnahme(dateiname, jahr);

					if (teilnahmeIdentifierProvider instanceof Privatteilnahme) {
						// Privatteilnahme privatteilnahme = (Privatteilnahme) teilnahmeIdentifierProvider;

					} else {
						Schulteilnahme schulteilnahme = (Schulteilnahme) teilnahmeIdentifierProvider;
						if (schulteilnahme.getId() == null) {
							schulteilnahme = schulteilnahmeDao.persist(schulteilnahme);
						}
					}

					final VerarbeitetesWorkbook workbook = createWorkbook(lines, klassenstufe);

					final List<Loesungszettel> alleLoesungszettel = new ArrayList<>();
					final TeilnahmeIdentifier tIdentifier = teilnahmeIdentifierProvider.provideTeilnahmeIdentifier();

					int nummer = loesungszettelDao.getMaxNummer(tIdentifier.getTeilnahmeart(), jahr, tIdentifier.getKuerzel(),
						klassenstufe);
					for (final LoesungszettelRohdaten rohdaten : workbook.getRohdaten()) {
						final Loesungszettel loesungszettel = new Loesungszettel.Builder(jahr, tIdentifier.getTeilnahmeart(),
							tIdentifier.getKuerzel(), ++nummer, rohdaten).withSprache(Sprache.de).build();
						alleLoesungszettel.add(loesungszettel);
					}
					loesungszettelDao.persist(alleLoesungszettel);

					moveFileQuietly(file, ImportStatus.FERTIG);

				} catch (final Exception e) {
					LOG.error("Fehler bei File " + file.getName() + ": " + e.getMessage(), e);
					moveFileQuietly(file, ImportStatus.FEHLER);
				}
			}
		}
	}

	private VerarbeitetesWorkbook createWorkbook(final List<String> lines, final Klassenstufe klassenstufe) {
		final VerarbeitetesWorkbook workbook = new VerarbeitetesWorkbook();
		workbook.setKlasse(klassenstufe);
		final TransformToWertungscodeStrategy transformStrategy = TransformToWertungscodeStrategy.chooseStrategy(lines.get(0));
		for (final String line : lines) {
			final String wertungscode = transformStrategy.getWertungscode(line);
			final String nutzereingabe = transformStrategy.getNutzereingabe(line);
			final LoesungszettelRohdatenProvider rohdatenProvider = new LoesungszettelRohdatenProvider(klassenstufe, wertungscode,
				nutzereingabe, false, Auswertungsquelle.UPLOAD);

			final LoesungszettelRohdaten rohdaten = rohdatenProvider.createLoesungszettelRohdaten();
			workbook.addEntry(rohdaten);
		}
		return workbook;
	}

	/**
	 * Wählt die passende TeilnahmeZuordnungsstrategie für den gegebenen Dateinamen aus.
	 *
	 * @param dateiname String
	 * @return Teilnahmezuordnungsstrategie oder null!!!
	 */
	TeilnahmeZuordnungsstrategie waehleStrategie(final String dateiname, final IEgladilConfiguration configuration) {

		if (dateiname == null) {
			throw new IllegalArgumentException("dateiname null");
		}

		if (DATEINAME_PRIVAT.equals(dateiname)) {
			return new PrivatteilnahmeZuordnungsstrategie(configuration, privatkontoDao);
		}
		if (dateiname.startsWith(UNBEKANNT_MARKER)) {
			return new UnbekanntZuordnungStrategie(schulteilnahmeDao, schuleReadOnlyDao);
		}
		final List<String> tokens = UNDERLINE_SPLITTER.splitToList(dateiname);
		if (tokens.size() > 1) {
			final String firstToken = tokens.get(0);
			if (firstToken.length() == 8) {
				return new SchulteilnahmeZuordnungsstrategie(schulteilnahmeDao);
			}
			return new UnbekanntInLandZuordnungStrategie(schulteilnahmeDao, schuleReadOnlyDao);
		}

		return null;
	}

	private void moveFileQuietly(final File file, final ImportStatus importStatus) {
		final String absPath = file.getAbsolutePath();
		final Path source = Paths.get(absPath);
		try {
			Files.move(source, source.resolveSibling(absPath + importStatus.getFileExtension()));
		} catch (final IOException e) {
			LOG.warn("File konnte nicht umbenannt werden: {} - {}", e.getClass().getSimpleName(), e.getMessage());
		}
	}
}
