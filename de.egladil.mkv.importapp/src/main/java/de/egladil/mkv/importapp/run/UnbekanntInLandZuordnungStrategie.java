//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Splitter;

import de.egladil.mkv.persistence.dao.ISchuleReadOnlyDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * UnbekanntInLandZuordnungStrategie ordnet einen Lösungszettel der Schule Unbekannt im Ort Unbekannt eines konkreten
 * (Bundes-)Landes zu.
 */
public class UnbekanntInLandZuordnungStrategie extends AbstractSchulteilnahmeZuordnungStrategie {

	private final Splitter SPLITTER = Splitter.on('_').trimResults();

	private static final String KUERZEL_SCHWEIZ = "CH";

	private static final List<String> SYNONYMS_NW = Arrays.asList(new String[] { "NW", "NRW" });

	private static final List<String> SYNONYMS_RP = Arrays.asList(new String[] { "RP", "RLP" });

	private static final List<String> SYNONYMS_ST = Arrays.asList(new String[] { "ST", "LSA" });

	private final ISchuleReadOnlyDao schuleReadOnlyDao;

	/**
	 * UnbekanntInLandZuordnungStrategie
	 */
	public UnbekanntInLandZuordnungStrategie(final ISchulteilnahmeDao schulteilnahmeDao,
		final ISchuleReadOnlyDao schuleReadOnlyDao) {
		super(schulteilnahmeDao);
		this.schuleReadOnlyDao = schuleReadOnlyDao;
	}

	@Override
	public TeilnahmeIdentifierProvider getOrCreateTeilnahme(final String dateiname, final String wettbewerbsjahr) {

		final String landkuerzel = this.getLandkuerzel(dateiname);

		final Optional<SchuleReadOnly> optSchule = schuleReadOnlyDao.findUnbekannteSchuleImLand(landkuerzel);
		if (!optSchule.isPresent()) {
			throw new MKVException("Import von Datei " + dateiname + " nicht möglich: keine Schule Unbekannt vorhanden :/");
		}

		final SchuleReadOnly schuleReadOnly = optSchule.get();
		return super.findOrCreateSchulteilnahme(schuleReadOnly.getKuerzel(), wettbewerbsjahr);
	}

	String getLandkuerzel(final String dateiname) {

		final String kuerzel = SPLITTER.splitToList(dateiname).get(0).toUpperCase();
		if (KUERZEL_SCHWEIZ.equals(kuerzel)) {
			return KUERZEL_SCHWEIZ;
		}
		if (SYNONYMS_NW.contains(kuerzel)) {
			return "de-NW";
		}
		if (SYNONYMS_RP.contains(kuerzel)) {
			return "de-RP";
		}
		if (SYNONYMS_ST.contains(kuerzel)) {
			return "de-ST";
		}
		return "de-" + kuerzel;
	}
}
