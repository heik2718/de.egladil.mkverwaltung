//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import java.util.List;
import java.util.Optional;

import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;

/**
 * AbstractSchulteilnahmeZuordnungStrategie
 */
public abstract class AbstractSchulteilnahmeZuordnungStrategie implements TeilnahmeZuordnungsstrategie {

	private final ISchulteilnahmeDao schulteilnahmeDao;

	/**
	 * AbstractSchulteilnahmeZuordnungStrategie
	 */
	public AbstractSchulteilnahmeZuordnungStrategie(final ISchulteilnahmeDao schulteilnahmeDao) {
		this.schulteilnahmeDao = schulteilnahmeDao;
	}

	protected Schulteilnahme findOrCreateSchulteilnahme(final String schulkuerzel, final String jahr) {
		final List<Schulteilnahme> teilnahmen = schulteilnahmeDao.findAllBySchulkuerzel(schulkuerzel);

		final Optional<Schulteilnahme> optTeilnahme = teilnahmen.stream().filter(s -> jahr.equals(s.getJahr())).findFirst();

		if (optTeilnahme.isPresent()) {
			return optTeilnahme.get();
		}

		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setJahr(jahr);
		schulteilnahme.setKuerzel(schulkuerzel);

		return schulteilnahme;
	}
}
