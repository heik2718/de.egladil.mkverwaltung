//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.mkv.persistence.config.MKVPersistenceModule;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;
import de.egladil.mkv.persistence.service.impl.TeilnehmerFacadeImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerServiceImpl;

/**
 * AppInjector
 */
public class AppInjector extends AbstractModule {

	private final String pathConfigRoot;

	/**
	 * AppInjector
	 */
	public AppInjector(final String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	@Override
	protected void configure() {
		loadProperties(binder());

		install(new MKVPersistenceModule(this.pathConfigRoot));

		bind(IEgladilConfiguration.class).to(ImportAppConfiguration.class);
		bind(TeilnehmerFacade.class).to(TeilnehmerFacadeImpl.class);
		bind(TeilnehmerService.class).to(TeilnehmerServiceImpl.class);
		bind(IProtokollService.class).to(ProtokollService.class);
	}

	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", this.pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
