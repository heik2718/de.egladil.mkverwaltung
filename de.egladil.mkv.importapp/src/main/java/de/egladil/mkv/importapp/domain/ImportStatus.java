//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.domain;

/**
 * ImportStatus
 */
public enum ImportStatus {

	FERTIG(".done"),
	FEHLER(".error");

	private final String fileExtension;

	/**
	 * Erzeugt eine Instanz von ImportStatus
	 */
	private ImportStatus(final String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFileExtension() {
		return fileExtension;
	}
}
