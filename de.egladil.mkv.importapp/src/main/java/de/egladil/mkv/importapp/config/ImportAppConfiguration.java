//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.config;

import de.egladil.common.config.AbstractEgladilConfiguration;

/**
 * ImportAppConfiguration
 */
public class ImportAppConfiguration extends AbstractEgladilConfiguration {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	public enum ImportAppConfigurationKey {
		uuidPrivatkonto
	}

	@Override
	protected String getConfigFileName() {
		return "importApp.properties";
	}
}
