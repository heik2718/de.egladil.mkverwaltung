//=====================================================
// Projekt: de.egladil.mkv.importapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.importapp.run;

import org.apache.commons.lang3.StringUtils;

/**
 * NFRTransformStrategy
 */
public class NFRTransformStrategy implements TransformToWertungscodeStrategy {

	@Override
	public String getWertungscode(final String csvLine) {
		return StringUtils.remove(csvLine, ';');
	}

	@Override
	public String getNutzereingabe(final String csvLine) {
		return csvLine;
	}
}
