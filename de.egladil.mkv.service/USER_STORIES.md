# User Stories für die Minikänguru-Verwaltung

## 1. Erfolgreiche Neuregistrierung mit Anmeldung einer Schule beim Minikänguru-Wettbewerb 2016

###Situation:
Es gibt noch kein Benutzerkonto für die Anwendung "Minikänguruverwaltung" mit der Mailadresse hannelore.walter@egladil.de.
Eine vorhandene Schule ist ausgewählt.

### Aktion:
Eine Person mit Vorname Hannelore, Nachname Walter, Mailadresse hannelore.walter@egladil.de registriert ihre Teilnahme an einer vorhandenen ausgewählten Schule am Minikänguru-Wettbewerb 2016. Alle Eingabedaten sind valide.

### Ergebnis:
- Es gibt ein Benutzerkonto für die Anwendung "Minikänguruverwaltung" mit der Mailadresse hannelore.walter@provider.com.
- Das Benutzerkonto ist noch nicht aktiviert.
- Es gibt einen CONFIRM_CODE zu diesem Benutzerkonto.
- Es gibt eine Schulteilnahme 2016 mit der zuvor ausgewählten Schule.
- Es gibt einen Eintrag in schulteilnahmen_lehrer mit der teilnahme-ID und der lehrerkonto-ID
- Eine Mail mit dem Link zum Aktivieren der Registrierung wurde an die Mailadresse von Hannelore Walter versendet.

## 2. Erfolgreiche Anmeldung eines Lehrers mit Benutzerkonto beim Minikänguru-Wettbewerb 2017

###Situation:
Es gibt ein Benutzerkonto mit UUID 653e0415-5e32-4a91-a330-3045bb2c7bfb für die Anwendung "Minikänguruverwaltung" mit der Mailadresse hannelore.walter@egladil.de. Hannelore Walter hat 1 Schulzuordnung. Es gibt noch keine Anmeldung für 2017 zu diesem Benutzerkonto. Hannelore Walter hat sich erfolgreich authentisiert.


### Aktion:
Hannelore Walter meldet sich über eine URL für den Wettbewerb 2017 an.

### Ergebnis:
Es gibt eine neue Schulteilnahme mit Jahr 2017 und einen Eintrag in schulteilnahmen_lehrer mit der teilnahme-ID und der lehrerkonto-ID

## 3. Erfolgreiche Neuregistrierung mit Anmeldung einer Privatperson Minikänguru-Wettbewerb 2016

###Situation:
Es gibt noch kein Benutzerkonto für die Anwendung "Minikänguruverwaltung" mit der Mailadresse ingo.inkognito@egladil.de.


### Aktion:
Eine Person mit Vorname Ingo, Nachname Inkognito, Mailadresse ingo.inkognito@egladil.de registriert sich für den Mnikänguru-Wettbewerb 2016. Alle Eingabedaten sind valide.

### Ergebnis:
- Es gibt ein Benutzerkonto für die Anwendung "Minikänguruverwaltung" mit der Mailadresse ingo.inkognito@egladil.de und der Rolle MKV_PRIVAT.
- Das Benutzerkonto ist noch nicht aktiviert.
- Es gibt einen CONFIRM_CODE zu diesem Benutzerkonto.
- Es gibt ein Privatkonto, das mit dem neuen Benutzerkonto verknüpft ist: Vorname Ingo, Nachname Inkognito
- Es gibt eine Privatteilnahme mit Jahr 2016, die mit dem neuen Privatkontakt verknüpft ist.
- Eine Mail mit dem Link zum Aktivieren der Registrierung wurde an die Mailadresse von Ingo Inkognito versendet.

## 4. Erfolgreiche Anmeldung einer Privatperson beim Minikänguru-Wettbewerb 2017

###Situation:
Es gibt ein Benutzerkonto mit UUID 4c6d4c18-c752-4da4-8c1e-ff38babbffc7 für die Anwendung "Minikänguruverwaltung" mit der Mailadresse ingo@egladil.de. Es gibt noch keine Anmeldung für 2017 zu diesem Benutzerkonto. Ingo Inkognito hat sich erfolgreich authentisiert.


### Aktion:
Ingo Inkognito meldet sich über eine URL für den Wettbewerb 2017 an.

### Ergebnis:
- Es gibt eine neue Privatteilnahme mit Jahr 2017, zu Ingo Inkognitos Benutzerkonto (Privatkontakt).


## 5. Antrag auf Aufnahme einer Schule in den Katalog
Als Lehrer möchte ich die Schule Testschule in 53636 Teststadt, Bahnhofstraße 6 im Bundesland Testland in den Schulkatalog eintragen lassen, damit ich mich mit dieser Schule zum Minikänguru-Wettbewerb anmelden kann.

###Situation:
Es gibt ein Bundesland Testland. Es gibt die Schule Testschule in 53636 Teststadt, Bahnhofstraße 6 im Bundesland Testland nicht.


### Aktion:
Mit der Mailadresse blubb@egladil.de fülle ich das Formular zum Anlegen einer Schule mit Anschrift vollständig aus und klicke auf "absenden".

### Ergebnis:
Auf der Webseite erscheint die Meldung, dass eine Mail versendet wurde. Eine Mail wurde an blubb@egladil.de mit Blindkopie an minikaenguru@egladil.de mit dem Katalogantrag versendet.

## 6. Passwort vergessen aktivieren
Als Lehrer oder Privatperson möchte ich mich einloggen, habe aber mein Passwort vergessen. Ich weiß meinen Loginnamen. Ich möchte mir ein neues Passwort geben.

### Situation
Mir gehört das Lehrerkonto mit der Mailadresse hallo@blubb.de

### Aktion
Ich betätige den Link "Passwort vergessen" und werde auf ein Formular geleitet, in das ich meine Mailadresse eintragen muss. Ich trage meine Mailadresse korrekt ein und klicke auf "absenden".

### Ergebnis
Ich erhalte eine Meldung, dass eine Mail mit einem Link zum Ändern meines Passworts an meine Mailadresse gesendet wurde. In meinem Mailaccount ist die Mail angekommen. Sie enthält einen personalisierten Link zum Ändern des Passworts, der 30min gültig ist.

## 7. Link "Einmalpasswort ändern" aufrufen
Ich habe "Passwort vergessen akivieren" durchgeführt und den Link in der Mail aufgerufen. Ich kann mir ein neues Passwort geben.

### Situation
Als Lehrer habe ich eine Mail mit einem Einmalpasswort erhalten. Ich habe die Minikänguru-Anwendung aufgerufen.

### Aktion
Ich klicke auf den Button "Einmalpasswort ändern".

### Erfgebnis
Es wird eine Seite mit einem Formular zum Eingeben meiner Mailadresse, eines neuen Passworts und der nochmaligen Eingabe des neuen Passworts angezeigt.

## 8. Vergessenes Passwort neu vergeben
Ich habe das Formular "Einmalpasswort ändern" vollstänig und korrekt ausgefüllt und möchte, dass durch "absenden" mein neues Passwort aktiviert wird.

### Situation
Das Formular ist "Einmalpasswort ändern" ist vollstänig und korrekt ausgefüllt.

### Aktion
Ich betätige den Button "absenden"

### Ergebnis
Es wird eine Erfolgsmeldung angezeigt und ich kann mit einem Link zur Loginseite wechseln. Ich kann mich mit dem neuen Passwort anmelden.


