# Konfigurationseinstellungen der Anwendung Minikänguru-Verwaltung


## 1. Zeitleiste

### Anfang März (privat Anfang Mai) bis Ende Juli

__startseite__

 - login: erlaubt
 - einmalpasswort: erlaubt
 - Konto anlegen: erlaubt

__home__

 - Anmeldung: erlaubt
 - Download: erlaubt
 - Upload: erlaubt
 - Profil ändern: erlaubt
 - Konto löschen: erlaubt

Hierzu:

__mkvservice-deb.yml__

 - neuanmeldungFreigeschaltet: true
 - uploadDisabled: false
 - Markerdateien vorhanden

__env.js__

 - window.__env.neuanmeldungFreigeschaltet = true;
 - window.__env.uploadDisabled = false;



### Anfang August bis Ende September

__startseite__

 - login: erlaubt
 - einmalpasswort: erlaubt
 - Konto anlegen: nicht erlaubt

__home__

 - Anmeldung: nicht erlaubt
 - Download: nicht erlaubt
 - Upload: nicht erlaubt
 - Profil ändern: erlaubt
 - Konto löschen: erlaubt

Hierzu:

__mkvservice-deb.yml__

 - neuanmeldungFreigeschaltet: false
 - uploadDisabled: true
 - keine Markerdateien vorhanden

__env.js__

 - window.__env.neuanmeldungFreigeschaltet = false;
 - window.__env.uploadDisabled = true;

### Anfang Oktober bis Ende Februar

__startseite__

 - login: erlaubt
 - einmalpasswort: erlaubt
 - Konto anlegen: erlaubt

__home__

 - Anmeldung: erlaubt
 - Download: nicht erlaubt
 - Upload: nicht erlaubt
 - Profil ändern: erlaubt
 - Konto löschen: erlaubt

Hierzu:

__mkvservice-deb.yml__

 - neuanmeldungFreigeschaltet: true
 - uploadDisabled: true
 - keine Markerdateien vorhanden

__env.js__

 - window.__env.neuanmeldungFreigeschaltet = true;
 - window.__env.uploadDisabled = true;

## 2. Beenden eines Wettbewerbsjahrs

Wenn ein Wettbewerbsjahr beendet ist
 - können keine Dateien mehr hochgeladen werden
 - keine Anmeldung für das aktuelle Wettbewerbsjahr getätigt werden
 - kein Benutzerkonto angemeldet werden

Das wird in auf dem Client und dem Sever mittels folgender Parameter konfiguriert:

__mkvservice-deb.yml__

 - neuanmeldungFreigeschaltet: false
 - uploadDisabled: false

__env.js__

 - window.__env.neuanmeldungFreigeschaltet = false;
 - window.__env.uploadDisabled = false;

## 3. Starten eines neuen Wettbewerbsjahrs

Wenn ein Wettbewerbsjahr begonnen wird, können sich Benutzer anmelden. Sinnvollerweise schaltet man uploadDisabled aber noch false, bis die Unterlagen freigegeben wurden

__mkvservice-deb.yml__

 - neuanmeldungFreigeschaltet: true
 - uploadDisabled: false
 - aktuellesWettbewerbsjahr: xxxx

__env.js__

 - window.__env.neuanmeldungFreigeschaltet = true;
 - window.__env.uploadDisabled = false;
 - window.__env.wettbewerbsjahr = 'XXXX';

## 4. Unterlagen freigeben

Auf dem Server müssen die Markerdateien lehrer.txt bzw. privat.txt im Downloadverzeichnis liegen. Außerdem gibt es folgende Parameter für den Client und den Server: (sinnvollerweise wird dann auch der upload aktiviert):

__mkvservice-deb.yml__

 - neuanmeldungFreigeschaltet: true
 - uploadDisabled: true

__env.js__

 - window.__env.neuanmeldungFreigeschaltet = true;
 - window.__env.uploadDisabled = true;


