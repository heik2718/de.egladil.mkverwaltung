//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.mail.SendFailedException;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.payload.EmailPayload;
import de.egladil.bv.aas.payload.TempPwdAendernPayload;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.config.OsUtils;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.benutzer.PasswortDelegate;
import de.egladil.mkv.service.mail.MailDelegate;

/**
 * TempPasswordResourceTest
 */
public class TempPasswordResourceTest {

	private static final String BENUTZER_UUID = "de565e-7362-77";

	private static final String DUMMYPASSWORT = "dstwi8gd";

	private TempPasswordResource resource;

	private IEgladilConfiguration configuration;

	private BenutzerkontoDelegate benutzerkontoDelegate;

	private PasswortDelegate passwortDelegate;

	private MailDelegate mailDelegate;

	private IProtokollService protokollService;

	private IAuthenticationService authenticationService;

	@BeforeEach
	public void setUp() {
		configuration = Mockito.mock(IEgladilConfiguration.class);
		benutzerkontoDelegate = Mockito.mock(BenutzerkontoDelegate.class);
		authenticationService = Mockito.mock(IAuthenticationService.class);
		passwortDelegate = new PasswortDelegate(authenticationService, benutzerkontoDelegate, protokollService);

		mailDelegate = Mockito.mock(MailDelegate.class);
		protokollService = Mockito.mock(IProtokollService.class);

		KontextReader.destroy();
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());

		Mockito.when(configuration.getProperty(MKVConstants.CONFIG_KEY_RESET_PASSWORD_DUMMYCODE)).thenReturn(DUMMYPASSWORT);
		resource = new TempPasswordResource(configuration, benutzerkontoDelegate, passwortDelegate, mailDelegate, protokollService);
	}

	@Test
	public void orderResetPassword_returns_400_wenn_payload_null() {

		// Act
		final Response response = resource.orderResetPassword(null);

		// Assert
		assertEquals(400, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_902_wenn_email_not_valid() {
		// Arrange
		final EmailPayload payload = new EmailPayload();
		payload.setEmail("huhu");

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(902, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_403_wenn_kleber_not_blank() {
		// Arrange
		final EmailPayload payload = new EmailPayload();
		payload.setEmail("huhu@provider.com");
		payload.setKleber(" g ");

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(403, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Diese Anfrage ist nicht erlaubt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_201_wenn_UnknownAccountException() {
		// Arrange
		final String email = "huhu@provider.com";
		final EmailPayload payload = new EmailPayload();
		payload.setEmail(email);
		Mockito.when(benutzerkontoDelegate.generateResetPasswordCode(email, 60))
			.thenThrow(new UnknownAccountException("unbekannte Mailadresse"));

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(201, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Eine Mail mit einem Einmalpasswort wurde an Ihre Email-Adresse versendet. Das Einmalpasswort ist 60 Minuten gültig. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_401_wenn_DisabledAccountException() {
		// Arrange
		final String email = "huhu@provider.com";
		final EmailPayload payload = new EmailPayload();
		payload.setEmail(email);
		Mockito.when(benutzerkontoDelegate.generateResetPasswordCode(email, 60))
			.thenThrow(new DisabledAccountException("unbekannte Mailadresse"));

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Sie haben Ihr Benutzerkonto noch nicht aktiviert oder es wurde deaktiviert.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_201_wenn_InvalidMailAddressException_aber_mail_versendet() {
		// Arrange
		final String email = "huhu@provider.com";
		final EmailPayload payload = new EmailPayload();
		payload.setEmail(email);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(email);
		benutzerkonto.setLoginName(email);
		benutzerkonto.setUuid(BENUTZER_UUID);

		aktivierungsdaten.setBenutzerkonto(benutzerkonto);

		Mockito.when(benutzerkontoDelegate.generateResetPasswordCode(email, 60)).thenReturn(aktivierungsdaten);

		Mockito.when(mailDelegate.resetPasswordMailVersenden(aktivierungsdaten))
			.thenThrow(new InvalidMailAddressException("unbekannte Mailadresse", new SendFailedException()));

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(201, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Eine Mail mit einem Einmalpasswort wurde an Ihre Email-Adresse versendet. Das Einmalpasswort ist 60 Minuten gültig. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_400_wenn_InvalidMailAddressException_mail_nicht_versendet() {
		// Arrange
		final String email = "huhu@provider.com";
		final EmailPayload payload = new EmailPayload();
		payload.setEmail(email);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(email);
		benutzerkonto.setLoginName(email);
		benutzerkonto.setUuid(BENUTZER_UUID);

		aktivierungsdaten.setBenutzerkonto(benutzerkonto);

		Mockito.when(benutzerkontoDelegate.generateResetPasswordCode(email, 60)).thenReturn(aktivierungsdaten);

		final InvalidMailAddressException invalidMailAddressException = Mockito.mock(InvalidMailAddressException.class);
		Mockito.when(invalidMailAddressException.getAllInvalidAdresses()).thenReturn(Arrays.asList(new String[] { email }));

		Mockito.when(mailDelegate.resetPasswordMailVersenden(aktivierungsdaten)).thenThrow(invalidMailAddressException);

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(400, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Mailadresse ist ungültig.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_500_wenn_EgladilMailException() {
		// Arrange
		final String email = "huhu@provider.com";
		final EmailPayload payload = new EmailPayload();
		payload.setEmail(email);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(email);
		benutzerkonto.setLoginName(email);
		benutzerkonto.setUuid(BENUTZER_UUID);

		aktivierungsdaten.setBenutzerkonto(benutzerkonto);

		Mockito.when(benutzerkontoDelegate.generateResetPasswordCode(email, 60)).thenReturn(aktivierungsdaten);

		Mockito.when(mailDelegate.resetPasswordMailVersenden(aktivierungsdaten))
			.thenThrow(new EgladilMailException("mailserver läuft nicht"));

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void orderResetPassword_returns_500_wenn_RuntimeException() {
		// Arrange
		final String email = "huhu@provider.com";
		final EmailPayload payload = new EmailPayload();
		payload.setEmail(email);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(email);
		benutzerkonto.setLoginName(email);
		benutzerkonto.setUuid(BENUTZER_UUID);

		aktivierungsdaten.setBenutzerkonto(benutzerkonto);

		Mockito.when(benutzerkontoDelegate.generateResetPasswordCode(email, 60)).thenReturn(aktivierungsdaten);

		Mockito.when(mailDelegate.resetPasswordMailVersenden(aktivierungsdaten))
			.thenThrow(new RuntimeException("irgendwas anderes"));

		// Act
		final Response response = resource.orderResetPassword(payload);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void passwortAendern_returns_902_wenn_payload_invalid() {
		// Arrange
		final TempPwdAendernPayload payload = new TempPwdAendernPayload();
		payload.setEmail("hallo");
		payload.setPasswort("start123");
		payload.setPasswortNeu("start456");
		payload.setPasswortNeuWdh("start456");

		// Act
		final Response response = resource.passwortAendern(payload);

		// Assert
		assertEquals(902, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void passwortAendern_returns_904_wenn_temppassword_expired() throws ParseException {
		// Arrange
		final TempPwdAendernPayload payload = new TempPwdAendernPayload();
		payload.setEmail("hallo@egladil.de");
		payload.setPasswort("zetezst7");
		payload.setPasswortNeu("start456");
		payload.setPasswortNeuWdh("start456");

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(payload.getEmail());
		benutzerkonto.setUuid(BENUTZER_UUID);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setConfirmationCode(payload.getPasswort());
		final Date datum = new SimpleDateFormat("dd.MM.yyyy").parse("01.10.2017");
		aktivierungsdaten.setExpirationTime(datum);
		benutzerkonto.addAktivierungsdaten(aktivierungsdaten);

		Mockito
			.when(benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
			.thenReturn(benutzerkonto);

		// Act
		final Response response = resource.passwortAendern(payload);

		// Assert
		assertEquals(904, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Das Einmalpasswort abgelaufen. Bitte fordern Sie ein neues an.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void passwortAendern_returns_wenn_EgladilAuthenticationException() throws ParseException {
		// Arrange
		final TempPwdAendernPayload payload = new TempPwdAendernPayload();
		payload.setEmail("hallo@egladil.de");
		payload.setPasswort("zetezst7");
		payload.setPasswortNeu("start456");
		payload.setPasswortNeuWdh("start456");

		Mockito
			.when(benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
			.thenThrow(new EgladilAuthenticationException("kennen wir nicht"));

		// Act
		final Response response = resource.passwortAendern(payload);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void passwortAendern_returns_wenn_DisabledAccountException() throws ParseException {
		// Arrange
		final TempPwdAendernPayload payload = new TempPwdAendernPayload();
		payload.setEmail("hallo@egladil.de");
		payload.setPasswort("zetezst7");
		payload.setPasswortNeu("start456");
		payload.setPasswortNeuWdh("start456");

		Mockito
			.when(benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
			.thenThrow(new DisabledAccountException("deaktiviert"));

		// Act
		final Response response = resource.passwortAendern(payload);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Sie haben Ihr Benutzerkonto noch nicht aktiviert oder es wurde deaktiviert.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void passwortAendern_returns_409_wenn_ConcurrentModification() throws ParseException {
		// Arrange
		final TempPwdAendernPayload payload = new TempPwdAendernPayload();
		payload.setEmail("hallo@egladil.de");
		payload.setPasswort("zetezst7");
		payload.setPasswortNeu("start456");
		payload.setPasswortNeuWdh("start456");

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setAnwendung(Anwendung.MKV);
		benutzerkonto.setEmail(payload.getEmail());
		benutzerkonto.setUuid(BENUTZER_UUID);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setConfirmationCode(payload.getPasswort());
		aktivierungsdaten.setExpirationTime(new Date());
		benutzerkonto.addAktivierungsdaten(aktivierungsdaten);

		Mockito
			.when(benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
			.thenReturn(benutzerkonto);

		Mockito.when(benutzerkontoDelegate.changePasswordAndSendMail(BENUTZER_UUID, payload.getPasswortNeu().toCharArray()))
			.thenThrow(new EgladilConcurrentModificationException("concurrent"));

		// Act
		final Response response = resource.passwortAendern(payload);

		// Assert
		assertEquals(409, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Ihr Benutzerprofil wurde in der Zwischenzeit geändert. Falls Sie dies selbst mit anderen Browser waren, loggen Sie sich hier bitte aus. Wenn nicht, senden Sie bitte eine Mail an minikaenguru@egladil.de.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void passwortAendern_returns_500_wenn_RuntimeException() throws ParseException {
		// Arrange
		final TempPwdAendernPayload payload = new TempPwdAendernPayload();
		payload.setEmail("hallo@egladil.de");
		payload.setPasswort("zetezst7");
		payload.setPasswortNeu("start456");
		payload.setPasswortNeuWdh("start456");

		Mockito
			.when(benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
			.thenThrow(new RuntimeException("deaktiviert"));

		// Act
		final Response response = resource.passwortAendern(payload);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
	}

}
