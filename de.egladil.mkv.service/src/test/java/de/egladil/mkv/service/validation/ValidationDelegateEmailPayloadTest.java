//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.bv.aas.payload.EmailPayload;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.service.TestUtils;

/**
 * ValidationDelegateEmailPayloadTest.
 */
public class ValidationDelegateEmailPayloadTest {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationDelegateEmailPayloadTest.class);

	private ValidationDelegate validationDelegate;

	private EmailPayload payload;

	private ObjectMapper mapper;

	@BeforeEach
	public void setUp() {
		validationDelegate = new ValidationDelegate();
		payload = TestUtils.validEmailPayload();
		mapper = new ObjectMapper();
	}

	@Test
	public void check_succeeds_when_valid() {
		// Act = Assert
		validationDelegate.check(payload, EmailPayload.class);
	}

	@Test
	public void check_throws_902_constraintViolation_when_payload_invalid_email() throws Exception {
		// Arrange
		payload.setEmail("luke");
		// Act + Assert
		try {
			validationDelegate.check(payload, EmailPayload.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

	@Test
	public void check_throws_forbidden_when_kleber_invalid() throws Exception {
		// Arrange
		payload.setKleber("h");

		// Act + Assert
		try {
			validationDelegate.check(payload, EmailPayload.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}

	@Test
	public void check_throws_forbidden_when_kleber_leerer_string_laenger_als_null() {
		// Arrange
		payload.setKleber(" ");

		// Act + Assert
		try {
			validationDelegate.check(payload, EmailPayload.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}
}
