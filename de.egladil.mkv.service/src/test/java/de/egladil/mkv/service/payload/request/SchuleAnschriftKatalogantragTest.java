//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.service.TestUtils;

/**
 * @author heikew
 *
 */
public class SchuleAnschriftKatalogantragTest {

	private static final Logger LOG = LoggerFactory.getLogger(SchuleAnschriftKatalogantragTest.class);

	private SchuleAnschriftKatalogantrag antrag;

	private Validator validator;

	@BeforeEach
	public void setUp() {
		antrag = TestUtils.validSchuleAnschriftKatalogantrag();
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_fails_when_email_null() {
		// Arrange

		antrag.setEmail(null);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_email_blank() {
		// Arrange

		antrag.setEmail(" ");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<SchuleAnschriftKatalogantrag>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	public void validate_fails_when_email_invalid() {
		// Arrange

		antrag.setEmail("hallo-hallo@hallo.@hallo");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_kleber_not_blank() {
		// Arrange

		antrag.setKleber(" ");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("kleber", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_name_null() {
		// Arrange

		antrag.setName(null);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("name", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_name_blank() {
		// Arrange

		antrag.setName(" ");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("name", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_name_zu_lang() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 101; i++) {
			sb.append("A");
		}
		final String name = sb.toString();
		assertEquals("Testsetting: brauchen 101 Zeichen", 101, name.length());
		antrag.setName(name);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("name", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_name_invalid() {
		// Arrange
		antrag.setName("прикладна́я");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("name", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_ort_null() {
		// Arrange

		antrag.setOrt(null);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("ort", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_ort_blank() {
		// Arrange

		antrag.setOrt(" ");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("ort", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_ort_zu_lang() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 101; i++) {
			sb.append("A");
		}
		final String name = sb.toString();
		assertEquals("Testsetting: brauchen 101 Zeichen", 101, name.length());
		antrag.setOrt(name);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("ort", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_ort_invalid() {
		// Arrange
		antrag.setOrt("Hoпр");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("ort", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_plz_null() {
		// Arrange

		antrag.setPlz(null);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("plz", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_plz_blank() {
		// Arrange

		antrag.setPlz(" ");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<SchuleAnschriftKatalogantrag>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("plz"));
	}

	@Test
	public void validate_fails_when_plz_zu_lang() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 11; i++) {
			sb.append("A");
		}
		final String name = sb.toString();
		assertEquals("Testsetting: brauchen 11 Zeichen", 11, name.length());
		antrag.setPlz(name);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("plz", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_plz_invalid() {
		// Arrange
		antrag.setPlz("Hoпр");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("plz", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_strasse_zu_lang() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 201; i++) {
			sb.append("A");
		}
		final String name = sb.toString();
		assertEquals("Testsetting: brauchen 201 Zeichen", 201, name.length());
		antrag.setStrasse(name);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("strasse", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_strasse_invalid() {
		// Arrange
		antrag.setStrasse("Hoпр");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("strasse", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_hausnummer_zu_lang() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 11; i++) {
			sb.append("A");
		}
		final String name = sb.toString();
		assertEquals("Testsetting: brauchen 11 Zeichen", 11, name.length());
		antrag.setHausnummer(name);

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("hausnummer", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_hausnummer_invalid() {
		// Arrange
		antrag.setHausnummer("Hoпр");

		// Act + Assert
		final Set<ConstraintViolation<SchuleAnschriftKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleAnschriftKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("hausnummer", cv.getPropertyPath().toString());
	}
}
