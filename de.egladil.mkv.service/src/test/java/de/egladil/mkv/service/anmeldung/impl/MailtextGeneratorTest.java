//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.mail.MailtextGenerator;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;

/**
 * MailtextGeneratorTest
 */
public class MailtextGeneratorTest {

	private static final Logger LOG = LoggerFactory.getLogger(MailtextGenerator.class);

	private MailtextGenerator generator;

	@BeforeEach
	public void setUp() {
		generator = new MailtextGenerator();
	}

	@Test
	public void getRegistrierungText_klappt() {
		// Arrange
		final String jahr = "2008";
		final String frist = "28.09.2016 08:32:55";
		final String link = "https://mathe-jung-alt.de/mkv/registrierungen/schulen/confirmation?t=7cb4b115-71dc-45b7-8b8e-4f79ca72a680";

		// Act
		final String mailtext = generator.getRegistrierungText(jahr, frist, link, "/mailtemplates/registrierung.txt");

		// Assert
		assertNotNull(mailtext);

		LOG.debug(mailtext);

	}

	@Test
	@DisplayName("exception when land null")
	public void getKatalogantragtextLandNull() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();

		// Act + Assert
		final Throwable ex = assertThrows(MKVException.class, () -> {
			generator.getSchuleKatalogantragText(payload);
		});
		assertEquals("Fehler im Ablauf: payload.land oder payload.nameLand ist nicht initialisiert!", ex.getMessage());
	}

	@Test
	public void getKatalogantragtext_mit_anschrift_klappt() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();
		final Land land = new Land();
		land.setName("Testland");
		payload.setLand(land);

		// Act
		final String mailtext = generator.getSchuleKatalogantragText(payload);

		// Assert
		assertNotNull(mailtext);

		LOG.debug(mailtext);

	}

	@Test
	public void getKatalogantragtext_mit_anschrift_ohne_optionale_felder_klappt() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();
		payload.setStrasse(null);
		payload.setHausnummer(null);
		final Land land = new Land();
		land.setName("Testland");
		payload.setLand(land);

		// Act
		final String mailtext = generator.getSchuleKatalogantragText(payload);

		// Assert
		assertNotNull(mailtext);

		LOG.debug(mailtext);
	}

	@Test
	public void getKatalogantragtextUrlLandNull() {
		// Arrange
		final SchuleUrlKatalogantrag payload = TestUtils.validSchuleUrlKatalogantrag();

		// Act + Assert
		final Throwable ex = assertThrows(MKVException.class, () -> {
			generator.getSchuleKatalogantragText(payload);
		});
		assertEquals("Fehler im Ablauf: payload.land ist nicht initialisiert!", ex.getMessage());

	}

	@Test
	public void getKatalogantragtext_mit_url_klappt() {
		// Arrange
		final SchuleUrlKatalogantrag payload = TestUtils.validSchuleUrlKatalogantrag();
		final Land land = new Land();
		land.setName("Testland");
		payload.setLand(land);

		// Act
		final String mailtext = generator.getSchuleKatalogantragText(payload);

		// Assert
		assertNotNull(mailtext);

		LOG.debug(mailtext);

	}

	@Test
	public void getTextResetPassword_klappt() {
		// Arrange
		final String link = "http://www.egladil.de";
		final String expires = "10.12.2016 07:15:21";
		final String pwd = "Qwertz27";

		// Act
		final String mailtext = generator.getTextResetPassword(link, expires, pwd);

		// Assert
		assertNotNull(mailtext);

		LOG.debug(mailtext);
	}

	@Test
	public void getTextWettbewerbsanmeldungMail_klappt() {
		// Act
		final String mailtext = generator.getTextWettbewerbsanmeldungMail("2017", "Anfang März");

		assertNotNull(mailtext);

		LOG.debug(mailtext);

	}
}
