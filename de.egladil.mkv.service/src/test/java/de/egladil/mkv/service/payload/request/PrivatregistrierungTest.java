//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.service.TestUtils;

/**
 * PrivatregistrierungTest
 */
public class PrivatregistrierungTest {

	private Validator validator;

	private Privatregistrierung anfrage;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		anfrage = TestUtils.validPrivatregistrierung();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act
		final Set<ConstraintViolation<Privatregistrierung>> errors = validator.validate(anfrage);

		// Assert
		assertTrue(errors.isEmpty());
	}
}
