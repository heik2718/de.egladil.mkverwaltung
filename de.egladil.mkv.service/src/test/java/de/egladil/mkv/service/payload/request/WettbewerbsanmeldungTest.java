//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * WettbewerbsanmeldungTest
 */
public class WettbewerbsanmeldungTest {

	private static final Logger LOG = LoggerFactory.getLogger(WettbewerbsanmeldungTest.class);

	private ValidationDelegate validationDelegate = new ValidationDelegate();

	@Test
	public void validate_passes() {
		// Arrange
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung("MKV_LEHRER", "2018", false);

		// Act + Assert
		validationDelegate.check(anmeldung, Wettbewerbsanmeldung.class);
	}

	@Test
	public void validate_throws_when_jahr_null() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung("MKV_PRIVAT", null, false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());

	}

	@Test
	public void validate_throws_when_jahr_blank() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung("MKV_PRIVAT", "", false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());

	}

	@Test
	public void validate_throws_when_jahr_nicht_numerisch() {
		try {
			validationDelegate.check(new Wettbewerbsanmeldung("MKV_PRIVAT", "Hallo", false), Wettbewerbsanmeldung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void validate_throws_when_jahr_zu_lang() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung("MKV_PRIVAT", "23456", false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());

	}

	@Test
	public void validate_throws_when_rolle_blank() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung(" ", "2017", false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());
	}

	@Test
	public void validate_throws_when_rolle_null() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung(null, "2017", false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());

	}

	@Test
	public void validate_throws_when_rolle_nicht_erlaubt() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung("BV_ADMIN", "2017", false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());

	}

	@Test
	public void validate_throws_when_rolle_invalid() {
		final Throwable ex = assertThrows(EgladilWebappException.class, () -> {
			validationDelegate.check(new Wettbewerbsanmeldung("ADMIN", "2017", false), Wettbewerbsanmeldung.class);
		});
		assertEquals("HTTP 902 ", ex.getMessage());

	}
}
