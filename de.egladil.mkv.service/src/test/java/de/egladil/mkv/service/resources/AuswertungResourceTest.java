//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.request.SchuleUrkundenauftrag;
import de.egladil.mkv.persistence.payload.request.TeilnehmerUrkundenauftrag;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmejahr;
import de.egladil.mkv.persistence.service.AuthorizationService;

/**
 * AuswertungResourceTest
 */
public class AuswertungResourceTest {

	private static final String BENUTZER_UUID = "gsagdig";

	private static final String TEILNEHMERKUERZEL = "JDJP9E2B20171217075222";

	public static final String JAHR = "2018";

	private static final String TEILNAHMEKUERZEL = "5GTFTGHH";

	private static final String TEILNAHMEART = "P";

	private UrkundenResource resource;

	private AuswertungService auswertungService;

	private AuthorizationService authorizationService;

	private SchulstatistikService schulstatistikService;

	private Principal principal;

	@BeforeEach
	void setUp() {
		auswertungService = Mockito.mock(AuswertungService.class);
		authorizationService = Mockito.mock(AuthorizationService.class);
		schulstatistikService = Mockito.mock(SchulstatistikService.class);
		resource = new UrkundenResource(auswertungService, authorizationService, schulstatistikService);
		principal = new EgladilPrincipal(BENUTZER_UUID);

		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.create(Teilnahmeart.P, TEILNAHMEKUERZEL, JAHR);

		Mockito.when(authorizationService.authorizeForTeilnahme(BENUTZER_UUID, teilnahmeIdentifier)).thenReturn("");
	}

	@Nested
	@DisplayName("Tests für einzelurkundenGenerieren")
	class EinzelurkundenTests {

		@Test
		@DisplayName("Principal null")
		void principalNull() {
			final Response response = resource.einzelurkundenGenerieren(null, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL,
				new TeilnehmerUrkundenauftrag());

			assertEquals(500, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals("Es ist ein interner Serverfehler aufgetreten.", payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("payload null")
		void payloadNull() {
			final Response response = resource.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, null);

			assertEquals(400, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("farbschemaname invalid")
		void unbekanntesFarbschema() {
			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setFarbschemaName("PINK");

			auftrag.setTeilnehmerKuerzel(new String[] { TEILNEHMERKUERZEL });

			final Response response = resource.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, auftrag);

			assertEquals(902, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals("Die Eingaben sind nicht korrekt.", payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("kein kuerzel")
		void teilnehmerUrkundenauftragInvalid1() {
			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setFarbschemaName("GREEN");

			final Response response = resource.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, auftrag);

			assertEquals(902, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals("Die Eingaben sind nicht korrekt.", payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("leeres kuerzel")
		void teilnehmerUrkundenauftragInvalid2() {
			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setFarbschemaName("GREEN");
			auftrag.setTeilnehmerKuerzel(new String[] { " " });

			final Response response = resource.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, auftrag);

			assertEquals(902, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals("Die Eingaben sind nicht korrekt.", payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("ungültiges kuerzel")
		void teilnehmerUrkundenauftragInvalid3() {
			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setFarbschemaName("GREEN");
			auftrag.setTeilnehmerKuerzel(new String[] { "RTFR%" });

			final Response response = resource.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, auftrag);

			assertEquals(902, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals("Die Eingaben sind nicht korrekt.", payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("kuerzel zu lang")
		void teilnehmerUrkundenauftragInvalid4() {
			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setFarbschemaName("GREEN");
			auftrag.setTeilnehmerKuerzel(new String[] { "AAAAAAAAAAAAAAAAAAAAAAA" });

			final Response response = resource.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, auftrag);

			assertEquals(902, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), payload.getApiMessage().getLevel());
			assertEquals("Die Eingaben sind nicht korrekt.", payload.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("keine Treffer bei Teilnehmer der Lösungszettel")
		void optionalNotPresent() {
			final TeilnehmerUrkundenauftrag auftrag = new TeilnehmerUrkundenauftrag();
			auftrag.setFarbschemaName(Farbschema.BLUE.toString());
			auftrag.setTeilnehmerKuerzel(new String[] { TEILNEHMERKUERZEL });

			final AuswertungService service = new AuswertungService() {

				@Override
				public List<GesamtpunktverteilungDaten> generiereGesamtpunktverteilung(final String jahr, final String kuerzelliste)
					throws ResourceNotFoundException {
					return null;
				}

				@Override
				public Optional<String> generiereAuswertungFuerEinzelteilnehmer(final String benutzerUuid,
					final TeilnehmerUrkundenauftrag auftrag, final TeilnahmeIdentifier teilnahmeIdentifier) {
					return Optional.empty();
				}

				@Override
				public Optional<AuswertungDownload> findAndIncrementAuswertungDownload(final String downloadcode) {
					return Optional.empty();
				}

				@Override
				public Optional<String> generiereSchulauswertung(final String benutzerUuid, final SchuleUrkundenauftrag auftrag,
					final TeilnahmeIdentifier teilnahmeIdentifier) {
					return Optional.empty();
				}

				@Override
				public List<GesamtpunktverteilungDaten> generiereGesamtpunktverteilung(final String jahr)
					throws ResourceNotFoundException {
					return null;
				}

				@Override
				public List<Teilnahmejahr> getAuswertbareJahre() {
					return null;
				}

				@Override
				public Optional<String> generiereKaengurusprungurkundenfuerEinzelteilnehmer(final String benutzerUuid,
					final TeilnehmerUrkundenauftrag auftrag, final TeilnahmeIdentifier teilnahmeIdentifier) {
					return Optional.empty();
				}

				@Override
				public Optional<String> generiereKaengurusprungurkundenfuerSchule(final String benutzerUuid,
					final SchuleUrkundenauftrag auftrag, final TeilnahmeIdentifier teilnahmeIdentifier) {
					return Optional.empty();
				}

				@Override
				public Map<Klassenstufe, String> berechneMediane(final String jahr) {
					final Map<Klassenstufe, String> result = new HashMap<>();
					result.put(Klassenstufe.EINS, "28,5");
					result.put(Klassenstufe.ZWEI, "26,75");
					return result;
				}

			};

			final Response response = new UrkundenResource(service, authorizationService, schulstatistikService)
				.einzelurkundenGenerieren(principal, JAHR, TEILNAHMEART, TEILNAHMEKUERZEL, auftrag);

			assertEquals(412, response.getStatus());
			final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), payload.getApiMessage().getLevel());
			assertEquals("Die Urkunden konnten nicht generiert werden, da es zu keinem Kind Antworten gibt.",
				payload.getApiMessage().getMessage());
		}
	}
}
