//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.mail.Address;
import javax.mail.SendFailedException;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.config.MKVTestWithMailexceptionModule;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.mail.MailserviceThrowingStub;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.registrierung.ILehrerRegistrierungService;
import de.egladil.mkv.service.registrierung.impl.LehrerRegistrierungServiceImpl;

/**
 * RegisterLehrerResourceWithMailExceptionIT
 */
public class LehrerregistrierungResourceWithMailExceptionIT {

	@Inject
	private LehrerregistrierungResource resource;

	@Inject
	private ILehrerRegistrierungService lehrerRegistrierungService;

	private Lehrerregistrierung anfrage;

	@Inject
	private IBenutzerDao benutzerDao;

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

	@Inject
	private ISchuleDao schuleDao;

	@Inject
	private IMailservice mailService;

	@Inject
	private MailDelegate mailDelegate;

	@BeforeEach
	public void setUp() {
		final Injector injector = Guice.createInjector(new MKVTestWithMailexceptionModule());
		injector.injectMembers(this);
		TestUtils.initMKVApiKontextReader("2014", true);
	}

	@Test
	public void registerLehrer_nicht_abgeschlossene_registrierung_wird_aufgeraeumt_EgladilMailException() {
		// Arrange
		anfrage = TestUtils.validRandomLehrerregistrierung();
		resource.setTest(true);
		final Optional<Schule> optSchule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			anfrage.getSchulkuerzel());
		assertTrue("Testsetting: Test kann nicht beginnen wegen fehlender Schule", optSchule.isPresent());

		// Act
		final Response response = resource.register(anfrage);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Ihr Benutzerkonto konnte wegen eines Serverfehlers nicht angelegt werden. Bitte senden Sie eine Mail an minikaenguru@egladil.de.",
			apiResponse.getApiMessage().getMessage());

		final String benutzerUuid = ((LehrerRegistrierungServiceImpl) lehrerRegistrierungService).getBenutzerUuidImTest();
		assertNotNull(benutzerUuid);

		final Benutzerkonto benutzer = benutzerDao.findBenutzerByUUID(benutzerUuid);
		assertNotNull(benutzer);
		assertTrue(benutzer.isAnonym());

		final Optional<Lehrerkonto> optKonto = lehrerkontoDao.findByUUID(benutzerUuid);
		assertTrue(optKonto.isPresent());
		final Lehrerkonto lehrerkonto = optKonto.get();
		assertTrue(lehrerkonto.getPerson().isAnonym());
	}

	@SuppressWarnings("deprecation")
	@Test
	public void registerLehrer_nicht_abgeschlossene_registrierung_wird_aufgeraeumt_InvalidMailException() {
		// Arrange

		anfrage = TestUtils.validRandomLehrerregistrierung();
		anfrage.setEmail(System.currentTimeMillis() + "142@bla.com");
		resource.setTest(true);
		final Optional<Schule> optSchule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			anfrage.getSchulkuerzel());
		assertTrue("Testsetting: Test kann nicht beginnen wegen fehlender Schule", optSchule.isPresent());

		final SendFailedException cause = Mockito.mock(SendFailedException.class);
		Mockito.when(cause.getInvalidAddresses()).thenReturn(new Address[] { TestUtils.getAddress(anfrage.getEmail()) });
		Mockito.when(cause.getValidSentAddresses()).thenReturn(null);
		Mockito.when(cause.getValidUnsentAddresses()).thenReturn(null);

		final InvalidMailAddressException exceptionToThrow = new InvalidMailAddressException(
			"Das ist eine Exception für einen Test", cause);

		((MailserviceThrowingStub) mailService).setExceptionToThrow(exceptionToThrow);
		mailDelegate.setMailService(mailService);
		((LehrerRegistrierungServiceImpl) lehrerRegistrierungService).setMailDelegate(mailDelegate);

		// Act
		final Response response = resource.register(anfrage);
		// Assert
		assertEquals(400, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Ihr Benutzerkonto konnte nicht angelegt werden. Die eingegeben Mailadresse ist ungültig.",
			apiResponse.getApiMessage().getMessage());

		final String benutzerUuid = ((LehrerRegistrierungServiceImpl) lehrerRegistrierungService).getBenutzerUuidImTest();
		assertNotNull(benutzerUuid);

		final Benutzerkonto benutzer = benutzerDao.findBenutzerByUUID(benutzerUuid);
		assertNotNull(benutzer);
		assertTrue(benutzer.isAnonym());

		final Optional<Lehrerkonto> optKonto = lehrerkontoDao.findByUUID(benutzerUuid);
		assertTrue(optKonto.isPresent());
		final Lehrerkonto lehrerkonto = optKonto.get();
		assertTrue(lehrerkonto.getPerson().isAnonym());
	}
}
