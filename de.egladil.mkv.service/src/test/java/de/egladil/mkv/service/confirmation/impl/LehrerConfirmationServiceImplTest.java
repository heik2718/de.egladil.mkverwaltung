//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.UniqueIdentifier;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.TestUtils;

/**
 * LehrerConfirmationServiceImplTest
 */
public class LehrerConfirmationServiceImplTest {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

	private IRegistrierungService registrierungService;

	private ILehrerkontoDao schulkontaktDao;

	private IBenutzerService benutzerService;

	private LehrerConfirmationServiceImpl confirmationService;

	@BeforeEach
	public void setUp() {
		registrierungService = Mockito.mock(IRegistrierungService.class);
		benutzerService = Mockito.mock(IBenutzerService.class);
		schulkontaktDao = Mockito.mock(ILehrerkontoDao.class);
		final ISchulteilnahmeDao schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		final IAnonymisierungsservice anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);

		confirmationService = new LehrerConfirmationServiceImpl(registrierungService, benutzerService, anonymisierungsservice,
			schulkontaktDao, schulteilnahmeDao);
	}

	@Test
	public void registrierungBestaetigen_throws_500_when_now_null() {
		try {
			confirmationService.registrierungBestaetigen(UUID.randomUUID().toString(), null, "2017");
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(500, e.getResponse().getStatus());
		}
	}

	@Test
	public void registrierungBestaetigen_throws_404_when_not_found() {
		try {
			confirmationService.jetztRegistrierungBestaetigen(UUID.randomUUID().toString(), "2017");
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(404, e.getResponse().getStatus());
		}
	}

	@Test
	public void registrierungBestaetigen_liest_aktivierungsdaten_nur_einmal() throws Exception {
		final Date now = SDF.parse("30.11.2016 09:16:43");
		final Date expiration = SDF.parse("30.11.2016 23:16:43");
		final String confirmationCode = UUID.randomUUID().toString();
		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setConfirmationCode(confirmationCode);
		regBest.setExpirationTime(expiration);

		final String benutzerUUID = UUID.randomUUID().toString();
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setId(13l);
		benutzerkonto.setUuid(benutzerUUID);
		benutzerkonto.addAktivierungsdaten(regBest);

		final Lehrerkonto schulkontakt = TestUtils.validActiveLehrerkonto(benutzerUUID, "2017");

		Mockito.when(registrierungService.findByConfirmationCode(new UniqueIdentifier(confirmationCode))).thenReturn(regBest);
		Mockito.when(schulkontaktDao.findByUUID(benutzerUUID)).thenReturn(Optional.of(schulkontakt));

		confirmationService.registrierungBestaetigen(confirmationCode, now, "2017");
		Mockito.verify(registrierungService, times(1)).findByConfirmationCode(new UniqueIdentifier(confirmationCode));
		Mockito.verify(schulkontaktDao, times(1)).findByUUID(benutzerUUID);
	}

	@Test
	public void getStrategy_returns_normalActivation() throws Exception {
		// Act
		final IConfirmationStrategy strategy = confirmationService.getConfirmationStrategy(false, false);

		// Assert
		assertTrue(strategy instanceof NormalActivationStrategy);
	}

	@Test
	public void getStrategy_returns_repeatedActivation_1() throws Exception {
		// Act
		final IConfirmationStrategy strategy = confirmationService.getConfirmationStrategy(false, true);

		// Assert
		assertTrue(strategy instanceof RepeatedActivationStrategy);
	}

	@Test
	public void getStrategy_returns_expiredActivation() throws Exception {
		// Act
		final IConfirmationStrategy strategy = confirmationService.getConfirmationStrategy(true, false);

		// Assert
		assertTrue(strategy instanceof ExpiredActivationStrategy);
	}

	@Test
	public void getStrategy_returns_repeatedActivation_2() throws Exception {
		// Act
		final IConfirmationStrategy strategy = confirmationService.getConfirmationStrategy(true, true);

		// Assert
		assertTrue(strategy instanceof RepeatedActivationStrategy);
	}
}
