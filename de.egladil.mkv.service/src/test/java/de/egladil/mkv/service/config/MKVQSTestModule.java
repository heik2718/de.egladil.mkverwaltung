//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenAuthenticator;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenDao;
import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.bv.aas.impl.AuthenticationServiceImpl;
import de.egladil.bv.aas.impl.BenutzerServiceImpl;
import de.egladil.bv.aas.impl.RegistrierungServiceImpl;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.config.OsUtils;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;
import de.egladil.email.service.IMailservice;
import de.egladil.mkv.persistence.config.MKVPersistenceModule;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.kataloge.impl.KatalogService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.persistence.service.impl.Anonymsierungsservice;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.anmeldung.impl.WettbewerbsanmeldungServiceImpl;
import de.egladil.mkv.service.confirmation.ILehrerConfirmationService;
import de.egladil.mkv.service.confirmation.IPrivatConfirmationService;
import de.egladil.mkv.service.confirmation.impl.LehrerConfirmationServiceImpl;
import de.egladil.mkv.service.confirmation.impl.PrivatConfirmationServiceImpl;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.mail.MailserviceStub;
import de.egladil.mkv.service.registrierung.ILehrerRegistrierungService;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;
import de.egladil.mkv.service.registrierung.impl.LehrerRegistrierungServiceImpl;
import de.egladil.mkv.service.registrierung.impl.PrivatRegistrierungServiceImpl;
import io.dropwizard.auth.Authenticator;

/**
 * MKVModule
 */
public class MKVQSTestModule extends MKVModule {

	private static final String CONFIG_ROOT = OsUtils.getQSConfigRoot();

	/**
	 * Erzeugt eine Instanz von AuswertungenTestModule
	 */
	public MKVQSTestModule() {
		super(CONFIG_ROOT);
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		super.loadProperties(binder());
		install(new BVPersistenceModule(CONFIG_ROOT));
		install(new MKVPersistenceModule(CONFIG_ROOT));

		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class);
		bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);
		bind(IMailservice.class).to(MailserviceStub.class);
		bind(MailDelegate.class);
		bind(ILehrerRegistrierungService.class).to(LehrerRegistrierungServiceImpl.class);
		bind(ILehrerConfirmationService.class).to(LehrerConfirmationServiceImpl.class);
		bind(IPrivatRegistrierungService.class).to(PrivatRegistrierungServiceImpl.class);
		bind(IPrivatConfirmationService.class).to(PrivatConfirmationServiceImpl.class);
		bind(IBenutzerService.class).to(BenutzerServiceImpl.class);
		bind(IEgladilConfiguration.class).to(EgladilMKVConfiguration.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);

		bind(IWettbewerbsanmeldungService.class).to(WettbewerbsanmeldungServiceImpl.class);

		bind(IRegistrierungService.class).to(RegistrierungServiceImpl.class);

		bind(IKatalogService.class).to(KatalogService.class);
		bind(IProtokollService.class).to(ProtokollService.class);

		bind(IAnonymisierungsservice.class).to(Anonymsierungsservice.class);

		bind(IAccessTokenDAO.class).to(CachingAccessTokenDao.class);
	}

	/**
	 * @see de.egladil.mkv.service.config.MKVModule#loadProperties(com.google.inject.Binder)
	 */
	@Override
	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", CONFIG_ROOT);
		Names.bindProperties(binder, properties);
	}
}
