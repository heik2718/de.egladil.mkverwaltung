//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.validation.ValidationUtils;
import de.egladil.mkv.service.TestUtils;

/**
 * @author heikew
 *
 */
public class LehrerregistrierungTest {

	private static final Logger LOG = LoggerFactory.getLogger(LehrerregistrierungTest.class);

	private Validator validator;

	private Lehrerregistrierung anfrage;

	@BeforeEach
	public void setUp() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		anfrage = TestUtils.validLehrerregistrierung();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_fails_when_not_agbGelesen() {
		// Arrange

		anfrage.setAgbGelesen(false);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Set<String> properties = new ValidationUtils().extractPropertyNames(errors);
		assertEquals(1, properties.size());
		assertTrue(properties.contains("agbGelesen"));
	}

	@Test
	public void validate_fails_when_schulkuerzel_null() {
		// Arrange

		anfrage.setSchulkuerzel(null);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("schulkuerzel", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_schulkuerzel_blank() {
		// Arrange

		anfrage.setSchulkuerzel("        ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("schulkuerzel", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_schulkuerzel_too_short() {
		// Arrange

		anfrage.setSchulkuerzel("ADRHZ6B");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("schulkuerzel", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_schulkuerzel_too_long() {
		// Arrange

		anfrage.setSchulkuerzel("ADRHZ6B3S");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("schulkuerzel", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_email_null() {
		// Arrange

		anfrage.setEmail(null);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_email_blank() {
		// Arrange

		anfrage.setEmail(" ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("email"));
	}

	@Test
	public void validate_fails_when_email_invalid() {
		// Arrange

		anfrage.setEmail("hallo-hallo@hallo.@hallo");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("email", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_passwort_null() {
		// Arrange

		anfrage.setPasswort(null);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("passwort", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_passwort_blank() {
		// Arrange

		anfrage.setPasswort(" ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(2, properties.size());
		assertTrue(properties.contains("passwort"));
		assertTrue(properties.contains("Lehrerregistrierung"));
	}

	@Test
	public void validate_fails_when_passwort_invalid() {
		// Arrange

		anfrage.setPasswort("Gerne<wider");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(2, properties.size());
		LOG.info(PrettyStringUtils.collectionToDefaultString(properties));
		assertTrue(properties.contains("passwort"));
		assertTrue(properties.contains("Lehrerregistrierung"));
	}

	@Test
	public void validate_fails_when_passwortWdh_null() {
		// Arrange

		anfrage.setPasswortWdh(null);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(2, properties.size());
		assertTrue(properties.contains("passwortWdh"));
		assertTrue(properties.contains("Lehrerregistrierung"));
	}

	@Test
	public void validate_fails_when_passwortWdh_blank() {
		// Arrange

		anfrage.setPasswortWdh(" ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(2, properties.size());
		assertTrue(properties.contains("passwortWdh"));
		assertTrue(properties.contains("Lehrerregistrierung"));
	}

	@Test
	public void validate_fails_when_passwortWdh_invalid() {
		// Arrange

		anfrage.setPasswortWdh("Gerne<wider");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(2, properties.size());
		LOG.info(PrettyStringUtils.collectionToDefaultString(properties));
		assertTrue(properties.contains("passwortWdh"));
		assertTrue(properties.contains("Lehrerregistrierung"));
	}

	@Test
	public void validate_fails_when_passwort_ungleich_passwortWdh() {
		// Arrange

		anfrage.setPasswortWdh("Qwertz!1");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString().equals("") ? cv.getRootBean().getClass().getSimpleName()
				: cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("Lehrerregistrierung"));
	}

	@Test
	public void validate_fails_when_kleber_not_blank() {
		// Arrange

		anfrage.setKleber("     ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("kleber", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_vorname_null() {
		// Arrange

		anfrage.setVorname(null);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("vorname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_vorname_blank() {
		// Arrange

		anfrage.setVorname(" ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("vorname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_vorname_too_long() {
		// Arrange

		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 101; i++) {
			sb.append("K");
		}
		anfrage.setVorname(sb.toString());
		assertEquals("Testsetting fehlerhaft", 101, anfrage.getVorname().length());

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("vorname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_vorname_invalid() {
		// Arrange

		anfrage.setVorname("Graf <KOKS>");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("vorname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_vorname_leer() {
		// Arrange

		anfrage.setVorname("");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("vorname"));
	}

	@Test
	public void validate_fails_when_nachname_null() {
		// Arrange

		anfrage.setNachname(null);

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("nachname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_nachname_blank() {
		// Arrange

		anfrage.setNachname(" ");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("nachname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_nachname_too_long() {
		// Arrange

		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 101; i++) {
			sb.append("K");
		}
		anfrage.setNachname(sb.toString());
		assertEquals("Testsetting fehlerhaft", 101, anfrage.getNachname().length());

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("nachname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_nachname_invalid() {
		// Arrange

		anfrage.setNachname("Graf <KOKS>");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<Lehrerregistrierung> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("nachname", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_nachname_leer() {
		// Arrange

		anfrage.setNachname("");

		// Act + Assert
		final Set<ConstraintViolation<Lehrerregistrierung>> errors = validator.validate(anfrage);

		assertFalse(errors.isEmpty());
		assertEquals(2, errors.size());

		assertEquals(2, errors.size());

		final Iterator<ConstraintViolation<Lehrerregistrierung>> iter = errors.iterator();
		final Set<String> properties = new HashSet<>();

		while (iter.hasNext()) {
			final ConstraintViolation<Lehrerregistrierung> cv = iter.next();
			properties.add(cv.getPropertyPath().toString());
			LOG.info(cv.getMessage());
		}

		assertEquals(1, properties.size());
		assertTrue(properties.contains("nachname"));
	}
}
