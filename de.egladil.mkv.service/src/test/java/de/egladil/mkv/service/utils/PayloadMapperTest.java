//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.utils;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.payload.request.Privatregistrierung;

/**
 * PayloadMapperTest
 */
public class PayloadMapperTest {

	@Test
	public void lehrerregToRegistrierungsanfrage_klappt() {
		// Arrange
		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();

		// Act
		final Registrierungsanfrage result = PayloadMapper.toRegistrierungsanfrage(payload);

		// Assert
		assertEquals(Anwendung.MKV, result.getAnwendung());
		assertEquals(payload.getEmail(), result.getEmail());
		assertEquals(payload.getEmail(), result.getLoginName());
		assertEquals(Role.MKV_LEHRER, result.getRole());
		assertEquals(payload.getPasswort(), result.getPasswort());
	}

	@Test
	public void privatRegToRegistrierungsanfrage_klappt() {
		// Arrange
		final Privatregistrierung payload = TestUtils.validPrivatregistrierung();

		// Act
		final Registrierungsanfrage result = PayloadMapper.toRegistrierungsanfrage(payload);

		// Assert
		assertEquals(Anwendung.MKV, result.getAnwendung());
		assertEquals(payload.getEmail(), result.getEmail());
		assertEquals(payload.getEmail(), result.getLoginName());
		assertEquals(Role.MKV_PRIVAT, result.getRole());
		assertEquals(payload.getPasswort(), result.getPasswort());
	}
}
