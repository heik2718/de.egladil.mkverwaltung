//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;

import java.util.Optional;

import javax.mail.Address;
import javax.mail.SendFailedException;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.config.OsUtils;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.kataloge.impl.KatalogService;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.config.EgladilMKVConfiguration;
import de.egladil.mkv.service.mail.MailDelegateStub;
import de.egladil.mkv.service.mail.MailserviceStub;
import de.egladil.mkv.service.mail.MailserviceThrowingStub;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;

/**
 * KatalogResourceTest
 */
public class KatalogResourceTest {

	private static final String CONFIG_ROOT = OsUtils.getDevConfigRoot();

	private static final String LANDKUERZEL_FEHLENDES = "EDCBA";

	private static final String LANDKUERZEL_VORHANDENES = "ABCDE";

	private Land land;

	private KatalogResource katalogResource;

	private KatalogService katalogDelegate;

	private MailDelegateStub mailDelegate;

	private IEgladilConfiguration configuration;

	private AuthorizationService authorizationService;

	@BeforeEach
	public void setUp() {
		land = new Land();
		land.setKuerzel(LANDKUERZEL_VORHANDENES);
		land.setName("Testland");

		authorizationService = Mockito.mock(AuthorizationService.class);

		katalogDelegate = Mockito.mock(KatalogService.class);
		Mockito.when(katalogDelegate.getLand(LANDKUERZEL_VORHANDENES)).thenReturn(Optional.of(land));
		Mockito.when(katalogDelegate.getLand(LANDKUERZEL_FEHLENDES)).thenReturn(Optional.empty());

		configuration = new EgladilMKVConfiguration(CONFIG_ROOT);

		mailDelegate = new MailDelegateStub(new MailserviceStub(), configuration);
		mailDelegate.setSendMail(true);
		katalogResource = new KatalogResource(katalogDelegate, mailDelegate, authorizationService);
	}

	@Test
	public void schuleintragBeantragen_mit_anschrift_versendet_mail_und_gibt_200_zurueck_wenn_payload_valid() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = new SchuleAnschriftKatalogantrag();
		payload.setEmail("blubb@egladil.de");
		payload.setHausnummer("6");
		payload.setPlz("53636");
		payload.setName("Testschule");
		payload.setOrt("Testort");
		payload.setStrasse("Bahnhofstraße");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_VORHANDENES, payload);

		// Assert
		Mockito.verify(katalogDelegate, times(1)).getLand(LANDKUERZEL_VORHANDENES);
		assertTrue(mailDelegate.isSendMailCalled());
		assertEquals(200, response.getStatus());

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Vielen Dank. Der Katalogeintrag wird zeitnah erstellt. Sie wurden per Email informiert. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_url_versendet_mail_und_gibt_200_zurueck_wenn_payload_valid() {
		// Arrange
		final SchuleUrlKatalogantrag payload = new SchuleUrlKatalogantrag();
		payload.setEmail("blubb@egladil.de");
		payload.setName("Testschule");
		payload.setUrl("www.testschule.de");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_VORHANDENES, payload);

		// Assert
		Mockito.verify(katalogDelegate, times(1)).getLand(LANDKUERZEL_VORHANDENES);
		assertTrue(mailDelegate.isSendMailCalled());
		assertEquals(200, response.getStatus());

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Vielen Dank. Der Katalogeintrag wird zeitnah erstellt. Sie wurden per Email informiert. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_url_gibt_404_not_found_zurueck_wenn_land_unbekannt_und_landName_leer() {
		// Arrange
		final SchuleUrlKatalogantrag payload = new SchuleUrlKatalogantrag();
		payload.setEmail("blubb@egladil.de");
		payload.setName("Testschule");
		payload.setUrl("www.testschule.de");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_FEHLENDES, payload);

		// Assert
		Mockito.verify(katalogDelegate, times(1)).getLand(LANDKUERZEL_FEHLENDES);
		assertFalse(mailDelegate.isSendMailCalled());
		assertEquals(902, response.getStatus());

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Der Name des Landes/Bundeslandes darf nicht leer sein.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_anschrift_gibt_202_zurueck_wenn_land_unbekannt_und_landName_nicht_leer() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();
		payload.setNameLand("Marokko");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_FEHLENDES, payload);

		// Assert
		Mockito.verify(katalogDelegate, times(1)).getLand(LANDKUERZEL_FEHLENDES);
		assertTrue(mailDelegate.isSendMailCalled());
		assertEquals(200, response.getStatus());

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Vielen Dank. Der Katalogeintrag wird zeitnah erstellt. Sie wurden per Email informiert. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_url_gibt_403_forbidden_zurueck_wenn_kleber_nicht_leer() {
		// Arrange
		final SchuleUrlKatalogantrag payload = new SchuleUrlKatalogantrag();
		payload.setEmail("blubb@egladil.de");
		payload.setName("Testschule");
		payload.setUrl("www.testschule.de");
		payload.setKleber("hi!");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_VORHANDENES, payload);

		// Assert
		assertEquals(403, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Diese Anfrage ist nicht erlaubt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_url_gibt_902_constraintViolation_zurueck_wenn_url_invalid() {
		// Arrange
		final SchuleUrlKatalogantrag payload = new SchuleUrlKatalogantrag();
		payload.setEmail("blubb@egladil.de");
		payload.setName("Testschule");
		payload.setUrl("file:///root/etc/");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_VORHANDENES, payload);

		// Assert
		assertEquals(902, response.getStatus());

		assertFalse(mailDelegate.isSendMailCalled());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_anschrift_gibt_902_constraintViolation_zurueck_wenn_plz_invalid() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();
		payload.setPlz("12345678901");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_VORHANDENES, payload);

		// Assert
		assertEquals(902, response.getStatus());

		assertFalse(mailDelegate.isSendMailCalled());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_anschrift_gibt_902_constraintViolation_zurueck_wenn_nameLand_invalid() {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();
		payload.setNameLand("cd ../../ mv config config.old");

		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_FEHLENDES, payload);

		// Assert
		assertEquals(902, response.getStatus());

		assertFalse(mailDelegate.isSendMailCalled());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_mit_url_gibt_400_bad_request_zurueck_wenn_payload_null() {
		// Act
		final Response response = katalogResource.schuleintragBeantragen(LANDKUERZEL_VORHANDENES, null);

		// Assert
		assertEquals(400, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_returns_400_when_empfaengerAddressInvalid() {
		// Arrange
		final SchuleUrlKatalogantrag katalogantrag = TestUtils.validSchuleUrlKatalogantrag();
		katalogantrag.setEmail("123@435.et");
		final SendFailedException cause = Mockito.mock(SendFailedException.class);
		Mockito.when(cause.getInvalidAddresses()).thenReturn(new Address[] { TestUtils.getAddress(katalogantrag.getEmail()) });
		Mockito.when(cause.getValidSentAddresses()).thenReturn(null);
		Mockito.when(cause.getValidUnsentAddresses()).thenReturn(null);
		final InvalidMailAddressException exceptionToThrow = new InvalidMailAddressException("invalid mails", cause);
		mailDelegate = new MailDelegateStub(new MailserviceThrowingStub(exceptionToThrow), configuration);
		mailDelegate.setSendMail(true);
		katalogResource = new KatalogResource(katalogDelegate, mailDelegate, authorizationService);

		// Act
		final Response response = katalogResource.schuleintragBeantragen("ABCDE", katalogantrag);

		// Assert
		assertEquals(400, response.getStatus());

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Der Katalogantrag konnte nicht weitergeleitet werden. Die eingegebene Mailadresse existiert nicht.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_returns_500_when_hiddenEmpfaengerAddressInvalid() {
		// Arrange
		final SchuleUrlKatalogantrag katalogantrag = TestUtils.validSchuleUrlKatalogantrag();
		final SendFailedException cause = Mockito.mock(SendFailedException.class);
		Mockito.when(cause.getInvalidAddresses()).thenReturn(new Address[] { TestUtils.getAddress("typo@1234.et") });
		Mockito.when(cause.getValidSentAddresses()).thenReturn(new Address[] { TestUtils.getAddress(katalogantrag.getEmail()) });
		Mockito.when(cause.getValidUnsentAddresses()).thenReturn(null);
		final InvalidMailAddressException exceptionToThrow = new InvalidMailAddressException("invalid mails", cause);
		mailDelegate = new MailDelegateStub(new MailserviceThrowingStub(exceptionToThrow), configuration);
		mailDelegate.setSendMail(true);
		katalogResource = new KatalogResource(katalogDelegate, mailDelegate, authorizationService);

		// Act
		final Response response = katalogResource.schuleintragBeantragen("ABCDE", katalogantrag);

		// Assert
		assertEquals(500, response.getStatus());

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
	}
}
