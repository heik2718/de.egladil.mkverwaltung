//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.response;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.LoesungszettelRohdaten;
import de.egladil.mkv.persistence.domain.enums.Auswertungsquelle;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.enums.Sprache;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.payload.response.PublicAntwortbuchstabe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * EnhancePublicTeilnehmerCommandTest
 */
public class EnhancePublicTeilnehmerCommandTest {

	private TeilnehmerFacade teilnehmerFacade;

	private PublicTeilnehmer publicTeilnehmer;

	@BeforeEach
	void setUp() {
		teilnehmerFacade = Mockito.mock(TeilnehmerFacade.class);
		publicTeilnehmer = new PublicTeilnehmer();
	}

	@Test
	@DisplayName("Konstruktor throws IllegalArgumentException wenn publicTeilnehmer null")
	void konstruktorPublicTeilnehmerNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new EnhancePublicTeilnehmerCommand(null, teilnehmerFacade);
		});
		assertEquals("publicTeilnehmer null", ex.getMessage());
	}

	@Test
	@DisplayName("Konstruktor throws IllegalArgumentException wenn teilnehmerFacade null")
	void konstruktorPublicTeilnehmerFacadeNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new EnhancePublicTeilnehmerCommand(publicTeilnehmer, null);
		});
		assertEquals("teilnehmerFacade null", ex.getMessage());
	}

	@Test
	@DisplayName("ohne Lösungszettel bleiben punkte und PublicAntwortbuchstabe null.")
	void executeOhneLoesungszettel() {
		final EnhancePublicTeilnehmerCommand command = new EnhancePublicTeilnehmerCommand(publicTeilnehmer, teilnehmerFacade);
		command.execute();

		assertNull(publicTeilnehmer.getAntworten());
		assertNull(publicTeilnehmer.getKaengurusprung());
		assertNull(publicTeilnehmer.getPunkte());
	}

	@Test
	@DisplayName("mit Lösungszettel werden die Punkte und Antwortbuchstaben initialisiert.")
	void executeMitLoesungszettel() {
		final LoesungszettelRohdaten daten = new LoesungszettelRohdaten.Builder(Auswertungsquelle.ONLINE, Klassenstufe.EINS, 3,
			3800, "r,f,r,n,r,n,f,r,r,r,f,r", "r,f,r,n,r,n,f,r,r,r,f,r").antwortcode("ABENDNECADDB").build();
		final Loesungszettel loesungszettel = new Loesungszettel.Builder("2018", Teilnahmeart.P, "XCDFR6G7", 1, daten)
			.withSprache(Sprache.de).build();
		final String loesungszettelkuerzel = loesungszettel.getKuerzel();

		final PublicAntwortbuchstabe[] antwortbuchstaben = new PublicAntwortbuchstabe[] { new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("E", 5), new PublicAntwortbuchstabe("-", 0),
			new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("E", 5),
			new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("D", 4),
			new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("B", 2) };

		publicTeilnehmer.setAntworten(antwortbuchstaben);
		publicTeilnehmer.setLoesungszettelKuerzel(loesungszettelkuerzel);

		final PublicAntwortbuchstabe[] expectedAntwortbuchstaben = new PublicAntwortbuchstabe[] {
			new PublicAntwortbuchstabe("A", 1), new PublicAntwortbuchstabe("B", 2), new PublicAntwortbuchstabe("E", 5),
			new PublicAntwortbuchstabe("-", 0), new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("-", 0),
			new PublicAntwortbuchstabe("E", 5), new PublicAntwortbuchstabe("C", 3), new PublicAntwortbuchstabe("A", 1),
			new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("D", 4), new PublicAntwortbuchstabe("B", 2) };

		Mockito.when(teilnehmerFacade.getUniqueLoesungszettel(loesungszettelkuerzel)).thenReturn(Optional.of(loesungszettel));

		final EnhancePublicTeilnehmerCommand command = new EnhancePublicTeilnehmerCommand(publicTeilnehmer, teilnehmerFacade);
		command.execute();

		assertArrayEquals(expectedAntwortbuchstaben, antwortbuchstaben);
		assertEquals("38", publicTeilnehmer.getPunkte());
		assertEquals(Integer.valueOf(3), publicTeilnehmer.getKaengurusprung());
	}
}
