//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service;

import org.junit.jupiter.api.BeforeEach;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.service.config.MKVTestModule;

/**
 * @author heikew
 *
 */
public abstract class AbstractGuiceIT {

	@BeforeEach
	public void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		final Injector injector = Guice.createInjector(new MKVTestModule());
		injector.injectMembers(this);
	}
}
