//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.config;

import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.common.config.OsUtils;

/**
 * EgladilMKVConfigurationTest
 */
public class EgladilMKVConfigurationTest {

	private EgladilMKVConfiguration egladilMKVConfiguration;

	@BeforeEach
	public void setUp() {
		egladilMKVConfiguration = new EgladilMKVConfiguration(getConfigHome());
	}

	private String getConfigHome() {
		return OsUtils.getDevConfigRoot();
	}

	@Test
	public void alle_konfigurationsparameter_gesetzt() {
		// Assert
		// @formatter:off
		final List<String> propertyNames = Arrays.asList(new String[]{
			"upload.path"
			, "cors.allowOrigin"
			, "csrf.targetOrigin"
			, "csrf.blockOnMissingOriginReferer"
			, "csrf.subpath.ping"
			, "csrf.confirmUrls"
			, "mail.mailserver.vorhanden"
			, "confirm.registrierung.lehrer.url"
			, "confirm.registrierung.privat.url"
			, "mail.registrierung.subject"
			, "mail.resetpassword.url"
			, "mail.resetpassword.dummycode"
			, "mail.resetpassword.subject"
			, "mail.passwort.neu.subject"
			, "protokoll.modus"
		});
		// @formatter:on

		// Act + Assert
		for (final String propertyName : propertyNames) {
			try {
				egladilMKVConfiguration.getProperty(propertyName);
			} catch (final EgladilConfigurationException e) {
				fail(propertyName + " - " + e.getMessage());
			}
		}
	}
}
