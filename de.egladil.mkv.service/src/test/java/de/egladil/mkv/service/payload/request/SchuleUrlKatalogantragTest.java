//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.service.TestUtils;

/**
 * @author heikew
 *
 */
public class SchuleUrlKatalogantragTest {

	private static final Logger LOG = LoggerFactory.getLogger(SchuleUrlKatalogantragTest.class);

	private SchuleUrlKatalogantrag antrag;

	private Validator validator;

	@BeforeEach
	public void setUp() {
		antrag = TestUtils.validSchuleUrlKatalogantrag();
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@Test
	public void validate_passes_when_valid() {
		// Act + Assert
		final Set<ConstraintViolation<SchuleUrlKatalogantrag>> errors = validator.validate(antrag);

		assertTrue(errors.isEmpty());
	}

	@Test
	public void validate_fails_when_url_null() {
		// Arrange

		antrag.setUrl(null);

		// Act + Assert
		final Set<ConstraintViolation<SchuleUrlKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleUrlKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("url", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_url_blank() {
		// Arrange

		antrag.setUrl(" ");

		// Act + Assert
		final Set<ConstraintViolation<SchuleUrlKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleUrlKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("url", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_ort_zu_lang() {
		// Arrange
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 301; i++) {
			sb.append("A");
		}
		final String name = sb.toString();
		assertEquals("Testsetting: brauchen 301 Zeichen", 301, name.length());
		antrag.setUrl(name);

		// Act + Assert
		final Set<ConstraintViolation<SchuleUrlKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleUrlKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("url", cv.getPropertyPath().toString());
	}

	@Test
	public void validate_fails_when_url_invalid() {
		// Arrange
		antrag.setUrl("Hoпр://ttt.de");

		// Act + Assert
		final Set<ConstraintViolation<SchuleUrlKatalogantrag>> errors = validator.validate(antrag);

		assertFalse(errors.isEmpty());
		assertEquals(1, errors.size());

		final ConstraintViolation<SchuleUrlKatalogantrag> cv = errors.iterator().next();
		LOG.debug(cv.getMessage());
		assertEquals("url", cv.getPropertyPath().toString());
	}
}
