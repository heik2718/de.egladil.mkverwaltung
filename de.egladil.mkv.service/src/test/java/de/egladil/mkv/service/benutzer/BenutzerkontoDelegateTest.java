//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

import javax.mail.Address;
import javax.mail.SendFailedException;
import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.mail.MailserviceThrowingStub;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.utils.PayloadMapper;

/**
 * BenutzerkontoDelegateIT
 */
public class BenutzerkontoDelegateTest {

	private static final String EMAIL = "x@x.de";

	private static final String TEMP_PASSWORD = "hdget7Z5";

	private static final String BENUTZER_UUID = "Hisafohwi-figua-fwhuofho";

	private static final char[] PWD_NEU = "Qwertz!1".toCharArray();

	private static final int GUELTIGKEIT_LINK = 15;

	private BenutzerkontoDelegate delegate;

	private IAuthenticationService authenticationService;

	private IBenutzerService benutzerService;

	private IRegistrierungService registrierungService;

	private IProtokollService protokollService;

	private IAnonymisierungsservice anonymisierungsservice;

	@BeforeEach
	public void setUp() {
		registrierungService = Mockito.mock(IRegistrierungService.class);
		authenticationService = Mockito.mock(IAuthenticationService.class);
		benutzerService = Mockito.mock(IBenutzerService.class);
		protokollService = Mockito.mock(IProtokollService.class);
		anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);
		final MailDelegate mailDelegate = Mockito.mock(MailDelegate.class);
		delegate = new BenutzerkontoDelegate(registrierungService, authenticationService, benutzerService, mailDelegate,
			protokollService, anonymisierungsservice);
	}

	@Nested
	@DisplayName("Tests für authenticateWithTemporaryPasswort ")
	class AuthenticateWithTempPassword {
		@Test
		@DisplayName("authenticateWithTemporaryPasswort  gibt UnknownAccountException weiter")
		public void unknownAccout() {
			// Arrange
			Mockito.when(authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD))
				.thenThrow(new UnknownAccountException("den gibt es nicht"));

			// Act + Assert
			final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
				delegate.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD);
			});
			assertEquals("den gibt es nicht", ex.getMessage());

		}

		@Test
		@DisplayName("authenticateWithTemporaryPasswort  gibt IncorrectCredentialsException weiter")
		public void incorrectCredentialsException() {
			// Arrange
			Mockito.when(authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD))
				.thenThrow(new IncorrectCredentialsException("den gibt es nicht"));

			// Act + Assert
			final Throwable ex = assertThrows(IncorrectCredentialsException.class, () -> {
				delegate.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD);
			});
			assertEquals("den gibt es nicht", ex.getMessage());

		}

		@Test
		@DisplayName("authenticateWithTemporaryPasswort  gibt DisabledAccountException weiter")
		public void disabledAccount() {
			// Arrange
			Mockito.when(authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD))
				.thenThrow(new DisabledAccountException("den gibt es nicht"));

			// Act + Assert
			final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
				delegate.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD);
			});
			assertEquals("den gibt es nicht", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateWithTemporaryPasswort  gibt ExcessiveAttemptsException weiter")
		public void excessiveAttempts() {
			// Arrange
			Mockito.when(authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD))
				.thenThrow(new ExcessiveAttemptsException("zu schnell"));

			// Act + Assert
			final Throwable ex = assertThrows(ExcessiveAttemptsException.class, () -> {
				delegate.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD);
			});
			assertEquals("zu schnell", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateWithTemporaryPasswort  gibt EgladilAuthenticationException weiter")
		public void egladilAuthenticationException() {
			// Arrange
			Mockito.when(authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD))
				.thenThrow(new EgladilAuthenticationException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilAuthenticationException.class, () -> {
				delegate.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateWithTemporaryPasswort  gibt IllegalArgumentException weiter")
		public void illegalArgumentException() {
			// Arrange
			Mockito.when(authenticationService.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD))
				.thenThrow(new IllegalArgumentException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				delegate.authenticateWithTemporaryPasswort(Anwendung.MKV, EMAIL, TEMP_PASSWORD);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

	}

	@Nested
	@DisplayName("Tests für changePasswortAndSendMail")
	class ChangePasswortAndSendMail {
		// FIXME XXX
		@Test
		@DisplayName("changePasswordAndSendMail  gibt EgladilStorageException weiter")
		public void egladilStorageException() {
			// Arrange
			Mockito.when(authenticationService.changePasswort(Anwendung.MKV, BENUTZER_UUID, PWD_NEU))
				.thenThrow(new EgladilStorageException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
				delegate.changePasswordAndSendMail(BENUTZER_UUID, PWD_NEU);
			});
			assertEquals("irgendwas", ex.getMessage());

		}

		@Test
		@DisplayName("changePasswordAndSendMail  gibt EgladilBVException weiter")
		public void egladilBVException() {
			// Arrange
			Mockito.when(authenticationService.changePasswort(Anwendung.MKV, BENUTZER_UUID, PWD_NEU))
				.thenThrow(new EgladilBVException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilBVException.class, () -> {
				delegate.changePasswordAndSendMail(BENUTZER_UUID, PWD_NEU);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("changePasswordAndSendMail gibt EgladilDuplicateEntryException weiter")
		public void egladilDuplicateEntryException() {
			// Arrange
			Mockito.when(authenticationService.changePasswort(Anwendung.MKV, BENUTZER_UUID, PWD_NEU))
				.thenThrow(new EgladilDuplicateEntryException("hammwer schon"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilDuplicateEntryException.class, () -> {
				delegate.changePasswordAndSendMail(BENUTZER_UUID, PWD_NEU);
			});
			assertEquals("hammwer schon", ex.getMessage());
		}

		@Test
		@DisplayName("changePasswordAndSendMail  gibt EgladilConcurrentModificationException weiter")
		public void concurrentModification() {
			// Arrange
			Mockito.when(authenticationService.changePasswort(Anwendung.MKV, BENUTZER_UUID, PWD_NEU))
				.thenThrow(new EgladilConcurrentModificationException("wer anders war schneller"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilConcurrentModificationException.class, () -> {
				delegate.changePasswordAndSendMail(BENUTZER_UUID, PWD_NEU);
			});
			assertEquals("wer anders war schneller", ex.getMessage());
		}

		@Test
		@DisplayName("changePasswordAndSendMail gibt IllegalArgumentException weiter")
		public void illegalArgument() {
			// Arrange
			Mockito.when(authenticationService.changePasswort(Anwendung.MKV, BENUTZER_UUID, PWD_NEU))
				.thenThrow(new IllegalArgumentException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				delegate.changePasswordAndSendMail(BENUTZER_UUID, PWD_NEU);
			});
			assertEquals("irgendwas", ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für authenticateAndChangeEmail")
	class AuthenticateAndChangeEmail {
		@Test
		@DisplayName("authenticateAndChangeEmail gibt EgladilStorageException weiter")
		public void egladilStorageExceptionr() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzer);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzer);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenThrow(new EgladilStorageException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());

		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt EgladilBVException weiter")
		public void egladilBVException() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzer);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzer);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenThrow(new EgladilBVException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilBVException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt EgladilDuplicateEntryException weiter")
		public void duplicateEntry() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzer);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzer);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer))
				.thenThrow(new EgladilDuplicateEntryException("hammwer schon"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilDuplicateEntryException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("hammwer schon", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail protokolliert")
		public void protokollierungstest() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzer);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzer);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);
			final String was = "[alt=alte-mailadresse@egladil.de, neu=neue-mailadresse@egladil.de]";
			final Ereignis ereignis = new Ereignis(BENUTZER_UUID, was, Ereignisart.EMAIL_GEAENDERT.getKuerzel());
			Mockito.when(protokollService.protokollieren(Ereignisart.EMAIL_GEAENDERT.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

			// Act + Assert
			delegate.authenticateAndChangeEmail(payload);
			Mockito.verify(protokollService, times(1)).protokollieren(Ereignisart.EMAIL_GEAENDERT.getKuerzel(), BENUTZER_UUID, was);
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt EgladilConcurrentModificationException weiter")
		public void concurrentModification() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzer);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzer);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer))
				.thenThrow(new EgladilConcurrentModificationException("wer anders war schneller"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilConcurrentModificationException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("wer anders war schneller", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt IllegalArgumentException weiter")
		public void illegalArgument() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new IllegalArgumentException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt IncorrectCredentialsException weiter")
		public void incorrectCredentials() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new IncorrectCredentialsException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(IncorrectCredentialsException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt UnknownAccountException weiter")
		public void unknownAccount() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new UnknownAccountException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt DisabledAccountException weiter")
		public void disabledAccount() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new DisabledAccountException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt ExcessiveAttemptsException weiter")
		public void excessiveAttempts() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new ExcessiveAttemptsException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(ExcessiveAttemptsException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt EgladilAuthenticationException weiter")
		public void authenticationException() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new EgladilAuthenticationException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(EgladilAuthenticationException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authenticateAndChangeEmail gibt EgladilStorageException weiter")
		public void egladilStorageException() {
			// Arrange
			final MailadresseAendernPayload payload = TestUtils.validMailadresseAendernPayload();
			final Benutzerkonto benutzer = new Benutzerkonto();
			benutzer.setUuid(BENUTZER_UUID);
			final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
				payload.getPassword().toCharArray());
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new EgladilStorageException("irgendwas"));
			Mockito.when(benutzerService.persistBenutzerkonto(benutzer)).thenReturn(benutzer);

			// Act + Assert
			final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
				delegate.authenticateAndChangeEmail(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für generateResetPasswordCode")
	class GenerateResetPasswordCode {
		@Test
		@DisplayName("generateResetPasswordCode gibt IllegalArgumentException weiter")
		public void illegalArgument() {
			// Arrange
			Mockito.when(benutzerService.generateResetPasswordCode(EMAIL, Anwendung.MKV, GUELTIGKEIT_LINK))
				.thenThrow(new IllegalArgumentException("irgendwas is null"));

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				delegate.generateResetPasswordCode(EMAIL, GUELTIGKEIT_LINK);
			});
			assertEquals("irgendwas is null", ex.getMessage());

		}

		@Test
		@DisplayName("generateResetPasswordCode gibt UnknownAccountException weiter")
		public void unknownAccount() {
			// Arrange
			Mockito.when(benutzerService.generateResetPasswordCode(EMAIL, Anwendung.MKV, GUELTIGKEIT_LINK))
				.thenThrow(new UnknownAccountException("kennmer nich"));

			// Act + Assert
			final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
				delegate.generateResetPasswordCode(EMAIL, GUELTIGKEIT_LINK);
			});
			assertEquals("kennmer nich", ex.getMessage());
		}

		@Test
		@DisplayName("generateResetPasswordCode gibt ExcessiveAttemptsException weiter")
		public void excessiveAttemts() {
			// Arrange
			Mockito.when(benutzerService.generateResetPasswordCode(EMAIL, Anwendung.MKV, GUELTIGKEIT_LINK))
				.thenThrow(new ExcessiveAttemptsException("zu oft"));

			// Act + Assert
			final Throwable ex = assertThrows(ExcessiveAttemptsException.class, () -> {
				delegate.generateResetPasswordCode(EMAIL, GUELTIGKEIT_LINK);
			});
			assertEquals("zu oft", ex.getMessage());
		}

		@Test
		@DisplayName("generateResetPasswordCode gibt EgladilAuthenticationException weiter")
		public void authenticationException() {
			// Arrange
			Mockito.when(benutzerService.generateResetPasswordCode(EMAIL, Anwendung.MKV, GUELTIGKEIT_LINK))
				.thenThrow(new EgladilAuthenticationException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilAuthenticationException.class, () -> {
				delegate.generateResetPasswordCode(EMAIL, GUELTIGKEIT_LINK);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("generateResetPasswordCode gibt DisabledAccountException weiter")
		public void disabledAccount() {
			// Arrange
			Mockito.when(benutzerService.generateResetPasswordCode(EMAIL, Anwendung.MKV, GUELTIGKEIT_LINK))
				.thenThrow(new DisabledAccountException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
				delegate.generateResetPasswordCode(EMAIL, GUELTIGKEIT_LINK);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("generateResetPasswordCode gibt EgladilStorageException weiter")
		public void egladilStorageException() {
			// Arrange
			Mockito.when(benutzerService.generateResetPasswordCode(EMAIL, Anwendung.MKV, GUELTIGKEIT_LINK))
				.thenThrow(new EgladilStorageException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
				delegate.generateResetPasswordCode(EMAIL, GUELTIGKEIT_LINK);
			});
			assertEquals("irgendwas", ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für benutzerkontoAnlegen")
	class KontoAnlegen {

		@SuppressWarnings("unchecked")
		@DisplayName("benutzerkontoAnlegen gibt ConstraintViolationException weiter")
		public void constraintViolation() {
			// Arrange
			final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
			final Registrierungsanfrage registrierungsanfrage = PayloadMapper.toRegistrierungsanfrage(payload);
			Mockito.when(registrierungService.register(registrierungsanfrage)).thenThrow(ConstraintViolationException.class);

			// Act + Assert
			final Throwable ex = assertThrows(ConstraintViolationException.class, () -> {
				delegate.benutzerkontoAnlegen(payload);
			});
			assertEquals("", ex.getMessage());

		}

		@Test
		@DisplayName("benutzerkontoAnlegen gibt IllegalArgumentException weiter")
		public void illegalArgument() {
			// Arrange
			final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
			final Registrierungsanfrage registrierungsanfrage = PayloadMapper.toRegistrierungsanfrage(payload);
			Mockito.when(registrierungService.register(registrierungsanfrage)).thenThrow(new IllegalArgumentException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				delegate.benutzerkontoAnlegen(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("benutzerkontoAnlegen gibt EgladilStorageException weiter")
		public void egladilStorageException() {
			// Arrange
			final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
			final Registrierungsanfrage registrierungsanfrage = PayloadMapper.toRegistrierungsanfrage(payload);
			Mockito.when(registrierungService.register(registrierungsanfrage)).thenThrow(new EgladilStorageException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
				delegate.benutzerkontoAnlegen(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("benutzerkontoAnlegen gibt EgladilDuplicateEntryException weiter")
		public void duplicateEntry() {
			// Arrange
			final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
			final Registrierungsanfrage registrierungsanfrage = PayloadMapper.toRegistrierungsanfrage(payload);
			Mockito.when(registrierungService.register(registrierungsanfrage))
				.thenThrow(new EgladilDuplicateEntryException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilDuplicateEntryException.class, () -> {
				delegate.benutzerkontoAnlegen(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("benutzerkontoAnlegen gibt EgladilConcurrentModificationException weiter")
		public void concurrentModification() {
			// Arrange
			final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
			final Registrierungsanfrage registrierungsanfrage = PayloadMapper.toRegistrierungsanfrage(payload);
			Mockito.when(registrierungService.register(registrierungsanfrage))
				.thenThrow(new EgladilConcurrentModificationException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilConcurrentModificationException.class, () -> {
				delegate.benutzerkontoAnlegen(payload);
			});
			assertEquals("irgendwas", ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für Methoden, die Exceptions nur fangen und loggen")
	class QuietActions {
		@Test
		public void removeTempPasswordQuietly() {
			// Arrange
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);
			final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
			aktivierungsdaten.setConfirmationCode(TEMP_PASSWORD);
			benutzerkonto.addAktivierungsdaten(aktivierungsdaten);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzerkonto)).thenThrow(new EgladilStorageException("irgendwas"));

			// Act
			try {
				delegate.removeTempPasswordQuietly(benutzerkonto, TEMP_PASSWORD);
			} catch (final Exception e) {
				// Assert
				fail("Das darf nicht");
			}
		}

		@Test
		@DisplayName("sendPasswortmailQuietly fängt EgladilMailException")
		public void egladilMailException() {
			// Arrange
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);
			final IMailservice mailService = new MailserviceThrowingStub(
				new EgladilMailException("irgendwas mit der Mailservice-KOnfiguration"));
			final IEgladilConfiguration config = Mockito.mock(IEgladilConfiguration.class);
			Mockito.when(config.getProperty("mail.mailserver.vorhanden")).thenReturn("true");
			final MailDelegate mailDelegate = new MailDelegate(mailService, config);
			delegate.setMailDelegate(mailDelegate);
			// Act
			try {
				delegate.sendPasswortmailQuietly(benutzerkonto);
			} catch (final Exception e) {
				// Assert
				fail("Das darf nicht");
			}
		}

		@Test
		@DisplayName("sendPasswortmailQuietly fängt InvalidMailAddressException")
		public void invalidMailAddressException() {
			// Arrange
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);
			final SendFailedException cause = Mockito.mock(SendFailedException.class);

			final Address invalidAddress = TestUtils.getAddress("evil@address.et");
			Mockito.when(cause.getInvalidAddresses()).thenReturn(new Address[] { invalidAddress });
			Mockito.when(cause.getValidSentAddresses()).thenReturn(null);
			Mockito.when(cause.getValidUnsentAddresses()).thenReturn(null);
			final InvalidMailAddressException exceptionToThrow = new InvalidMailAddressException("die Adresse ist ungültig", cause);
			final IMailservice mailService = new MailserviceThrowingStub(exceptionToThrow);
			final IEgladilConfiguration config = Mockito.mock(IEgladilConfiguration.class);
			Mockito.when(config.getProperty("mail.mailserver.vorhanden")).thenReturn("true");
			final MailDelegate mailDelegate = new MailDelegate(mailService, config);
			delegate.setMailDelegate(mailDelegate);
			// Act
			try {
				delegate.sendPasswortmailQuietly(benutzerkonto);
			} catch (final Exception e) {
				// Assert
				fail("Das darf nicht");
			}
		}
	}

	@Nested
	@DisplayName("Tests für benutzerkontoAnonymisieren")
	class KontoAnonymisieren {
		@Test
		public void aktivierungsdatenNull() {
			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				delegate.benutzerkontoAnonymisieren(null);
			});
			assertEquals("aktivierungsdaten darf nicht null sein", ex.getMessage());
		}

		@Test
		@DisplayName("benutzerkontoAnonymisieren fuft Anonymisierung auf wenn benutzerkonto null")
		public void benutzerkontoAnonymisieren_ruft_anonymisierung_auf_wenn_benutzerkonto_null() {
			// Arrange
			final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
			aktivierungsdaten.setConfirmationCode(TEMP_PASSWORD);

			// Act
			delegate.benutzerkontoAnonymisieren(aktivierungsdaten);

			// Assert
			Mockito.verify(anonymisierungsservice, times(0)).kontoAnonymsieren(BENUTZER_UUID, "nicht abgeschlossene Registrierung");
		}

		@Test
		@DisplayName("benutzerkontoAnonymisieren fuft Anonymisierung auf")
		public void anonymisieren() {
			// Arrange
			final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);
			aktivierungsdaten.setBenutzerkonto(benutzerkonto);

			Mockito.when(anonymisierungsservice.kontoAnonymsieren(BENUTZER_UUID, "nicht abgeschlossene Registrierung"))
				.thenReturn("");

			// Act
			delegate.benutzerkontoAnonymisieren(aktivierungsdaten);

			// Assert
			Mockito.verify(anonymisierungsservice, times(1)).kontoAnonymsieren(BENUTZER_UUID, "nicht abgeschlossene Registrierung");
		}
	}
}
