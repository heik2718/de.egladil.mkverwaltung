//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.TestUtils;

/**
 * ExpiredActivationStrategyTest
 */
public class ExpiredActivationStrategyTest {

	private IBenutzerService benutzerService;

	private ExpiredActivationStrategy strategy;

	private IKontaktUndTeilnahmeCallback callback;

	private IRegistrierungService registrierungService;

	@BeforeEach
	public void setUp() {
		benutzerService = Mockito.mock(IBenutzerService.class);
		callback = Mockito.mock(IKontaktUndTeilnahmeCallback.class);
		registrierungService = Mockito.mock(IRegistrierungService.class);
		strategy = new ExpiredActivationStrategy(registrierungService, benutzerService);
	}

	@Test
	public void apply_registration_returns_expiredActivation() throws Exception {
		// Arrange
		final String uuid = "62368-4356";
		final String jahr = "2017";
		final Optional<IMKVKonto> kontakt = Optional.of(TestUtils.validActiveLehrerkonto(uuid, jahr));

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(uuid);
		aktivierungsdaten.setBenutzerkonto(benutzer);
		aktivierungsdaten.setConfirmationCode("a65ef5-d23");

		final IAnonymisierungsservice anonymisierungService = Mockito.mock(IAnonymisierungsservice.class);
		Mockito.when(anonymisierungService.kontoAnonymsieren(uuid, kontakt, "Expired Activation")).thenReturn("");

		// Act
		final ConfirmationStatus actual = strategy.applyOnRegistration(aktivierungsdaten, jahr, kontakt, callback, anonymisierungService);

		// Assert
		assertEquals(ConfirmationStatus.expiredActivation, actual);
		Mockito.verify(anonymisierungService, times(1)).kontoAnonymsieren(uuid, kontakt, IConfirmationStrategy.CONTEXT_MESSAGE);
	}
}
