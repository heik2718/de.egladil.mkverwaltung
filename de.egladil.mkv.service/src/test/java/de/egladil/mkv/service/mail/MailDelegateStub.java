//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.mail;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.email.service.IMailservice;

/**
 * MailDelegateStub
 */
public class MailDelegateStub extends MailDelegate {

	/**
	 * Erzeugt eine Instanz von MailDelegateStub
	 */
	public MailDelegateStub(IMailservice mailService, IEgladilConfiguration configuration) {
		super(mailService, configuration);
	}

	public boolean isSendMailCalled() {
		MailserviceStub mailService = (MailserviceStub) getMailService();
		return mailService.isSendMailCalled();
	}

}
