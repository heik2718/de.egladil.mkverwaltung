//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.TestUtils;

/**
 * SchulkontaktNormalActivationTest
 */
public class NormalActivationStrategyTest {

	private IRegistrierungService registrierungService;

	private IKontaktUndTeilnahmeCallback callback;

	private NormalActivationStrategy strategy;

	@BeforeEach
	public void setUp() {
		registrierungService = Mockito.mock(IRegistrierungService.class);
		callback = Mockito.mock(IKontaktUndTeilnahmeCallback.class);
		strategy = new NormalActivationStrategy(registrierungService);
	}

	@Test
	public void apply_registration_returns_normalActivation() throws Exception {
		// Arrange
		final String uuid = "62368-4356";
		final String jahr = "2017";
		final Optional<IMKVKonto> kontakt = Optional.of(TestUtils.validActiveLehrerkonto(uuid, jahr));

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		final Benutzerkonto benutzer = new Benutzerkonto();
		benutzer.setUuid(uuid);
		aktivierungsdaten.setBenutzerkonto(benutzer);
		aktivierungsdaten.setConfirmationCode("a65ef5-d23");

		final IAnonymisierungsservice anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);

		// Act
		final ConfirmationStatus actual = strategy.applyOnRegistration(aktivierungsdaten, jahr, kontakt, callback,
			anonymisierungsservice);

		// Assert
		assertEquals(ConfirmationStatus.normalActivation, actual);

		final TeilnahmeIdentifierProvider teilnahme = kontakt.get().getTeilnahmeZuJahr(jahr);
		assertNotNull(teilnahme);
		Mockito.verify(callback, times(1)).persist(kontakt.get());
		Mockito.verify(registrierungService, times(1)).activateBenutzer(aktivierungsdaten);
		Mockito.verify(anonymisierungsservice, times(0)).kontoAnonymsieren(uuid, kontakt, IConfirmationStrategy.CONTEXT_MESSAGE);
	}
}
