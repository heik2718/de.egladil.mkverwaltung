//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.common.config.OsUtils;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;
import de.egladil.mkv.persistence.payload.request.SchulzuordnungAendernPayload;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.benutzer.LehrerkontoDelegate;
import de.egladil.mkv.service.benutzer.PasswortDelegate;
import de.egladil.mkv.service.benutzer.PrivatkontoDelegate;
import de.egladil.mkv.service.benutzer.SessionDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * MitgliederResourceTest
 */
public class MitgliederResourceTest {

	private static final String KUERZEL_NEU = "HZT65G7K";

	private static final String KUERZEL_ALT = "1Z63652H";

	private static final String KUERZEL_TEILNAHME = "HZTF54DT";

	private static final String UUID = "UUID";

	private static final String BEARER = "jakshdh-dfhw";

	private Benutzerkonto benutzerkonto;

	private int anzCheckCalled = 0;

	private int anzahlProtokollCalled = 0;

	private MitgliederResource resource;

	private PasswortDelegate passwortDelegate;

	private BenutzerkontoDelegate benutzerkontoDelegate;

	private IAuthenticationService authenticationService;

	private IBenutzerService benutzerService;

	private LehrerkontoDelegate lehrerkontoDelegate;

	private IAccessTokenDAO accessTokenDAO;

	private ISchulteilnahmeDao schulteilnahmeDao;

	private BenutzerPayloadMapper benutzerPayloadMapper;

	private final PasswortAendernPayload passwortPayload = TestUtils.validPasswortAendernPayload();

	private final MailadresseAendernPayload mailPayload = TestUtils.validMailadresseAendernPayload();

	private SchulzuordnungAendernPayload schulePayload;

	private UsernamePasswordToken usernamePasswordToken;

	private final AccessToken accessToken = new AccessToken(BEARER);

	private String aktuellesWettbewerbsjahr;

	@BeforeEach
	void setUp() {
		anzCheckCalled = 0;
		anzahlProtokollCalled = 0;
		final IRegistrierungService registrierungService = Mockito.mock(IRegistrierungService.class);
		authenticationService = Mockito.mock(IAuthenticationService.class);
		benutzerService = Mockito.mock(IBenutzerService.class);
		final MailDelegate mailDelegate = Mockito.mock(MailDelegate.class);

		final IProtokollService protokollService = new IProtokollService() {

			private final Ereignis ereignis = new Ereignis(Ereignisart.EMAIL_GEAENDERT.getKuerzel(), UUID, "ndsjkhah");

			@Override
			public Ereignis protokollieren(final Ereignis ereignis) {
				anzahlProtokollCalled++;
				return ereignis;
			}

			@Override
			public Ereignis protokollieren(final String ereignisart, final String wer, final String was) {
				anzahlProtokollCalled++;
				return ereignis;
			}

			@Override
			public List<Ereignis> findByEreignisart(final String ereignisart) {
				return null;
			}
		};

		final IAnonymisierungsservice anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);
		lehrerkontoDelegate = Mockito.mock(LehrerkontoDelegate.class);
		final PrivatkontoDelegate privatkontoDelegate = Mockito.mock(PrivatkontoDelegate.class);

		benutzerkontoDelegate = new BenutzerkontoDelegate(registrierungService, authenticationService, benutzerService,
			mailDelegate, protokollService, anonymisierungsservice);

		benutzerPayloadMapper = Mockito.mock(BenutzerPayloadMapper.class);
		accessTokenDAO = Mockito.mock(IAccessTokenDAO.class);
		schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);

		final SessionDelegate sessionDelegate = Mockito.mock(SessionDelegate.class);

		passwortDelegate = Mockito.mock(PasswortDelegate.class);
		resource = new MitgliederResource(benutzerkontoDelegate, lehrerkontoDelegate, privatkontoDelegate, benutzerPayloadMapper,
			anonymisierungsservice, accessTokenDAO, schulteilnahmeDao, sessionDelegate, passwortDelegate);

		benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(UUID);
		usernamePasswordToken = new UsernamePasswordToken(mailPayload.getUsername(), mailPayload.getPassword().toCharArray());

		schulePayload = new SchulzuordnungAendernPayload();
		schulePayload.setAnmelden(false);
		schulePayload.setKuerzelAlt(KUERZEL_ALT);
		schulePayload.setKuerzelNeu(KUERZEL_NEU);

		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		aktuellesWettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	@Nested
	@DisplayName("Tests für Passwort ändern")
	class PasswortAendern {

		@Test
		@DisplayName("response 400 (bad request) wenn principal null")
		void principalNull() {
			// Act
			final Response response = resource.passwortAendern(null, passwortPayload, BEARER);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("response 400 (bad request) wenn bearer null")
		void bearerNull() {
			// Act
			final Response response = resource.passwortAendern(new EgladilPrincipal(UUID), passwortPayload, null);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("response 400 (bad request) wenn payload null")
		void payloadNull() {
			// Act
			final Response response = resource.passwortAendern(new EgladilPrincipal(UUID), null, BEARER);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit UnknownAccountException")
		void unknownAccount() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new UnknownAccountException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit IncorrectCredentialsException")
		void incorrectCredentials() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new IncorrectCredentialsException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Das alte Passwort ist nicht korrekt.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit ExcessiveAttemptsException")
		void excessiveAttempts() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new ExcessiveAttemptsException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit EgladilAuthenticationException")
		void egladilAuthenticationException() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new EgladilAuthenticationException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit DisabledAccountException")
		void disabledAccount() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new DisabledAccountException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals("Sie haben Ihr Benutzerkonto noch nicht aktiviert oder es wurde deaktiviert.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit EgladilDuplicateEntryException")
		void duplicateEntry() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new EgladilDuplicateEntryException("diese exception darf an dieser Stelle nicht auftreten"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(500, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Es ist ein interner Serverfehler aufgetreten.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("passwortAendern mit EgladilConcurrentModificationException")
		void concurrentModification() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(passwortDelegate.authentisierenUndPasswortAendern(passwortPayload, UUID))
				.thenThrow(new EgladilConcurrentModificationException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.passwortAendern(principal, passwortPayload, BEARER);

			// Assert
			assertEquals(409, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Ihr Benutzerprofil wurde in der Zwischenzeit geändert. Falls Sie dies selbst mit anderen Browser waren, loggen Sie sich hier bitte aus. Wenn nicht, senden Sie bitte eine Mail an minikaenguru@egladil.de.",
				pm.getApiMessage().getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für Email ändern")
	class EmailAendern {
		@Test
		@DisplayName("response 400 (bad request) wenn principal null")
		void principalNull() {
			// Act
			final Response response = resource.emailAendern(null, mailPayload, BEARER);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("response 400 (bad request) wenn bearer null")
		void bearerNull() {
			// Act
			final Response response = resource.emailAendern(new EgladilPrincipal(UUID), mailPayload, null);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("response 400 (bad request) wenn payload null")
		void payloadNull() {
			// Act
			final Response response = resource.emailAendern(new EgladilPrincipal(UUID), null, BEARER);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit UnknownAccountException")
		void unknownAccount() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new UnknownAccountException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit IncorrectCredentialsException")
		void incorrectCredentials() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new IncorrectCredentialsException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Ihre alte Mailadresse und das Passwort passen nicht zusammen.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit ExcessiveAttemptsException")
		void excessiveAttempts() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new ExcessiveAttemptsException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit EgladilAuthenticationException")
		void egladilAuthenticationException() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new EgladilAuthenticationException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit DisabledAccountException")
		void disabledAccount() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);
			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
				.thenThrow(new DisabledAccountException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals("Sie haben Ihr Benutzerkonto noch nicht aktiviert oder es wurde deaktiviert.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit EgladilDuplicateEntryException")
		void duplicateEntry() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			final EgladilDuplicateEntryException ex = new EgladilDuplicateEntryException("mailadresse gibt es schon");
			ex.setUniqueIndexName("uk_benutzer_1");

			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzerkonto);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzerkonto);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzerkonto)).thenThrow(ex);

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(900, response.getStatus());
			assertEquals(1, anzCheckCalled);
			assertEquals(0, anzahlProtokollCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals("Es gibt bereits ein Benutzerkonto mit dieser Mailadresse. Bitte wählen Sie eine andere.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("emailAendern mit EgladilConcurrentModificationException")
		void concurrentModification() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzerkonto);
			Mockito.when(benutzerService.findBenutzerkontoByUUID(UUID)).thenReturn(benutzerkonto);
			Mockito.when(benutzerService.persistBenutzerkonto(benutzerkonto))
				.thenThrow(new EgladilConcurrentModificationException("jkag"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.emailAendern(principal, mailPayload, BEARER);

			// Assert
			assertEquals(409, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Ihr Benutzerprofil wurde in der Zwischenzeit geändert. Falls Sie dies selbst mit anderen Browser waren, loggen Sie sich hier bitte aus. Wenn nicht, senden Sie bitte eine Mail an minikaenguru@egladil.de.",
				pm.getApiMessage().getMessage());
		}
	}

	@Nested
	@DisplayName("Tests für schulzuordnungAendern")
	class SchulzuordnungAendern {
		@Test
		@DisplayName("response 400 (bad request) wenn principal null")
		void principalNull() {
			// Act
			final Response response = resource.schulzuordnungAendern(null, schulePayload);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("response 400 (bad request) wenn payload null")
		void payloadNull() {
			// Act
			final Response response = resource.schulzuordnungAendern(new EgladilPrincipal(UUID), null);

			// Assert
			assertEquals(400, response.getStatus());
			assertEquals(0, anzCheckCalled);
			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern keine Session")
		void noAccessToken() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.empty());

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(408, response.getStatus());
			assertEquals(0, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals("Sie waren zu lange inaktiv. Ihre Session wurde beendet. Bitte loggen Sie sich erneut ein.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern kein Benutzerkonto gefunden")
		void keinBenutzerkonto() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(null);
			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(500, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Es ist ein interner Serverfehler aufgetreten.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern UnknownAccountException")
		void keinLehrerkonto() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);
			Mockito.when(lehrerkontoDelegate.changeSchule(schulePayload, benutzerkonto, aktuellesWettbewerbsjahr))
				.thenThrow(new UnknownAccountException("agwiowe ew og"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Es ist ein interner Serverfehler aufgetreten.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern DisabledAccountException")
		void disabledAccount() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			// Löst DisabledAccountException aus
			benutzerkonto.setGesperrt(true);
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(401, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals("Sie haben Ihr Benutzerkonto noch nicht aktiviert oder es wurde deaktiviert.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern ResourceNotFoundException")
		void schuleNichtGefunden() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);
			Mockito.when(lehrerkontoDelegate.changeSchule(schulePayload, benutzerkonto, aktuellesWettbewerbsjahr))
				.thenThrow(new ResourceNotFoundException("agwiowe ew og"));

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(404, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("agwiowe ew og", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern ConcurrentModification beim ersten Speichern")
		void concurrentModification() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);
			final OptimisticLockException concEx = new OptimisticLockException("sigdi ui qi");
			final PersistenceException persistenceException = new PersistenceException(concEx);
			Mockito.when(lehrerkontoDelegate.changeSchule(schulePayload, benutzerkonto, aktuellesWettbewerbsjahr))
				.thenThrow(persistenceException);

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(409, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
			assertEquals(
				"Ihr Benutzerprofil wurde in der Zwischenzeit geändert. Falls Sie dies selbst mit anderen Browser waren, loggen Sie sich hier bitte aus. Wenn nicht, senden Sie bitte eine Mail an die Addresse im Impressum.",
				pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern unerwartete PersistenceException beim ersten Speichern")
		void persistenceException1() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);
			final PersistenceException persistenceException = new PersistenceException("irgendwas unerwartetes");
			Mockito.when(lehrerkontoDelegate.changeSchule(schulePayload, benutzerkonto, aktuellesWettbewerbsjahr))
				.thenThrow(persistenceException);

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(500, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
			assertEquals("Es ist ein interner Serverfehler aufgetreten.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern ConcurrentModificationException beim zweiten Speichern")
		void concurrentModifictionDelete() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);
			final Lehrerkonto lehrerkonto = new Lehrerkonto();
			Mockito.when(lehrerkontoDelegate.changeSchule(schulePayload, benutzerkonto, aktuellesWettbewerbsjahr))
				.thenReturn(lehrerkonto);

			final OptimisticLockException concEx = new OptimisticLockException("sigdi ui qi");
			final PersistenceException persistenceException = new PersistenceException(concEx);

			final Schulteilnahme schulteilnahme = new Schulteilnahme();
			schulteilnahme.setJahr(aktuellesWettbewerbsjahr);
			schulteilnahme.setKuerzel(KUERZEL_TEILNAHME);
			Mockito.when(schulteilnahmeDao.findOrphan(KUERZEL_ALT, aktuellesWettbewerbsjahr))
				.thenReturn(Optional.of(schulteilnahme));

			Mockito.when(schulteilnahmeDao.delete(schulteilnahme)).thenThrow(persistenceException);

			final MKVBenutzer angemeldeterMKVBenutzer = new MKVBenutzer();

			final ErweiterteKontodaten erweiterteDaten = new ErweiterteKontodaten();
			erweiterteDaten.setSchule(new PublicSchule("vshfd", KUERZEL_NEU));
			angemeldeterMKVBenutzer.setErweiterteKontodaten(erweiterteDaten);

			final PersonBasisdaten basisdaten = new PersonBasisdaten();
			basisdaten.setRolle("MKV_LEHRER");
			angemeldeterMKVBenutzer.setBasisdaten(basisdaten);

			Mockito.when(benutzerPayloadMapper.createBenutzer(benutzerkonto, false)).thenReturn(angemeldeterMKVBenutzer);

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(200, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.INFO.toString(), pm.getApiMessage().getLevel());
			assertEquals("Der Schulwechsel war erfolgreich. Ihre neue Schule ist die vshfd.", pm.getApiMessage().getMessage());
		}

		@Test
		@DisplayName("schulzuordnungAendern unerwartete PersistenceException beim zweiten Speichern")
		void anderePersistenceExceptionDelete() {
			final ValidationDelegate validationDelegate = new ValidationDelegate() {

				@Override
				public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
					anzCheckCalled++;
					super.check(payload, clazz);
				}

			};
			resource.setValidationDelegate(validationDelegate);

			Mockito.when(accessTokenDAO.findAccessTokenByPrimaryPrincipal(UUID)).thenReturn(Optional.of(accessToken));
			Mockito.when(benutzerkontoDelegate.getBenutzerkonto(UUID)).thenReturn(benutzerkonto);
			final Lehrerkonto lehrerkonto = new Lehrerkonto();
			Mockito.when(lehrerkontoDelegate.changeSchule(schulePayload, benutzerkonto, aktuellesWettbewerbsjahr))
				.thenReturn(lehrerkonto);

			final PersistenceException persistenceException = new PersistenceException("shdgiqg");

			final Schulteilnahme schulteilnahme = new Schulteilnahme();
			schulteilnahme.setJahr(aktuellesWettbewerbsjahr);
			schulteilnahme.setKuerzel(KUERZEL_TEILNAHME);
			Mockito.when(schulteilnahmeDao.findOrphan(KUERZEL_ALT, aktuellesWettbewerbsjahr))
				.thenReturn(Optional.of(schulteilnahme));

			Mockito.when(schulteilnahmeDao.delete(schulteilnahme)).thenThrow(persistenceException);

			final MKVBenutzer angemeldeterMKVBenutzer = new MKVBenutzer();
			final ErweiterteKontodaten erweiterteKontodaten = new ErweiterteKontodaten();
			erweiterteKontodaten.setSchule(new PublicSchule("vshfd", KUERZEL_NEU));
			angemeldeterMKVBenutzer.setErweiterteKontodaten(erweiterteKontodaten);

			Mockito.when(benutzerPayloadMapper.createBenutzer(benutzerkonto, false)).thenReturn(angemeldeterMKVBenutzer);

			final Principal principal = new EgladilPrincipal(UUID);

			// Act
			final Response response = resource.schulzuordnungAendern(principal, schulePayload);

			// Assert
			assertEquals(200, response.getStatus());
			assertEquals(1, anzCheckCalled);

			final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
			assertEquals(MessageLevel.INFO.toString(), pm.getApiMessage().getLevel());
			assertEquals("Der Schulwechsel war erfolgreich. Ihre neue Schule ist die vshfd.", pm.getApiMessage().getMessage());
		}
	}
}
