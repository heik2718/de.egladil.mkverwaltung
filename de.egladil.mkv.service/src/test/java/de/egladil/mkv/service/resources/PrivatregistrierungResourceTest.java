//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;

import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.confirmation.IPrivatConfirmationService;
import de.egladil.mkv.service.confirmation.impl.ConfirmationStatus;
import de.egladil.mkv.service.payload.request.Privatregistrierung;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * RegisterSchuleResourceTest
 */
public class PrivatregistrierungResourceTest {

	private static final String WETTBEWERBSJAHR = "2008";

	private static final String CONFIRMATIONCODE = "bd6df95a-a4c9-4ec0-bf5e-7d898cc923d8";

	private IPrivatRegistrierungService privatRegistrierungService;

	private Privatregistrierung anfrage;

	private IPrivatConfirmationService confirmationService;

	private PrivatregistrierungResource resource;

	@BeforeEach
	public void setUp() {
		privatRegistrierungService = Mockito.mock(IPrivatRegistrierungService.class);
		confirmationService = Mockito.mock(IPrivatConfirmationService.class);
		anfrage = TestUtils.validPrivatregistrierung();
		resource = new PrivatregistrierungResource(privatRegistrierungService, confirmationService);
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			resource.setDownloadPath("D:\\projekte\\priv\\download");
		} else {
			resource.setDownloadPath("/home/heike/Downloads/mkv");
		}
		TestUtils.initMKVApiKontextReader(WETTBEWERBSJAHR, true);
	}

	@Test
	public void register_returns_bad_request_when_anfrageEntity_null() {
		// Act
		final Response response = resource.register(null);
		assertEquals(400, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.", pm.getApiMessage().getMessage());
	}

	@Test
	public void register_calls_validationDelegate_check() {
		// Arrange
		final ValidationDelegate validationDelegate = Mockito.mock(ValidationDelegate.class);
		resource.setValidationDelegate(validationDelegate);

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setConfirmationCode(CONFIRMATIONCODE);
		aktivierungsdaten.setExpirationTime(new Date());

		Mockito.when(privatRegistrierungService.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, WETTBEWERBSJAHR, false))
			.thenReturn(aktivierungsdaten);

		// Act
		final Response response = resource.register(anfrage);

		// Assert
		Mockito.verify(validationDelegate, times(1)).check(anfrage, Privatregistrierung.class);
		assertEquals(201, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), pm.getApiMessage().getLevel());
		assertEquals("Ihr Benutzerkonto wurde erfolgreich angelegt und eine Mail wurde an Ihre Email-Adresse versendet. Bitte schauen Sie in Ihrem Postfach nach, um die Registrierung abzuschließen. Sie sind nach der Aktivierung zum Wettbewerb 2008 angemeldet.", pm.getApiMessage().getMessage());
	}

	@Test
	public void register_returns_forbidden_when_kleber_nicht_blank() {
		// Arrange
		anfrage.setKleber(" v  ");

		// Act
		final Response response = resource.register(anfrage);
		assertEquals(403, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		assertEquals("Diese Anfrage ist nicht erlaubt.", pm.getApiMessage().getMessage());
	}

	@Test
	public void confirm_returns_expired_when_expiredActivation() throws Exception {
		Mockito.when(confirmationService.jetztRegistrierungBestaetigen(CONFIRMATIONCODE, WETTBEWERBSJAHR))
			.thenReturn(ConfirmationStatus.expiredActivation);

		// Act
		final StringWriter w = new StringWriter();
		IOUtils.copy(getClass().getResourceAsStream("/html/activationExpiredTest.html"), w);
		final String expected = w.toString().trim();

		// Act
		final InputStream response = resource.confirm(CONFIRMATIONCODE);

		// Assert
		assertNotNull(response);
		final StringWriter sw = new StringWriter();
		IOUtils.copy(response, sw);
		final String actual = sw.toString().trim();

		assertEquals(expected, actual);

	}

	@Test
	public void confirm_returns_success_html_when_normalActivation() throws Exception {
		Mockito.when(confirmationService.jetztRegistrierungBestaetigen(CONFIRMATIONCODE, WETTBEWERBSJAHR))
			.thenReturn(ConfirmationStatus.normalActivation);

		// Act
		final StringWriter w = new StringWriter();
		IOUtils.copy(getClass().getResourceAsStream("/html/activationSuccessTest.html"), w);
		final String expected = w.toString().trim();

		// Act
		final InputStream response = resource.confirm(CONFIRMATIONCODE);

		// Assert
		assertNotNull(response);
		final StringWriter sw = new StringWriter();
		IOUtils.copy(response, sw);
		final String actual = sw.toString().trim();

		assertEquals(expected, actual);
	}

	@Test
	public void confirm_returns_success_html_when_repeatedActivation() throws Exception {
		Mockito.when(confirmationService.jetztRegistrierungBestaetigen(CONFIRMATIONCODE, WETTBEWERBSJAHR))
			.thenReturn(ConfirmationStatus.repeatedActivation);

		// Act
		final StringWriter w = new StringWriter();
		IOUtils.copy(getClass().getResourceAsStream("/html/activationSuccessTest.html"), w);
		final String expected = w.toString().trim();
		System.out.println("expected (/html/activationSuccessTest.html)");
		System.out.println(expected);

		// Act
		final InputStream response = resource.confirm(CONFIRMATIONCODE);

		// Assert
		assertNotNull(response);
		final StringWriter sw = new StringWriter();
		IOUtils.copy(response, sw);
		final String actual = sw.toString().trim();
		System.out.println("actual");
		System.out.println(actual);

		assertEquals(expected, actual);
	}

	@Test
	public void confirm_returns_serverError_when_confirmationStatus_null() throws Exception {
		Mockito.when(confirmationService.jetztRegistrierungBestaetigen(CONFIRMATIONCODE, WETTBEWERBSJAHR)).thenReturn(null);

		// Act
		final StringWriter w = new StringWriter();
		IOUtils.copy(getClass().getResourceAsStream("/html/activationFailed.html"), w);
		final String expected = w.toString();

		// Act
		final InputStream response = resource.confirm(CONFIRMATIONCODE);

		// Assert
		assertNotNull(response);
		final StringWriter sw = new StringWriter();
		IOUtils.copy(response, sw);
		final String actual = sw.toString();

		assertEquals(expected, actual);
	}

	@Test
	public void register_returns_duplicate_entry_when_egladilDuplicateEntry_caught() throws ParseException {
		// Arrange
		final EgladilDuplicateEntryException egladilDuplicateEntryException = new EgladilDuplicateEntryException(
			"Ein Benutzerkonto mit dieser Mailadresse gibt es schon");
		egladilDuplicateEntryException.setUniqueIndexName("uk_benutzer_1");
		Mockito.when(privatRegistrierungService.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, "2008", true))
			.thenThrow(egladilDuplicateEntryException);

		TestUtils.initMKVApiKontextReader("2008", true);

		// Act
		final Response response = resource.register(anfrage);
		assertEquals(900, response.getStatus());
		final Object entity = response.getEntity();
		assertTrue(entity instanceof APIResponsePayload);
		final APIResponsePayload pem = (APIResponsePayload) entity;
		assertEquals(MessageLevel.WARN.toString(), pem.getApiMessage().getLevel());
		assertEquals("Dieses Benutzerkonto gibt es schon.", pem.getApiMessage().getMessage());
	}
}
