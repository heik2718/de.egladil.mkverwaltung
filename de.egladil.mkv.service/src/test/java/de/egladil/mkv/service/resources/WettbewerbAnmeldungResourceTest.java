//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.security.Principal;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.benutzer.SessionDelegate;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * WettbewerbAnmeldungResourceTest
 */
public class WettbewerbAnmeldungResourceTest {

	private static final String JAHR = "2018";

	private static final String BEN_UUID = "hashfhw";

	private WettbewerbAnmeldungResource resource;

	private IWettbewerbsanmeldungService wettbewerbsanmeldungService;

	private SessionDelegate sessionDelegate;

	private TeilnehmerFacade teilnehmerFacade;

	private Principal principal;

	@BeforeEach
	public void setUp() {
		wettbewerbsanmeldungService = Mockito.mock(IWettbewerbsanmeldungService.class);
		sessionDelegate = Mockito.mock(SessionDelegate.class);
		teilnehmerFacade = Mockito.mock(TeilnehmerFacade.class);
		resource = new WettbewerbAnmeldungResource(wettbewerbsanmeldungService, sessionDelegate);
		TestUtils.initMKVApiKontextReader(JAHR, true);
		principal = new EgladilPrincipal(BEN_UUID);
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_409_concModification_zurueck_bei_EgladilConcurrentModificationException() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), JAHR, false);

		Mockito.when(wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, BEN_UUID, Role.MKV_LEHRER))
			.thenThrow(new EgladilConcurrentModificationException("jemand anderes"));

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(409, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Ihr Benutzerprofil wurde in der Zwischenzeit geändert. Falls Sie dies selbst mit anderen Browser waren, loggen Sie sich hier bitte aus. Wenn nicht, senden Sie bitte eine Mail an die Addresse im Impressum.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_902_duplicate_zurueck_bei_EgladilDuplicateEntryException() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), JAHR, false);

		final EgladilDuplicateEntryException ex = new EgladilDuplicateEntryException("gibts schon");
		ex.setUniqueIndexName("uk_schulteilnahmen_1");
		Mockito.when(wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, BEN_UUID, Role.MKV_LEHRER)).thenThrow(ex);

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(900, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Schule ist in diesem Jahr bereits angemeldet", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_412_precondition_zurueck_bei_wettbewerb_zu() {
		// Arrange
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), JAHR, false);
		TestUtils.initMKVApiKontextReader(JAHR, false);

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(412, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Eine Anmeldung zum Wettbewerb 2018 ist noch nicht möglich.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_902_bad_request_wenn_payload_invalid() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung("HALLO", JAHR, false);

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(902, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
		assertTrue(apiResponse.getPayload() instanceof ConstraintViolationMessage);
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_500_wenn_jahr_nicht_korrekt() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), "2019", false);

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
		assertNull(apiResponse.getPayload());
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_500_bei_EgladilStorageException() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), JAHR, false);

		Mockito.when(wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, BEN_UUID, Role.MKV_LEHRER))
			.thenThrow(new EgladilStorageException("speichern fehlgeschlagen"));

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
		assertNull(apiResponse.getPayload());
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_500_bei_MKVException() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), JAHR, false);

		Mockito.when(wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, BEN_UUID, Role.MKV_LEHRER))
			.thenThrow(new MKVException("irgendwas internes"));

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
		assertNull(apiResponse.getPayload());
	}

	@Test
	public void zumWettbewerbAnmelden_gibt_500_bei_IllegalArgumentException() {
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), JAHR, false);

		Mockito.when(wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, BEN_UUID, Role.MKV_LEHRER))
			.thenThrow(new IllegalArgumentException("benutzerService null"));

		// Act
		final Response response = resource.zumWettbewerbAnmelden(principal, anmeldung);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
		assertNull(apiResponse.getPayload());
	}
}
