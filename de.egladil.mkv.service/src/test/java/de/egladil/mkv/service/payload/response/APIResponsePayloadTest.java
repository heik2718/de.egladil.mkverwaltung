//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.response;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.mkv.persistence.payload.response.Kontext;

/**
 * APIResponsePayloadTest
 */
public class APIResponsePayloadTest {

	@Test
	public void testJson() throws Exception {
		// Arrange
		final Kontext kontext = new Kontext();
		kontext.setAuthToken("sfzfaizf");
		final APIResponsePayload response = new APIResponsePayload(APIMessage.info("alles gut"), kontext);

		// Act
		final String json = new ObjectMapper().writeValueAsString(response);
		System.out.println(json);

	}

}
