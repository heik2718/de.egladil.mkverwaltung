//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.bv.aas.auth.EmailBasedCredentials;
import de.egladil.common.config.OsUtils;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.service.AbstractGuiceIT;

/**
 * SessionResourceIT
 */
public class SessionResourceIT extends AbstractGuiceIT {

	private static final String TEMP_CSRF_TOKEN = "TEMPTOKEN";

	private static final Logger LOG = LoggerFactory.getLogger(SessionResourceIT.class);

	@Inject
	private SessionResource sessionResource;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
	}

	@Test
	public void login_username_unbekannt() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);

		// Act
		final Response response = sessionResource.login(credentials, TEMP_CSRF_TOKEN);

		// Assert
		assertNotNull(response);
		assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
		final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
		final String message = payload.getApiMessage().getMessage();
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", message);
		assertNull(payload.getPayload());
	}

	@Test
	public void login_passwort_unbekannt() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("vader@egladil.de", "g3he1mxxx", null);

		// Act
		final Response response = sessionResource.login(credentials, TEMP_CSRF_TOKEN);

		// Assert
		assertNotNull(response);
		assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
		final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
		final String message = payload.getApiMessage().getMessage();
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.", message);
		assertNull(payload.getPayload());
	}

	@Test
	public void login_lehrer_klappt() throws Exception {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("vader@egladil.de", "start123", null);

		// Act
		final Response response = sessionResource.login(credentials, TEMP_CSRF_TOKEN);

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
		assertNotNull(payload.getPayload());
		assertTrue(payload.getPayload() instanceof MKVBenutzer);
	}

	@Test
	public void login_privat_klappt() throws Exception {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("meister@egladil.de", "start123", null);

		// Act
		final Response response = sessionResource.login(credentials, TEMP_CSRF_TOKEN);

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
		assertNotNull(payload.getPayload());
		assertTrue(payload.getPayload() instanceof MKVBenutzer);

		System.out.println(new ObjectMapper().writeValueAsString(payload.getPayload()));
	}

	@Test
	public void getKontext_klappt() throws Exception {
		// Act
		final Response response = sessionResource.getKontext();

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload payload = (APIResponsePayload) response.getEntity();
		assertNotNull(payload.getPayload());
		assertTrue(payload.getPayload() instanceof Kontext);

		final ObjectMapper objectMapper = new ObjectMapper();
		LOG.info(objectMapper.writeValueAsString(payload.getPayload()));
	}

}
