//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.mail.MailDelegateStub;
import de.egladil.mkv.service.mail.MailserviceStub;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;

/**
 * LehrerRegistrierungServiceImplTest
 */
public class LehrerRegistrierungServiceImplTest {

	private static final String JAHR = "2017";

	private static final String BENUTZR_UUID = "adgsahl-haudg5";

	private static final Logger LOG = LoggerFactory.getLogger(LehrerRegistrierungServiceImplTest.class);

	private ISchuleDao schuleDao;

	private IBenutzerService benutzerService;

	private MailDelegate mailDelegate;

	private ILehrerkontoDao lehrerkontoDao;

	private ISchulteilnahmeDao schulteilnahmeDao;

	private BenutzerkontoDelegate benutzerkontoDelegate;

	private IAnonymisierungsservice anonymisierungsservice;

	private ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private IProtokollService protokollService;

	private LehrerRegistrierungServiceImpl service;

	private MailserviceStub mailService;

	private Benutzerkonto benutzer;

	@BeforeEach
	public void setUp() {
		benutzerService = Mockito.mock(IBenutzerService.class);
		mailService = new MailserviceStub();

		final IEgladilConfiguration mailConfig = Mockito.mock(IEgladilConfiguration.class);
		Mockito.when(mailConfig.getProperty("mail.mailserver.vorhanden")).thenReturn("true");
		Mockito.when(mailConfig.getProperty("confirm.registrierung.lehrer.url")).thenReturn("http://localhost:8080");
		Mockito.when(mailConfig.getProperty("mail.registrierung.subject")).thenReturn("Irgendein Bla");

		mailDelegate = new MailDelegateStub(mailService, mailConfig);
		benutzerkontoDelegate = Mockito.mock(BenutzerkontoDelegate.class);
		schuleDao = Mockito.mock(ISchuleDao.class);
		schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		lehrerteilnahmeInfoDao = Mockito.mock(ILehrerteilnahmeInfoDao.class);
		anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);
		protokollService = Mockito.mock(IProtokollService.class);

		service = new LehrerRegistrierungServiceImpl(benutzerService, mailDelegate, benutzerkontoDelegate, schuleDao,
			schulteilnahmeDao, lehrerkontoDao, lehrerteilnahmeInfoDao, anonymisierungsservice, protokollService,
			Mockito.mock(IAccessTokenDAO.class), Mockito.mock(AuswertungsgruppenService.class));

		benutzer = new Benutzerkonto();
		benutzer.setAktiviert(true);
		benutzer.setAnwendung(Anwendung.MKV);
		benutzer.setEmail("mail@provider.de");
		benutzer.setId(7l);
		benutzer.setLoginName(benutzer.getEmail());
		benutzer.setUuid(BENUTZR_UUID);

	}

	@Test
	public void mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden_schulteilnahme_persist_fehler() {
		// Arrange
		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.empty());
		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setJahr(JAHR);
		schulteilnahme.setKuerzel(payload.getSchulkuerzel());

		final Schule schule = new Schule();
		schule.setId(17l);
		schule.setKuerzel(payload.getSchulkuerzel());
		payload.setSchule(schule);

		Mockito.when(schulteilnahmeDao.persist(schulteilnahme)).thenThrow(new PersistenceException("Mockexception"));
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setBenutzerkonto(benutzer);

		final Ereignis ereignis1 = new Ereignis(Ereignisart.ANONYMISIERT.getKuerzel(), BENUTZR_UUID,
			"Persistieren Schulteilnahme und Lehrerkonto");
		Mockito.when(protokollService.protokollieren(ereignis1)).thenReturn(ereignis1);

		final Ereignis ereignis2 = new Ereignis(Ereignisart.SCHULTEILNAHME_GELOESCHT.getKuerzel(), "Schulkuerzel = " + payload.getSchulkuerzel(),
			"Persistieren Schulteilnahme und Lehrerkonto");
		Mockito.when(protokollService.protokollieren(ereignis2)).thenReturn(ereignis2);

		// Act
		try {
			service.mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(payload, aktivierungsdaten, JAHR, true);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			LOG.info(e.getMessage());
			Mockito.verify(anonymisierungsservice, Mockito.times(1)).kontoAnonymsieren(BENUTZR_UUID,
				"Persistieren Schulteilnahme und Lehrerkonto");
		}
	}

	@Test
	public void mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden_lehrerkonto_persist_fehler() {
		// Arrange
		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.empty());
		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setId(123l);
		schulteilnahme.setJahr(JAHR);
		schulteilnahme.setKuerzel(payload.getSchulkuerzel());

		final Schule schule = new Schule();
		schule.setId(17l);
		schule.setKuerzel(payload.getSchulkuerzel());
		payload.setSchule(schule);

		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.of(schulteilnahme));
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setBenutzerkonto(benutzer);

		final Lehrerkonto kontakt = new Lehrerkonto();
		kontakt.setUuid(BENUTZR_UUID);

		Mockito.when(lehrerkontoDao.persist(kontakt)).thenThrow(new PersistenceException());
		Mockito.when(lehrerteilnahmeInfoDao.findAlleMitSchulkuerzel(payload.getSchulkuerzel())).thenReturn(new ArrayList<>());

		final Ereignis ereignis = new Ereignis(Ereignisart.SCHULTEILNAHME_GELOESCHT.getKuerzel(), "Schulkuerzel = " + payload.getSchulkuerzel(),
			"Persistieren Lehrerkonto gescheitert");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		try {
			service.mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(payload, aktivierungsdaten, JAHR, true);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			LOG.info(e.getMessage());
			Mockito.verify(anonymisierungsservice, Mockito.times(1)).kontoAnonymsieren(BENUTZR_UUID,
				"Persistieren Schulteilnahme und Lehrerkonto");
			Mockito.verify(schulteilnahmeDao, Mockito.times(1)).delete(schulteilnahme);
			Mockito.verify(protokollService, Mockito.times(1)).protokollieren(ereignis);
		}
	}

	@Test
	public void mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden_aufraeumen_exception_benutzer() {
		// Arrange

		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier
			.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR);
		Mockito.when(schulteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.empty());
		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setId(123l);
		schulteilnahme.setJahr(JAHR);
		schulteilnahme.setKuerzel(payload.getSchulkuerzel());

		final Schule schule = new Schule();
		schule.setId(17l);
		schule.setKuerzel(payload.getSchulkuerzel());
		payload.setSchule(schule);

		Mockito.when(schulteilnahmeDao.findByTeilnahmeIdentifier(teilnahmeIdentifier)).thenReturn(Optional.of(schulteilnahme));
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setBenutzerkonto(benutzer);

		final Lehrerkonto kontakt = new Lehrerkonto();
		kontakt.setUuid(BENUTZR_UUID);

		Mockito.when(anonymisierungsservice.kontoAnonymsieren(BENUTZR_UUID, "Persistieren Schulteilnahme und Lehrerkonto"))
			.thenThrow(new RuntimeException());
		Mockito.when(lehrerkontoDao.persist(kontakt)).thenThrow(new PersistenceException());
		Mockito.when(lehrerteilnahmeInfoDao.findAlleMitSchulkuerzel(payload.getSchulkuerzel())).thenReturn(new ArrayList<>());

		final Ereignis ereignis = new Ereignis(Ereignisart.SCHULTEILNAHME_GELOESCHT.getKuerzel(), "Schulkuerzel = " + payload.getSchulkuerzel(),
			"Persistieren Lehrerkonto gescheitert");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		try {
			service.mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(payload, aktivierungsdaten, JAHR, true);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			LOG.info(e.getMessage());
			Mockito.verify(anonymisierungsservice, Mockito.times(1)).kontoAnonymsieren(BENUTZR_UUID,
				"Persistieren Schulteilnahme und Lehrerkonto");
			Mockito.verify(schulteilnahmeDao, Mockito.times(1)).delete(schulteilnahme);
			Mockito.verify(protokollService, Mockito.times(1)).protokollieren(ereignis);
		}
	}

	@Test
	public void mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden_aufraeumen_exception_schulteilnahme() {
		// Arrange
		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.empty());
		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setId(123l);
		schulteilnahme.setJahr(JAHR);
		schulteilnahme.setKuerzel(payload.getSchulkuerzel());

		final Schule schule = new Schule();
		schule.setId(17l);
		schule.setKuerzel(payload.getSchulkuerzel());
		payload.setSchule(schule);

		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.of(schulteilnahme));
		Mockito.when(schulteilnahmeDao.delete(schulteilnahme)).thenThrow(new RuntimeException("soll ins LOG"));

		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setBenutzerkonto(benutzer);

		final Lehrerkonto kontakt = new Lehrerkonto();
		kontakt.setUuid(BENUTZR_UUID);

		Mockito.when(lehrerkontoDao.persist(kontakt)).thenThrow(new PersistenceException());
		Mockito.when(lehrerteilnahmeInfoDao.findAlleMitSchulkuerzel(payload.getSchulkuerzel())).thenReturn(new ArrayList<>());

		final Ereignis ereignis = new Ereignis(Ereignisart.SCHULTEILNAHME_GELOESCHT.getKuerzel(), "Schulkuerzel = " + payload.getSchulkuerzel(),
			"Persistieren Lehrerkonto gescheitert");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		try {
			service.mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(payload, aktivierungsdaten, JAHR, true);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			LOG.info(e.getMessage());
			Mockito.verify(anonymisierungsservice, Mockito.times(1)).kontoAnonymsieren(BENUTZR_UUID,
				"Persistieren Schulteilnahme und Lehrerkonto");
			Mockito.verify(schulteilnahmeDao, Mockito.times(1)).delete(schulteilnahme);
			Mockito.verify(protokollService, Mockito.times(0)).protokollieren(ereignis);
		}
	}

	@Test
	public void mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden_aufraeumen_exception_mail() {
		// Arrange
		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.empty());
		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setId(123l);
		schulteilnahme.setJahr(JAHR);
		schulteilnahme.setKuerzel(payload.getSchulkuerzel());

		final Schule schule = new Schule();
		schule.setId(17l);
		schule.setKuerzel(payload.getSchulkuerzel());
		payload.setSchule(schule);

		Mockito
			.when(schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(payload.getSchulkuerzel(), JAHR)))
			.thenReturn(Optional.of(schulteilnahme));
		final Aktivierungsdaten aktivierungsdaten = new Aktivierungsdaten();
		aktivierungsdaten.setBenutzerkonto(benutzer);
		aktivierungsdaten.setExpirationTime(new Date());

		final Lehrerkonto kontakt = new Lehrerkonto();
		kontakt.setUuid(BENUTZR_UUID);

		Mockito.when(lehrerkontoDao.persist(kontakt)).thenReturn(kontakt);
		Mockito.when(lehrerteilnahmeInfoDao.findAlleMitSchulkuerzel(payload.getSchulkuerzel())).thenReturn(new ArrayList<>());

		final Ereignis ereignis = new Ereignis(Ereignisart.SCHULTEILNAHME_GELOESCHT.getKuerzel(), "Schulkuerzel = " + payload.getSchulkuerzel(),
			"Persistieren Lehrerkonto gescheitert");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		mailService.setExceptionToThrow(new EgladilMailException("mail klappt nicht"));

		// Act
		try {
			service.mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(payload, aktivierungsdaten, JAHR, true);
			fail("keine EgladilMailException");
		} catch (final EgladilMailException e) {
			LOG.info(e.getMessage());
			assertTrue(mailService.isSendMailCalled());
			Mockito.verify(anonymisierungsservice, Mockito.times(1)).kontoAnonymsieren(BENUTZR_UUID,
				"Mailversand bei Registrierung");
			Mockito.verify(schulteilnahmeDao, Mockito.times(1)).delete(schulteilnahme);
			Mockito.verify(protokollService, Mockito.times(1)).protokollieren(ereignis);
		}
	}

	@Test
	public void createTransientLehrerkonto_erzeugt_vollstaendiges_lehrerkonto() {
		// Arrange
		final Lehrerregistrierung payload = TestUtils.validLehrerregistrierung();
		final Schulteilnahme schulteilnahme = new Schulteilnahme();
		schulteilnahme.setId(123l);
		schulteilnahme.setJahr(JAHR);
		schulteilnahme.setKuerzel(payload.getSchulkuerzel());

		final Schule schule = new Schule();
		schule.setId(17l);
		schule.setKuerzel(payload.getSchulkuerzel());
		payload.setSchule(schule);

		final Person person = payload.getKontaktdaten();

		// Act
		final Lehrerkonto lehrerkonto = service.createTransientLehrerkonto(BENUTZR_UUID, payload, schulteilnahme, schule);

		// Assert
		assertEquals(Role.MKV_LEHRER, lehrerkonto.getRole());
		assertEquals(schule, lehrerkonto.getSchule());
		assertEquals(schulteilnahme, lehrerkonto.getTeilnahmeZuJahr(JAHR));
		assertNull(lehrerkonto.getId());
		assertEquals(person, lehrerkonto.getPerson());
	}
}
