//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.mkv.service.TestUtils;

/**
 * SchulregistrierungJsonTest
 */
public class MKVRegistrierungJsonTest {

	private static final String LEHRERREGISTRIERUNG_DATA = "{\"type\":\"lehrer\",\"vorname\":\"Hannelore\",\"nachname\":\"Kuckuck\",\"email\":\"hannelore.kuckuck@egladil.de\",\"passwort\":\"Qwertz!2\",\"passwortWdh\":\"Qwertz!2\",\"kleber\":null,\"agbGelesen\":true,\"automatischBenachrichtigen\":true,\"gleichAnmelden\":true,\"schulkuerzel\":\"Z45FG9IM\"}";

	private static final String PRIVATREGISTRIERUNG_DATA = "{\"type\":\"privat\",\"vorname\":\"Jonny\",\"nachname\":\"Kuckuck\",\"email\":\"mail@provider.de\",\"passwort\":\"Qwertz!2\",\"passwortWdh\":\"Qwertz!2\",\"kleber\":null,\"agbGelesen\":true,\"automatischBenachrichtigen\":false,\"gleichAnmelden\":true}";

	private ObjectMapper mapper;

	@BeforeEach
	public void setUp() {
		mapper = new ObjectMapper();
	}

	@Test
	public void marshal_lehrerregistrierung_works() throws Exception {
		final Lehrerregistrierung lehrerregistrierung = TestUtils.validLehrerregistrierung();
		final String jsonDataString = mapper.writeValueAsString(lehrerregistrierung);
		assertEquals(LEHRERREGISTRIERUNG_DATA, jsonDataString);

	}

	@Test
	public void unmarshal_lehrerregistrierung_works() throws JsonParseException, JsonMappingException, IOException {
		final Lehrerregistrierung lehrerregistrierung = mapper.readValue(LEHRERREGISTRIERUNG_DATA, Lehrerregistrierung.class);
		assertEquals("Z45FG9IM", lehrerregistrierung.getSchulkuerzel());
	}

	@Test
	public void marshal_privatregistrierung_works() throws Exception {
		final Privatregistrierung registrierung = TestUtils.validPrivatregistrierung();
		registrierung.setAutomatischBenachrichtigen(false);
		final String jsonDataString = mapper.writeValueAsString(registrierung);
		assertEquals(PRIVATREGISTRIERUNG_DATA, jsonDataString);
	}

	@Test
	public void unmarshal_privatregistrierung_works() throws Exception {
		final Privatregistrierung privatregistrierung = mapper.readValue(PRIVATREGISTRIERUNG_DATA, Privatregistrierung.class);
		assertEquals("Jonny", privatregistrierung.getVorname());
		assertEquals("Kuckuck", privatregistrierung.getNachname());
		assertEquals("mail@provider.de", privatregistrierung.getEmail());
		assertEquals("Qwertz!2", privatregistrierung.getPasswort());
		assertEquals("Qwertz!2", privatregistrierung.getPasswortWdh());
		assertNull(privatregistrierung.getKleber());
		assertTrue(privatregistrierung.isAgbGelesen());
		assertFalse(privatregistrierung.isAutomatischBenachrichtigen());
	}

}
