//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.auth.EmailBasedCredentials;
import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.common.config.OsUtils;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.request.SchulzuordnungAendernPayload;
import de.egladil.mkv.persistence.payload.request.StatusPayload;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.service.AbstractGuiceIT;

/**
 * MitgliederResourceIT
 */
public class MitgliederResourceIT extends AbstractGuiceIT {

	private static final String BENUTZER_UUID = "72c8c630-47be-451f-b34c-826fbd6b967f";

	private static final String ROLLE = "MKV_LEHRER";

	private static final String EMAIL = "pwd1@egladil.de";

	private static final Logger LOG = LoggerFactory.getLogger(MitgliederResourceIT.class);

	private Principal principal;

	@Inject
	private MitgliederResource mitgliederResource;

	@Inject
	private ISchuleDao schuleDao;

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

	@Inject
	private SessionResource sessionResource;

	@Override
	@BeforeEach
	public void setUp() {
		super.setUp();
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
//		mitgliederResource.setDownloadPath("/home/heike/Download/mkv");
		principal = new EgladilPrincipal(BENUTZER_UUID);
	}

	@Test
	public void passwortAendern_klappt() throws Exception {
		final String bearer = "Bearer HGzugudqgi";
		{
			// Arrange 1
			final PasswortAendernPayload payload = new PasswortAendernPayload();
			payload.setPasswort("start123");
			payload.setPasswortNeu("start987");
			payload.setPasswortNeuWdh("start987");
			payload.setEmail(EMAIL);

			// Act 1
			final Response response = mitgliederResource.passwortAendern(principal, payload, bearer);

			// Assert 1
			assertNotNull(response);
			assertEquals(200, response.getStatus());
			final Object entity = response.getEntity();
			assertTrue(entity instanceof APIResponsePayload);
			final APIResponsePayload pm = (APIResponsePayload) entity;
			final String jsonString = new ObjectMapper().writeValueAsString(pm);
			LOG.info(jsonString);
		}
		{

			// Arrange 2
			final PasswortAendernPayload payload = new PasswortAendernPayload();
			payload.setEmail(EMAIL);
			payload.setPasswort("start987");
			payload.setPasswortNeu("start123");
			payload.setPasswortNeuWdh("start123");

			// Act 2
			final Response response = mitgliederResource.passwortAendern(principal, payload, bearer);

			// Assert 2
			assertNotNull(response);
			assertEquals(200, response.getStatus());
			final Object entity = response.getEntity();
			assertTrue(entity instanceof APIResponsePayload);
			final APIResponsePayload pm = (APIResponsePayload) entity;
			final String jsonString = new ObjectMapper().writeValueAsString(pm);
			LOG.info(jsonString);
		}
	}

	@Test
	public void nameAendern_klappt() {
		// Arrange
		final PersonAendernPayload payload = new PersonAendernPayload();
		payload.setNachname("Hö-" + new Random().nextInt());
		payload.setVorname("Heinz");
		payload.setUsername(EMAIL);
		payload.setRolle(ROLLE);

		// Act
		final Response response = mitgliederResource.nameAendern(principal, payload);

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Ihr Name wurde erfolgreich geändert.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void newsletterAendern_klappt() {
		// Arrange
		final StatusPayload payload = new StatusPayload();
		payload.setRolle(ROLLE);
		payload.setNeuerStatus(true);

		// Act
		final Response response = mitgliederResource.newsletterAendern(principal, payload);

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Sie haben die automatische Benachrichtigung erfolgreich aktiviert.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schulzuordnugAendern_klappt() {
		// Arrange
		final Response kontextResponse = sessionResource.getKontext();
		final APIResponsePayload sessionApiResponse = (APIResponsePayload) kontextResponse.getEntity();
		final Kontext kontext = (Kontext) sessionApiResponse.getPayload();
		final String tempCsrfToken = kontext.getXsrfToken();

		final EmailBasedCredentials cred = new EmailBasedCredentials(EMAIL, "start123", null);
		sessionResource.login(cred, tempCsrfToken);

		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(BENUTZER_UUID);
		assertTrue(opt.isPresent());

		final Lehrerkonto lehrerkonto = opt.get();
		final Schule alteSchule = lehrerkonto.getSchule();

		final List<Schule> alleSchulen = schuleDao.findAll(Schule.class);

		Schule neueSchule = null;
		for (final Schule schule : alleSchulen) {
			if (!schule.equals(alteSchule)) {
				neueSchule = schule;
				break;
			}
		}

		final SchulzuordnungAendernPayload payload = new SchulzuordnungAendernPayload();
		payload.setKuerzelAlt(alteSchule.getKuerzel());
		payload.setKuerzelNeu(neueSchule.getKuerzel());

		// Act
		final Response response = mitgliederResource.schulzuordnungAendern(principal, payload);

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertTrue(apiResponse.getApiMessage().getMessage()
			.startsWith("Der Schulwechsel war erfolgreich. Ihre neue Schule ist die Schule"));
	}

	@Test
	public void emailAendernKlappt() {
		fail("Test fehlt");
	}

}
