//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.bv.aas.auth.EmailBasedCredentials;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.exception.EgladilWebappException;

/**
 * ValidationDelegateCredentialsTest
 */
public class ValidationDelegateCredentialsTest {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationDelegateCredentialsTest.class);

	private ValidationDelegate validationDelegate;

	private ObjectMapper mapper;

	@BeforeEach
	public void setUp() {
		validationDelegate = new ValidationDelegate();
		mapper = new ObjectMapper();
	}

	@Test
	public void check_throws_902_constraintViolation_when_password_invalid() throws Exception {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("mail@provider.com", "start12", null);

		// Act + Assert
		try {
			validationDelegate.check(credentials, EmailBasedCredentials.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

	@Test
	public void check_throws_902_constraintViolation_when_email_invalid() throws Exception {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("mail-provider.com", "start123", null);

		// Act + Assert
		try {
			validationDelegate.check(credentials, EmailBasedCredentials.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

}
