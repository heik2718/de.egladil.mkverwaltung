//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.service.AbstractGuiceIT;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.Privatregistrierung;

/**
 * RegisterSchuleResourceIT
 */
public class PrivatregistrierungResourceIT extends AbstractGuiceIT {

	@Inject
	private PrivatregistrierungResource resource;

	private Privatregistrierung payload;

	@Inject
	private IBenutzerDao benutzerDao;

	@Inject
	private IPrivatkontoDao privatkontoDao;

	@Test
	public void register_klappt() {
		// Arrange
		payload = TestUtils.validRandomPrivatregistrierung();
		// payload.setEmail("unique-privatkontakt@egladil.de");
		TestUtils.initMKVApiKontextReader("2014", true);

		// Act
		final Response response = resource.register(payload);

		// Assert
		assertEquals(201, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Ihr Benutzerkonto wurde erfolgreich angelegt und eine Mail wurde an Ihre Email-Adresse versendet. Bitte schauen Sie in Ihrem Postfach nach, um die Registrierung abzuschließen. Sie sind nach der Aktivierung zum Wettbewerb 2014 angemeldet.", apiResponse.getApiMessage().getMessage());

		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName(payload.getEmail(), Anwendung.MKV);
		assertNotNull(benutzerkonto);
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());

		final Optional<Privatkonto> opt = privatkontoDao.findByUUID(benutzerkonto.getUuid());
		assertTrue(opt.isPresent());
		final Privatkonto privatkonto = opt.get();
		final TeilnahmeIdentifierProvider teilnahmeZuJahr = privatkonto.getTeilnahmeZuJahr("2014");
		assertNotNull(teilnahmeZuJahr);
		assertEquals(payload.isAutomatischBenachrichtigen(), privatkonto.isAutomatischBenachrichtigen());
	}

	@Test
	public void register_returns_900_duplicate_wenn_gleiche_person_bereits_vorhanden() {
		// Arrange
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail("unique-privatkontakt@egladil.de", Anwendung.MKV);
		assertNotNull("Test kann nicht laufen: kein passendes Benutzerkonto vorhanden", benutzerkonto);

		final Optional<Privatkonto> opt = privatkontoDao.findByUUID(benutzerkonto.getUuid());
		assertTrue(opt.isPresent());

		final Privatkonto privatkonto = opt.get();
		final Privatregistrierung privatregistrierung = new Privatregistrierung();
		privatregistrierung.setAgbGelesen(true);
		privatregistrierung.setEmail("unique-privatkontakt@egladil.de");
		privatregistrierung.setNachname(privatkonto.getPerson().getNachname());
		privatregistrierung.setVorname(privatkonto.getPerson().getVorname());
		privatregistrierung.setPasswort("Qwertz!2");
		privatregistrierung.setPasswortWdh("Qwertz!2");

		// Act
		final Response response = resource.register(privatregistrierung);

		// Assert
		assertEquals(900, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
		assertEquals("Dieses Benutzerkonto gibt es schon.", pm.getApiMessage().getMessage());
	}
}
