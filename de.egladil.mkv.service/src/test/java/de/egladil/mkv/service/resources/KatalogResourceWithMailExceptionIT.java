//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.config.MKVTestWithMailexceptionModule;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;

/**
 * KatalogResourceWithMailExceptionIT
 */
public class KatalogResourceWithMailExceptionIT {

	@Inject
	private KatalogResource resource;

	@BeforeEach
	public void setUp() {
		final Injector injector = Guice.createInjector(new MKVTestWithMailexceptionModule());
		injector.injectMembers(this);
	}

	@Test
	public void schuleintragBeantragen_returns_903_when_mailservice_fails() {
		// Arrange
		final String landkuerzel = "ABCDE";
		final SchuleUrlKatalogantrag katalogantrag = TestUtils.validSchuleUrlKatalogantrag();

		// Act
		final Response response = resource.schuleintragBeantragen(landkuerzel, katalogantrag);

		// Assert
		assertEquals(903, response.getStatus());
		assertTrue(response.getEntity() instanceof APIResponsePayload);

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Der Katalogantrag konnte wegen eines internen Fehlers nicht weitergeleitet werden. Bitte senden Sie eine Mail an minikaenguru@egladil.de.",
			apiResponse.getApiMessage().getMessage());
	}

}
