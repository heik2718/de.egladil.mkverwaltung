//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;

/**
 * @author heikew
 *
 */
public class ValidationDelegateLehrerregistrierungTest {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationDelegateLehrerregistrierungTest.class);

	private ValidationDelegate validationDelegate;

	private Lehrerregistrierung anfrage;

	private ObjectMapper mapper;

	@BeforeEach
	public void setUp() {
		validationDelegate = new ValidationDelegate();
		anfrage = TestUtils.validLehrerregistrierung();
		mapper = new ObjectMapper();
	}

	@Test
	public void check_throws_forbidden_when_kleber_invalid() {
		// Arrange
		anfrage.setKleber("h");

		// Act + Assert
		try {
			validationDelegate.check(anfrage, Lehrerregistrierung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}

	@Test
	public void check_throws_forbidden_when_kleber_leerer_string_laenger_als_null() {
		// Arrange
		anfrage.setKleber(" ");

		// Act + Assert
		try {
			validationDelegate.check(anfrage, Lehrerregistrierung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}

	@Test
	public void check_throws_902_constraintViolation_when_agb_nicht_gelesen() throws Exception {
		// Arrange
		anfrage.setAgbGelesen(false);

		// Act + Assert
		try {
			validationDelegate.check(anfrage, Lehrerregistrierung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

	@Test
	public void check_throws_902_constraintViolation_when_schulkuerzel_null() throws Exception {
		// Arrange
		anfrage.setSchulkuerzel(null);

		// Act + Assert
		try {
			validationDelegate.check(anfrage, Lehrerregistrierung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

	@Test
	public void check_throws_902_constraintViolation_when_schulkuerzel_leerzeichen() throws Exception {
		// Arrange
		anfrage.setSchulkuerzel(" ");

		// Act + Assert
		try {
			validationDelegate.check(anfrage, Lehrerregistrierung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

	@Test
	public void check_throws_902_constraintViolation_when_schulkuerzel_invalid() throws Exception {
		// Arrange
		anfrage.setSchulkuerzel("987654321");

		// Act + Assert
		try {
			validationDelegate.check(anfrage, Lehrerregistrierung.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}
}
