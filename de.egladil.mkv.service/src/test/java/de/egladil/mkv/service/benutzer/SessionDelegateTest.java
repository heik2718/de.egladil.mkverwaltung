//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;

import java.util.Optional;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.SessionToken;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.service.TestUtils;

/**
 * SessionDelegateTest
 */
public class SessionDelegateTest {

	private static final String AKTJAHR = "2017";

	private static final String EMAIL = "x@x.de";

	private static final String TEMP_PASSWORD = "hdget7Z5";

	private static final String BEARER = "hdget-7Z5ret";

	private static final String ACCESS_TOKEN_ID = "abe43-dd5e4-56";

	private static final String BENUTZER_UUID = "hsakhf-hafwoo412g1";

	private IAccessTokenDAO accessTokenDAO;

	private ILehrerkontoDao lehrerkontoDao;

	private IPrivatkontoDao privatkontoDao;

	private ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private SessionDelegate sessionDelegate;

	private UsernamePasswordToken usernamePasswordToken;

	private ISchulteilnahmeDao schulteilnahmeDao;

	private IKatalogService katalogService;

	private IProtokollService protokollService;

	private TeilnehmerFacade teilnehmerFacade;

	private TeilnahmenFacade teilnahmenFacade;

	private AdvFacade advFacade;

	@BeforeEach
	public void setUp() {
		TestUtils.initMKVApiKontextReader(AKTJAHR, true);

		accessTokenDAO = Mockito.mock(IAccessTokenDAO.class);
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);

		lehrerteilnahmeInfoDao = Mockito.mock(ILehrerteilnahmeInfoDao.class);
		protokollService = Mockito.mock(IProtokollService.class);
		schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		teilnehmerFacade = Mockito.mock(TeilnehmerFacade.class);
		teilnahmenFacade = Mockito.mock(TeilnahmenFacade.class);
		advFacade = Mockito.mock(AdvFacade.class);
		katalogService = Mockito.mock(IKatalogService.class);

		final BenutzerPayloadMapper benutzerPayloadMapper = new BenutzerPayloadMapper(Mockito.mock(IBenutzerService.class),
			lehrerkontoDao, privatkontoDao, lehrerteilnahmeInfoDao, teilnehmerFacade, advFacade, katalogService, teilnahmenFacade);
		// benutzerPayloadMapper.setDownloadPath(TestUtils.getDownloadPath());
		sessionDelegate = new SessionDelegate(accessTokenDAO, lehrerkontoDao, privatkontoDao, benutzerPayloadMapper,
			protokollService);

		usernamePasswordToken = new UsernamePasswordToken(EMAIL, TEMP_PASSWORD.toCharArray());
	}

	// @Nested
	// @DisplayName("Testen authenticate")
	// class AuthenticateTests {
	// @Test
	// @DisplayName("authenticate propaiert IllegalArgumentException")
	// public void illegalArgumentException_weiter() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new IllegalArgumentException("irgendwas"));
	//
	// final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	//
	// }
	//
	// @Test
	// @DisplayName("authenticate propaiert EgladilAuthenticationException")
	// public void egladilAuthenticationException() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new EgladilAuthenticationException("irgendwas"));
	//
	// final Throwable ex = assertThrows(EgladilAuthenticationException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	// }
	//
	// @Test
	// @DisplayName("authenticate propaiert UnknownAccountException")
	// public void unknownAccount() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new UnknownAccountException("irgendwas"));
	//
	// final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	// }
	//
	// @Test
	// @DisplayName("authenticate propaiert IncorrectCredentialsException")
	// public void incorrectCredentials() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new IncorrectCredentialsException("irgendwas"));
	//
	// final Throwable ex = assertThrows(IncorrectCredentialsException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	// }
	//
	// @Test
	// @DisplayName("authenticate propaiert ExcessiveAttemptsException")
	// public void excessiveAttempts() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new ExcessiveAttemptsException("irgendwas"));
	//
	// final Throwable ex = assertThrows(ExcessiveAttemptsException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	// }
	//
	// @Test
	// @DisplayName("authenticate propaiert DisabledAccountException")
	// public void disabledAccount() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new DisabledAccountException("irgendwas"));
	//
	// final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	// }
	//
	// @Test
	// @DisplayName("authenticate propaiert EgladilStorageException")
	// public void egladilStorageException() {
	// // Arrange
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new EgladilStorageException("irgendwas"));
	//
	// final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// });
	// assertEquals("irgendwas", ex.getMessage());
	// }
	//
	// @Test
	// @DisplayName("authenticate invalidiert das CSRF-Token")
	// public void invalidateTemporaryCsrfTokenQuietly() {
	// // Arrange irgenwie will Mockito hier nicht mitspielen :/
	// final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
	// final Benutzerkonto benutzerkonto = new Benutzerkonto();
	// benutzerkonto.setUuid(BENUTZER_UUID);
	// benutzerkonto.setId(435l);
	//
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzerkonto);
	// Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
	// Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
	//
	// final AccessToken accessToken = new AccessToken(BENUTZER_UUID);
	// accessToken.setCsrfToken(TEMP_PASSWORD);
	// Mockito.when(accessTokenDAO.generateNewAccessToken(benutzerkonto)).thenReturn(accessToken);
	// Mockito.when(accessTokenDAO.invalidateTemporaryCsrfToken(TEMP_PASSWORD)).thenReturn("");
	// Mockito.when(advFacade.findAdvVereinbarungFuerSchule(lehrerkonto.getSchule().getKuerzel()))
	// .thenReturn(Optional.empty());
	//
	// // Act
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	//
	// // Assert
	// Mockito.verify(accessTokenDAO, times(1)).invalidateTemporaryCsrfToken(TEMP_PASSWORD);
	// }
	//
	// @Test
	// @DisplayName("authenticate invalidiert das CSRF-Token nur falls keine Exception")
	// public void invalidateTemporaryCsrfTokenMitException() {
	// Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
	// .thenThrow(new EgladilStorageException("irgendwas"));
	//
	// Mockito.when(accessTokenDAO.invalidateTemporaryCsrfToken(TEMP_PASSWORD)).thenReturn("");
	//
	// // Act
	// try {
	// sessionDelegate.authenticateAndCreateSession(usernamePasswordToken, TEMP_PASSWORD);
	// } catch (final Exception e) {
	// // Assert
	// Mockito.verify(accessTokenDAO, times(0)).invalidateTemporaryCsrfToken(TEMP_PASSWORD);
	// }
	// }
	// }

	@Nested
	@DisplayName("Tests für substituteSession")
	class SubstituteSessionTests {
		@Test
		public void substituteSession_ruft_invalidateSessionQuietly() {
			final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);
			benutzerkonto.setId(45l);

			Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
			Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);

			final AccessToken accessToken = new AccessToken(ACCESS_TOKEN_ID, BENUTZER_UUID);
			accessToken.setCsrfToken(TEMP_PASSWORD);
			accessToken.setPrimaryPrincipal(BENUTZER_UUID);
			Mockito.when(accessTokenDAO.generateNewAccessToken(benutzerkonto)).thenReturn(accessToken);
			Mockito.when(accessTokenDAO.invalidateAccessToken(accessToken.getAccessTokenId())).thenReturn("");
			Mockito.when(advFacade.findAdvVereinbarungFuerSchule(lehrerkonto.getSchule().getKuerzel()))
				.thenReturn(Optional.empty());

			// Act
			final SessionToken sessionToken = sessionDelegate.substituteSession(benutzerkonto, BEARER);

			// Assert
			Mockito.verify(lehrerkontoDao, times(0)).persist(lehrerkonto);
			Mockito.verify(accessTokenDAO, times(1)).generateNewAccessToken(benutzerkonto);
			Mockito.verify(accessTokenDAO, times(1)).invalidateAccessToken(BEARER);
			assertFalse(BEARER.equals(sessionToken.getAccessToken()));
		}

		@Test
		public void substituteSession_ruft_invalidateSessionQuietly_nur_falls_keine_exception_1() {
			final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);

			final AccessToken accessToken = new AccessToken(ACCESS_TOKEN_ID, BENUTZER_UUID);
			accessToken.setCsrfToken(TEMP_PASSWORD);
			accessToken.setPrimaryPrincipal(BENUTZER_UUID);

			Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
			Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);

			Mockito.when(accessTokenDAO.generateNewAccessToken(benutzerkonto)).thenThrow(new NullPointerException());
			Mockito.when(accessTokenDAO.invalidateAccessToken(accessToken.getAccessTokenId())).thenReturn("");

			// Act
			try {
				sessionDelegate.substituteSession(benutzerkonto, BEARER);
			} catch (final Exception e) {

				// Assert
				Mockito.verify(accessTokenDAO, times(1)).generateNewAccessToken(benutzerkonto);
				Mockito.verify(accessTokenDAO, times(0)).invalidateAccessToken(BEARER);
			}
		}

		@Test
		public void substituteSession_ruft_invalidateSessionQuietly_nur_falls_keine_exception_2() {
			final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
			final Benutzerkonto benutzerkonto = new Benutzerkonto();
			benutzerkonto.setUuid(BENUTZER_UUID);

			final AccessToken accessToken = new AccessToken(ACCESS_TOKEN_ID, BENUTZER_UUID);
			accessToken.setCsrfToken(TEMP_PASSWORD);
			accessToken.setPrimaryPrincipal(BENUTZER_UUID);

			Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenThrow(new UnknownAccountException("kennen wir nicht"));

			Mockito.when(accessTokenDAO.generateNewAccessToken(benutzerkonto)).thenReturn(accessToken);
			Mockito.when(accessTokenDAO.invalidateAccessToken(accessToken.getAccessTokenId())).thenReturn("");

			// Act
			try {
				sessionDelegate.substituteSession(benutzerkonto, BEARER);
			} catch (final Exception e) {

				// Assert
				Mockito.verify(lehrerkontoDao, times(0)).persist(lehrerkonto);
				Mockito.verify(accessTokenDAO, times(0)).generateNewAccessToken(benutzerkonto);
				Mockito.verify(accessTokenDAO, times(0)).invalidateAccessToken(BEARER);
			}
		}
	}

	@Test
	public void logoutQuietly_ruft_invalidateSessionQuietly() {
		// Arrange irgenwie will Mockito hier nicht mitspielen :/
		final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);

		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);
		Mockito.when(accessTokenDAO.invalidateAccessToken(TEMP_PASSWORD)).thenReturn("");

		// Act
		sessionDelegate.logoutQuietly(BENUTZER_UUID, TEMP_PASSWORD);

		// Assert
		Mockito.verify(accessTokenDAO, times(1)).invalidateAccessToken(TEMP_PASSWORD);
	}

	@Nested
	@DisplayName("Tests für invalidateSessionQuietly")
	class InvalidateSessionTests {
		@Test
		@DisplayName("invalidateSessionQuietly fängt und loggt RuntimeException")
		public void invalidateSessionQuietly() {
			// Arrange
			Mockito.when(accessTokenDAO.invalidateAccessToken(TEMP_PASSWORD)).thenThrow(new NullPointerException("hähä"));
			try {
				sessionDelegate.invalidateSessionQuietly(TEMP_PASSWORD);
			} catch (final RuntimeException e) {
				fail("hierher nich");
			}
		}

		@Test
		@DisplayName("invalidateTemporaryCsrfTokenQuietly fängt und loggt RuntimeException")
		public void invalidateTemporaryCsrfTokenQuietly() {
			// Arrange
			Mockito.when(accessTokenDAO.invalidateTemporaryCsrfToken(TEMP_PASSWORD)).thenThrow(new NullPointerException("hähä"));
			try {
				sessionDelegate.invalidateTemporaryCsrfTokenQuietly(TEMP_PASSWORD);
			} catch (final RuntimeException e) {
				fail("hierher nich");
			}
		}
	}

	@Nested
	@DisplayName("Tests für logoutQuietly")
	class LogoutTests {
		@Test
		@DisplayName("logoutQuietly fängt RuntimeException, invalidiert accessToken, aner protokolliert nicht")
		public void logoutQuietly_faengt_runtimeException_beim_finden() {

			final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
			Mockito.when(accessTokenDAO.invalidateAccessToken(TEMP_PASSWORD)).thenReturn("");
			Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenThrow(new PersistenceException("nich gefunden"));
			Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenReturn(lehrerkonto);

			try {
				sessionDelegate.logoutQuietly(BENUTZER_UUID, TEMP_PASSWORD);
				Mockito.verify(accessTokenDAO, times(1)).invalidateAccessToken(TEMP_PASSWORD);
				Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LOGOUT.getKuerzel(), BENUTZER_UUID,
					Role.MKV_LEHRER.toString());
			} catch (final RuntimeException e) {
				fail("hierher nich");
			}
		}

		@Test
		@DisplayName("logoutQuietly fängt RuntimeException und invalidiert accessToken, speichert nicht, aber protokolliert")
		public void logoutQuietlyKontaktNichtGefunden() {

			final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
			Mockito.when(accessTokenDAO.invalidateAccessToken(TEMP_PASSWORD)).thenReturn("");
			Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());
			Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());

			try {
				sessionDelegate.logoutQuietly(BENUTZER_UUID, TEMP_PASSWORD);
				Mockito.verify(accessTokenDAO, times(1)).invalidateAccessToken(TEMP_PASSWORD);
				Mockito.verify(lehrerkontoDao, times(0)).persist(lehrerkonto);
				Mockito.verify(protokollService, times(1)).protokollieren(Ereignisart.LOGOUT.getKuerzel(), BENUTZER_UUID,
					"Kontakt wurde nicht gefunden");
			} catch (final RuntimeException e) {
				fail("hierher nich");
			}
		}

		@Test
		@DisplayName("logoutQuietly fängt PersistenceException beim Speichern, invalidiert accessToken und protokolliert")
		public void logoutQuietlyPersistenceExceptionBeimSpeichern() {

			final Lehrerkonto lehrerkonto = TestUtils.validActiveLehrerkonto(BENUTZER_UUID, AKTJAHR);
			Mockito.when(accessTokenDAO.invalidateAccessToken(TEMP_PASSWORD)).thenReturn("");
			Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
			Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenThrow(new PersistenceException("ging nich"));

			try {
				sessionDelegate.logoutQuietly(BENUTZER_UUID, TEMP_PASSWORD);
				Mockito.verify(accessTokenDAO, times(1)).invalidateAccessToken(TEMP_PASSWORD);
				Mockito.verify(protokollService, times(1)).protokollieren(Ereignisart.LOGOUT.getKuerzel(), BENUTZER_UUID,
					Role.MKV_LEHRER.toString());
			} catch (final RuntimeException e) {
				fail("hierher nich");
			}
		}
	}
}
