//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import java.util.Date;

import org.junit.jupiter.api.Test;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.OsUtils;
import de.egladil.email.service.impl.MailserviceImpl;
import de.egladil.mkv.service.config.EgladilMKVConfiguration;
import de.egladil.mkv.service.mail.MailDelegate;

/**
 * MailDelegateIT
 */
public class MailDelegateIT {

	private static final String CONFIG_ROOT = OsUtils.getDevConfigRoot();

	@Test
	public void schulregistrierungsmailVersenden_klappt() {
		// Arrange
		final EgladilMKVConfiguration configuration = new EgladilMKVConfiguration(CONFIG_ROOT);
		final MailDelegate mailDelegate = new MailDelegate(new MailserviceImpl(CONFIG_ROOT), configuration);
		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setConfirmationCode("7cb4b115-71dc-45b7-8b8e-4f79ca72a680");
		regBest.setExpirationTime(new Date());

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setEmail("heike@egladil.de");
		regBest.setBenutzerkonto(benutzerkonto);

		// Act + Assert (= keine Exception)
		mailDelegate.schulregistrierungsmailVersenden(regBest, "2017", "/mailtemplates/registrierung_ohne_anmeldung.txt");
	}

	@Test
	public void privatregistrierungsmailVersenden_klappt() {
		// Arrange
		final EgladilMKVConfiguration configuration = new EgladilMKVConfiguration(CONFIG_ROOT);
		final MailDelegate mailDelegate = new MailDelegate(new MailserviceImpl(CONFIG_ROOT), configuration);
		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setConfirmationCode("7cb4b115-71dc-45b7-8b8e-4f79ca72a680");
		regBest.setExpirationTime(new Date());

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setEmail("heike@egladil.de");
		regBest.setBenutzerkonto(benutzerkonto);

		// Act + Assert (= keine Exception)
		mailDelegate.privatregistrierungsmailVersenden(regBest, "2017", "/mailtemplates/registrierung_anmeldung_nicht_freigeschaltet.txt");
	}

}
