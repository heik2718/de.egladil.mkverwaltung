//=====================================================
// Projekt: de.egladil.common.validation
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.response;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.validation.json.InvalidProperty;

/**
 * ConstraintViolationMessageTest
 */
public class ConstraintViolationMessageTest {

	@Test
	public void testJson() throws Exception {
		// Arrange
		final ConstraintViolationMessage cvm = new ConstraintViolationMessage();
		cvm.setCrossValidationMessage("feld1 und feld2 passen nicht zusammen");
		cvm.setInvalidProperties(Arrays
			.asList(new InvalidProperty[] { new InvalidProperty("feld1", "message1"), new InvalidProperty("feld2", "message2") }));

		// Act
		final String json = new ObjectMapper().writeValueAsString(cvm);

		// Assert
		System.out.println(json);

	}

}
