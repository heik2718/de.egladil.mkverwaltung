//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.service.TestUtils;

/**
 * ValidationDelegatePasswortAendernTest
 */
public class ValidationDelegatePasswortAendernTest {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationDelegatePasswortAendernTest.class);

	private ValidationDelegate validationDelegate;

	private PasswortAendernPayload payload;

	private ObjectMapper mapper;

	@BeforeEach
	public void setUp() {
		validationDelegate = new ValidationDelegate();
		payload = TestUtils.validPasswortAendernPayload();
		mapper = new ObjectMapper();
	}

	@Test
	public void check_succeeds_when_valid() {
		// Act = Assert
		validationDelegate.check(payload, PasswortAendernPayload.class);
	}

	@Test
	public void check_throws_forbidden_when_kleber_invalid() {
		// Arrange
		payload.setKleber("h");

		// Act + Assert
		try {
			validationDelegate.check(payload, PasswortAendernPayload.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}

	@Test
	public void check_throws_forbidden_when_kleber_leerer_string_laenger_als_null() {
		// Arrange
		payload.setKleber(" ");

		// Act + Assert
		try {
			validationDelegate.check(payload, PasswortAendernPayload.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}

	@Test
	public void check_throws_902_constraintViolation_when_payload_invalid() throws Exception {
		// Arrange
		payload.setEmail("jshuet");
		payload.setPasswort("Huui");
		payload.setPasswortNeu("start234");
		payload.setPasswortNeuWdh("start345");

		// Act + Assert
		try {
			validationDelegate.check(payload, PasswortAendernPayload.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}
}
