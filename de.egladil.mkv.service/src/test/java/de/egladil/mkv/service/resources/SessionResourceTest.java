//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.auth.EmailBasedCredentials;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.common.config.OsUtils;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.service.benutzer.SessionDelegate;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * SessionResourceTest
 */
public class SessionResourceTest {

	private static final String BEARER = "Bearer fde435-dd5e67";

	private static final String TEMP_TOKEN = "asguidgqiu";

	private static final String BENUTZER_UUID = "gshjgdfiu-huweuo76";

	private SessionResource sessionResource;

	private SessionDelegate sessionDelegate;

	private IAccessTokenDAO accessTokenDAO;

	private IAuthenticationService authenticationService;

	private int anzCheckCalled;

	@BeforeEach
	public void setUp() {
		sessionDelegate = Mockito.mock(SessionDelegate.class);
		accessTokenDAO = Mockito.mock(IAccessTokenDAO.class);
		authenticationService = Mockito.mock(IAuthenticationService.class);
		sessionResource = new SessionResource(sessionDelegate, accessTokenDAO, authenticationService);
		anzCheckCalled = 0;

		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
	}

	@Test
	public void loginCreadentialsNull() {
		// Act
		final Response response = sessionResource.login(null, TEMP_TOKEN);

		// Assert
		assertEquals(400, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten: der Request ist fehlerhaft und kann nicht verarbeitet werden.",
			apiResponse.getApiMessage().getMessage());
		assertNull(apiResponse.getPayload());
	}

	@Test
	public void loginMitEgladilAuthenticationException() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(),
			credentials.getPassword().toCharArray());
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new EgladilAuthenticationException("gesperrt"));

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void loginMitIncorrectCredentialsException() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(),
			credentials.getPassword().toCharArray());
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new IncorrectCredentialsException("email oder passwort"));

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void loginMitUnknownAccountException() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(),
			credentials.getPassword().toCharArray());
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new UnknownAccountException("email unbekannt"));

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void loginMitExcessiveAttemptsException() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(),
			credentials.getPassword().toCharArray());
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new ExcessiveAttemptsException("zu häufig"));

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Das hat leider nicht geklappt. Falsche Email-Passwort-Kombination.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void loginMitDisabledAccountException() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(),
			credentials.getPassword().toCharArray());
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new DisabledAccountException("deaktiviert"));

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Sie haben Ihr Benutzerkonto noch nicht aktiviert oder es wurde deaktiviert.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void loginMitBeliebigerRuntimeException() {
		// Arrange
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "start123", null);
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(),
			credentials.getPassword().toCharArray());
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new RuntimeException("irgendwas unbekanntes"));

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void loginRuftValidateGenauEinmalAuf() {
		// Arrange
		final ValidationDelegate validationDelegate = new ValidationDelegate() {

			@Override
			public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
				anzCheckCalled++;
				super.check(payload, clazz);
			}

		};
		final EmailBasedCredentials credentials = new EmailBasedCredentials("kurti@egladil.de", "starten!!", null);
		sessionResource.setValidationDelegate(validationDelegate);

		// Act
		final Response response = sessionResource.login(credentials, TEMP_TOKEN);

		// Assert
		assertEquals(1, anzCheckCalled);
		assertEquals(902, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void getKontextMitRuntimeException() {
		// Arrange
		Mockito.when(authenticationService.createCsrfToken())
			.thenThrow(new RuntimeException("unerwartet beim Erzeugen eines csrf-Tokens"));

		// Act
		final Response response = sessionResource.getKontext();

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertNull(apiResponse.getPayload());
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void logoutMitRuntimeExceptionVorCreateKontext() {
		// Arrange
		Mockito.when(sessionDelegate.logoutQuietly(BENUTZER_UUID, BEARER))
			.thenThrow(new RuntimeException("unerwartet beim Ausloggen"));

		// Act
		final Response response = sessionResource.logout(new EgladilPrincipal(BENUTZER_UUID), BEARER);

		// Assert
		assertEquals(200, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertNotNull(apiResponse.getPayload());
		assertTrue(apiResponse.getPayload() instanceof Kontext);
	}

	@Test
	public void logoutMitRuntimeExceptionInCreateKontext() {
		// Arrange
		Mockito.when(authenticationService.createCsrfToken())
			.thenThrow(new RuntimeException("unerwartet beim Erzeugen einer anyonymen Session nach dem Logout."));

		// Act
		final Response response = sessionResource.logout(new EgladilPrincipal(BENUTZER_UUID), BEARER);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", apiResponse.getApiMessage().getMessage());
		assertNull(apiResponse.getPayload());
	}

	@Test
	public void createAnonymousSessionKlappt() throws Exception {
		// Arrange
		Mockito.when(authenticationService.createCsrfToken()).thenReturn(TEMP_TOKEN);

		// Act
		final APIResponsePayload apiResponse = sessionResource.createAnonymousSession();

		// Assert
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("", apiResponse.getApiMessage().getMessage());
		assertTrue(apiResponse.getPayload() instanceof Kontext);
	}

	@Test
	public void logoutTauschtCsrfTokenAus() throws Exception {
		// Arrange
		Mockito.when(authenticationService.createCsrfToken()).thenReturn(TEMP_TOKEN);

		// Act
		sessionResource.createAnonymousSession();

		// Assert
		Mockito.verify(authenticationService, times(1)).createCsrfToken();
	}
}
