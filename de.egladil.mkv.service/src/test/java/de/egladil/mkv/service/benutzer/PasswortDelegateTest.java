//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.payload.TempPwdAendernPayload;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.service.TestUtils;

/**
 * PasswortDelegateTest
 */
public class PasswortDelegateTest {

	private static final String DUMMY_PASSWORD = "DummyP4ssw0R1";

	private PasswortDelegate passwortDelegate;

	private BenutzerkontoDelegate benutzerkontoDelegate;

	private IAuthenticationService authenticationService;

	@BeforeEach
	public void setUp() {
		authenticationService = Mockito.mock(IAuthenticationService.class);
		benutzerkontoDelegate = Mockito.mock(BenutzerkontoDelegate.class);
		final IProtokollService protokollService = Mockito.mock(IProtokollService.class);
		passwortDelegate = new PasswortDelegate(authenticationService, benutzerkontoDelegate, protokollService);
	}

	@Nested
	@DisplayName("Tests für authentisierenUndPasswortAendern Einmalpasswor")
	class TempPasswortAendern {
		@Test
		@DisplayName("EgladilWebapplicationException bei dummypasswort")
		public void ummypassword() {
			// Arrange
			final String dummypassword = DUMMY_PASSWORD;
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			payload.setPasswort(dummypassword);

			// Act + Assert
			try {
				passwortDelegate.authentisierenUndPasswortAendern(payload, dummypassword);
				fail("keine EgladilWebappException");
			} catch (final EgladilWebappException e) {
				final Response response = e.getResponse();
				assertEquals(401, response.getStatus());
			}
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt UnknownAccountException weiter")
		public void unknownAccount() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new UnknownAccountException("den gibt es nicht"));

			// Act + Assert
			final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("den gibt es nicht", ex.getMessage());

		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt IncorrectCredentialsException weiter")
		public void incorrectCredentials() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new IncorrectCredentialsException("den gibt es nicht"));

			// Act + Assert
			final Throwable ex = assertThrows(IncorrectCredentialsException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("den gibt es nicht", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt DisabledAccountException weiter")
		public void disabledAccount() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new DisabledAccountException("gesperrt"));

			// Act + Assert
			final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("gesperrt", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt UnknownAccountException weiter")
		public void excessiveAttempts() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new ExcessiveAttemptsException("zu schnell"));

			// Act + Assert
			final Throwable ex = assertThrows(ExcessiveAttemptsException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("zu schnell", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt EgladilAuthenticationException weiter")
		public void egladilAuthenticationException() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new EgladilAuthenticationException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilAuthenticationException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt IllegalArgumentException weiter")
		public void illegalArgument() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new IllegalArgumentException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt EgladilStorageException weiter")
		public void egladilStorageException() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new EgladilStorageException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt EgladilBVException weiter")
		public void egladilBVException() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new EgladilBVException("irgendwas"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilBVException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("irgendwas", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt UnknownAccountException weiter")
		public void duplicateEntry() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new EgladilDuplicateEntryException("den hamwer schon"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilDuplicateEntryException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("den hamwer schon", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt UnknownAccountException weiter")
		public void concurrentModification() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new EgladilConcurrentModificationException("wer anders war schneller"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilConcurrentModificationException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("wer anders war schneller", ex.getMessage());
		}

		@Test
		@DisplayName("authentisierenUndPasswortAendern TempPasswort gibt EgladilMailException weiter")
		public void mailException() {
			// Arrange
			final TempPwdAendernPayload payload = TestUtils.validTempPwdAendernPayload();
			Mockito.when(
				benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV, payload.getEmail(), payload.getPasswort()))
				.thenThrow(new EgladilMailException("mailserver :/"));

			// Act + Assert
			final Throwable ex = assertThrows(EgladilMailException.class, () -> {
				passwortDelegate.authentisierenUndPasswortAendern(payload, DUMMY_PASSWORD);
			});
			assertEquals("mailserver :/", ex.getMessage());
		}

	}
}
