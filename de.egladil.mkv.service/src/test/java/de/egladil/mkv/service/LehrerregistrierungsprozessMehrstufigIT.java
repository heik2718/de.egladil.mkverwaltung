//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.confirmation.impl.LehrerConfirmationServiceImpl;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.registrierung.impl.LehrerRegistrierungServiceImpl;

/**
 * LehrerregistrierungsprozessMehrstufigIT
 */
public class LehrerregistrierungsprozessMehrstufigIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(LehrerregistrierungsprozessMehrstufigIT.class);

	@Inject
	private ISchuleDao schuleDao;

	@Inject
	private IRegistrierungService registrierungService;

	@Inject
	private IBenutzerService benutzerService;

	@Inject
	private MailDelegate mailDelegate;

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

	@Inject
	private IBenutzerDao benutzerDao;

	@Inject
	private ISchulteilnahmeDao schulteilnahmeDao;

	@Inject
	private BenutzerkontoDelegate benutzerkontoDelegate;

	@Inject
	private IAnonymisierungsservice anonymisierungsservice;

	private LehrerRegistrierungServiceImpl lehreranmeldungService;

	private LehrerConfirmationServiceImpl lehrerConfirmationService;

	@Inject
	private ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	@Inject
	private AuswertungsgruppenService auswertungsgruppenService;

	@Inject
	private IAccessTokenDAO accessTokenDAO;

	@Inject
	private IProtokollService protokollService;

	private String benutzerUUID;

	private String aktuellerConfirmationCode;

	@Test
	public void registrieren_aktivieren_anmelden_anmeldungAktivieren_mailMock() {
		lehreranmeldungService = new LehrerRegistrierungServiceImpl(benutzerService, mailDelegate, benutzerkontoDelegate, schuleDao,
			schulteilnahmeDao, lehrerkontoDao, lehrerteilnahmeInfoDao, anonymisierungsservice, protokollService, accessTokenDAO,
			auswertungsgruppenService);

		lehrerConfirmationService = new LehrerConfirmationServiceImpl(registrierungService, benutzerService, anonymisierungsservice,
			lehrerkontoDao, schulteilnahmeDao);

		final String kuerzel2008 = "ZYOP42TB";
		final String jahr = "2008";

		final long millies = System.currentTimeMillis();

		final Lehrerregistrierung anfrage = new Lehrerregistrierung("Rundum", "Schlag-" + millies,
			"rundum.schlag" + millies + "@egladil.de", "Qwertz!2", "Qwertz!2", null);
		anfrage.setAgbGelesen(true);
		anfrage.setSchulkuerzel(kuerzel2008);

		final Optional<Schule> opt = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			anfrage.getSchulkuerzel());
		assertTrue("Testsetting: Fehler keine Schule mit kuerzel " + kuerzel2008, opt.isPresent());

		final Schule schule = opt.get();

		anfrage.setSchule(schule);
		Aktivierungsdaten aktivierungsdaten = null;

		// Act 1
		{
			aktivierungsdaten = lehreranmeldungService.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, jahr, false);
			aktuellerConfirmationCode = aktivierungsdaten.getConfirmationCode();

			// Assert
			final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
			assertNotNull(benutzerkonto);
			assertFalse(benutzerkonto.isAktiviert());
			assertFalse(benutzerkonto.isGesperrt());
			assertEquals(anfrage.getEmail(), benutzerkonto.getLoginName());
			benutzerUUID = benutzerkonto.getUuid();

			final Optional<Lehrerkonto> lk = lehrerkontoDao.findByUUID(benutzerkonto.getUuid());
			assertTrue(lk.isPresent());
			final Lehrerkonto lehrerkonto = lk.get();

			final Person kontaktdaten = lehrerkonto.getPerson();
			assertEquals(anfrage.getVorname(), kontaktdaten.getVorname());
			assertEquals(anfrage.getNachname(), kontaktdaten.getNachname());

			final TeilnahmeIdentifierProvider schulteilname = lehrerkonto.getTeilnahmeZuJahr(jahr);
			assertNull(schulteilname);
			assertTrue(lehrerkonto.isAutomatischBenachrichtigen());

			LOG.info("{} hat Benutzerkonto und Wettbewerbsanmeldung für {} - beides inaktiv", kontaktdaten, jahr);
		}

		// Act 2
		lehrerConfirmationService.jetztRegistrierungBestaetigen(aktuellerConfirmationCode, jahr);

		// Assert 2
		{
			final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
			assertTrue(benutzerkonto.isAktiviert());

			final Optional<Aktivierungsdaten> optAkt2 = benutzerkonto
				.findAktivierungsdatenByConfirmCode(aktivierungsdaten.getConfirmationCode());
			assertTrue(optAkt2.isPresent());
			final Aktivierungsdaten aktDaten = optAkt2.get();
			assertTrue(aktDaten.isConfirmed());

			final Optional<Lehrerkonto> lk = lehrerkontoDao.findByUUID(benutzerUUID);
			assertTrue(lk.isPresent());

			LOG.info("Benutzerkonto {} und Teilnahme {} sind aktiviert", benutzerkonto, jahr);
		}

		// FIXME hier Schule wechseln

		// {
		// Schule neueSchule = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
		// kuerzel2009).get();
		// Lehrerkonto lehrerkonto = lehrerkontoDao.findByUUID(benutzerUUID).get();
		// lehrerkonto.neueSchuleAnmelden(neueSchule, jahr, true);
		//
		// assertEquals(2, lehrerkonto.getSchulzuordnungen().size());
		//
		// Lehrerkonto lehrerkontoMit2AktivenSchulen = lehrerkontoDao.persist(lehrerkonto);
		//
		// assertEquals(2, lehrerkontoMit2AktivenSchulen.getAktiveSchulzuordnungen().size());
		//
		// jahr = "2009";
		// // Act 3
		// {
		// Lehreranmeldung anmeldung = new Lehreranmeldung();
		// anmeldung.setBenachrichtigen(true);
		// anmeldung.setPrincipalName(benutzerUUID);
		// anmeldung.setJahr(jahr);
		//
		// lehrerRegistrierungService.kontaktZumWettbewerbAnmelden(anmeldung);
		// Lehrerkonto schulkontakt = lehrerkontoDao.findByUUID(benutzerUUID).get();
		//
		// List<ITeilnahme> teilnahmen = schulkontakt.getTeilnahmenZuJahr(jahr);
		// assertEquals(2, teilnahmen.size());
		//
		// for (ITeilnahme teilnahme2009 : teilnahmen) {
		// assertNotNull(teilnahme2009);
		// MKTeilnahme teilnahmedaten2009 = teilnahme2009.getTeilnahmedaten();
		// assertTrue(teilnahmedaten2009.isAktiviert());
		// }
		//
		// Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByUUID(benutzerUUID);
		// assertTrue(benutzerkonto.isAktiviert());
		//
		// LOG.info("vorhandenes Lehrerkonto zu Wettbewerb {} angemeldet, Anmeldung aktiv", jahr);
		// }
		// {
		// Lehrerkonto konto = lehrerkontoDao.findByUUID(benutzerUUID).get();
		// List<LehrerSchulzuordnung> schulzuordungen = konto.getSchulzuordnungen();
		// assertEquals(2, schulzuordungen.size());
		//
		// LehrerSchulzuordnung inaktiveSchulzuordnung = schulzuordungen.get(0);
		// inaktiveSchulzuordnung.setAktiv(false);
		// }
		// }
	}
}
