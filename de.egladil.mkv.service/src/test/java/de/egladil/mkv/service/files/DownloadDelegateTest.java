//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.shiro.authc.DisabledAccountException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.utils.MockInMemoryDB;
import de.egladil.mkv.service.payload.response.DateiInfo;

/**
 * DownloadDelegateTest
 */
public class DownloadDelegateTest {

	private static final Logger LOG = LoggerFactory.getLogger(DownloadDelegateTest.class);

	private static final String WETTBEWERBSJAHR = "2017";

	private static final String DOWNLOAD_PATH = "/home/heike/Downloads/mkv-junit";

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private ILehrerkontoDao lehrerkontoDao;

	private IPrivatkontoDao privatkontoDao;

	private IProtokollService protokollService;

	private IBenutzerService benutzerService;

	@SuppressWarnings("deprecation")
	private MockInMemoryDB mockDB = new MockInMemoryDB();

	private DownloadDelegate downloadDelegate;

	@BeforeEach
	public void setUp() {
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		protokollService = Mockito.mock(IProtokollService.class);
		benutzerService = Mockito.mock(IBenutzerService.class);
		downloadDelegate = new DownloadDelegate(lehrerkontoDao, privatkontoDao, protokollService, benutzerService);
	}

	@Test
	public void checkPermission_wirft_PreconditionFailed_wenn_keine_anmeldung_aktuelles_jahr() {
		// Arrange
		final Optional<Lehrerkonto> opt = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_3);
		final Lehrerkonto lehrerkonto = opt.get();

		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_3)).thenReturn(opt);
		Mockito.when(privatkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_3)).thenReturn(Optional.empty());

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setUuid(MockInMemoryDB.UUID_LEHRER_3);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(lehrerkonto.getUuid())).thenReturn(benutzerkonto);

		final String expected = "download.ohneAnmeldung";

		final Ereignis ereignis = new Ereignis(Ereignisart.DOWNLOAD_OHNE_RECHT.getKuerzel(), MockInMemoryDB.UUID_LEHRER_3,
			"keine Teilnahme mit Jahr 2017");
		Mockito.when(protokollService.protokollieren(ereignis)).thenReturn(ereignis);

		// Act
		try {
			downloadDelegate.checkPermission(MockInMemoryDB.UUID_LEHRER_3, WETTBEWERBSJAHR, DOWNLOAD_PATH);
			fail("keine PreconditionFailedException");
		} catch (final PreconditionFailedException e) {
			// Assert
			assertEquals(expected, e.getMessage());
			Mockito.verify(protokollService, times(1)).protokollieren(ereignis);
			LOG.info(applicationMessages.getString(e.getMessage()));
		}
	}

	@Test
	public void checkPermission_wirft_PreconditionFailed_wenn_downloads_nicht_freigeschaltet() {
		// Arrange
		downloadDelegate.setFreischaltungLehrerMarker("höhö.txt");
		final Optional<Lehrerkonto> opt = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_1);
		final Lehrerkonto lehrerkonto = opt.get();

		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(opt);
		Mockito.when(privatkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(Optional.empty());

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setUuid(MockInMemoryDB.UUID_LEHRER_1);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(lehrerkonto.getUuid())).thenReturn(benutzerkonto);

		final String expected = "download.nicht_freigeschaltet";

		// Act
		try {
			downloadDelegate.checkPermission(MockInMemoryDB.UUID_LEHRER_1, WETTBEWERBSJAHR, DOWNLOAD_PATH);
			fail("keine PreconditionFailedException");
		} catch (final PreconditionFailedException e) {
			// Assert
			assertEquals(expected, e.getMessage());
			LOG.info(applicationMessages.getString(e.getMessage()));
		}
	}

	@Test
	public void checkPermission_wirft_DisabledAccount_wenn_benutzer_gesperrt() {
		// Arrange
		final Optional<Lehrerkonto> opt = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_1);
		final Lehrerkonto lehrerkonto = opt.get();

		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(opt);
		Mockito.when(privatkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(Optional.empty());

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setGesperrt(true);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setUuid(MockInMemoryDB.UUID_LEHRER_1);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(lehrerkonto.getUuid())).thenReturn(benutzerkonto);

		// Act
		try {
			downloadDelegate.checkPermission(MockInMemoryDB.UUID_LEHRER_1, WETTBEWERBSJAHR, DOWNLOAD_PATH);
			fail("keine DisabledAccountException");
		} catch (final DisabledAccountException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void checkPermission_wirft_DisabledAccount_wenn_benutzer_deaktiviert() {
		// Arrange
		final Optional<Lehrerkonto> opt = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_1);
		final Lehrerkonto lehrerkonto = opt.get();

		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(opt);
		Mockito.when(privatkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(Optional.empty());

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(false);
		benutzerkonto.setUuid(MockInMemoryDB.UUID_LEHRER_1);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(lehrerkonto.getUuid())).thenReturn(benutzerkonto);

		// Act
		try {
			downloadDelegate.checkPermission(MockInMemoryDB.UUID_LEHRER_1, WETTBEWERBSJAHR, DOWNLOAD_PATH);
			fail("keine DisabledAccountException");
		} catch (final DisabledAccountException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void checkPermission_hat_konto() {
		// Arrange
		final Optional<Lehrerkonto> opt = mockDB.findLehrerkontoByUuid(MockInMemoryDB.UUID_LEHRER_1);
		final Lehrerkonto lehrerkonto = opt.get();

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setUuid(MockInMemoryDB.UUID_LEHRER_1);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(lehrerkonto.getUuid())).thenReturn(benutzerkonto);

		Mockito.when(lehrerkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(opt);
		Mockito.when(privatkontoDao.findByUUID(MockInMemoryDB.UUID_LEHRER_1)).thenReturn(Optional.empty());

		// Act
		final IMKVKonto vo = downloadDelegate.checkPermission(MockInMemoryDB.UUID_LEHRER_1, WETTBEWERBSJAHR, DOWNLOAD_PATH);

		// Assert
		assertNotNull(vo);
	}

	@Test
	public void getDateiinfosKlappt() {

		// Act
		final List<DateiInfo> dateiInfos = downloadDelegate.getDateiInfosWettbewerbsdownloads(DOWNLOAD_PATH);
		List<String> rolenames = Arrays.asList(new String[] { "MKV_LEHRER", "MKV_PRIVAT" });

		// Assert
		assertFalse(dateiInfos.isEmpty());

		for (final DateiInfo info : dateiInfos) {
			System.out.println(info.toString());
			assertTrue(info.allowedForRoles(rolenames));
		}
	}
}
