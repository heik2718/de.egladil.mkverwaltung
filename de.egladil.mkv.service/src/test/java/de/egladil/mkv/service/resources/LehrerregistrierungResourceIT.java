//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.service.AbstractGuiceIT;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;

/**
 * RegisterSchuleResourceIT
 */
public class LehrerregistrierungResourceIT extends AbstractGuiceIT {

	@Inject
	private LehrerregistrierungResource resource;

	private Lehrerregistrierung payload;

	@Inject
	private IBenutzerDao benutzerDao;

	@Inject
	private ILehrerkontoDao lehrerkontoDao;

	@Test
	public void register_klappt() {
		// Arrange
		TestUtils.initMKVApiKontextReader("2014", true);

		payload = TestUtils.validRandomLehrerregistrierung();

		// Act
		final Response response = resource.register(payload);

		// Assert
		assertEquals(201, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Ihr Benutzerkonto wurde erfolgreich angelegt und eine Mail wurde an Ihre Email-Adresse versendet. Bitte schauen Sie in Ihrem Postfach nach, um die Registrierung abzuschließen. Sie sind nach der Aktivierung zum Wettbewerb 2014 angemeldet.",
			apiResponse.getApiMessage().getMessage());

		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByLoginName(payload.getEmail(), Anwendung.MKV);
		assertNotNull(benutzerkonto);
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());

		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(benutzerkonto.getUuid());
		assertTrue(opt.isPresent());
		final Lehrerkonto lehrerkonto = opt.get();
		final TeilnahmeIdentifierProvider teilnahmeZuJahr = lehrerkonto.getTeilnahmeZuJahr("2014");
		assertNotNull(teilnahmeZuJahr);

		final Schulteilnahme teilnahme = (Schulteilnahme) teilnahmeZuJahr;
		assertEquals(teilnahme.getKuerzel(), lehrerkonto.getSchule().getKuerzel());
		assertEquals(teilnahme.getJahr(), "2014");
		assertEquals(payload.isAutomatischBenachrichtigen(), lehrerkonto.isAutomatischBenachrichtigen());
	}
}
