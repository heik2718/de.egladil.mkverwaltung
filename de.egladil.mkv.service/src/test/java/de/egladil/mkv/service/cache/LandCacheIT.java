//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.cache.LandCache;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.service.AbstractGuiceIT;

/**
 * LandCacheIT
 */
public class LandCacheIT extends AbstractGuiceIT {

	@Inject
	private LandCache landCache;

	@Test
	public void getLand_gibt_vorhandenes_land_zurueck() {
		// Arrange
		final String kuerzel = "ABCDE";

		// Act
		final Optional<Land> optLand = landCache.getLand(kuerzel);

		// Assert
		assertTrue(optLand.isPresent());
		final Land land = optLand.get();
		assertEquals(kuerzel, land.getKuerzel());
		assertEquals("Testland", land.getName());
	}

	@Test
	public void getLand_wirft_resource_not_found_wenn_nicht_vorhanden() {
		// Arrange
		final String kuerzel = "EDCBA";

		// Act
		try {
			landCache.getLand(kuerzel);
		} catch (final EgladilWebappException e) {
			final Response response = e.getResponse();
			assertEquals(404, response.getStatus());
		}
	}
}
