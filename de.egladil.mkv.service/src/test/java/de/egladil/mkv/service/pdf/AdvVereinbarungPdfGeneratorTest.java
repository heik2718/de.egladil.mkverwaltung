//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.pdf;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.domain.adv.AdvText;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AdvVereinbarungPdfGeneratorTest
 */
public class AdvVereinbarungPdfGeneratorTest {

	private static final String DOWNLOAD_PATH = "/home/heike/Downloads/mkv/pdf";

	private static final String PFAD_VEREINBARUNG = "/home/heike/temp/adv-vereinbarung-UV4ER2D2.pdf";

	private AdvVereinbarung vereinbarung;

	@BeforeEach
	void setUp() {
		vereinbarung = new AdvVereinbarung();
		vereinbarung.setHausnummer("35");
		vereinbarung.setSchulkuerzel("UV4ER2D2");
		vereinbarung.setLaendercode("de");
		vereinbarung.setOrt("Fernwald");
		vereinbarung.setPlz("65432");
		vereinbarung.setSchulname("Brüder-Grimm-Schule");
		vereinbarung.setStrasse("Hauptstraße");
		vereinbarung.setZugestimmtAm("07.05.2018 13:02:18");

		final AdvText advText = new AdvText();
		advText.setDateiname("adv-vereinbarung-1.0.pdf");
		advText.setVersionsnummer("1.0");

		vereinbarung.setAdvText(advText);
	}

	@Test
	void generierenKlappt() throws Exception {
		// Arrange
		final AdvVereinbarungPdfGenerator generator = new AdvVereinbarungPdfGenerator(vereinbarung, DOWNLOAD_PATH);

		// Act
		final byte[] pdf = generator.generatePdf();

		try (final ByteArrayInputStream bis = new ByteArrayInputStream(pdf);
			final FileOutputStream fos = new FileOutputStream(PFAD_VEREINBARUNG)) {
			IOUtils.copy(bis, fos);
			fos.flush();
		}

		assertTrue(new File(PFAD_VEREINBARUNG).canRead());
	}

	@Test
	void generiereThrowsMkvExceptionWennDateiNichtGefunden() {
		// Arrange
		vereinbarung.getAdvText().setDateiname("foo.pdf");
		final AdvVereinbarungPdfGenerator generator = new AdvVereinbarungPdfGenerator(vereinbarung, DOWNLOAD_PATH);

		final Throwable ex = assertThrows(MKVException.class, () -> {
			generator.generatePdf();
		});
		assertEquals(
			"Datei nicht gefunden oder keine Berechtigung - /home/heike/Downloads/mkv/pdf/foo.pdf (Datei oder Verzeichnis nicht gefunden)",
			ex.getMessage());
	}

	@Test
	void constructorThrowsWhenVereinbarungNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new AdvVereinbarungPdfGenerator(null, DOWNLOAD_PATH);
		});
		assertEquals("vereinbarung null", ex.getMessage());
	}
}
