//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.payload.request.Privatregistrierung;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;

/**
 * PrivatanmeldungServiceImplOhneMailserviceIT
 */
public class PrivatRegistrierungServiceImplIT extends AbstractRegistrierungServiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(PrivatRegistrierungServiceImplIT.class);

	@Inject
	private IPrivatRegistrierungService service;

	@Inject
	private IWettbewerbsanmeldungService wettbewerbsanmeldungService;

	@Test
	public void kontaktAnlegenUndZumWettbewerbAnmelden_klappt() {
		LOG.info("Testen Userstory #3 ohne Mailversand");
		final String jahr = "2008";
		TestUtils.initMKVApiKontextReader(jahr, true);
		// wettbewerbsanmeldungService.setDownloadPath(TestUtils.getDownloadPath());
		// long millis = System.currentTimeMillis();
		//
		// Privatregistrierung anfrage = new Privatregistrierung("MKV", "Ingo IT", "Inkognito-" + millis,
		// "ingo." + millis + "@egladil.de", "Qwertz!2", "Qwertz!2", null);
		final long millis = System.currentTimeMillis();
		final Privatregistrierung anfrage = new Privatregistrierung("Ingo", "Inkognito-" + millis, millis + "-ingo@egladil.de",
			"Qwertz!2", "Qwertz!2", null);

		anfrage.setAgbGelesen(true);
		anfrage.setAutomatischBenachrichtigen(true);

		// Act
		final Aktivierungsdaten aktivierungsdaten = service.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, jahr, true);

		// Assert
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
		assertNotNull(benutzerkonto);
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());
		assertEquals(anfrage.getEmail(), benutzerkonto.getLoginName());
		assertEquals(benutzerkonto, aktivierungsdaten.getBenutzerkonto());

		final Optional<Privatkonto> opt = privatkontaktDao.findByUUID(benutzerkonto.getUuid());
		assertTrue(opt.isPresent());

		final Privatkonto registeredPrivatkonto = opt.get();

		final Person kontaktdaten = registeredPrivatkonto.getPerson();
		assertEquals(anfrage.getVorname(), kontaktdaten.getVorname());
		assertEquals(anfrage.getNachname(), kontaktdaten.getNachname());

		final List<Privatteilnahme> registeredSchulteilnahmen = registeredPrivatkonto.getTeilnahmen();
		assertFalse(registeredSchulteilnahmen.isEmpty());

		Privatteilnahme neueTeilnahme = null;
		for (final Privatteilnahme t : registeredSchulteilnahmen) {
			if (jahr.equals(t.getJahr())) {
				neueTeilnahme = t;
				break;
			}
		}
		assertNotNull(neueTeilnahme);
	}

	@Test
	public void kontaktAnlegenUndZumWettbewerbAnmelden_gleiche_usercredentials_throws_900() {
		service = new PrivatRegistrierungServiceImpl(benutzerService, mailDelegate, benutzerkontoDelegate, privatkontaktDao,
			anonymisierungsservice, null);
		LOG.info("Testen Userstory #3 ohne Mailversand");
		final Privatregistrierung anfrage = new Privatregistrierung("Ingo", "Inkognito", "ingo@egladil.de", "Qwertz!2", "Qwertz!2",
			null);
		final String jahr = "2008";
		anfrage.setAgbGelesen(true);
		anfrage.setAutomatischBenachrichtigen(true);

		// Act
		try {
			service.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, jahr, false);
			fail("keine EgladilDuplicateEntryException");
		} catch (final EgladilDuplicateEntryException e) {
			LOG.info(e.getMessage());
			assertEquals("uk_benutzer_1", e.getUniqueIndexName());
		}
	}

	@Test
	public void kontaktZumWettbewerbAnmelden_klappt() {
		// Arrange
		final Privatkonto privatkonto = privatkontaktDao.findByUUID("83b3773b-3d3f-4586-bbf9-71621aa3298b").get();
		final boolean benachrichtigen = !privatkonto.isAutomatischBenachrichtigen();
		final String jahr = "" + jahrOhneAnmeldung(privatkonto);
		final int expectedAnzahl = privatkonto.getTeilnahmen().size() + 1;

		TestUtils.initMKVApiKontextReader(jahr, true);
		// wettbewerbsanmeldungService.setDownloadPath(TestUtils.getDownloadPath());

		final Wettbewerbsanmeldung privatanmeldung = new Wettbewerbsanmeldung(Role.MKV_PRIVAT.toString(), jahr, benachrichtigen);

		final MKVBenutzer result = wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(privatanmeldung, privatkonto.getUuid(),
			Role.MKV_PRIVAT);
		assertEquals(benachrichtigen, privatkonto.isAutomatischBenachrichtigen());
		assertEquals(expectedAnzahl, result.getErweiterteKontodaten().getAnzahlTeilnahmen());

		final PublicTeilnahme aktuelleTeilnahme = result.getErweiterteKontodaten().getAktuelleTeilnahme();
		assertNotNull(aktuelleTeilnahme);
		assertTrue(aktuelleTeilnahme.isAktuelle());
		assertNotNull(aktuelleTeilnahme.getTeilnahmeIdentifier());
		assertNotNull(aktuelleTeilnahme.getTeilnahmeIdentifier().getKuerzel());
		assertEquals(jahr, aktuelleTeilnahme.getTeilnahmeIdentifier().getJahr());
		assertEquals(0, aktuelleTeilnahme.getAnzahlTeilnehmer());
		assertNull(aktuelleTeilnahme.getKollegen());
	}

	private int jahrOhneAnmeldung(final Privatkonto privatkonto) {
		final List<String> jahre = new ArrayList<>();
		for (final Privatteilnahme t : privatkonto.getTeilnahmen()) {
			jahre.add(t.getJahr());
		}
		// noch nicht vorhandenes Jahr suchen.
		int jahr = 3000;
		while (jahre.contains(jahr + "")) {
			jahr++;
		}
		return jahr;
	}
}
