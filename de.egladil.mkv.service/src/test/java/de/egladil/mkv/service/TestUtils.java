//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service;

import java.util.Random;
import java.util.UUID;

import javax.mail.Address;

import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.payload.EmailPayload;
import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.bv.aas.payload.TempPwdAendernPayload;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.request.StatusPayload;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.payload.request.Privatregistrierung;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * TestUtils
 */
public class TestUtils {

	public static String getDownloadPath() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "D:\\projekte\\priv\\download";
		}
		return "/home/heike/Downloads/mkv-junit";
	}

	public static Lehrerregistrierung validLehrerregistrierung() {
		final Lehrerregistrierung result = new Lehrerregistrierung("Hannelore", "Kuckuck", "hannelore.kuckuck@egladil.de",
			"Qwertz!2", "Qwertz!2", null);
		result.setSchulkuerzel("Z45FG9IM");
		result.setAgbGelesen(true);
		result.setAutomatischBenachrichtigen(true);
		return result;
	}

	public static Lehrerregistrierung validRandomLehrerregistrierung() {
		final String rand = new Random().nextInt() + "";
		final Lehrerregistrierung result = new Lehrerregistrierung("Hannelore", "Random-" + rand,
			"hannelore.random-" + rand + "@egladil.de", "Qwertz!2", "Qwertz!2", null);
		result.setSchulkuerzel("MXVCSK7J");
		result.setAgbGelesen(true);
		result.setAutomatischBenachrichtigen(true);
		return result;
	}

	public static Privatregistrierung validPrivatregistrierung() {
		final Privatregistrierung result = new Privatregistrierung("Jonny", "Kuckuck", "mail@provider.de", "Qwertz!2", "Qwertz!2",
			null);
		result.setAgbGelesen(true);
		return result;
	}

	public static Privatregistrierung validRandomPrivatregistrierung() {
		final String rand = new Random().nextInt() + "";
		final Privatregistrierung result = new Privatregistrierung("Jonny", "Random-" + rand,
			"jonny.random-" + rand + "@provider.de", "Qwertz!2", "Qwertz!2", null);
		result.setAgbGelesen(true);
		return result;
	}

	public static Wettbewerbsanmeldung validPrivatanmeldung() {
		final Wettbewerbsanmeldung result = new Wettbewerbsanmeldung(Role.MKV_PRIVAT.toString(), "2010", true);
		return result;
	}

	public static Person validKontaktdaten() {
		final Person kontaktdaten = new Person();
		kontaktdaten.setVorname("Olle, IT");
		kontaktdaten.setNachname("Bolle " + System.currentTimeMillis());
		return kontaktdaten;
	}

	public static final Privatkonto validRandomPrivatkontakt(final String jahr) {
		final Person person = validKontaktdaten();
		final Privatkonto result = new Privatkonto(UUID.randomUUID().toString(), person,
			new Privatteilnahme(jahr, new KuerzelGenerator().generateDefaultKuerzel()), true);
		return result;
	}

	public static final Privatkonto validPrivatkontakt(final String jahr) {
		final Person person = new Person("Olle, IT", "Bolle");
		final Privatkonto result = new Privatkonto(UUID.randomUUID().toString(), person,
			new Privatteilnahme(jahr, new KuerzelGenerator().generateDefaultKuerzel()), true);
		return result;
	}

	public static Privatteilnahme validPrivatteilnahme() {
		final Privatteilnahme result = new Privatteilnahme("2013", new KuerzelGenerator().generateDefaultKuerzel());
		return result;
	}

	public static Lehrerkonto validActiveLehrerkonto(final String uuid, final String jahr) {
		final Schule schule = new Schule();
		schule.setKuerzel("HZTDFR4E");
		schule.setName("Dorfschule");

		final Ort ort = new Ort();
		ort.setName("Kleinkuckucksheim");
		ort.setKuerzel("8HZDFR4E");
		ort.addSchule(schule);

		final Schulteilnahme teilnahme = new Schulteilnahme();
		teilnahme.setKuerzel(schule.getKuerzel());
		teilnahme.setJahr("2017");
		teilnahme.setId(632l);
		final Lehrerkonto lehrerkonto = new Lehrerkonto(uuid, new Person("A", "B"), schule, true);
		lehrerkonto.addSchulteilnahme(teilnahme);// .addTeilnahme(new Lehrerteilnahme(teilnahme));
		return lehrerkonto;
	}

	public static SchuleAnschriftKatalogantrag validSchuleAnschriftKatalogantrag() {
		final SchuleAnschriftKatalogantrag result = new SchuleAnschriftKatalogantrag();
		result.setEmail("gueltige@email.de");
		result.setHausnummer("54");
		result.setKleber(null);
		result.setName("Irgendeine Dorfschule");
		result.setOrt("Kleinkuckkucksheim");
		result.setPlz("CH-64500");
		result.setStrasse("Waldstraße");
		return result;
	}

	public static SchuleUrlKatalogantrag validSchuleUrlKatalogantrag() {
		final SchuleUrlKatalogantrag result = new SchuleUrlKatalogantrag();
		result.setEmail("gueltige@email.de");
		result.setKleber(null);
		result.setName("Irgendeine Dorfschule");
		result.setUrl("http://www.dorfschule.de");
		result.setNameLand("Neues Land");
		return result;
	}

	public static PasswortAendernPayload validPasswortAendernPayload() {
		final PasswortAendernPayload result = new PasswortAendernPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		result.setPasswort("start123");
		result.setPasswortNeu("start987");
		result.setPasswortNeuWdh("start987");
		return result;
	}

	public static TempPwdAendernPayload validTempPwdAendernPayload() {
		final TempPwdAendernPayload result = new TempPwdAendernPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		result.setPasswortNeu("start987");
		result.setPasswortNeuWdh("start987");
		result.setPasswort("Juhet8Ztrd");
		return result;
	}

	public static EmailPayload validEmailPayload() {
		final EmailPayload result = new EmailPayload();
		result.setKleber("");
		result.setEmail("luke@sky.com");
		return result;
	}

	public static MailadresseAendernPayload validMailadresseAendernPayload() {
		final MailadresseAendernPayload result = new MailadresseAendernPayload("alte-mailadresse@egladil.de", "start123",
			"neue-mailadresse@egladil.de");
		return result;
	}

	public static PersonAendernPayload validPersonAendernPayload() {
		final PersonAendernPayload result = new PersonAendernPayload("horst-schlemmer@web.de", "Horst", "Schlemmer", null,
			"MKV_LEHRER");
		return result;
	}

	public static Address getAddress(final String mailAddress) {
		final Address address = new Address() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			public String toString() {
				return mailAddress;
			}

			@Override
			public String getType() {
				// TODO Generierter Code
				return null;
			}

			@Override
			public boolean equals(final Object address) {
				if (this == address) {
					return true;
				}
				if (!(address instanceof Address)) {
					return false;
				}
				return this.toString().equals(((Address) address).toString());
			}
		};
		return address;
	}

	public static StatusPayload validStatusPayload() {
		final StatusPayload result = new StatusPayload();
		result.setRolle(Role.MKV_LEHRER.toString());
		return result;
	}

	public static void initMKVApiKontextReader(final String wettbewerbsjahr, final boolean neuanmeldungFreigeschaltet) {
		KontextReader.destroy();
		final Kontext mockKontext = new Kontext();
		mockKontext.setWettbewerbsjahr(wettbewerbsjahr);
		mockKontext.setNeuanmeldungFreigegeben(neuanmeldungFreigeschaltet);
		mockKontext.setDatumFreigabePrivat("31.05.XXXX");
		mockKontext.setDatumFreigabeLehrer("05.03.XXXX");
		KontextReader.getInstance().setMockKontextForTest(mockKontext);
	}
}
