//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * CreateSchulzuordnungSuccessMessageCommandTest
 */
public class CreateSchulzuordnungSuccessMessageCommandTest {

	private CreateSchulzuordnungSuccessMessageCommand command;

	@BeforeEach
	public void setUp() {
		command = new CreateSchulzuordnungSuccessMessageCommand(true, "2017");
	}

	@Test
	public void getMessageSchulwechselOhneAnmeldung() {
		// Arrange
		final String nameSchule = "Fliederbuschschule";
		final String expected = "Der Schulwechsel war erfolgreich. Ihre neue Schule ist die Fliederbuschschule.";

		// Act
		final String msg = command.getSchulzuordnungSuccessMessage(false, nameSchule);

		// Assert
		assertEquals(msg, expected);
	}

	@Test
	public void getMessageSchulwechselMitAnmeldungAnmeldungFreigeschaltet() {
		// Arrange
		final String nameSchule = "Fliederbuschschule";
		final String expected = "Der Schulwechsel war erfolgreich. Sie haben die Fliederbuschschule erfolgreich zum Wettbewerb 2017 angemeldet.";

		// Act
		final String msg = command.getSchulzuordnungSuccessMessage(true, nameSchule);

		// Assert
		assertEquals(msg, expected);
	}

	@Test
	public void getMessageSchulwechselMitAnmeldungAnmeldungNichtFreigeschaltet() {
		// Arrange
		command = new CreateSchulzuordnungSuccessMessageCommand(false, "2017");
		final String nameSchule = "Fliederbuschschule";
		final String expected = "Der Schulwechsel war erfolgreich. Ihre neue Schule ist die Fliederbuschschule. Der Anmeldezeitraum für das Wettbewerbsjahr 2017 hat noch nicht begonnen. Bitte melden Sie Ihre neue Schule später zum Wettbewerb an.";

		// Act
		final String msg = command.getSchulzuordnungSuccessMessage(true, nameSchule);

		// Assert
		assertEquals(msg, expected);
	}

}
