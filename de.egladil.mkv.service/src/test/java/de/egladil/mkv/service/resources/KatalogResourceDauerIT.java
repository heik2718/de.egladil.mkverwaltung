//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.mkv.service.AbstractGuiceQSIT;

/**
 * KatalogResourceIT
 */
@Disabled
public class KatalogResourceDauerIT extends AbstractGuiceQSIT {

	private static final Logger LOG = LoggerFactory.getLogger(KatalogResourceDauerIT.class);

	private Map<String, List<String>> landUndOrtskuerzel = new HashMap<>();

	private final Random random = new Random();

	@Inject
	private KatalogResource resource;

	@Test
	public void land_und_ort_abfrager() {
		final List<String> laenderkuerzel = Arrays.asList(new String[] { "de-BB", "de-BW", "de-NW" });
		landUndOrtskuerzel.put("de-BB", Arrays.asList(new String[] { "NS808DP5", "ZQYQQGCF", "9I5S9F89", "DZSPGMR2", "6BH9RPZR" }));
		landUndOrtskuerzel.put("de-BW", Arrays.asList(new String[] { "GP2XX776", "R7KS7GVI", "CJCZK60H", "NWSTXRRA", "UYIPNXX2" }));
		landUndOrtskuerzel.put("de-NW", Arrays.asList(new String[] { "LXBC06US", "NLGZ2U1X", "P7E07GPQ", "WF44DP7L", "H4NYKSNL" }));

		while (true) {
			String landk = null;
			try {
				landk = laenderkuerzel.get(random.nextInt(3));
			} catch (final ArrayIndexOutOfBoundsException e) {
				landk = laenderkuerzel.get(1);
			}
			final List<String> ortkuerzel = landUndOrtskuerzel.get(landk);
			String ortk = null;
			try {
				ortk = ortkuerzel.get(random.nextInt(6));
			} catch (final ArrayIndexOutOfBoundsException e) {
				ortk = ortkuerzel.get(3);
			}

			LOG.info("landk = {}, ortk = {}", landk, ortk);

			try {
				resource.getLand(landk);
				LOG.info("Land {} geholt", landk);
			} catch (final Exception e) {
				LOG.error(e.getClass() + " - " + e.getMessage() + " beim Lesen von Land " + landk);
			}

			final int seconds = random.nextInt(7);
			LOG.info("warten {}s", seconds);
			long millis = seconds * 1000;
			try {
				Thread.sleep(millis);
			} catch (final InterruptedException e1) {
			}

			try {
				resource.getOrt(landk, ortk);
				LOG.info("Ort {} geholt", ortk);
			} catch (final Exception e) {
				LOG.error(e.getClass() + " - " + e.getMessage() + " beim Lesen von Land " + landk + ", Ort " + ortk);
			}

			final int minutes = random.nextInt(12);
			LOG.info("warten {}min", minutes);
			millis = minutes * 60000;
			// seconds = random.nextInt(4);
			// long millis = seconds * 1000;
			try {
				Thread.sleep(millis);
			} catch (final InterruptedException e1) {
			}

		}
	}
}
