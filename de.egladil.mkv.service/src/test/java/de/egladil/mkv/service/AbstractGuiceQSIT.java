//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service;

import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.mkv.service.config.MKVQSTestModule;

/**
 * @author heikew
 *
 */
public abstract class AbstractGuiceQSIT {

	@Before
	public void setUp() {
		Injector injector = Guice.createInjector(new MKVQSTestModule());
		injector.injectMembers(this);
	}
}
