//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.impl.PrivatteilnahmeDaoImpl;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.confirmation.impl.PrivatConfirmationServiceImpl;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.Privatregistrierung;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;
import de.egladil.mkv.service.registrierung.impl.PrivatRegistrierungServiceImpl;

/**
 * PrivatregsitrierungsprozessMehrstufigIT
 */
public class PrivatregsitrierungsprozessMehrstufigIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(PrivatregsitrierungsprozessMehrstufigIT.class);

	private PrivatRegistrierungServiceImpl privatanmeldungService;

	private PrivatConfirmationServiceImpl privatConfirmationService;

	@Inject
	private IRegistrierungService registrierungService;

	@Inject
	private IBenutzerService benutzerService;

	@Inject
	private MailDelegate mailDelegate;

	@Inject
	private IPrivatkontoDao privatkontaktDao;

	@Inject
	private IBenutzerDao benutzerDao;

	@Inject
	private IAktivierungDao aktivierungDao;

	@Inject
	private BenutzerkontoDelegate benutzerkontoDelegate;

	@Inject
	private IAnonymisierungsservice anonymisierungsservice;

	@Inject
	private IWettbewerbsanmeldungService wettbewerbsanmeldungService;

	private String benutzerUUID;

	private String aktuellerConfirmationCode;

	private PrivatteilnahmeDaoImpl privatteilnahmeDao;

	@Test
	public void registrieren_aktivieren_anmelden_anmeldungAktivieren_mailMock() {
		privatanmeldungService = new PrivatRegistrierungServiceImpl(benutzerService, mailDelegate, benutzerkontoDelegate,
			privatkontaktDao, anonymisierungsservice, null);
		privatConfirmationService = new PrivatConfirmationServiceImpl(registrierungService, benutzerService, anonymisierungsservice,
			privatkontaktDao, privatteilnahmeDao);
		// wettbewerbsanmeldungService.setDownloadPath(TestUtils.getDownloadPath());

		String jahr = "2008";
		TestUtils.initMKVApiKontextReader(jahr, true);

		final long millies = System.currentTimeMillis();

		final Privatregistrierung anfrage = new Privatregistrierung("Privat", "Kontakt-" + millies,
			"privat.kontakt" + millies + "@egladil.de", "Qwertz!2", "Qwertz!2", null);
		anfrage.setAgbGelesen(true);
		anfrage.setGleichAnmelden(true);
		Aktivierungsdaten aktivierungsdaten = null;

		// Act 1
		{
			aktivierungsdaten = privatanmeldungService.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, jahr, true);
			aktuellerConfirmationCode = aktivierungsdaten.getConfirmationCode();

			// Assert
			final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
			assertNotNull(benutzerkonto);
			assertFalse(benutzerkonto.isAktiviert());
			assertFalse(benutzerkonto.isGesperrt());
			assertEquals(anfrage.getEmail(), benutzerkonto.getLoginName());
			benutzerUUID = benutzerkonto.getUuid();

			final Privatkonto registeredKontakt = privatkontaktDao.findByUUID(benutzerkonto.getUuid()).get();
			assertNotNull(registeredKontakt);

			final Person kontaktdaten = registeredKontakt.getPerson();
			assertEquals(anfrage.getVorname(), kontaktdaten.getVorname());
			assertEquals(anfrage.getNachname(), kontaktdaten.getNachname());

			final List<Privatteilnahme> registeredTeilnahmen = registeredKontakt.getTeilnahmen();
			assertFalse(registeredTeilnahmen.isEmpty());

			Privatteilnahme neueTeilnahme = null;
			for (final Privatteilnahme t : registeredTeilnahmen) {
				if (jahr.equals(t.getJahr())) {
					neueTeilnahme = t;
					break;
				}
			}
			assertNotNull(neueTeilnahme);
			assertTrue(registeredKontakt.isAutomatischBenachrichtigen());

			LOG.info("{} hat Benutzerkonto und Wettbewerbsanmeldung für {} - beides inaktiv", kontaktdaten, jahr);
		}

		// Act 2
		privatConfirmationService.jetztRegistrierungBestaetigen(aktuellerConfirmationCode, jahr);

		// Assert 2
		{
			final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
			assertTrue(benutzerkonto.isAktiviert());

			final Aktivierungsdaten aktivierungsdaten2 = aktivierungDao
				.findByConfirmationCode(aktivierungsdaten.getConfirmationCode());
			// FIXME könnte mit den Jahren zu großen Payloads führen!
			assertNotNull(aktivierungsdaten2);

			final Privatkonto privatkontakt2 = privatkontaktDao.findByUUID(benutzerUUID).get();
			final TeilnahmeIdentifierProvider teilnahme2008 = privatkontakt2.getTeilnahmeZuJahr(jahr);
			assertTrue(teilnahme2008 instanceof Privatteilnahme);

			LOG.info("{} und Teilnahme {} sind aktiviert", benutzerkonto, jahr);
		}

		{
			jahr = "2009";
			final Wettbewerbsanmeldung privatanmeldung = new Wettbewerbsanmeldung(Role.MKV_PRIVAT.toString(), jahr, true);

			// Act 3
			wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(privatanmeldung, benutzerUUID, Role.MKV_PRIVAT);
			final Privatkonto privatkontakt = privatkontaktDao.findByUUID(benutzerUUID).get();

			final TeilnahmeIdentifierProvider teilnahme2009 = privatkontakt.getTeilnahmeZuJahr(jahr);
			assertNotNull(teilnahme2009);

			final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByUUID(benutzerUUID);
			assertTrue(benutzerkonto.isAktiviert());

			LOG.info("vorhandener Privatkontakt zu Wettbewerb {} angemeldet, Anmeldung nicht aktiv", jahr);

		}

	}

}
