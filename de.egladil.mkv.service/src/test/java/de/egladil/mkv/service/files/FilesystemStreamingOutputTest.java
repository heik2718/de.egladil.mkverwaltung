//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * FilesystemStreamingOutputTest
 */
public class FilesystemStreamingOutputTest {

	@Test
	public void constructorDownloadDir() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new FilesystemStreamingOutput(null, "2017_minikaenguru_aufgaben_klasse_1.pdf");
		});
		assertEquals("pathDownloadDir darf nicht null sein.", ex.getMessage());
	}

	@Test
	public void constructorFileNameNull() {
		// Act + Assert
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			new FilesystemStreamingOutput("/home/heike/Downloads/mkv", null);
		});
		assertEquals("fileName darf nicht null sein.", ex.getMessage());

	}

	@Test
	public void constructorOhneLeseberechtigung() {
		// Act + Assert
		final Throwable ex = assertThrows(MKVException.class, () -> {
			new FilesystemStreamingOutput("/root", "2017_minikaenguru_aufgaben_klasse_1.pdf");
		});
		assertEquals("Keine Leseberechtigung für Verzeichnis [/root])", ex.getMessage());

	}

}
