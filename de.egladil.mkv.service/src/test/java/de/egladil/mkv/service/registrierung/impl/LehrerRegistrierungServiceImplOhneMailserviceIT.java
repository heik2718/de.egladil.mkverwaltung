//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * SchulanmeldungServiceImplOhneMailserviceIT
 */
public class LehrerRegistrierungServiceImplOhneMailserviceIT extends AbstractRegistrierungServiceIT {

	@Test
	public void schuleUndLehrer() {

		final String kuerzel = "ZS9FE9VH";
		final String jahr = "2008";

		final Optional<Schule> opt = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		assertTrue("Testsetting: Fehler keine Schule mit kuerzel " + kuerzel, opt.isPresent());
		final Schule schule = opt.get();

		final long millies = System.currentTimeMillis();

		final Lehrerregistrierung anfrage = new Lehrerregistrierung("Hannelore IT", "Walter-" + millies,
			"hannelore." + millies + "@egladil.de", "Qwertz!2", "Qwertz!2", null);
		anfrage.setAgbGelesen(true);
		anfrage.setSchulkuerzel(kuerzel);
		anfrage.setSchule(schule);

		// Act
		final Aktivierungsdaten registrierungsbestaetigung = lehrerRegistrierungService
			.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, jahr, true);

		// Assert
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
		assertNotNull(benutzerkonto);
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());
		assertEquals(anfrage.getEmail(), benutzerkonto.getLoginName());
		assertEquals(benutzerkonto, registrierungsbestaetigung.getBenutzerkonto());

		final Optional<Lehrerkonto> lk = lehrerkontoDao.findByUUID(benutzerkonto.getUuid());
		assertTrue(lk.isPresent());

		final Lehrerkonto neuesLehreroknto = lk.get();

		final Person kontaktdaten = neuesLehreroknto.getPerson();
		assertEquals(anfrage.getVorname(), kontaktdaten.getVorname());
		assertEquals(anfrage.getNachname(), kontaktdaten.getNachname());

		final List<Schulteilnahme> schulteilnahmen = neuesLehreroknto.getSchulteilnahmen();
		assertEquals(1, schulteilnahmen.size());

		final Schulteilnahme schulteilnahme = schulteilnahmen.get(0);
		assertNotNull(schulteilnahme);

		assertTrue(neuesLehreroknto.isAutomatischBenachrichtigen());

		final Optional<Auswertungsgruppe> optRoot = auswertungsgruppenService
			.getRootAuswertungsgruppe(schulteilnahme.provideTeilnahmeIdentifier());
		assertTrue(optRoot.isPresent());
	}

	@Test
	public void neuregistrierung() {

		final String kuerzel = "WCB2BPKZ";
		final String jahr = "2017";

		final Optional<Schule> opt = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		assertTrue("Testsetting: Fehler keine Schule mit kuerzel " + kuerzel, opt.isPresent());
		final Schule schule = opt.get();

		final long millies = System.currentTimeMillis();

		final Lehrerregistrierung anfrage = new Lehrerregistrierung("Hannelore", "Gunther", "hannelore." + millies + "@egladil.de",
			"Qwertz!2", "Qwertz!2", null);
		anfrage.setAgbGelesen(true);
		anfrage.setSchulkuerzel(kuerzel);
		anfrage.setSchule(schule);

		// Act
		final Aktivierungsdaten registrierungsbestaetigung = lehrerRegistrierungService
			.kontaktAnlegenUndZumWettbewerbAnmelden(anfrage, jahr, false);

		// Assert
		final Benutzerkonto benutzerkonto = benutzerDao.findBenutzerByEmail(anfrage.getEmail(), Anwendung.MKV);
		assertNotNull(benutzerkonto);
		assertFalse(benutzerkonto.isAktiviert());
		assertFalse(benutzerkonto.isGesperrt());
		assertEquals(anfrage.getEmail(), benutzerkonto.getLoginName());
		assertEquals(benutzerkonto, registrierungsbestaetigung.getBenutzerkonto());

		final Optional<Lehrerkonto> lk = lehrerkontoDao.findByUUID(benutzerkonto.getUuid());
		assertTrue(lk.isPresent());

		final Lehrerkonto neuesLehreroknto = lk.get();

		final Person kontaktdaten = neuesLehreroknto.getPerson();
		assertEquals(anfrage.getVorname(), kontaktdaten.getVorname());
		assertEquals(anfrage.getNachname(), kontaktdaten.getNachname());

		final List<Schulteilnahme> lehrerteilnahmen = neuesLehreroknto.getSchulteilnahmen();
		assertEquals(1, lehrerteilnahmen.size());

		// final Lehrerteilnahme lehrerteilnahme = lehrerteilnahmen.get(0);
		final Schulteilnahme schulteilnahme = lehrerteilnahmen.get(0);
		assertNotNull(schulteilnahme);

		assertTrue(neuesLehreroknto.isAutomatischBenachrichtigen());
	}

	@Test
	@DisplayName("angemeldete Lehrerin")
	public void testLehrer() {
		// Lehrerin ist angemeldet => ist bekannt über Principal

		final String benutzerUUID = "cfb50775-9bcb-4a02-b61b-613a4cbafbc6";
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(benutzerUUID);
		assertTrue("Test kann nicht stattfinden: Lehrerkonto nicht vorhanden", opt.isPresent());
		final Lehrerkonto vorhandenes = opt.get();
		assertNotNull("Test kann nicht stattfinden: Lehrerkonto hat keine Schule", vorhandenes.getSchule());

		final boolean benachrichtigen = true;

		final List<Schulteilnahme> lehrerteilnahmen = vorhandenes.getSchulteilnahmen();
		assertFalse(lehrerteilnahmen.isEmpty());

		final String wettbewerbsjahr = "" + jahrOhneAnmeldung(lehrerteilnahmen);
		final int expectedAnzahl = lehrerteilnahmen.size() + 1;

		TestUtils.initMKVApiKontextReader(wettbewerbsjahr, true);

		// wettbewerbsanmeldungService.setDownloadPath(TestUtils.getDownloadPath());

		// Act
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), wettbewerbsjahr,
			benachrichtigen);
		final MKVBenutzer geaendertes = wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, benutzerUUID,
			Role.MKV_LEHRER);

		// Assert
		assertEquals(expectedAnzahl, geaendertes.getErweiterteKontodaten().getAnzahlTeilnahmen());

		final PublicTeilnahme aktuelle = geaendertes.getErweiterteKontodaten().getAktuelleTeilnahme();
		assertTrue(aktuelle.isAktuelle());

		assertNotNull(aktuelle);
		final PublicSchule neue = aktuelle.getSchule();
		assertNotNull(neue);
	}

	@Test
	public void testWeitererLehrer() {
		// Lehrerin ist angemeldet => ist bekannt über Principal
		// Schulkuerzel = WCB2BPKZ

		final String benutzerUUID = "23a52cb7-fd86-4294-9b39-5e93081244cb";
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(benutzerUUID);
		assertTrue("Test kann nicht stattfinden: Lehrerkonto nicht vorhanden", opt.isPresent());
		final Lehrerkonto vorhandenes = opt.get();
		assertNotNull("Test kann nicht stattfinden: Lehrerkonto hat keine Schule", vorhandenes.getSchule());

		final boolean benachrichtigen = true;

		final List<Schulteilnahme> lehrerteilnahmen = vorhandenes.getSchulteilnahmen();
		assertFalse(lehrerteilnahmen.isEmpty());

		final String wettbewerbsjahr = "" + jahrOhneAnmeldung(lehrerteilnahmen);
		final int expectedAnzahl = lehrerteilnahmen.size() + 1;

		final List<Schulteilnahme> schulteilnahmen = schulteilnahmeDao.findAllByJahr(wettbewerbsjahr);
		final int expectedAnzahlSchulteilnahmen = schulteilnahmen.isEmpty() ? 1 : schulteilnahmen.size();

		TestUtils.initMKVApiKontextReader(wettbewerbsjahr, true);
		// wettbewerbsanmeldungService.setDownloadPath(TestUtils.getDownloadPath());

		System.out.println("Jahr = " + wettbewerbsjahr);

		// Act
		final Wettbewerbsanmeldung anmeldung = new Wettbewerbsanmeldung(Role.MKV_LEHRER.toString(), wettbewerbsjahr,
			benachrichtigen);
		final MKVBenutzer geaendertes = wettbewerbsanmeldungService.benutzerZumWettbewerbAnmelden(anmeldung, benutzerUUID,
			Role.MKV_LEHRER);

		// Assert
		assertEquals(expectedAnzahl, geaendertes.getErweiterteKontodaten().getAnzahlTeilnahmen());

		final PublicTeilnahme aktuelle = geaendertes.getErweiterteKontodaten().getAktuelleTeilnahme();
		assertTrue(aktuelle.isAktuelle());

		assertNotNull(aktuelle);

		final PublicSchule neue = aktuelle.getSchule();
		assertNotNull(neue);

		final List<Schulteilnahme> aktuelleTeilnahmen = schulteilnahmeDao.findAllByJahr(wettbewerbsjahr);
		assertEquals(expectedAnzahlSchulteilnahmen, aktuelleTeilnahmen.size());
	}

	private int jahrOhneAnmeldung(final List<Schulteilnahme> lehrerteilnahmen) {
		final List<String> jahre = new ArrayList<>();
		for (final Schulteilnahme t : lehrerteilnahmen) {
			jahre.add(t.getJahr());
		}
		// noch nicht vorhandenes Jahr suchen.
		int jahr = 3000;
		while (jahre.contains(jahr + "")) {
			jahr++;
		}
		return jahr;
	}
}
