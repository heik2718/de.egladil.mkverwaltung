//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;

/**
 * @author heikew
 *
 */
public class ValidationDelegateSchuleKatalogantragTest {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationDelegateSchuleKatalogantragTest.class);

	private ValidationDelegate validationDelegate;

	private SchuleUrlKatalogantrag anfrage;

	private ObjectMapper mapper;

	@BeforeEach
	public void setUp() {
		validationDelegate = new ValidationDelegate();
		anfrage = TestUtils.validSchuleUrlKatalogantrag();
		mapper = new ObjectMapper();
	}

	@Test
	public void check_throws_forbidden_when_kleber_invalid() {
		// Arrange
		anfrage.setKleber("h");

		// Act + Assert
		try {
			validationDelegate.check(anfrage, SchuleUrlKatalogantrag.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(403, e.getResponse().getStatus());
		}
	}

	@Test
	public void check_schuleUrlKatalogantrag_throws_902_constraintViolation_when_anfrage_invalid() throws Exception {
		// Arrange
		anfrage.setUrl("file:///root/");

		// Act + Assert
		try {
			validationDelegate.check(anfrage, SchuleUrlKatalogantrag.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}

	@Test
	public void check_schuleAnschriftKatalogantrag_throws_902_constraintViolation_when_anfrage_invalid() throws Exception {
		// Arrange
		final SchuleAnschriftKatalogantrag payload = TestUtils.validSchuleAnschriftKatalogantrag();
		payload.setEmail("holla");
		payload.setPlz("12345678901");

		// Act + Assert
		try {
			validationDelegate.check(payload, SchuleAnschriftKatalogantrag.class);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(902, e.getResponse().getStatus());
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) e.getResponse().getEntity();
			final String json = mapper.writeValueAsString(cvm);
			LOG.info(json);
		}
	}
}
