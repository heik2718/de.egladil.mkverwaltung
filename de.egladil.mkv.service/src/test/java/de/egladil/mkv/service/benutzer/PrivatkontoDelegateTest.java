//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Optional;

import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * PrivatkontoDelegateTest. Alles, was mit verifyBenutzer zu tun hat, ist bereits durch LehrerkobtoDelegateTest
 * abgedeckt.
 */
public class PrivatkontoDelegateTest {

	private static final String BENUTZER_UUID = "gsgga-zweu628";

	private static final String EMAIL = "horst.schlemmer@provider.com";

	private PersonAendernPayload payload;

	private IBenutzerService benutzerService;

	private IPrivatkontoDao privatkontoDao;

	private PrivatkontoDelegate delegate;

	@BeforeEach
	public void setUp() {
		benutzerService = Mockito.mock(IBenutzerService.class);
		privatkontoDao = Mockito.mock(IPrivatkontoDao.class);
		payload = new PersonAendernPayload(EMAIL, "Horst", "Schlemmer", null);

		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());
		benutzerkonto.setLoginName(payload.getUsername());

		// dieser Mock funktioniert so, wie gewünscht, um nur noch die privatkontospezifischen Dinge testen zu müssen.
		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		delegate = new PrivatkontoDelegate(privatkontoDao, benutzerService);
	}

	@Test
	@DisplayName("verifyAndChangePerson: Umwandlung OptimisticLockException in EgladilConcurrentModificationException")
	public void verifyAndChangePersonOptimisticLockException() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		final Person person = new Person("Mario", "May");

		final Privatteilnahme privatteilnahme = new Privatteilnahme("2017", new KuerzelGenerator().generateDefaultKuerzel());
		final Privatkonto privatkonto = new Privatkonto(BENUTZER_UUID, person, privatteilnahme, true);
		Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(privatkonto));
		Mockito.when(privatkontoDao.persist(privatkonto))
			.thenThrow(new PersistenceException(new OptimisticLockException("optimistic lock")));
		// Act
		final Throwable ex = assertThrows(EgladilConcurrentModificationException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Jemand anders hat das Privatkonto vorher geaendert", ex.getMessage());

	}

	@Test
	@DisplayName("verifyAndChangePerson: Umwandlung PersistenceException in EgladilStorageException")
	public void verifyAndChangePerson_wandelt_PersistenceException() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		final Person person = new Person("Mario", "May");

		final Privatteilnahme privatteilnahme = new Privatteilnahme("2017", new KuerzelGenerator().generateDefaultKuerzel());
		final Privatkonto privatkonto = new Privatkonto(BENUTZER_UUID, person, privatteilnahme, false);
		Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(privatkonto));

		final PersistenceException pe = new PersistenceException(new SQLException());

		Mockito.when(privatkontoDao.persist(privatkonto)).thenThrow(pe);
		// Act
		final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("PersistenceException beim Speichern eines Privatkonto: java.sql.SQLException", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson erzeugt ResourceNotFoundException wenn Konto nicht gefunden")
	public void verifyAndChangePersonPrivatkontoNichtGefunden() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);
		Mockito.when(privatkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());

		// Act
		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Ein passendes Privatkonto wurde nicht gefunden", ex.getMessage());
	}

}
