//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;

/**
 * LehrerkontoServiceTest
 */
public class LehrerkontoDelegateTest {

	private static final String BENUTZER_UUID = "gsgga-zweu628";

	private static final String EMAIL = "horst.schlemmer@provider.com";

	private ISchuleDao schuleDao;

	private ILehrerkontoDao lehrerkontoDao;

	private ISchulteilnahmeDao schulteilnahmeDao;

	private IBenutzerService benutzerService;

	private LehrerkontoDelegate delegate;

	private PersonAendernPayload payload;

	@BeforeEach
	public void setUp() {
		schuleDao = Mockito.mock(ISchuleDao.class);
		lehrerkontoDao = Mockito.mock(ILehrerkontoDao.class);
		schulteilnahmeDao = Mockito.mock(ISchulteilnahmeDao.class);
		benutzerService = Mockito.mock(IBenutzerService.class);
		payload = new PersonAendernPayload(EMAIL, "Horst", "Schlemmer", null);
		delegate = new LehrerkontoDelegate(schuleDao, lehrerkontoDao, schulteilnahmeDao, benutzerService);
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft DisabledAccountException wenn benutzerkonto gesperrt")
	public void verifyAndChangePersonDisabledAccountException() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(true);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		// Act
		final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Das Benutzerkonto ist gesperrt.", ex.getMessage());

	}

	@Test
	@DisplayName("verifyAndChangePerson wirft DisabledAccountException wenn Benutzerkonto nicht aktiviert")
	public void verifyAndChangePersonDisabledAccount() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setAktiviert(false);

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		// Act
		final Throwable ex = assertThrows(DisabledAccountException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Das Benutzerkonto ist nicht aktiviert.", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft UnknownAccountException wenn benutzerkonto und email nicht passen")
	public void verifyAndChangePersonBenutzerkontoUndEmailInkomensurabel() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(EMAIL);

		payload.setUsername("harald@web.de");

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		// Act
		final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("benutzerkonto.loginname != payload.benutzername", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft UnknownAccountException wenn Benutzerkonto null")
	public void verifyAndChangePersonBenutzerkontoNull() {
		// Arrange
		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(null);

		// Act
		final Throwable ex = assertThrows(UnknownAccountException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Benutzerkonto mit UUID [gsgga-zweu628] nicht gefunden", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn payload null")
	public void verifyAndChangePersonPayloadNull() {
		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			delegate.verifyAndChangePerson(null, BENUTZER_UUID);
		});
		assertEquals("Parameter darf nicht null sein: payload null", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn benutzerUuid null")
	public void verifyAndChangePersonBenutzerUuidNull() {

		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			delegate.verifyAndChangePerson(payload, null);
		});
		assertEquals("Parameter darf nicht null sein: benutzerUuid null", ex.getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException wenn beide Parameter null")
	public void beideParameterNull() {

		final Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
			delegate.verifyAndChangePerson(null, null);
		});
		assertEquals("Parameter darf nicht null sein: payload null", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft EgladilConcurrentModificationException weiter")
	public void verifyAndChangePersonEgladilConcurrentModificationException() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		final Person person = new Person("Mario", "May");
		final Lehrerkonto lehrerkonto = new Lehrerkonto();
		lehrerkonto.setUuid(BENUTZER_UUID);
		lehrerkonto.setPerson(person);
		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto))
			.thenThrow(new PersistenceException(new OptimisticLockException("optimistic lock")));
		// Act
		final Throwable ex = assertThrows(EgladilConcurrentModificationException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Jemand anders hat das Lehrerkonto vorher geaendert", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft EgladilStorageException weiter")
	public void verifyAndChangePersonEgladilStorageException() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);

		final Person person = new Person("Mario", "May");
		final Lehrerkonto lehrerkonto = new Lehrerkonto();
		lehrerkonto.setUuid(BENUTZER_UUID);
		lehrerkonto.setPerson(person);
		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.of(lehrerkonto));
		Mockito.when(lehrerkontoDao.persist(lehrerkonto)).thenThrow(new PersistenceException());
		// Act
		final Throwable ex = assertThrows(EgladilStorageException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("PersistenceException beim Speichern eines Lehrerkontos: null", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft ResourceNotFoundException weiter")
	public void verifyAndChangePersonLehrerkontoNichtGefunden() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);
		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());

		// Act
		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Ein passendes Lehrerkonto wurde nicht gefunden", ex.getMessage());
	}

	@Test
	@DisplayName("verifyAndChangePerson wirft ResourceNotFoundException weiter")
	public void verifyAndRegisterDownloadLehrerkontoFehlt() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.setGesperrt(false);
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setLoginName(payload.getUsername());

		Mockito.when(benutzerService.findBenutzerkontoByUUID(BENUTZER_UUID)).thenReturn(benutzerkonto);
		Mockito.when(lehrerkontoDao.findByUUID(BENUTZER_UUID)).thenReturn(Optional.empty());

		// Act
		final Throwable ex = assertThrows(ResourceNotFoundException.class, () -> {
			delegate.verifyAndChangePerson(payload, BENUTZER_UUID);
		});
		assertEquals("Ein passendes Lehrerkonto wurde nicht gefunden", ex.getMessage());
	}
}
