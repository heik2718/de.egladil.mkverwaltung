//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import com.google.inject.Inject;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.storage.IAktivierungDao;
import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.AbstractGuiceIT;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.registrierung.ILehrerRegistrierungService;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;

/**
 * AbstractRegistrierungServiceIT
 */
public abstract class AbstractRegistrierungServiceIT extends AbstractGuiceIT {

	@Inject
	protected IBenutzerDao benutzerDao;

	@Inject
	protected IAktivierungDao aktivierungDao;

	@Inject
	protected IRegistrierungService registrierungService;

	@Inject
	protected IBenutzerService benutzerService;

	@Inject
	protected MailDelegate mailDelegate;

	@Inject
	protected IAuthenticationService authenticationService;

	@Inject
	protected BenutzerkontoDelegate benutzerkontoDelegate;

	@Inject
	protected ILehrerRegistrierungService lehrerRegistrierungService;

	@Inject
	protected ILehrerkontoDao lehrerkontoDao;

	@Inject
	protected ISchuleDao schuleDao;

	@Inject
	protected IPrivatRegistrierungService privatRegistrierungService;

	@Inject
	protected IPrivatkontoDao privatkontaktDao;

	@Inject
	protected IAnonymisierungsservice anonymisierungsservice;

	@Inject
	protected IWettbewerbsanmeldungService wettbewerbsanmeldungService;

	@Inject
	protected ISchulteilnahmeDao schulteilnahmeDao;

	@Inject
	protected AuswertungsgruppenService auswertungsgruppenService;
}
