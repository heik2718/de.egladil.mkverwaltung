//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.payload.response.PublicOrt;
import de.egladil.mkv.service.AbstractGuiceIT;
import de.egladil.mkv.service.TestUtils;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;

/**
 * KatalogResourceIT
 */
public class KatalogResourceIT extends AbstractGuiceIT {

	@Inject
	private KatalogResource resource;

	@SuppressWarnings("unchecked")
	@Test
	public void getLaender_klappt() {
		// Act
		final Response response = resource.getLaender();

		// Assert
		assertNotNull(response);
		assertEquals(200, response.getStatus());

		final List<PublicLand> entity = (List<PublicLand>) response.getEntity();
		assertFalse(entity.isEmpty());
	}

	@Test
	public void getLand_treffer_klappt() {
		// Arrange
		final String kuerzel = "de-BE";

		// Act
		final Response response = resource.getLand(kuerzel);

		// Assert
		assertNotNull(response);
		assertEquals(200, response.getStatus());

		assertNotNull(response.getEntity());
		assertTrue(response.getEntity() instanceof PublicLand);
		final PublicLand entity = (PublicLand) response.getEntity();
		assertEquals(kuerzel, entity.getKuerzel());

		// assertNotNull(entity.getOrte());
		// assertFalse(entity.getOrte().isEmpty());
	}

	@Test
	public void getLand_kein_treffer_klappt() {
		// Arrange
		final String kuerzel = "XXX";

		// Act
		try {
			resource.getLand(kuerzel);
		} catch (final EgladilWebappException e) {
			// Assert
			final Response response = e.getResponse();
			assertNotNull(response);
			assertEquals(404, response.getStatus());
		}
	}

	@Test
	public void getOrt_treffer_klappt() {
		// Arrange
		final String landkuerzel = "ABCDE";
		final String ortkuerzel = "GTR4GK8B";

		// Act
		final Response response = resource.getOrt(landkuerzel, ortkuerzel);

		// Assert
		assertNotNull(response);
		assertEquals(200, response.getStatus());

		assertNotNull(response.getEntity());
		assertTrue(response.getEntity() instanceof PublicOrt);
		final PublicOrt entity = (PublicOrt) response.getEntity();
		assertEquals(ortkuerzel, entity.getKuerzel());

		// assertNotNull(entity.getSchulen());
		// assertFalse(entity.getSchulen().isEmpty());
	}

	@Test
	public void getOrt_land_treffer_ort_kein_treffer_klappt() {
		// Arrange
		final String landkuerzel = "de-BE";
		final String ortkuerzel = "QPO3Z";

		// Act
		try {
			resource.getOrt(landkuerzel, ortkuerzel);
		} catch (final EgladilWebappException e) {
			// Assert
			final Response response = e.getResponse();
			assertNotNull(response);
			assertEquals(404, response.getStatus());
		}
	}

	@Test
	public void getOrt_land_kein_treffer_ort_treffer_klappt() {
		// Arrange
		final String landkuerzel = "de-BEZ";
		final String ortkuerzel = "U4901QPO3Z";

		// Act
		try {
			resource.getOrt(landkuerzel, ortkuerzel);
		} catch (final EgladilWebappException e) {
			// Assert
			final Response response = e.getResponse();
			assertNotNull(response);
			assertEquals(404, response.getStatus());
		}
	}

	@Test
	public void schuleintragBeantragen_klappt() {
		// Arrange
		final String landkuerzel = "de-BY";
		final SchuleAnschriftKatalogantrag katalogantrag = TestUtils.validSchuleAnschriftKatalogantrag();

		// Act
		final Response response = resource.schuleintragBeantragen(landkuerzel, katalogantrag);

		// Assert
		assertEquals(200, response.getStatus());
		assertTrue(response.getEntity() instanceof APIResponsePayload);

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Vielen Dank. Der Katalogeintrag wird zeitnah erstellt. Sie wurden per Email informiert. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_vorhandenes_landkuerzel_schlaegt_landName() {
		// Arrange
		final String landkuerzel = "de-BY";
		final SchuleAnschriftKatalogantrag katalogantrag = TestUtils.validSchuleAnschriftKatalogantrag();
		katalogantrag.setNameLand("Schweden");

		// Act
		final Response response = resource.schuleintragBeantragen(landkuerzel, katalogantrag);

		// Assert
		assertEquals(200, response.getStatus());
		assertTrue(response.getEntity() instanceof APIResponsePayload);

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Vielen Dank. Der Katalogeintrag wird zeitnah erstellt. Sie wurden per Email informiert. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_returns_200_when_landkuerzel_nicht_bekannt_und_nameLand_nicht_leer() {
		// Arrange
		final String landkuerzel = "XXXXX";
		final SchuleAnschriftKatalogantrag katalogantrag = TestUtils.validSchuleAnschriftKatalogantrag();
		katalogantrag.setNameLand("Kuba");

		// Act
		final Response response = resource.schuleintragBeantragen(landkuerzel, katalogantrag);

		// Assert
		assertEquals(200, response.getStatus());
		assertTrue(response.getEntity() instanceof APIResponsePayload);

		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals(
			"Vielen Dank. Der Katalogeintrag wird zeitnah erstellt. Sie wurden per Email informiert. Bitte schauen Sie in Ihrem Postfach nach.",
			apiResponse.getApiMessage().getMessage());
	}

	@Test
	public void schuleintragBeantragen_returns_902_when_landkuerzel_nicht_bekannt_landname_null() {
		// Arrange
		final String landkuerzel = "UNBEK";
		final SchuleAnschriftKatalogantrag katalogantrag = TestUtils.validSchuleAnschriftKatalogantrag();
		katalogantrag.setNameLand(null);

		// Act
		final Response response = resource.schuleintragBeantragen(landkuerzel, katalogantrag);

		// Assert
		assertEquals(902, response.getStatus());
		final APIResponsePayload apiResponse = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), apiResponse.getApiMessage().getLevel());
		assertEquals("Der Name des Landes/Bundeslandes darf nicht leer sein.", apiResponse.getApiMessage().getMessage());
	}
}
