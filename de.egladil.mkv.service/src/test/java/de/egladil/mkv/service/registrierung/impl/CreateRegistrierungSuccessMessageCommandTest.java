//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * CreateRegistrierungSuccessMessageCommandTest
 */
public class CreateRegistrierungSuccessMessageCommandTest {

	private CreateRegistrierungSuccessMessageCommand command;

	@BeforeEach
	public void setUp() {
		command = new CreateRegistrierungSuccessMessageCommand(true, "2017");
	}

	@Test
	public void getRegistrierungSuccessMessageOhneAnmeldung() {
		// Arrange
		final String expected = "Ihr Benutzerkonto wurde erfolgreich angelegt und eine Mail wurde an Ihre Email-Adresse versendet. Bitte schauen Sie in Ihrem Postfach nach, um die Registrierung abzuschließen. Nach der Aktivierung Ihres Benutzerkontos können Sie sich zum Wettbewerb 2017 anmelden.";

		// Act
		final String actual = command.getRegistrierungSuccessMessage(false);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getRegistrierungSuccessMessageMitAnmeldungFreigeschaltet() {
		// Arrange
		final String expected = "Ihr Benutzerkonto wurde erfolgreich angelegt und eine Mail wurde an Ihre Email-Adresse versendet. Bitte schauen Sie in Ihrem Postfach nach, um die Registrierung abzuschließen. Sie sind nach der Aktivierung zum Wettbewerb 2017 angemeldet.";

		// Act
		final String actual = command.getRegistrierungSuccessMessage(true);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getRegistrierungSuccessMessageMitAnmeldungNichFreigeschaltet() {
		// Arrange
		command = new CreateRegistrierungSuccessMessageCommand(false, "2017");
		final String expected = "Ihr Benutzerkonto wurde erfolgreich angelegt und eine Mail wurde an Ihre Email-Adresse versendet. Bitte schauen Sie in Ihrem Postfach nach, um die Registrierung abzuschließen. Da der Wettbewerb 2017 noch nicht begonnen hat, sind Sie nach der Aktivierung noch NICHT angemeldet.";

		// Act
		final String actual = command.getRegistrierungSuccessMessage(true);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getMailtemplatePathOhneAnmeldung() {
		// Arrange
		final String expected = "/mailtemplates/registrierung_ohne_anmeldung.txt";

		// Act
		final String actual = command.getMailtemplatePath(false);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getMailtemplatePathMitAnmeldungFreigeschaltet() {
		// Arrange
		final String expected = "/mailtemplates/registrierung.txt";

		// Act
		final String actual = command.getMailtemplatePath(true);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void getMailtemplatePathMitAnmeldungNichFreigeschaltet() {
		// Arrange
		command = new CreateRegistrierungSuccessMessageCommand(false, "2017");
		final String expected = "/mailtemplates/registrierung_anmeldung_nicht_freigeschaltet.txt";;

		// Act
		final String actual = command.getMailtemplatePath(true);

		// Assert
		assertEquals(expected, actual);
	}

}
