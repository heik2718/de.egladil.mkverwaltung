//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.UniqueIdentifier;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * SchuleConfirmationServiceImplTest
 */
public class PrivatConfirmationServiceImplTest {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

	private IRegistrierungService registrierungService;

	private IPrivatkontoDao kontaktDao;

	private IBenutzerService benutzerService;

	private PrivatConfirmationServiceImpl confirmationService;

	@BeforeEach
	public void setUp() {
		registrierungService = Mockito.mock(IRegistrierungService.class);
		benutzerService = Mockito.mock(IBenutzerService.class);
		kontaktDao = Mockito.mock(IPrivatkontoDao.class);
		final IPrivatteilnahmeDao teilnahmeDao = Mockito.mock(IPrivatteilnahmeDao.class);
		final IAnonymisierungsservice anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);

		confirmationService = new PrivatConfirmationServiceImpl(registrierungService, benutzerService, anonymisierungsservice,
			kontaktDao, teilnahmeDao);
	}

	@Test
	public void jetztRegistrierungBestaetigen_throws_when_jahr_null() {
		try {
			confirmationService.jetztRegistrierungBestaetigen(UUID.randomUUID().toString(), null);
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(500, e.getResponse().getStatus());
		}
	}

	@Test
	public void registrierungBestaetigen_throws_when_now_null() {
		try {
			confirmationService.registrierungBestaetigen(UUID.randomUUID().toString(), null, "2017");
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(500, e.getResponse().getStatus());
		}
	}

	@Test
	public void registrierungBestaetigen_throws_when_not_found() {
		try {
			confirmationService.jetztRegistrierungBestaetigen(UUID.randomUUID().toString(), "2017");
			fail("keine EgladilWebappException");
		} catch (final EgladilWebappException e) {
			assertEquals(404, e.getResponse().getStatus());
		}
	}

	@Test
	public void registrierungBestaetigen_liest_aktivierungsdaten_nur_einmal() throws Exception {
		final Date now = SDF.parse("30.11.2016 09:16:43");
		final Date expiration = SDF.parse("30.11.2016 23:16:43");
		final String confirmationCode = UUID.randomUUID().toString();
		final Aktivierungsdaten regBest = new Aktivierungsdaten();
		regBest.setConfirmationCode(confirmationCode);
		regBest.setExpirationTime(expiration);

		final String benutzerUUID = UUID.randomUUID().toString();
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setId(13l);
		benutzerkonto.setUuid(benutzerUUID);
		benutzerkonto.addAktivierungsdaten(regBest);

		final Privatkonto privatkonto = new Privatkonto(benutzerUUID, new Person("Rudi", "Rettich"),
			new Privatteilnahme("2018", new KuerzelGenerator().generateDefaultKuerzel()), false);
		final Privatteilnahme teilnahme = new Privatteilnahme();
		privatkonto.addTeilnahme(teilnahme);

		Mockito.when(registrierungService.findByConfirmationCode(new UniqueIdentifier(confirmationCode))).thenReturn(regBest);
		Mockito.when(kontaktDao.findByUUID(benutzerUUID)).thenReturn(Optional.of(privatkonto));

		confirmationService.registrierungBestaetigen(confirmationCode, now, "2017");
		Mockito.verify(registrierungService, times(1)).findByConfirmationCode(new UniqueIdentifier(confirmationCode));
		Mockito.verify(kontaktDao, times(1)).findByUUID(benutzerUUID);
	}
}
