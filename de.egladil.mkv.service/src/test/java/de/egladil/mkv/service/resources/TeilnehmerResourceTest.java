//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.auswertungen.TeilnehmerBuilder;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicKlassenstufe;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * TeilnehmerResourceTest
 */
public class TeilnehmerResourceTest {

	private static final Principal PRINCIPAL = new EgladilPrincipal("gasgga");

	private static final String TEILNAHMEKUERZEL = "HGT5D212";

	private static final int JAHR = 2018;

	private TeilnehmerFacade teilnehmerFacade;

	private AuthorizationService authorizationService;

	private PublicTeilnehmer payload;

	private TeilnahmeIdentifier teilnahmeIdentifier;

	@BeforeEach
	void setUp() {
		authorizationService = Mockito.mock(AuthorizationService.class);
		Mockito.when(authorizationService.authorizeForTeilnahme(PRINCIPAL.getName(), teilnahmeIdentifier)).thenReturn("");

		teilnehmerFacade = Mockito.mock(TeilnehmerFacade.class);
		payload = new PublicTeilnehmer();
		payload.setNachname("Schlemmer");
		payload.setKlassenstufe(PublicKlassenstufe.fromKlassenstufe(Klassenstufe.EINS));
		payload.setVorname("Hannelore");

		teilnahmeIdentifier = TeilnahmeIdentifier.createPrivatteilnahmeIdentifier(TEILNAHMEKUERZEL, "" + JAHR);
	}

	@Test
	@DisplayName("pathparameter werden validiert: teilnahmekuerzel evil")
	void createWithInvalidPathParameters() {
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);
		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", "../etc/", payload);
		assertEquals(902, response.getStatus());
	}

	@Test
	@DisplayName("payload wird validiert: teilnahmekuerzel evil")
	void createWithInvalidPayload() {
		payload.setVorname(" ");
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);

		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", TEILNAHMEKUERZEL, payload);

		assertEquals(902, response.getStatus());
		final APIResponsePayload entity = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), entity.getApiMessage().getLevel());
		assertEquals("Die Eingaben sind nicht korrekt.", entity.getApiMessage().getMessage());
	}

	@Test
	@DisplayName("create fails wenn dublette")
	void createDuplicate() {
		final EgladilDuplicateEntryException ex = new EgladilDuplicateEntryException("gibts schon");
		ex.setUniqueIndexName("uk_teilnehmer_2");
		Mockito.when(teilnehmerFacade.createTeilnehmer(PRINCIPAL.getName(), teilnahmeIdentifier, payload)).thenThrow(ex);
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);

		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", TEILNAHMEKUERZEL, payload);

		assertEquals(900, response.getStatus());
		final APIResponsePayload entity = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), entity.getApiMessage().getLevel());
		assertEquals(
			"Sie haben bereits ein Kind mit gleichem Vornamen, gleichem Nachnamen und gleicher Klassesnstufe erfasst. Falls das richtig ist, verwenden Sie bitte den Zusatz zur Unterscheidung.",
			entity.getApiMessage().getMessage());
	}

	@Test
	@DisplayName("IllegalArgumentException führt zu internal server error")
	void createIllegalArgumentException() {
		final IllegalArgumentException ex = new IllegalArgumentException("gsagd");
		Mockito.when(teilnehmerFacade.createTeilnehmer(PRINCIPAL.getName(), teilnahmeIdentifier, payload)).thenThrow(ex);
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);

		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", TEILNAHMEKUERZEL, payload);

		assertEquals(500, response.getStatus());
		final APIResponsePayload entity = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), entity.getApiMessage().getLevel());
		assertEquals("Es ist ein interner Serverfehler aufgetreten.", entity.getApiMessage().getMessage());
	}

	@Test
	@DisplayName("PreconditionFailedException führt zu internal server error")
	void createPreconditionsFailed() {
		final PreconditionFailedException ex = new PreconditionFailedException("gsagd");
		Mockito.when(teilnehmerFacade.createTeilnehmer(PRINCIPAL.getName(), teilnahmeIdentifier, payload)).thenThrow(ex);
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);

		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", TEILNAHMEKUERZEL, payload);

		assertEquals(412, response.getStatus());
		final APIResponsePayload entity = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), entity.getApiMessage().getLevel());
		assertEquals("Für diese Aktion haben Sie keine Berechtigung. Bitte senden Sie eine Mail.",
			entity.getApiMessage().getMessage());
	}

	@Test
	@DisplayName("testen Response bei 201")
	void createKlappt() throws Exception {
		final Teilnehmer teilnehmer = new TeilnehmerBuilder(teilnahmeIdentifier, Klassenstufe.EINS).vorname("Hannelore")
			.nachname("Schlemmer").checkAndBuild();
		final String teilnehmerkuerzel = teilnehmer.getKuerzel();
		Mockito.when(teilnehmerFacade.createTeilnehmer(PRINCIPAL.getName(), teilnahmeIdentifier, payload)).thenReturn(teilnehmer);
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);

		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", TEILNAHMEKUERZEL, payload);

		assertEquals(201, response.getStatus());

		final APIResponsePayload entity = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.INFO.toString(), entity.getApiMessage().getLevel());

		// assertEquals(1, entity.getHypermediaLinks().size());
		// final HateoasLink link = entity.getHypermediaLinks().get(0);
		// // assertEquals(teilnehmerkuerzel + "/loesungszettel", link.getHref());
		// assertEquals("teilnehmer", link.getRel());
		// assertEquals("GET", link.getMethod());
		// assertNull(link.getType());

		final PublicTeilnehmer pt = (PublicTeilnehmer) entity.getPayload();
		assertEquals(teilnehmerkuerzel, pt.getKuerzel());

		System.out.println(new ObjectMapper().writeValueAsString(entity));
	}

	@Test
	@DisplayName("EgladilAuthorizationException")
	void createNotAuthorized() {
		Mockito.when(authorizationService.authorizeForTeilnahme(PRINCIPAL.getName(), teilnahmeIdentifier)).thenThrow(new EgladilAuthorizationException("falsche Teilnahme"));

		final PreconditionFailedException ex = new PreconditionFailedException("gsagd");
		Mockito.when(teilnehmerFacade.createTeilnehmer(PRINCIPAL.getName(), teilnahmeIdentifier, payload)).thenThrow(ex);
		final TeilnehmerResource resource = new TeilnehmerResource(teilnehmerFacade, authorizationService);

		final Response response = resource.createTeilnehmer(PRINCIPAL, "2018", "P", TEILNAHMEKUERZEL, payload);

		assertEquals(401, response.getStatus());
		final APIResponsePayload entity = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), entity.getApiMessage().getLevel());
		assertEquals("Sie sind nicht autorisiert.",
			entity.getApiMessage().getMessage());
	}
}
