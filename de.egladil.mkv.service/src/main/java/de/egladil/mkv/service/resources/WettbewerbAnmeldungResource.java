//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.SessionToken;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.benutzer.SessionDelegate;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * WettbewerbAnmeldungResource
 */
@Singleton
@Path("/teilnahmen")
public class WettbewerbAnmeldungResource {

	private static final Logger LOG = LoggerFactory.getLogger(WettbewerbAnmeldungResource.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final IWettbewerbsanmeldungService wettbewerbsanmeldungService;

	private final SessionDelegate sessionDelegate;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * Erzeugt eine Instanz von WettbewerbAnmeldungResource
	 */
	@Inject
	public WettbewerbAnmeldungResource(final IWettbewerbsanmeldungService wettbewerbsanmeldungService,
		final SessionDelegate sessionDelegate) {
		this.wettbewerbsanmeldungService = wettbewerbsanmeldungService;
		this.sessionDelegate = sessionDelegate;
	}

	@OPTIONS
	@Path("/teilnahme")
	@Timed
	public Response optionsAnmelden() {
		return Response.ok().build();
	}

	/**
	 * Meldet die Person zum aktuellen Wettbewerbsjahr an.<br>
	 * <br>
	 * Response Statuscodes:
	 * <ul>
	 * <li>200: OK</li>
	 * <li>412: Wettbewerb noch nicht eröffnet</li>
	 * <li>500: interner Serverfehler</li>
	 * <li>900: unique index in DB verletzt</li>
	 * <li>901: konkurrierendes Update</li>
	 * </ul>
	 *
	 *
	 * @param principal
	 * @param wettbewerbsanmeldung
	 * @return Response mit Payload APIResponsePayload, APIMessage und AngemeldeterMKVBenutzer
	 */
	@POST
	@Path("/teilnahme")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Timed
	public Response zumWettbewerbAnmelden(@Auth final Principal principal, final Wettbewerbsanmeldung wettbewerbsanmeldung) {
		try {
			validationDelegate.check(wettbewerbsanmeldung, Wettbewerbsanmeldung.class);
			checkJahr(wettbewerbsanmeldung);
			final Role role = Role.valueOf(wettbewerbsanmeldung.getRolle());
			final String benutzerUuid = principal.getName();
			final MKVBenutzer angemeldeterMKVBenutzer = wettbewerbsanmeldungService
				.benutzerZumWettbewerbAnmelden(wettbewerbsanmeldung, benutzerUuid, role);
			final Optional<AccessToken> optToken = sessionDelegate.findAccessToken(benutzerUuid);

			final APIResponsePayload payload = new APIResponsePayload(
				APIMessage.info(applicationMessages.getString("teilnahme.anmeldung.success")), angemeldeterMKVBenutzer);
			if (optToken.isPresent()) {
				final AccessToken accessToken = optToken.get();
				payload.setSessionToken(new SessionToken(accessToken.getAccessTokenId(), accessToken.getCsrfToken()));
			}
			return Response.ok().entity(payload).build();
		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, wettbewerbsanmeldung);
		} catch (final EgladilConcurrentModificationException e) {
			// Das sollte nicht vorkommen, weil es sich um eine Registrierung handelt. Dann müsste der Anwender die
			// Daten nochmals senden.
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Konkurrierendes Update beim Anlegen eines MKV-Kontos.");
			return exceptionHandler.mapToMKVApiResponse(e, "profil.concurrent", wettbewerbsanmeldung);
		} catch (final PreconditionFailedException e) {
			// ist schon geloggt
			final APIResponsePayload payload = new APIResponsePayload(APIMessage.error(e.getMessage()));
			return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(payload).build();
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception bei der Wettewerbsanmeldung: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, wettbewerbsanmeldung);
		}
	}

	/**
	 * Prüft das gelieferte Jahr gegen das auf dem Server konfigurierte.
	 *
	 * @param wettbewerbsanmeldung Wettbewerbsanmeldung.
	 */
	private void checkJahr(final Wettbewerbsanmeldung wettbewerbsanmeldung)
		throws EgladilConfigurationException, PreconditionFailedException {
		final Kontext kontext = KontextReader.getInstance().getKontext();
		final String aktuellesWettbewerbsjahr = kontext.getWettbewerbsjahr();
		if (!aktuellesWettbewerbsjahr.equals(wettbewerbsanmeldung.getJahr())) {
			LOG.warn(
				"/teilnahmen/jahr wurde mit falschem Parameter {} aufgerufen. Konfigurationen pruefen: wettbewerbsjahr server = {}",
				wettbewerbsanmeldung.getJahr(), aktuellesWettbewerbsjahr);
			throw new EgladilConfigurationException("aktuelles Wettbewerbsjahr und jahr vom Client stimmen nicht überein");
		}
		if (!kontext.isNeuanmeldungFreigegeben()) {
			LOG.warn(
				"/teilnahmen/jahr wurde aufgerufen, obwohl das Wettbewerbsjahr noch nicht eröffnet ist: aktuelles Wettbewerbsjahr {}. Konfiguration auf dem Client prüfen!",
				aktuellesWettbewerbsjahr);
			final String msg = MessageFormat.format(applicationMessages.getString("teilnahme.geschlossenerWerbewerb"),
				new Object[] { aktuellesWettbewerbsjahr });
			throw new PreconditionFailedException(msg);
		}
	}
}
