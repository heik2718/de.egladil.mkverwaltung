//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.egladil.bv.aas.domain.Role;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.mkv.persistence.domain.kataloge.Schule;

/**
 * Daten für eine Registrierung zu eines Lehrers mit einer Schule bei der Minikänguruverwaltung.<br>
 * <br>
 * JSON:
 * {"type":"schule","clientId":"MKV","vorname":"Hannelore","nachname":"Kuckuck","email":"mail@provider.de","passwort":"Qwertz!2","passwortWdh":"Qwertz!2","kleber":null,"schulkuerzel":"Z45FG9IM"}
 */
public class Lehrerregistrierung extends AbstractMKVRegistrierung {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Kuerzel
	@Size(min = 8, max = 8)
	private String schulkuerzel;

	@JsonIgnore
	private Schule schule;

	/**
	 * Erzeugt eine Instanz von Schulregistrierungsanfrage
	 */
	public Lehrerregistrierung() {
	}

	/**
	 * Erzeugt eine Instanz von Schulregistrierungsanfrage
	 */
	public Lehrerregistrierung(final String vorname, final String nachname, final String email, final String passwort,
		final String passwortWdh, final String kleber) {
		super(vorname, nachname, email, passwort, passwortWdh, kleber, true);
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	@Override
	public String toString() {
		final String superLog = super.toString();
		return superLog + " [schulkuerzel=" + schulkuerzel + "]";
	}

	/**
	 * Liefert die Membervariable schulkuerzel
	 *
	 * @return die Membervariable schulkuerzel
	 */
	public String getSchulkuerzel() {
		return schulkuerzel;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param schulkuerzel neuer Wert der Membervariablen schulkuerzel
	 */
	public void setSchulkuerzel(final String schulkuerzel) {
		this.schulkuerzel = schulkuerzel;
	}

	/**
	 * Liefert die Membervariable schule
	 *
	 * @return die Membervariable schule
	 */
	public Schule getSchule() {
		return schule;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param schule neuer Wert der Membervariablen schule
	 */
	public void setSchule(final Schule schule) {
		this.schule = schule;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getRole()
	 */
	@Override
	@JsonIgnore
	public Role getRole() {
		return Role.MKV_LEHRER;
	}
}
