//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.utils;

import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.mkv.service.payload.request.IMKVRegistrierung;

/**
 * PayloadMapper
 */
public final class PayloadMapper {
	/**
	 * Mapping-Methode.
	 *
	 * @param payload
	 * @return
	 */
	public static Registrierungsanfrage toRegistrierungsanfrage(IMKVRegistrierung payload) {
		Registrierungsanfrage registrierungsanfrage = new Registrierungsanfrage();
		registrierungsanfrage.setAnwendung(Anwendung.MKV);
		registrierungsanfrage.setEmail(payload.getEmail());
		registrierungsanfrage.setLoginName(payload.getEmail());
		registrierungsanfrage.setPasswort(payload.getPasswort());
		registrierungsanfrage.setRole(payload.getRole());
		return registrierungsanfrage;
	}
}
