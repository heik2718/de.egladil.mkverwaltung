//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

import java.util.Comparator;

import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.service.payload.response.DateiInfo;

/**
 * DateiInfoKategorieKlasseComparator führt zur Sortierung nach Dateikategorie, dann Klassenstufe, dann Dateiname.
 */
public class DateiInfoKategorieKlasseComparator implements Comparator<DateiInfo> {

	/**
	 * DateiInfoKategorieKlasseComparator
	 */
	public DateiInfoKategorieKlasseComparator() {
	}

	@Override
	public int compare(final DateiInfo arg0, final DateiInfo arg1) {
		if (arg0 == null) {
			return -1;
		}
		if (arg1 == null) {
			return 1;
		}
		final DateiKategorie kat0 = arg0.getMyKategorie();
		final DateiKategorie kat1 = arg1.getMyKategorie();
		if (kat0 == kat1) {
			final Klassenstufe klasse0 = arg0.getMyKlasse();
			final Klassenstufe klasse1 = arg1.getMyKlasse();

			if (klasse0 == klasse1) {
				return arg0.getDateiname().compareToIgnoreCase(arg1.getDateiname());
			} else {
				if (klasse0 == null) {
					return 1;
				}
				if (klasse1 == null) {
					return -1;
				}
				return klasse0.getNummer() - klasse1.getNummer();
			}
		} else {
			return kat0.getSortnummer() - kat1.getSortnummer();
		}
	}
}
