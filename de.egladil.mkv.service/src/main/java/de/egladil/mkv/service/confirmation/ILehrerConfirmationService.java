//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation;

import de.egladil.mkv.service.confirmation.impl.IConfirmationService;

/**
 * ILehrerConfirmationService. Markerinterface für Guice
 */
public interface ILehrerConfirmationService extends IConfirmationService {

}
