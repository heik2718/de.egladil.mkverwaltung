//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import de.egladil.mkv.auswertungen.urkunden.PdfMerger;
import de.egladil.mkv.auswertungen.urkunden.UebersichtFontProvider;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * AdvVereinbarungPdfGenerator
 */
public class AdvVereinbarungPdfGenerator {

	private final UebersichtFontProvider fontProvider;

	private final AdvVereinbarung vereinbarung;

	private final String downloadPath;

	/**
	 * AdvVereinbarungPdfGenerator
	 */
	public AdvVereinbarungPdfGenerator(final AdvVereinbarung vereinbarung, final String downloadPath) {

		if (vereinbarung == null) {
			throw new IllegalArgumentException("vereinbarung null");
		}
		this.vereinbarung = vereinbarung;
		this.downloadPath = downloadPath;
		this.fontProvider = new UebersichtFontProvider();
	}

	public byte[] generatePdf() {

		final String dateiname = vereinbarung.getAdvText().getDateiname();
		final String pathFile = downloadPath + File.separator + dateiname;

		try (FileInputStream fis = new FileInputStream(new File(pathFile));
			ByteArrayOutputStream os = new ByteArrayOutputStream()) {

			IOUtils.copy(fis, os);

			final byte[] deckblatt = generiereDeckblatt();
			final byte[] pdfAllgemein = os.toByteArray();

			final List<byte[]> seiten = new ArrayList<>();
			seiten.add(deckblatt);
			seiten.add(pdfAllgemein);

			final byte[] pdfs = new PdfMerger().concatPdf(seiten);

			return pdfs;
		} catch (final IOException e) {
			final String msg = "Datei nicht gefunden oder keine Berechtigung - " + e.getMessage();
			throw new MKVException(msg);
		}
	}

	private byte[] generiereDeckblatt() {
		final Document doc = new Document(PageSize.A4);
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

			PdfWriter.getInstance(doc, out);
			doc.open();

			Font font = fontProvider.getFontBold(14);
			Paragraph element = new Paragraph("Vereinbarung zur Auftragsverarbeitung", font);
			element.setAlignment(Element.ALIGN_CENTER);
			doc.add(element);

			font = fontProvider.getFontBold(12);
			element = new Paragraph("nach Art. 28 Abs. 3 Datenschutz-Grundverordnung (DSGVO)", font);
			element.setAlignment(Element.ALIGN_CENTER);
			doc.add(element);

			font = fontProvider.getFontNormal();

			doc.add(new Paragraph("Version " + vereinbarung.getAdvText().getVersionsnummer(), font));

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph("zwischen", font));

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph(vereinbarung.getSchulname(), font));
			doc.add(new Paragraph(vereinbarung.getStrasse() + " " + vereinbarung.getHausnummer(), font));
			if ("de".equals(vereinbarung.getLaendercode())) {
				doc.add(new Paragraph(vereinbarung.getPlz() + " " + vereinbarung.getOrt(), font));

			} else {
				doc.add(
					new Paragraph(vereinbarung.getLaendercode() + "-" + vereinbarung.getPlz() + " " + vereinbarung.getOrt(), font));
			}

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph("als Auftraggeber - nachfolgend Auftraggeber -", font));

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph("und", font));

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph("Heike Winkelvoß", font));
			doc.add(new Paragraph("Schultheißweg 25", font));
			doc.add(new Paragraph("55252 Mainz-Kastel", font));

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph("als Auftragnehmer- nachfolgend Auftragnehmer -", font));

			doc.add(Chunk.NEWLINE);

			doc.add(new Paragraph("abgeschlossen am " + vereinbarung.getZugestimmtAmDruck(), font));

			doc.close();
			return out.toByteArray();
		} catch (final IOException e) {
			throw new MKVException("konnte keinen ByteArrayOutputStream erzeugen: " + e.getMessage(), e);
		} catch (final DocumentException e) {
			throw new MKVException("konnte keinen PdfWriter erzeugen: " + e.getMessage(), e);
		}
	}
}
