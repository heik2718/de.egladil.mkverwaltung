//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ILehrerteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.LehrerteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.anmeldung.impl.FindOrCreateSchulteilnahmeCommand;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.registrierung.ILehrerRegistrierungService;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;

/**
 * LehrerRegistrierungServiceImpl
 */
@Singleton
public class LehrerRegistrierungServiceImpl extends AbstractRegistrierungService<Lehrerregistrierung>
	implements ILehrerRegistrierungService {

	private static final Logger LOG = LoggerFactory.getLogger(LehrerRegistrierungServiceImpl.class);

	private final ISchuleDao schuleDao;

	private final ILehrerkontoDao lehrerkontoDao;

	private final ISchulteilnahmeDao schulteilnahmeDao;

	private final IAnonymisierungsservice anonymisierungsservice;

	private final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao;

	private final IProtokollService protokollService;

	private final AuswertungsgruppenService auswertungsgruppenService;

	private final UniqueIndexNameMapper uniqueIndexNameMapper;

	/**
	 * Erzeugt eine Instanz von NeuerLehreranmeldungService
	 *
	 */
	@Inject
	public LehrerRegistrierungServiceImpl(final IBenutzerService benutzerService, final MailDelegate mailDelegate,
		final BenutzerkontoDelegate benutzerkontoDelegate, final ISchuleDao schuleDao, final ISchulteilnahmeDao schulteilnahmeDao,
		final ILehrerkontoDao lehrerkontoDao, final ILehrerteilnahmeInfoDao lehrerteilnahmeInfoDao,
		final IAnonymisierungsservice anonymisierungsservice, final IProtokollService protokollService,
		final IAccessTokenDAO accessTokenDAO, final AuswertungsgruppenService auswertungsgruppenService) {

		super(benutzerService, mailDelegate, benutzerkontoDelegate, anonymisierungsservice, accessTokenDAO);
		this.schuleDao = schuleDao;
		this.lehrerkontoDao = lehrerkontoDao;
		this.schulteilnahmeDao = schulteilnahmeDao;
		this.anonymisierungsservice = anonymisierungsservice;
		this.lehrerteilnahmeInfoDao = lehrerteilnahmeInfoDao;
		this.protokollService = protokollService;
		this.auswertungsgruppenService = auswertungsgruppenService;
		this.uniqueIndexNameMapper = new UniqueIndexNameMapper();
	}

	/**
	 * @see de.egladil.mkv.service.registrierung.impl.AbstractRegistrierungService#getRole()
	 */
	@Override
	protected Role getRole() {
		return Role.MKV_LEHRER;
	}

	/**
	 * @see de.egladil.mkv.service.registrierung.impl.AbstractRegistrierungService#mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(de.egladil.mkv.service.domain.requestentities.IMKVRegistrierung,
	 * Aktivierungsdaten, String)
	 */
	@Override
	protected IMKVKonto mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(final Lehrerregistrierung anfrage,
		final Aktivierungsdaten aktivierungsdaten, final String jahr, final boolean neuanmeldungFreigeschaltet) {
		Schulteilnahme schulteilnahme = null;
		try {
			final Schule schule = anfrage.getSchule();
			if (schule == null) {
				throw new MKVException(
					"Fehler im Ablauf: schule sollte bereits durch kontaktAnlegenUndZumWettbewerbAnmelden() gesetzt worden sein");
			}
			Lehrerkonto lehrerkonto = null;
			if (anfrage.isGleichAnmelden() && neuanmeldungFreigeschaltet) {
				final FindOrCreateSchulteilnahmeCommand command = new FindOrCreateSchulteilnahmeCommand(schulteilnahmeDao);
				schulteilnahme = command.findOrCreateSchulteilnahme(schule.getKuerzel(), jahr);
				lehrerkonto = createTransientLehrerkonto(aktivierungsdaten.getBenutzerkonto().getUuid(), anfrage, schulteilnahme,
					schule);

				this.auswertungsgruppenService.findOrCreateRootAuswertungsgruppe(schulteilnahme.provideTeilnahmeIdentifier());

			} else {
				lehrerkonto = createTransientLehrerkonto(aktivierungsdaten.getBenutzerkonto().getUuid(), anfrage, schule);
			}
			final IMKVKonto result = lehrerkontoDao.persist(lehrerkonto);
			final String classpathMailtemplate = new CreateRegistrierungSuccessMessageCommand(neuanmeldungFreigeschaltet, jahr)
				.getMailtemplatePath(anfrage.isGleichAnmelden());

			getMailDelegate().schulregistrierungsmailVersenden(aktivierungsdaten, jahr, classpathMailtemplate);
			return result;
		} catch (final EgladilDuplicateEntryException e) {
			throw ExceptionFactory.duplicateEntry(e.getUniqueIndexName(), uniqueIndexNameMapper.exceptionToMessage(e));
		} catch (PersistenceException | EgladilStorageException e) {
			benutzerkontoAnonymisieren(aktivierungsdaten.getBenutzerkonto());
			if (schulteilnahme != null) {
				schulteilnahmeLoeschen(schulteilnahme, jahr);
			}
			throw ExceptionFactory.internalServerErrorException();
		} catch (InvalidMailAddressException | EgladilMailException e) {
			nichtAbgeschlosseneRegistrierungWegraeumen(aktivierungsdaten);
			if (schulteilnahme != null) {
				schulteilnahmeLoeschen(schulteilnahme, jahr);
			}
			throw e;
		}
	}

	private void schulteilnahmeLoeschen(final Schulteilnahme schulteilnahme, final String jahr) {
		final List<LehrerteilnahmeInformation> teilnahmenMitSchule = lehrerteilnahmeInfoDao
			.findAlleMitSchulkuerzel(schulteilnahme.getKuerzel());
		if (teilnahmenMitSchule.isEmpty()) {
			final Optional<Schulteilnahme> optTeilnahme = schulteilnahmeDao
				.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(schulteilnahme.getKuerzel(), jahr));
			if (optTeilnahme.isPresent()) {
				try {
					schulteilnahmeDao.delete(schulteilnahme);
					final Ereignis ereignis = new Ereignis(Ereignisart.SCHULTEILNAHME_GELOESCHT.getKuerzel(),
						"Schulkuerzel = " + schulteilnahme.getKuerzel(),
						CONTEXT_MESSAGE_REGISTRIERUNG_GESCHEITERT_PERSIST_SCHULTEILNAHME);
					protokollService.protokollieren(ereignis);
				} catch (final Exception e) {
					LOG.error(GlobalConstants.LOG_PREFIX_DATENMUELL
						+ "Schulteilnahme wegen nicht beendeter Lehrerregistrierung - {}" + schulteilnahme);
				}
			}
		}
	}

	/**
	 *
	 * @param benutzerkonto
	 */
	private void benutzerkontoAnonymisieren(final Benutzerkonto benutzerkonto) {
		try {
			anonymisierungsservice.kontoAnonymsieren(benutzerkonto.getUuid(),
				CONTEXT_MESSAGE_REGISTRIERUNG_GESCHEITERT_PERSIST_LEHRER);
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DATENMUELL + "Anonymisieren des Benutzerkontos {}", benutzerkonto);
		}
	}

	/**
	 * Erzeugt ein korrekt verknüpftes Lehrerkonto mit der gegebenen Person und der persistenten Schulteilnahme. TODO
	 *
	 * @param anfrage Lehrerregistrierung
	 * @return Lehrerkonto
	 */
	Lehrerkonto createTransientLehrerkonto(final String uuid, final Lehrerregistrierung anfrage, final Schule schule) {
		final Person person = new Person(anfrage.getVorname(), anfrage.getNachname());
		final Lehrerkonto lehrerkonto = new Lehrerkonto(uuid, person, schule, anfrage.isAutomatischBenachrichtigen());
		return lehrerkonto;
	}

	/**
	 * Erzeugt ein korrekt verknüpftes Lehrerkonto mit der gegebenen Person und der persistenten Schulteilnahme. TODO
	 *
	 * @param anfrage
	 * @param schulteilnahme
	 * @return
	 */
	Lehrerkonto createTransientLehrerkonto(final String uuid, final Lehrerregistrierung anfrage,
		final Schulteilnahme schulteilnahme, final Schule schule) {
		final Person person = new Person(anfrage.getVorname(), anfrage.getNachname());
		final Lehrerkonto lehrerkonto = new Lehrerkonto(uuid, person, schule, anfrage.isAutomatischBenachrichtigen());
		lehrerkonto.addSchulteilnahme(schulteilnahme);
		return lehrerkonto;
	}

	/**
	 * @see de.egladil.mkv.service.registrierung.impl.AbstractRegistrierungService#kontaktAnlegenUndZumWettbewerbAnmelden(de.egladil.mkv.service.domain.requestentities.IMKVRegistrierung,
	 * java.lang.String, boolean)
	 */
	@Override
	public Aktivierungsdaten kontaktAnlegenUndZumWettbewerbAnmelden(final Lehrerregistrierung registrierungsanfrage,
		final String jahr, final boolean neuanmeldungFreieschaltet) throws EgladilDuplicateEntryException, MKVException {
		final Optional<Schule> opt = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			registrierungsanfrage.getSchulkuerzel());
		if (!opt.isPresent()) {
			throw new MKVException("Es gibt keine Schule mit dem kuerzel [" + registrierungsanfrage.getSchulkuerzel() + "]");
		}
		registrierungsanfrage.setSchule(opt.get());
		return super.kontaktAnlegenUndZumWettbewerbAnmelden(registrierungsanfrage, jahr, neuanmeldungFreieschaltet);
	}
}
