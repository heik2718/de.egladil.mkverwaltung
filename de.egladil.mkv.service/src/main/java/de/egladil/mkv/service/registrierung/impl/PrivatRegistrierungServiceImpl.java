//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import com.google.inject.Inject;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.Privatregistrierung;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;

/**
 * PrivatRegistrierungServiceImpl
 */
public class PrivatRegistrierungServiceImpl extends AbstractRegistrierungService<Privatregistrierung>
	implements IPrivatRegistrierungService {

	private final IPrivatkontoDao privatkontoDao;

	private final UniqueIndexNameMapper uniqueIndexNameMapper;

	/**
	 * Erzeugt eine Instanz von PrivatRegistrierungServiceImpl
	 *
	 * @param accessTokenDAO TODO
	 */
	@Inject
	public PrivatRegistrierungServiceImpl(final IBenutzerService benutzerService, final MailDelegate mailDelegate,
		final BenutzerkontoDelegate benutzerkontoDelegate, final IPrivatkontoDao privatkontoDao,
		final IAnonymisierungsservice anonymisierungsservice, final IAccessTokenDAO accessTokenDAO) {
		super(benutzerService, mailDelegate, benutzerkontoDelegate, anonymisierungsservice, accessTokenDAO);
		this.privatkontoDao = privatkontoDao;
		uniqueIndexNameMapper = new UniqueIndexNameMapper();
	}

	/**
	 * @see de.egladil.mkv.service.registrierung.impl.AbstractRegistrierungService#getRole()
	 */
	@Override
	protected Role getRole() {
		return Role.MKV_PRIVAT;
	}

	@Override
	protected IMKVKonto mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(final Privatregistrierung anfrage,
		final Aktivierungsdaten aktivierungsdaten, final String jahr, final boolean neuanmeldungFreigeschaltet) {
		try {
			final Person person = new Person(anfrage.getVorname(), anfrage.getNachname());
			Privatkonto privatkonto = null;
			if (anfrage.isGleichAnmelden() && neuanmeldungFreigeschaltet) {
				privatkonto = new Privatkonto(aktivierungsdaten.getBenutzerkonto().getUuid(), person,
					new Privatteilnahme(jahr, new KuerzelGenerator().generateDefaultKuerzel()),
					anfrage.isAutomatischBenachrichtigen());
			} else {
				privatkonto = new Privatkonto(aktivierungsdaten.getBenutzerkonto().getUuid(), person,
					anfrage.isAutomatischBenachrichtigen());
			}
			final IMKVKonto result = privatkontoDao.persist(privatkonto);

			final String classpathMailtemplate = new CreateRegistrierungSuccessMessageCommand(neuanmeldungFreigeschaltet, jahr)
				.getMailtemplatePath(anfrage.isGleichAnmelden());

			getMailDelegate().privatregistrierungsmailVersenden(aktivierungsdaten, jahr, classpathMailtemplate);
			return result;
		} catch (final EgladilDuplicateEntryException e) {
			throw ExceptionFactory.duplicateEntry(e.getUniqueIndexName(), uniqueIndexNameMapper.exceptionToMessage(e));
		} catch (InvalidMailAddressException | EgladilMailException e) {
			nichtAbgeschlosseneRegistrierungWegraeumen(aktivierungsdaten);
			throw e;
		}
	}
}
