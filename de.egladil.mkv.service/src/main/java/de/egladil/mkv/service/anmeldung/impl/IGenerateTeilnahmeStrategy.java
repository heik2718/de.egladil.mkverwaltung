//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import de.egladil.bv.aas.domain.Role;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.service.benutzer.LehrerkontoDelegate;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * IGenerateTeilnahmeStrategy
 */
public interface IGenerateTeilnahmeStrategy {

	/**
	 * Generiert und persistiert falls erforderlich die durch anmeldung definierte Teilnahme.
	 *
	 * @param anmeldung Wettbewerbsanmeldung die Daten der Wettbewerbsanmeldung.
	 * @param benutzerUUID String die UUID des Benutzers.
	 */
	TeilnahmeIdentifierProvider generateTeilnahme(final Wettbewerbsanmeldung anmeldung, String benutzerUUID);

	/**
	 * Gibt die passende Strategie zurück.
	 *
	 * @param lehrerkontoDao
	 * @param privatkontoDao
	 * @param role
	 * @return
	 */
	static IGenerateTeilnahmeStrategy newInstance(final LehrerkontoDelegate lehrerkontoDelegate,
		final IPrivatkontoDao privatkontoDao, final Role role) {
		switch (role) {
		case MKV_LEHRER:
			return new GenerateLehrerteilnahmeStrategy(lehrerkontoDelegate);
		case MKV_PRIVAT:
			return new GeneratePrivatteilnahmeStrategy(privatkontoDao);
		default:
			throw new MKVException("Teilnahme mit Role " + role + " nicht möglich.");
		}
	}

}
