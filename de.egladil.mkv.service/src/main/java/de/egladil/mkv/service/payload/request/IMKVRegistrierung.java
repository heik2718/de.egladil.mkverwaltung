package de.egladil.mkv.service.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.ILoggable;
import de.egladil.mkv.persistence.domain.user.Person;

public interface IMKVRegistrierung extends ILoggable {

	/**
	 * Liefert die Membervariable vorname
	 *
	 * @return die Membervariable vorname
	 */
	String getVorname();

	/**
	 * Liefert die Membervariable nachname
	 *
	 * @return die Membervariable nachname
	 */
	String getNachname();

	/**
	 * Liefert die Membervariable email
	 *
	 * @return die Membervariable email
	 */
	String getEmail();

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	String getKleber();

	/**
	 * Liefert die Membervariable passwort
	 *
	 * @return die Membervariable passwort
	 */
	String getPasswort();

	/**
	 * Liefert die Membervariable automatischBenachrichtigen
	 *
	 * @return die Membervariable automatischBenachrichtigen
	 */
	boolean isAutomatischBenachrichtigen();

	/**
	 *
	 * @return
	 */
	Role getRole();

	@JsonIgnore
	Person getKontaktdaten();
}