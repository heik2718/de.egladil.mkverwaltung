//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.IMKVRegistrierung;
import de.egladil.mkv.service.registrierung.IRegistrierungService;

/**
 * AnmeldungService
 */
public abstract class AbstractRegistrierungService<T extends IMKVRegistrierung> implements IRegistrierungService<T> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractRegistrierungService.class);

	private MailDelegate mailDelegate;

	private final IBenutzerService benutzerService;

	private final BenutzerkontoDelegate benutzerkontoDelegate;

	private final IAnonymisierungsservice anonymisierungsservice;

	private final IAccessTokenDAO accessTokenDAO;

	private boolean test;

	private String benutzerUuidImTest;

	/**
	 * Erzeugt eine Instanz von AnmeldungService
	 */
	public AbstractRegistrierungService(final IBenutzerService benutzerService, final MailDelegate mailDelegate,
		final BenutzerkontoDelegate benutzerkontoDelegate, final IAnonymisierungsservice anonymisierungsservice,
		final IAccessTokenDAO accessTokenDAO) {
		this.benutzerService = benutzerService;
		this.mailDelegate = mailDelegate;
		this.benutzerkontoDelegate = benutzerkontoDelegate;
		this.anonymisierungsservice = anonymisierungsservice;
		this.accessTokenDAO = accessTokenDAO;
	}

	/**
	 * @see de.egladil.mkv.service.registrierung.IRegistrierungService#kontaktAnlegenUndZumWettbewerbAnmelden(de.egladil.mkv.service.domain.requestentities.AbstractMKVRegistrierung,
	 * java.lang.String, boolean)
	 */
	@Override
	public Aktivierungsdaten kontaktAnlegenUndZumWettbewerbAnmelden(final T registrierungsanfrage, final String jahr,
		final boolean neuanmeldungFreieschaltet) throws ConstraintViolationException, IllegalArgumentException, EgladilBVException,
		EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException, EgladilMailException {
		final Aktivierungsdaten aktivierungsdaten = benutzerkontoDelegate.benutzerkontoAnlegen(registrierungsanfrage);
		if (this.isTest()) {
			setBenutzerUuidImTest(aktivierungsdaten.getBenutzerkonto().getUuid());
		}
		mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(registrierungsanfrage, aktivierungsdaten, jahr,
			neuanmeldungFreieschaltet);
		LOG.debug("MKV-Konto angelegt: Aktivierungsdaten = {}", aktivierungsdaten);
		return aktivierungsdaten;
	}

	protected abstract Role getRole();

	protected abstract IMKVKonto mkvKontoMitWettbewerbsanmeldungAnlegenSpeichernUndMailVersenden(T anfrage,
		Aktivierungsdaten aktivierungsdaten, String jahr, boolean neuanmeldungFreieschaltet);

	/**
	 * @see de.egladil.mkv.service.registrierung.IRegistrierungService#nichtAbgeschlosseneRegistrierungWegraeumen(de.egladil.bv.aas.domain.Aktivierungsdaten)
	 */
	@Override
	public void nichtAbgeschlosseneRegistrierungWegraeumen(final Aktivierungsdaten aktivierungsdaten) {
		final Benutzerkonto benutzerkonto = aktivierungsdaten.getBenutzerkonto();
		if (benutzerkonto != null) {
			anonymisierungsservice.kontoAnonymsieren(benutzerkonto.getUuid(), CONTEXT_MESSAGE);
		}
	}

	protected IBenutzerService getBenutzerService() {
		return benutzerService;
	}

	protected BenutzerkontoDelegate getBenutzerkontoDelegate() {
		return benutzerkontoDelegate;
	}

	protected MailDelegate getMailDelegate() {
		return mailDelegate;
	}

	protected boolean isTest() {
		return test;
	}

	public void setTest(final boolean test) {
		this.test = test;
	}

	public String getBenutzerUuidImTest() {
		return benutzerUuidImTest;
	}

	protected void setBenutzerUuidImTest(final String benutzerUuidImTest) {
		this.benutzerUuidImTest = benutzerUuidImTest;
	}

	/**
	 * Darf nur für Tests verwendet werden!
	 *
	 * @param mailDelegate
	 */
	@Deprecated // nur für Tests verwenden!
	public void setMailDelegate(final MailDelegate mailDelegate) {
		this.mailDelegate = mailDelegate;
	}

	protected IAccessTokenDAO getAccessTokenDao() {
		return accessTokenDAO;
	}
}
