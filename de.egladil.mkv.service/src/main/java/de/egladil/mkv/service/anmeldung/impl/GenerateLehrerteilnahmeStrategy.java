//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.service.benutzer.LehrerkontoDelegate;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * GenerateLehrerteilnahmeStrategy
 */
public class GenerateLehrerteilnahmeStrategy implements IGenerateTeilnahmeStrategy {

	private static final Logger LOG = LoggerFactory.getLogger(GenerateLehrerteilnahmeStrategy.class);

	private final LehrerkontoDelegate lehrerkontoDelegate;

	/**
	 * Erzeugt eine Instanz von GenerateLehrerteilnahmeStrategy
	 */
	public GenerateLehrerteilnahmeStrategy(final LehrerkontoDelegate lehrerkontoDelegate) {
		if (lehrerkontoDelegate == null) {
			throw new IllegalArgumentException("lehrerkontoDelegate null");
		}
		this.lehrerkontoDelegate = lehrerkontoDelegate;
	}

	/**
	 * @see de.egladil.mkv.service.anmeldung.impl.IGenerateTeilnahmeStrategy#generateTeilnahme(de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung,
	 * String)
	 */
	@Override
	public TeilnahmeIdentifierProvider generateTeilnahme(final Wettbewerbsanmeldung anmeldung, final String benutzerUUID) {
		final Optional<Lehrerkonto> opt = lehrerkontoDelegate.findByUUID(benutzerUUID);
		if (!opt.isPresent()) {
			throw new MKVException("Lehrerkonto mit UUID [" + benutzerUUID + "] nicht gefunden");
		}
		final Lehrerkonto lehrerkonto = opt.get();

		final TeilnahmeIdentifierProvider vorhandene = lehrerkonto.getTeilnahmeZuJahr(anmeldung.getJahr());
		if (vorhandene != null) {
			LOG.debug("Lehrer bereits angemeldet: {}", lehrerkonto);
			return null;
		}

		if (anmeldung.getSchulkuerzel() != null) {
			final String schulkuerzelNeu = anmeldung.getSchulkuerzel();
			final Optional<Schule> optNeu = lehrerkontoDelegate.getSchuleMitKuerzel(schulkuerzelNeu);
			if (!optNeu.isPresent()) {
				throw new ResourceNotFoundException("neue Schule mit [kuerzel=" + schulkuerzelNeu + "] existriert nicht");
			}
			final Schule neueSchule = optNeu.get();
			lehrerkonto.setSchule(neueSchule);
		}

		final FindOrCreateSchulteilnahmeCommand command = new FindOrCreateSchulteilnahmeCommand(
			lehrerkontoDelegate.getSchulteilnahmeDao());
		final Schulteilnahme schulteilnahme = command.findOrCreateSchulteilnahme(lehrerkonto.getSchule().getKuerzel(),
			anmeldung.getJahr());
		// final Lehrerteilnahme lehrerteilnahme = new Lehrerteilnahme(schulteilnahme);
		lehrerkonto.addSchulteilnahme(schulteilnahme);
		lehrerkonto.setAutomatischBenachrichtigen(anmeldung.isBenachrichtigen());
		final IMKVKonto result = lehrerkontoDelegate.kontoSpeichern(lehrerkonto);
		LOG.debug("Konto {} zum Wettbewerb angemeldet: ", result);
		return schulteilnahme;
	}
}
