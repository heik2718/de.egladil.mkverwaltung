//=====================================================
// Project: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.mkv.service.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Singleton;

import de.egladil.mkv.persistence.payload.request.LogEntry;
import de.egladil.mkv.persistence.utils.LogDelegate;

/**
* LogResource
*/
@Singleton
@Path("/log")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LogResource {

	private static final Logger LOG = LoggerFactory.getLogger(LogResource.class);

	@POST
	public Response log(final LogEntry logEntry) {

		new LogDelegate().log(logEntry, LOG, "mkv");

		return Response.ok().build();

	}

}
