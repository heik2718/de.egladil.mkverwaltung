//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.service.payload.request.IMKVRegistrierung;

/**
 * IRegistrierungService ist zuständig für die Registrierung eines Benutzerkontos.
 */
public interface IRegistrierungService<T extends IMKVRegistrierung> {

	String CONTEXT_MESSAGE = "Mailversand bei Registrierung";

	/**
	 * Erzeut ein Benutzerkonto, ein Privat- oder Schulkonto sowie eine Wettbewerbsteilnahme für das gegebene Jahr.
	 * Versendet eine Registrierungsmail an die zum Benutzer gehörende Mailadresse.
	 *
	 * @param registrierungsanfrage
	 * @param jahr
	 * @param neuanmeldungFreieschaltet boolean
	 * @return Aktivierungsdaten
	 */
	Aktivierungsdaten kontaktAnlegenUndZumWettbewerbAnmelden(T registrierungsanfrage, String jahr, boolean neuanmeldungFreieschaltet)
		throws EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException,
		EgladilMailException;

	/**
	 * Kann aufgerufen werden, wenn die Registrierung nicht erfolgreich beendet werden konnte, weil z.B. die Mail nicht
	 * versendet wurde. Das neu angelegte Benutzerkonto und das Lehrer- bzw. Privatkonto werden anonymisiert.
	 *
	 * @param aktivierungsdaten
	 */
	void nichtAbgeschlosseneRegistrierungWegraeumen(Aktivierungsdaten aktivierungsdaten) throws EgladilStorageException;
}
