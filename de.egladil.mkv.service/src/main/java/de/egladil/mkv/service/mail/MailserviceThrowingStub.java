//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.mail;

import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.IMailservice;

/**
 * MailserviceStub
 */
public class MailserviceThrowingStub implements IMailservice {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private boolean sendMailCalled;

	private RuntimeException exceptionToThrow;

	/**
	 * Erzeugt eine Instanz von MailserviceThrowingStub
	 */
	public MailserviceThrowingStub() {
		exceptionToThrow = new EgladilMailException("Das ist eine Exception für den Test.");
	}

	/**
	 * @param throwException
	 */
	public MailserviceThrowingStub(RuntimeException exceptionToThrow) {
		sendMailCalled = false;
		this.exceptionToThrow = exceptionToThrow;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.email.service.IMailservice#sendMail(de.egladil.email.service.IEmailDaten)
	 */
	@Override
	public boolean sendMail(IEmailDaten maildaten) throws EgladilConfigurationException {
		sendMailCalled = true;
		throw exceptionToThrow;
	}

	/**
	 * Liefert die Membervariable sendMailCalled
	 *
	 * @return die Membervariable sendMailCalled
	 */
	public boolean isSendMailCalled() {
		return sendMailCalled;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param exceptionToThrow neuer Wert der Membervariablen exceptionToThrow
	 */
	public void setExceptionToThrow(RuntimeException exceptionToThrow) {
		this.exceptionToThrow = exceptionToThrow;
	}
}
