//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

/**
 * ConfirmationStatus
 */
public enum ConfirmationStatus {

	expiredActivation,
	normalActivation,
	repeatedActivation;
}
