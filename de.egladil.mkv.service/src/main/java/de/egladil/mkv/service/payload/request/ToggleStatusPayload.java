//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import java.io.Serializable;

/**
 * Payload, der einen zweiwertigen Status ändert. Beispiel: stornieren / reaktivieren, benachrichtigen / nicht
 * benachrichtigen, aktivieren / deaktivieren.
 */
public class ToggleStatusPayload implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private boolean aktiv;

	/**
	 * Liefert die Membervariable aktiv
	 *
	 * @return die Membervariable aktiv
	 */
	public boolean isAktiv() {
		return aktiv;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param aktiv neuer Wert der Membervariablen aktiv
	 */
	public void setAktiv(boolean aktiv) {
		this.aktiv = aktiv;
	}

}
