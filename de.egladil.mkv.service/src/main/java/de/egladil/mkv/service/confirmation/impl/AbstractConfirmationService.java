//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.UniqueIdentifier;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;

/**
 * SchuleConfirmationServiceImpl
 */
public abstract class AbstractConfirmationService implements IConfirmationService {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractConfirmationService.class);

	private final IRegistrierungService registrierungService;

	private final IBenutzerService benutzerService;

	private final IAnonymisierungsservice anonymsierungsservice;

	/**
	 * Erzeugt eine Instanz von SchuleConfirmationServiceImpl
	 */
	public AbstractConfirmationService(final IRegistrierungService registrierungService, final IBenutzerService benutzerService,
		final IAnonymisierungsservice anonymsierungsservice) {
		this.registrierungService = registrierungService;
		this.benutzerService = benutzerService;
		this.anonymsierungsservice = anonymsierungsservice;
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IConfirmationService#jetztRegistrierungBestaetigen(java.lang.String,
	 * String)
	 */
	@Override
	public ConfirmationStatus jetztRegistrierungBestaetigen(final String confirmationCode, final String jahr)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException {
		if (StringUtils.isBlank(jahr)) {
			throw ExceptionFactory.internalServerErrorException(LOG, null, "jahr muss gesetzt sein");
		}
		return registrierungBestaetigen(confirmationCode, new Date(), jahr);
	}

	/**
	 * Zu Testzwecken kann ein Date mitgegeben werden
	 *
	 * @param confirmationCode
	 * @param now
	 * @return
	 */
	ConfirmationStatus registrierungBestaetigen(final String confirmationCode, final Date now, final String jahr)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException {
		if (now == null) {
			throw ExceptionFactory.internalServerErrorException(LOG, null,
				"registrierungBestaetigen(): Parameter now darf nicht null sein");
		}
		final Aktivierungsdaten regBest = registrierungService.findByConfirmationCode(new UniqueIdentifier(confirmationCode));
		if (regBest == null) {
			throw ExceptionFactory.resourceNotFound(LOG, "registrierungBestaetigen(): " + confirmationCode);
		}
		// final IKontaktDao<IKontakt> kontaktDao = getKontaktDao();
		final Benutzerkonto benutzer = regBest.getBenutzerkonto();
		final boolean expired = regBest.isExpired(now);
		LOG.debug("confirmCode expired: {}, {}", expired, benutzer);
		final Optional<IMKVKonto> opt = findKontaktByUUID(benutzer.getUuid());
		IConfirmationStrategy strategy = null;
		if (!opt.isPresent()) {
			strategy = this.getConfirmationStrategy(true, false);
		} else {
			strategy = this.getConfirmationStrategy(expired, benutzer.isAktiviert());
		}

		final ConfirmationStatus result = strategy.applyOnRegistration(regBest, jahr, opt, getKontaktUndTeilnahmeCallback(),
			anonymsierungsservice);
		return result;
	}

	protected abstract Optional<IMKVKonto> findKontaktByUUID(String uuid);

	protected abstract IKontaktUndTeilnahmeCallback getKontaktUndTeilnahmeCallback();

	/**
	 * Ermittelt anhand des Aktivierungsstatus des kontaktobjekts und den Aktivierungsdaten die Strategie.
	 *
	 * @param regBest
	 * @param kontaktobjekt
	 * @return
	 */
	IConfirmationStrategy getConfirmationStrategy(final boolean expired, final boolean aktiviert) {
		if (aktiviert) {
			return new RepeatedActivationStrategy();
		}
		return expired ? new ExpiredActivationStrategy(registrierungService, benutzerService)
			: new NormalActivationStrategy(registrierungService);
	}
}
