//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.config;

import io.dropwizard.Configuration;

/**
 * MKVServiceConfiguration
 */
public class MKVServiceConfiguration extends Configuration {

	private boolean test;

	private String configRoot;

	private String allowedOrigins;

	private String uploadPath;

	private String sandboxPath;

	private String downloadPath;

	private int maxFileSize;

	private String maxFileSizeText;

	private boolean rohdatenGenerieren = false;

	private String hateoasApiUrl;

	/**
	 * Erzeugt eine Instanz von MKVServiceConfiguration
	 */
	public MKVServiceConfiguration() {
	}

	/**
	 * Erzeugt eine Instanz von MKVServiceConfiguration
	 */
	public MKVServiceConfiguration(final String configRoot) {
		this.configRoot = configRoot;
	}

	public boolean isTest() {
		return test;
	}

	public String getConfigRoot() {
		return configRoot;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public String getAllowedOrigins() {
		return allowedOrigins;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public int getMaxFileSize() {
		return maxFileSize;
	}

	public String getMaxFileSizeText() {
		return maxFileSizeText;
	}

	public String getSandboxPath() {
		return sandboxPath;
	}

	public boolean isRohdatenGenerieren() {
		return rohdatenGenerieren;
	}

	public final String getHateoasApiUrl() {
		return hateoasApiUrl;
	}
}
