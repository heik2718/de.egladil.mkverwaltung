//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;

/**
 * @author heikew
 *
 */
@Singleton
public class EgladilMKVConfiguration extends AbstractEgladilConfiguration implements IEgladilConfiguration {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilMKVConfiguration
	 */
	@Inject
	public EgladilMKVConfiguration(@Named("configRoot") String pathConfigRoot) {
		super(pathConfigRoot);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.config.AbstractEgladilConfiguration#getConfigFileName()
	 */
	@Override
	protected String getConfigFileName() {
		return "mkv_service.properties";
	}
}
