//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.domain.auswertungen.Antworteingabemodus;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.Teilnehmer;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.service.payload.response.EnhancePublicTeilnehmerCommand;
import de.egladil.mkv.service.utils.TeilnahmeIdentifierDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * TeilnehmerResource bedient den API-Endpoint für alles, was mit Teilnehmern (also Kindern) zu tun hat.
 */
@Singleton
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes({ MediaType.APPLICATION_JSON + "; charset=utf-8" })
@Path("/teilnahmen")
public class TeilnehmerResource {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnehmerResource.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final AuthorizationService authorizationService;

	private final TeilnehmerFacade teilnehmerFacade;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * TeilnehmerResource
	 */
	@Inject
	public TeilnehmerResource(final TeilnehmerFacade teilnehmerFacade, final AuthorizationService authorizationService) {
		this.teilnehmerFacade = teilnehmerFacade;
		this.authorizationService = authorizationService;
	}

	@Timed
	@OPTIONS
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/teilnehmer")
	public Response options(@PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {

		return Response.ok().build();
	}

	/**
	 * Alle Teilnehmer zum durch die URL definierten Teilnahmeidentifier zurückgeben.
	 *
	 * @param principal Principal
	 * @param jahr String das Jahr
	 * @param nameTeilnahmeart String P/S
	 * @param teilnahmekuerzel String kuerzel der Teilnahme
	 * @return Response
	 */
	@Timed
	@GET
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/teilnehmer")
	public Response getTeilnehmer(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			authorizationService.authorizeForTeilnahme(principal.getName(), teilnahmeIdentifier);

			final List<Teilnehmer> teilnehmer = teilnehmerFacade.getTeilnehmer(teilnahmeIdentifier);
			final List<PublicTeilnehmer> responsePayload = new ArrayList<>(teilnehmer.size());
			for (final Teilnehmer t : teilnehmer) {
				final PublicTeilnehmer pt = PublicTeilnehmer.fromTeilnehmer(t);
				enhancePublicTeilnehmer(pt);
				responsePayload.add(pt);
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), responsePayload);
			return Response.ok().entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), principal.getName());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Holen der Teilnehmerliste. {}, {}", teilnahmeIdentifier, null, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Alle Teilnehmer zum durch die URL definierten Teilnahmeidentifier zurückgeben.
	 *
	 * @param principal Principal
	 * @param jahr String das Jahr
	 * @param nameTeilnahmeart String P/S
	 * @param teilnahmekuerzel String kuerzel der Teilnahme
	 * @return Response
	 */
	@Timed
	@GET
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen/{kuerzelAuswertungsgruppe}/teilnehmer")
	public Response getTeilnehmerZuGruppe(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel,
		@PathParam(value = "kuerzelAuswertungsgruppe") final String kuerzelAuswertungsgruppe) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			authorizationService.authorizeForTeilnahme(principal.getName(), teilnahmeIdentifier);

			final Optional<Auswertungsgruppe> optGruppe = this.teilnehmerFacade.getAuswertungsgruppe(kuerzelAuswertungsgruppe);
			if (!optGruppe.isPresent()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Suche nach Auswertungsgruppe mit kuerzel {}", kuerzelAuswertungsgruppe);
				throw new ResourceNotFoundException("Auswertungsgruppe");
			}
			final Auswertungsgruppe auswertungsgruppe = optGruppe.get();
			final List<Teilnehmer> teilnehmer = auswertungsgruppe.getAlleTeilnehmer();
			final List<PublicTeilnehmer> responsePayload = new ArrayList<>(teilnehmer.size());
			for (final Teilnehmer t : teilnehmer) {
				final PublicTeilnehmer pt = PublicTeilnehmer.fromTeilnehmer(t);
				enhancePublicTeilnehmer(pt);
				responsePayload.add(pt);
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), responsePayload);
			return Response.ok().entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), principal.getName());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final ResourceNotFoundException e) {
			LOG.warn(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Holen der Teilnehmerliste. {}, {}", teilnahmeIdentifier, null, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Neuen Teilnehmer zur gegebenen Teilnahme anlegen
	 *
	 * @param principal Priincipal authorisierung
	 * @param jahr int Jahr der Teilnahme
	 * @param nameTeilnahmeart String die Teilnahmeart S oder P
	 * @param teilnahmekuerzel String das kuerzel der Teilnahme
	 * @param payload TeilnehmerPayload
	 * @return Rsponse mit entity APIResponsePayload mit neuem Telnehmer als payload und HATEOAS link zu neuem
	 * Teilnehmer
	 */
	@Timed
	@POST
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/teilnehmer")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createTeilnehmer(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, final PublicTeilnehmer payload) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		final String benutzerUuid = principal.getName();
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			validationDelegate.check(payload, PublicTeilnehmer.class);

			authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			if (StringUtils.isNoneBlank(payload.getEingabemodus())) {
				try {
					Antworteingabemodus.valueOf(payload.getEingabemodus());
				} catch (final IllegalArgumentException e) {
					LOG.warn("ungültiger Eingabemodus {} ", payload.getEingabemodus());
					final String msg = applicationMessages.getString("general.validationErrors");
					final APIResponsePayload entity = new APIResponsePayload(APIMessage.error(msg));
					return Response.status(Status.BAD_REQUEST.getStatusCode()).entity(entity).build();
				}
			} else {
				payload.setEingabemodus(null);
			}

			final Teilnehmer teilnehmer = teilnehmerFacade.createTeilnehmer(benutzerUuid, teilnahmeIdentifier, payload);
			final PublicTeilnehmer pt = PublicTeilnehmer.fromTeilnehmer(teilnehmer);
			final String nachname = pt.getNachname() == null ? "" : pt.getNachname();
			String text = null;
			if (pt.getLoesungszettelKuerzel() != null) {
				text = MessageFormat.format(applicationMessages.getString("teilnehmer.createMitAntworten.success"),
					new Object[] { pt.getVorname(), nachname, pt.getPunkte(), pt.getKaengurusprung() });
			} else {
				text = MessageFormat.format(applicationMessages.getString("teilnehmer.create.success"),
					new Object[] { pt.getVorname(), nachname });
			}
			final APIMessage msg = APIMessage.info(text);
			final APIResponsePayload entity = new APIResponsePayload(msg, pt);
			// final HateoasLink hypermediaLink = new HateoasLink(teilnehmer.getKuerzel() + "/loesungszettel",
			// "teilnehmer",
			// "GET", null);
			// entity.addHypermediaLink(hypermediaLink);
			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler payload {}: {}", payload, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error("Exception beim Anlegen eines Teilnehmers. {}, {}", teilnahmeIdentifier, payload, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	/**
	 * Vorhandenen Teilnehmer speichern.
	 *
	 * @param principal Priincipal authorisierung
	 * @param jahr int Jahr der Teilnahme
	 * @param nameTeilnahmeart String die Teilnahmeart S oder P
	 * @param teilnahmekuerzel String das kuerzel der Teilnahme
	 * @param payload TeilnehmerPayload
	 * @return Response mit entity APIResponsePayload mit neuem Telnehmer als payload und HATEOAS link zu neuem
	 * Teilnehmer
	 */
	@Timed
	@PUT
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/teilnehmer")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response updateTeilnehmer(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, final PublicTeilnehmer payload) {

		final String benutzerUuid = principal.getName();
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {

			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			validationDelegate.check(payload, PublicTeilnehmer.class);

			authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			final Teilnehmer teilnehmer = teilnehmerFacade.updateTeilnehmer(benutzerUuid, teilnahmeIdentifier, payload);
			final PublicTeilnehmer pt = PublicTeilnehmer.fromTeilnehmer(teilnehmer);
			final String nachname = pt.getNachname() == null ? "" : pt.getNachname();
			String text = null;
			if (pt.getLoesungszettelKuerzel() != null) {
				text = MessageFormat.format(applicationMessages.getString("teilnehmer.updateMitAntworten.success"),
					new Object[] { pt.getVorname(), nachname, pt.getPunkte(), pt.getKaengurusprung() });
			} else {
				text = MessageFormat.format(applicationMessages.getString("teilnehmer.update.success"),
					new Object[] { pt.getVorname(), nachname });
			}
			final APIMessage msg = APIMessage.info(text);
			final APIResponsePayload entity = new APIResponsePayload(msg, pt);
			// final HateoasLink hypermediaLink = new HateoasLink(teilnehmer.getKuerzel() + "/loesungszettel",
			// "teilnehmer",
			// "GET", null);
			// entity.addHypermediaLink(hypermediaLink);
			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler payload {}: {}", payload, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final ResourceNotFoundException e) {
			LOG.warn(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Anlegen eines Teilnehmers. {}, {}", teilnahmeIdentifier, payload, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	/**
	 * Teilnehmer mit dem kuerzel löschen.
	 *
	 * @param principal Principal authorisierung
	 * @param kuerzel String das kuerzel des Teilnehmers
	 * @return Response mit entity APIResponsePayload
	 */
	@Timed
	@DELETE
	@Path("/teilnehmer/{kuerzel}")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response deleteTeilnehmer(@Auth final Principal principal, @PathParam(value = "kuerzel") final String kuerzel) {

		try {
			// Authorization in teilnehmerFacade!
			final Optional<Teilnehmer> optTeilnehmer = teilnehmerFacade.deleteTeilnehmer(principal.getName(), kuerzel);
			String text = null;
			if (optTeilnehmer.isPresent()) {
				final Teilnehmer pt = optTeilnehmer.get();
				final String nachname = pt.getNachname() == null ? "" : pt.getNachname();
				text = MessageFormat.format(applicationMessages.getString("teilnehmer.delete.success"),
					new Object[] { pt.getVorname(), nachname });
			} else {
				text = applicationMessages.getString("general.delete.success");
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(text));
			return Response.ok().entity(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), principal.getName());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Löschen des Teilnehmers mit kuerzel {}: {}", kuerzel, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Teilnehmer zur gegebenen Teilnahme werden gelöscht.
	 *
	 * @param principal Principal authorisierung
	 * @param jahr int Jahr der Teilnahme
	 * @param nameTeilnahmeart String die Teilnahmeart S oder P
	 * @param teilnahmekuerzel String das kuerzel der Teilnahme
	 * @return Response mit entity APIResponsePayload
	 */
	@Timed
	@DELETE
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/teilnehmer")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response deleteAllTeilnehmer(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		final String benutzerUuid = principal.getName();

		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			teilnehmerFacade.deleteAllTeilnehmer(principal.getName(), teilnahmeIdentifier);
			final APIResponsePayload entity = new APIResponsePayload(
				APIMessage.info(applicationMessages.getString("teilnehmer.all.delete.success")));
			return Response.ok().entity(entity).build();
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler teilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Löschen der Teilnehmer zu einer Teilnahme: {}, {}", teilnahmeIdentifier, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	private void enhancePublicTeilnehmer(final PublicTeilnehmer pt) {
		final EnhancePublicTeilnehmerCommand cmd = new EnhancePublicTeilnehmerCommand(pt, teilnehmerFacade);
		cmd.execute();
	}
}
