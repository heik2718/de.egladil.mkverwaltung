//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.confirmation.IPrivatConfirmationService;

/**
 * PrivatConfirmationServiceImpl
 */
@Singleton
public class PrivatConfirmationServiceImpl extends AbstractConfirmationService
	implements IPrivatConfirmationService, IKontaktUndTeilnahmeCallback {

	private final IPrivatkontoDao kontaktDao;

	private final IPrivatteilnahmeDao teilnahmeDao;

	/**
	 * Erzeugt eine Instanz von PrivatConfirmationServiceImpl
	 */
	@Inject
	public PrivatConfirmationServiceImpl(final IRegistrierungService registrierungService, final IBenutzerService benutzerService,
		final IAnonymisierungsservice anonymsierungsservice, final IPrivatkontoDao kontaktDao, final IPrivatteilnahmeDao teilnahmeDao) {
		super(registrierungService, benutzerService, anonymsierungsservice);
		this.kontaktDao = kontaktDao;
		this.teilnahmeDao = teilnahmeDao;
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IKontaktUndTeilnahmeCallback#persist(java.util.List)
	 */
	@Override
	public void persist(final List<TeilnahmeIdentifierProvider> teilnahmen)
		throws EgladilConcurrentModificationException, EgladilDuplicateEntryException, MKVException {
		final List<Privatteilnahme> privatTeilnahmen = toPrivatteilnahmen(teilnahmen);
		try {
			teilnahmeDao.persist(privatTeilnahmen);
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optEx.isPresent()) {
				final String msg = "Jemand anders hat die Teilnahmen inzwischen geändert";
				throw new EgladilConcurrentModificationException(msg);
			}
			throw new MKVException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	/**
	 * TODO
	 *
	 * @param teilnahmen
	 * @return
	 */

	private List<Privatteilnahme> toPrivatteilnahmen(final List<TeilnahmeIdentifierProvider> teilnahmen) {
		if (teilnahmen == null) {
			throw new MKVException("teilnahmen duerfen nicht null sein!");
		}
		final List<Privatteilnahme> privatTeilnahmen = new ArrayList<>();
		for (final TeilnahmeIdentifierProvider t : teilnahmen) {
			privatTeilnahmen.add((Privatteilnahme) t);
		}
		return privatTeilnahmen;
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.AbstractConfirmationService#findKontaktByUUID(java.lang.String)
	 */
	@Override
	protected Optional<IMKVKonto> findKontaktByUUID(final String uuid) {
		final Optional<Privatkonto> opt = kontaktDao.findByUUID(uuid);
		return opt.isPresent() ? Optional.of(opt.get()) : Optional.empty();
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IKontaktUndTeilnahmeCallback#persist(de.egladil.mkv.persistence.domain.IMKVKonto)
	 */
	@Override
	public IMKVKonto persist(final IMKVKonto kontakt) {
		return kontaktDao.persist((Privatkonto) kontakt);
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.AbstractConfirmationService#getKontaktUndTeilnahmeCallback()
	 */
	@Override
	protected IKontaktUndTeilnahmeCallback getKontaktUndTeilnahmeCallback() {
		return this;
	}
}
