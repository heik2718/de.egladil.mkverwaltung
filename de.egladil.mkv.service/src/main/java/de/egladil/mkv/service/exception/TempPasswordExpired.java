//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.exception;

/**
* TempPasswordExpired
*/
public class TempPasswordExpired extends RuntimeException {

	/* serialVersionUID		*/
	private static final long serialVersionUID = 1L;

	/**
	* TempPasswordExpired
	*/
	public TempPasswordExpired() {
	}

	/**
	* TempPasswordExpired
	*/
	public TempPasswordExpired(final String arg0) {
		super(arg0);
	}

	/**
	* TempPasswordExpired
	*/
	public TempPasswordExpired(final Throwable arg0) {
		super(arg0);
	}

	/**
	* TempPasswordExpired
	*/
	public TempPasswordExpired(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}
}
