//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.LandKuerzel;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.payload.response.PublicOrt;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.AbstractSchuleKatalogantrag;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * KatalogResource
 */
@Singleton
@Path("")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class KatalogResource {

	private static final Logger LOG = LoggerFactory.getLogger(KatalogResource.class);

	private final IKatalogService katalogService;

	private ValidationDelegate validationDelegate = new ValidationDelegate();

	private final MailDelegate mailDelegate;

	private final AuthorizationService authorizationService;

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * Erzeugt eine Instanz von KatalogResource
	 */
	@Inject
	public KatalogResource(final IKatalogService katalogService, final MailDelegate mailDelegate,
		final AuthorizationService authorizationService) {
		this.katalogService = katalogService;
		this.mailDelegate = mailDelegate;
		this.authorizationService = authorizationService;
	}

	/**
	 * Gibt eine Liste von PublicLand als JSON zurück.<br>
	 * <br>
	 * <strong>Beispiel:</strong> /laender
	 *
	 * @return
	 */
	@GET
	@Path("laender")
	@Timed
	public Response getLaender() {
		try {
			final List<PublicLand> entity = katalogService.getPublicLaender();
			return Response.ok().entity(entity).build();
		} catch (final MKVException e) {
			LOG.error(e.getMessage());
			return ExceptionFactory.internalServerErrorException().getResponse();
		}
	}

	/**
	 * Land mit gegebenem Kürzel.<br>
	 * <br>
	 * <strong>Beispiel:</strong> /laender/de-BW Rückgabe:
	 * <ul>
	 * <li>das Land, wenn es vorhanden ist</li>
	 * <li>404, wenn es kein Land mit dem kuerzel gibt</li>
	 * <li>500, bei einer MKVException</li>
	 * </ul>
	 *
	 *
	 * @param landkuerzel
	 * @return Response Entity ist Land mit Ortsliste ohne Schulen.
	 */
	@GET
	@Path("laender/{land}")
	@Timed
	public Response getLand(@PathParam("land") @NotBlank @LandKuerzel @Size(min = 1, max = 5) final String landkuerzel) {
		try {
			final Optional<Land> opt = katalogService.getLand(landkuerzel);
			if (opt.isPresent()) {
				return Response.ok().entity(PublicLand.createMitOrten(opt.get())).build();
			}
			return ExceptionFactory.resourceNotFound(LOG, "Suche nach Land mit [kuerzel=" + landkuerzel + "]").getResponse();
		} catch (final ConstraintViolationException e) {
			return ExceptionFactory.badRequest("Suche nach Land mit [kuerzel=" + landkuerzel + "]").getResponse();
		} catch (final MKVException e) {
			return ExceptionFactory.internalServerErrorException().getResponse();
		}
	}

	/**
	 * Ort im Land mit gegeben ortkuerzel.<br>
	 * <br>
	 * <strong>Beispiel:</strong> /laender/de-BW/TG-65-A
	 *
	 * @param landkuerzel
	 * @param ortkuerzel
	 * @return Response Entity ist Ort mit Schulliste
	 */
	@GET
	@Path("laender/{land}/{ort}")
	@Timed
	public Response getOrt(@PathParam("land") @NotBlank @LandKuerzel @Size(min = 1, max = 5) final String landkuerzel,
		@PathParam("ort") @NotBlank @Kuerzel @Size(min = 8, max = 8) final String ortkuerzel) {
		try {
			final Optional<Ort> opt = katalogService.getOrt(landkuerzel, ortkuerzel);
			if (opt.isPresent()) {
				return Response.ok().entity(PublicOrt.createMitSchulen(opt.get())).build();
			}
			return ExceptionFactory
				.resourceNotFound(LOG, "Suche nach Ort mit [ortkuerzel=" + ortkuerzel + ", landkuerzel=" + landkuerzel + "]")
				.getResponse();
		} catch (final ConstraintViolationException e) {
			return ExceptionFactory
				.badRequest("Suche nach Ort mit [ortkuerzel=" + ortkuerzel + ", landkuerzel=" + landkuerzel + "]").getResponse();
		} catch (final MKVException e) {
			return ExceptionFactory.internalServerErrorException().getResponse();
		}
	}

	@POST
	@Path("laender/{land}/schulantrag")
	@Consumes(MediaType.APPLICATION_JSON)
	@Timed
	public Response schuleintragBeantragen(
		@PathParam("land") @NotBlank @LandKuerzel @Size(min = 1, max = 5) final String landkuerzel,
		final AbstractSchuleKatalogantrag katalogantrag) {
		try {
			if (katalogantrag == null) {
				LOG.error("Parameter katalogantrag darf nicht null sein!");
				throw ExceptionFactory.badRequest();
			}
			final Optional<Land> opt = katalogService.getLand(landkuerzel);
			if (opt.isPresent()) {
				katalogantrag.setNameLand(opt.get().getName());
				katalogantrag.setLand(opt.get());
			} else {
				if (StringUtils.isBlank(katalogantrag.getNameLand())) {
					final APIResponsePayload apiRsponse = new APIResponsePayload(
						APIMessage.error(applicationMessages.getString("Katalogantrag.error.nameLand")));
					return Response.status(CustomResponseStatus.VALIDATION_ERRORS.getStatusCode()).entity(apiRsponse).build();
				}
			}
			validationDelegate.check(katalogantrag, katalogantrag.ownClass());
			mailDelegate.schulkatalogmailVersenden(katalogantrag);
			final APIResponsePayload entity = new APIResponsePayload(
				APIMessage.info(applicationMessages.getString("Katalogantrag.success")));
			return Response.ok().entity(entity).build();
		} catch (final EgladilWebappException e) {
			LOG.debug("Suche nach Land mit [landkuerzel=" + landkuerzel + "]");
			return exceptionHandler.mapToMKVApiResponse(e, null, katalogantrag);
		} catch (final InvalidMailAddressException e) {
			if (e.getAllInvalidAdresses().contains(katalogantrag.getEmail())) {
				LOG.warn(
					GlobalConstants.LOG_PREFIX_MAILVERSAND
						+ "Fehler beim Beantragen eines Eintrags in den Schulkatalog: Empfänger {} ist ungültig.",
					katalogantrag.getEmail());
				final EgladilWebappException webappException = ExceptionFactory
					.badRequest(applicationMessages.getString("Katalogantrag.error.mailempfaeger"));
				final Response mkvApiResponse = exceptionHandler.mapToMKVApiResponse(webappException, null, katalogantrag);
				return mkvApiResponse;
			}
			LOG.error(GlobalConstants.LOG_PREFIX_MAILVERSAND
				+ "Fehler beim Beantragen eines Eintrags in den Schulkatalog: hidden Empfänger ist keine gültige Mailadrese: allInvalidAddresses=[{}]",
				PrettyStringUtils.collectionToDefaultString(e.getAllInvalidAdresses()));
			final EgladilWebappException webappException = ExceptionFactory.internalServerErrorException();
			return exceptionHandler.mapToMKVApiResponse(webappException, null, katalogantrag);
		} catch (final EgladilMailException e) {
			LOG.error("Mailfehler beim Beantragen eines Eintrags in den Schulkatalog: " + e.getMessage(), e);
			return Response.status(CustomResponseStatus.MAILSERVER_NOT_AVAILABLE.getStatusCode())
				.entity(new APIResponsePayload(APIMessage.error(applicationMessages.getString("Katalogantrag.error.mailserver"))))
				.build();
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception mit Payload {}: {}", katalogantrag, e.getMessage(), e);
			final EgladilWebappException webappException = ExceptionFactory.internalServerErrorException(LOG, e, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(webappException, null, katalogantrag);
		}
	}

	@GET
	@Path("schulen/{schulkuerzel}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Timed
	public Response getSchule(@Auth final Principal principal,
		@PathParam("schulkuerzel") @NotBlank @Kuerzel @Size(min = 8, max = 8) final String schulkuerzel) {

		final String benutzerUuid = principal.getName();
		try {

			authorizationService.authorizeLehrerForSchule(benutzerUuid, schulkuerzel);
//			authorizationService.authorizeForSchuleAktuellesWettbewerbsjahr(benutzerUuid, schulkuerzel);

			final Optional<PublicSchule> opt = katalogService.getSchule(schulkuerzel);
			if (opt.isPresent()) {
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), opt.get());
				return Response.ok().entity(entity).build();
			} else {
				LOG.warn("Schule " + schulkuerzel + " existiert nicht oder nicht mehr.");
				throw new ResourceNotFoundException("Schule existiert nicht");
			}
		} catch (final EgladilAuthenticationException | EgladilAuthorizationException | DisabledAccountException
			| ResourceNotFoundException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception: {}", e.getMessage(), e);
			final EgladilWebappException webappException = ExceptionFactory.internalServerErrorException(LOG, e, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(webappException, null, null);
		}
	}
}
