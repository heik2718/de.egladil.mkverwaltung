//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.service.payload.request.AbstractMKVRegistrierung;

/**
 * RegistrierungsanfrageValidator
 */
public class MKVRegistrierungValidator implements ConstraintValidator<ValidMKVRegistrierung, AbstractMKVRegistrierung> {

	private static final Logger LOG = LoggerFactory.getLogger(MKVRegistrierungValidator.class);

	/**
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(ValidMKVRegistrierung constraintAnnotation) {
		// nix erforderlich
	}

	/**
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(AbstractMKVRegistrierung value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (!value.isAgbGelesen()) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.mkvregistrierung.agbGelesen}")
				.addPropertyNode("agbGelesen").addConstraintViolation();
			LOG.debug("agb nicht geleesen");
			return false;
		}
		if (value.getPasswort() != null && !value.getPasswort().equals(value.getPasswortWdh())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("{de.egladil.constraints.mkvregistrierung.passwords}").addBeanNode()
				.addConstraintViolation();
			return false;
		}
		return true;
	}
}
