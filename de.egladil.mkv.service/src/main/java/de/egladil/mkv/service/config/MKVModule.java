//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenAuthenticator;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenDao;
import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.bv.aas.impl.AuthenticationServiceImpl;
import de.egladil.bv.aas.impl.BenutzerServiceImpl;
import de.egladil.bv.aas.impl.RegistrierungServiceImpl;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.impl.MailserviceImpl;
import de.egladil.mkv.auswertungen.persistence.AsyncUploadListener;
import de.egladil.mkv.auswertungen.persistence.IUploadListener;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.auswertungen.statistik.impl.SchulstatistikServiceImpl;
import de.egladil.mkv.auswertungen.statistik.impl.VerteilungGeneratorImpl;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.auswertungen.urkunden.impl.AuswertungServiceImpl;
import de.egladil.mkv.persistence.config.MKVPersistenceModule;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.kataloge.impl.KatalogService;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;
import de.egladil.mkv.persistence.service.impl.AdvFacadeImpl;
import de.egladil.mkv.persistence.service.impl.Anonymsierungsservice;
import de.egladil.mkv.persistence.service.impl.AuswertungsgruppenServiceImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerFacadeImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerServiceImpl;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.anmeldung.impl.WettbewerbsanmeldungServiceImpl;
import de.egladil.mkv.service.confirmation.ILehrerConfirmationService;
import de.egladil.mkv.service.confirmation.IPrivatConfirmationService;
import de.egladil.mkv.service.confirmation.impl.LehrerConfirmationServiceImpl;
import de.egladil.mkv.service.confirmation.impl.PrivatConfirmationServiceImpl;
import de.egladil.mkv.service.health.BVHealthCheck;
import de.egladil.mkv.service.registrierung.ILehrerRegistrierungService;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;
import de.egladil.mkv.service.registrierung.impl.LehrerRegistrierungServiceImpl;
import de.egladil.mkv.service.registrierung.impl.PrivatRegistrierungServiceImpl;
import io.dropwizard.auth.Authenticator;

/**
 * MKVModule
 */
public class MKVModule extends AbstractModule {

	private final String pathConfigRoot;

	public MKVModule(final String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());

		install(new BVPersistenceModule(this.pathConfigRoot));
		install(new MKVPersistenceModule(this.pathConfigRoot));

		bind(IMailservice.class).to(MailserviceImpl.class);
		bind(ILehrerRegistrierungService.class).to(LehrerRegistrierungServiceImpl.class);
		bind(ILehrerConfirmationService.class).to(LehrerConfirmationServiceImpl.class);
		bind(IPrivatRegistrierungService.class).to(PrivatRegistrierungServiceImpl.class);
		bind(IPrivatConfirmationService.class).to(PrivatConfirmationServiceImpl.class);

		bind(IWettbewerbsanmeldungService.class).to(WettbewerbsanmeldungServiceImpl.class);

		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class);
		bind(IBenutzerService.class).to(BenutzerServiceImpl.class);
		bind(IRegistrierungService.class).to(RegistrierungServiceImpl.class);
		bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);

		bind(IAccessTokenDAO.class).to(CachingAccessTokenDao.class);

		bind(IEgladilConfiguration.class).to(EgladilMKVConfiguration.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);

		bind(IKatalogService.class).to(KatalogService.class);
		bind(IProtokollService.class).to(ProtokollService.class);

		bind(IAnonymisierungsservice.class).to(Anonymsierungsservice.class);

		bind(IUploadListener.class).to(AsyncUploadListener.class);

		bind(BVHealthCheck.class);

		bind(AuswertungService.class).to(AuswertungServiceImpl.class);
		bind(AuswertungsgruppenService.class).to(AuswertungsgruppenServiceImpl.class);
		bind(TeilnehmerService.class).to(TeilnehmerServiceImpl.class);
		bind(SchulstatistikService.class).to(SchulstatistikServiceImpl.class);

		bind(AdvFacade.class).to(AdvFacadeImpl.class);
		bind(TeilnehmerFacade.class).to(TeilnehmerFacadeImpl.class);
		bind(IVerteilungGenerator.class).to(VerteilungGeneratorImpl.class);
	}

	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", this.pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
