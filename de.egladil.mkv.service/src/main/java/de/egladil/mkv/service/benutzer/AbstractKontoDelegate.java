//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.request.StatusPayload;

/**
 * AbstractKontoDelegate
 */
public abstract class AbstractKontoDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractKontoDelegate.class);

	private final IBenutzerService benutzerService;

	/**
	 * Erzeugt eine Instanz von AbstractKontoDelegate
	 */
	public AbstractKontoDelegate(final IBenutzerService benutzerService) {
		this.benutzerService = benutzerService;
	}

	/**
	 * Verifiziert, dass der payload.benutzername zum Benutzerkonto passt und ändert dann die Daten im Lehrerkonto.
	 *
	 * @param payload
	 * @param benutzerUuid
	 * @return
	 */
	public IMKVKonto verifyAndChangePerson(final PersonAendernPayload payload, final String benutzerUuid)
		throws IllegalArgumentException, UnknownAccountException, DisabledAccountException, EgladilConcurrentModificationException,
		EgladilStorageException, ResourceNotFoundException {
		verifyBenutzer(payload, benutzerUuid);
		return changePerson(payload, benutzerUuid);
	}

	/**
	 * Das flag mailbenachrichtigung im Lehrer- oder Privatkonto wird geändert.
	 *
	 * @param payload
	 * @param benutzerUuid
	 * @return
	 */
	public IMKVKonto changeStatusNewsletter(final StatusPayload payload, final String benutzerUuid)
		throws IllegalArgumentException {
		if (payload == null || benutzerUuid == null) {
			throw new IllegalArgumentException("Parameter darf nicht null sein: "
				+ (payload == null ? "payload null" : (benutzerUuid == null ? "benutzerUuid null" : "b")));
		}
		checkBenutzerkonto(benutzerUuid);
		final IMKVKonto result = specialChangeStatusNewsletter(payload, benutzerUuid);
		return result;
	}

	/**
	 * Autorisierung ist durch. Jetzt ändern und persistieren.
	 *
	 * @param payload StatusPayload ist bereits auf null geprüft
	 * @param benutzerkonto Benutzerkonto existiert und ist aktiv
	 */
	protected abstract IMKVKonto specialChangeStatusNewsletter(final StatusPayload payload, final String benutzerUuid);

	private Benutzerkonto checkBenutzerkonto(final String benutzerUuid) {
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(benutzerUuid);
		if (benutzerkonto == null) {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Benutzerkonto mit UUID [{}] nicht gefunden", benutzerUuid);
			throw new UnknownAccountException("Benutzerkonto mit UUID [" + benutzerUuid + "] nicht gefunden");
		}
		if (benutzerkonto.isGesperrt()) {
			throw new DisabledAccountException("Das Benutzerkonto ist gesperrt.");
		}
		if (!benutzerkonto.isAktiviert()) {
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE
				+ " Benutzer konnte sich einloggen, obwohl das Benutzerkonto nicht aktiviert war.");
			throw new DisabledAccountException("Das Benutzerkonto ist nicht aktiviert.");
		}
		return benutzerkonto;
	}

	/**
	 * Ändert Name und Vorname der Person.
	 *
	 * @param payload
	 * @param benutzerUuid
	 * @return
	 */
	protected abstract IMKVKonto changePerson(PersonAendernPayload payload, String benutzerUuid)
		throws ResourceNotFoundException, EgladilStorageException, EgladilConcurrentModificationException;

	/**
	 * Verifiziert, dass der payload.benutzername zum Benutzerkonto passt.
	 *
	 * @param payload
	 * @param benutzerUuid
	 */
	protected void verifyBenutzer(final PersonAendernPayload payload, final String benutzerUuid)
		throws IllegalArgumentException, UnknownAccountException, DisabledAccountException {
		if (payload == null || benutzerUuid == null) {
			throw new IllegalArgumentException("Parameter darf nicht null sein: "
				+ (payload == null ? "payload null" : (benutzerUuid == null ? "benutzerUuid null" : "b")));
		}

		final Benutzerkonto benutzerkonto = checkBenutzerkonto(benutzerUuid);
		if (!benutzerkonto.getLoginName().equalsIgnoreCase(payload.getUsername().toLowerCase())) {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "benutzerkonto.loginname != payload.benutzername: " + payload.toBotLog()
				+ ", benutzerUuid=" + benutzerUuid);
			throw new UnknownAccountException("benutzerkonto.loginname != payload.benutzername");
		}
	}
}
