//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.payload.Registrierungsanfrage;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.IMKVRegistrierung;
import de.egladil.mkv.service.utils.PayloadMapper;

/**
 * BenutzerkontoDelegate
 */
@Singleton
public class BenutzerkontoDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerkontoDelegate.class);

	private final IRegistrierungService registrierungService;

	private final IAuthenticationService authenticationService;

	private final IBenutzerService benutzerService;

	private final IProtokollService protokollService;

	private final IAnonymisierungsservice anonymisierungsservice;

	private MailDelegate mailDelegate;

	/**
	 * Erzeugt eine Instanz von BenutzerkontoDelegate
	 */
	@Inject
	public BenutzerkontoDelegate(final IRegistrierungService registrierungService,
		final IAuthenticationService authenticationService, final IBenutzerService benutzerService, final MailDelegate mailDelegate,
		final IProtokollService protokollService, final IAnonymisierungsservice anonymisierungsservice) {
		this.registrierungService = registrierungService;
		this.authenticationService = authenticationService;
		this.benutzerService = benutzerService;
		this.mailDelegate = mailDelegate;
		this.protokollService = protokollService;
		this.anonymisierungsservice = anonymisierungsservice;
	}

	/**
	 * Erzeugt ein Registrierungsanfrage- Objekt und schickt es an den RegistrierungService.
	 *
	 * @param anfrage
	 * @return Registrierungsbestaetigung
	 */
	public Aktivierungsdaten benutzerkontoAnlegen(final IMKVRegistrierung anfrage) throws ConstraintViolationException,
		IllegalArgumentException, EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException {
		final Registrierungsanfrage registrierungsanfrage = PayloadMapper.toRegistrierungsanfrage(anfrage);

		// Fall, dass es solch ein Benutzerkonto schon gibt, wird durch EgladilDuplicateEntryException propagiert
		final Aktivierungsdaten registrierungsbestaetigung = registrierungService.register(registrierungsanfrage);
		return registrierungsbestaetigung;
	}

	/**
	 * Authentisierung mit einem temporären Passwort.
	 *
	 * @param anwendung
	 * @param email
	 * @param tempPassword
	 * @return AuthenticationInfo das Benutzerkonto.
	 */
	public AuthenticationInfo authenticateWithTemporaryPasswort(final Anwendung anwendung, final String email,
		final String tempPassword) throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException {
		final AuthenticationInfo authInfo = authenticationService.authenticateWithTemporaryPasswort(anwendung, email, tempPassword);
		return authInfo;
	}

	/**
	 * Ändert das Passwort und versendet die Mail.
	 *
	 * @param benutzerUuid
	 * @param passwordNeu
	 * @return Optional<Benutzerkonto>
	 */
	public Optional<Benutzerkonto> changePasswordAndSendMail(final String benutzerUuid, final char[] passwordNeu)
		throws EgladilStorageException, EgladilBVException, EgladilDuplicateEntryException, EgladilConcurrentModificationException,
		DisabledAccountException {
		final Optional<Benutzerkonto> optBenutzer = authenticationService.changePasswort(Anwendung.MKV, benutzerUuid, passwordNeu);

		if (optBenutzer.isPresent()) {
			sendPasswortmailQuietly(optBenutzer.get());
			return optBenutzer;
		}
		return Optional.empty();
	}

	/**
	 * Authentisiert und ändert anschließend die Mailadresse und damit den Loginnamen.
	 *
	 * @param payload
	 * @return
	 */
	public Optional<Benutzerkonto> authenticateAndChangeEmail(final MailadresseAendernPayload payload)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException {
		final UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(),
			payload.getPassword().toCharArray());
		final AuthenticationInfo authenticationInfo = authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV);
		final String benutzerUuid = ((Benutzerkonto) authenticationInfo).getUuid();
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(benutzerUuid);
		benutzerkonto.setLoginName(payload.getEmail());
		benutzerkonto.setEmail(payload.getEmail());

		final Benutzerkonto result = benutzerService.persistBenutzerkonto(benutzerkonto);

		protokollService.protokollieren(Ereignisart.EMAIL_GEAENDERT.getKuerzel(), benutzerUuid,
			"[alt=" + payload.getUsername() + ", neu=" + payload.getEmail() + "]");

		return Optional.of(result);
	}

	/**
	 *
	 * @param benutzerUuid String UUID des Benutzers.
	 * @return Benutzerkonto oder null
	 */
	public Benutzerkonto getBenutzerkonto(final String benutzerUuid) {
		final Benutzerkonto result = benutzerService.findBenutzerkontoByUUID(benutzerUuid);
		return result;
	}

	/**
	 * Das Temporäre Passwort wird gelöscht. Eventuelle Exceptions werden als INFO geloggt.
	 *
	 * @param tempPassword
	 */
	public void removeTempPasswordQuietly(final Benutzerkonto benutzerkonto, final String tempPassword) {
		if (benutzerkonto == null || tempPassword == null) {
			LOG.debug("Einer der Parameter war null: tue nix");
			return;
		}
		final Optional<Aktivierungsdaten> opt = benutzerkonto.findAktivierungsdatenByConfirmCode(tempPassword);
		if (!opt.isPresent()) {
			LOG.debug("Aktivierungsdaten nicht mehr da: tue nix");
			return;
		}
		try {
			benutzerkonto.removeAktivierungsdaten(opt.get());
			benutzerService.persistBenutzerkonto(benutzerkonto);
		} catch (final Exception e) {
			final String msg = GlobalConstants.LOG_PREFIX_DATENMUELL + e.getClass().getName()
				+ " beim Loeschen der Aktivierungsdaten mit [confirmationCode=" + tempPassword + "]: " + e.getMessage();
			LOG.info(msg);
			LOG.debug(e.getMessage(), e);
		} finally {
			PrettyStringUtils.erase(tempPassword);
		}
	}

	void sendPasswortmailQuietly(final Benutzerkonto benutzerkonto) {
		try {
			mailDelegate.passwortGeaendertMailSenden(benutzerkonto);
		} catch (final InvalidMailAddressException e) {
			if (e.getAllInvalidAdresses().contains(benutzerkonto.getEmail())) {
				LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE
					+ "Infomail Passwort ändern wurde nicht gesendet: Empfänger ist keine gültige Mailadresse.");
			}
		} catch (final Exception e) {
			LOG.warn("Infomail Passwort ändern wurde nicht gesendet: " + e.getMessage(), e);
		}
	}

	/**
	 * @param aktivierungsdaten
	 */
	public void benutzerkontoAnonymisieren(final Aktivierungsdaten aktivierungsdaten) {
		if (aktivierungsdaten == null) {
			throw new IllegalArgumentException("aktivierungsdaten darf nicht null sein");
		}
		LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + "aktivierungsdaten ohne Benutzerkonto: {}", aktivierungsdaten);
		if (aktivierungsdaten.getBenutzerkonto() != null) {
			anonymisierungsservice.kontoAnonymsieren(aktivierungsdaten.getBenutzerkonto().getUuid(),
				"nicht abgeschlossene Registrierung");
		}
	}

	/**
	 * Erzeugt ein temporäres Passwort zu Benutzer mit der gegeben Mailadresse.
	 *
	 * @param email
	 * @param gueltigkeitLink
	 * @return
	 */
	public Aktivierungsdaten generateResetPasswordCode(final String email, final int gueltigkeitLink)
		throws IllegalArgumentException, UnknownAccountException, DisabledAccountException, EgladilStorageException {
		return benutzerService.generateResetPasswordCode(email, Anwendung.MKV, gueltigkeitLink);
	}

	/**
	 * Zu Testzwecken verwenden.
	 *
	 * @param mailDelegate
	 */
	void setMailDelegate(final MailDelegate mailDelegate) {
		this.mailDelegate = mailDelegate;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param tempPasswordForTest neuer Wert der Membervariablen tempPasswordForTest
	 */
	public void setTempPasswordForTest(final String tempPasswordForTest) {
		benutzerService.setTempPasswordForTest(tempPasswordForTest);
	}
}
