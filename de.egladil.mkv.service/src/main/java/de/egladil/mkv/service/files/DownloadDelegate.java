//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.Downloaddaten;
import de.egladil.mkv.persistence.domain.teilnahmen.IDownload;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IdentifierMKVKontoProvider;
import de.egladil.mkv.service.payload.response.DateiInfo;

/**
 * DownloadDelegate
 */
@Singleton
public class DownloadDelegate {

	private static final String DATEINAMEN_BESCHREIBUNGEN = "dateinamen_beschreibungen.txt";

	private static final String WETTEWERBRESOURCE_URL = "dateien";

	private static final String HTMLRESOURCE_URL = "htmlcontent";

	private static final Logger LOG = LoggerFactory.getLogger(DownloadDelegate.class);

	private String freischaltungLehrerMarker = "lehrer.txt";

	private static final String FREISCHALTUNG_PRIVAT_MARKER = "privat.txt";

	private final IBenutzerService benutzerService;

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final IProtokollService protokollService;

	/**
	 * Erzeugt eine Instanz von DownloadDelegate
	 */
	@Inject
	public DownloadDelegate(final ILehrerkontoDao lehrerkontoDao, final IPrivatkontoDao privatkontoDao,
		final IProtokollService protokollService, final IBenutzerService benutzerService) {
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.protokollService = protokollService;
		this.benutzerService = benutzerService;
	}

	/**
	 * Prüft die Vorbedingungen für den Download.
	 *
	 * @param benutzerUuid String die UUID des Benutzers.
	 * @param wettbewerbsjahr String das aktuelle Wettbewerbsjahr.
	 * @param downloadPath String pfad zum Download-Verzeichnis.
	 * @throws UnknownAccountException
	 * @throws DisabledAccountException
	 * @throws PreconditionFailedException als message kommt der key aus ApplicationMessages_de.properties zurück.
	 */
	public IMKVKonto checkPermission(final String benutzerUuid, final String wettbewerbsjahr, final String downloadPath)
		throws UnknownAccountException, DisabledAccountException, PreconditionFailedException {
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(benutzerUuid);
		if (benutzerkonto == null) {
			throw new UnknownAccountException("kein Benutzerkonto mit uuid " + benutzerUuid + " bekannt");
		}
		if (benutzerkonto.isGesperrt()) {
			final Ereignis ereignis = new Ereignis(Ereignisart.DOWNLOAD_OHNE_RECHT.getKuerzel(), benutzerkonto.getUuid(),
				"Benutzerkonto gesperrt");
			protokollService.protokollieren(ereignis);
			throw new DisabledAccountException("Benutzerkonto " + benutzerUuid + " gesperrt");
		}
		if (!benutzerkonto.isAktiviert()) {
			final Ereignis ereignis = new Ereignis(Ereignisart.DOWNLOAD_OHNE_RECHT.getKuerzel(), benutzerkonto.getUuid(),
				"Benutzerkonto nicht aktiviert");
			protokollService.protokollieren(ereignis);
			throw new DisabledAccountException("Benutzerkonto " + benutzerUuid + " deaktiviert");
		}
		final Optional<IMKVKonto> opt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(benutzerUuid, lehrerkontoDao,
			privatkontoDao);
		final IMKVKonto konto = opt.get();
		final TeilnahmeIdentifierProvider teilnahme = konto.getTeilnahmeZuJahr(wettbewerbsjahr);
		if (teilnahme == null) {
			final Ereignis ereignis = new Ereignis(Ereignisart.DOWNLOAD_OHNE_RECHT.getKuerzel(), konto.getUuid(),
				"keine Teilnahme mit Jahr " + wettbewerbsjahr);
			protokollService.protokollieren(ereignis);
			throw new PreconditionFailedException("download.ohneAnmeldung");
		}
		final Role role = konto.getRole();
		String markerpath = null;
		switch (role) {
		case MKV_LEHRER:
			markerpath = downloadPath + File.separator + freischaltungLehrerMarker;
			break;
		case MKV_PRIVAT:
			markerpath = downloadPath + File.separator + FREISCHALTUNG_PRIVAT_MARKER;
			break;
		default:
			LOG.error(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "unbekannte EnsureRole [{}]", role.toString());
			throw new MKVException("unbekannte Rolle " + role.toString());
		}
		final boolean markerfilePresent = new File(markerpath).isFile();
		// if (!markerfilePresent && !hatSpeziellesDownloadrecht(konto)) {
		if (!markerfilePresent) {
			LOG.warn(GlobalConstants.LOG_PREFIX_DOWNLOAD + "noch nicht freigeschaltet für {}. downloadCode {}", role.toString(),
				benutzerUuid);
			throw new PreconditionFailedException("download.nicht_freigeschaltet");
		}
		return konto;
	}

	/**
	 * Speichert den download. Eventuelle Exceptions werden abgefangen und geloggt.
	 *
	 * @param konto IMKVKonto
	 * @param dateiname String
	 * @param wettbewerbsjahr String
	 * @return IMKVKonto das persistierte Konto.
	 */
	public IMKVKonto downloadStillSpeichern(final IMKVKonto konto, final String dateiname, final String wettbewerbsjahr) {
		if (konto == null) {
			throw new IllegalArgumentException("konto null");
		}
		if (wettbewerbsjahr == null) {
			throw new IllegalArgumentException("wettbewerbsjahr null");
		}
		try {
			IDownload download = konto.findDownload(dateiname, wettbewerbsjahr);
			if (download == null) {
				download = konto.createBlankDownload();
				final Downloaddaten downloaddaten = new Downloaddaten();
				downloaddaten.setDateiname(dateiname);
				downloaddaten.setJahr(wettbewerbsjahr);
				download.setDownloaddaten(downloaddaten);
				konto.addDownload(download);
			}
			download.getDownloaddaten().anzahlErhoehen();

			IMKVKonto result = null;

			switch (konto.getRole()) {
			case MKV_LEHRER:
				result = lehrerkontoDao.persist((Lehrerkonto) konto);
				break;
			case MKV_PRIVAT:
				result = privatkontoDao.persist((Privatkonto) konto);
				break;
			default:
				LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "konto hatte unerwartete EnsureRole {}",
					konto.getRole().toString());
				throw new MKVException("Konto hatte unerwartete EnsureRole");
			}

			return result;
		} catch (final EgladilConcurrentModificationException e) {
			LOG.warn(GlobalConstants.LOG_PREFIX_DOWNLOAD
				+ "Konkurrierendes Update auf Konto {} beim Speichern des Downloads mit dateiname {}.", konto, dateiname);
			return konto;
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + "Unerwartete Exception beim Speichern des Downloads: Konto " + konto
				+ ", Dateiname=" + dateiname + ": " + e.getMessage(), e);
			return konto;
		}
	}

	public List<DateiInfo> getDateiInfosWettbewerbsdownloads(final String downloadPath) {

		final ArrayList<DateiInfo> result = new ArrayList<>();
		final File verzeichnis = new File(downloadPath);
		if (!verzeichnis.exists() || !verzeichnis.isDirectory() || !verzeichnis.canRead()) {
			throw new MKVException(
				"File " + downloadPath + " existiert nicht, ist kein Verzeichnis oder kann nicht gelesen werden.");
		}
		final Properties props = new Properties();
		try {
			try (InputStream in = new FileInputStream(new File(downloadPath + File.separator + DATEINAMEN_BESCHREIBUNGEN))) {
				props.load(in);
			}
		} catch (final IOException e) {
			throw new MKVException("kann Dateinamen-Beschreibungen nicht lesen: " + e.getMessage(), e);
		}
		final File[] inhalt = verzeichnis.listFiles();
		for (final File file : inhalt) {
			if (!file.isDirectory()) {
				final String dateiname = file.getName();
				final String val = props.getProperty(dateiname);
				if (val != null) {
					DateiInfo info = null;
					final DateiKategorie kategorie = DateiKategorie.valueOfDateiname(dateiname);
					final Klassenstufe klassenstufe = Klassenstufe.getKlasseFuerDownload(dateiname);
					switch (kategorie) {
					case aufgaben:
					case hilfsmittel:
						info = new DateiInfo(kategorie, klassenstufe);
						break;
					default:
						break;
					}
					if (info != null) {
						info.setDateiname(dateiname);
						if (dateiname.toLowerCase().contains(".zip")) {
							info.addRolename(Role.MKV_PRIVAT.toString());
						}
						info.setDateiUrl(WETTEWERBRESOURCE_URL + "/" + dateiname);
						info.setBeschreibung(props.getProperty(dateiname));
						info.setGroesse(getFileSizeInfo(file));
						if (!result.contains(info)) {
							result.add(info);
						}
					}
				} else {
					LOG.debug("kein Eintrag mit dateiname = {} in {} vorhanden. Wird ignoriert.", dateiname,
						DATEINAMEN_BESCHREIBUNGEN);
				}
			}
		}
		Collections.sort(result, new DateiInfoBeschreibungSorter());
		return result;
	}

	public List<DateiInfo> getInfosStaticHtmlContent(final String downloadPath) {

		final ArrayList<DateiInfo> result = new ArrayList<>();
		final File verzeichnis = new File(downloadPath);
		if (!verzeichnis.exists() || !verzeichnis.isDirectory() || !verzeichnis.canRead()) {
			throw new MKVException(
				"File " + downloadPath + " existiert nicht, ist kein Verzeichnis oder kann nicht gelesen werden.");
		}

		final File[] inhalt = verzeichnis.listFiles();

		for (final File file : inhalt) {
			if (!file.isDirectory()) {
				final DateiInfo info = new DateiInfo();
				final String dateiname = file.getName();
				info.setDateiname(dateiname);
				info.setGroesse(getFileSizeInfo(file));
				info.setDateiUrl(HTMLRESOURCE_URL + "/" + dateiname);
				result.add(info);
			}
		}

		return result;
	}

	public String getAsString(final String filePath) throws IOException {
		try (final FileInputStream fis = new FileInputStream(new File(filePath)); StringWriter sw = new StringWriter()) {
			IOUtils.copy(fis, sw, "UTF-8");
			return sw.toString();
		}

	}

	private String getFileSizeInfo(final File file) {
		final long bytes = file.length();
		final float kB = bytes / 1024;
		return kB + " kB";
	}

	void setFreischaltungLehrerMarker(final String freischaltungLehrerMarker) {
		this.freischaltungLehrerMarker = freischaltungLehrerMarker;
	}

}
