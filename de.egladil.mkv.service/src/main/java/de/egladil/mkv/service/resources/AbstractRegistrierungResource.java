//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.UuidPayload;
import de.egladil.mkv.service.confirmation.impl.ConfirmationStatus;
import de.egladil.mkv.service.payload.request.AbstractMKVRegistrierung;
import de.egladil.mkv.service.registrierung.impl.CreateRegistrierungSuccessMessageCommand;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * AbstractRegistrierungResource
 */
public abstract class AbstractRegistrierungResource {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractRegistrierungResource.class);

	protected static final String ACTIVATION_EXPIRED_RESOURCE = "/html/activationExpired.html";

	protected static final String ACTIVATION_SUCCESS_RESOURCE = "/html/activationSuccess.html";

	protected static final String ACTIVATION_FAILED_RESOURCE = "/html/activationFailed.html";

	protected static final String ACTIVATION_BAD_REQUEST_RESOURCE = "/html/activationBadRequest.html";

	protected static final String ACTIVATION_DELETED_RESOURCE = "/html/activationDeleted.html";

	protected final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	protected ValidationDelegate validationDelegate = new ValidationDelegate();

	protected final ResourceExceptionHandler exceptionHandler;

	private String downloadPath;

	private String benutzerUuidImTest;

	private boolean test;

	/**
	 * AbstractRegistrierungResource
	 */
	public AbstractRegistrierungResource() {
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
	}

	/**
	 * Liefert die Membervariable aktuellesWettebwerbsjahr
	 *
	 * @return die Membervariable aktuellesWettebwerbsjahr
	 */
	protected String getAktuellesWettebwerbsjahr() {
		return KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	/**
	 * Zu Testzwecken einen Mock setzen.
	 *
	 * @param validationDelegate neuer Wert der Membervariablen validationDelegate
	 */
	protected void setValidationDelegate(final ValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}

	/**
	 * Wird durch den Benutzerkonto aktivieren- Link aufgerufen. Alle Exceptions außer der EgladilWebappException mit
	 * 404 werden gleich behandelt: Eintrag ins log und Rückgabe von ACTIVATION_FAILED_RESOURCE.
	 *
	 * @param confirmationCode
	 * @return InputStream
	 */
	public InputStream confirm(@QueryParam("t") final String confirmationCode) {
		try {

			validationDelegate.check(new UuidPayload(confirmationCode), UuidPayload.class);

			final ConfirmationStatus confirmationStatus = registrierungBestaetigen(confirmationCode);
			if (confirmationStatus == null) {
				LOG.error("confirmationService.jetztRegistrierungBestaetigen returned null");
				return getClass().getResourceAsStream(ACTIVATION_FAILED_RESOURCE);
			}
			String pathname = null;
			switch (confirmationStatus) {
			case expiredActivation:
				pathname = downloadPath + ACTIVATION_EXPIRED_RESOURCE;
				return new FileInputStream(new File(pathname));
			case normalActivation:
			case repeatedActivation:
				pathname = downloadPath + ACTIVATION_SUCCESS_RESOURCE;
				return new FileInputStream(new File(pathname));
			default:
				LOG.error("unbekannter ConfirmationStatus {}", confirmationStatus.name());
				return getClass().getResourceAsStream(ACTIVATION_FAILED_RESOURCE);
			}
		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			final int status = e.getResponse().getStatus();
			final CustomResponseStatus crs = CustomResponseStatus.valueOfStatusCode(status);
			switch (crs) {
			case NOT_FOUND:
				return getClass().getResourceAsStream(ACTIVATION_DELETED_RESOURCE);
			case BAD_REQUEST:
			case VALIDATION_ERRORS:
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "confirmationCode ungueltig - [{}]", confirmationCode);
				return getClass().getResourceAsStream(ACTIVATION_BAD_REQUEST_RESOURCE);
			default:
				return getClass().getResourceAsStream(ACTIVATION_FAILED_RESOURCE);
			}
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return getClass().getResourceAsStream(ACTIVATION_FAILED_RESOURCE);
		}
	}

	/**
	 * Spezielle Teile des Bestätigens.
	 *
	 * @param confirmationCode
	 * @return ConfirmationStatus
	 */
	protected abstract ConfirmationStatus registrierungBestaetigen(String confirmationCode)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException;

	protected boolean isNeuanmeldungFreigegeben() {
		return KontextReader.getInstance().getKontext().isNeuanmeldungFreigegeben();
	}

	/**
	 * Erzeugt ein neues Benutzerkonto sowie eine neue Teilnahme im aktuellen Jahr.<br>
	 * <br>
	 * Responsecodes.
	 * <ul>
	 * <li>200: OK</li>
	 * <li>412: precondition failed wenn Neuanmeldung nicht freigeschaltet (siehe mkvservice-deb.yml</li>
	 * <li>500: Servererror</li>
	 * <li>900: unique index in DB verletzt</li>
	 * <li>901: konkurrierendes Update</li>
	 * <li>902: Validierungsfehler</li>
	 * </ul>
	 *
	 * @param payload T der Payload
	 * @param clazz Class der Type des Payloads
	 * @return Response mit MKVApiResonse.
	 */
	protected <T extends AbstractMKVRegistrierung> Response doRegister(final T payload, final Class<T> clazz) {

		if (KontextReader.getInstance().getKontext().isDumpPayload()) {
			LOG.info("Payload-Dump: {}", payload);
		}


		final String aktuellesWettebwerbsjahr = this.getAktuellesWettebwerbsjahr();
		final boolean neuanmeldungFreigegeben = this.isNeuanmeldungFreigegeben();
		if (!isNeuanmeldungFreigegeben()) {
			LOG.warn(
				"/registrierungen/lehrer oder /registrierungen/privat wurde aufgerufen, obwohl das Wettbewerbsjahr noch nicht eröffnet ist: aktuelles Wettbewerbsjahr {}. Konfiguration auf dem Client prüfen!",
				aktuellesWettebwerbsjahr);
			// final String msg =
			// MessageFormat.format(applicationMessages.getString("teilnahme.geschlossenerWerbewerb"),
			// new Object[] { aktuellesWettebwerbsjahr });
			// final PublicMessage pm = new PublicMessage(msg, MessageLevel.WARN);
			// return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(pm).build();
		}
		try {
			validationDelegate.check(payload, clazz);
			final Aktivierungsdaten aktivierungsdaten = kontaktAnlegenUndZumWettbewerbAnmelden(payload);
			final String message = new CreateRegistrierungSuccessMessageCommand(neuanmeldungFreigegeben, aktuellesWettebwerbsjahr)
				.getRegistrierungSuccessMessage(payload.isGleichAnmelden());
			if (aktivierungsdaten != null) {
				LOG.debug("{}", aktivierungsdaten);
			}
			return Response.status(Status.CREATED).entity(new APIResponsePayload(APIMessage.info(message))).build();
		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilConcurrentModificationException e) {
			// Das sollte nicht vorkommen, weil es sich um eine Registrierung handelt. Dann müsste der Anwender die
			// Daten nochmals senden.
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Konkurrierendes Update beim Anlegen eines MKV-Kontos.");
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final InvalidMailAddressException e) {
			if (e.getAllInvalidAdresses().contains(payload.getEmail())) {
				LOG.warn(
					GlobalConstants.LOG_PREFIX_MAILVERSAND
						+ " Die Registrierung konnte wegen Invalid Address nicht abgeschlossen werden: invalid addresses {}",
					PrettyStringUtils.collectionToDefaultString(e.getAllInvalidAdresses()));
				return Response.status(Status.BAD_REQUEST)
					.entity(
						new APIResponsePayload(APIMessage.error(applicationMessages.getString("Registration.error.invalidMail"))))
					.build();
			}
			LOG.error(GlobalConstants.LOG_PREFIX_MAILVERSAND
				+ " Die Registrierung konnte wegen eines Fehlers in der Mailkonfiguration nicht erfolgreich beendet werden. "
				+ e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new APIResponsePayload(APIMessage.error(applicationMessages.getString("Registration.error.mailconfig"))))
				.build();
		} catch (final EgladilMailException e) {
			LOG.error(GlobalConstants.LOG_PREFIX_MAILVERSAND
				+ " Die Registrierung konnte wegen eines Fehlers beim Versenden der Mail nicht erfolgreich beendet werden - "
				+ e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new de.egladil.common.webapp.APIResponsePayload(
				APIMessage.error(applicationMessages.getString("Registration.error.mailconfig")))).build();
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception beim Registrieren: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	/**
	 * Spezielle Implementierung dieser Aktion. Seit 4.0 erfolgt das Anlegen einer Teilnahme kontextabhängig (Checkbox
	 * beim Registrieren und Anwendungskonfiguration neuanmeldung freigeschaltet).
	 *
	 * @param payload AbstractMKVRegistrierung
	 * @return Aktivierungsdaten
	 */
	protected abstract <T extends AbstractMKVRegistrierung> Aktivierungsdaten kontaktAnlegenUndZumWettbewerbAnmelden(T payload)
		throws EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException,
		EgladilMailException;

	/**
	 * Setzt die Membervariable
	 *
	 * @param rootUrl neuer Wert der Membervariablen rootUrl
	 */
	public void setDownloadPath(final String rootUrl) {
		this.downloadPath = rootUrl;
	}

	/**
	 * Hat nur im IT Sinn.
	 *
	 * @return
	 */
	public String getBenutzerUuidImTest() {
		return benutzerUuidImTest;
	}

	/**
	 * Kennzeichnet aufruf als Test.
	 *
	 * @param test
	 */
	public void setTest(final boolean test) {
		this.test = test;
	}

	/**
	 * Liefert die Membervariable test
	 *
	 * @return die Membervariable test
	 */
	protected boolean isTest() {
		return test;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param benutzerUuidImTest neuer Wert der Membervariablen benutzerUuidImTest
	 */
	protected void setBenutzerUuidImTest(final String benutzerUuidImTest) {
		this.benutzerUuidImTest = benutzerUuidImTest;
	}
}
