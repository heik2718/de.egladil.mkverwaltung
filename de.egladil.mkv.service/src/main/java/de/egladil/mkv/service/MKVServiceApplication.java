//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service;

import java.security.Principal;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;

import de.egladil.bv.aas.auth.CsrfFilter;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.common.persistence.EgladilPersistFilter;
import de.egladil.common.webapp.AngularJsonVulnerabilityProtectionInterceptor;
import de.egladil.common.webapp.exception.EgladilLoggingExceptionMapper;
import de.egladil.common.webapp.exception.JsonProcessingExceptionMapper;
import de.egladil.common.webapp.exception.LoggingConstraintViolationExceptionMapper;
import de.egladil.email.service.IMailservice;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.service.config.MKVModule;
import de.egladil.mkv.service.config.MKVServiceConfiguration;
import de.egladil.mkv.service.filters.CSPFilter;
import de.egladil.mkv.service.filters.SecureHeadersFilter;
import de.egladil.mkv.service.health.BVHealthCheck;
import de.egladil.mkv.service.health.ErrorResourceHealthCheck;
import de.egladil.mkv.service.health.KatalogResourceHealthCheck;
import de.egladil.mkv.service.health.MailserviceHealthCheck;
import de.egladil.mkv.service.health.SessionResourceHealthCheck;
import de.egladil.mkv.service.resources.AdvResource;
import de.egladil.mkv.service.resources.AuswertungsgruppeResource;
import de.egladil.mkv.service.resources.ErrorResource;
import de.egladil.mkv.service.resources.FileResource;
import de.egladil.mkv.service.resources.KatalogResource;
import de.egladil.mkv.service.resources.LehrerregistrierungResource;
import de.egladil.mkv.service.resources.LogResource;
import de.egladil.mkv.service.resources.MitgliederResource;
import de.egladil.mkv.service.resources.PrivatregistrierungResource;
import de.egladil.mkv.service.resources.SessionResource;
import de.egladil.mkv.service.resources.StaticHtmlResource;
import de.egladil.mkv.service.resources.TeilnahmeResource;
import de.egladil.mkv.service.resources.TeilnehmerResource;
import de.egladil.mkv.service.resources.TempPasswordResource;
import de.egladil.mkv.service.resources.UrkundenResource;
import de.egladil.mkv.service.resources.WettbewerbAnmeldungResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper;
import io.dropwizard.setup.Environment;

/**
 * MKVServiceApplication
 */
public class MKVServiceApplication extends Application<MKVServiceConfiguration> {

	private static final String SERVICE_NAME = "MKV-REST-API";

	private static final String BEARER_PREFIX = "Bearer";

	private static final Logger LOG = LoggerFactory.getLogger(MKVServiceApplication.class);

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}
	}

	public static void main(final String[] args) {
		try {
			final MKVServiceApplication application = new MKVServiceApplication();
			application.run(args);
		} catch (final Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see io.dropwizard.Application#getName()
	 */
	@Override
	public String getName() {
		return SERVICE_NAME;
	}

	/**
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration,
	 *      io.dropwizard.setup.Environment)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(final MKVServiceConfiguration configuration, final Environment environment) throws Exception {
		LOG.info("configRoot: {}", configuration.getConfigRoot());

		// final String aktuellesWettbewerbsjahr =
		// configuration.getAktuellesWettbewerbsjahr();
		// if (StringUtils.isBlank(aktuellesWettbewerbsjahr)) {
		// throw new EgladilConfigurationException("Das aktuelle Wettbewerbsjahr ist
		// nicht gesetzt: mkvservice-deb.yml
		// prüfen!");
		// }
		final String downloadPath = configuration.getDownloadPath();

		configureCors(configuration, environment);

		configureErrorPages(environment);
		configureExceptionMappers(environment);

		final Injector injector = Guice.createInjector(new MKVModule(configuration.getConfigRoot()));

		initPersistFilter(environment, injector);
		initSecureHeaders(environment);

		// authenticator ist hier ein CachingAccessTokenAuthenticator
		final Authenticator<String, Principal> authenticator = injector.getInstance(Authenticator.class);
		final OAuthCredentialAuthFilter<Principal> oAuthFilter = new OAuthCredentialAuthFilter.Builder<Principal>()
				.setAuthenticator(authenticator).setPrefix(BEARER_PREFIX).buildAuthFilter();
		environment.jersey().register(new AuthDynamicFeature(oAuthFilter));
		// der Parameter Principal.class in new
		// AuthValueFactoryProvider.Binder<>(Principal.class) sorgt dafür, dass der
		// vom Authenticator zuückgegebene Principal als Parameter
		// in die Methode der REST-Resource-Klassenstufe gesetzt wird. Muss
		// übereinstimmen mit
		// OAuthCredentialAuthFilter.Builder<Principal>! Sonst ist der Parameter null!!!
		environment.jersey().register(new AuthValueFactoryProvider.Binder<>(Principal.class));
		environment.jersey().register(RolesAllowedDynamicFeature.class);
		environment.jersey().register(MultiPartFeature.class);

//		environment.jersey().register(AngularJsonVulnerabilityProtectionInterceptor.class);
		registerCsrfFilter(environment, injector);

		KontextReader.getInstance().init(configuration.getConfigRoot());

		final BVHealthCheck bvHealthCheck = injector.getInstance(BVHealthCheck.class);
		environment.healthChecks().register("BV", bvHealthCheck);

		final ErrorResource errorResource = new ErrorResource();
		environment.jersey().register(errorResource);

		final ErrorResourceHealthCheck errorHealthCheck = new ErrorResourceHealthCheck(errorResource);
		environment.healthChecks().register("errorpages", errorHealthCheck);

		final KatalogResource katalogResource = injector.getInstance(KatalogResource.class);
		environment.jersey().register(katalogResource);

		final KatalogResourceHealthCheck katalogHealthCheck = new KatalogResourceHealthCheck(katalogResource);
		environment.healthChecks().register("laender", katalogHealthCheck);

		final LehrerregistrierungResource registerLehrerResource = injector
				.getInstance(LehrerregistrierungResource.class);
		registerLehrerResource.setDownloadPath(downloadPath);
		environment.jersey().register(registerLehrerResource);

		final PrivatregistrierungResource registerPrivatpersonResource = injector
				.getInstance(PrivatregistrierungResource.class);
		registerPrivatpersonResource.setDownloadPath(downloadPath);
		environment.jersey().register(registerPrivatpersonResource);

		final MitgliederResource mitgliederResource = injector.getInstance(MitgliederResource.class);
		// mitgliederResource.setDownloadPath(configuration.getDownloadPath());
		environment.jersey().register(mitgliederResource);

		final SessionResource sessionResource = injector.getInstance(SessionResource.class);
		// sessionResource.setDownloadPath(configuration.getDownloadPath());
		environment.jersey().register(sessionResource);

		final SessionResourceHealthCheck sessionHealthCheck = new SessionResourceHealthCheck(sessionResource);
		environment.healthChecks().register("session", sessionHealthCheck);

		final WettbewerbAnmeldungResource wettbewerbAnmeldungResource = injector
				.getInstance(WettbewerbAnmeldungResource.class);
		environment.jersey().register(wettbewerbAnmeldungResource);

		final TempPasswordResource tempPasswordResource = injector.getInstance(TempPasswordResource.class);
		environment.jersey().register(tempPasswordResource);

		final IMailservice mailService = injector.getInstance(IMailservice.class);
		final MailserviceHealthCheck mailserviceHealthCheck = new MailserviceHealthCheck(mailService);
		environment.healthChecks().register("mailservice", mailserviceHealthCheck);

		final FileResource fileResoure = injector.getInstance(FileResource.class);
		fileResoure.setDownloadPath(downloadPath);
		fileResoure.setUploadPath(configuration.getUploadPath());
		fileResoure.setMaxFileSizeText(configuration.getMaxFileSizeText());
		fileResoure.setSandboxPath(configuration.getSandboxPath());
		fileResoure.setRohdatenGenerieren(configuration.isRohdatenGenerieren());
		fileResoure.setMaxFileSize(configuration.getMaxFileSize());
		environment.jersey().register(fileResoure);

		final TeilnehmerResource teilnehmerResource = injector.getInstance(TeilnehmerResource.class);
		environment.jersey().register(teilnehmerResource);

		final UrkundenResource urkundenResource = injector.getInstance(UrkundenResource.class);
		urkundenResource.setHateoasApiUrl(configuration.getHateoasApiUrl());
		environment.jersey().register(urkundenResource);

		final AuswertungsgruppeResource auswertungsgruppeResource = injector
				.getInstance(AuswertungsgruppeResource.class);
		environment.jersey().register(auswertungsgruppeResource);

		final TeilnahmeResource teilnahmeResource = injector.getInstance(TeilnahmeResource.class);
		environment.jersey().register(teilnahmeResource);

		final StaticHtmlResource staticHtmlResource = injector.getInstance(StaticHtmlResource.class);
		staticHtmlResource.setDownloadPath(configuration.getDownloadPath());
		environment.jersey().register(staticHtmlResource);

		final AdvResource advResource = injector.getInstance(AdvResource.class);
		advResource.setHateoasApiUrl(configuration.getHateoasApiUrl());
		advResource.setDownloadPath(configuration.getDownloadPath());
		environment.jersey().register(advResource);

		environment.jersey().register(injector.getInstance(LogResource.class));
	}

	/**
	 * Statt der standard-Jetty-Htmlseiten wird hier Plain Old JSON ohne sensible
	 * Informationen zurückgegeben
	 *
	 * @param environment
	 */
	private void configureErrorPages(final Environment environment) {
		final ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
		errorHandler.addErrorPage(401, "/errorpages/401");
		errorHandler.addErrorPage(404, "/errorpages/404");
		errorHandler.addErrorPage(500, "/errorpages/500");
		errorHandler.addErrorPage(900, "/errorpages/900");
		environment.getApplicationContext().setErrorHandler(errorHandler);
	}

	/**
	 * Registriert adaptierte ExceptionMapper aus
	 * io.dropwizard.server.AbstractServerFactory.
	 *
	 * @param environment
	 */
	private void configureExceptionMappers(final Environment environment) {
		environment.jersey().register(new EgladilLoggingExceptionMapper());
		environment.jersey().register(new JsonProcessingExceptionMapper());
		environment.jersey().register(new LoggingConstraintViolationExceptionMapper());
		environment.jersey().register(new EarlyEofExceptionMapper());
	}

	private void configureCors(final MKVServiceConfiguration configuration, final Environment environment) {
		// ab Dropwizard 0.8.1 erforderlich
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
		final FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORSFilter",
				CrossOriginFilter.class);

		filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
				"Content-Type,Accept,Authorization,Origin,X-XSRF-TOKEN");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, configuration.getAllowedOrigins());
		filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
		filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM,
				"Content-Type,Accept,Authorization,Origin,X-XSRF-TOKEN,Content-Disposition");
		// filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM, "true");

		// Add URL mapping
		// FIXME: secure?
		filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
	}

	private void initPersistFilter(final Environment environment, final Injector injector) {
		final Key<EgladilPersistFilter> keyBV = Key.get(EgladilPersistFilter.class, BVPersistenceUnit.class);
		final Key<EgladilPersistFilter> keyMKV = Key.get(EgladilPersistFilter.class, MKVPersistenceUnit.class);

		final EgladilPersistFilter pfBV = injector.getInstance(keyBV);
		final EgladilPersistFilter pfMKV = injector.getInstance(keyMKV);

		final FilterRegistration.Dynamic bvFilter = environment.servlets().addFilter("BVPersistFilter", pfBV);
		final FilterRegistration.Dynamic mkvFilter = environment.servlets().addFilter("MKVPersistFilter", pfMKV);

		bvFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
		mkvFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
	}

	private void initSecureHeaders(final Environment environment) {
		{
			final FilterRegistration.Dynamic filter = environment.servlets().addFilter("CSPFilter", CSPFilter.class);
			filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		}
		{
			final FilterRegistration.Dynamic filter = environment.servlets().addFilter("SecureHeadersFilter",
					SecureHeadersFilter.class);
			filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		}
	}

	/**
	 * Registriert einen CSRF-Filter
	 *
	 * @param environment
	 * @param injector
	 * @param test
	 */
	private void registerCsrfFilter(final Environment environment, final Injector injector) {
		final CsrfFilter csrfFilter = injector.getInstance(CsrfFilter.class);

		final FilterRegistration.Dynamic filter = environment.servlets().addFilter("csrfFilter", csrfFilter);
		filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "/*");
	}

	// @Override
	// public void initialize(final Bootstrap<MKVServiceConfiguration> bootstrap) {
	// super.initialize(bootstrap);
	// bootstrap.addBundle(new SwaggerBundle<MKVServiceConfiguration>() {
	// @Override
	// protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(final
	// MKVServiceConfiguration configuration) {
	// return configuration.swaggerBundleConfiguration;
	// }
	// });
	// }
}
