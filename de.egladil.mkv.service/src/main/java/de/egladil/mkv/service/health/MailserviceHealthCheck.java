//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.health;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.codahale.metrics.health.HealthCheck;

import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.IMailservice;

/**
 * MailserverHeathCheck
 */
public class MailserviceHealthCheck extends HealthCheck {

	private final IMailservice mailService;

	/**
	 * Erzeugt eine Instanz von MailserverHeathCheck
	 */
	public MailserviceHealthCheck(IMailservice mailService) {
		this.mailService = mailService;
	}

	/**
	 * @see com.codahale.metrics.health.HealthCheck#check()
	 */
	@Override
	protected Result check() throws Exception {
		IEmailDaten maildaten = new IEmailDaten() {

			@Override
			public String getText() {
				return "Der Mailserver läuft.";
			}

			@Override
			public Collection<String> getHiddenEmpfaenger() {
				return Collections.emptyList();
			}

			@Override
			public String getEmpfaenger() {
				return "info@egladil.de";
			}

			@Override
			public String getBetreff() {
				return "MKV: Mailserverhealthcheck";
			}

			@Override
			public List<String> alleEmpfaengerFuersLog() {
				return Arrays.asList(new String[] { getEmpfaenger() });
			}
		};
		try {
			mailService.sendMail(maildaten);
			return Result.healthy();
		} catch (EgladilMailException e) {
			return Result.unhealthy("Mailserver nicht erreichbar");
		}
	}

}
