//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.Passwort;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.service.validation.ValidMKVRegistrierung;

/**
 * AbstractRegistrierungsanfrage. Unterscheidung nach type: [privat|lehrer]
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = Privatregistrierung.class, name = "privat"),
	@Type(value = Lehrerregistrierung.class, name = "lehrer") })
@ValidMKVRegistrierung
public abstract class AbstractMKVRegistrierung implements IMKVRegistrierung {

	/** */
	private static final long serialVersionUID = 1L;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	private String vorname;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	private String nachname;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	private String email;

	@NotNull
	@Passwort
	private String passwort;

	@NotNull
	@Passwort
	private String passwortWdh;

	@Honeypot
	private String kleber;

	private boolean agbGelesen;

	private boolean automatischBenachrichtigen;

	private boolean gleichAnmelden = true;

	/**
	 * Liefert die Membervariable agbGelesen
	 *
	 * @return die Membervariable agbGelesen
	 */
	public boolean isAgbGelesen() {
		return agbGelesen;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param agbGelesen neuer Wert der Membervariablen agbGelesen
	 */
	public void setAgbGelesen(final boolean agbGelesen) {
		this.agbGelesen = agbGelesen;
	}

	/**
	 * Erzeugt eine Instanz von AbstractRegistrierungsanfrage
	 */
	public AbstractMKVRegistrierung() {
	}

	/**
	 *
	 * Erzeugt eine Instanz von AbstractMKVRegistrierung
	 */
	public AbstractMKVRegistrierung(final String vorname, final String nachname, final String email, final String passwort,
		final String passwortWdh, final String kleber, final boolean benachrichtigen) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.email = email;
		this.passwort = passwort;
		this.passwortWdh = passwortWdh;
		this.kleber = kleber;
		this.automatischBenachrichtigen = benachrichtigen;
	}

	@Override
	public String toBotLog() {
		return "Registrierungsanfrage [vorname=" + vorname + ", nachname=" + nachname + ", email=" + email + ", passwort="
			+ passwort + ", passwortWdh=" + passwortWdh + ", kleber=" + kleber + ", agbGelesen=" + agbGelesen
			+ ", automatischBenachrichtigen=" + automatischBenachrichtigen + "]";
	}

	@Override
	public String toString() {
		return "Registrierungsanfrage [vorname=" + vorname + ", nachname=" + nachname + ", email=" + email + ", agbGelesen="
			+ agbGelesen + ", automatischBenachrichtigen=" + automatischBenachrichtigen + "]";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AbstractMKVRegistrierung other = (AbstractMKVRegistrierung) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getVorname()
	 */
	@Override
	public String getVorname() {
		return vorname;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param vorname neuer Wert der Membervariablen vorname
	 */
	public void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getNachname()
	 */
	@Override
	public String getNachname() {
		return nachname;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param nachname neuer Wert der Membervariablen nachname
	 */
	public void setNachname(final String nachname) {
		this.nachname = nachname;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getEmail()
	 */
	@Override
	public String getEmail() {
		return email;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param email neuer Wert der Membervariablen email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getKleber()
	 */
	@Override
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getPasswort()
	 */
	@Override
	public String getPasswort() {
		return passwort;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param passwort neuer Wert der Membervariablen passwort
	 */
	public void setPasswort(final String passwort) {
		this.passwort = passwort;
	}

	/**
	 * Liefert die Membervariable passwortWdh
	 *
	 * @return die Membervariable passwortWdh
	 */
	public String getPasswortWdh() {
		return passwortWdh;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param passwortWdh neuer Wert der Membervariablen passwortWdh
	 */
	public void setPasswortWdh(final String passwortWdh) {
		this.passwortWdh = passwortWdh;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#isAutomatischBenachrichtigen()
	 */
	@Override
	public boolean isAutomatischBenachrichtigen() {
		return automatischBenachrichtigen;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param automatischBenachrichtigen neuer Wert der Membervariablen automatischBenachrichtigen
	 */
	public void setAutomatischBenachrichtigen(final boolean automatischBenachrichtigen) {
		this.automatischBenachrichtigen = automatischBenachrichtigen;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getKontaktdaten()
	 */
	@Override
	public Person getKontaktdaten() {
		return new Person(vorname, nachname);
	}

	public final boolean isGleichAnmelden() {
		return gleichAnmelden;
	}

	public final void setGleichAnmelden(final boolean gleichAnmelden) {
		this.gleichAnmelden = gleichAnmelden;
	}

}
