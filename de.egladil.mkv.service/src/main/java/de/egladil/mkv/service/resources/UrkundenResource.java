//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.jetty.http.HttpMethod;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ByteArrayStreamingOutput;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.domain.auswertungen.Farbschema;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.DownloadcodePayload;
import de.egladil.mkv.persistence.payload.request.SchuleUrkundenauftrag;
import de.egladil.mkv.persistence.payload.request.TeilnehmerUrkundenauftrag;
import de.egladil.mkv.persistence.payload.response.DownloadArt;
import de.egladil.mkv.persistence.payload.response.DownloadCode;
import de.egladil.mkv.persistence.payload.response.PublicFarbschema;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.utils.ClasspathBinaryResourceReader;
import de.egladil.mkv.persistence.utils.ClasspathResourceByteArray;
import de.egladil.mkv.persistence.utils.ImageBase64Converter;
import de.egladil.mkv.persistence.utils.ImageSerialisierung;
import de.egladil.mkv.service.utils.TeilnahmeIdentifierDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * UrkundenResource
 */
@Singleton
@Path("")
public class UrkundenResource {

	private static final Logger LOG = LoggerFactory.getLogger(UrkundenResource.class);

	private String hateoasApiUrl;

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private final AuswertungService auswertungService;

	private final SchulstatistikService schulstatistikService;

	private final AuthorizationService authorizationService;

	private List<PublicFarbschema> vordefinierteFarbschema = new ArrayList<>();

	private ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * UrkundenResource
	 */
	@Inject
	public UrkundenResource(final AuswertungService auswertungService, final AuthorizationService authorizationService,
		final SchulstatistikService schulstatistikService) {
		this.auswertungService = auswertungService;
		this.authorizationService = authorizationService;
		this.schulstatistikService = schulstatistikService;
	}

	@POST
	@Path("/auswertungen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/urkunden")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response einzelurkundenGenerieren(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, final TeilnehmerUrkundenauftrag payload) {

		String benutzerUuid = null;
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			if (principal == null) {
				throw new MKVException("principal null");
			}
			benutzerUuid = principal.getName();
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			validationDelegate.check(payload, TeilnehmerUrkundenauftrag.class);

			final Optional<String> optDownloadCode = auswertungService.generiereAuswertungFuerEinzelteilnehmer(benutzerUuid,
				payload, teilnahmeIdentifier);

			if (!optDownloadCode.isPresent()) {
				final String text = applicationMessages.getString("auswertung.einzelurkunden.keineDaten");
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(text));
				return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(entity).build();
			}

			String text = null;
			if (payload.getTeilnehmerKuerzel().length == 1) {
				text = applicationMessages.getString("auswertung.einzelurkunden.success.eine");
			} else {
				text = applicationMessages.getString("auswertung.einzelurkunden.success.mehrere");
			}
			final String downloadCode = optDownloadCode.get();
			final DownloadCode responsePayload = new DownloadCode(downloadCode, teilnahmeIdentifier, DownloadArt.URKUNDE);
			final String subpath = "/auswertungen/auswertung/" + downloadCode;
			final URI location = new URI(hateoasApiUrl + subpath);

			final HateoasPayload hateoasPayload = new HateoasPayload(downloadCode, subpath);
			hateoasPayload.addLink(new HateoasLink(subpath, "self", HttpMethod.GET.toString(), MediaType.APPLICATION_OCTET_STREAM));

			responsePayload.setHateoasPayload(hateoasPayload);

			final APIMessage msg = APIMessage.info(text);
			final APIResponsePayload entity = new APIResponsePayload(msg, responsePayload);
			return Response.status(Status.CREATED).location(location).entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilWebappException e) {
			LOG.warn("Validierungsfehler payload {}: {}", payload, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error("Exception beim Generieren der Auswertungen {}: {}", payload, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	@POST
	@Path("/auswertungen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/jumpcerts")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response einzelurkundenKaengurusprungGenerieren(@Auth final Principal principal,
		@PathParam(value = "jahr") final String jahr, @PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, final TeilnehmerUrkundenauftrag payload) {

		String benutzerUuid = null;
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			if (principal == null) {
				throw new MKVException("principal null");
			}
			benutzerUuid = principal.getName();
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			validationDelegate.check(payload, TeilnehmerUrkundenauftrag.class);

			final Optional<String> optDownloadCode = auswertungService
				.generiereKaengurusprungurkundenfuerEinzelteilnehmer(benutzerUuid, payload, teilnahmeIdentifier);

			if (!optDownloadCode.isPresent()) {
				final String text = applicationMessages.getString("auswertung.einzelurkunden.keineDaten");
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(text));
				return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(entity).build();
			}

			String text = null;
			if (payload.getTeilnehmerKuerzel().length == 1) {
				text = applicationMessages.getString("auswertung.einzelurkunden.success.eine");
			} else {
				text = applicationMessages.getString("auswertung.einzelurkunden.success.mehrere");
			}
			final String downloadCode = optDownloadCode.get();
			final DownloadCode responsePayload = new DownloadCode(downloadCode, teilnahmeIdentifier, DownloadArt.URKUNDE);
			final String subpath = "/auswertungen/auswertung/" + downloadCode;
			final URI location = new URI(hateoasApiUrl + subpath);

			final HateoasPayload hateoasPayload = new HateoasPayload(downloadCode, subpath);
			hateoasPayload.addLink(new HateoasLink(subpath, "self", HttpMethod.GET.toString(), MediaType.APPLICATION_OCTET_STREAM));

			responsePayload.setHateoasPayload(hateoasPayload);

			final APIMessage msg = APIMessage.info(text);
			final APIResponsePayload entity = new APIResponsePayload(msg, responsePayload);
			return Response.status(Status.CREATED).location(location).entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilWebappException e) {
			LOG.warn("Validierungsfehler payload {}: {}", payload, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error("Exception beim Generieren der Kängurusprungurkunden {}: {}", payload, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	@POST
	@Path("/auswertungen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppe/urkunden")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response schulurkundenGenerieren(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, final SchuleUrkundenauftrag payload) {

		// try {
		// Thread.sleep(2000);
		// } catch (final InterruptedException e1) {
		// //
		// }

		String benutzerUuid = null;
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			if (principal == null) {
				throw new MKVException("principal null");
			}
			benutzerUuid = principal.getName();
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			validationDelegate.check(payload, SchuleUrkundenauftrag.class);

			final Optional<String> optDownloadCode = auswertungService.generiereSchulauswertung(benutzerUuid, payload,
				teilnahmeIdentifier);

			if (!optDownloadCode.isPresent()) {
				final String text = applicationMessages.getString("auswertung.schulurkunden.keineDaten");
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(text));
				return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(entity).build();
			}

			final String text = applicationMessages.getString("auswertung.schulurkunden.success");

			final String downloadCode = optDownloadCode.get();
			final DownloadCode responsePayload = new DownloadCode(downloadCode, teilnahmeIdentifier, DownloadArt.URKUNDE);
			final String subpath = "/auswertungen/auswertung/" + downloadCode;
			final URI location = new URI(hateoasApiUrl + subpath);

			final HateoasPayload hateoasPayload = new HateoasPayload(downloadCode, subpath);
			hateoasPayload.addLink(new HateoasLink(subpath, "self", HttpMethod.GET.toString(), MediaType.APPLICATION_OCTET_STREAM));

			responsePayload.setHateoasPayload(hateoasPayload);

			final APIMessage msg = APIMessage.info(text);
			final APIResponsePayload entity = new APIResponsePayload(msg, responsePayload);
			return Response.status(Status.CREATED).location(location).entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilWebappException e) {
			LOG.warn("Validierungsfehler payload {}: {}", payload, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error("Exception beim Generieren der Auswertungen {}: {}", payload, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	@POST
	@Path("/auswertungen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppe/jumpcerts")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response schulurkundenKaengurusprungGenerieren(@Auth final Principal principal,
		@PathParam(value = "jahr") final String jahr, @PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, final SchuleUrkundenauftrag payload) {

		// try {
		// Thread.sleep(2000);
		// } catch (final InterruptedException e1) {
		// //
		// }

		String benutzerUuid = null;
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			if (principal == null) {
				throw new MKVException("principal null");
			}
			benutzerUuid = principal.getName();
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			validationDelegate.check(payload, SchuleUrkundenauftrag.class);

			final Optional<String> optDownloadCode = auswertungService.generiereKaengurusprungurkundenfuerSchule(benutzerUuid,
				payload, teilnahmeIdentifier);

			if (!optDownloadCode.isPresent()) {
				final String text = applicationMessages.getString("auswertung.schule.urkunde.kaengurusprung.mind.zwei");
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(text));
				return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(entity).build();
			}

			final String text = applicationMessages.getString("auswertung.schulurkunden.success");

			final String downloadCode = optDownloadCode.get();
			final DownloadCode responsePayload = new DownloadCode(downloadCode, teilnahmeIdentifier, DownloadArt.URKUNDE);
			final String subpath = "/auswertungen/auswertung/" + downloadCode;
			final URI location = new URI(hateoasApiUrl + subpath);

			final HateoasPayload hateoasPayload = new HateoasPayload(downloadCode, subpath);
			hateoasPayload.addLink(new HateoasLink(subpath, "self", HttpMethod.GET.toString(), MediaType.APPLICATION_OCTET_STREAM));

			responsePayload.setHateoasPayload(hateoasPayload);

			final APIMessage msg = APIMessage.info(text);
			final APIResponsePayload entity = new APIResponsePayload(msg, responsePayload);
			return Response.status(Status.CREATED).location(location).entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilWebappException e) {
			LOG.warn("Validierungsfehler payload {}: {}", payload, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error("Exception beim Generieren der Auswertungen {}: {}", payload, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	@GET
	@Path("/auswertungen/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertung/{downloadcode}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Timed
	public Response downloadFile(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel,
		@PathParam("downloadcode") final String downloadcode) {

		final String benutzerUuid = principal.getName();
		DownloadcodePayload payload = null;
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			payload = new DownloadcodePayload(downloadcode);
			validationDelegate.check(payload, DownloadcodePayload.class);

			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			// TODO: blöde Namenswahl, da Privatpersonen ihre Urkunden so auch herunterladen.
			final Optional<AuswertungDownload> opt = schulstatistikService.findDownload(downloadcode);

			if (opt.isPresent()) {
				final AuswertungDownload download = opt.get();

				final ByteArrayStreamingOutput result = new ByteArrayStreamingOutput(download.getDaten());
				final ContentDisposition contentDisposition = ContentDisposition.type("attachment")
					.fileName("auswertung_minikaenguru_" + jahr + "_" + downloadcode + ".pdf").build();
				final Response response = Response.ok(result).header("Content-Type", "application/octet-stream")
					.header("Content-Disposition", contentDisposition).build();

				schulstatistikService.deleteDownloadQietly(benutzerUuid, download);
				return response;
			} else {
				LOG.warn("kein Auswertungsdownload mit downloadcode {} vorhanden", downloadcode);
				final APIResponsePayload entity = new APIResponsePayload(
					APIMessage.error(applicationMessages.getString("statistik.schuluebersicht.notFound")));
				return Response.status(Status.NOT_FOUND).entity(entity).build();
			}
		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@GET
	@Path("farbschemas")
	@Timed
	public Response getFarbschema(@Auth final Principal principal) {
		try {
			if (vordefinierteFarbschema.isEmpty()) {
				for (final Farbschema farbschema : Farbschema.values()) {
					final ClasspathResourceByteArray byteArray = new ClasspathResourceByteArray(
						farbschema.getThumbnailResourcePath());
					new ClasspathBinaryResourceReader(byteArray).execute();
					final ImageSerialisierung image = new ImageSerialisierung(byteArray.getData());
					new ImageBase64Converter(image).execute();
					final PublicFarbschema publicFarbschema = PublicFarbschema.fromFarbschema(farbschema);
					publicFarbschema.setThumbnail(image.getBase64Image());
					vordefinierteFarbschema.add(publicFarbschema);
				}
			}

			final APIResponsePayload payload = new APIResponsePayload(APIMessage.info(""), vordefinierteFarbschema);
			return Response.ok().entity(payload).build();
		} catch (final Exception e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	public final void setHateoasApiUrl(final String hateoasApiUrl) {
		this.hateoasApiUrl = hateoasApiUrl;
	}
}
