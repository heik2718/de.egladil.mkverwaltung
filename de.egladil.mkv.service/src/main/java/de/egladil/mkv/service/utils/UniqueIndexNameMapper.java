//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.UniqueIndexNameMessageProvider;

/**
 * UniqueIndexNameMapper
 */
public class UniqueIndexNameMapper implements UniqueIndexNameMessageProvider {

	private static Map<String, String> uniqueIndexMap;

	static {
		uniqueIndexMap = new HashMap<>();
		uniqueIndexMap.put("uk_laender_1", "Ein Land mit diesem KUERZEL gibt es schon. {0}");
		uniqueIndexMap.put("uk_laender_2", "Ein Land mit diesem Namen gibt es schon. {0}");

		uniqueIndexMap.put("uk_orte_1", "Einen Ort mit diesem KUERZEL gibt es schon. {0}");
		uniqueIndexMap.put("uk_orte_2", "Einen Ort mit diesem Namen gibt es in diesem Land schon.{0}");

		uniqueIndexMap.put("uk_schulen_1", "Eine Schule mit diesem KUERZEL gibt es schon. {0}");
		uniqueIndexMap.put("uk_schulen_2", "Eine Schule mit diesem Namen gibt es in diesem Ort schon. {0}");

		uniqueIndexMap.put("uk_privatteilnahmen_1", "Eine Privatteilnahme mit diesem KUERZEL gibt es schon.");
		uniqueIndexMap.put("uk_privatzettel_1", "Einen Privatzettel mit diesem KUERZEL gibt es schon.");

		uniqueIndexMap.put("uk_lehrer_schulen_1", "Eine Schulzuordnung mit diesem KUERZEL gibt es schon.");
		uniqueIndexMap.put("uk_klassenzettel_1", "Einen Antwortzettel mit diesem KUERZEL gibt es schon.");
		uniqueIndexMap.put("uk_loesungsbuchstaben_1",
			"Eine Wettbewerbslösung zu diesem Jahr und dieser Klassenstufe gibt es schon.");
		uniqueIndexMap.put("uk_privatkontakte_2", "Einen Privatkontakt mit dieser UUID gibt es schon.");
		uniqueIndexMap.put("uk_privatteilnahmen_2", "Sie sind bereits für diesen Wettbewerb angemeldet.");
		uniqueIndexMap.put("uk_lehrerkonten_2", "Ein Lehrerkonto mit dieser UUID gibt es schon.");
		uniqueIndexMap.put("uk_lehrer_schulen_2", "Sie sind dieser Schule bereits zugeornet.");

		uniqueIndexMap.put("uk_lehrerdownloads_1", "Der Benutzer hat im gegebenen Jahr diese Unterlagen schon heruntergeladen.");
		uniqueIndexMap.put("uk_wettbewerbe_1", "Die Lösungsbuchstaben gibt es für das Jahr und die Klassenstufe schon.");
		uniqueIndexMap.put("uk_loesungszettel_2", "Diese Nummer wurde bereits vergeben.");
		uniqueIndexMap.put("uk_privatdownloads_1", "Der Benutzer hat im gegebenen Jahr diese Unterlagen schon heruntergeladen.");
		uniqueIndexMap.put("uk_uploads_1", "Diese Datei wurde bereits hocheladen");
		uniqueIndexMap.put("uk_lehrerteilnahmen_1", "Der Lehrer ist der Schulteinahme bereits zugeordnet.");
		uniqueIndexMap.put("uk_schulteilnahmen_1", "Die Schule ist in diesem Jahr bereits angemeldet");

		uniqueIndexMap.put("uk_teilnehmer_2",
			"Sie haben bereits ein Kind mit gleichem Vornamen, gleichem Nachnamen und gleicher Klassesnstufe erfasst. Falls das richtig ist, verwenden Sie bitte den Zusatz zur Unterscheidung.");

		/**
		 * Das ist kein DB-Index! Er verhindert, dass zur gleichen Schule 2 Personen mit gleichem Vornamen und Nachnamen
		 * zugeordnet sind
		 */
		uniqueIndexMap.put("businessindex_lehrer_schulen",
			"An der gewählten Schule gibt es bereits einen Kollegen/eine Kollegin mit dem gleichen Vor- und Nachnamen.");

		uniqueIndexMap.put("uk_privatzettel_2",
			"Ein Antwortzettel mit dieser Klassenstufe und dieser Nummer wurde von Ihnen bereits erfasst.");
		uniqueIndexMap.put("uk_schulzettel_2",
			"Ein Antwortzettel mit dieser Klassenstufe und dieser Nummer wurde von Ihnen bereits erfasst.");

		uniqueIndexMap.put("uk_aktivierungsdaten_1", "Aktivierungsdaten mit diesem ConfirmationCode gibt es schon.");
		uniqueIndexMap.put("uk_benutzer_1", "Dieses Benutzerkonto gibt es schon.");
		uniqueIndexMap.put("uk_benutzer_2", "Dieses Benutzerkonto gibt es schon.");
		uniqueIndexMap.put("uk_benutzer_3", "Einen Benutzer mit dieser UUID gibt es schon.");

		uniqueIndexMap.put("uk_benutzerrollen", "Der Benutzer hat diese EnsureRole schon.");
		uniqueIndexMap.put("uk_rechte_1", "Dieses Recht gibt es zu dieser EnsureRole schon.");
		uniqueIndexMap.put("uk_rollen_1", "Diese EnsureRole gibt es schon.");

		uniqueIndexMap.put("uk_auswertungsgruppen_2", "Diese Klasse gibt es schon.");
	}

	/**
	 * Utility-Methode, die alle vorhandenen Unique indexes auf verständliche Meldung mapped.
	 *
	 * @param uniqueIndexName
	 * @return
	 */
	private static Optional<String> uniqueIndexNameToMessage(final String uniqueIndexName) {
		final String message = uniqueIndexMap.get(uniqueIndexName.toLowerCase());
		return message == null ? Optional.empty() : Optional.of(message);
	}

	@Override
	public Optional<String> exceptionToMessage(final EgladilDuplicateEntryException e) {
		final Optional<String> message = uniqueIndexNameToMessage(e.getUniqueIndexName());
		return message;
	}
}
