//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.bv.aas.validation.EnsureRole;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Kuerzel;

/**
 * Wettbewerbsanmeldung
 */
public class Wettbewerbsanmeldung implements ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotBlank
	@Digits(integer = 4, fraction = 0)
	private String jahr;

	private boolean benachrichtigen;

	@NotBlank
	@EnsureRole(erlaubteRollen = { "MKV_PRIVAT", "MKV_LEHRER" })
	private String rolle;

	@Kuerzel
	private String schulkuerzel;

	/**
	 * Erzeugt eine Instanz von Wettbewerbsanmeldung
	 */
	public Wettbewerbsanmeldung() {
	}

	/**
	 * Erzeugt eine Instanz von Wettbewerbsanmeldung
	 */
	public Wettbewerbsanmeldung(final String rolle, final String jahr, final boolean benachrichtigen) {
		this.rolle = rolle;
		this.jahr = jahr;
		this.benachrichtigen = benachrichtigen;
	}

	/**
	 * Erzeugt eine Instanz von Wettbewerbsanmeldung
	 */
	public Wettbewerbsanmeldung(final String rolle, final String jahr, final boolean benachrichtigen, final String schulkuerzel) {
		this.rolle = rolle;
		this.jahr = jahr;
		this.benachrichtigen = benachrichtigen;
		this.schulkuerzel = schulkuerzel;
	}

	public String getJahr() {
		return jahr;
	}

	public boolean isBenachrichtigen() {
		return benachrichtigen;
	}

	public String getRolle() {
		return rolle;
	}

	public void setRolle(final String rolle) {
		this.rolle = rolle;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Wettbewerbsanmeldung [jahr=");
		builder.append(jahr);
		builder.append(", rolle=");
		builder.append(rolle);
		builder.append(", benachrichtigen=");
		builder.append(benachrichtigen);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String toBotLog() {
		return toString();
	}

	public String getSchulkuerzel() {
		return schulkuerzel;
	}
}
