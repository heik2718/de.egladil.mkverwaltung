//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.bv.aas.payload.TempPwdAendernPayload;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilBVException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.service.exception.TempPasswordExpired;

/**
 * PasswortDelegate
 */
@Singleton
public class PasswortDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(PasswortDelegate.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final IAuthenticationService authenticationService;

	private final BenutzerkontoDelegate benutzerkontoDelegate;

	private final IProtokollService protokollService;

	/**
	 * Erzeugt eine Instanz von PasswortDelegate
	 */
	@Inject
	public PasswortDelegate(final IAuthenticationService authenticationService, final BenutzerkontoDelegate benutzerkontoDelegate,
		final IProtokollService protokollService) {
		this.authenticationService = authenticationService;
		this.benutzerkontoDelegate = benutzerkontoDelegate;
		this.protokollService = protokollService;
	}

	/**
	 *
	 * Tja, was wohl.
	 *
	 * @param payload
	 * @param benutzerUuid
	 * @return AuthenticationInfo
	 */
	public AuthenticationInfo authentisierenUndPasswortAendern(final PasswortAendernPayload payload, final String benutzerUuid)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException,
		EgladilConcurrentModificationException, EgladilDuplicateEntryException, EgladilWebappException {
		final AuthenticationInfo authenticationInfo = authenticationService
			.authenticate(new UsernamePasswordToken(payload.getEmail(), payload.getPasswort().toCharArray()), Anwendung.MKV);

		final Optional<Benutzerkonto> optBenutzer = benutzerkontoDelegate.changePasswordAndSendMail(benutzerUuid,
			payload.getPasswortNeu().toCharArray());

		if (optBenutzer.isPresent()) {
			protokollService.protokollieren(Ereignisart.PASSWORT_GEAENDERT.getKuerzel(), benutzerUuid, "regulär - Profiländerung");

			return authenticationInfo;
		}
		LOG.error(GlobalConstants.LOG_PREFIX_IMPOSSIBLE
			+ " authenticationService.authorizeAndChangePasswort wirft keine Exception und gibt kein Benutzerkonto zurueck.");
		throw ExceptionFactory.internalServerErrorException();
	}

	/**
	 * Tja, was wohl.
	 *
	 * @param payload
	 * @param dummypassword
	 * @return MKVApiResponse
	 */
	public APIResponsePayload authentisierenUndPasswortAendern(final TempPwdAendernPayload payload, final String dummypassword)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException, EgladilStorageException,
		EgladilBVException, EgladilDuplicateEntryException, EgladilConcurrentModificationException, TempPasswordExpired {
		final String passwortAlt = payload.getPasswort();
		if (dummypassword.equals(passwortAlt)) {
			final String details = "Dummypasswort erhalten: " + payload.toString();
			throw ExceptionFactory.notAuthenticated(LOG, null, details);
		}

		final AuthenticationInfo authenticationInfo = benutzerkontoDelegate.authenticateWithTemporaryPasswort(Anwendung.MKV,
			payload.getEmail(), passwortAlt);

		Benutzerkonto benutzerkonto = (Benutzerkonto) authenticationInfo;
		final Optional<Aktivierungsdaten> optAkt = benutzerkonto.findAktivierungsdatenByConfirmCode(passwortAlt);
		if (!optAkt.isPresent()) {
			throw new IncorrectCredentialsException("Temporaeres Passwort nicht mehr vorhanden");
		}
		if (optAkt.get().isExpired(new Date())) {
			final String details = "Email {}, Benutzer {}: temporaeres Passwort ist nicht mehr gueltig";
			LOG.info(details, payload.getEmail(), benutzerkonto.getUuid().substring(0, 8));
			benutzerkontoDelegate.removeTempPasswordQuietly(benutzerkonto, passwortAlt);
			throw new TempPasswordExpired();
		}
		final Optional<Benutzerkonto> optGespeichert = benutzerkontoDelegate.changePasswordAndSendMail(benutzerkonto.getUuid(),
			payload.getPasswortNeu().toCharArray());
		if (!optGespeichert.isPresent()) {
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE
				+ " benutzerkontoDelegate.changePasswordAndSendMail wirft keine Exception und gibt kein Benutzerkonto zurueck.");
			throw ExceptionFactory.internalServerErrorException();
		}
		benutzerkonto = optGespeichert.get();
		benutzerkontoDelegate.removeTempPasswordQuietly(benutzerkonto, passwortAlt);

		final String rollen = PrettyStringUtils.collectionToDefaultString(benutzerkonto.getRoles());

		protokollService.protokollieren(Ereignisart.PASSWORT_GEAENDERT.getKuerzel(), benutzerkonto.getUuid(),
			"Passwort zurücksetzen- Mechanismus - Rollen: " + rollen);
		final APIResponsePayload result = new APIResponsePayload(
			APIMessage.info(applicationMessages.getString("einmalpassword.change.success")));
		return result;
	}
}
