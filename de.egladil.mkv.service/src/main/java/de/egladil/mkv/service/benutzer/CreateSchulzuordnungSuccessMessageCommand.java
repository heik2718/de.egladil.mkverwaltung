//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * CreateRegistrierungSuccessMessageCommand
 */
public class CreateSchulzuordnungSuccessMessageCommand {

	private final boolean neuanmeldungFreigeschaltet;

	private final String aktuellesWettbewerbsjahr;

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	/**
	 * CreateRegistrierungSuccessMessageCommand
	 */
	public CreateSchulzuordnungSuccessMessageCommand(final boolean neuanmeldungFreigeschaltet,
		final String aktuellesWettbewerbsjahr) {
		this.neuanmeldungFreigeschaltet = neuanmeldungFreigeschaltet;
		this.aktuellesWettbewerbsjahr = aktuellesWettbewerbsjahr;
	}

	/**
	 * Erzeugt vom Kontext abhängige Meldung über den Schulwechel.
	 *
	 * @param anmelden boolean
	 * @param nameSchule String
	 * @return String
	 */
	public String getSchulzuordnungSuccessMessage(final boolean anmelden, final String nameSchule) {
		if (!anmelden) {
			return MessageFormat.format(applicationMessages.getString("schulwechselOhneAnmeldung.success"),
				new Object[] { nameSchule });
		}
		if (neuanmeldungFreigeschaltet) {
			return MessageFormat.format(applicationMessages.getString("schulwechselMitAnmeldung.success"),
				new Object[] { nameSchule, aktuellesWettbewerbsjahr });
		}
		return MessageFormat.format(applicationMessages.getString("schulwechselMitAnmeldungAnmeldungDeaktiviert.success"),
			new Object[] { nameSchule, aktuellesWettbewerbsjahr });
	}

}
