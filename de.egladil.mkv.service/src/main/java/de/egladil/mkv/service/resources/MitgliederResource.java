//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.persistence.PersistenceException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.IAuthConstants;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.payload.PasswortAendernPayload;
import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.validation.json.InvalidProperty;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.SessionToken;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.request.MailadresseAendernPayload;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.request.SchulzuordnungAendernPayload;
import de.egladil.mkv.persistence.payload.request.StatusPayload;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.payload.response.benutzer.ErweiterteKontodaten;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.payload.response.benutzer.PersonBasisdaten;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.benutzer.CreateSchulzuordnungSuccessMessageCommand;
import de.egladil.mkv.service.benutzer.LehrerkontoDelegate;
import de.egladil.mkv.service.benutzer.PasswortDelegate;
import de.egladil.mkv.service.benutzer.PrivatkontoDelegate;
import de.egladil.mkv.service.benutzer.SessionDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * MitgliederResource löst KontoResource von angularjs ab.
 */
@Singleton
@Path("/mitglieder")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_HTML })
public class MitgliederResource {

	public final static Logger LOG = LoggerFactory.getLogger(MitgliederResource.class);

	private final ResourceBundle applicationMessages;

	private final IAnonymisierungsservice anonymisierungsservice;

	private final BenutzerkontoDelegate benutzerkontoDelegate;

	private final LehrerkontoDelegate lehrerkontoDelegate;

	private final PrivatkontoDelegate privatkontoDelegate;

	private final PasswortDelegate passwortDelegate;

	private final BenutzerPayloadMapper benutzerPayloadMapper;

	private final IAccessTokenDAO accessTokenDAO;

	private final ISchulteilnahmeDao schulteilnahmeDao;

	private final SessionDelegate sessionDelegate;

	private ValidationDelegate validationDelegate = new ValidationDelegate();

	private final ResourceExceptionHandler exceptionHandler;

	/**
	 * MitgliederResource
	 */
	@Inject
	public MitgliederResource(final BenutzerkontoDelegate benutzerkontoDelegate, final LehrerkontoDelegate lehrerkontoDelegate,
		final PrivatkontoDelegate privatkontoDelegate, final BenutzerPayloadMapper benutzerPayloadMapper,
		final IAnonymisierungsservice anonymisierungsservice, final IAccessTokenDAO accessTokenDAO,
		final ISchulteilnahmeDao schulteilnahmeDao, final SessionDelegate sessionDelegate,
		final PasswortDelegate passwortDelegate) {
		super();

		this.applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

		this.benutzerkontoDelegate = benutzerkontoDelegate;
		this.lehrerkontoDelegate = lehrerkontoDelegate;
		this.privatkontoDelegate = privatkontoDelegate;
		this.benutzerPayloadMapper = benutzerPayloadMapper;
		this.anonymisierungsservice = anonymisierungsservice;
		this.accessTokenDAO = accessTokenDAO;
		this.schulteilnahmeDao = schulteilnahmeDao;
		this.sessionDelegate = sessionDelegate;
		this.passwortDelegate = passwortDelegate;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	}

	/**
	 * Das durch principal definierte MKV-Konto wird anonymisiert.
	 *
	 * @param principal Principal
	 * @param payload StatusPayload
	 * @return Response
	 */
	@DELETE
	@Timed
	@Path("/mitglied")
	public Response benutzerkontoAnonymisieren(@Auth final Principal principal, final StatusPayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, StatusPayload.class);
			if (!payload.isNeuerStatus()) {
				final InvalidProperty iv = new InvalidProperty("neuerStatus",
					applicationMessages.getString("konto.delete.validationErrorDetails"));
				final ConstraintViolationMessage cvm = new ConstraintViolationMessage(
					applicationMessages.getString("general.validationErrors"));
				cvm.addInvalidProperty(iv);

				// FIXME wissen noch nicht, wie wir das im Frontend verarbeiten wollen.
				final APIMessage mkvMessage = APIMessage.error("Validierungsfehler");
				final APIResponsePayload entity = new APIResponsePayload(mkvMessage, cvm);
				return Response.status(CustomResponseStatus.VALIDATION_ERRORS.getStatusCode()).entity(entity).build();
			}
			anonymisierungsservice.kontoAnonymsieren(benutzerUuid, "Benutzer über MKV");

			final APIMessage mkvMessage = APIMessage.info(applicationMessages.getString("konto.delete.success"));
			return Response.ok().entity(new APIResponsePayload(mkvMessage)).build();
		} catch (final WebApplicationException | DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final UnknownAccountException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(details);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final ResourceNotFoundException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(details);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilConcurrentModificationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "konto.delete.concurrent", payload);
		} catch (final Throwable e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.error(details, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Gibt eine PublicMKVLehrer zurück, die bei Erfolg die neue PublicSchule als Payload enthält.
	 *
	 * @param principal
	 * @param payload
	 * @return Response
	 */
	@PUT
	@Timed
	@Path("/lehrer/schule")
	public Response schulzuordnungAendern(@Auth final Principal principal, final SchulzuordnungAendernPayload payload) {
		String benutzerUuid = null;
		Optional<AccessToken> optAccessToken = null;
		Benutzerkonto benutzer = null;
		Optional<Schulteilnahme> optOrphan = null;
		try {
			if (principal == null) {
				LOG.error("Parameter principal darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			if (payload == null) {
				LOG.error("Parameter payload darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			benutzerUuid = principal.getName();
			optAccessToken = accessTokenDAO.findAccessTokenByPrimaryPrincipal(benutzerUuid);

			if (!optAccessToken.isPresent()) {
				LOG.error("konnte keine Session zu Benutzer {} finden", benutzerUuid);
				final APIMessage mkvMessage = APIMessage.warn(applicationMessages.getString("general.requestTimeout"));
				final APIResponsePayload responsePayload = new APIResponsePayload(mkvMessage);
				return Response.status(Status.REQUEST_TIMEOUT).entity(responsePayload).build();
			}
			validationDelegate.check(payload, SchulzuordnungAendernPayload.class);
			final String aktuellesWettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();
			benutzer = benutzerkontoDelegate.getBenutzerkonto(benutzerUuid);
			if (benutzer == null) {
				throw new NullPointerException("konnte keinen Benutzer mit UUID finden");
			}

			if (benutzer.isGesperrt()) {
				throw new DisabledAccountException("Benutzerkonto inzwischen gesperrt.");
			}
			lehrerkontoDelegate.changeSchule(payload, benutzer, aktuellesWettbewerbsjahr);

			// das funktionierte seltsamerweise im lehrerKontoDelegate nicht :(
			optOrphan = schulteilnahmeDao.findOrphan(payload.getKuerzelAlt(), aktuellesWettbewerbsjahr);
			if (optOrphan.isPresent()) {
				try {
					schulteilnahmeDao.delete(optOrphan.get());
				} catch (final PersistenceException e) {
					final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
						.toEgladilConcurrentModificationException(e);
					if (optConc.isPresent()) {
						LOG.warn("Konkurrierendes Update beim Löschen der verwaisten Schulteilnahme: " + optOrphan.get().toString()
							+ ", benutzerUuid=" + benutzerUuid + payload.toBotLog());
					} else {
						LOG.warn("Unerwartete PersistenceException  beim Löschen der verwaisten Schulteilnahme: "
							+ optOrphan.get().toString() + ", benutzerUuid=" + benutzerUuid + payload.toBotLog() + " - "
							+ e.getMessage());
					}
				}
			}
			final APIResponsePayload entity = createPublicLehrerWithAccessToken(payload, optAccessToken, benutzer);
			return Response.ok().entity(entity).build();
		} catch (final WebApplicationException | DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final UnknownAccountException e) {
			final String details = GlobalConstants.LOG_PREFIX_IMPOSSIBLE
				+ "schulzuordnungAendern: benutzerUuid gehört zu keinem Lehrerkonto. [benutzerUuid=" + benutzerUuid + ", "
				+ payload;
			LOG.warn(details);
			final APIMessage msg = APIMessage.error(applicationMessages.getString("general.internalServerError"));
			final APIResponsePayload entity = new APIResponsePayload(msg);
			return Response.status(CustomResponseStatus.UNAUTHORIZED.getStatusCode()).entity(entity).build();
		} catch (final ResourceNotFoundException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(details);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final PersistenceException e) {
			final String details = "Konkurrierendes update beim Ändern der Schulzuordnung [benutzerUuid=" + benutzerUuid + ", "
				+ payload;
			LOG.warn(details);
			return exceptionHandler.mapToMKVApiResponse(e, "profil.concurrent", payload);
		} catch (final Throwable e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.error(details, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	private APIResponsePayload createPublicLehrerWithAccessToken(final SchulzuordnungAendernPayload payload,
		final Optional<AccessToken> optAccessToken, final Benutzerkonto benutzer) {

		final MKVBenutzer angemeldeterMKVBenutzer = benutzerPayloadMapper.createBenutzer(benutzer, false);
		final String msg = getSchulzuordnungSuccessMessage(payload.isAnmelden(),
			angemeldeterMKVBenutzer.getErweiterteKontodaten().getSchule().getName());
		final AccessToken accessToken = optAccessToken.get();

		final APIMessage mkvMessage = APIMessage.info(msg);
		final APIResponsePayload entity = new APIResponsePayload(mkvMessage, angemeldeterMKVBenutzer);
		entity.setSessionToken(new SessionToken(accessToken.getAccessTokenId(), accessToken.getCsrfToken()));
		return entity;
	}

	private String getSchulzuordnungSuccessMessage(final boolean anmelden, final String nameSchule) {
		final Kontext kontext = KontextReader.getInstance().getKontext();
		return new CreateSchulzuordnungSuccessMessageCommand(kontext.isNeuanmeldungFreigegeben(), kontext.getWettbewerbsjahr())
			.getSchulzuordnungSuccessMessage(anmelden, nameSchule);
	}

	/**
	 *
	 * @param principal Principal fürs authentication framework. Die Annotation bewirkt, dass der Parameter durch
	 * io.dropwizard.auth gesetzt wird. Der Request sollte diesen tunlichst nicht enthalten.
	 *
	 * @param payload PasswortAendernPayload die neuen Daten
	 *
	 * @param bearer String - der Parameter muss hier explizit ausgelesen werden zum Austauschen der Session, da das
	 * AuthenticationToken transparent vom Framework geprüft wird und an dieser Stelle kein Zugang dazu besteht. Die
	 * Session-ID kommt mit vorangestelltem 'Bearer ' an.
	 *
	 * @return Response
	 */
	@PUT
	@Timed
	@Path("/mitglied/email")
	public Response emailAendern(@Auth final Principal principal, final MailadresseAendernPayload payload,
		@HeaderParam(IAuthConstants.BEARER_KEY) final String bearer) {
		UsernamePasswordToken usernamePasswordToken = null;
		try {
			if (principal == null) {
				LOG.error("Parameter principal darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			if (bearer == null) {
				LOG.error("Parameter bearer darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			validationDelegate.check(payload, MailadresseAendernPayload.class);

			usernamePasswordToken = new UsernamePasswordToken(payload.getUsername(), payload.getPassword().toCharArray());

			final Optional<Benutzerkonto> optBen = benutzerkontoDelegate.authenticateAndChangeEmail(payload);
			final MKVBenutzer pu = sessionDelegate.getBenutzer(optBen.get());
			final SessionToken sessionToken = sessionDelegate.substituteSession(optBen.get(), bearer);

			final APIMessage mkvMessage = APIMessage.info(applicationMessages.getString("email.change.success"));
			final APIResponsePayload entity = new APIResponsePayload(mkvMessage, pu);
			entity.setSessionToken(sessionToken);

			return Response.ok().entity(entity).build();
		} catch (final IncorrectCredentialsException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "email.change.incorrectCredentials", payload);
		} catch (final WebApplicationException | DisabledAccountException | EgladilAuthenticationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilDuplicateEntryException e) {
			final Optional<String> opt = new UniqueIndexNameMapper().exceptionToMessage(e);
			if (opt.isPresent()) {
				return exceptionHandler.mapToMKVApiResponse(e, "email.change.duplicate", payload);
			} else {
				LOG.error("Unbekannter UniqueIdexName [{}]", e.getUniqueIndexName());
				final Throwable th = new Throwable("Unbekannter UniqueIdexName [" + e.getUniqueIndexName() + "]", e);
				return exceptionHandler.mapToMKVApiResponse(th, null, payload);
			}
		} catch (final EgladilConcurrentModificationException e) {
			LOG.warn("Konkurrierendes Update beim Ändern der Mailadresse.");
			return exceptionHandler.mapToMKVApiResponse(e, "email.change.concurrent", payload);
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception beim Ändern der Mailadresse: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} finally {
			if (usernamePasswordToken != null) {
				usernamePasswordToken.clear();
			}
		}
	}

	/**
	 *
	 * @param principal Principal fürs authentication framework. Die Annotation bewirkt, dass der Parameter durch
	 * io.dropwizard.auth gesetzt wird. Der Request sollte diesen tunlichst nicht enthalten.
	 *
	 * @param payload PasswortAendernPayload die neuen Daten
	 *
	 * @param bearer String - der Parameter muss hier explizit ausgelesen werden zum Autauschen der Session, da das
	 * AuthenticationToken transparent vom Framework geprüft wird und an dieser Stelle kein Zugang dazu besteht. Die
	 * Session-ID kommt mit vorangestelltem 'Bearer ' an.
	 *
	 * @return Response
	 */
	@PUT
	@Timed
	@Path("/mitglied/passwort")
	public Response passwortAendern(@Auth final Principal principal, final PasswortAendernPayload payload,
		@HeaderParam(IAuthConstants.BEARER_KEY) final String bearer) {
		String benutzerUuid = null;
		try {
			if (principal == null) {
				LOG.error("Parameter principal darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			if (bearer == null) {
				LOG.error("Parameter bearer darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			if (payload == null) {
				LOG.error("Parameter payload darf nicht null sein");
				throw ExceptionFactory.badRequest();
			}
			validationDelegate.check(payload, PasswortAendernPayload.class);
			benutzerUuid = principal.getName();
			final AuthenticationInfo authenticationInfo = passwortDelegate.authentisierenUndPasswortAendern(payload, benutzerUuid);
			final MKVBenutzer pu = sessionDelegate.getBenutzer((Benutzerkonto) authenticationInfo);

			final APIMessage mkvMessage = APIMessage.info(applicationMessages.getString("password.change.success"));
			final APIResponsePayload successPayload = new APIResponsePayload(mkvMessage, pu);
			successPayload.setSessionToken(sessionDelegate.substituteSession(authenticationInfo, bearer));

			return Response.ok().entity(successPayload).build();
		} catch (final WebApplicationException | DisabledAccountException | ResourceNotFoundException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + (payload != null ? payload.toString() : "null");
			LOG.warn(e.getMessage() + ": " + details);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final ExcessiveAttemptsException e) {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "beim Ändern des Passworts zu Benutzer " + principal.getName() + " "
				+ e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, "password.change.incorrectCredentials", payload);
		} catch (final EgladilAuthenticationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "password.change.incorrectCredentials", payload);
		} catch (final EgladilAuthorizationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "person.change.notAuthenticated", payload);
		} catch (final EgladilDuplicateEntryException e) {
			LOG.error(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "EgladilDuplicateEntryException Exception beim Ändern des Passworts: "
				+ e.getMessage(), e);
			final MKVException ex = new MKVException("serverfehler");
			return exceptionHandler.mapToMKVApiResponse(ex, null, payload);
		} catch (final EgladilConcurrentModificationException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(
				GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Konkurrierendes Update beim Ändern des Newsletterstatus - " + details);
			return exceptionHandler.mapToMKVApiResponse(e, "password.change.concurrent", payload);
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception beim Ändern der Person: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	@PUT
	@Timed
	@Path("/mitglied/name")
	public Response nameAendern(@Auth final Principal principal, final PersonAendernPayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, PersonAendernPayload.class);

			IMKVKonto kontakt = null;
			final Role role = Role.valueOf(payload.getRolle());
			switch (role) {
			case MKV_LEHRER:
				kontakt = lehrerkontoDelegate.verifyAndChangePerson(payload, benutzerUuid);
				break;
			case MKV_PRIVAT:
				kontakt = privatkontoDelegate.verifyAndChangePerson(payload, benutzerUuid);
				break;
			default:
				return ExceptionFactory
					.internalServerErrorException(LOG, null,
						GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "EnsureRole [" + role.toString() + "] ist hier nicht moeglich")
					.getResponse();
			}
			LOG.debug("Person geaendert: {}", kontakt);
			final MKVBenutzer pu = new MKVBenutzer();
			final PersonBasisdaten basisdaten = new PersonBasisdaten();
			basisdaten.setVorname(kontakt.getPerson().getVorname());
			basisdaten.setNachname(kontakt.getPerson().getNachname());
			pu.setBasisdaten(basisdaten);
			final APIMessage mkvMessage = APIMessage.info(applicationMessages.getString("person.change.success"));
			final APIResponsePayload entity = new APIResponsePayload(mkvMessage, pu);
			return Response.ok().entity(entity).build();
		} catch (final WebApplicationException | DisabledAccountException | ResourceNotFoundException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(e.getMessage() + ": " + details);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthorizationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "person.change.notAuthenticated", payload);
		} catch (final EgladilDuplicateEntryException e) {
			final Optional<String> opt = new UniqueIndexNameMapper().exceptionToMessage(e);
			if (opt.isPresent()) {
				return exceptionHandler.mapToMKVApiResponse(e, "person.change.duplicate", payload);
			} else {
				LOG.error("Unbekannter UniqueIdexName [{}]", e.getUniqueIndexName());
				final Throwable th = new Throwable("Unbekannter UniqueIdexName [" + e.getUniqueIndexName() + "]", e);
				return exceptionHandler.mapToMKVApiResponse(th, null, payload);
			}
		} catch (final EgladilConcurrentModificationException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Konkurrierendes Update beim Ändern der Person - " + details);
			return exceptionHandler.mapToMKVApiResponse(e, "person.change.concurrent", payload);
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception beim Ändern der Person: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	/**
	 * @param principal Principal die Autorisierung.
	 * @param payload StatusPayload darf nicht null sein.
	 * @return Response
	 */
	@PUT
	@Timed
	@Path("/mitglied/newsletter")
	public Response newsletterAendern(@Auth final Principal principal, final StatusPayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, StatusPayload.class);
			final Role role = Role.valueOf(payload.getRolle());
			IMKVKonto kontakt = null;
			switch (role) {
			case MKV_LEHRER:
				kontakt = lehrerkontoDelegate.changeStatusNewsletter(payload, benutzerUuid);
				break;
			case MKV_PRIVAT:
				kontakt = privatkontoDelegate.changeStatusNewsletter(payload, benutzerUuid);
				break;
			default:
				LOG.error(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "EnsureRole [" + role.toString() + "] ist hier nicht moeglich");
				throw new EgladilConfigurationException("Konfigurationsfehler im Client");
			}
			LOG.debug("Status mailbenachrichtigung geaendert: {}", kontakt);
			final String msg = payload.isNeuerStatus() ? applicationMessages.getString("statusNewsletter.change.aktiviert")
				: applicationMessages.getString("statusNewsletter.change.deaktiviert");
			final MKVBenutzer pu = new MKVBenutzer();
			final ErweiterteKontodaten erweiterteDaten = new ErweiterteKontodaten();
			erweiterteDaten.setMailbenachrichtigung(payload.isNeuerStatus());
			pu.setErweiterteKontodaten(erweiterteDaten);
			;
			final APIMessage mkvMessage = APIMessage.info(msg);
			final APIResponsePayload entity = new APIResponsePayload(mkvMessage, pu);
			return Response.ok().entity(entity).build();
		} catch (final WebApplicationException | DisabledAccountException | ResourceNotFoundException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(e.getMessage() + ": " + details);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilAuthorizationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "person.change.notAuthenticated", payload);
		} catch (final EgladilDuplicateEntryException e) {
			final Optional<String> opt = new UniqueIndexNameMapper().exceptionToMessage(e);
			if (opt.isPresent()) {
				return exceptionHandler.mapToMKVApiResponse(e, "person.change.duplicate", payload);
			} else {
				LOG.error("Unbekannter UniqueIdexName [{}]", e.getUniqueIndexName());
				final Throwable th = new Throwable("Unbekannter UniqueIdexName [" + e.getUniqueIndexName() + "]", e);
				return exceptionHandler.mapToMKVApiResponse(th, null, payload);
			}
		} catch (final EgladilConcurrentModificationException e) {
			final String details = "[benutzerUuid=" + benutzerUuid + ", " + payload;
			LOG.warn(
				GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Konkurrierendes Update beim Ändern des Newsletterstatus - " + details);
			return exceptionHandler.mapToMKVApiResponse(e, "statusNewsletter.change.concurrent", payload);
		} catch (final Throwable e) {
			LOG.error("Unerwartete Exception beim Ändern der Person: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	final void setValidationDelegate(final ValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}
}
