//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.service.confirmation.IPrivatConfirmationService;
import de.egladil.mkv.service.confirmation.impl.ConfirmationStatus;
import de.egladil.mkv.service.payload.request.AbstractMKVRegistrierung;
import de.egladil.mkv.service.payload.request.Privatregistrierung;
import de.egladil.mkv.service.registrierung.IPrivatRegistrierungService;

/**
 * PrivatregistrierungResource
 */
@Singleton
@Path("/registrierungen")
public class PrivatregistrierungResource extends AbstractRegistrierungResource {

	private final IPrivatRegistrierungService privatRegistrierungService;

	private final IPrivatConfirmationService confirmationService;

	/**
	 * Erzeugt eine Instanz von PrivatregistrierungResource
	 */
	@Inject
	public PrivatregistrierungResource(final IPrivatRegistrierungService privatRegistrierungService,
		final IPrivatConfirmationService confirmationService) {
		this.privatRegistrierungService = privatRegistrierungService;
		this.confirmationService = confirmationService;
	}

	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/privat")
	public Response register(final Privatregistrierung payload) {
		return super.doRegister(payload, Privatregistrierung.class);
	}

	/**
	 * @see de.egladil.mkv.service.resources.AbstractRegistrierungResource#kontaktAnlegenUndZumWettbewerbAnmelden(de.egladil.mkv.service.payload.request.AbstractMKVRegistrierung)
	 */
	@Override
	protected <T extends AbstractMKVRegistrierung> Aktivierungsdaten kontaktAnlegenUndZumWettbewerbAnmelden(final T payload)
		throws EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException,
		EgladilMailException {
		final Aktivierungsdaten aktivierungsdaten = privatRegistrierungService.kontaktAnlegenUndZumWettbewerbAnmelden(
			(Privatregistrierung) payload, getAktuellesWettebwerbsjahr(), super.isNeuanmeldungFreigegeben());
		return aktivierungsdaten;
	}

	@Override
	@GET
	@Timed
	@Path("/privat/confirmation")
	@Produces("text/html")
	public InputStream confirm(@QueryParam("t") final String confirmationCode) {
		return super.confirm(confirmationCode);
	}

	/**
	 * @see de.egladil.mkv.service.resources.AbstractRegistrierungResource#registrierungBestaetigen(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	protected ConfirmationStatus registrierungBestaetigen(final String confirmationCode) {
		final ConfirmationStatus confirmationStatus = confirmationService.jetztRegistrierungBestaetigen(confirmationCode,
			getAktuellesWettebwerbsjahr());
		return confirmationStatus;
	}
}
