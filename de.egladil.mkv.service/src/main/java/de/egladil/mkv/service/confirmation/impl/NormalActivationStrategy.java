//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;

/**
 * NormalActivationStrategy
 */
public class NormalActivationStrategy implements IConfirmationStrategy {

	private static final Logger LOG = LoggerFactory.getLogger(NormalActivationStrategy.class);

	private final IRegistrierungService registrierungService;

	/**
	 * Erzeugt eine Instanz von NormalActivationStrategy
	 */
	public NormalActivationStrategy(final IRegistrierungService registrierungService) {
		this.registrierungService = registrierungService;
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IConfirmationStrategy#applyOnRegistration(de.egladil.bv.aas.domain.Aktivierungsdaten,
	 * String, Optional, de.egladil.mkv.service.confirmation.impl.IKontaktUndTeilnahmeCallback)
	 */
	@Override
	public ConfirmationStatus applyOnRegistration(final Aktivierungsdaten aktivierungsdaten, final String jahr, final Optional<IMKVKonto> kontakt,
		final IKontaktUndTeilnahmeCallback callback, final IAnonymisierungsservice anonymisierungsservice)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException {
		if (kontakt.isPresent()) {
			registrierungService.activateBenutzer(aktivierungsdaten);
			callback.persist(kontakt.get());
			return ConfirmationStatus.normalActivation;
		} else {
			final String msg = "Fehler im Ablauf bei der Aktivierung eines Benutzerkontos: Lehrerkonto/Privatkontoo nicht vorhanden";
			LOG.warn("{} - {}", msg, aktivierungsdaten);
			throw new MKVException(msg);
		}
	}
}
