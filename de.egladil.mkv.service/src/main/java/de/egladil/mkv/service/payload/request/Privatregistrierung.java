//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.egladil.bv.aas.domain.Role;

/**
 * Daten für eine private Registrierung bei der Minikänguruverwaltung.<br>
 * <br>
 * JSON:
 *
 */
public class Privatregistrierung extends AbstractMKVRegistrierung {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von PrivateRegistrierungsanfrage
	 */
	public Privatregistrierung() {
	}

	/**
	 * Erzeugt eine Instanz von PrivateRegistrierungsanfrage
	 */
	public Privatregistrierung(String vorname, String nachname, String email, String passwort, String passwortWdh, String kleber) {
		super(vorname, nachname, email, passwort, passwortWdh, kleber, true);
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.IMKVRegistrierung#getRole()
	 */
	@Override
	@JsonIgnore
	public Role getRole() {
		return Role.MKV_PRIVAT;
	}
}
