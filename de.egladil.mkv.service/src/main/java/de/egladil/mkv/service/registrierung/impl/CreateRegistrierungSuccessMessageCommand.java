//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung.impl;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * CreateRegistrierungSuccessMessageCommand erzeugt auf leicht testbare weise die kontextabhängige Meldung für die
 * APiMessage.
 */
public class CreateRegistrierungSuccessMessageCommand {

	private final boolean neuanmeldungFreigeschaltet;

	private final String aktuellesWettbewerbsjahr;

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	/**
	 * CreateRegistrierungSuccessMessageCommand
	 */
	public CreateRegistrierungSuccessMessageCommand(final boolean neuanmeldungFreigeschaltet,
		final String aktuellesWettbewerbsjahr) {
		this.neuanmeldungFreigeschaltet = neuanmeldungFreigeschaltet;
		this.aktuellesWettbewerbsjahr = aktuellesWettbewerbsjahr;
	}

	/**
	 * Erzeugt vom Kontext abhängige Meldung über die Registrierung.
	 *
	 * @param anmelden boolean
	 * @param nameSchule String
	 * @return String
	 */
	public String getRegistrierungSuccessMessage(final boolean gleichAnmelden) {
		String result = "";
		if (!neuanmeldungFreigeschaltet) {
			result = MessageFormat.format(applicationMessages.getString("Registration.success.anmeldung.nicht.freigeschaltet"),
				new Object[] { aktuellesWettbewerbsjahr });
		} else if (gleichAnmelden) {
			result = MessageFormat.format(applicationMessages.getString("Registration.success.mit.anmeldung"),
				new Object[] { aktuellesWettbewerbsjahr });
		} else {
			result = MessageFormat.format(applicationMessages.getString("Registration.success.ohne.anmeldung"),
				new Object[] { aktuellesWettbewerbsjahr });
		}
		return result;
	}

	/**
	 * Gibt kontextabhängig den Namen des zu verwendenden Mailtemplates zurück.
	 *
	 * @param gleichAnmelden
	 * @return
	 */
	public String getMailtemplatePath(final boolean gleichAnmelden) {
		String result = "";
		if (!neuanmeldungFreigeschaltet) {
			result = "/mailtemplates/registrierung_anmeldung_nicht_freigeschaltet.txt";
		} else if (gleichAnmelden) {
			result = "/mailtemplates/registrierung.txt";
		} else {
			result = "/mailtemplates/registrierung_ohne_anmeldung.txt";
		}
		return result;
	}
}
