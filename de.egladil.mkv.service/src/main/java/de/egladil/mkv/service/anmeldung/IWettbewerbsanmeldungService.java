//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung;

import de.egladil.bv.aas.domain.Role;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * IWettbewerbsanmeldungService
 */
public interface IWettbewerbsanmeldungService {

	/**
	 * Zu einem gegebenen Lehrer- bzw. Privatkonto werden Wettbewerbsteilnahmen für das gegebene Jahr erzeugt und
	 * gespeichert.
	 *
	 * @param anmeldung Wettbewerbsanmeldung darf nicht null sein.
	 * @param benutzerUUID String darf nicht null sein
	 * @param role Role muss MKV_LEHRER oder MKV_PRIVAT sein
	 * @return PublicMKVUser die kompletten Kontoinformationen.
	 */
	MKVBenutzer benutzerZumWettbewerbAnmelden(Wettbewerbsanmeldung anmeldung, String benutzerUUID, Role role)
		throws IllegalArgumentException, EgladilDuplicateEntryException, EgladilConcurrentModificationException,
		EgladilStorageException, MKVException;
}
