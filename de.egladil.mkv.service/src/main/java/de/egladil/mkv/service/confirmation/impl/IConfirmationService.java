//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import javax.validation.ConstraintViolationException;

import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * ISchuleConfirmationService
 */
public interface IConfirmationService {

	/**
	 * Aktion auf den per Mail gesendeten Link zur Bestätigung der Schulregistrierun.
	 *
	 * @param confirmationCode
	 * @param jahr String das aktuelle Wettbewerbsjahr
	 */
	ConfirmationStatus jetztRegistrierungBestaetigen(String confirmationCode, String jahr)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException;
}
