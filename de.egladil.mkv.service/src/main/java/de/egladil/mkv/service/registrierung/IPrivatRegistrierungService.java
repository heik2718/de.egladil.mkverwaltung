//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung;

import de.egladil.mkv.service.payload.request.Privatregistrierung;

/**
 * IPrivatRegistrierungService Markerinterface
 */
public interface IPrivatRegistrierungService extends IRegistrierungService<Privatregistrierung> {

}
