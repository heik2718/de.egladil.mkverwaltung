//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.auswertungen.files.UploadDelegate;
import de.egladil.mkv.auswertungen.files.UploadLog;
import de.egladil.mkv.auswertungen.files.impl.UploadUtils;
import de.egladil.mkv.auswertungen.persistence.IUploadListener;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.request.DateinamePayload;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.service.files.DownloadDelegate;
import de.egladil.mkv.service.files.FilesystemStreamingOutput;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.response.DateiInfo;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * FileResource upload und download von Dateien.
 */
@Singleton
@Path("/dateien")
public class FileResource {

	private static final Logger LOG = LoggerFactory.getLogger(FileResource.class);

	private String downloadPath;

	private String uploadPath;

	private String sandboxPath;

	private int maxFileSize;

	private String maxFileSizeText;

	private final UploadUtils uploadUtils = new UploadUtils();

	private final UploadDelegate uploadDelegate;

	private final DownloadDelegate downloadDelegate;

	private final MailDelegate mailDelegate;

	private final IBenutzerService benutzerService;

	private final IUploadListener uploadListener;

	private boolean rohdatenGenerieren = false;

	private final BenutzerPayloadMapper benutzerPayloadMapper;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * Erzeugt eine Instanz von FiledownloadResource
	 */
	@Inject
	public FileResource(final UploadDelegate uploadDelegate, final DownloadDelegate downloadDelegate,
		final MailDelegate mailDelegate, final IBenutzerService benutzerService, final IUploadListener uploadListener,
		final BenutzerPayloadMapper benutzerPayloadMapper) {
		this.uploadDelegate = uploadDelegate;
		this.downloadDelegate = downloadDelegate;
		this.mailDelegate = mailDelegate;
		this.benutzerService = benutzerService;
		this.uploadListener = uploadListener;
		this.benutzerPayloadMapper = benutzerPayloadMapper;
	}

	/**
	 * Erzeugt eine Liste von Dateinamen mit Beschreibungen als Payload
	 *
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response getFileList(@Auth final Principal principal) {
		try {
			final List<DateiInfo> dateiInfos = downloadDelegate.getDateiInfosWettbewerbsdownloads(downloadPath);
			final APIResponsePayload payload = new APIResponsePayload(APIMessage.info(""),
				filternByRole(dateiInfos, principal.getName()));
			return Response.ok(payload).build();
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	private List<DateiInfo> filternByRole(final List<DateiInfo> dateiInfos, final String benutzerUuid) {
		final List<DateiInfo> results = new ArrayList<>();
		final Benutzerkonto benutzerkonto = this.benutzerService.findBenutzerkontoByUUID(benutzerUuid);
		for (final DateiInfo info : dateiInfos) {
			if (info.allowedForRoles(benutzerkonto.getRoles())) {
				results.add(info);
			}
		}
		return results;
	}

	@OPTIONS
	@Path("/{dateiname}")
	@Timed
	public Response optionsDownload() {
		return Response.ok().build();
	}

	@GET
	@Path("/{dateiname}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Timed
	public Response downloadFile(@Auth final Principal principal, @PathParam("dateiname") final String dateiname) {

		final DateinamePayload payload = new DateinamePayload(dateiname);
		final String benutzerUuid = principal.getName();
		try {

			validationDelegate.check(payload, DateinamePayload.class);
			final String wettbewerbsjahr = this.readWettbewerbsjahr();
			final IMKVKonto konto = downloadDelegate.checkPermission(benutzerUuid, wettbewerbsjahr, downloadPath);
			final FilesystemStreamingOutput result = new FilesystemStreamingOutput(downloadPath, dateiname);
			downloadDelegate.downloadStillSpeichern(konto, dateiname, wettbewerbsjahr);
			final ContentDisposition contentDisposition = ContentDisposition.type("attachment").fileName(dateiname).build();
			final Response response = Response.ok(result).header("Content-Type", "application/octet-stream")
				.header("Content-Disposition", contentDisposition).build();
			return response;

		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final PreconditionFailedException e) {
			// ist schon als Ereignis protokolliert
			return exceptionHandler.mapToMKVApiResponse(e, e.getMessage(), payload);
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 *
	 * @param inputStream
	 * @param fileDetails
	 * @return Status 200, 208, 401, 403, 412, 500, 902
	 */
	@POST
	@Path("/datei")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" })
	public Response uploadFile(@Auth final Principal principal, @FormDataParam("file") final InputStream inputStream,
		@FormDataParam("file") final FormDataContentDisposition fileDetails) {

		final String benutzerUuid = principal.getName();
		try {

			if (this.isUploadDisabled()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_UPLOAD
					+ "Upload versucht, obwohl disabled. Konfiguration auf Client und Server prüfen.");
				throw new PreconditionFailedException("upload.disabled");
			}

			final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(benutzerUuid);
			final Optional<APIResponsePayload> optPublicMessage = uploadDelegate.validatePayloadSize(benutzerUuid,
				fileDetails.getSize(), maxFileSize, maxFileSizeText);
			if (optPublicMessage.isPresent()) {
				return Response.status(CustomResponseStatus.VALIDATION_ERRORS.getStatusCode()).entity(optPublicMessage.get())
					.build();
			}
			final String wettbewerbsjahr = this.readWettbewerbsjahr();
			final byte[] bytes = uploadUtils.readBytes(inputStream);

			final UploadLog uploadLog = uploadDelegate.uploadBytes(benutzerUuid, bytes, uploadPath, sandboxPath, wettbewerbsjahr);
			LOG.info(uploadLog.getMessage());

			final Optional<Upload> optUpload = uploadLog.getUpload();

			if (optUpload.isPresent()) {
				mailDelegate.uploadMailVersenden(benutzerkonto.getEmail());
				if (rohdatenGenerieren) {
					uploadListener.processUpload(optUpload.get().getId(), uploadPath);
				}
			}
			final APIResponsePayload pm = uploadLog.getApiResponsePayload();
			// kein neuer Upload: dann 208, sonst 200
			final MKVBenutzer pu = benutzerPayloadMapper.createBenutzer(benutzerkonto, false);
			final APIResponsePayload payload = new APIResponsePayload(APIMessage.info(pm.getApiMessage().getMessage()), pu);
			return optUpload.isPresent() ? Response.ok().entity(payload).build() : Response.status(208).entity(payload).build();

//			return Response.ok().build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final PreconditionFailedException e) {
			// ist schon als Ereignis protokolliert
			return exceptionHandler.mapToMKVApiResponse(e, e.getMessage(), null);
		} catch (final MKVException e) {
			// wurde bereits geloggt
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_UPLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}

	private String readWettbewerbsjahr() {
		return KontextReader.getInstance().getKontext().getWettbewerbsjahr();
	}

	private boolean isUploadDisabled() {
		return KontextReader.getInstance().getKontext().isUploadDisabled();
		// return true;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param downloadPath neuer Wert der Membervariablen downloadPath
	 */
	public void setDownloadPath(final String downloadPath) {
		this.downloadPath = downloadPath;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param uploadPath neuer Wert der Membervariablen uploadPath
	 */
	public void setUploadPath(final String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public void setMaxFileSize(final int maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	public void setMaxFileSizeText(final String maxFileSizeText) {
		this.maxFileSizeText = maxFileSizeText;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param sandboxPath neuer Wert der Membervariablen sandboxPath
	 */
	public void setSandboxPath(final String sandboxPath) {
		this.sandboxPath = sandboxPath;
	}

	public void setRohdatenGenerieren(final boolean rohdatenGenerieren) {
		this.rohdatenGenerieren = rohdatenGenerieren;
	}
}
