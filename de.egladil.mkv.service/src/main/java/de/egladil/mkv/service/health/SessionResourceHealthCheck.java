package de.egladil.mkv.service.health;

import javax.ws.rs.core.Response;

import com.codahale.metrics.health.HealthCheck;

import de.egladil.common.validation.validators.UuidStringValidator;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.service.resources.SessionResource;

public class SessionResourceHealthCheck extends HealthCheck {

	private final SessionResource pingResource;

	private final UuidStringValidator uuidValidator;

	/**
	 * Erzeugt eine Instanz von PingHealthCheck
	 */
	public SessionResourceHealthCheck(final SessionResource pingResource) {
		this.pingResource = pingResource;
		uuidValidator = new UuidStringValidator();
	}

	@Override
	protected Result check() throws Exception {
		final Response response = pingResource.getKontext();
		if (response.getStatus() == 200) {
			final APIResponsePayload messageEntity = (APIResponsePayload) response.getEntity();
			if (messageEntity.getPayload() != null) {
				final Kontext kontext = (Kontext) messageEntity.getPayload();
				if (uuidValidator.isValid(kontext.getXsrfToken(), null)) {
					return Result.healthy();
				}
			}
			return Result.unhealthy("kontext enthält kein reguläres csrf-Token");
		}
		return Result.unhealthy("erwarten Statuscode 200, war aber " + response.getStatus());
	}
}
