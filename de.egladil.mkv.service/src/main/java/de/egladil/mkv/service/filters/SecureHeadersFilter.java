//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * SecureHeadersFilter
 */
public class SecureHeadersFilter implements Filter {

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
		throws IOException, ServletException {

		this.handle((HttpServletResponse) response);
		chain.doFilter(request, response);

	}

	private void handle(final HttpServletResponse response) {
		response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
		response.addHeader("X-Content-Type-Options", "nosniff");
		response.addHeader("X-Frame-Options", "DENY");
	}

	@Override
	public void destroy() {
	}

}
