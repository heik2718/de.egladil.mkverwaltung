//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.health;

import javax.ws.rs.core.Response;

import com.codahale.metrics.health.HealthCheck;

import de.egladil.mkv.service.resources.ErrorResource;

/**
 * ErrorResourceHealthCheck
 */
public class ErrorResourceHealthCheck extends HealthCheck {

	private final ErrorResource errorResource;

	/**
	 * Erzeugt eine Instanz von ErrorResourceHealthCheck
	 */
	public ErrorResourceHealthCheck(ErrorResource errorResource) {
		this.errorResource = errorResource;
	}

	/**
	 * @see com.codahale.metrics.health.HealthCheck#check()
	 */
	@Override
	protected Result check() throws Exception {
		StringBuilder sb = new StringBuilder("Errorpage(s) not available:\n");
		boolean unhealthy = false;
		{
			Response response = errorResource.get401();
			if (response.getStatus() != 401) {
				sb.append("errorpage 401 nicht korrekt\n");
				unhealthy = true;
			}
		}
		{
			Response response = errorResource.get404();
			if (response.getStatus() != 404) {
				sb.append("errorpage 404 nicht korrekt\n");
				unhealthy = true;
			}
		}
		{
			Response response = errorResource.get500();
			if (response.getStatus() != 500) {
				sb.append("errorpage 500 fehlt\n");
				unhealthy = true;
			}
		}
		{
			Response response = errorResource.get900();
			if (response.getStatus() != 900) {
				sb.append("errorpage 900 fehlt\n");
				unhealthy = true;
			}
		}

		return unhealthy ? Result.unhealthy(sb.toString()) : Result.healthy();
	}
}
