//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Plz;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * SchuleKatalogeintrag
 */
public class SchuleAnschriftKatalogantrag extends AbstractSchuleKatalogantrag {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Plz
	@NotBlank
	@Size(min = 1, max = 10)
	private String plz;

	@StringLatin
	@NotBlank
	@Size(min = 1, max = 100)
	private String ort;

	@StringLatin
	@Size(min = 0, max = 89)
	private String strasse;

	@DeutscherName
	@Size(min = 0, max = 10)
	private String hausnummer;

	@Override
	public String toBotLog() {
		String result = "SchuleAnschriftKatalogantrag " + super.toBotLog();
		result += ", plz=" + plz + ", ort=" + ort + ", strasse=" + strasse + ", hausnummer=" + hausnummer + "]";
		return result;
	}

	/**
	 * @return the plz
	 */
	public String getPlz() {
		return plz;
	}

	/**
	 * @param plz the plz to set
	 */
	public void setPlz(final String plz) {
		this.plz = plz;
	}

	/**
	 * @return the ort
	 */
	public String getOrt() {
		return ort;
	}

	/**
	 * @param ort the ort to set
	 */
	public void setOrt(final String ort) {
		this.ort = ort;
	}

	/**
	 * @return the strasse
	 */
	public String getStrasse() {
		return strasse;
	}

	/**
	 * @param strasse the strasse to set
	 */
	public void setStrasse(final String strasse) {
		this.strasse = strasse;
	}

	/**
	 * @return the hausnummer
	 */
	public String getHausnummer() {
		return hausnummer;
	}

	/**
	 * @param hausnummer the hausnummer to set
	 */
	public void setHausnummer(final String hausnummer) {
		this.hausnummer = hausnummer;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.AbstractSchuleKatalogantrag#ownClass()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends ILoggable> Class<T> ownClass() {
		return (Class<T>) SchuleAnschriftKatalogantrag.class;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("SchuleAnschriftKatalogantrag [" + super.toString());
		builder.append(", plz=");
		builder.append(plz);
		builder.append(", ort=");
		builder.append(ort);
		builder.append(", strasse=");
		builder.append(strasse);
		builder.append(", hausnummer=");
		builder.append(hausnummer);
		builder.append("]");
		return builder.toString();
	}
}
