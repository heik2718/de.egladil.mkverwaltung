//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

import java.util.Comparator;

import de.egladil.mkv.service.payload.response.DateiInfo;

/**
 * DateiInfoBeschreibungSorter
 */
public class DateiInfoBeschreibungSorter implements Comparator<DateiInfo> {

	@Override
	public int compare(final DateiInfo arg0, final DateiInfo arg1) {
		return arg0.getBeschreibung().toLowerCase().compareTo(arg1.getBeschreibung().toLowerCase());
	}

}
