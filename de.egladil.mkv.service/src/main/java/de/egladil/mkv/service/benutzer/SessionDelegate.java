//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import java.util.Date;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.AccessTokenUtils;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.SessionToken;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.service.IdentifierMKVKontoProvider;

/**
 * SessionDelegate behandelt alles rund ums Login und Logout.
 */
@Singleton
public class SessionDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(SessionDelegate.class);

	private final IAccessTokenDAO accessTokenDAO;

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final BenutzerPayloadMapper benutzerPayloadMapper;

	private final IProtokollService protokollService;

	/**
	 * Erzeugt eine Instanz von KontoDelegate
	 */
	@Inject
	public SessionDelegate(final IAccessTokenDAO accessTokenDAO, final ILehrerkontoDao lehrerkontoDao,
		final IPrivatkontoDao privatkontoDao, final BenutzerPayloadMapper benutzerPayloadMapper,
		final IProtokollService protokollService) {
		this.accessTokenDAO = accessTokenDAO;
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.benutzerPayloadMapper = benutzerPayloadMapper;
		this.protokollService = protokollService;
	}

	public SessionToken getSession(final AuthenticationInfo authenticationInfo, final String tempCsrfToken) {

		final AccessToken accessToken = accessTokenDAO.generateNewAccessToken(authenticationInfo);
		invalidateTemporaryCsrfTokenQuietly(tempCsrfToken);
		return new SessionToken(accessToken.getAccessTokenId(), accessToken.getCsrfToken());
	}

	public MKVBenutzer getBenutzer(final Benutzerkonto benutzerkonto) {

		final String benutzerUuid = benutzerkonto.getUuid();

		final Optional<IMKVKonto> optKontakt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(benutzerUuid,
			lehrerkontoDao, privatkontoDao);
		if (!optKontakt.isPresent()) {
			throw new UnknownAccountException("kein Lehrerkonto oder Privatkonto mit UUID [" + benutzerUuid + "] gefunden");
		}
		final IMKVKonto kontakt = optKontakt.get();

		final IMKVKonto persistedKontakt = storeLoginTimeQuietly(kontakt);
		final MKVBenutzer angemeldeterMKVBenutzer = benutzerPayloadMapper.createBenutzer(benutzerkonto, false);

		protokollService.protokollieren(Ereignisart.LOGIN.getKuerzel(), benutzerUuid, persistedKontakt.getRole().toString());
		return angemeldeterMKVBenutzer;
	}

	/**
	 * Prüft, ob der DownloadLink zu einer aktuellen Session gehört.
	 *
	 * @param benutzerUuid
	 * @return Optional
	 */
	public Optional<AccessToken> findAccessToken(final String benutzerUuid) {
		final Optional<AccessToken> optAccessToken = accessTokenDAO.findAccessTokenByPrimaryPrincipal(benutzerUuid);
		if (optAccessToken.isPresent()) {
			// Damit die Session kein Timeout bekommt.
			accessTokenDAO.refresh(optAccessToken.get());
		}
		return optAccessToken;
	}

	/**
	 * Die SessionID des gegebenen Benutzers wird ausgetauscht.
	 *
	 * @param authenticationInfo
	 * @param bearer die alte Session-ID (Authentication-Token)
	 * @return PublicMKVUser
	 * @throws UnknownAccountException
	 */
	public SessionToken substituteSession(final AuthenticationInfo authenticationInfo, final String bearer)
		throws IllegalArgumentException, NullPointerException, UnknownAccountException {
		if (authenticationInfo == null) {
			throw new IllegalArgumentException("authenticationInfo darf nicht null sein");
		}
		if (bearer == null) {
			throw new IllegalArgumentException("accessToken darf nicht null sein");
		}
		final Benutzerkonto benutzer = (Benutzerkonto) authenticationInfo;
		final String uuid = benutzer.getUuid();
		final Optional<IMKVKonto> optKontakt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(uuid, lehrerkontoDao,
			privatkontoDao);
		if (!optKontakt.isPresent()) {
			throw new UnknownAccountException("kein Lehrerkonto oder Privatkonto mit UUID [" + uuid + "] gefunden");
		}
		final AccessToken accessToken = accessTokenDAO.generateNewAccessToken(authenticationInfo);
		invalidateSessionQuietly(bearer);
		return new SessionToken(accessToken.getAccessTokenId(), accessToken.getCsrfToken());
	}

	/**
	 *
	 *
	 * @param autor
	 */
	private IMKVKonto storeLoginTimeQuietly(final IMKVKonto kontakt) {
		try {
			final Person a = kontakt.getPerson();
			a.setLastLogin(new Date());
			final Role role = kontakt.getRole();
			switch (role) {
			case MKV_LEHRER:
				return lehrerkontoDao.persist((Lehrerkonto) kontakt);
			case MKV_PRIVAT:
				return privatkontoDao.persist((Privatkonto) kontakt);
			default:
				LOG.error("Kontakt hatte unbekannte Role {}", role.toString());
				return kontakt;
			}
		} catch (final PersistenceException e) {
			final Optional<EgladilConcurrentModificationException> optOpt = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optOpt.isPresent()) {
				LOG.warn("Konkurrierendes Update beim eigenen Login: " + optOpt.get().getMessage());
			}
			final Optional<EgladilDuplicateEntryException> optDup = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optDup.isPresent()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + " EgladilDuplicateEntryException beim Speichern der Loginzeit");
			}
			LOG.warn("Logintime des MKV-Users [" + kontakt.getUuid() + "] konnte nicht gespeichert werden: " + e.getMessage());
			// Hier nochmal nachlesen?
			return kontakt;
		} catch (final Exception e) {
			LOG.error("Fehler beim Speichern des Loginzeitpunkts zum MKV-User [{}]: {}", kontakt.getUuid(), e.getMessage(), e);
			return kontakt;
		}
	}

	/**
	 * Speichert die Logoutzeit, protokolliert das Logout-Ereignis und invalisdiert die Session. Alle Exceptions werden
	 * nur gelogged.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param benutzerUuid
	 * @param bearer
	 */
	public String logoutQuietly(final String benutzerUuid, final String bearer) {
		Optional<IMKVKonto> optKontakt = Optional.empty();
		IMKVKonto persistedKontakt = null;
		try {
			optKontakt = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(benutzerUuid, lehrerkontoDao,
				privatkontoDao);
			if (optKontakt.isPresent()) {
				final IMKVKonto kontakt = optKontakt.get();
				final Person a = kontakt.getPerson();
				a.setLastLogout(new Date());
				final Role role = kontakt.getRole();
				switch (role) {
				case MKV_LEHRER:
					persistedKontakt = lehrerkontoDao.persist((Lehrerkonto) kontakt);
					break;
				case MKV_PRIVAT:
					persistedKontakt = privatkontoDao.persist((Privatkonto) kontakt);
					break;
				default:
					LOG.error("Kontakt hatte unbekannte Role {}", role.toString());
				}
			} else {
				final String was = "Kontakt wurde nicht gefunden";
				protokollService.protokollieren(Ereignisart.LOGOUT.getKuerzel(), benutzerUuid, was);
			}
		} catch (final PersistenceException e) {
			LOG.error("Logouttime des MKV-Users [" + benutzerUuid + "] konnte nicht gespeichert werden: " + e.getMessage());
		} catch (final Throwable e) {
			LOG.error("Fehler beim Speichern des Loginzeitpunkts zum MKV-User [{}]: {}", benutzerUuid, e.getMessage(), e);
		} finally {
			// das CSRF-Token wird als Attribut der AccessTokens mit gelöscht. Das temporäre CSRF-Token wurde
			// bereits beim Login gelöscht.
			invalidateSessionQuietly(bearer);
			if (persistedKontakt != null) {
				final String rolle = persistedKontakt.getRole() == null ? "" : persistedKontakt.getRole().toString();
				protokollService.protokollieren(Ereignisart.LOGOUT.getKuerzel(), benutzerUuid, rolle);
			} else if (optKontakt.isPresent()) {
				final String rolle = optKontakt.get().getRole().toString();
				protokollService.protokollieren(Ereignisart.LOGOUT.getKuerzel(), benutzerUuid, rolle);
			}
		}

		return "";
	}

	/**
	 * Entfernt die SessionId.
	 *
	 * @param csrfToken
	 */
	void invalidateTemporaryCsrfTokenQuietly(final String csrfToken) {
		try {
			accessTokenDAO.invalidateTemporaryCsrfToken(csrfToken);
		} catch (final Exception e) {
			LOG.error("Session konnte nicht entwertet werden: " + e.getMessage(), e);
		}
	}

	/**
	 * Entfernt die SessionId.
	 *
	 * @param accessTokenId
	 */
	public void invalidateSessionQuietly(final String bearer) {
		try {
			final String accessTokenId = new AccessTokenUtils().extractAccessTokenId(bearer);
			accessTokenDAO.invalidateAccessToken(accessTokenId);
		} catch (final Throwable e) {
			LOG.error("Session konnte nicht entwertet werden: " + e.getMessage(), e);
		}
	}
}
