//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.EmailBasedCredentials;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.IAuthConstants;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.SessionToken;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.crypto.provider.EgladilEncryptionException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.payload.response.Kontext;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.service.benutzer.SessionDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * SessionResource
 */
@Singleton
@Path("/session")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_HTML })
public class SessionResource {

	private static final Logger LOG = LoggerFactory.getLogger(SessionResource.class);

	private SessionDelegate sessionDelegate;

	private IAccessTokenDAO accessTokenDAO;

	private IAuthenticationService authenticationService;

	private ValidationDelegate validationDelegate = new ValidationDelegate();

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * SessionResource
	 */
	public SessionResource() {
	}

	/**
	 * SessionResource
	 */
	@Inject
	public SessionResource(final SessionDelegate sessionDelegate, final IAccessTokenDAO accessTokenDAO,
		final IAuthenticationService authenticationService) {
		super();
		this.sessionDelegate = sessionDelegate;
		this.accessTokenDAO = accessTokenDAO;
		this.authenticationService = authenticationService;
	}

	@OPTIONS
	@Timed
	public Response optionsLogin() {
		return Response.ok().build();
	}

	/**
	 * Tja, eben einloggen.
	 *
	 * @param credentials
	 * @param tempCsrfToken String das wird hier benötigt, weil es invalidiert werden muss.
	 * @return
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(final EmailBasedCredentials credentials, @HeaderParam("X-XSRF-TOKEN") final String tempCsrfToken) {
		UsernamePasswordToken usernamePasswordToken = null;


		if (KontextReader.getInstance().getKontext().isDumpPayload()) {
			LOG.info("Payload-Dump: {}", credentials);
		}


		try {
			validationDelegate.check(credentials, EmailBasedCredentials.class);
			usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(), credentials.getPassword().toCharArray());

			final AuthenticationInfo authenticationInfo = authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV);

			final MKVBenutzer benutzer = sessionDelegate.getBenutzer((Benutzerkonto) authenticationInfo);
			final SessionToken sessionToken = sessionDelegate.getSession(authenticationInfo, tempCsrfToken);

			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("erfolgreich eingeloggt"), benutzer);
			entity.setSessionToken(sessionToken);

			return Response.ok().entity(entity).build();

		} catch (final EgladilWebappException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, credentials);
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			final String details = credentials.toString();
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), details);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilConcurrentModificationException e) {
			// Dann müsste der Anwender die Daten nochmals senden.
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE
				+ " beim Einloggen - EgladilConcurrentModificationException speichern der Loginzeit sollte schon gefangen sein.");
			return exceptionHandler.mapToMKVApiResponse(e, "general.concurrent", null);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} finally {
			if (credentials != null) {
				credentials.clear();
			}
			if (usernamePasswordToken != null) {
				usernamePasswordToken.clear();
			}
		}
	}

	/**
	 *
	 * @param principal Principal fürs authentication framework. Die Annotation bewirkt, dass der Parameter durch
	 * io.dropwizard.auth gesetzt wird. Der Request sollte diesen tunlichst nicht enthalten.
	 *
	 * @param bearer String - der Parameter muss hier explizit ausgelesen werden zum Ungüligmachen der Session, da das
	 * AuthenticationToken transparent vom Framework geprüft wird und an dieser Stelle kein Zugang dazu besteht. Die
	 * Session-ID kommt mit vorangestelltem 'Bearer ' an.
	 *
	 * @return Response
	 */
	@DELETE
	@Timed
	public Response logout(@Auth final Principal principal, @HeaderParam(IAuthConstants.BEARER_KEY) final String bearer) {
		// @Auth ist eine dropwizard-Abkürzung zum SecurityContext. Auch möglich wäre stattdessen
		// @Context SecurityContext secContext als Parameter und secContext.getPrincipal()...
		try {
			sessionDelegate.logoutQuietly(principal.getName(), bearer);
			final APIResponsePayload entity = createAnonymousSession();
			return Response.ok().entity(entity).build();
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			try {
				final APIResponsePayload entity = createAnonymousSession();
				return Response.ok().entity(entity).build();
			} catch (final Exception e1) {
				LOG.error("Unerwartete Exception  beim Erzeugen einer anonymen Session: {}", e.getMessage(), e);
				return exceptionHandler.mapToMKVApiResponse(e, null, null);
			}
		}
	}

	/**
	 * Holt den Kontext mit einer temporären Session
	 *
	 * @return Response mit APIResponsePayload - Kontext - Payload
	 */
	@GET
	@Timed
	@Path("kontext")
	public Response getKontext() {
		try {
			final APIResponsePayload entity = createAnonymousSession();
		// @formatter:off
		return Response
			.ok()
			.entity(entity)
			.type(MediaType.APPLICATION_JSON)
			.encoding("UTF-8")
			.build();
        // @formatter:on
		} catch (final EgladilEncryptionException e) {
			LOG.error("temp CSRF-Token konnte nicht generiert werden: {}", e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Unerwartete Exception: {}", e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 *
	 *
	 * @return
	 * @throws Exception
	 */

	APIResponsePayload createAnonymousSession() throws Exception {
		final Kontext kontext = KontextReader.getInstance().getKontext();
		final String csrfToken = authenticationService.createCsrfToken();
		accessTokenDAO.registerTemporaryCsrfToken(csrfToken);
		kontext.setXsrfToken(csrfToken);

		final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), kontext);
		return entity;
	}

	/**
	 * Zu Testzwecken ein Mock.
	 *
	 * @param validationDelegate ValidationDelegate
	 */
	void setValidationDelegate(final ValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}
}
