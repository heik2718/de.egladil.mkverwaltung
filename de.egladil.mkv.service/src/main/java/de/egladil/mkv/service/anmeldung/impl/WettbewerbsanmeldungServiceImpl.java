//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService;
import de.egladil.mkv.service.benutzer.LehrerkontoDelegate;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * WettbewerbsanmeldungServiceImpl
 */
@Singleton
public class WettbewerbsanmeldungServiceImpl implements IWettbewerbsanmeldungService {

	private static final Logger LOG = LoggerFactory.getLogger(WettbewerbsanmeldungServiceImpl.class);

	private final IBenutzerService benutzerService;

	private final IPrivatkontoDao privatkontoDao;

	private final LehrerkontoDelegate lehrerkontoDelegate;

	private final BenutzerPayloadMapper benutzerPayloadMapper;

	private final MailDelegate mailDelegate;

	private final AuswertungsgruppenService auswertungsgruppenService;

	/**
	 * Erzeugt eine Instanz von WettbewerbsanmeldungServiceImpl
	 */
	@Inject
	public WettbewerbsanmeldungServiceImpl(final IBenutzerService benutzerService, final LehrerkontoDelegate lehrerkontoDelegate,
		final IPrivatkontoDao privatkontoDao, final BenutzerPayloadMapper benutzerPayloadMapper, final MailDelegate mailDelegate,
		final AuswertungsgruppenService auswertungsgruppenService) {
		this.benutzerService = benutzerService;
		this.lehrerkontoDelegate = lehrerkontoDelegate;
		this.privatkontoDao = privatkontoDao;
		this.benutzerPayloadMapper = benutzerPayloadMapper;
		this.mailDelegate = mailDelegate;
		this.auswertungsgruppenService = auswertungsgruppenService;
	}

	/**
	 * @see de.egladil.mkv.service.anmeldung.IWettbewerbsanmeldungService#benutzerZumWettbewerbAnmelden(de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung,
	 * String, Role)
	 */
	@Override
	public MKVBenutzer benutzerZumWettbewerbAnmelden(final Wettbewerbsanmeldung anmeldung, final String benutzerUUID,
		final Role role)
		throws EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException, MKVException {

		if (anmeldung == null) {
			throw new IllegalArgumentException("wettbewerbsanmeldung ist null");
		}
		if (benutzerUUID == null) {
			throw new IllegalArgumentException("benutzerUUID ist null");
		}
		if (role == null) {
			throw new IllegalArgumentException("role ist null");
		}
		final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(benutzerUUID);

		if (benutzerkonto == null) {
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "benutzerkonto mit UUID [" + benutzerUUID + "] nicht gefunden");
			throw new MKVException("benutzerkonto mit UUID [" + benutzerUUID + "] nicht gefunden");
		}
		final IGenerateTeilnahmeStrategy generateTeilnahmeStrategy = IGenerateTeilnahmeStrategy.newInstance(lehrerkontoDelegate,
			privatkontoDao, role);
		final TeilnahmeIdentifierProvider teilnahme = generateTeilnahmeStrategy.generateTeilnahme(anmeldung,
			benutzerkonto.getUuid());
		if (isSchulteilnahme(teilnahme)) {
			this.auswertungsgruppenService.findOrCreateRootAuswertungsgruppe(teilnahme.provideTeilnahmeIdentifier());
		}

		final MKVBenutzer result = benutzerPayloadMapper.createBenutzer(benutzerkonto, false);
		try {
			mailDelegate.wettbewerbsanmeldungsmailVersenden(benutzerkonto.getEmail(), anmeldung.getJahr(), role);
		} catch (final EgladilMailException e) {
			LOG.warn("Wettbewerbsanmeldungsmail konnte nicht versendet werden: " + result.toString() + " - " + e.getMessage());
		}
		return result;
	}

	private boolean isSchulteilnahme(final TeilnahmeIdentifierProvider teilnahme) {
		return teilnahme != null && Teilnahmeart.S == teilnahme.provideTeilnahmeIdentifier().getTeilnahmeart();
	}
}
