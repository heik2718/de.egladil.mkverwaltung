//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.egladil.email.service.IEmailDaten;

/**
 * @author heikew
 *
 */
public class MKVMail implements IEmailDaten {

	private String empfaenger;

	private String betreff;

	private String text;

	private Set<String> hiddenEmpfaenger;

	/**
	 *
	 */
	public MKVMail() {
	}

	/**
	 * @param empfaenger
	 * @param betreff
	 * @param text
	 */
	public MKVMail(String empfaenger, String betreff, String text) {
		super();
		this.empfaenger = empfaenger;
		this.betreff = betreff;
		this.text = text;
	}

	@Override
	public List<String> alleEmpfaengerFuersLog() {
		List<String> result = new ArrayList<>();
		if (empfaenger != null) {
			result.add(empfaenger);
		}
		if (hiddenEmpfaenger != null) {
			result.addAll(hiddenEmpfaenger);
		}
		return result;
	}

	public void addHiddenEmpfaenger(String hempf) {
		if (hiddenEmpfaenger == null) {
			hiddenEmpfaenger = new HashSet<>();
		}
		hiddenEmpfaenger.add(hempf);
	}

	public void addAllHiddenEmpfaenger(Collection<String> alle) {
		if (hiddenEmpfaenger == null) {
			hiddenEmpfaenger = new HashSet<>();
		}
		hiddenEmpfaenger.addAll(alle);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.email.service.IEmailDaten#getEmpfaenger()
	 */
	@Override
	public String getEmpfaenger() {
		return empfaenger;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.email.service.IEmailDaten#getBetreff()
	 */
	@Override
	public String getBetreff() {
		return betreff;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.email.service.IEmailDaten#getText()
	 */
	@Override
	public String getText() {
		return text;
	}

	/**
	 * @param empfaenger the empfaenger to set
	 */
	public void setEmpfaenger(String empfaenger) {
		this.empfaenger = empfaenger;
	}

	/**
	 * @param betreff the betreff to set
	 */
	public void setBetreff(String betreff) {
		this.betreff = betreff;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getHiddenEmpfaenger()
	 */
	@Override
	public Collection<String> getHiddenEmpfaenger() {
		return hiddenEmpfaenger == null ? Collections.emptySet() : Collections.unmodifiableSet(hiddenEmpfaenger);
	}
}
