//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.registrierung;

import de.egladil.mkv.service.payload.request.Lehrerregistrierung;

/**
 * ILehrerRegistrierungService.
 */
public interface ILehrerRegistrierungService extends IRegistrierungService<Lehrerregistrierung> {

	String CONTEXT_MESSAGE_REGISTRIERUNG_GESCHEITERT_PERSIST_LEHRER = "Persistieren Schulteilnahme und Lehrerkonto";

	String CONTEXT_MESSAGE_REGISTRIERUNG_GESCHEITERT_PERSIST_SCHULTEILNAHME = "Persistieren Lehrerkonto gescheitert";
}
