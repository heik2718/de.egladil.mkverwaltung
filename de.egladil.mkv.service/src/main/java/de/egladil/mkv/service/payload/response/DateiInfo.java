//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import de.egladil.bv.aas.domain.Role;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.service.files.DateiKategorie;

/**
 * DateiInfo stellt Infos über herunterladbare Dateien zur Verfügung.
 */
public class DateiInfo {

	private String dateiname;

	private String beschreibung;

	private String groesse;

	private String kategorie;

	private String klasse;

	private DateiKategorie myKategorie;

	private Klassenstufe myKlasse;

	private String dateiUrl;

	private List<String> rolenames = new ArrayList<>();

	/**
	 * DateiInfo
	 */
	public DateiInfo() {
	}

	public DateiInfo(final DateiKategorie kategorie, final Klassenstufe klassenstufe) {
		this.myKategorie = kategorie;
		this.myKlasse = klassenstufe;
		this.kategorie = kategorie.toString();
		if (klassenstufe != null) {
			this.klasse = klassenstufe.getLabel();
		}
		addRolename(Role.MKV_LEHRER.toString());
	}

	public void addRolename(final String rolename) {
		this.rolenames.add(rolename);
	}

	public final String getDateiname() {
		return dateiname;
	}

	public final void setDateiname(final String dateiname) {
		this.dateiname = dateiname;
	}

	public final String getBeschreibung() {
		return beschreibung;
	}

	public final void setBeschreibung(final String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public final String getGroesse() {
		return groesse;
	}

	public final void setGroesse(final String groesse) {
		this.groesse = groesse;
	}

	public final String getKategorie() {
		return kategorie;
	}

	public final String getKlasse() {
		return klasse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateiname == null) ? 0 : dateiname.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DateiInfo other = (DateiInfo) obj;
		if (dateiname == null) {
			if (other.dateiname != null) {
				return false;
			}
		} else if (!dateiname.equals(other.dateiname)) {
			return false;
		}
		return true;
	}

	public final DateiKategorie getMyKategorie() {
		return myKategorie;
	}

	public final Klassenstufe getMyKlasse() {
		return myKlasse;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("DateiInfo [dateiname=");
		builder.append(dateiname);
		builder.append("]");
		return builder.toString();
	}

	public final String getDateiUrl() {
		return dateiUrl;
	}

	public final void setDateiUrl(final String resourcePath) {
		this.dateiUrl = resourcePath;
	}

	public boolean allowedForRoles(final Collection<String> rolenames) {
		final Iterator<String> iter = rolenames.iterator();
		while (iter.hasNext()) {
			if (this.rolenames.contains(iter.next())) {
				return true;
			}
		}
		return false;
	}
}
