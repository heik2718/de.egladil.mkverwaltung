//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.utils;

import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.plausi.ConvertToTeilnahmeartCommand;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * TeilnahmeIdentifierDelegate
 */
public class TeilnahmeIdentifierDelegate {

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	/**
	 * Erzeugt und validiert einen TeilnahmeIdentifier.
	 *
	 * @param nameTeilnahmeart String
	 * @param teilnahmekuerzel String
	 * @param jahr Strinh
	 * @return TeilnahmeIdentifier
	 */
	public TeilnahmeIdentifier createAndValidateTeilnahmeIdentifier(final String nameTeilnahmeart, final String teilnahmekuerzel,
		final String jahr) {
		final ConvertToTeilnahmeartCommand cmd = new ConvertToTeilnahmeartCommand(nameTeilnahmeart);
		cmd.execute();
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.create(cmd.getTeilnahmeart(), teilnahmekuerzel, jahr);
		validationDelegate.check(teilnahmeIdentifier, TeilnahmeIdentifier.class);
		return teilnahmeIdentifier;
	}
}
