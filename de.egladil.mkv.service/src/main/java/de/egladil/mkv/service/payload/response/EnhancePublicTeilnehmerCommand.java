//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.response;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import de.egladil.mkv.persistence.converter.AntwortcodeConverter;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.payload.response.PublicTeilnehmer;
import de.egladil.mkv.persistence.renderer.PunkteRenderer;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.utils.ICommand;

/**
 * EnhancePublicTeilnehmerCommand
 */
public class EnhancePublicTeilnehmerCommand implements ICommand {

	private final PublicTeilnehmer publicTeilnehmer;

	private final TeilnehmerFacade teilnehmerFacade;

	private final AntwortcodeConverter antwortcodeConverter;

	/**
	 * EnhancePublicTeilnehmerCommand
	 */
	public EnhancePublicTeilnehmerCommand(final PublicTeilnehmer publicTeilnehmer, final TeilnehmerFacade teilnehmerFacade) {
		if (publicTeilnehmer == null) {
			throw new IllegalArgumentException("publicTeilnehmer null");
		}
		if (teilnehmerFacade == null) {
			throw new IllegalArgumentException("teilnehmerFacade null");
		}
		this.publicTeilnehmer = publicTeilnehmer;
		this.teilnehmerFacade = teilnehmerFacade;
		this.antwortcodeConverter = new AntwortcodeConverter();
	}

	@Override
	public void execute() {
		if (StringUtils.isBlank(publicTeilnehmer.getLoesungszettelKuerzel())) {
			return;
		}
		final Optional<Loesungszettel> opt = teilnehmerFacade.getUniqueLoesungszettel(publicTeilnehmer.getLoesungszettelKuerzel());
		if (!opt.isPresent()) {
			return;
		}
		final Loesungszettel loesungszettel = opt.get();
		publicTeilnehmer.setPunkte(new PunkteRenderer(loesungszettel.getPunkte().intValue()).render());
		publicTeilnehmer.setKaengurusprung(loesungszettel.getKaengurusprung());
		publicTeilnehmer
			.setAntworten(antwortcodeConverter.fromAntwortcode(loesungszettel.getLoesungszettelRohdaten().getAntwortcode()));
	}
}
