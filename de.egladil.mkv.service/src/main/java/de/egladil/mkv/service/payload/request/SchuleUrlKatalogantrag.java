//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.HttpUrl;

/**
 * SchuleKatalogeintrag
 */
public class SchuleUrlKatalogantrag extends AbstractSchuleKatalogantrag {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@HttpUrl
	@NotBlank
	@Size(min = 1, max = 300)
	private String url;

	@Override
	public String toBotLog() {
		String result = "SchuleAnschriftKatalogantrag " + super.toBotLog();
		result += ", url=" + url + "]";
		return result;
	}

	@Override
	public String toString() {
		return super.toString() + ", url=" + url;
	}

	/**
	 * Liefert die Membervariable url
	 *
	 * @return die Membervariable url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param url neuer Wert der Membervariablen url
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * @see de.egladil.mkv.service.payload.request.AbstractSchuleKatalogantrag#ownClass()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends ILoggable> Class<T> ownClass() {
		return (Class<T>) SchuleUrlKatalogantrag.class;
	}
}
