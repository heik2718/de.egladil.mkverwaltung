//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.service.confirmation.ILehrerConfirmationService;

/**
 * LehrerConfirmationServiceImpl
 */
@Singleton
public class LehrerConfirmationServiceImpl extends AbstractConfirmationService
	implements ILehrerConfirmationService, IKontaktUndTeilnahmeCallback {

	private final ILehrerkontoDao lehrerkontoDao;

	private final ISchulteilnahmeDao schulteilnahmeDao;

	/**
	 * Erzeugt eine Instanz von LehrerConfirmationServiceImpl
	 */
	@Inject
	public LehrerConfirmationServiceImpl(final IRegistrierungService registrierungService, final IBenutzerService benutzerService,
		final IAnonymisierungsservice anonymsierungsservice, final ILehrerkontoDao lehrerkontoDao,
		final ISchulteilnahmeDao schulteilnahmeDao) {
		super(registrierungService, benutzerService, anonymsierungsservice);
		this.lehrerkontoDao = lehrerkontoDao;
		this.schulteilnahmeDao = schulteilnahmeDao;
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IKontaktUndTeilnahmeCallback#persist(java.util.List)
	 */
	@Override
	public void persist(final List<TeilnahmeIdentifierProvider> teilnahmen)
		throws EgladilConcurrentModificationException, EgladilDuplicateEntryException, MKVException {
		try {
			schulteilnahmeDao.persist(toSchulteilnahmen(teilnahmen));
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> integrityException = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(e);
			if (integrityException.isPresent()) {
				throw integrityException.get();
			}
			final Optional<EgladilConcurrentModificationException> optEx = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optEx.isPresent()) {
				final String msg = "Jemand anders hat die Teilnahmen inzwischen geändert";
				throw new EgladilConcurrentModificationException(msg);
			}
			throw new MKVException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IKontaktUndTeilnahmeCallback#persist(de.egladil.mkv.persistence.domain.IMKVKonto)
	 */
	@Override
	public IMKVKonto persist(final IMKVKonto kontakt) {
		return lehrerkontoDao.persist((Lehrerkonto) kontakt);
	}

	private List<Schulteilnahme> toSchulteilnahmen(final List<TeilnahmeIdentifierProvider> teilnahmen) {
		if (teilnahmen == null) {
			throw new MKVException("teilnahmen darf nicht null sein!");
		}
		final List<Schulteilnahme> result = new ArrayList<>();
		for (final TeilnahmeIdentifierProvider t : teilnahmen) {
			result.add((Schulteilnahme) t);
		}
		return result;
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.AbstractConfirmationService#findKontaktByUUID(java.lang.String)
	 */
	@Override
	protected Optional<IMKVKonto> findKontaktByUUID(final String uuid) {
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(uuid);
		return opt.isPresent() ? Optional.of((IMKVKonto) opt.get()) : Optional.empty();
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.AbstractConfirmationService#getKontaktUndTeilnahmeCallback()
	 */
	@Override
	protected IKontaktUndTeilnahmeCallback getKontaktUndTeilnahmeCallback() {
		return this;
	}
}
