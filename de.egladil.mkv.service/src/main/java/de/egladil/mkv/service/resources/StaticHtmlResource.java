//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Dateiname;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.service.files.DownloadDelegate;
import de.egladil.mkv.service.payload.response.DateiInfo;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * StaticHtmlResource
 */
@Singleton
@Path("htmlcontent")
public class StaticHtmlResource {

	private static final Logger LOG = LoggerFactory.getLogger(StaticHtmlResource.class);

	private String downloadPath;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	private final DownloadDelegate downloadDelegate;

	private class Payload implements ILoggable {

		/* serialVersionUID */
		private static final long serialVersionUID = 1L;

		@Dateiname
		private final String value;

		public Payload(final String value) {
			this.value = value;
		}

		@Override
		public String toBotLog() {
			return value;
		}
	}

	/**
	 * StaticHtmlResource
	 */
	@Inject
	public StaticHtmlResource(final DownloadDelegate downloadDelegate) {
		this.downloadDelegate = downloadDelegate;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response getFileList() {

		try {
			final List<DateiInfo> liste = downloadDelegate.getInfosStaticHtmlContent(downloadPath);
			LOG.debug("{}: Anzahl Dateien: {}", downloadPath, liste.size());
			final APIResponsePayload payload = new APIResponsePayload(APIMessage.info(""), liste);
			return Response.ok().entity(payload).build();
		} catch (final MKVException e) {
			LOG.error(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@GET
	@Path("{dateiname}")
	@Produces(MediaType.TEXT_HTML)
	@Timed
	public Response getStaticContent(@PathParam("dateiname") final String dateiname) {

		final Payload payload = new Payload(dateiname);
		final String pathFile = downloadPath + File.separator + dateiname;

		try {
			validationDelegate.check(payload, Payload.class);
			final String content = downloadDelegate.getAsString(pathFile);
			return Response.ok().entity(content).build();
		} catch (final EgladilWebappException e) {
			LOG.warn("Fehlerhafter dateiname {}", dateiname);
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (final IOException e) {
			LOG.warn("Datei {} nicht gefunden oder keine Berechtigung: {}", pathFile, e.getMessage());
			return Response.status(Status.NOT_FOUND).build();
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return Response.serverError().build();
		}
	}

	public final void setDownloadPath(final String downloadPath) {
		this.downloadPath = downloadPath + File.separator + "html";
	}
}
