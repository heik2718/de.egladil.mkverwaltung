//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.mkv.service.confirmation.ILehrerConfirmationService;
import de.egladil.mkv.service.confirmation.impl.ConfirmationStatus;
import de.egladil.mkv.service.payload.request.AbstractMKVRegistrierung;
import de.egladil.mkv.service.payload.request.Lehrerregistrierung;
import de.egladil.mkv.service.registrierung.ILehrerRegistrierungService;
import de.egladil.mkv.service.registrierung.impl.LehrerRegistrierungServiceImpl;

/**
 * ResgisterSchuleResource
 */
@Singleton
@Path("/registrierungen")
public class LehrerregistrierungResource extends AbstractRegistrierungResource {

	private final ILehrerRegistrierungService lehrerRegistrierungService;

	private final ILehrerConfirmationService confirmationService;

	/**
	 * Erzeugt eine Instanz von RegisterSchuleResource
	 */
	@Inject
	public LehrerregistrierungResource(final ILehrerRegistrierungService lehrerRegistrierungService,
		final ILehrerConfirmationService confirmationService) {
		super();
		this.lehrerRegistrierungService = lehrerRegistrierungService;
		this.confirmationService = confirmationService;
	}

	@POST
	@Timed
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/lehrer")
	public Response register(final Lehrerregistrierung payload) {
		return super.doRegister(payload, Lehrerregistrierung.class);
	}

	@Override
	protected <T extends AbstractMKVRegistrierung> Aktivierungsdaten kontaktAnlegenUndZumWettbewerbAnmelden(final T payload)
		throws EgladilDuplicateEntryException, EgladilConcurrentModificationException, EgladilStorageException,
		EgladilMailException {
		if (this.isTest()) {
			((LehrerRegistrierungServiceImpl) lehrerRegistrierungService).setTest(true);
		}
		final Aktivierungsdaten aktivierungsdaten = lehrerRegistrierungService.kontaktAnlegenUndZumWettbewerbAnmelden(
			(Lehrerregistrierung) payload, getAktuellesWettebwerbsjahr(), super.isNeuanmeldungFreigegeben());
		return aktivierungsdaten;
	}

	@Override
	@GET
	@Timed
	@Path("/lehrer/confirmation")
	@Produces("text/html")
	public InputStream confirm(@QueryParam("t") final String confirmationCode) {
		return super.confirm(confirmationCode);
	}

	/**
	 * @see de.egladil.mkv.service.resources.AbstractRegistrierungResource#registrierungBestaetigen(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	protected ConfirmationStatus registrierungBestaetigen(final String confirmationCode) {
		final ConfirmationStatus confirmationStatus = confirmationService.jetztRegistrierungBestaetigen(confirmationCode,
			getAktuellesWettebwerbsjahr());
		return confirmationStatus;
	}
}
