//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import java.util.Optional;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.ISchuleDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.enums.Dateiart;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.request.SchulzuordnungAendernPayload;
import de.egladil.mkv.persistence.payload.request.StatusPayload;
import de.egladil.mkv.service.anmeldung.impl.FindOrCreateSchulteilnahmeCommand;

/**
 * LehrerkontoService
 */
@Singleton
public class LehrerkontoDelegate extends AbstractKontoDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(LehrerkontoDelegate.class);

	private final ISchuleDao schuleDao;

	private final ILehrerkontoDao lehrerkontoDao;

	private final ISchulteilnahmeDao schulteilnahmeDao;

	/**
	 * Erzeugt eine Instanz von LehrerkontoService
	 */
	@Inject
	public LehrerkontoDelegate(final ISchuleDao schuleDao, final ILehrerkontoDao lehrerkontoDao,
		final ISchulteilnahmeDao schulteilnahmeDao, final IBenutzerService benutzerService) {
		super(benutzerService);
		this.schuleDao = schuleDao;
		this.lehrerkontoDao = lehrerkontoDao;
		this.schulteilnahmeDao = schulteilnahmeDao;
	}

	/**
	 * Gibt das Lehrerkonto mit der gegebenen UUID zurück, falls existent.
	 *
	 * @param benutzerUUID
	 * @return
	 */
	public Optional<Lehrerkonto> findByUUID(final String benutzerUUID) {
		return lehrerkontoDao.findByUUID(benutzerUUID);
	}

	/**
	 * Verifiziert, dass der payload.benutzername zum Benutzerkonto passt und ändert dann die Daten im Lehrerkonto.
	 *
	 * @param payload
	 * @param benutzerUuid
	 * @return
	 */
	@Override
	public IMKVKonto verifyAndChangePerson(final PersonAendernPayload payload, final String benutzerUuid)
		throws IllegalArgumentException, UnknownAccountException, DisabledAccountException, EgladilConcurrentModificationException,
		EgladilStorageException, ResourceNotFoundException {
		verifyBenutzer(payload, benutzerUuid);
		return changePerson(payload, benutzerUuid);
	}

	/**
	 *
	 * @see de.egladil.mkv.service.benutzer.AbstractKontoDelegate#changePerson(de.egladil.mkv.persistence.payload.request.PersonAendernPayload,
	 * java.lang.String)
	 */
	@Override
	protected IMKVKonto changePerson(final PersonAendernPayload payload, final String benutzerUuid)
		throws EgladilConcurrentModificationException, EgladilStorageException, ResourceNotFoundException {
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(benutzerUuid);
		if (opt.isPresent()) {
			try {
				final Lehrerkonto lehrerkonto = opt.get();
				lehrerkonto.setPerson(new Person(payload.getVorname(), payload.getNachname()));
				final Lehrerkonto result = lehrerkontoDao.persist(lehrerkonto);
				return result;
			} catch (final PersistenceException e) {
				final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
					.toEgladilConcurrentModificationException(e);
				if (optConc.isPresent()) {
					throw new EgladilConcurrentModificationException("Jemand anders hat das Lehrerkonto vorher geaendert");
				}
				throw new EgladilStorageException("PersistenceException beim Speichern eines Lehrerkontos: " + e.getMessage(), e);
			}
		}
		LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + " Benutzerkonto ohne Lehrerkonto [UUID={}]", benutzerUuid);
		throw new ResourceNotFoundException("Ein passendes Lehrerkonto wurde nicht gefunden");
	}

	/**
	 * Wechselt die Schulzuordnung eines Lehrerkontos.
	 *
	 * @param payload
	 * @param benutzerUuid
	 * @return Lehrerkonto
	 * @throws ResourceNotFoundException wenn alte oder neue Schule nicht existieren.
	 * @throws UnknownAccountException falls es kein passendes Lehrerkonto gibt.
	 */
	public Lehrerkonto changeSchule(final SchulzuordnungAendernPayload payload, final Benutzerkonto benutzer,
		final String aktuellesWettbewerbsjahr) throws UnknownAccountException, ResourceNotFoundException, PersistenceException {
		final Optional<Lehrerkonto> optLehrer = lehrerkontoDao.findByUUID(benutzer.getUuid());
		if (!optLehrer.isPresent()) {
			throw new UnknownAccountException("Kein Lehrerkonto mit UUID " + benutzer.getUuid() + " gefunden");
		}
		final Optional<Schule> optAktuell = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			payload.getKuerzelAlt());
		if (!optAktuell.isPresent()) {
			throw new ResourceNotFoundException("alte Schule mit [kuerzel=" + payload.getKuerzelAlt() + "] existriert nicht");
		}

		final Optional<Schule> optNeu = schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME,
			payload.getKuerzelNeu());
		if (!optNeu.isPresent()) {
			throw new ResourceNotFoundException("neue Schule mit [kuerzel=" + payload.getKuerzelNeu() + "] existriert nicht");
		}

		final Lehrerkonto lehrerkonto = optLehrer.get();

		final TeilnahmeIdentifierProvider schulteilnahmeAlt = lehrerkonto.getTeilnahmeZuJahr(aktuellesWettbewerbsjahr);
		if (schulteilnahmeAlt != null) {
			final Schulteilnahme t = (Schulteilnahme) schulteilnahmeAlt;
			lehrerkonto.removeSchulteilnahme(t);
		}

		// final Lehrerteilnahme lehrerteilnahmeAlt = lehrerkonto.getLehrerteilnahmeZuJahr(aktuellesWettbewerbsjahr);
		// Lehrerkonto zwischen = null;
		// if (lehrerteilnahmeAlt != null) {
		// lehrerkonto.removeTeilnahme(lehrerteilnahmeAlt);
		// if (lehrerkonto.getSchule() == null) {
		// System.err.println("alte schule null");
		// }
		// zwischen = lehrerkontoDao.persist(lehrerkonto);
		// LOG.debug("alte Lehrerteilnahme gelöscht.");
		// } else {
		// zwischen = lehrerkonto;
		// }
		// final Optional<Schulteilnahme> optOrphan = schulteilnahmeDao.findOrphan(payload.getKuerzelAlt(),
		// aktuellesWettbewerbsjahr);
		// if (optOrphan.isPresent()) {
		// schulteilnahmeDao.delete(optOrphan.get());
		// }

		final Schule neueSchule = optNeu.get();
		lehrerkonto.setSchule(neueSchule);

		if (payload.isAnmelden()) {
			final FindOrCreateSchulteilnahmeCommand command = new FindOrCreateSchulteilnahmeCommand(schulteilnahmeDao);
			final Schulteilnahme schulteilnahmeNeu = command.findOrCreateSchulteilnahme(neueSchule.getKuerzel(),
				aktuellesWettbewerbsjahr);
			// final Lehrerteilnahme lehrerteilnahmeNeu = new Lehrerteilnahme(schulteilnahmeNeu);
			lehrerkonto.addSchulteilnahme(schulteilnahmeNeu);
		}

		final Lehrerkonto result = lehrerkontoDao.persist(lehrerkonto);
		LOG.debug("Konto {} zum Wettbewerb angemeldet: ", result);
		return result;
	}

	/**
	 * Verifiziert, ob das zum downloadcode gehörige Lehrerkonto existiert, aktiv und nicht gesperrt ist und ob eine
	 * Anmeldung zum gegebenen Wettbewerbsjahr vorhanden ist.
	 *
	 * @param wettbewerbsjahr String das Wettbewerbsjahr.
	 * @param downloadcode String der zum Benutzerkonto gehörende downloadcode
	 * @param dateiart Dateiart
	 */
	public void verifyAndRegisterDownload(final String wettbewerbsjahr, final String downloadcode, final Dateiart dateiart)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException,
		EgladilConcurrentModificationException, EgladilStorageException {

	}

	/**
	 * @see de.egladil.mkv.service.benutzer.AbstractKontoDelegate#specialChangeStatusNewsletter(de.egladil.mkv.persistence.payload.request.StatusPayload,
	 * de.egladil.bv.aas.domain.Benutzerkonto)
	 */
	@Override
	protected IMKVKonto specialChangeStatusNewsletter(final StatusPayload payload, final String benutzerUuid) {
		final Optional<Lehrerkonto> opt = lehrerkontoDao.findByUUID(benutzerUuid);
		if (opt.isPresent()) {
			try {
				final Lehrerkonto lehrerkonto = opt.get();
				lehrerkonto.setAutomatischBenachrichtigen(payload.isNeuerStatus());
				final Lehrerkonto result = lehrerkontoDao.persist(lehrerkonto);
				return result;
			} catch (final PersistenceException e) {
				final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
					.toEgladilConcurrentModificationException(e);
				if (optConc.isPresent()) {
					throw new EgladilConcurrentModificationException("Jemand anders hat das Lehrerkonto vorher geaendert");
				}
				throw new EgladilStorageException("PersistenceException beim Speichern eines Lehrerkontos: " + e.getMessage(), e);
			}
		}
		LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + " Benutzerkonto ohne Lehrerkonto [UUID={}]", benutzerUuid);
		throw new ResourceNotFoundException("Ein passendes Lehrerkonto wurde nicht gefunden");
	}

	/**
	 * Speichert das gegebene Lehrerkonto und gibt das Ergebnis zurück.
	 *
	 * @param lehrerkonto
	 * @return
	 */
	public IMKVKonto kontoSpeichern(final Lehrerkonto lehrerkonto) {
		return lehrerkontoDao.persist(lehrerkonto);
	}

	/**
	 * Sucht die Schule mit dem gegebenen kuerzel.
	 *
	 * @param schulkuerzelNeu
	 * @return
	 */
	public Optional<Schule> getSchuleMitKuerzel(final String schulkuerzelNeu) {
		return schuleDao.findByUniqueKey(Schule.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, schulkuerzelNeu);
	}

	public ISchulteilnahmeDao getSchulteilnahmeDao() {
		return schulteilnahmeDao;
	}
}
