//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.payload.EmailPayload;
import de.egladil.bv.aas.payload.TempPwdAendernPayload;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.service.benutzer.BenutzerkontoDelegate;
import de.egladil.mkv.service.benutzer.PasswortDelegate;
import de.egladil.mkv.service.exception.TempPasswordExpired;
import de.egladil.mkv.service.mail.MailDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;

/**
 * ResetPasswordResource
 */
@Singleton
@Path("/temppwd")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_HTML })
public class TempPasswordResource {

	private static final Logger LOG = LoggerFactory.getLogger(TempPasswordResource.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private ValidationDelegate validationDelegate = new ValidationDelegate();

	private final MailDelegate mailDelegate;

	private final IEgladilConfiguration configuration;

	private final BenutzerkontoDelegate benutzerkontoDelegate;

	private final IProtokollService protokollService;

	private final PasswortDelegate passwortDelegate;

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * Erzeugt eine Instanz von ResetPasswordResource
	 */
	@Inject
	public TempPasswordResource(final IEgladilConfiguration configuration, final BenutzerkontoDelegate benutzerkontoDelegate,
		final PasswortDelegate passwortDelegate, final MailDelegate mailDelegate, final IProtokollService protokollService) {
		this.configuration = configuration;
		this.benutzerkontoDelegate = benutzerkontoDelegate;
		this.passwortDelegate = passwortDelegate;
		this.mailDelegate = mailDelegate;
		this.protokollService = protokollService;
	}

	/**
	 * Fordert einen resetPasswort-Link an. Dieser wird per Mail versendet.
	 *
	 * @param payload EmailPayload
	 * @return Response - 201, wenn link generiert und mail versendet. Entity is ein APIResponsePayload.
	 */
	@POST
	@Timed
	public Response orderResetPassword(final EmailPayload payload) {
		final int gueltigkeitLink = KontextReader.getInstance().getKontext().getGueltigkeitEinmalpasswort();
		try {
			validationDelegate.check(payload, EmailPayload.class);
			final Aktivierungsdaten aktivierungsdaten = benutzerkontoDelegate.generateResetPasswordCode(payload.getEmail(),
				gueltigkeitLink);
			mailDelegate.resetPasswordMailVersenden(aktivierungsdaten);
			protokollService.protokollieren(Ereignisart.KURZZEITPASSWORT_ANGEFORDERT.getKuerzel(),
				aktivierungsdaten.getBenutzerkonto().getUuid(), "Bekannte Mailadresse");
			final APIResponsePayload entity = createOrderResetSuccessMessage(gueltigkeitLink);
			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilWebappException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final UnknownAccountException e) {
			mailDelegate.resetPasswordDummyMailVersenden(payload.getEmail(), gueltigkeitLink);
			protokollService.protokollieren(Ereignisart.KURZZEITPASSWORT_ANGEFORDERT.getKuerzel(), payload.getEmail(), "Unbekannte Mailadresse");
			final APIResponsePayload entity = createOrderResetSuccessMessage(gueltigkeitLink);
			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final DisabledAccountException e) {
			final APIResponsePayload responsePayload = new APIResponsePayload(
				APIMessage.warn(applicationMessages.getString("authentication.disabledAccount")));
			return Response.status(Status.UNAUTHORIZED).entity(responsePayload).build();
		} catch (final InvalidMailAddressException e) {
			final String invalidAddresses = PrettyStringUtils.collectionToDefaultString(e.getAllInvalidAdresses());
			if (e.getAllInvalidAdresses().contains(payload.getEmail())) {
				LOG.warn(
					GlobalConstants.LOG_PREFIX_MAILVERSAND
						+ "Fehler beim Versenden des Kurzzeitpassworts: Empfängeradresse ist ungültig: {}, invalid addresses [{}]",
					e.getMessage(), invalidAddresses);
				return Response.status(Status.BAD_REQUEST)
					.entity(
						new APIResponsePayload(APIMessage.error(applicationMessages.getString("orderResetPassword.invalidEmail"))))
					.build();
			}
			LOG.warn(GlobalConstants.LOG_PREFIX_IMPOSSIBLE + "Passwortmail, Mailempfänger valid: {}, invalid addresses [{}] ",
				e.getMessage(), invalidAddresses);
			final APIResponsePayload entity = createOrderResetSuccessMessage(gueltigkeitLink);
			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilMailException e) {
			LOG.error("Mail zum Passwort Zuruecksetzen konnte nicht versendet werden: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	private APIResponsePayload createOrderResetSuccessMessage(final int gueltigkeitLink) {
		final String msg = MessageFormat.format(applicationMessages.getString("orderResetPassword.success"), gueltigkeitLink);
		final APIResponsePayload result = new APIResponsePayload(APIMessage.info(msg));
		return result;
	}

	/**
	 * Authentiert die Anfrage anhand der Email und des Aktivierungscodes. Falls erfolgreich, wird das Passwort
	 * geändert.
	 *
	 * @param payload TempPwdAendernPayload
	 * @return Response - 200, wenn geändert. Entity is ein APIResponsePayload., 904, falls abgelaufen.
	 */
	@PUT
	@Timed
	public Response passwortAendern(final TempPwdAendernPayload payload) {

		if (KontextReader.getInstance().getKontext().isDumpPayload()) {
			LOG.info("Payload-Dump: {}", payload);
		}


		try {
			final String dummypassword = configuration.getProperty(MKVConstants.CONFIG_KEY_RESET_PASSWORD_DUMMYCODE);
			validationDelegate.check(payload, TempPwdAendernPayload.class);
			final APIResponsePayload pm = passwortDelegate.authentisierenUndPasswortAendern(payload, dummypassword);
			return Response.ok().entity(pm).build();
		} catch (final EgladilWebappException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final TempPasswordExpired e) {
			return Response.status(CustomResponseStatus.NOT_SUCCESSFUL.getStatusCode())
				.entity(new APIResponsePayload(APIMessage.error(applicationMessages.getString("einmalpassword.invalid")))).build();
		} catch (final EgladilAuthenticationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final DisabledAccountException e) {
			return Response.status(Status.UNAUTHORIZED)
				.entity(new APIResponsePayload(APIMessage.warn(applicationMessages.getString("authentication.disabledAccount"))))
				.build();
		} catch (final EgladilConcurrentModificationException e) {
			// Dann müsste der Anwender die Daten nochmals senden.
			return exceptionHandler.mapToMKVApiResponse(e, "einmalpassword.change.concurrent", null);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} finally {
			payload.clear();
		}
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param tempPasswordForTest neuer Wert der Membervariablen tempPasswordForTest
	 */
	public void setTempPasswordForTest(final String tempPasswordForTest) {
		benutzerkontoDelegate.setTempPasswordForTest(tempPasswordForTest);
	}

	/**
	 * Zu Testzwecken.
	 *
	 * @param validationDelegate
	 */
	final void setValidationDelegate(final ValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}
}
