//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Schulregistrierungsanfrage ist valid, wenn clientId MKV sowie password und passwordRepeated gleich sind.<br>
 * <br>
 * Alle anderen Regel werden direkt auf den Attributen validiert.
 */
@Target({ ElementType.TYPE, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MKVRegistrierungValidator.class)
@Documented
public @interface ValidMKVRegistrierung {

	String message() default "{de.egladil.constraints.mkvregistrierung}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
