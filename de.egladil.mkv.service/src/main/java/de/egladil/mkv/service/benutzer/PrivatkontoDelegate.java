//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.benutzer;

import java.util.Optional;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.user.Person;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.payload.request.PersonAendernPayload;
import de.egladil.mkv.persistence.payload.request.StatusPayload;

/**
 * PrivatkontoDelegate
 */
@Singleton
public class PrivatkontoDelegate extends AbstractKontoDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(PrivatkontoDelegate.class);

	private final IPrivatkontoDao privatkontoDao;

	/**
	 * Erzeugt eine Instanz von PrivatkontoDelegate
	 */
	@Inject
	public PrivatkontoDelegate(final IPrivatkontoDao privatkontoDao, final IBenutzerService benutzerService) {
		super(benutzerService);
		this.privatkontoDao = privatkontoDao;
	}

	/**
	 * @see de.egladil.mkv.service.benutzer.AbstractKontoDelegate#changePerson(de.egladil.mkv.persistence.payload.request.PersonAendernPayload,
	 * java.lang.String)
	 */
	@Override
	protected IMKVKonto changePerson(final PersonAendernPayload payload, final String benutzerUuid) {
		final Optional<Privatkonto> opt = privatkontoDao.findByUUID(benutzerUuid);
		if (!opt.isPresent()) {
			LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + " Benutzerkonto ohne Privatkonto [UUID={}]", benutzerUuid);
			throw new ResourceNotFoundException("Ein passendes Privatkonto wurde nicht gefunden");
		}
		try {
			final Privatkonto privatkonto = opt.get();
			privatkonto.setPerson(new Person(payload.getVorname(), payload.getNachname()));
			final Privatkonto result = privatkontoDao.persist(privatkonto);
			return result;
		} catch (final PersistenceException e) {
			final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optConc.isPresent()) {
				throw new EgladilConcurrentModificationException("Jemand anders hat das Privatkonto vorher geaendert");
			}
			throw new EgladilStorageException("PersistenceException beim Speichern eines Privatkonto: " + e.getMessage(), e);
		}
	}

	/**
	 * @see de.egladil.mkv.service.benutzer.AbstractKontoDelegate#specialChangeStatusNewsletter(de.egladil.mkv.persistence.payload.request.StatusPayload,
	 * java.lang.String)
	 */
	@Override
	protected IMKVKonto specialChangeStatusNewsletter(final StatusPayload payload, final String benutzerUuid) {
		final Optional<Privatkonto> opt = privatkontoDao.findByUUID(benutzerUuid);
		if (!opt.isPresent()) {
			LOG.warn(GlobalConstants.LOG_PREFIX_DATENMUELL + " Benutzerkonto ohne Privatkonto [UUID={}]", benutzerUuid);
			throw new ResourceNotFoundException("Ein passendes Privatkonto wurde nicht gefunden");
		}
		try {
			final Privatkonto privatkonto = opt.get();
			privatkonto.setAutomatischBenachrichtigen(payload.isNeuerStatus());
			final Privatkonto result = privatkontoDao.persist(privatkonto);
			return result;
		} catch (final PersistenceException e) {
			final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optConc.isPresent()) {
				throw new EgladilConcurrentModificationException("Jemand anders hat das Privatkonto vorher geaendert");
			}
			throw new EgladilStorageException("PersistenceException beim Speichern eines Privatkonto: " + e.getMessage(), e);
		}
	}
}
