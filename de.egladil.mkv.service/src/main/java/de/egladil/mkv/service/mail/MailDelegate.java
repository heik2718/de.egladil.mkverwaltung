//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.mail;

import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.config.TimeUtils;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.InvalidMailAddressException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.service.payload.request.AbstractSchuleKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;
import de.egladil.mkv.service.vo.MKVMail;

/**
 * MailDelegate
 */
public class MailDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(MailDelegate.class);

	private static final String CONFIG_KEY_SUBJECT_REGISTRIERUNG = "mail.registrierung.subject";

	private static final String CONFIG_KEY_SUBJECT_PASSWORT_NEU = "mail.passwort.neu.subject";

	private static final String CONFIG_KEY_URL_REGISTRIERUNG_SCHULE = "confirm.registrierung.lehrer.url";

	private static final String CONFIG_KEY_URL_REGISTRIERUNG_PRIVAT = "confirm.registrierung.privat.url";

	private static final String CONFIG_KEY_MAILSERVER_VORHANDEN = "mail.mailserver.vorhanden";

	private static final String CONFIG_KEY_URL_RESET_PASSWORD = "mail.resetpassword.url";

	private static final String CONFIG_KEY_SUBJECT_RESET_PASSWORD = "mail.resetpassword.subject";

	private static final String CONFIG_KEY_SUBJECT_UPLOAD = "mail.upload.subject";

	private static final String CONFIG_KEY_SUBJECT_ANMELDUNG = "mail.anmeldung.subject";

	private final MailtextGenerator mailtextGenerator;

	private IMailservice mailService;

	private final IEgladilConfiguration configuration;

	private boolean sendMail;

	/**
	 * Erzeugt eine Instanz von MailDelegate
	 */
	@Inject
	public MailDelegate(final IMailservice mailService, final IEgladilConfiguration configuration) {
		this.mailService = mailService;
		this.configuration = configuration;
		this.mailtextGenerator = new MailtextGenerator();
		if (configuration != null) {
			sendMail = Boolean.valueOf(configuration.getProperty(CONFIG_KEY_MAILSERVER_VORHANDEN));
		}
	}

	public MKVMail schulregistrierungsmailVersenden(final Aktivierungsdaten aktivierungsdaten, final String jahr,
		final String classpathMailtemplate) throws EgladilMailException {
		final String link = configuration.getProperty(CONFIG_KEY_URL_REGISTRIERUNG_SCHULE)
			+ aktivierungsdaten.getConfirmationCode();
		final Benutzerkonto benutzerkonto = aktivierungsdaten.getBenutzerkonto();
		return this.registrierungsmailVersenden(benutzerkonto.getEmail(), aktivierungsdaten.getExpirationTime(), link, jahr,
			classpathMailtemplate);
	}

	/**
	 *
	 * @param aktivierungsdaten
	 * @param jahr
	 * @return
	 * @throws EgladilMailException
	 */
	@Deprecated
	public MKVMail privatregistrierungsmailVersendenAngularJS(final Aktivierungsdaten aktivierungsdaten, final String jahr)
		throws EgladilMailException {
		final String link = configuration.getProperty(CONFIG_KEY_URL_REGISTRIERUNG_PRIVAT)
			+ aktivierungsdaten.getConfirmationCode();
		final Benutzerkonto benutzerkonto = aktivierungsdaten.getBenutzerkonto();
		return this.registrierungsmailVersendenAngularJS(benutzerkonto.getEmail(), aktivierungsdaten.getExpirationTime(), link,
			jahr);
	}

	/**
	 *
	 * @param aktivierungsdaten Aktivierungsdaten
	 * @param jahr String
	 * @param classpathMailTemplate String
	 * @return MKVMail
	 * @throws EgladilMailException
	 */
	public MKVMail privatregistrierungsmailVersenden(final Aktivierungsdaten aktivierungsdaten, final String jahr,
		final String classpathMailTemplate) throws EgladilMailException {
		final String link = configuration.getProperty(CONFIG_KEY_URL_REGISTRIERUNG_PRIVAT)
			+ aktivierungsdaten.getConfirmationCode();
		final Benutzerkonto benutzerkonto = aktivierungsdaten.getBenutzerkonto();
		return this.registrierungsmailVersenden(benutzerkonto.getEmail(), aktivierungsdaten.getExpirationTime(), link, jahr,
			classpathMailTemplate);
	}

	/**
	 * Versendet eine Mail zur erfolgreichen Wettbewerbsanmeldung an die genannte Mailadresse.
	 *
	 * @param mailadresse Strinf darf nicht null sein.
	 * @param jahr String das Wettbewerbsjahr
	 * @return MKVMail
	 */
	public MKVMail wettbewerbsanmeldungsmailVersenden(final String mailadresse, final String jahr, final Role role) {
		String infoFreischaltung = null;
		if (Role.MKV_LEHRER == role) {
			infoFreischaltung = KontextReader.getInstance().getKontext().getDatumFreigabeLehrer();
		} else {
			infoFreischaltung = KontextReader.getInstance().getKontext().getDatumFreigabePrivat();
		}
		final String subject = configuration.getProperty(CONFIG_KEY_SUBJECT_ANMELDUNG);
		final String text = mailtextGenerator.getTextWettbewerbsanmeldungMail(jahr, infoFreischaltung);
		return doSendMail(mailadresse, subject, text);
	}

	/**
	 * Bastelt die Mail zusammen und versendet sie.
	 *
	 * @param registrierungsbestaetigung
	 * @param benutzerkonto
	 * @param jahr
	 * @param configKeyURL
	 * @throws EgladilServerException
	 */
	@Deprecated
	private MKVMail registrierungsmailVersendenAngularJS(final String mailTo, final Date expires, final String link,
		final String jahr) throws EgladilMailException {
		final String expiration = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN).format(expires);
		final String mailSubject = configuration.getProperty(CONFIG_KEY_SUBJECT_REGISTRIERUNG);
		final String mailText = mailtextGenerator.getRegistrierungTextAngularJS(jahr, expiration, link);
		return doSendMail(mailTo, mailSubject, mailText);
	}

	/**
	 * Bastelt die Mail zusammen und versendet sie.
	 *
	 * @param registrierungsbestaetigung
	 * @param benutzerkonto
	 * @param jahr
	 * @param configKeyURL
	 * @param classpathMailtemplate String
	 * @throws EgladilServerException
	 */
	private MKVMail registrierungsmailVersenden(final String mailTo, final Date expires, final String link, final String jahr,
		final String classpathMailtemplate) throws EgladilMailException {
		final String expiration = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN).format(expires);
		final String mailSubject = configuration.getProperty(CONFIG_KEY_SUBJECT_REGISTRIERUNG);
		final String mailText = mailtextGenerator.getRegistrierungText(jahr, expiration, link, classpathMailtemplate);
		return doSendMail(mailTo, mailSubject, mailText);
	}

	/**
	 * @param payload
	 * @return
	 * @throws EgladilMailException
	 */
	public MKVMail schulkatalogmailVersenden(final AbstractSchuleKatalogantrag payload)
		throws EgladilMailException, InvalidMailAddressException {
		final String mailtext = payload instanceof SchuleAnschriftKatalogantrag
			? mailtextGenerator.getSchuleKatalogantragText((SchuleAnschriftKatalogantrag) payload)
			: mailtextGenerator.getSchuleKatalogantragText((SchuleUrlKatalogantrag) payload);
		final MKVMail mail = new MKVMail(payload.getEmail(), "Minikänguru: Schulkatalog", mailtext);
		mail.addHiddenEmpfaenger("minikaenguru@egladil.de");
		doSendMail(mail);
		return mail;
	}

	/**
	 * Versendet eine Mail mit dem Link zum zurücksetzen des Passworts.
	 *
	 * @param aktivierungsdaten Aktivierungsdaten
	 * @return ConfirmationMail
	 * @throws EgladilMailException
	 */
	public MKVMail resetPasswordMailVersenden(final Aktivierungsdaten aktivierungsdaten) throws EgladilMailException {
		final String link = configuration.getProperty(CONFIG_KEY_URL_RESET_PASSWORD);
		final Benutzerkonto benutzerkonto = aktivierungsdaten.getBenutzerkonto();
		final String expires = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN)
			.format(aktivierungsdaten.getExpirationTime());
		final String text = mailtextGenerator.getTextResetPassword(link, expires, aktivierungsdaten.getConfirmationCode());
		final String betreff = configuration.getProperty(CONFIG_KEY_SUBJECT_RESET_PASSWORD);

		final MKVMail mail = new MKVMail(benutzerkonto.getEmail(), betreff, text);
		doSendMail(mail);
		return mail;
	}

	/**
	 * Versendet eine Mail mit einem Fake-Link zum Zurücksetzen des Passworts. Alle eventuell auftretenden Exceptions
	 * werden gefangen und geloggt. <br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param aktivierungsdaten Aktivierungsdaten
	 * @return String die Gültigkeit des Links.
	 */
	public String resetPasswordDummyMailVersenden(final String email, final int expireMinutes) {
		final Date expireTime = TimeUtils.calculateExpireTime(new Date(), expireMinutes, ChronoUnit.MINUTES);
		final String link = configuration.getProperty(CONFIG_KEY_URL_RESET_PASSWORD);
		final String pwd = configuration.getProperty(MKVConstants.CONFIG_KEY_RESET_PASSWORD_DUMMYCODE);
		final String expires = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN).format(expireTime);
		final String text = mailtextGenerator.getTextResetPassword(link, expires, pwd);
		final String betreff = configuration.getProperty(CONFIG_KEY_SUBJECT_RESET_PASSWORD);

		final MKVMail mail = new MKVMail(email, betreff, text);
		try {
			doSendMail(mail);
		} catch (final InvalidMailAddressException e) {
			if (e.getAllInvalidAdresses().contains(email)) {
				LOG.warn("Mail mit Dummypasswort wurde nicht gesendet: {} ist keine gültige Mailadresse", email);
			}
		} catch (final Exception e) {
			LOG.warn("Mail mit Dummypasswort wurde nicht gesendet: " + e.getMessage(), e);
		}
		return expires;
	}

	/**
	 * Versendet eine Infomail über ein Upload. Es wird keine Exception generiert, falls das nicht klappt.
	 *
	 * @param email String die Mailadresse falls null, wird keine Mail versendet.
	 */
	public void uploadMailVersenden(final String email) {
		if (email == null) {
			LOG.debug("keine Email-Adresse vorhanden. Brechen ab...");
			return;
		}
		try {
			final String text = mailtextGenerator.getTextUploadMail();
			final String betreff = configuration.getProperty(CONFIG_KEY_SUBJECT_UPLOAD);
			final MKVMail mail = new MKVMail(email, betreff, text);
			doSendMail(mail);
		} catch (final Exception e) {
			LOG.warn("Die Upload-Mail wurde nicht versendet: {}", e.getMessage());
		}

	}

	/**
	 * Versendet eine Mail an mich selbst, wenn ein Upload fehlerhaft war.
	 */
	public void uploadfehlerMailVersenden() {
		try {
			final String text = "Fehlerhafte oder leere Datei hochgeladen.";
			final String betreff = "Fehlerhafte oder leere Datei hochgeladen";
			final MKVMail mail = new MKVMail("minikaenguru@egladil.de", betreff, text);
			doSendMail(mail);
		} catch (final Exception e) {
			LOG.warn("Die Uploadfehler-Mail an mich selbst wurde nicht versendet: {}", e.getMessage());
		}
	}

	void doSendMail(final MKVMail mail) {
		final String alleEmpfaenger = PrettyStringUtils.collectionToDefaultString(mail.alleEmpfaengerFuersLog());
		LOG.debug(alleEmpfaenger);
		if (sendMail) {
			mailService.sendMail(mail);
		} else {
			LOG.debug("Haben keinen Mailserver");
		}
	}

	/**
	 * Versendet eine Info-Mail über das Ändern des Passworts.<br>
	 * <br>
	 * Die Methode ich nicht void, damit man sie Mocken kann.
	 *
	 * @param benutzerkonto
	 * @throws EgladilMailException
	 */
	public String passwortGeaendertMailSenden(final Benutzerkonto benutzerkonto) throws EgladilMailException {
		final String jetzt = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN).format(new Date());
		final String text = mailtextGenerator.getMailPasswortGeaendertText(jetzt);
		final String subject = configuration.getProperty(CONFIG_KEY_SUBJECT_PASSWORT_NEU);
		LOG.debug("subject=" + subject);
		doSendMail(new MKVMail(benutzerkonto.getEmail(), subject, text));
		return "";
	}

	private MKVMail doSendMail(final String mailTo, final String mailSubject, final String mailText) {
		final MKVMail confirmationMail = new MKVMail(mailTo, mailSubject, mailText);
		doSendMail(confirmationMail);
		return confirmationMail;
	}

	/**
	 * Liefert die Membervariable mailService
	 *
	 * @return die Membervariable mailService
	 */
	protected IMailservice getMailService() {
		return mailService;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param sendMail neuer Wert der Membervariablen sendMail
	 */
	public void setSendMail(final boolean sendMail) {
		this.sendMail = sendMail;
	}

	/**
	 * Zu Testzwecken kann der Mailservice ausgetauscht werden.
	 *
	 * @param mailService
	 */
	public void setMailService(final IMailservice mailService) {
		this.mailService = mailService;
	}
}
