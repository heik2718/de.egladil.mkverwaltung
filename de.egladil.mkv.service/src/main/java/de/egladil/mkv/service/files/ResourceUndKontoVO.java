//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

import de.egladil.mkv.persistence.domain.IMKVKonto;

/**
 * ResourceUndKontoVO transportiert das Ergebnis der Authorisierung und das dabei gefundene IMKVKonto.
 */
public class ResourceUndKontoVO {

	private String respourcePath;

	private IMKVKonto konto;

	public String getRespourcePath() {
		return respourcePath;
	}

	public void setRespourcePath(String respourcePath) {
		this.respourcePath = respourcePath;
	}

	public IMKVKonto getKonto() {
		return konto;
	}

	public void setKonto(IMKVKonto konto) {
		this.konto = konto;
	}

}
