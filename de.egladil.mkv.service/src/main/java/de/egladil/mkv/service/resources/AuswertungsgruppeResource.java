//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.persistence.PersistenceException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.AuswertungsgruppePayload;
import de.egladil.mkv.persistence.payload.response.PublicAuswertungsgruppe;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.service.utils.TeilnahmeIdentifierDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * AuswertungsgruppeResource
 */
@Singleton
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes({ MediaType.APPLICATION_JSON + "; charset=utf-8" })
@Path("/teilnahmen")
public class AuswertungsgruppeResource {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungsgruppeResource.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final AuswertungsgruppenService auswertungsgruppenService;

	private final AuthorizationService authorizationService;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * AuswertungsgruppeResource
	 *
	 * @param auswertungsgruppenService AuswertungsgruppenService
	 */
	@Inject
	public AuswertungsgruppeResource(final AuswertungsgruppenService auswertungsgruppenService,
		final AuthorizationService authorizationService) {
		this.auswertungsgruppenService = auswertungsgruppenService;
		this.authorizationService = authorizationService;
	}

	/**
	 * Holt die Root-Auswertungsgruppe zum durch die URL definierten Teilnahmeidentifier.
	 *
	 * @param principal Principal
	 * @param jahr String das Jahr
	 * @param nameTeilnahmeart String P/S
	 * @param teilnahmekuerzel String kuerzel der Teilnahme
	 * @return Response
	 */
	@Timed
	@OPTIONS
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen")
	public Response optionsRootAuswertungsgruppe(@PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {

		return Response.ok().build();
	}

	/**
	 * Holt die Root-Auswertungsgruppe zum durch die URL definierten Teilnahmeidentifier.
	 *
	 * @param principal Principal
	 * @param jahr String das Jahr
	 * @param nameTeilnahmeart String P/S
	 * @param teilnahmekuerzel String kuerzel der Teilnahme
	 * @return Response
	 */
	@Timed
	@GET
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen")
	public Response getRootAuswertungsgruppe(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {

		final String benutzerUuid = principal.getName();
		TeilnahmeIdentifier teilnahmeIdentifier = null;
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			final Optional<Auswertungsgruppe> optGruppe = auswertungsgruppenService.getRootAuswertungsgruppe(teilnahmeIdentifier);
			PublicAuswertungsgruppe publicAuswertungsgruppe = null;
			if (optGruppe.isPresent()) {
				final Auswertungsgruppe g = optGruppe.get();
				publicAuswertungsgruppe = PublicAuswertungsgruppe.fromAuswertungsgruppe(g, true);
			} else {
				return Response.status(CustomResponseStatus.NOT_FOUND.getStatusCode()).build();
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), publicAuswertungsgruppe);
			return Response.ok().entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Holen der Auswertungsgruppen. {}, {}", teilnahmeIdentifier, null, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@Timed
	@POST
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen/{kuerzelRoot}")
	public Response auswertungsgruppeAnlegen(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel,
		@PathParam(value = "kuerzelRoot") final String kuerzelRoot, final AuswertungsgruppePayload payload) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		final String benutzerUuid = principal.getName();
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			validationDelegate.check(payload, AuswertungsgruppePayload.class);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			final Optional<Auswertungsgruppe> optGruppe = auswertungsgruppenService.getRootAuswertungsgruppe(teilnahmeIdentifier);
			PublicAuswertungsgruppe publicAuswertungsgruppe = null;
			if (optGruppe.isPresent()) {

				final Auswertungsgruppe rootGruppe = optGruppe.get();
				if (!rootGruppe.getKuerzel().equals(kuerzelRoot)) {
					LOG.warn("Anlegen Klasse zu Teilnahme {} nicht möglich: falsches Rootgruppe-Kuerzel {}", teilnahmeIdentifier,
						kuerzelRoot);
					return Response.status(CustomResponseStatus.BAD_REQUEST.getStatusCode()).build();
				}

				if (payload.getNameKlassenstufe() != null) {
					payload.setKlassenstufe(Klassenstufe.valueOf(payload.getNameKlassenstufe()));
				}

				final Auswertungsgruppe g = auswertungsgruppenService.createAuswertungsgruppe(benutzerUuid, rootGruppe, payload);
				publicAuswertungsgruppe = PublicAuswertungsgruppe.fromAuswertungsgruppe(g, true);
			} else {
				return Response.status(CustomResponseStatus.NOT_FOUND.getStatusCode()).build();
			}
			final APIResponsePayload entity = new APIResponsePayload(
				APIMessage.info(applicationMessages.getString("auswertungsgruppen.create.success")), publicAuswertungsgruppe);
			return Response.ok().entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final PersistenceException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Anlegen einer Auswertungsgruppe. {}, {}", teilnahmeIdentifier, null, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Ändert den Namen der Auswertungsgruppe und gibt die Root-Gruppe im Response zurück.
	 *
	 * @param principal
	 * @param jahr
	 * @param nameTeilnahmeart
	 * @param teilnahmekuerzel
	 * @param kuerzel
	 * @param payload
	 * @return Response
	 */
	@Timed
	@PUT
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen/{kuerzel}")
	public Response auswertungsgruppeAendern(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, @PathParam(value = "kuerzel") final String kuerzel,
		final AuswertungsgruppePayload payload) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		final String benutzerUuid = principal.getName();
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			validationDelegate.check(payload, AuswertungsgruppePayload.class);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			final Auswertungsgruppe auswertungsgruppe = auswertungsgruppenService.updateAuswertungsgruppe(benutzerUuid, kuerzel,
				payload);
			PublicAuswertungsgruppe publicAuswertungsgruppe = null;
			String msg = null;

			if (auswertungsgruppe != null) {
				if (auswertungsgruppe.getParent() == null) {
					publicAuswertungsgruppe = PublicAuswertungsgruppe.fromAuswertungsgruppe(auswertungsgruppe, true);
					msg = applicationMessages.getString("auswertungsgruppen.schule.update.success");
				} else {
					publicAuswertungsgruppe = PublicAuswertungsgruppe.fromAuswertungsgruppe(auswertungsgruppe.getParent(), true);
					msg = applicationMessages.getString("auswertungsgruppen.klasse.update.success");
				}
			} else {
				return Response.status(CustomResponseStatus.NOT_FOUND.getStatusCode()).build();
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(msg), publicAuswertungsgruppe);
			return Response.ok().entity(entity).build();
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Holen der Auswertungsgruppen. {}, {}", teilnahmeIdentifier, null, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Löscht die gegebene Auswertungsgruppe. Das Löschen schlägt fehl, wenn der Benutzer nicht für diese Teilnahme
	 * autorisiert ist, es sich um die Rootgruppe handelt oder die Auswertungsgruppe Teilnehmer hat.
	 *
	 * @param principal
	 * @param jahr
	 * @param nameTeilnahmeart
	 * @param teilnahmekuerzel
	 * @param kuerzel
	 * @return Response
	 */
	@Timed
	@DELETE
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/auswertungsgruppen/{kuerzel}")
	public Response auswertungsgruppeLoeschen(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel, @PathParam(value = "kuerzel") final String kuerzel) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		final String benutzerUuid = principal.getName();
		try {
			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			this.authorizationService.authorizeForTeilnahme(benutzerUuid, teilnahmeIdentifier);

			final Auswertungsgruppe rootgruppe = auswertungsgruppenService.checkAndDeleteAuswertungsgruppe(benutzerUuid, kuerzel,
				teilnahmeIdentifier);
			PublicAuswertungsgruppe publicAuswertungsgruppe = null;
			String msg = null;

			if (rootgruppe != null) {
				publicAuswertungsgruppe = PublicAuswertungsgruppe.fromAuswertungsgruppe(rootgruppe, true);
				// enhancePublicAuswertungsgruppe(rootgruppe, publicAuswertungsgruppe);
				msg = applicationMessages.getString("auswertungsgruppen.klasse.delete.success");
			} else {
				return Response.status(CustomResponseStatus.NOT_FOUND.getStatusCode()).build();
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(msg), publicAuswertungsgruppe);
			return Response.ok().entity(entity).build();
		} catch (final EgladilConcurrentModificationException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final ResourceNotFoundException e) {
			LOG.warn(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: teilnahmeart={}, teilnahmekuerzel={}, jahr={}",
					nameTeilnahmeart, teilnahmekuerzel, jahr);
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final MKVException e) {
			LOG.warn(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error("Exception beim Holen der Auswertungsgruppen. {}, {}", teilnahmeIdentifier, null, e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}
}
