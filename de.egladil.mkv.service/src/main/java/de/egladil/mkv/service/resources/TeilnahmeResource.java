//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.request.AuswertungsgruppeKuerzel;
import de.egladil.mkv.persistence.payload.response.DownloadArt;
import de.egladil.mkv.persistence.payload.response.DownloadCode;
import de.egladil.mkv.persistence.payload.response.teilnahmen.PublicTeilnahme;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.service.utils.TeilnahmeIdentifierDelegate;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * TeilnahmeResource
 */
@Singleton
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes({ MediaType.APPLICATION_JSON + "; charset=utf-8" })
@Path("/teilnahmen")
public class TeilnahmeResource {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnahmeResource.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final AuswertungsgruppenService auswertungsgruppenService;

	private final IBenutzerService benutzerService;

	private final SchulstatistikService schulstatistikService;

	private final AuthorizationService authorizationService;

	private final TeilnahmenFacade teilnahmenFacade;

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	/**
	 * TeilnahmeResource
	 */
	@Inject
	public TeilnahmeResource(final AuswertungsgruppenService auswertungsgruppenService,
		final SchulstatistikService schulstatistikService, final AuthorizationService authorizationService,
		final IBenutzerService benutzerService, final TeilnahmenFacade teilnahmenFacade) {
		this.auswertungsgruppenService = auswertungsgruppenService;
		this.schulstatistikService = schulstatistikService;
		this.authorizationService = authorizationService;
		this.benutzerService = benutzerService;
		this.teilnahmenFacade = teilnahmenFacade;
	}

	@GET
	public Response getTeilnahmen(@Auth final Principal principal) {

		final String benutzerUuid = principal.getName();

		try {
			final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(benutzerUuid);
			if (benutzerkonto == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "benutzerkonto mit UUID [" + benutzerUuid + "] nicht gefunden");
				throw new MKVException("benutzerkonto mit UUID [" + benutzerUuid + "] nicht gefunden");
			}
			final List<PublicTeilnahme> payload = teilnahmenFacade.getAllTeilnahmen(benutzerkonto);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"), payload);
			return Response.ok(entity).build();
		} catch (final MKVException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Fehler beim Laden der Teilnahmen zum Benutzer: {}, message: {}", benutzerUuid, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@GET // muss post werden Änderung auch in auswertung.service.ts erforderlich
	@Path("/rootgruppen/{kuerzel}/schulstatistik")
	@Timed
	public Response getStatistikAktuellesWettbewerbsjahr(@Auth final Principal principal,
		@PathParam("kuerzel") final String kuerzel) {

		final String benutzerUuid = principal.getName();

		try {
			validationDelegate.check(new AuswertungsgruppeKuerzel(kuerzel), AuswertungsgruppeKuerzel.class);

			final Optional<Auswertungsgruppe> optGruppe = auswertungsgruppenService.findAuswertungsgruppe(kuerzel);
			if (!optGruppe.isPresent()) {
				LOG.warn("Auswertungsgruppe " + kuerzel + " existiert nicht oder nicht mehr.");
				throw new ResourceNotFoundException(applicationMessages.getString("statistik.schuluebersicht.notFound"));
			}

			final Auswertungsgruppe auswertungsgruppe = optGruppe.get();
			if (!auswertungsgruppe.isRoot()) {
				final APIMessage msg = APIMessage.error(applicationMessages.getString("statistik.schuluebersicht.klasse"));
				final APIResponsePayload entity = new APIResponsePayload(msg, null);
				return Response.status(Status.PRECONDITION_FAILED).entity(entity).build();
			}
			authorizationService.authorizeForTeilnahme(benutzerUuid, auswertungsgruppe.provideTeilnahmeIdentifier());

			final Optional<String> optDownloadCode = schulstatistikService.generiereStatistikAktuellesJahr(benutzerUuid, kuerzel);

			if (!optDownloadCode.isPresent()) {
				throw new ResourceNotFoundException(applicationMessages.getString("statistik.schuluebersicht.notFound"));
			}

			final String downloadCode = optDownloadCode.get();
			final DownloadCode payload = new DownloadCode(downloadCode, auswertungsgruppe.provideTeilnahmeIdentifier(),
				DownloadArt.STATISTIK_SCHULE);
			final APIMessage msg = APIMessage.info(applicationMessages.getString("statistik.schuluebersicht.created"));
			final APIResponsePayload entity = new APIResponsePayload(msg, payload);

			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilWebappException e) {
			LOG.error("ValidationException beim Erzeugen Schulübersicht zur Rootauswertungsgruppe: {}, message: {}", kuerzel,
				e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final ResourceNotFoundException e) {
			LOG.error("Resource not found beim Erzeugen Schulübersicht Rootauswertungsgruppe: {}", kuerzel);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Fehler beim Erzeugen Schulübersicht zur Rootauswertungsgruppe: {}, message: {}", kuerzel, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@OPTIONS
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/schulstatistik")
	@Timed
	public Response options(@PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {

		return Response.ok().build();
	}

	@GET // muss post werden Änderung auch in auswertung.service.ts erforderlich
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/schulstatistik")
	@Timed
	public Response getTeilnahmestatistik(@Auth final Principal principal, @PathParam(value = "jahr") final String jahr,
		@PathParam(value = "teilnahmeart") final String nameTeilnahmeart,
		@PathParam(value = "teilnahmekuerzel") final String teilnahmekuerzel) {

		TeilnahmeIdentifier teilnahmeIdentifier = null;
		final String benutzerUuid = principal.getName();

		try {

			teilnahmeIdentifier = new TeilnahmeIdentifierDelegate().createAndValidateTeilnahmeIdentifier(nameTeilnahmeart,
				teilnahmekuerzel, jahr);

			final Optional<Schule> optSchule = auswertungsgruppenService.findSchule(teilnahmekuerzel);
			if (!optSchule.isPresent()) {
				LOG.warn("Schule " + teilnahmekuerzel + " existiert nicht oder nicht mehr.");
				throw new ResourceNotFoundException(applicationMessages.getString("statistik.schuluebersicht.notFound"));
			}

			authorizationService.authorizeLehrerForSchuleOderTeilnahme(benutzerUuid, teilnahmeIdentifier);

			final Schule schule = optSchule.get();

			final Optional<String> optDownloadCode = schulstatistikService.generiereSchulstatistik(benutzerUuid,
				teilnahmeIdentifier, schule.getName());
			if (!optDownloadCode.isPresent()) {
				throw new ResourceNotFoundException(applicationMessages.getString("statistik.schuluebersicht.notFound"));
			}

			final String downloadCode = optDownloadCode.get();
			final DownloadCode payload = new DownloadCode(downloadCode, teilnahmeIdentifier, DownloadArt.STATISTIK_LOESUNGSZETTEL);
			final APIMessage msg = APIMessage.info(applicationMessages.getString("statistik.schuluebersicht.created"));
			final APIResponsePayload entity = new APIResponsePayload(msg, payload);

			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilWebappException e) {
			if (teilnahmeIdentifier == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Fehler in url: nameTeilnehmerart={}, teilnahmekuerzel={}, jahr={}");
			} else {
				LOG.warn("Validierungsfehler TeilnahmeIdentifier {}: {}", teilnahmeIdentifier, e.getMessage());
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final ResourceNotFoundException e) {
			LOG.error("Resource not found beim Erzeugen Schulstatistik: jahr {}, {}", jahr, teilnahmekuerzel);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthorizationException | EgladilAuthenticationException | DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			if (teilnahmeIdentifier == null) {
				LOG.error("Fehler beim Erzeugen der Schulstatistik: {}", e.getMessage(), e);
			} else {
				LOG.error("Fehler beim Erzeugen der Schulstatistik TeilnahmeIdentifier: {}, message: {}", teilnahmeIdentifier,
					e.getMessage(), e);
			}
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}
}
