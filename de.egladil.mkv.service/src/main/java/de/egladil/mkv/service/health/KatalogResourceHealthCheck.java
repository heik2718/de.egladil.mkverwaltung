//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.health;

import javax.ws.rs.core.Response;

import com.codahale.metrics.health.HealthCheck;

import de.egladil.mkv.service.resources.KatalogResource;

/**
 * KatalogHealthCheck
 */
public class KatalogResourceHealthCheck extends HealthCheck {

	private final KatalogResource katalogResource;

	/**
	 * Erzeugt eine Instanz von KatalogHealthCheck
	 */
	public KatalogResourceHealthCheck(final KatalogResource katalogResource) {
		this.katalogResource = katalogResource;
	}

	/**
	 * @see com.codahale.metrics.health.HealthCheck#check()
	 */
	@Override
	protected Result check() throws Exception {
		final Response response = katalogResource.getLaender();
		return response.getStatus() == 200 ? Result.healthy() : Result.unhealthy("Datenbank nicht erreichbar");
	}

}
