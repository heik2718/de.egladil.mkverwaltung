//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.inject.Singleton;

import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.exception.CustomResponseStatus;

/**
 * ErrorResource
 */
@Singleton
@Path("/errorpages")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class ErrorResource {

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	@GET
	public Response getDefault() {
		final APIResponsePayload pem = new APIResponsePayload(
			APIMessage.error(applicationMessages.getString("ErrorResource.default")));
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(pem).build();
	}

	@GET
	@Path("401")
	public Response get401() {
		final APIResponsePayload pem = new APIResponsePayload(
			APIMessage.error(applicationMessages.getString("ErrorResource.notAuthorized")));
		return Response.status(Response.Status.UNAUTHORIZED).entity(pem).build();
	}

	@GET
	@Path("404")
	public Response get404() {
		final APIResponsePayload pem = new APIResponsePayload(
			APIMessage.error(applicationMessages.getString("ErrorResource.notFound")));
		return Response.status(Response.Status.NOT_FOUND).entity(pem).build();
	}

	@GET
	@Path("500")
	public Response get500() {
		return getDefault();
	}

	@GET
	@Path("900")
	public Response get900() {
		final APIResponsePayload pem = new APIResponsePayload(
			APIMessage.error(applicationMessages.getString("ErrorResource.duplicateEntry")));
		return Response.status(CustomResponseStatus.DUPLICATE_ENTRY.getStatusCode()).entity(pem).build();
	}
}
