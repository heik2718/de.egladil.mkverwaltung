//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.resources;

import java.io.File;
import java.net.URI;
import java.security.Principal;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.jetty.http.HttpMethod;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ByteArrayStreamingOutput;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.payload.request.AdvVereinbarungAntrag;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.AuthorizationService;
import de.egladil.mkv.service.pdf.AdvVereinbarungPdfGenerator;
import de.egladil.mkv.service.utils.UniqueIndexNameMapper;
import de.egladil.mkv.service.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * AdvResource
 */
@Singleton
@Path("adv")
public class AdvResource {

	private static final Logger LOG = LoggerFactory.getLogger(AdvResource.class);

	private String hateoasApiUrl;

	private String downloadPath;

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	private ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	private final AdvFacade advFacade;

	private final AuthorizationService authorizationService;

	/**
	 * AdvResource
	 */
	@Inject
	public AdvResource(final AdvFacade advFacade, final AuthorizationService authorizationService) {
		this.advFacade = advFacade;
		this.authorizationService = authorizationService;
	}

	@POST
	@Path("vereinbarungen")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response createAdvVereinbarung(@Auth final Principal principal, final AdvVereinbarungAntrag antrag) {

		if (KontextReader.getInstance().getKontext().isDumpPayload()) {
			LOG.info("Payload-Dump: {}", antrag);
		}

		final String benutzerUuid = principal.getName();

		try {
			validationDelegate.check(antrag, AdvVereinbarungAntrag.class);
			final String schulkuerzel = antrag.getSchulkuerzel();
			authorizationService.authorizeLehrerForSchule(benutzerUuid, schulkuerzel);

			final AdvVereinbarung vereinbahrung = advFacade.createAdvVereinbarung(benutzerUuid, antrag);
			LOG.info("AdvVereinbarung für Schule {} angelegt: id={}", schulkuerzel, vereinbahrung.getId());

			final String subpath = "/adv/" + schulkuerzel;
			final URI location = new URI(hateoasApiUrl + subpath);

			final APIResponsePayload entity = new APIResponsePayload(
				APIMessage.info(applicationMessages.getString("adv.create.success")),
				new HateoasLink(subpath, "self", HttpMethod.GET.toString(), MediaType.APPLICATION_OCTET_STREAM));
			return Response.created(location).entity(entity).build();

		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Exception beim Anlegen der AdvVereinbarung {}: {}", antrag, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, antrag);
		}
	}

	@GET
	@Path("vereinbarungen/{schulkuerzel}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Timed
	public Response getAdvVereinbarung(@Auth final Principal principal, @PathParam("schulkuerzel") final String schulkuerzel) {

		final String benutzerUuid = principal.getName();

		try {
			authorizationService.authorizeLehrerForSchule(benutzerUuid, schulkuerzel);

			validationDelegate.check(new Payload(schulkuerzel), Payload.class);

			final Optional<AdvVereinbarung> optVereinbarung = advFacade.findAdvVereinbarungFuerSchule(schulkuerzel);

			if (!optVereinbarung.isPresent()) {
				return Response.status(Status.NOT_FOUND).build();
			}

			final byte[] pdf = new AdvVereinbarungPdfGenerator(optVereinbarung.get(), downloadPath).generatePdf();

			final ByteArrayStreamingOutput result = new ByteArrayStreamingOutput(pdf);
			final ContentDisposition contentDisposition = ContentDisposition.type("attachment")
				.fileName("vereinbarung_auftragsdatenverarbeitung.pdf").build();
			final Response response = Response.ok(result).header("Content-Type", "application/octet-stream")
				.header("Content-Disposition", contentDisposition).build();

			return response;

		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException | EgladilAuthorizationException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Exception beim Drucken einer AdvVereinbarung schulkuerzel {}: {}", schulkuerzel, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	public final void setHateoasApiUrl(final String hateoasApiUrl) {
		this.hateoasApiUrl = hateoasApiUrl + "/adv";
	}

	private class Payload implements ILoggable {

		/* serialVersionUID */
		private static final long serialVersionUID = 1L;

		@Kuerzel
		@NotBlank
		private final String value;

		Payload(final String value) {
			this.value = value;
		}

		@Override
		public String toBotLog() {
			return value;
		}
	}

	public final void setDownloadPath(final String downloadPath) {
		this.downloadPath = downloadPath + File.separator + "pdf";
	}

}
