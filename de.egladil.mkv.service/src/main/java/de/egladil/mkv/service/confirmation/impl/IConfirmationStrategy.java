//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;

/**
 * IConfirmationStrategy
 */
public interface IConfirmationStrategy {

	String CONTEXT_MESSAGE = "Aktivierung Benutzerkonto";

	/**
	 *
	 * @param aktivierungsdaten
	 * @param jahr
	 * @param kontakt
	 * @param callback
	 * @param anonymsierungsservice
	 * @return
	 */
	ConfirmationStatus applyOnRegistration(Aktivierungsdaten aktivierungsdaten, String jahr, Optional<IMKVKonto> kontakt,
		IKontaktUndTeilnahmeCallback callback, IAnonymisierungsservice anonymsierungsservice)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException;
}
