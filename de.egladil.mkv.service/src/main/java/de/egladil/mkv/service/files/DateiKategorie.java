//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.files;

/**
 * DateiKategorie
 */
public enum DateiKategorie {

	aufgaben(1),
	hilfsmittel(2),
	marker(0);

	private final int sortnummer;

	/**
	 * DateiKategorie
	 */
	private DateiKategorie(final int sortnummer) {
		this.sortnummer = sortnummer;
	}

	/**
	 * Ermittelt anhand von Dateinamenskonventionen die Kateorie.
	 *
	 * @param dateiname String
	 * @return DateiKategorie.
	 */
	public static DateiKategorie valueOfDateiname(final String dateiname) {
		if (dateiname == null) {
			throw new NullPointerException("dateiname");
		}
		if (dateiname.toLowerCase().contains("auswertung")) {
			return DateiKategorie.hilfsmittel;
		}
		if (dateiname.toLowerCase().contains(".zip")) {
			return DateiKategorie.aufgaben;
		}
		if (dateiname.toLowerCase().contains("aufgaben") || dateiname.toLowerCase().contains("loesungen") || dateiname.toLowerCase().contains("exercises") || dateiname.toLowerCase().contains("solutions")) {
			return DateiKategorie.aufgaben;
		}
		if (dateiname.toLowerCase().contains(".txt") || dateiname.toLowerCase().contains(".xml")) {
			return DateiKategorie.marker;
		}
		return DateiKategorie.hilfsmittel;
	}

	public final int getSortnummer() {
		return sortnummer;
	}
}
