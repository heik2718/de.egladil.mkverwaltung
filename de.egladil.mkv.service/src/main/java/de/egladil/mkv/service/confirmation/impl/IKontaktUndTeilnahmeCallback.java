//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.List;

import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * IKontaktUndTeilnahmeCallback
 */
public interface IKontaktUndTeilnahmeCallback {

	void persist(List<TeilnahmeIdentifierProvider> teilnahmen)
		throws EgladilConcurrentModificationException, EgladilDuplicateEntryException, MKVException;

	IMKVKonto persist(IMKVKonto kontakt)
		throws EgladilConcurrentModificationException, EgladilDuplicateEntryException, MKVException;
}
