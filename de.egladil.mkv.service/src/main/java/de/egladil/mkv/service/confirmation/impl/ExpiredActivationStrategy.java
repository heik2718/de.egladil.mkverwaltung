//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation.impl;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.domain.Aktivierungsdaten;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;

/**
 * ExpiredActivationStrategy Benutzer und MKV-Konto werden anonymsiert.
 */
public class ExpiredActivationStrategy implements IConfirmationStrategy {

	/**
	 * Erzeugt eine Instanz von NormalActivationStrategy
	 */
	public ExpiredActivationStrategy(IRegistrierungService registrierungService, IBenutzerService benutzerService) {
	}

	/**
	 * @see de.egladil.mkv.service.confirmation.impl.IConfirmationStrategy#applyOnRegistration(de.egladil.bv.aas.domain.Aktivierungsdaten,
	 * String, Optional, de.egladil.mkv.service.confirmation.impl.IKontaktUndTeilnahmeCallback)
	 */
	@Override
	public ConfirmationStatus applyOnRegistration(Aktivierungsdaten aktivierungsdaten, String jahr, Optional<IMKVKonto> kontakt,
		IKontaktUndTeilnahmeCallback callback, IAnonymisierungsservice anonymisierungsservice)
		throws IllegalArgumentException, ConstraintViolationException, EgladilDuplicateEntryException,
		EgladilConcurrentModificationException, EgladilStorageException, MKVException {

		Benutzerkonto benutzer = aktivierungsdaten.getBenutzerkonto();
		anonymisierungsservice.kontoAnonymsieren(benutzer.getUuid(), kontakt, CONTEXT_MESSAGE);
		return ConfirmationStatus.expiredActivation;
	}
}
