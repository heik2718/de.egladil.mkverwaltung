//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.TeilnahmeIdentifierProvider;
import de.egladil.mkv.persistence.domain.teilnahmen.Privatteilnahme;
import de.egladil.mkv.persistence.domain.user.Privatkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;
import de.egladil.mkv.service.payload.request.Wettbewerbsanmeldung;

/**
 * GeneratePrivatteilnahmeStrategy
 */
public class GeneratePrivatteilnahmeStrategy implements IGenerateTeilnahmeStrategy {

	private static final Logger LOG = LoggerFactory.getLogger(GeneratePrivatteilnahmeStrategy.class);

	private final IPrivatkontoDao privatkontoDao;

	/**
	 * Erzeugt eine Instanz von GeneratePrivatteilnahmeStrategy
	 */
	public GeneratePrivatteilnahmeStrategy(final IPrivatkontoDao privatkontoDao) {
		if (privatkontoDao == null) {
			throw new IllegalArgumentException("privatkontoDao null");
		}
		this.privatkontoDao = privatkontoDao;
	}

	@Override
	public TeilnahmeIdentifierProvider generateTeilnahme(final Wettbewerbsanmeldung anmeldung, final String benutzerUUID) {

		final Optional<Privatkonto> opt = privatkontoDao.findByUUID(benutzerUUID);
		if (!opt.isPresent()) {
			throw new MKVException("Privatkonto mit [UUID=" + benutzerUUID + "] nicht gefunden");
		}
		final Privatkonto privatkonto = opt.get();
		final TeilnahmeIdentifierProvider teilnahme = privatkonto.getTeilnahmeZuJahr(anmeldung.getJahr());
		if (teilnahme != null) {
			LOG.debug("Privatmensch bereits angemeldet: {}", privatkonto);
			return null;
		}
		final Privatteilnahme neueTeilnahme = new Privatteilnahme(anmeldung.getJahr(),
			new KuerzelGenerator().generateDefaultKuerzel());
		privatkonto.addTeilnahme(neueTeilnahme);
		privatkonto.setAutomatischBenachrichtigen(anmeldung.isBenachrichtigen());
		final IMKVKonto result = privatkontoDao.persist(privatkonto);
		LOG.debug("Konto {} zum Wettbewerb angemeldet: ", result);
		return neueTeilnahme;
	}
}
