//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.anmeldung.impl;

import java.util.Optional;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeDao;
import de.egladil.mkv.persistence.domain.teilnahmen.Schulteilnahme;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * FindOrCreateSchulteilnahmeCommand sucht oder erzeugt und persistiert eine neue Schulteilnahme.
 */
public class FindOrCreateSchulteilnahmeCommand {

	private final ISchulteilnahmeDao schulteilnahmeDao;

	/**
	 * Erzeugt eine Instanz von FindOrCreateSchulteilnahmeCommand
	 */
	public FindOrCreateSchulteilnahmeCommand(final ISchulteilnahmeDao schulteilnahmeDao) throws IllegalArgumentException {
		if (schulteilnahmeDao == null) {
			throw new IllegalArgumentException("schulteilnahmeDao null");
		}
		this.schulteilnahmeDao = schulteilnahmeDao;
	}

	/**
	 * Sucht die Schulteilnahme mit dem gegebenen Schulkürzel und dem gegebenen Jahr oder legt eine neue peristente An.
	 *
	 * @param schulkuerzel String darf nicht null sein.
	 * @param jahr String darf nicht null sein.
	 * @return Schulteilnahme nicht null
	 * @throws IllegalArgumentException falls ein Parameter null
	 * @throws EgladilStorageException bei Exception in der DB-Layer.
	 */
	public Schulteilnahme findOrCreateSchulteilnahme(final String schulkuerzel, final String jahr) throws EgladilStorageException {
		if (schulkuerzel == null || jahr == null) {
			throw new IllegalArgumentException("schulkuerzel oder jahr null: " + schulkuerzel + ", " + jahr);
		}
		final Optional<Schulteilnahme> optTeilnahme = schulteilnahmeDao
			.findByTeilnahmeIdentifier(TeilnahmeIdentifier.createSchulteilnahmeIdentifier(schulkuerzel, jahr));
		Schulteilnahme schulteilnahme = null;
		if (!optTeilnahme.isPresent()) {
			schulteilnahme = createAndPersistSchulteilnahme(schulkuerzel, jahr);
		} else {
			schulteilnahme = optTeilnahme.get();
		}
		return schulteilnahme;
	}

	private Schulteilnahme createAndPersistSchulteilnahme(final String schulkuerzel, final String jahr)
		throws EgladilStorageException {
		final Schulteilnahme teilnahme = new Schulteilnahme();
		teilnahme.setJahr(jahr);
		teilnahme.setKuerzel(schulkuerzel);
		try {
			final Schulteilnahme newEntity = schulteilnahmeDao.persist(teilnahme);
			return newEntity;
		} catch (final Exception e) {
			throw new EgladilStorageException("Fehler beim Speichern einer neuen Schulteilnahme: " + e.getMessage(), e);
		}
	}
}
