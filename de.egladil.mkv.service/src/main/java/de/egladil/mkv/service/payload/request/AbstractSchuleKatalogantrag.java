//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.payload.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.mkv.persistence.domain.kataloge.Land;

/**
 * AbstractSchuleKatalogantrag. Unterscheidung nach type: [url|anschrift]
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = SchuleUrlKatalogantrag.class, name = "url"),
	@Type(value = SchuleAnschriftKatalogantrag.class, name = "anschrift") })
public abstract class AbstractSchuleKatalogantrag implements ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "{org.hibernate.validator.constraints.Email.message}")
	private String email;

	@NotBlank
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@JsonIgnore
	private Land land;

	@DeutscherName
	@Size(min = 1, max = 100)
	private String nameLand;

	@Honeypot
	private String kleber;

	@Override
	public String toBotLog() {
		return " [email=" + email + ", name=" + name + ", kleber=" + kleber;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("AbstractSchuleKatalogantrag [email=");
		builder.append(email);
		builder.append(", name=");
		builder.append(name);
		builder.append(", land=");
		builder.append(land);
		builder.append(", nameLand=");
		builder.append(nameLand);
		builder.append(", kleber=");
		builder.append(kleber);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the land
	 */
	public Land getLand() {
		return land;
	}

	/**
	 * @param land the land to set
	 */
	public void setLand(final Land land) {
		this.land = land;
	}

	/**
	 * @return the kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * @param kleber the kleber to set
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

	/**
	 * Benötigt für BeansValidation.
	 *
	 * @return
	 */
	public abstract <T extends ILoggable> Class<T> ownClass();

	public final String getNameLand() {
		if (this.nameLand != null) {
			return this.nameLand;
		}
		if (this.land != null) {
			return this.land.getName();
		}
		return null;
	}

	public final void setNameLand(final String landName) {
		this.nameLand = landName;
	}
}
