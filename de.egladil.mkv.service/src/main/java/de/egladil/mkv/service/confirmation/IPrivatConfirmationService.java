//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.confirmation;

import de.egladil.mkv.service.confirmation.impl.IConfirmationService;

/**
 * IPrivatConfirmationService Markerinterface für guice.
 */
public interface IPrivatConfirmationService extends IConfirmationService {
}
