//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.service.mail;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.service.payload.request.SchuleAnschriftKatalogantrag;
import de.egladil.mkv.service.payload.request.SchuleUrlKatalogantrag;

/**
 * MailtextGenerator
 */
public class MailtextGenerator {

	private static final String ENCODING = "UTF-8";

	private static final String PATH_MAILTEMPLATE_REGISTRIERUNG = "/mailtemplates/registrierung.txt";

	private static final String PATH_MAILTEMPLATE_SCHULE_ANSCHRIFT = "/mailtemplates/katalogeintragSchuleAnschrift.txt";

	private static final String PATH_MAILTEMPLATE_SCHULE_URL = "/mailtemplates/katalogeintragSchuleUrl.txt";

	private static final String PATH_MAILTEMPLATE_PASSWORD_GEAENDERT = "/mailtemplates/passwortGeaendert.txt";

	private static final String PATH_MAILTEMPLATE_RESET_PASSWORD = "/mailtemplates/passwortZureucksetzen.txt";

	private static final String PATH_MAILTEMPLATE_UPLOAD = "/mailtemplates/upload.txt";

	private static final String PATH_MAILTEMPLATE_WETTBEWEBSANMELDUNG = "/mailtemplates/anmeldung.txt";

	/**
	 * Liest das Mailtemplate ein und ersetzt die Platzhalter.
	 *
	 * @param jahr
	 * @param frist
	 * @param link
	 * @return String
	 */
	@Deprecated
	public String getRegistrierungTextAngularJS(final String jahr, final String frist, final String link) {
		InputStream in = null;
		StringWriter sw = null;
		try {
			in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_REGISTRIERUNG);
			sw = new StringWriter();

			String text = copyStream(in, sw);
			text = text.replaceAll("#0#", jahr);
			text = text.replaceAll("#1#", frist);
			text = text.replaceAll("#2#", link);

			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_REGISTRIERUNG + " nicht laden: " + e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(sw);
		}
	}

	/**
	 * Liest das Mailtemplate ein und ersetzt die Platzhalter.
	 *
	 * @param jahr String
	 * @param frist String
	 * @param link String
	 * @param classpathResource String
	 * @return String
	 */
	public String getRegistrierungText(final String jahr, final String frist, final String link, final String classpathResource) {
		InputStream in = null;
		StringWriter sw = null;
		try {
			in = getClass().getResourceAsStream(classpathResource);
			sw = new StringWriter();

			String text = copyStream(in, sw);
			text = text.replaceAll("#0#", jahr);
			text = text.replaceAll("#1#", frist);
			text = text.replaceAll("#2#", link);

			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_REGISTRIERUNG + " nicht laden: " + e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(sw);
		}
	}

	/**
	 * Liest das Mailtemplate und ersetzt die Platzhalter.
	 *
	 * @param payload
	 * @return Strin
	 */
	public String getSchuleKatalogantragText(final SchuleAnschriftKatalogantrag payload) {
		if (payload.getNameLand() == null) {
			throw new MKVException("Fehler im Ablauf: payload.land oder payload.nameLand ist nicht initialisiert!");
		}
		InputStream in = null;
		StringWriter sw = null;
		try {
			in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_SCHULE_ANSCHRIFT);
			sw = new StringWriter();
			String text = copyStream(in, sw);
			text = text.replaceAll("#0#", payload.getNameLand());
			text = text.replaceAll("#1#", payload.getName());
			text = text.replaceAll("#2#", payload.getPlz());
			text = text.replaceAll("#3#", payload.getOrt());
			text = text.replaceAll("#4#", StringUtils.isBlank(payload.getStrasse()) ? "-" : payload.getStrasse());
			text = text.replaceAll("#5#", StringUtils.isBlank(payload.getHausnummer()) ? "-" : payload.getHausnummer());

			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_REGISTRIERUNG + " nicht laden: " + e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(sw);
		}
	}

	public String getSchuleKatalogantragText(final SchuleUrlKatalogantrag payload) {
		if (payload.getLand() == null) {
			throw new MKVException("Fehler im Ablauf: payload.land ist nicht initialisiert!");
		}
		InputStream in = null;
		StringWriter sw = null;
		try {
			in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_SCHULE_URL);
			sw = new StringWriter();
			String text = copyStream(in, sw);
			text = text.replaceAll("#0#", payload.getNameLand());
			text = text.replaceAll("#1#", payload.getName());
			text = text.replaceAll("#2#", payload.getUrl());
			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_REGISTRIERUNG + " nicht laden: " + e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(sw);
		}
	}

	/**
	 *
	 * @return
	 */
	public String getMailPasswortGeaendertText(final String datumUhrzeit) {
		InputStream in = null;
		final StringWriter sw = new StringWriter();
		try {
			in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_PASSWORD_GEAENDERT);
			String text = copyStream(in, sw);
			text = text.replaceAll("#0#", datumUhrzeit);
			return text;
		} catch (final IOException e) {
			throw new MKVException(
				"Konnte Mailtemplate " + PATH_MAILTEMPLATE_PASSWORD_GEAENDERT + " nicht laden: " + e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(sw);
		}
	}

	/**
	 *
	 * @param link
	 * @param expireTime
	 * @return
	 */
	public String getTextResetPassword(final String link, final String expireTime, final String pwd) {
		try (InputStream in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_RESET_PASSWORD);
			StringWriter sw = new StringWriter()) {
			String text = copyStream(in, sw);
			text = text.replaceAll("#0#", expireTime).replaceAll("#1#", link).replaceAll("#2#", pwd);
			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_RESET_PASSWORD + " nicht laden: " + e.getMessage(),
				e);
		}
	}

	public String getTextUploadMail() throws MKVException {
		try (InputStream in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_UPLOAD); StringWriter sw = new StringWriter()) {
			final String text = copyStream(in, sw);
			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_UPLOAD + " nicht laden: " + e.getMessage(), e);
		}

	}

	public String getTextWettbewerbsanmeldungMail(final String jahr, final String info) {
		try (InputStream in = getClass().getResourceAsStream(PATH_MAILTEMPLATE_WETTBEWEBSANMELDUNG);
			StringWriter sw = new StringWriter()) {
			String text = copyStream(in, sw);
			text = text.replaceAll("#1#", jahr);
			text = text.replaceAll("#2#", info);
			return text;
		} catch (final IOException e) {
			throw new MKVException("Konnte Mailtemplate " + PATH_MAILTEMPLATE_UPLOAD + " nicht laden: " + e.getMessage(), e);
		}
	}

	/**
	 * @param in
	 * @param sw
	 * @return
	 * @throws IOException
	 */
	private String copyStream(final InputStream in, final StringWriter sw) throws IOException {
		IOUtils.copy(in, sw, ENCODING);
		final String text = sw.toString();
		return text;
	}
}
