Guten Tag,

Ihre Anfrage, die unten genannte Schule in den Minikänguru-Schulkatalog aufzunehmen, wurde weitergeleitet und wird
zeitnah bearbeitet. Sobald die Schule eingetragen wurde, erhalten Sie eine weitere Email.

Schuldaten:
---------------
Land/Bundesland:   #0#
Name der Schule:   #1#
   Postleitzahl:   #2#
            Ort:   #3#
         Straße:   #4#
     Hausnummer:   #5#

Bitte antworten Sie nicht auf diese Mail. Für Fragen, Anregungen, Lob und Kritik verwenden Sie bitte die Mailadresse minikaenguru@egladil.de.


Noch eine Bitte zum Umgang mit Mails von noreply@egladil.de oder minikaenguru@egladil.de: Bitte markieren Sie Mails von diesen Mail-Adressen nicht als Spam. Das gefährdet die Durchführung des Wettbewerbs für interessierte Schulen, denn es gibt einige Provider, die meine Mails blockieren, wenn sie von mehreren Empfängern als Spam markiert werden. Meine Mails erreichen dann nicht mehr alle interessierten Personen.

Mit freundlichen Grüßen,
Heike Winkelvoß.