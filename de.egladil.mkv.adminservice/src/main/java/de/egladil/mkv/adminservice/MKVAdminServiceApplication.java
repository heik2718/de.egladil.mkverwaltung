//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice;

import java.security.Principal;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;

import de.egladil.bv.aas.auth.CsrfFilter;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.common.persistence.EgladilPersistFilter;
import de.egladil.common.webapp.AngularJsonVulnerabilityProtectionInterceptor;
import de.egladil.common.webapp.exception.EgladilLoggingExceptionMapper;
import de.egladil.common.webapp.exception.JsonProcessingExceptionMapper;
import de.egladil.common.webapp.exception.LoggingConstraintViolationExceptionMapper;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.config.MKVAdminModule;
import de.egladil.mkv.adminservice.config.MKVAdminServiceConfiguration;
import de.egladil.mkv.adminservice.filters.CSPFilter;
import de.egladil.mkv.adminservice.filters.SecureHeadersFilter;
import de.egladil.mkv.adminservice.health.BVHealthCheck;
import de.egladil.mkv.adminservice.health.LaenderResourceHealthCheck;
import de.egladil.mkv.adminservice.health.PingHealthCheck;
import de.egladil.mkv.adminservice.resources.AuswertungResource;
import de.egladil.mkv.adminservice.resources.AuswertungsgruppenResource;
import de.egladil.mkv.adminservice.resources.AuthenticationResource;
import de.egladil.mkv.adminservice.resources.BenutzerResource;
import de.egladil.mkv.adminservice.resources.ExportBenutzerResource;
import de.egladil.mkv.adminservice.resources.InitDownloadcodesResource;
import de.egladil.mkv.adminservice.resources.LaenderResource;
import de.egladil.mkv.adminservice.resources.LogResource;
import de.egladil.mkv.adminservice.resources.MailqueueResource;
import de.egladil.mkv.adminservice.resources.OrteResource;
import de.egladil.mkv.adminservice.resources.PingResource;
import de.egladil.mkv.adminservice.resources.SchulenResource;
import de.egladil.mkv.adminservice.resources.UploadsResource;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper;
import io.dropwizard.setup.Environment;

/**
 * MKVAdminServiceApplication
 */
public class MKVAdminServiceApplication extends Application<MKVAdminServiceConfiguration> {

	private static final Logger LOG = LoggerFactory.getLogger(MKVAdminServiceApplication.class);

	private static final String SERVICE_NAME = "MKV-ADMIN-API";

	private static final String BEARER_PREFIX = "Bearer";

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}
	}

	public static void main(final String[] args) {
		try {
			final MKVAdminServiceApplication application = new MKVAdminServiceApplication();
			application.run(args);
		} catch (final Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * @see io.dropwizard.Application#getName()
	 */
	@Override
	public String getName() {
		return SERVICE_NAME;
	}

	/**
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration,
	 *      io.dropwizard.setup.Environment)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(final MKVAdminServiceConfiguration configuration, final Environment environment) throws Exception {
		LOG.info("configRoot: {}", configuration.getConfigRoot());
		configureCors(configuration, environment);
		configureErrorPages(environment);
		configureExceptionMappers(environment);

		KontextReader.getInstance().init(configuration.getConfigRoot());
		DynamicAdminConfigReader.getInstance().init(configuration.getConfigRoot());

		final Injector injector = Guice.createInjector(new MKVAdminModule(configuration.getConfigRoot()));

		initPersistFilter(environment, injector);
		initSecureHeaders(environment);

		// authenticator ist hier ein CachingAccessTokenAuthenticator
		final Authenticator<String, Principal> authenticator = injector.getInstance(Authenticator.class);
		final OAuthCredentialAuthFilter<Principal> oAuthFilter = new OAuthCredentialAuthFilter.Builder<Principal>()
				.setAuthenticator(authenticator).setPrefix(BEARER_PREFIX).buildAuthFilter();
		environment.jersey().register(new AuthDynamicFeature(oAuthFilter));
		// der Parameter Principal.class in new
		// AuthValueFactoryProvider.Binder<>(Principal.class) sorgt dafür, dass der
		// vom Authenticator zurückgegebene Principal als Parameter
		// in die Methode der REST-Resource-Klassenstufe gesetzt wird. Muss
		// übereinstimmen mit
		// OAuthCredentialAuthFilter.Builder<Principal>! Sonst ist der Parameter null!!!
		environment.jersey().register(new AuthValueFactoryProvider.Binder<>(Principal.class));
		environment.jersey().register(RolesAllowedDynamicFeature.class);
		environment.jersey().register(MultiPartFeature.class);
//		environment.jersey().register(AngularJsonVulnerabilityProtectionInterceptor.class);

		if (!configuration.isTest()) {
			registerCsrfFilter(environment, injector);
		}

		final BVHealthCheck bvHealthCheck = injector.getInstance(BVHealthCheck.class);
		environment.healthChecks().register("BV", bvHealthCheck);

		final PingResource pingResource = injector.getInstance(PingResource.class);
		environment.jersey().register(pingResource);

		final AuthenticationResource authResource = injector.getInstance(AuthenticationResource.class);
		environment.jersey().register(authResource);

		environment.healthChecks().register("ping", new PingHealthCheck(pingResource));

		final InitDownloadcodesResource initDownloadResource = injector.getInstance(InitDownloadcodesResource.class);
		environment.jersey().register(initDownloadResource);

		final MailqueueResource mailqueueResource = injector.getInstance(MailqueueResource.class);
		mailqueueResource.setMaxAnzMailempfaenger(configuration.getMaxAnzMailempfaenger());
		mailqueueResource.setPathMails(configuration.getVerzeichnisNewsletter());
		environment.jersey().register(mailqueueResource);

		final BenutzerResource benutzerResource = injector.getInstance(BenutzerResource.class);
		environment.jersey().register(benutzerResource);

		final AuswertungResource auswertungResource = injector.getInstance(AuswertungResource.class);
		environment.jersey().register(auswertungResource);

		final UploadsResource uploadsResource = injector.getInstance(UploadsResource.class);
		uploadsResource.setUploadPath(configuration.getUploadPath());
		uploadsResource.setSandboxPath(configuration.getSandboxPath());
		environment.jersey().register(uploadsResource);

		environment.jersey().register(injector.getInstance(AuswertungsgruppenResource.class));
		environment.jersey().register(injector.getInstance(SchulenResource.class));
		environment.jersey().register(injector.getInstance(OrteResource.class));
		final LaenderResource laenderResource = injector.getInstance(LaenderResource.class);
		environment.jersey().register(laenderResource);

		environment.jersey().register(injector.getInstance(ExportBenutzerResource.class));

		environment.healthChecks().register("kataloge", new LaenderResourceHealthCheck(laenderResource));

		environment.jersey().register(injector.getInstance(LogResource.class));

	}

	/**
	 * Statt der standard-Jetty-Htmlseiten wird hier Plain Old JSON ohne sensible
	 * Informationen zurückgegeben
	 *
	 * @param environment
	 */
	private void configureErrorPages(final Environment environment) {
		final ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
		errorHandler.addErrorPage(401, "/errorpages/401");
		errorHandler.addErrorPage(404, "/errorpages/404");
		errorHandler.addErrorPage(500, "/errorpages/500");
		errorHandler.addErrorPage(900, "/errorpages/900");
		environment.getApplicationContext().setErrorHandler(errorHandler);
	}

	/**
	 * Registriert adaptierte ExceptionMapper aus
	 * io.dropwizard.server.AbstractServerFactory.
	 *
	 * @param environment
	 */
	private void configureExceptionMappers(final Environment environment) {
		environment.jersey().register(new EgladilLoggingExceptionMapper());
		environment.jersey().register(new JsonProcessingExceptionMapper());
		environment.jersey().register(new LoggingConstraintViolationExceptionMapper());
		environment.jersey().register(new EarlyEofExceptionMapper());
	}

	private void configureCors(final MKVAdminServiceConfiguration configuration, final Environment environment) {
		// ab Dropwizard 0.8.1 erforderlich
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
		final FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORSFilter",
				CrossOriginFilter.class);

		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, configuration.getAllowedOrigins());
		filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
				"Content-Type,Accept,Authorization,Origin,X-XSRF-TOKEN");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
		// filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM, "true");

		// Add URL mapping
		// FIXME: secure?
		filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
	}

	private void initPersistFilter(final Environment environment, final Injector injector) {

		final EgladilPersistFilter pfBV = injector
				.getInstance(Key.get(EgladilPersistFilter.class, BVPersistenceUnit.class));
		final EgladilPersistFilter pfMKV = injector
				.getInstance(Key.get(EgladilPersistFilter.class, MKVPersistenceUnit.class));

		final FilterRegistration.Dynamic bvFilter = environment.servlets().addFilter("BVPersistFilter", pfBV);
		final FilterRegistration.Dynamic mkvFilter = environment.servlets().addFilter("MKVPersistFilter", pfMKV);

		bvFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
		mkvFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
	}

	private void initSecureHeaders(final Environment environment) {
		{
			final FilterRegistration.Dynamic filter = environment.servlets().addFilter("CSPFilter", CSPFilter.class);
			filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		}
		{
			final FilterRegistration.Dynamic filter = environment.servlets().addFilter("SecureHeadersFilter",
					SecureHeadersFilter.class);
			filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		}
	}

	/**
	 * Registriert einen CSRF-Filter
	 *
	 * @param environment
	 * @param injector
	 * @param test
	 */
	private void registerCsrfFilter(final Environment environment, final Injector injector) {
		final CsrfFilter csrfFilter = injector.getInstance(CsrfFilter.class);

		final FilterRegistration.Dynamic filter = environment.servlets().addFilter("csrfFilter", csrfFilter);
		filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "/*");
	}
}
