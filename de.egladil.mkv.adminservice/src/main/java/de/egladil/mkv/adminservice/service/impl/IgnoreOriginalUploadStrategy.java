//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

/**
 * IgnoreOriginalUploadStrategy
 */
public class IgnoreOriginalUploadStrategy implements IOriginalUploadStrategy {

	private final String kontoUuid;

	/**
	 * Erzeugt eine Instanz von IgnoreOriginalUploadStrategy
	 */
	public IgnoreOriginalUploadStrategy(final String kontoUuid) {
		super();
		this.kontoUuid = kontoUuid;
	}

	@Override
	public String updateOriginalUpload(final Long idNeuesUpload) {
		String msg = "ADMIN hat Datei für Konto-UUID '" + this.kontoUuid + "' hochgeladen. Neues Upload: " + idNeuesUpload;
		return msg;
	}

}
