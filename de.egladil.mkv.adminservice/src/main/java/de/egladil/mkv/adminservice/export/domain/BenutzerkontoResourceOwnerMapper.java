//=====================================================
// Projekt: bv-export
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.export.domain;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.LoginSecrets;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.bv.aas.domain.Salz;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.MKVPersistenceUnit;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.user.Lehrerkonto;
import de.egladil.mkv.persistence.domain.user.Privatkonto;

/**
 * BenutzerkontoResourceOwnerMapper
 */
@MKVPersistenceUnit
public class BenutzerkontoResourceOwnerMapper implements Function<Benutzerkonto, ResourceOwner> {

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerkontoResourceOwnerMapper.class);

	private static final String LEHRER = "LEHRER,STANDARD";

	private static final String PRIVAT = "PRIVAT,STANDARD";

	private static final String STANDARD = "STANDARD";

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	/**
	 * Erzeugt eine Instanz von BenutzerkontoResourceOwnerMapper
	 */
	@Inject
	public BenutzerkontoResourceOwnerMapper(final ILehrerkontoDao lehrerkontoDao, final IPrivatkontoDao privatkontoDao) {
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
	}

	@Override
	public ResourceOwner apply(final Benutzerkonto benutzer) {

		final List<Rolle> rollen = benutzer.getRollen();

		// wegen filter ist klar, dass es genau eine Rolle gibt
		final Role role = rollen.get(0).getRole();
		final IMKVKonto mkvKonto = this.getMKVKonto(role, benutzer.getUuid());

		if (mkvKonto == null) {
			LOG.info("{} hat keinen Eintrag in mkverwaltung: wird ignoriert", benutzer);
			return null;
		}

		final ResourceOwner result = new ResourceOwner();
		result.setAktiviert(benutzer.isAktiviert());
		result.setAnonym(benutzer.isAnonym());
		result.setEmail(benutzer.getEmail());
		result.setLoginName(benutzer.getLoginName());
		result.setLoginSecrets(from(benutzer.getLoginSecrets()));
		result.setNachname(mkvKonto.getPerson().getNachname());
		switch (role) {
		case MKV_LEHRER:
			result.setRoles(LEHRER);
			break;
		case MKV_PRIVAT:
			result.setRoles(PRIVAT);
			break;
		default:
			result.setRoles(STANDARD);
			break;
		}
		result.setUuid(benutzer.getUuid());
		result.setVorname(mkvKonto.getPerson().getVorname());
		return result;
	}

	private Credentials from(final LoginSecrets loginSecrets) {

		final Credentials result = new Credentials();
		result.setLastLoginAttempt(loginSecrets.getLastLoginAttempt());
		result.setPasswordhash(loginSecrets.getPasswordhash());
		result.setSalt(from(loginSecrets.getSalz()));

		return result;

	}

	private Salt from(final Salz salz) {

		final Salt result = new Salt();
		result.setAlgorithmName(salz.getAlgorithmName());
		result.setIterations(salz.getIterations());
		result.setWert(salz.getWert());

		return result;
	}

	private IMKVKonto getMKVKonto(final Role role, final String uuid) {

		switch (role) {
		case MKV_LEHRER:
			final Optional<Lehrerkonto> lehrerkonto = this.lehrerkontoDao.findByUUID(uuid);
			if (lehrerkonto.isPresent()) {
				return lehrerkonto.get();
			}
			break;
		case MKV_PRIVAT:
			final Optional<Privatkonto> privatkonto = this.privatkontoDao.findByUUID(uuid);
			if (privatkonto.isPresent()) {
				return privatkonto.get();
			}
			break;
		default:
			return null;
		}

		return null;
	}
}
