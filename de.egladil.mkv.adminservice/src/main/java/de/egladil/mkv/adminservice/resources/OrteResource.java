//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;
import java.util.List;

import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.mkv.adminservice.payload.request.NeuerOrtPayload;
import de.egladil.mkv.adminservice.service.OrtskatalogService;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.persistence.payload.response.PublicOrt;
import io.dropwizard.auth.Auth;

/**
 * OrteResource zum Suchen und Pflegen des Ortskatalogs.
 */
@Path("")
@Singleton
public class OrteResource {

	private static final Logger LOG = LoggerFactory.getLogger(OrteResource.class);

	private final OrtskatalogService ortskatalogService;

	private final ResourceExceptionHandler exceptionHandler;

	private final ValidationDelegate validationDelegate;

	/**
	 * OrteResource
	 */
	@Inject
	public OrteResource(final OrtskatalogService ortskatalogService) {
		this.ortskatalogService = ortskatalogService;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
		this.validationDelegate = new ValidationDelegate();
	}

	/**
	 * Sucht eine Schule mit namensanteil und ggf. ortnamenteil und gibt eine Liste von PublicSchulePayload zurück.<br>
	 * <br>
	 * Wenn die Treffermenge zu groß ist (PreconditionFailedException), kommt ein 412 - precondition failed zurück.<br>
	 * Wenn Parameter nicht valide sind, kommt 422 - unprocessable entity
	 *
	 * @param principal
	 * @param name
	 * @param land
	 * @return Response
	 */
	@Path("orte")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response findOrte(@Auth final Principal principal, @QueryParam("name") @DeutscherName @Size(max = 100) final String name,
		@DeutscherName @Size(max = 100) @QueryParam("land") final String land) {

		try {

			if (StringUtils.isAllBlank(new String[] { name, land })) {
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.error("Ungültige Suchparameter: all blank"));
				return Response.status(422).entity(entity).build();
			}

			final List<PublicOrt> trefferliste = ortskatalogService.findOrte(name, land);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), trefferliste);
			return Response.ok(entity).build();

		} catch (final PreconditionFailedException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "general.maxTrefferExceeded", null);
		} catch (final Exception e) {
			LOG.error("[name=" + name + ", ort=" + land + "]: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Gibt Ort mit Schulen und Hateoas-Links zurück.
	 *
	 * @param principal
	 * @param kuerzel
	 * @return Response
	 */
	@Path("orte/{kuerzel}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response getOrt(@Auth final Principal principal, @PathParam("kuerzel") @NotBlank @Kuerzel final String kuerzel) {

		try {
			final PublicOrt po = ortskatalogService.getOrt(kuerzel);
			if (po == null) {
				LOG.warn("Ortskatalogeintrag mit kuerzel {} nicht vorhanden", kuerzel);
				return Response.status(Status.NOT_FOUND).build();
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), po);
			return Response.ok(entity).build();
		} catch (final Exception e) {
			LOG.error("[kuerzel=" + kuerzel + "]: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * @param principal EgladilPrincipal, enthält als name die Benutzer-UUID. Diese wird durch Dropwizard gesetzt
	 * @param payload
	 * @return Response
	 */
	@POST
	@Path("/orte")
	@Timed
	public Response ortAnlegen(@Auth final Principal principal, final NeuerOrtPayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, NeuerOrtPayload.class);
			final PublicOrt po = ortskatalogService.ortAnlegen(payload.getLandkuerzel(), payload.getName(), benutzerUuid);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("Ort erfolgreich angelegt"), po);
			return Response.status(201).entity(entity).build();
		} catch (final EgladilAuthorizationException e) {
			final String msg = e.getMessage() + " " + payload.toString();
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + msg);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilDuplicateEntryException | ResourceNotFoundException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Exception e) {
			LOG.error("Unerwartete Exception " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}
}
