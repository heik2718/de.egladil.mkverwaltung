//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.util.List;

import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.payload.response.PublicLand;

/**
 * LaenderService
 */
public interface LaenderService {

	/**
	 * Die Länder werden lazy geladen, also ohne Teilnahmen und Orte.
	 *
	 * @return List PublicLand
	 */
	List<PublicLand> getLaender();

	/**
	 * Läd das Land mit dem kuerzel vollständig (also auch mit Teilnahmen)
	 *
	 * @param kuerzel String
	 * @return PublicLand
	 */
	PublicLand getLand(String kuerzel);

	/**
	 * Legt ein neues Land an.
	 *
	 * @param kuerzel String
	 * @param name String
	 * @param benutzerUuid
	 * @return PublicLand (lazy)
	 */
	PublicLand landAnlegen(String kuerzel, String name, String benutzerUuid)
		throws EgladilAuthorizationException, EgladilDuplicateEntryException;

	// /**
	// * Holt die Teilnahmegruppe des gegebenen Landes zum gegebenen Jahr.
	// *
	// * @param landkuerzel String
	// * @param jahr
	// * @return Teilnahmegruppe
	// * @throws ResourceNotFoundException falls es kein Land mit dem gegebenen kuerzel gibt.
	// */
	// Teilnahmegruppe getTeilnahmegruppe(String landkuerzel, String jahr) throws ResourceNotFoundException;
	//
	// /**
	// * Erstellt eine Liste aller Länder bzw. auch Zusammenfassungen dieser mit Teilnahmen im gegebenen Jahr.
	// *
	// * @param jahr Strin das Jahr. Darf nicht null sein.
	// * @return List
	// */
	// List<Teilnahmegruppe> getLaenderuebersicht(String jahr);
}
