//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.LandKuerzel;

/**
 * NeuesLandPayload
 */
public class LandPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 5)
	@LandKuerzel
	private String kuerzel;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	/**
	 * Erzeugt eine Instanz von LandPayload
	 */
	public LandPayload() {
	}

	/**
	 * Erzeugt eine Instanz von NeuesLandPayload
	 */
	public LandPayload(final String kuerzel, final String name) {
		this.kuerzel = kuerzel;
		this.name = name;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LandPayload [kuerzel=" + kuerzel + ", name=" + name + "]";
	}

	/**
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return toString();
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Membervariable kuerzel
	 *
	 * @return die Membervariable kuerzel
	 */
	public String getKuerzel() {
		return kuerzel;
	}

}
