//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.adminservice.payload.response.AuswertungstabelleInfo;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;

/**
 * UploadsDelegate
 */
public interface UploadsDelegate {

	/**
	 *
	 * @param uploadPath String
	 * @return List
	 */
	List<AuswertungstabelleInfo> getContentsUploadDir(String uploadPath);

	/**
	 * Mit Hilfe des TeilnahmeIdentifiers und der Checksumme wird die UploadInfo geholt. Die Daten werden in
	 * Lösungszettel umgewandelt, Es wird ein neuer Eintrag in UploadInfo erzeugt. Die alte UploadInfo wird aktualisiert
	 * (Status.KORRIGIERT). Die Datei im Upload-Verzeichnis umbenannt in .korr. Es wird ein Eintrag im Ereignisprotokoll
	 * erzeugt, der in WAS die original-Upload-ID und die neue Upload-ID enthält. Außerdem wird ein Eintrag ins log
	 * gecshrieben.<br>
	 * <br>
	 * Die Checksumme des alten fehlerhaften uploads wird nicht geändert, damit ein erneuter Upload durch einen Lehrer
	 * nix kaputtmacht.
	 *
	 * @param benutzerUuid String
	 * @param data byte[] die Datei als Binärdaten.
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param checksumme String die Checksumme
	 * @param pathUploadDir String
	 * @param pathSandboxDir String
	 * @return List der aktuelle Inhalt des upload-Verzeichnisses
	 */
	List<AuswertungstabelleInfo> updateUpload(final String benutzerUuid, final byte[] data, TeilnahmeIdentifier teilnahmeIdentifier,
		String checksumme, final String pathUploadDir, final String pathSandboxDir)
		throws NullPointerException, IOException, URISyntaxException, EgladilStorageException;

	/**
	 * Mit Hilfe des TeilnahmeIdentifiers und der Checksumme wird die UploadInfo geholt. Die Daten werden in
	 * Lösungszettel umgewandelt, Es wird ein neuer Eintrag in UploadInfo erzeugt. Die alte UploadInfo wird aktualisiert
	 * (Status.KORRIGIERT). Die Datei im Upload-Verzeichnis umbenannt in .korr. Es wird ein Eintrag im Ereignisprotokoll
	 * erzeugt, der in WAS die original-Upload-ID und die neue Upload-ID enthält. Außerdem wird ein Eintrag ins log
	 * gecshrieben.<br>
	 * <br>
	 * Die Checksumme des alten fehlerhaften uploads wird nicht geändert, damit ein erneuter Upload durch einen Lehrer
	 * nix kaputtmacht.
	 *
	 * @param benutzerUuid String
	 * @param data byte[] die Datei als Binärdaten.
	 * @param teilnahmeIdentifier TeilnahmeIdentifier
	 * @param kontoUuid String die UUID desjenigen, der die Datei per Mail gesendet hat.
	 * @param pathUploadDir String
	 * @param pathSandboxDir String
	 * @return List der aktuelle Inhalt des upload-Verzeichnisses
	 */
	List<AuswertungstabelleInfo> insertUpload(final String benutzerUuid, final byte[] data, String kontoUuid,
		final String pathUploadDir, final String pathSandboxDir)
		throws NullPointerException, IOException, URISyntaxException, EgladilStorageException;

	/**
	 * Verschiebt die Dateien mit den gegebenen Dateinamen in das done-Verzeichnis des aktuellen Wettbewerbsjahrs, also
	 * nach upload/mkv/done/${wettbewerbsjahr}. Das Verzeichnis wird angelegt, wenn es noch nicht exostiert.
	 *
	 * @param dateinamen String[]
	 * @param pathUploadDir String das Verzeichnis mit den Uploads.
	 * @return List die aktuelle Liste der AuswertungstabellenInfos nach dem Verschieben.
	 */
	List<AuswertungstabelleInfo> verschiebeDateien(String[] dateinamen, String pathUploadDir) throws IOException;

}
