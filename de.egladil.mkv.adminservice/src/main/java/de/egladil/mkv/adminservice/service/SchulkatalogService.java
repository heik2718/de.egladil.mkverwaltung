//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.util.List;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.persistence.payload.request.GeaenderteSchulePayload;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;
import de.egladil.mkv.persistence.payload.response.PublicSchule;

/**
 * SchulkatalogService
 */
public interface SchulkatalogService {

	/**
	 * Sucht Schulen mit gegebenem Namen und, falls nicht blank, gegebenem ortnamen.
	 *
	 * @param name String name oder Teile des Namen
	 * @param ort String name oder Teile des Namen
	 * @return List
	 * @throws PreconditionFailedException wenn die Treffermenge zu groß ist.
	 */
	List<PublicSchule> findSchulen(final String name, final String ort) throws PreconditionFailedException;

	/**
	 * Sucht die Schule mit dem gegebenen Kuerzel
	 *
	 * @param kuerzel
	 * @return PublicSchule oder null.
	 */
	PublicSchule getSchule(String kuerzel);

	/**
	 * Sucht alle Jahre, in denen es Teilnahmen zu einem gegebenen Land gibt.
	 *
	 * @param landkuerzel String
	 * @return List
	 */
	List<String> findTeilnahmejahreLand(String landkuerzel);

	/**
	 * Gibt die Anzahl der Schulen in dem Ort mit der gegebenen ID zurück.
	 *
	 * @param id Long
	 * @return int
	 */
	int anzahlSchulenInOrt(Long id);

	/**
	 * Legt eine neue Schule im gegebenen Ort an.
	 *
	 * @param payload NeueSchulePayload
	 * @param benutzerUuid String
	 * @return PublicSchule (lazy)
	 *
	 */
	PublicSchule schuleAnlegen(NeueSchulePayload payload, String benutzerUuid);

	/**
	 * Ändert die Attribute der Schule.
	 *
	 * @param kuerzel
	 * @param payload
	 * @param benutzerUuid
	 * @return
	 */
	PublicSchule schuleAendern(String kuerzel, GeaenderteSchulePayload payload, String benutzerUuid);
}
