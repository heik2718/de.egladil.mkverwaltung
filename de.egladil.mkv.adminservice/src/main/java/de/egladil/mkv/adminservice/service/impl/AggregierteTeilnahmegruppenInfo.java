//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

/**
 * AggregierteTeilnahmegruppenInfo
 */
public class AggregierteTeilnahmegruppenInfo {

	private final long anzahlSchulteilnahmen;

	private final long anzahlKinder;

	private final String kuerzel;

	/**
	 * AggregierteTeilnahmegruppenInfo
	 */
	public AggregierteTeilnahmegruppenInfo(final long anzahlSchulteilnahmen, final long anzahlKinder, final String kuerzel) {
		this.anzahlSchulteilnahmen = anzahlSchulteilnahmen;
		this.anzahlKinder = anzahlKinder;
		this.kuerzel = kuerzel;
	}

	public final long getAnzahlSchulteilnahmen() {
		return anzahlSchulteilnahmen;
	}

	public final long getAnzahlKinder() {
		return anzahlKinder;
	}

	public final String getKuerzel() {
		return kuerzel;
	}

}
