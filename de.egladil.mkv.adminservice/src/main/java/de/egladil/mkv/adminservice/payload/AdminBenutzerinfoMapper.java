//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;

/**
 * AdminBenutzerinfoMapper
 */
public class AdminBenutzerinfoMapper {

	private static final Logger LOG = LoggerFactory.getLogger(AdminBenutzerinfoMapper.class);

	private final BenutzerPayloadMapper benutzerPayloadMapper;

	/**
	 * AdminBenutzerinfoMapper
	 */
	public AdminBenutzerinfoMapper(final BenutzerPayloadMapper benutzerPayloadMapper) {
		this.benutzerPayloadMapper = benutzerPayloadMapper;
	}

	/**
	 * Mapped die Trefferliste.
	 *
	 * @param trefferliste List
	 * @return List
	 */
	public List<MKVBenutzer> map(final List<Benutzerkonto> trefferliste) {
		final List<MKVBenutzer> result = new ArrayList<>(trefferliste.size());
		for (final Benutzerkonto benutzer : trefferliste) {
			try {
				final MKVBenutzer mkvBenutzer = this.benutzerPayloadMapper.createBenutzer(benutzer, true);
				result.add(mkvBenutzer);
			} catch (final MKVException e) {
				LOG.warn(e.getMessage());
			}
		}

		return result;
	}
}
