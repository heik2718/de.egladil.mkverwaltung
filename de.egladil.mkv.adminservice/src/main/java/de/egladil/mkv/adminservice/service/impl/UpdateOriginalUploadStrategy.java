//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;

/**
 * UpdateOriginalUploadStrategy
 */
public class UpdateOriginalUploadStrategy implements IOriginalUploadStrategy {

	private static final Logger LOG = LoggerFactory.getLogger(UpdateOriginalUploadStrategy.class);

	private final IUploadDao uploadDao;

	private final AuswertungstabelleFileWriter fileWriter;

	private final Upload originalUpload;

	private final String pathUploadDir;

	private final String dateinameOriginalUpload;

	/**
	 * Erzeugt eine Instanz von UpdateOriginalUploadStrategy
	 */
	public UpdateOriginalUploadStrategy(final IUploadDao uploadDao, final AuswertungstabelleFileWriter fileWriter,
		final Upload originalUpload, final String pathUploadDir, final String dateinameOriginalUpload) {
		super();
		this.uploadDao = uploadDao;
		this.fileWriter = fileWriter;
		this.originalUpload = originalUpload;
		this.pathUploadDir = pathUploadDir;
		this.dateinameOriginalUpload = dateinameOriginalUpload;
	}

	@Override
	public String updateOriginalUpload(final Long idNeuesUpload) {
		originalUpload.setUploadStatus(UploadStatus.KORRIGIERT);
		uploadDao.persist(originalUpload);
		LOG.info("Original-Upload auf KORRIGIERT gesetzt");
		String msg = "ADMIN hat Upload " + originalUpload.getId() + " korrigiert. Neues Upload: " + idNeuesUpload;
		fileWriter.renameFileQuietly(pathUploadDir, dateinameOriginalUpload, UploadStatus.KORRIGIERT.getFileExtension());
		return msg;
	}

}
