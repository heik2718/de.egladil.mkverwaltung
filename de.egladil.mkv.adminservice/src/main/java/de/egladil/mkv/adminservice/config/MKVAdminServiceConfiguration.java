//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.config;

import io.dropwizard.Configuration;

/**
 * MKVAdminServiceConfiguration
 */
public class MKVAdminServiceConfiguration extends Configuration {

	private boolean test;

	private String configRoot;

	private String allowedOrigins;

	private boolean landFreischalten;

	private int maxAnzMailempfaenger;

	private String verzeichnisNewsletter;

	private String uploadPath;

	private String sandboxPath;

	private String aktuellesWettbewerbsjahr;

	/**
	 * Erzeugt eine Instanz von MKVAdminServiceConfiguration
	 */
	public MKVAdminServiceConfiguration() {
	}

	public boolean isTest() {
		return test;
	}

	public String getConfigRoot() {
		return configRoot;
	}

	public String getAllowedOrigins() {
		return allowedOrigins;
	}

	/**
	 * Liefert die Membervariable landFreischalten
	 *
	 * @return die Membervariable landFreischalten
	 */
	public boolean isLandFreischalten() {
		return landFreischalten;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param configRoot neuer Wert der Membervariablen configRoot
	 */
	public void setConfigRoot(String configRoot) {
		this.configRoot = configRoot;
	}

	/**
	 * Liefert die Membervariable maxAnzMailempfaenger
	 *
	 * @return die Membervariable maxAnzMailempfaenger
	 */
	public int getMaxAnzMailempfaenger() {
		return maxAnzMailempfaenger;
	}

	public String getVerzeichnisNewsletter() {
		return verzeichnisNewsletter;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public String getSandboxPath() {
		return sandboxPath;
	}

	public String getAktuellesWettbewerbsjahr() {
		return aktuellesWettbewerbsjahr;
	}
}
