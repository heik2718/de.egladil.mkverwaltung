//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.util.Comparator;

import de.egladil.mkv.adminservice.payload.response.RootgruppeResponsePayload;

/**
 * RootgruppeResponsePayloadNameSorter
 */
public class RootgruppeResponsePayloadNameSorter implements Comparator<RootgruppeResponsePayload> {

	@Override
	public int compare(final RootgruppeResponsePayload first, final RootgruppeResponsePayload second) {
		return first.getRootgruppe().getName().compareTo(second.getRootgruppe().getName());
	}
}
