//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;
import java.util.List;

import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.InvalidInputException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.mkv.adminservice.service.SchulkatalogService;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.persistence.payload.request.GeaenderteSchulePayload;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.utils.LengthTester;
import de.egladil.mkv.persistence.utils.UrkundeConstants;
import io.dropwizard.auth.Auth;

/**
 * SchulenResource
 */
@Path("")
@Singleton
public class SchulenResource {

	private static final Logger LOG = LoggerFactory.getLogger(SchulenResource.class);

	private final SchulkatalogService schulkatalogService;

	private final ResourceExceptionHandler exceptionHandler;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	/**
	 * SchulenResource
	 */
	@Inject
	public SchulenResource(final SchulkatalogService schulkatalogService) {
		this.schulkatalogService = schulkatalogService;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
	}

	/**
	 * Sucht eine Schule mit namensanteil und ggf. ortnamenteil und gibt eine Liste von PublicSchulePayload zurück.<br>
	 * <br>
	 * Wenn die Treffermenge zu groß ist (PreconditionFailedException), kommt ein 412 - precondition failed zurück.<br>
	 * Wenn Parameter nicht valide sind, kommt 422 - unprocessable entity
	 *
	 * @param principal
	 * @param name
	 * @param ort
	 * @return Response
	 */
	@Path("schulen")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response findSchulen(@Auth final Principal principal,
		@QueryParam("name") @DeutscherName @Size(max = 100) final String name,
		@DeutscherName @Size(max = 100) @QueryParam("ort") final String ort) {

		try {

			if (StringUtils.isAllBlank(new String[] { name, ort })) {
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.error("Ungültige Suchparameter: all blank"));
				return Response.status(422).entity(entity).build();
			}

			final List<PublicSchule> trefferliste = schulkatalogService.findSchulen(name, ort);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), trefferliste);
			return Response.ok(entity).build();

		} catch (final PreconditionFailedException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "general.maxTrefferExceeded", null);
		} catch (final Exception e) {
			LOG.error("[name=" + name + ", ort=" + ort + "]: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Gibt die Schule mit Hateoas-Links zurück.
	 *
	 * @param principal
	 * @param kuerzel
	 * @return Response
	 */
	@Path("schulen/{kuerzel}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response getSchule(@Auth final Principal principal, @PathParam("kuerzel") @NotBlank @Kuerzel final String kuerzel) {

		try {
			final PublicSchule ps = schulkatalogService.getSchule(kuerzel);
			if (ps == null) {
				LOG.warn("Schulkatalogeintrag mit kuerzel {} nicht vorhanden", kuerzel);
				return Response.status(Status.NOT_FOUND).build();
			}
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), ps);
			return Response.ok(entity).build();

		} catch (final PreconditionFailedException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "general.maxTrefferExceeded", null);
		} catch (final Exception e) {
			LOG.error("[kuerzel=" + kuerzel + "]: " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 *
	 * @param principal EgladilPrincipal, enthält als name die Benutzer-UUID. Diese wird durch Dropwizard gesetzt
	 * @param payload
	 * @return
	 */
	@POST
	@Path("/schulen")
	@Timed
	public Response schuleAnlegen(@Auth final Principal principal, final NeueSchulePayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, NeueSchulePayload.class);
			checkSchulnameUrkundenkompatibel(payload.getName());

			final PublicSchule ps = schulkatalogService.schuleAnlegen(payload, benutzerUuid);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("Schule erfolgreich angelegt"), ps);
			return Response.status(201).entity(entity).build();
		} catch (final EgladilAuthorizationException e) {
			final String msg = e.getMessage() + " " + payload.toString();
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + msg);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilDuplicateEntryException | ResourceNotFoundException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Exception e) {
			LOG.error("Unerwartete Exception " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	/**
	 *
	 * @param principal EgladilPrincipal, enthält als name die Benutzer-UUID. Diese wird durch Dropwizard gesetzt
	 * @param payload
	 * @return
	 */
	@PUT
	@Path("/schulen/{kuerzel}")
	@Timed
	public Response schuleAendern(@Auth final Principal principal, @PathParam("kuerzel") @NotBlank @Kuerzel final String kuerzel,
		final GeaenderteSchulePayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, GeaenderteSchulePayload.class);
			checkSchulnameUrkundenkompatibel(payload.getName());

			final PublicSchule ps = schulkatalogService.schuleAendern(kuerzel, payload, benutzerUuid);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("Schule erfolgreich geändert"), ps);
			return Response.status(201).entity(entity).build();
		} catch (final EgladilAuthorizationException e) {
			final String msg = e.getMessage() + " " + payload.toString();
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + msg);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilDuplicateEntryException | ResourceNotFoundException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final InvalidInputException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Exception e) {
			LOG.error("Unerwartete Exception " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

	/**
	 * @param payload
	 */
	private void checkSchulnameUrkundenkompatibel(final String name) {
		try {
			new LengthTester().checkTooLongAndThrow(name, UrkundeConstants.SIZE_TEXT_NORMAL);
		} catch (final IllegalArgumentException e) {
			throw new InvalidInputException("Der Name passt nicht auf eine Urkunde");
		}
	}
}
