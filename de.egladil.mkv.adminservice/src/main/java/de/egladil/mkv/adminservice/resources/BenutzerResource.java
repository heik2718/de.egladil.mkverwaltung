//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.math.BigInteger;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.payload.AdminBenutzerinfoMapper;
import de.egladil.mkv.adminservice.payload.request.BenutzerSuchmodus;
import de.egladil.mkv.adminservice.service.MKVBenutzerService;
import de.egladil.mkv.adminservice.validation.AdminSuchePayload;
import de.egladil.mkv.adminservice.validation.StringLatinPayload;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.payload.request.UuidPayload;
import de.egladil.mkv.persistence.payload.response.benutzer.MKVBenutzer;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import io.dropwizard.auth.Auth;

/**
 * BenutzerResource
 */
@Singleton
@Path("/benutzer")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class BenutzerResource {

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerResource.class);

	private final IBenutzerService benutzerService;

	private final MKVBenutzerService mkvBenutzerService;

	private final BenutzerPayloadMapper benutzerPayloadMapper;

	private final AdminBenutzerinfoMapper adminBenutzerinfoMapper;

	private final ValidationDelegate validationDelegate;

	private final IAnonymisierungsservice anonymisierungsservice;

	private final ResourceExceptionHandler exceptionHandler;

	@Inject
	public BenutzerResource(final IBenutzerService benutzerService, final BenutzerPayloadMapper benutzerPayloadMapper,
		final IAnonymisierungsservice anonymisierungsservice, final MKVBenutzerService mkvBenutzerService) {
		this.adminBenutzerinfoMapper = new AdminBenutzerinfoMapper(benutzerPayloadMapper);
		this.benutzerService = benutzerService;
		this.benutzerPayloadMapper = benutzerPayloadMapper;
		this.mkvBenutzerService = mkvBenutzerService;
		this.validationDelegate = new ValidationDelegate();
		this.anonymisierungsservice = anonymisierungsservice;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
	}

	/**
	 * Sucht Benutzerdaten entweder mit deren Mail-Adresse oder mit Vor- und/oder Nachname. Die Suche erfolgt unscharf.
	 *
	 * @param principal
	 * @param param String
	 * @param modus BenutzerSuchmodus
	 * @return Response
	 */
	@GET
	public Response findBenutzer(@Auth final Principal principal,
		@QueryParam(value = "modus") @NotNull final BenutzerSuchmodus modus,
		@QueryParam(value = "param") @NotBlank final String param) {
		try {

			List<Benutzerkonto> trefferliste = new ArrayList<>();

			switch (modus) {
			case EMAIL:
				validationDelegate.check(new AdminSuchePayload(param), AdminSuchePayload.class);
				trefferliste = benutzerService.findByEmailLike(param, Anwendung.MKV);
				break;
			case NAME:
				validationDelegate.check(new StringLatinPayload(param), StringLatinPayload.class);
				trefferliste = mkvBenutzerService.findMKVBenutzerWithName(param);
				break;
			case ID:
				validationDelegate.check(new UuidPayload(param), UuidPayload.class);
				trefferliste = mkvBenutzerService.findMKVBenutzerWithId(param);
			default:
				break;
			}

			final BigInteger anzahlTreffer = BigInteger.valueOf(trefferliste.size());

			final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();
			final int maximaleTreffermenge = config.getMaximaleTreffermengeBenutzer();

			if (anzahlTreffer.compareTo(BigInteger.valueOf(maximaleTreffermenge)) > 0) {
				LOG.warn("MKVBenutzersuche mit Modus {}, param '{}': Anzahl Treffer: {}, erlaubt: {}", modus, param,
					anzahlTreffer.intValue(), maximaleTreffermenge);
				throw new PreconditionFailedException(
					"maximal " + maximaleTreffermenge + " Treffer erlaubt. Bitte Suche einschränken");
			}

			final List<MKVBenutzer> result = adminBenutzerinfoMapper.map(trefferliste);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"), result);

			return Response.ok().entity(entity).build();
		} catch (final EgladilWebappException e) {
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.error("Die Eingaben sind nicht korrekt."));
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "ungültiger QueryParameter [" + param + "] (Benutzersuche mit modus " + modus
				+ " )");
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("MKVBenutzersuche mit Modus {}, param '{}': {}", modus, param, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final PreconditionFailedException e) {
			return exceptionHandler.mapToMKVApiResponse(e, "general.maxTrefferExceeded", null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Sucht PublicMKVUser mit uuid. Genaue Suche!
	 *
	 * @param principal
	 * @param uuid die UUID des Benuterkontos
	 * @return Response
	 */
	@GET
	@Path("/{uuid}")
	public Response getBenutzerWithUuid(@Auth final Principal principal, @PathParam(value = "uuid") final String uuid) {
		try {
			validationDelegate.check(new UuidPayload(uuid), UuidPayload.class);

			final Benutzerkonto benutzer = benutzerService.findBenutzerkontoByUUID(uuid);
			if (benutzer == null) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Benutzersuche mit UUID " + uuid);
				return ExceptionFactory.resourceNotFound("Tja, gibts halt nich").getResponse();
			}
			final MKVBenutzer user = benutzerPayloadMapper.createBenutzer(benutzer, false);
			return Response.ok().entity(user).build();
		} catch (IllegalArgumentException | EgladilWebappException e) {
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.error("Die Eingaben sind nicht korrekt."));
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "ungültiger PathParameter [" + uuid + "] (Benutzersuche mit UUID)");
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /benutzer/{}: {}", uuid, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@DELETE
	@Path("/{uuid}")
	public Response benutzerAnonymisieren(@Auth final Principal principal, @PathParam(value = "uuid") final String firstChars) {
		try {
			validationDelegate.check(new UuidPayload(firstChars), UuidPayload.class);
			final Optional<Benutzerkonto> optBen = benutzerService.findBenutzerkontoByFirstCharsOfUUID(firstChars);
			if (!optBen.isPresent()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Benutzersuche mit UUID " + firstChars);
				return ExceptionFactory.resourceNotFound("Tja, gibts halt nich ¯\\_(ツ)_/¯").getResponse();
			}
			final String uuid = optBen.get().getUuid();
			anonymisierungsservice.kontoAnonymsieren(uuid, "Admin über MKVADMIN");
			final APIResponsePayload pm = new APIResponsePayload(
				APIMessage.info("Benutzerkonto mit UUID " + uuid + " anonymisiert"));
			return Response.ok().entity(pm).build();
		} catch (IllegalArgumentException | EgladilWebappException e) {
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.error("Die Eingaben sind nicht korrekt."));
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "ungültiger PathParameter [" + firstChars + "] (Benutzersuche mit UUID)");
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (final MKVException e) {
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "Benutzersuche mit UUID " + firstChars);
			return ExceptionFactory.resourceNotFound("Tja, gibts halt nich ¯\\_(ツ)_/¯").getResponse();
		} catch (EgladilAuthenticationException | DisabledAccountException e) {
			return ExceptionFactory.forbidden().getResponse();
		} catch (final Exception e) {
			return ExceptionFactory.internalServerErrorException(LOG, e, "Benutzersuche mit UUID " + firstChars).getResponse();
		}
	}
}
