//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.auswertungen;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.auswertungen.persistence.ProcessUploadResult;
import de.egladil.mkv.auswertungen.persistence.UploadProcessor;
import de.egladil.mkv.persistence.dao.ILoesungszettelDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.dao.IUploadInfoDao;
import de.egladil.mkv.persistence.dao.IWettbewerbsloesungDao;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.UploadInfo;

/**
 * AuswertungenRunner erzeugt aus den hochgeladenen Auswertungsdateien die persistenten Loesungszettel.
 */
@Singleton
public class AuswertungenRunner {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungenRunner.class);

	private final IUploadInfoDao uploadInfoDao;

	private final IUploadDao uploadDao;

	private final ILoesungszettelDao loesungszettelDao;

	private final IProtokollService protokollService;

	private final IWettbewerbsloesungDao wettbewerbsloesungDao;

	private final AuswertungstabelleFileWriter fileWriter;

	public class UploadLog {
		private final int anzahlGesamt;

		private long uploadStarted;

		private long uploadFinished;

		private int anzahlOk;

		private int anzahlLeer;

		private int anzahlFehler;

		public UploadLog(final int anzahlGesamt) {
			this.anzahlGesamt = anzahlGesamt;
			this.uploadStarted = System.currentTimeMillis();
		}

		@Override
		public String toString() {
			return "UploadLog [anzahlGesamt=" + anzahlGesamt + ", anzahlOK=" + anzahlOk + ", anzahlLeer=" + anzahlLeer
				+ ", anzahlFehler=" + anzahlFehler + ", Dauer=" + calcDuration() + "]";
		}

		void markOK() {
			anzahlOk++;
		}

		void markLeer() {
			anzahlLeer++;
		}

		void markFehler() {
			anzahlFehler++;
		}

		void markFinished() {
			uploadFinished = System.currentTimeMillis();
		}

		private String calcDuration() {
			final long millies = uploadFinished - uploadStarted;
			return millies + " ms";
		}
	}

	@Inject
	public AuswertungenRunner(final IUploadInfoDao uploadInfoDao, final IUploadDao uploadDao,
		final ILoesungszettelDao loesungszettelDao, final IWettbewerbsloesungDao wettbewerbsloesungDao,
		final IProtokollService protokollService, final AuswertungstabelleFileWriter fileWriter) {
		this.uploadInfoDao = uploadInfoDao;
		this.uploadDao = uploadDao;
		this.loesungszettelDao = loesungszettelDao;
		this.wettbewerbsloesungDao = wettbewerbsloesungDao;
		this.protokollService = protokollService;
		this.fileWriter = fileWriter;
	}

	/**
	 * Läd alle noch nicht verarbeiteten Auswertungen und verwandelt sie in loesungszettel. Protokolliert wird mit
	 * Ereignisart UPLOAD_VERARBEITET.
	 *
	 * @param benutzerUuid String für die Protokollierung.
	 * @param pathUploadDir String absoluter Pfad zum Upload-Verzeichnis
	 */
	public UploadLog processUploads(final String benutzerUuid, final String pathUploadDir) {

		final List<UploadInfo> uploadInfos = uploadInfoDao.findValidUploadsByStatus(UploadStatus.NEU);
		final UploadLog uploadLog = new UploadLog(uploadInfos.size());
		if (uploadInfos.isEmpty()) {
			LOG.info("keine neuen Uploads vorhanden.");
			return uploadLog;
		}
		final UploadProcessor uploadProcessor = new UploadProcessor(loesungszettelDao, wettbewerbsloesungDao, uploadDao,
			uploadInfoDao, null, fileWriter);

		for (final UploadInfo uploadInfo : uploadInfos) {
			final ProcessUploadResult processUploadResult = uploadProcessor.processUploadAndExceptions(uploadInfo, pathUploadDir);
			final UploadStatus statusNeu = processUploadResult.getStatusNeu();
			if (statusNeu != null) {
				switch (statusNeu) {
				case FEHLER:
					uploadLog.markFehler();
					break;
				case FERTIG:
					uploadLog.markOK();
					break;
				case LEER:
					uploadLog.markLeer();
					break;
				default:
					LOG.debug("Status neu = {}", statusNeu);
					break;
				}
			}

		}
		uploadLog.markFinished();

		final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_VERARBEITET.getKuerzel(), benutzerUuid, uploadLog.toString());
		protokollService.protokollieren(ereignis);
		return uploadLog;
	}
}
