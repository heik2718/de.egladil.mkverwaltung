//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.service.MKVBenutzerService;
import de.egladil.mkv.persistence.dao.IAdminBenutzerDao;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.domain.user.AdminBenutzerinfo;

/**
 * MKVBenutzerServiceImpl
 */
@Singleton
public class MKVBenutzerServiceImpl implements MKVBenutzerService {

	private static final Logger LOG = LoggerFactory.getLogger(MKVBenutzerServiceImpl.class);

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	private final IBenutzerService benutzerService;

	private final IAdminBenutzerDao adminBenutzerDao;

	@Inject
	public MKVBenutzerServiceImpl(final IAdminBenutzerDao adminBenutzerDao, final ILehrerkontoDao lehrerkontoDao,
		final IPrivatkontoDao privatkontoDao, final IBenutzerService benutzerService) {

		this.adminBenutzerDao = adminBenutzerDao;
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
		this.benutzerService = benutzerService;
	}

	@Override
	public List<Benutzerkonto> findMKVBenutzerWithName(final String name) {

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException(name);
		}

		final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();
		final int maximaleTreffermenge = config.getMaximaleTreffermengeBenutzer();

		final BigInteger anzahlTreffer = adminBenutzerDao.getAnzahlWithNameLike(name);

		if (anzahlTreffer.compareTo(BigInteger.valueOf(maximaleTreffermenge)) > 0) {
			LOG.warn("MKVBenutzersuche mit name '{}': Anzahl Treffer: {}, erlaubt: {}", name, anzahlTreffer.intValue(),
				maximaleTreffermenge);
			throw new PreconditionFailedException("maximal " + maximaleTreffermenge + " Treffer erlaubt. Bitte Suche einschränken");
		}

		final List<AdminBenutzerinfo> treffermenge = adminBenutzerDao.findWithName(name);
		final List<Benutzerkonto> result = new ArrayList<>(treffermenge.size());
		for (final AdminBenutzerinfo info : treffermenge) {
			final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(info.getUuid());
			if (benutzerkonto != null) {
				result.add(benutzerkonto);
			} else {
				LOG.warn("Benutzersuche mit name '{}': kein Benutzerkonto mit uuid '{}' vorhanden. Treffer wird ignoriert.");
			}
		}

		return result;
	}

	@Override
	public List<Benutzerkonto> findMKVBenutzerWithId(final String uuidPart) {

		if (StringUtils.isBlank(uuidPart)) {
			throw new IllegalArgumentException(uuidPart);
		}

		final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();
		final int maximaleTreffermenge = config.getMaximaleTreffermengeBenutzer();

		final BigInteger anzahlTreffer = adminBenutzerDao.getAnzahlWhereUuidStartsWith(uuidPart);
		if (anzahlTreffer.compareTo(BigInteger.valueOf(maximaleTreffermenge)) > 0) {
			LOG.warn("MKVBenutzersuche mit name '{}': Anzahl Treffer: {}, erlaubt: {}", uuidPart, anzahlTreffer.intValue(),
				maximaleTreffermenge);
			throw new PreconditionFailedException("maximal " + maximaleTreffermenge + " Treffer erlaubt. Bitte Suche einschränken");
		}

		final List<AdminBenutzerinfo> treffermenge = adminBenutzerDao.findWhereUuidStartsWith(uuidPart);
		final List<Benutzerkonto> result = new ArrayList<>(treffermenge.size());
		for (final AdminBenutzerinfo info : treffermenge) {
			final Benutzerkonto benutzerkonto = benutzerService.findBenutzerkontoByUUID(info.getUuid());
			if (benutzerkonto != null) {
				result.add(benutzerkonto);
			} else {
				LOG.warn("Benutzersuche mit name '{}': kein Benutzerkonto mit uuid '{}' vorhanden. Treffer wird ignoriert.");
			}
		}

		return result;
	}
}
