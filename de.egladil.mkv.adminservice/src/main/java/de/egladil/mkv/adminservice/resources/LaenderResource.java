//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.mkv.adminservice.payload.request.LandPayload;
import de.egladil.mkv.adminservice.service.LaenderService;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import io.dropwizard.auth.Auth;

/**
 * LaenderResource
 */
@Path("")
@Singleton
public class LaenderResource {

	private static final Logger LOG = LoggerFactory.getLogger(LaenderResource.class);

	private final ResourceExceptionHandler exceptionHandler;

	private final LaenderService laenderService;

	private final ValidationDelegate validationDelegate;

	/**
	 * LaenderResource
	 */
	@Inject
	LaenderResource(final LaenderService laenderService) {
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
		this.laenderService = laenderService;
		validationDelegate = new ValidationDelegate();
	}

	public int anzahlLaender() {
		return this.laenderService.getLaender().size();
	}

	/**
	 * Läd alle anzeigbaren Länder aus dem Länderkatalog.
	 *
	 * @param principal
	 * @return
	 */
	@Path("laender")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response getLaender(@Auth final Principal principal) {

		try {
			final List<PublicLand> laender = this.laenderService.getLaender();

			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info(""), laender);
			return Response.ok(entity).build();

		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /laender: {}", e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Läd das gegebene Land mit hateoas.
	 *
	 * @param principal
	 * @return
	 */
	@Path("laender/{kuerzel}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response getLand(@Auth final Principal principal, @PathParam("kuerzel") @NotBlank @Kuerzel final String kuerzel) {
		return null;
	}

	/**
	 * @param principal EgladilPrincipal, enthält als name die Benutzer-UUID. Diese wird durch Dropwizard gesetzt
	 * @param payload
	 * @return Response
	 */
	@POST
	@Path("/laender")
	@Timed
	public Response landAnlegen(@Auth final Principal principal, final LandPayload payload) {
		final String benutzerUuid = principal.getName();
		try {
			validationDelegate.check(payload, LandPayload.class);
			final PublicLand pl = laenderService.landAnlegen(payload.getKuerzel(), payload.getName(), benutzerUuid);
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("Land erfolgreich angelegt"), pl);
			return Response.status(201).entity(entity).build();
		} catch (final EgladilAuthorizationException e) {
			final String msg = e.getMessage() + " " + payload.toString();
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + msg);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final EgladilDuplicateEntryException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Exception e) {
			LOG.error("Unerwartete Exception " + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		}
	}

}
