//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.LandKuerzel;

/**
 * NeuerOrtPayload
 */
public class NeuerOrtPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@NotNull
	@Size(min = 1, max = 5)
	@LandKuerzel
	private String landkuerzel;

	/**
	 * Erzeugt eine Instanz von NeuerOrtPayload
	 */
	public NeuerOrtPayload() {
	}

	/**
	 * Erzeugt eine Instanz von NeuerOrtPayload
	 */
	public NeuerOrtPayload(final String landkuerzel, final String name) {
		this.name = name;
		this.landkuerzel = landkuerzel;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NeuerOrtPayload [name=" + name + ", landkuerzel=" + landkuerzel + "]";
	}

	/**
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return toString();
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Membervariable landkuerzel
	 *
	 * @return die Membervariable landkuerzel
	 */
	public String getLandkuerzel() {
		return landkuerzel;
	}
}
