//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.domain.Role;
import de.egladil.common.exception.EgladilAuthorizationException;

/**
 * BaseAuthorizedService
 */
public abstract class BaseAuthorizedService {

	private final IBenutzerService benutzerService;

	/**
	 * BaseAuthorizedService
	 */
	public BaseAuthorizedService(final IBenutzerService benutzerService) {
		this.benutzerService = benutzerService;
	}

	/**
	 * Autorisiert die aktion oder auch nicht :).
	 *
	 * @param benutzerUuid
	 */
	protected void authorize(final String benutzerUuid) {
		if (!benutzerService.isBenutzerInRole(Role.MKV_ADMIN, benutzerUuid)) {
			throw new EgladilAuthorizationException("Keine Berechtigung.");
		}
	}
}
