//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.mails;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.validation.PathValidator;
import de.egladil.mkv.adminservice.payload.request.NewsletterPayload;
import de.egladil.mkv.persistence.domain.user.IMailempfaenger;
import de.egladil.mkv.persistence.exceptions.MKVException;
import de.egladil.mkv.persistence.file.DateiValidationStrategie;

/**
 * MailqueueDelegate
 */
@Singleton
public class MailqueueDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(MailqueueDelegate.class);

	private final IMailqueueDao mailqueueDao;

	private int sizeDBFieldEmpfaenger = 2000;

	private final PathValidator pathValidator = new PathValidator();

	private class Mailempfaenger implements IMailempfaenger {
		/* serialVersionUID */
		private static final long serialVersionUID = 1L;

		private final String email;

		public Mailempfaenger(final String email) {
			this.email = email;
		}

		@Override
		public String getEmail() {
			return email;
		}

		@Override
		public String toString() {
			return email;
		}

		@Override
		public Long getId() {
			return null;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final Mailempfaenger other = (Mailempfaenger) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (email == null) {
				if (other.email != null)
					return false;
			} else if (!email.equals(other.email))
				return false;
			return true;
		}

		private MailqueueDelegate getOuterType() {
			return MailqueueDelegate.this;
		}

	}

	/**
	 * Erzeugt eine Instanz von MailqueueDelegate
	 */
	@Inject
	public MailqueueDelegate(final IMailqueueDao mailqueueDao) {
		this.mailqueueDao = mailqueueDao;
	}

	/**
	 * Erzeugt aus der Datei payload.filenameEmpfaenger Einträge in mailqueue.
	 *
	 * @param payload
	 * @param pathMails
	 */
	public List<IMailempfaenger> queueMails(final NewsletterPayload payload, final String pathMails, final int maxAnzahlEmpfaengerJeMail)
		throws IllegalArgumentException, IOException, MKVException {
		if (payload == null) {
			throw new IllegalArgumentException("payload null");
		}
		if (pathMails == null) {
			throw new IllegalArgumentException("pathMails null");
		}
		final List<IMailempfaenger> empfaenger = this.getMailempfaenger(payload, pathMails);
		final String contents = this.getMailbody(payload, pathMails);
		final List<List<IMailempfaenger>> gruppen = this.mailempfaengerGruppieren(empfaenger, maxAnzahlEmpfaengerJeMail);
		this.mailqueueSpeichern(gruppen, payload.getBetreff(), contents, maxAnzahlEmpfaengerJeMail);
		return empfaenger;
	}

	/**
	 * Erzeugt aus den Gruppen MailqueueItems und speichert diese in der Datenbank.
	 *
	 * @param gruppen List<List<T>>
	 * @param jahr String
	 * @param anzahlEmpfaenger int fürs LOG
	 * @throws IOException
	 */
	<T extends IMailempfaenger> void mailqueueSpeichern(final List<List<T>> gruppen, final String subject, final String body,
		final int anzahlEmpfaenger) throws IOException {
		final int gesamt = gruppen.size();

		LOG.info(GlobalConstants.LOG_PREFIX_MAILVERSAND + "Eintrag in Mailqueue: Anzahl Empfänger= {}, Anzahl Gruppen={}",
			anzahlEmpfaenger, gesamt);

		int count = 0;

		for (final List<T> gruppe : gruppen) {
			final MailqueueItem item = new MailqueueItem();
			final List<String> mailadressen = new ArrayList<>();
			for (final IMailempfaenger e : gruppe) {
				mailadressen.add(e.getEmail());
			}
			final String recipients = PrettyStringUtils.collectionToDefaultString(mailadressen);
			item.setRecipients(recipients);
			item.setStatus(MailqueueItemStatus.WAITING);
			item.setSubject(subject);
			item.setMailBody(body);
			item.setStatusmessage("noch nicht gesendet");

			try {
				mailqueueDao.persist(item);
				count++;
				LOG.info("{} von {} fertig", count, gesamt);
			} catch (final PersistenceException e) {
				LOG.error("MailqueItem konnte nicht gespeichert werden. Empfänger = {}, {}", mailadressen, e.getMessage(), e);
			}
		}
	}

	/**
	 * Fasst alle Mailempfänger zu Gruppen von maximal maxAnzahlEmpfaengerJeMail zusammen, deren zusammengesetzte
	 * Mailadressen nicht länger als 2000 Zeichen sind.
	 *
	 * @param alle
	 * @param maxAnzahlEmpfaengerJeMail
	 * @param
	 *
	 * @return
	 */
	<T extends IMailempfaenger> List<List<T>> mailempfaengerGruppieren(final List<T> alle, final int maxAnzahlEmpfaengerJeMail) {
		final int anzahlKommas = maxAnzahlEmpfaengerJeMail;
		int maximaleLaengeEmpfaenger = 0;
		maximaleLaengeEmpfaenger = sizeDBFieldEmpfaenger - anzahlKommas - 25; // bischen Puffer für DEFAULT-Empfänger

		final List<List<T>> result = new ArrayList<>();
		String empfaenger = "";
		int aktuelleAnzahl = 0;
		List<T> gruppe = new ArrayList<>();

		for (final T e : alle) {
			gruppe.add(e);
			empfaenger += e.getEmail();

			if (aktuelleAnzahl < maxAnzahlEmpfaengerJeMail - 1 && empfaenger.length() < maximaleLaengeEmpfaenger) {
				aktuelleAnzahl = gruppe.size();
			} else {
				result.add(gruppe);
				gruppe = new ArrayList<>();
				aktuelleAnzahl = 0;
				empfaenger = "";
			}

		}
		if (gruppe.size() > 0) {
			result.add(gruppe);
		}
		return result;
	}

	/**
	 * List den Inhalt der Datei pathMaildir/payload.dateinameInhalt.
	 *
	 * @param payload NewsletterPayload darf nicht null sein.
	 * @param pathMaildir String darf nicht null sein und muss auf ein reguläres lesbares Verzeichnis zeigen.
	 * @return String das, was in der Datei steht
	 * @throws NullPointerException wenn einer der Parameter null ist
	 * @throws IOException
	 */
	String getMailbody(final NewsletterPayload payload, final String pathMaildir) throws IOException {
		if (payload == null) {
			throw new NullPointerException("Parameter payload");
		}
		if (pathMaildir == null) {
			throw new NullPointerException("Parameter pathMaildir");
		}
		final String absPath = pathMaildir + "/" + payload.getDateinameInhalt();
		pathValidator.validate(absPath, new DateiValidationStrategie());
		try (InputStream in = new FileInputStream(new File(absPath)); StringWriter sw = new StringWriter()) {
			IOUtils.copy(in, sw, "UTF-8");
			final String result = sw.toString();
			LOG.debug(result);
			return result;
		}
	}

	/**
	 * List den Inhalt der Datei pathMaildir/payload.dateinameEmfaenger.
	 *
	 * @param payload NewsletterPayload darf nicht null sein.
	 * @param pathMaildir String darf nicht null sein und muss auf ein reguläres lesbares Verzeichnis zeigen.
	 * @return String das, was in der Datei steht
	 * @throws NullPointerException wenn einer der Parameter null ist
	 * @throws IOException
	 */
	List<IMailempfaenger> getMailempfaenger(final NewsletterPayload payload, final String pathMaildir) throws IOException {
		if (payload == null) {
			throw new NullPointerException("Parameter payload");
		}
		if (pathMaildir == null) {
			throw new NullPointerException("Parameter pathMaildir");
		}
		final String absPath = pathMaildir + "/" + payload.getDateinameEmfaenger();
		pathValidator.validate(absPath, new DateiValidationStrategie());
		final List<IMailempfaenger> result = new ArrayList<>();
		List<String> lines = null;

		try (FileInputStream fis = new FileInputStream(new File(absPath))) {
			lines = IOUtils.readLines(fis);
		}

		for (final String line : lines) {
			result.add(new Mailempfaenger(line));
		}
		return result;
	}

	void setSizeDBFieldEmpfaenger(final int sizeDBFieldEmpfaenger) {
		this.sizeDBFieldEmpfaenger = sizeDBFieldEmpfaenger;
	}
}
