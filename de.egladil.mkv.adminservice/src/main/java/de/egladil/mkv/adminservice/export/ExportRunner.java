//=====================================================
// Projekt: bv-export
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.bv.aas.storage.IBenutzerDao;
import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.mkv.adminservice.export.domain.BenutzerkontoResourceOwnerMapper;
import de.egladil.mkv.adminservice.export.domain.ResourceOwner;

/**
 * ExportRunner
 */
public class ExportRunner implements Consumer<ResourceOwner> {

	private static final Logger LOG = LoggerFactory.getLogger(ExportRunner.class.getName());

	private final IBenutzerDao benutzerDao;

	private final BenutzerkontoResourceOwnerMapper mapper;

	private final String pathExportRoot;

	private final String pathExportDir;

	private final ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Erzeugt eine Instanz von ExportRunner
	 */
	@Inject
	public ExportRunner(final IBenutzerDao benutzerDao, final BenutzerkontoResourceOwnerMapper mapper) {
		super();
		this.benutzerDao = benutzerDao;
		this.mapper = mapper;
		this.pathExportRoot = System.getProperty("user.home") + File.separator + "bv-migration";
		pathExportDir = pathExportRoot + File.separator + "export";
	}

	/**
	 *
	 * @param pathExportList
	 */
	public void startExport() {

		// uploadPath + File.separator + "uuid.lst"
		final String pathExportList = pathExportRoot + File.separator + "uuid.lst";

		final List<String> uuids = getUUIDs(pathExportList);

		LOG.info("exportiere {} Benutzer nach {}", uuids.size(), pathExportDir);

		// @formatter:off
		uuids.stream()
			.map(uuid -> {return benutzerDao.findBenutzerByUUID(uuid);})
			.filter((k) -> k != null && k.getRollen().size() == 1)
			.map(k -> mapper.apply(k))
			.filter(ro -> ro != null)
			.forEach(ro -> this.accept(ro));
		// @formatter:on
	}

	private List<String> getUUIDs(final String pathExportList) {

		try (InputStream in = new FileInputStream(new File(pathExportList))) {
			final List<String> result = IOUtils.readLines(in, Charset.forName("UTF-8"));

			return result;

		} catch (final IOException e) {
			throw new EgladilConfigurationException(e.getMessage(), e);
		}

	}

	@Override
	public void accept(final ResourceOwner resourceOwner) {

		final File file = new File(pathExportDir + File.separator + resourceOwner.getUuid() + ".json");
		try (OutputStream out = new FileOutputStream(file)) {
			final String str = objectMapper.writeValueAsString(resourceOwner);
			IOUtils.write(str, out, Charset.forName("UTF-8"));
			out.flush();

			LOG.info("{} exportiert", resourceOwner);
		} catch (final IOException e) {
			LOG.error("{} konnte nicht exportiert werden: {}", resourceOwner, e.getMessage());
		}

	}
}
