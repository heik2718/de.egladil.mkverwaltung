//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Consumer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.adminservice.download.FilesystemStreamingOutput;
import de.egladil.mkv.adminservice.exception.MkvAdminException;
import de.egladil.mkv.adminservice.payload.response.AuswertungstabelleInfo;
import de.egladil.mkv.adminservice.service.UploadsDelegate;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.auswertungen.files.impl.UploadUtils;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.request.DateinamePayload;
import de.egladil.mkv.persistence.payload.request.UuidPayload;
import de.egladil.mkv.persistence.plausi.ConvertToTeilnahmeartCommand;
import io.dropwizard.auth.Auth;

/**
 * UploadsResource wird AdminFileResource ablösen.
 */
@Singleton
@Path("/uploads")
@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class UploadsResource {

	private static final Logger LOG = LoggerFactory.getLogger(UploadsResource.class);

	private String uploadPath;

	private String sandboxPath;

	private ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final UploadsDelegate uploadsDelegate;

	private final ValidationDelegate validationDelegate;

	private final ResourceExceptionHandler exceptionHandler;

	/**
	 * UploadsResource
	 */
	@Inject
	public UploadsResource(final UploadsDelegate uploadsDelegate) {
		this.uploadsDelegate = uploadsDelegate;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
		this.validationDelegate = new ValidationDelegate();
	}

	/**
	 * Listet die AuswertungstabelleInfos aus dem uploadDir auf.
	 *
	 * @param principal
	 * @return
	 */
	@GET
	@Path("/contents")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response readUploadDirContents(@Auth
	final Principal principal) {

		try {
			final List<AuswertungstabelleInfo> payload = uploadsDelegate.getContentsUploadDir(uploadPath);
			final APIMessage msg = APIMessage.info(applicationMessages.getString("uploads.contents.success"));
			final APIResponsePayload entity = new APIResponsePayload(msg, payload);
			return Response.ok().entity(entity).build();
		} catch (Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@GET
	@Path("/{dateiname}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Timed
	public Response downloadFile(@Auth
	final Principal principal, @PathParam("dateiname")
	final String dateiname) {

		final DateinamePayload payload = new DateinamePayload(dateiname);
		final String benutzerUuid = principal.getName();
		try {

			validationDelegate.check(payload, DateinamePayload.class);
			final FilesystemStreamingOutput result = new FilesystemStreamingOutput(uploadPath, dateiname);
			final ContentDisposition contentDisposition = ContentDisposition.type("attachment").fileName(dateiname).build();
			final Response response = Response.ok(result).header("Content-Type", "application/octet-stream")
				.header("Content-Disposition", contentDisposition).build();
			return response;
		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final PreconditionFailedException e) {
			// ist schon als Ereignis protokolliert
			return exceptionHandler.mapToMKVApiResponse(e, e.getMessage(), payload);
		} catch (final Exception e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@PUT
	@Path("/{jahr}/{teilnahmeart}/{teilnahmekuerzel}/{checksumme}")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response uploadFile(@Auth
	final Principal principal, @PathParam("jahr")
	final String jahr, @PathParam("teilnahmeart")
	final String teilnahmeart, @PathParam("teilnahmekuerzel")
	final String teilnahmekuerzel, @PathParam("checksumme")
	final String checksumme, @FormDataParam("file")
	final InputStream inputStream, @FormDataParam("file")
	final FormDataContentDisposition fileDetails) {

		TeilnahmeIdentifier teilnahmeIdentifier = createTeilnahmeIdentifier(teilnahmeart, teilnahmekuerzel, jahr);
		// die Whitelist ist etwas umfangreicher als benötigt, aber ich baue hier nicht extra eine neue.
		DateinamePayload dateinamePayload = new DateinamePayload(checksumme);
		String benutzerUuid = principal.getName();

		try {
			final byte[] bytes = new UploadUtils().readBytes(inputStream);

			validationDelegate.check(teilnahmeIdentifier, TeilnahmeIdentifier.class);
			validationDelegate.check(dateinamePayload, DateinamePayload.class);

			List<AuswertungstabelleInfo> auswertungstabelleInfos = uploadsDelegate.updateUpload(benutzerUuid, bytes,
				teilnahmeIdentifier, checksumme, uploadPath, sandboxPath);

			final APIMessage msg = APIMessage.info(applicationMessages.getString("upload.file.success"));
			final APIResponsePayload entity = new APIResponsePayload(msg, auswertungstabelleInfos);
			return Response.ok().entity(entity).build();
		} catch (final EgladilWebappException e) {
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.error("Die Eingaben sind nicht korrekt."));
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "ungültiger PathParameter: " + teilnahmeIdentifier + ", checksumme='"
				+ checksumme + "'");
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (NullPointerException | IOException e) {
			final APIResponsePayload pm = new APIResponsePayload(
				APIMessage.error("Hochgeladene Daten fehlen oder waren irgendwie falsch."));
			LOG.error("Fehler beim Hochladen einer Auswertung mit {} und Checksumme={}: {}", teilnahmeIdentifier, checksumme,
				e.getMessage(), e);
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (Exception e) {
			final APIResponsePayload pm = new APIResponsePayload(
				APIMessage.error("Unerwartete Exception beim Hochladen einer Datei"));
			LOG.error("Fehler beim Hochladen einer Auswertung mit {} und Checksumme={}: {}", teilnahmeIdentifier, checksumme,
				e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(pm).build();
		}

	}

	@POST
	@Path("/{kontoUuid}")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response uploadFileFromMail(@Auth
	final Principal principal, @PathParam("kontoUuid")
	final String kontoUuid, @FormDataParam("file")
	final InputStream inputStream, @FormDataParam("file")
	final FormDataContentDisposition fileDetails) {

		String benutzerUuid = principal.getName();

		try {
			final byte[] bytes = new UploadUtils().readBytes(inputStream);

			validationDelegate.check(new UuidPayload(kontoUuid), UuidPayload.class);

			List<AuswertungstabelleInfo> auswertungstabelleInfos = uploadsDelegate.insertUpload(benutzerUuid, bytes, kontoUuid,
				uploadPath, sandboxPath);

			final APIMessage msg = APIMessage.info(applicationMessages.getString("upload.file.success"));
			final APIResponsePayload entity = new APIResponsePayload(msg, auswertungstabelleInfos);
			return Response.ok().entity(entity).build();
		} catch (final EgladilWebappException e) {
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.error("Die Eingaben sind nicht korrekt."));
			LOG.warn(GlobalConstants.LOG_PREFIX_BOT + "ungültiger PathParameter: kontoUuid='" + kontoUuid + "'");
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (MkvAdminException e) {
			final APIResponsePayload pm = new APIResponsePayload(APIMessage.error(e.getMessage()));
			LOG.error("Fehler beim Hochladen einer Auswertung mit Konto-UUID={}: {}", kontoUuid, e.getMessage());
			return Response.status(Status.PRECONDITION_FAILED).entity(pm).build();
		} catch (NullPointerException | IOException e) {
			final APIResponsePayload pm = new APIResponsePayload(
				APIMessage.error("Hochgeladene Daten fehlen oder waren irgendwie falsch."));
			LOG.error("Fehler beim Hochladen einer Auswertung mit Konto-UUID={}: {}", kontoUuid, e.getMessage(), e);
			return Response.status(Status.BAD_REQUEST).entity(pm).build();
		} catch (Exception e) {
			final APIResponsePayload pm = new APIResponsePayload(
				APIMessage.error("Unerwartete Exception beim Hochladen einer Datei"));
			LOG.error("Fehler beim Hochladen einer Auswertung mit Konto-UUID={}: {}", kontoUuid, e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(pm).build();
		}
	}

	@SuppressWarnings("unused")
	private Response createErrorResponseForTest(final String details) {
		final APIResponsePayload pm = new APIResponsePayload(APIMessage.error("Erzeugen mal eine Exception: " + details));
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(pm).build();
	}

	@OPTIONS
	@Path("/dateien")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response optionsDelete() {
		return Response.ok().build();
	}

	@DELETE
	@Path("/dateien")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response moveFiles(@Auth
	final Principal principal, final String[] dateinamen) {

		try {

			if (dateinamen.length > 500) {
				final ConstraintViolationMessage details = new ConstraintViolationMessage("mehr als 500 Dateinamen.");
				throw ExceptionFactory.validationErrors(details);
			}

			Arrays.stream(dateinamen).forEach(new Consumer<String>() {
				@Override
				public void accept(final String t) {
					validationDelegate.check(new DateinamePayload(t), DateinamePayload.class);
				}
			});

			List<AuswertungstabelleInfo> auswertungstabelleInfos = uploadsDelegate.verschiebeDateien(dateinamen, uploadPath);

			final APIMessage msg = APIMessage.info(applicationMessages.getString("move.files.ready"));
			final APIResponsePayload entity = new APIResponsePayload(msg, auswertungstabelleInfos);
			return Response.ok().entity(entity).build();

		} catch (Exception e) {
			final APIResponsePayload pm = new APIResponsePayload(
				APIMessage.error("Unerwartete Exception beim Hochladen einer Datei"));
			LOG.error("Fehler beim Verschieben von Dateien nach done: {}", e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(pm).build();
		}
	}

	private TeilnahmeIdentifier createTeilnahmeIdentifier(final String teilnahmeart, final String teilnahmekuerzel,
		final String jahr) {
		final ConvertToTeilnahmeartCommand cmd = new ConvertToTeilnahmeartCommand(teilnahmeart);
		cmd.execute();
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.create(cmd.getTeilnahmeart(), teilnahmekuerzel, jahr);
		return teilnahmeIdentifier;

	}

	public final void setUploadPath(final String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public void setSandboxPath(final String sandboxPath) {
		this.sandboxPath = sandboxPath;
	}

}
