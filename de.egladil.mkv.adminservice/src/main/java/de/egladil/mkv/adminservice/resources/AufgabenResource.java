//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import javax.ws.rs.Path;

import com.google.inject.Singleton;

/**
* AufgabenResource REST-API für aufgaben
*/
@Singleton
@Path("aufgaben")
public class AufgabenResource {

}
