//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.adminservice.service.LaenderService;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.payload.response.PublicLand;
import de.egladil.mkv.persistence.payload.response.compare.PublicLandNameComparator;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;

/**
 * LaenderServiceImpl
 */
@Singleton
public class LaenderServiceImpl extends BaseAuthorizedService implements LaenderService {

	private final ILandDao landDao;

	private final TeilnahmenFacade teilnahmenFacade;

	/**
	 * LaenderServiceImpl
	 */
	@Inject
	public LaenderServiceImpl(final ILandDao landDao, final TeilnahmenFacade teilnahmenFacade,
		final IBenutzerService benutzerService) {
		super(benutzerService);

		this.landDao = landDao;
		this.teilnahmenFacade = teilnahmenFacade;
	}

	@Override
	public List<PublicLand> getLaender() {

		final List<Land> laender = this.landDao.findAllFreigeschaltet();
		final List<PublicLand> result = new ArrayList<>();
		for (final Land land : laender) {
			final PublicLand pl = PublicLand.createLazy(land);
			pl.setAnzahlOrte(this.anzahlOrteInLand(land.getId()));
			result.add(pl);
		}

		Collections.sort(result, new PublicLandNameComparator());
		return result;
	}

	@Override
	public PublicLand getLand(final String kuerzel) {

		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}

		final Optional<Land> optLand = this.landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		if (!optLand.isPresent()) {
			return null;
		}
		final Land land = optLand.get();
		return enhanceLand(land);
	}

	@Override
	public PublicLand landAnlegen(final String kuerzel, final String name, final String benutzerUuid) {
		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}
		if (name == null) {
			throw new IllegalArgumentException("name null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}
		this.authorize(benutzerUuid);

		final Land land = new Land();
		land.setKuerzel(kuerzel);
		land.setName(name);
		land.setFreigeschaltet(true);
		try {
			final Land persisted = landDao.persist(land);
			final PublicLand pl = PublicLand.createLazy(persisted);
			pl.setAnzahlOrte(0);
			return pl;
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optEx = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optEx.isPresent()) {
				throw optEx.get();
			}
			throw new EgladilStorageException("Fehler in einer Datenbanktransaktion", e);
		}
	}

	private PublicLand enhanceLand(final Land source) {

		final String kuerzel = source.getKuerzel();
		final PublicLand land = new PublicLand(source.getName(), kuerzel);
		land.setAnzahlOrte(source.getOrte().size());

		final List<String> teilnahmejahre = this.teilnahmenFacade.findTeilnahmejahreLand(source.getKuerzel());
		land.addAllTeilnahmeJahre(teilnahmejahre);

		final HateoasPayload hateoasPayload = new HateoasPayload(kuerzel, "/laender/" + kuerzel);
		for (final String jahr : teilnahmejahre) {
			final HateoasLink link = new HateoasLink("/statistik/jahr/" + jahr, "Teilnahmen " + jahr, HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			hateoasPayload.addLink(link);
		}
		final HateoasLink link = new HateoasLink("/" + kuerzel + "/orte", "Orte", HttpMethod.GET, MediaType.APPLICATION_JSON);
		hateoasPayload.addLink(link);

		return land;
	}

	private int anzahlOrteInLand(final Long id) {

		final BigInteger anz = landDao.anzahlOrteInLand(id);
		if (anz != null) {
			return anz.intValue();
		}
		return 0;
	}
}
