//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.config;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mkv.persistence.domain.enums.Aufgabenkategorie;
import de.egladil.mkv.persistence.payload.response.AufgabenkategorieAngemessenheitsintervall;

/**
 * DynamicAdminConfig liest die Datei mkvadmin.json jedesmal neu aus.
 */
public class DynamicAdminConfig {

	private int maximaleTreffermengeSchulsuche;

	private int maximaleTreffermengeSchulenInOrt;

	private int maximaleTreffermengeOrtsuche;

	private int maximaleTreffermengeOrteInLand;

	private int maximaleTreffermengeBenutzer;

	private int maxExtractedBytes;

	private List<AufgabenkategorieAngemessenheitsintervall> angemessenheitsintervalle = new ArrayList<>();

	public final int getMaximaleTreffermengeSchulsuche() {
		return maximaleTreffermengeSchulsuche;
	}

	public final int getMaximaleTreffermengeSchulenInOrt() {
		return maximaleTreffermengeSchulenInOrt;
	}

	public final void setMaximaleTreffermengeSchulsuche(final int maximaleTreffermengeSchulsuche) {
		this.maximaleTreffermengeSchulsuche = maximaleTreffermengeSchulsuche;
	}

	public final void setMaximaleTreffermengeSchulenInOrt(final int maximaleTreffermengeSchulenInOrt) {
		this.maximaleTreffermengeSchulenInOrt = maximaleTreffermengeSchulenInOrt;
	}

	public final int getMaximaleTreffermengeBenutzer() {
		return maximaleTreffermengeBenutzer;
	}

	public final void setMaximaleTreffermengeBenutzer(final int maximaleTreffermengeBenutzer) {
		this.maximaleTreffermengeBenutzer = maximaleTreffermengeBenutzer;
	}

	/**
	 *
	 * @return
	 */
	public List<AufgabenkategorieAngemessenheitsintervall> getAngemessenheitsintervalle() {
		return angemessenheitsintervalle;
	}

	/**
	 * @param intervalle
	 */
	public void setAngemessenheitsintervalle(final List<AufgabenkategorieAngemessenheitsintervall> intervalle) {
		this.angemessenheitsintervalle = intervalle;
	}

	/**
	 *
	 * @param kategorie Kategorie
	 * @return AufgabenkategorieAngemessenheitsintervall oder null;
	 */
	public AufgabenkategorieAngemessenheitsintervall getAngemessenheitsintervall(final Aufgabenkategorie kategorie) {
		if (kategorie == null) {
			throw new IllegalArgumentException("kategorie null");
		}
		for (final AufgabenkategorieAngemessenheitsintervall intervall : this.angemessenheitsintervalle) {
			if (kategorie == intervall.getAufgabenkategorie()) {
				return intervall;
			}
		}
		return null;
	}

	public final int getMaximaleTreffermengeOrtsuche() {
		return maximaleTreffermengeOrtsuche;
	}

	public final void setMaximaleTreffermengeOrtsuche(final int maximaleTreffermengeOrtsuche) {
		this.maximaleTreffermengeOrtsuche = maximaleTreffermengeOrtsuche;
	}

	public final int getMaximaleTreffermengeOrteInLand() {
		return maximaleTreffermengeOrteInLand;
	}

	public final void setMaximaleTreffermengeOrteInLand(final int maximaleTreffermengeOrteInLand) {
		this.maximaleTreffermengeOrteInLand = maximaleTreffermengeOrteInLand;
	}

	public final int getMaxExtractedBytes() {
		return maxExtractedBytes;
	}

	public final void setMaxExtractedBytes(final int maxExtractedBytes) {
		this.maxExtractedBytes = maxExtractedBytes;
	}
}
