//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.adminservice.export.ExportRunner;

/**
 * ExportBenutzerResource
 */
@Singleton
@Path("/export")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class ExportBenutzerResource {

	private final ExportRunner exportRunner;

	/**
	 * Erzeugt eine Instanz von ExportBenutzerResource
	 */
	@Inject
	public ExportBenutzerResource(final ExportRunner exportRunner) {
		this.exportRunner = exportRunner;
	}

	@GET
	@Path("/benutzer")
//	public Response startExport(@Auth
//	final Principal principal) {
	public Response startExport() {

		exportRunner.startExport();

		return Response.ok().build();
	}
}
