//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.payload.response.PublicSchule;

/**
 * SchulsucheTreffer
 */
@Deprecated
public class SchulsucheTreffer implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private List<PublicSchule> schulen = new ArrayList<>();

	@JsonIgnore
	public void addSchulen(final List<Schule> trefferliste) throws IllegalArgumentException {
		if (trefferliste == null) {
			throw new IllegalArgumentException("trefferliste darf nichht null sein");
		}
		for (final Schule s : trefferliste) {
			final PublicSchule ps = PublicSchule.create(s);
			if (!schulen.contains(ps)) {
				schulen.add(ps);
			}
		}
	}

	@JsonIgnore
	public int size() {
		return schulen.size();
	}

	/**
	 * Liefert die Membervariable schulen
	 *
	 * @return die Membervariable schulen
	 */
	public List<PublicSchule> getSchulen() {
		return Collections.unmodifiableList(schulen);
	}

}
