//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.validation;

import java.io.Serializable;

import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.StringLatin;

/**
 * StringLatinPayload kapselt die Whitelist für Suchen in MKV-Admin.
 */
public class StringLatinPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@StringLatin
	@Size(max = 100)
	private final String code;

	/**
	 * Erzeugt eine Instanz von AdminSuchePayload
	 */
	public StringLatinPayload(final String code) {
		this.code = code;
	}

	/**
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return code;
	}
}
