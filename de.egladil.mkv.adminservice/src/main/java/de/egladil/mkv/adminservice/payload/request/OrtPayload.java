//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Kuerzel;

/**
 * OrtPayload zum Ändern des Ortes.
 */
public class OrtPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@Size(min = 8, max = 8)
	@Kuerzel
	private String ortkuerzel;

	/**
	 * Erzeugt eine Instanz von OrtPayload
	 */
	public OrtPayload() {
	}

	public OrtPayload(final String ortkuerzel, final String name) {
		this.name = name;
		this.ortkuerzel = ortkuerzel;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OrtPayload [name=" + name + ", ortkuerzel=" + ortkuerzel + "]";
	}

	/**
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return toString();
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Membervariable landkuerzel
	 *
	 * @return die Membervariable landkuerzel
	 */
	public String getOrtkuerzel() {
		return ortkuerzel;
	}
}
