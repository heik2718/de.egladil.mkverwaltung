//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.upload;

import java.util.List;

import de.egladil.mkv.persistence.domain.teilnahmen.Upload;

/**
 * UploadInfoPrinter
 */
public class UploadInfoPrinter {

	/**
	 * Serialisiert die Uploads.
	 *
	 * @param uploads List darf nicht null sein.
	 * @return String[]
	 * @throws NullPointerException wenn uploads null
	 */
	public String[] uploadsToStringArray(List<Upload> uploads) {
		if (uploads == null) {
			throw new NullPointerException();
		}
		String[] result = new String[uploads.size()];
		for (int i = 0; i < uploads.size(); i++) {
			result[i] = uploads.get(i).toString();
		}
		return result;
	}

}
