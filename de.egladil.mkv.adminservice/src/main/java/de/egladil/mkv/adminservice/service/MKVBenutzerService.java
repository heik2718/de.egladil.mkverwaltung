//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.util.List;

import de.egladil.bv.aas.domain.Benutzerkonto;

/**
 * MKVBenutzerService
 */
public interface MKVBenutzerService {

	/**
	 * Sucht alle Benutzerkonten, die zu Lehrer- oder Privatkonten mit vorname und/oder nachname passen. Die Suche
	 * erfolgt unscharf.
	 *
	 * @param name String darf nicht blank sein.
	 * @return List
	 */
	List<Benutzerkonto> findMKVBenutzerWithName(String name);

	/**
	 * Sucht alle Benutzer, deren UUID mit param beginnt.
	 *
	 * @param uuidPart String
	 * @return List
	 */
	List<Benutzerkonto> findMKVBenutzerWithId(String uuidPart);

}
