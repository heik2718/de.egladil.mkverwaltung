//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.health;

import com.codahale.metrics.health.HealthCheck;

import de.egladil.mkv.adminservice.resources.LaenderResource;

/**
 * KatalogHealthCheck
 */
public class LaenderResourceHealthCheck extends HealthCheck {

	private final LaenderResource laenderResource;

	/**
	 * Erzeugt eine Instanz von KatalogHealthCheck
	 */
	public LaenderResourceHealthCheck(final LaenderResource katalogResource) {
		this.laenderResource = katalogResource;
	}

	/**
	 * @see com.codahale.metrics.health.HealthCheck#check()
	 */
	@Override
	protected Result check() throws Exception {
		final int anzahlLaender = laenderResource.anzahlLaender();
		return anzahlLaender > 0 ? Result.healthy() : Result.unhealthy("MKV-Datenbank nicht erreichbar");
	}
}
