//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;

/**
 * RootgruppeResponsePayload
 */
public class RootgruppeResponsePayload implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private Auswertungsgruppe rootgruppe;

	@JsonProperty
	private SchulteilnahmeReadOnly schulteilnahme;

	@JsonProperty
	private boolean nameDiffersFromKatalog;

	@JsonProperty
	private long anzahlLoesungszettel;

	@JsonProperty
	private boolean auswertungsupload;

	public final Auswertungsgruppe getRootgruppe() {
		return rootgruppe;
	}

	public final void setRootgruppe(final Auswertungsgruppe auswertungsgruppe) {
		this.rootgruppe = auswertungsgruppe;
	}

	public final boolean isNameDiffersFromKatalog() {
		return nameDiffersFromKatalog;
	}

	public final void setNameDiffersFromKatalog(final boolean nameDiffersFromKatalog) {
		this.nameDiffersFromKatalog = nameDiffersFromKatalog;
	}

	public final void setSchulteilnahme(final SchulteilnahmeReadOnly schulteilnahme) {
		this.schulteilnahme = schulteilnahme;
	}

	public final SchulteilnahmeReadOnly getSchulteilnahme() {
		return schulteilnahme;
	}

	public final long getAnzahlLoesungszettel() {
		return anzahlLoesungszettel;
	}

	public final void setAnzahlLoesungszettel(final long anzahlLoesungszettel) {
		this.anzahlLoesungszettel = anzahlLoesungszettel;
	}

	public final boolean isAuswertungsupload() {
		return auswertungsupload;
	}

	public final void setAuswertungsupload(final boolean auswertungsupload) {
		this.auswertungsupload = auswertungsupload;
	}
}
