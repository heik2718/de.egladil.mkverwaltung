//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.service.OrtskatalogService;
import de.egladil.mkv.adminservice.service.SchulkatalogService;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.IOrtDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.payload.response.OrtLage;
import de.egladil.mkv.persistence.payload.response.PublicOrt;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.compare.PublicOrtNameComparator;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * OrtskatalogServiceImpl
 */
@Singleton
public class OrtskatalogServiceImpl extends BaseAuthorizedService implements OrtskatalogService {

	private final static Logger LOG = LoggerFactory.getLogger(OrtskatalogServiceImpl.class);

	private final IOrtDao ortDao;

	private final ILandDao landDao;

	private final SchulkatalogService schulkatalogService;

	private final KuerzelGenerator kuerzelGenerator;

	/**
	 * OrtskatalogServiceImpl
	 */
	@Inject
	public OrtskatalogServiceImpl(final IOrtDao ortDao, final ILandDao landDao, final SchulkatalogService schulkatalogService,
		final IBenutzerService benutzerService) {

		super(benutzerService);
		this.ortDao = ortDao;
		this.landDao = landDao;
		this.schulkatalogService = schulkatalogService;
		this.kuerzelGenerator = new KuerzelGenerator();
	}

	@Override
	public List<PublicOrt> findOrte(final String nameOrt, final String nameLand) {
		final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();

		if (StringUtils.isBlank(nameOrt)) {
			throw new IllegalArgumentException("nameOrt blank");
		}

		int maximaleTreffermenge = config.getMaximaleTreffermengeOrtsuche();
		if (StringUtils.isBlank(nameOrt)) {
			maximaleTreffermenge = config.getMaximaleTreffermengeOrteInLand();
		}
		final int anzahlTreffer = countOrte(nameOrt, nameLand);

		if (anzahlTreffer > maximaleTreffermenge) {
			LOG.warn("Ortsuche mit [name=" + nameOrt + ", land=" + nameLand + "]: Anzahl Treffer: {}, erlaubt: {}", anzahlTreffer,
				maximaleTreffermenge);
			throw new PreconditionFailedException("maximal " + maximaleTreffermenge + " Treffer erlaubt. Bitte Suche einschränken");
		}

		final List<Ort> orte = ortDao.findOrte(nameOrt, nameLand);
		final Map<Long, OrtLage> laender = getLaender(orte);

		final List<PublicOrt> result = new ArrayList<>();

		for (final Ort ort : orte) {
			final OrtLage lage = laender.get(ort.getLandId());
			final PublicOrt publicOrt = PublicOrt.createLazy(ort, lage);
			publicOrt.setAnzahlSchulen(schulkatalogService.anzahlSchulenInOrt(ort.getId()));
			result.add(publicOrt);
		}

		Collections.sort(result, new PublicOrtNameComparator());
		return result;
	}

	private final Map<Long, OrtLage> getLaender(final List<Ort> orte) {

		final Map<Long, OrtLage> laender = new HashMap<>();
		for (final Ort ort : orte) {
			final OrtLage lage = laender.get(ort.getLandId());
			if (lage == null) {
				final Land land = landDao.findById(Land.class, ort.getLandId()).get();
				laender.put(land.getId(), new OrtLage(land.getKuerzel(), land.getName()));
			}
		}

		return laender;
	}

	private int countOrte(final String name, final String land) {
		final BigInteger anzahl = ortDao.anzahlTreffer(name, land);
		return anzahl.intValue();
	}

	@Override
	public PublicOrt getOrt(final String kuerzel) {

		final Optional<Ort> optOrt = ortDao.findByUniqueKey(Ort.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);
		if (!optOrt.isPresent()) {
			return null;
		}

		final Ort ort = optOrt.get();
		final OrtLage ortLage = ortDao.getOrtLage(kuerzel);
		final PublicOrt po = PublicOrt.createLazy(ort, ortLage);
		for (final Schule schule : ort.getAllSchulen()) {
			final PublicSchule ps = schulkatalogService.getSchule(schule.getKuerzel());
			po.addKind(ps);
		}
		po.setAnzahlSchulen(po.getKinder().size());
		return po;
	}

	@Override
	public PublicOrt ortAnlegen(final String kuerzelLand, final String name, final String benutzerUuid)
		throws EgladilAuthorizationException, EgladilDuplicateEntryException {

		if (kuerzelLand == null) {
			throw new IllegalArgumentException("kuerzelLand null");
		}
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("name blank");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}

		authorize(benutzerUuid);

		final Optional<Land> optLand = landDao.findByUniqueKey(Land.class, MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzelLand);
		if (!optLand.isPresent()) {
			throw new ResourceNotFoundException("Kein Land mit [kuerzel=" + kuerzelLand + "] gefunden");
		}
		final Land land = optLand.get();
		final String kuerzel = kuerzelGenerator.generateDefaultKuerzel();
		final Ort ort = new Ort();
		ort.setKuerzel(kuerzel);
		ort.setName(name);
		land.addOrt(ort);

		try {
			landDao.persist(land);

			final OrtLage ortLage = new OrtLage(kuerzelLand, land.getName());
			final PublicOrt po = PublicOrt.createLazy(ort, ortLage);

			return po;

		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optEx = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optEx.isPresent()) {
				throw optEx.get();
			}
			throw new EgladilStorageException("Fehler in einer Transaktion: " + e.getMessage(), e);
		}
	}
}
