//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

/**
 * BenutzerSuchmodus
 */
public enum BenutzerSuchmodus {

	EMAIL,
	NAME,
	ID

}
