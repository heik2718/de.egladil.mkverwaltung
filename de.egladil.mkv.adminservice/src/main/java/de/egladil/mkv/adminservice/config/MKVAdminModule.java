//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenAuthenticator;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenDao;
import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.bv.aas.impl.AuthenticationServiceImpl;
import de.egladil.bv.aas.impl.BenutzerServiceImpl;
import de.egladil.bv.aas.impl.RegistrierungServiceImpl;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.impl.MailserviceImpl;
import de.egladil.mkv.adminservice.service.LaenderService;
import de.egladil.mkv.adminservice.service.MKVBenutzerService;
import de.egladil.mkv.adminservice.service.OrtskatalogService;
import de.egladil.mkv.adminservice.service.SchulkatalogService;
import de.egladil.mkv.adminservice.service.UploadsDelegate;
import de.egladil.mkv.adminservice.service.impl.LaenderServiceImpl;
import de.egladil.mkv.adminservice.service.impl.MKVBenutzerServiceImpl;
import de.egladil.mkv.adminservice.service.impl.OrtskatalogServiceImpl;
import de.egladil.mkv.adminservice.service.impl.SchulkatalogServiceImpl;
import de.egladil.mkv.adminservice.service.impl.UploadsDelegateImpl;
import de.egladil.mkv.auswertungen.persistence.IUploadListener;
import de.egladil.mkv.auswertungen.persistence.SyncUploadListener;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.auswertungen.statistik.impl.SchulstatistikServiceImpl;
import de.egladil.mkv.auswertungen.statistik.impl.VerteilungGeneratorImpl;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.auswertungen.urkunden.impl.AuswertungServiceImpl;
import de.egladil.mkv.persistence.config.MKVPersistenceModule;
import de.egladil.mkv.persistence.kataloge.IKatalogService;
import de.egladil.mkv.persistence.kataloge.impl.KatalogService;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.mkv.persistence.service.TeilnehmerService;
import de.egladil.mkv.persistence.service.impl.AdvFacadeImpl;
import de.egladil.mkv.persistence.service.impl.Anonymsierungsservice;
import de.egladil.mkv.persistence.service.impl.AuswertungsgruppenServiceImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerFacadeImpl;
import de.egladil.mkv.persistence.service.impl.TeilnehmerServiceImpl;
import io.dropwizard.auth.Authenticator;

/**
 * MKVModule
 */
public class MKVAdminModule extends AbstractModule {

	// private final MKVAdminServiceConfiguration configuration;

	private final String pathConfigRoot;

	/**
	 * Erzeugt eine Instanz von MKVAdminModule
	 */
	public MKVAdminModule(final String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());

		install(new BVPersistenceModule(pathConfigRoot));
		install(new MKVPersistenceModule(pathConfigRoot));

		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class);
		bind(IBenutzerService.class).to(BenutzerServiceImpl.class);
		bind(IRegistrierungService.class).to(RegistrierungServiceImpl.class);
		bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);

		bind(IAccessTokenDAO.class).to(CachingAccessTokenDao.class);

		bind(IEgladilConfiguration.class).to(EgladilMKVAdminConfiguration.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);

		bind(IKatalogService.class).to(KatalogService.class);
		bind(IProtokollService.class).to(ProtokollService.class);

		bind(IAnonymisierungsservice.class).to(Anonymsierungsservice.class);

		bind(IVerteilungGenerator.class).to(VerteilungGeneratorImpl.class);
		bind(AuswertungsgruppenService.class).to(AuswertungsgruppenServiceImpl.class);
		bind(TeilnehmerService.class).to(TeilnehmerServiceImpl.class);

		bind(IUploadListener.class).to(SyncUploadListener.class);
		bind(IMailservice.class).to(MailserviceImpl.class);



		bind(SchulstatistikService.class).to(SchulstatistikServiceImpl.class);
		bind(UploadsDelegate.class).to(UploadsDelegateImpl.class);

		bind(AdvFacade.class).to(AdvFacadeImpl.class);
		bind(TeilnehmerFacade.class).to(TeilnehmerFacadeImpl.class);

		bind(SchulkatalogService.class).to(SchulkatalogServiceImpl.class);
		bind(LaenderService.class).to(LaenderServiceImpl.class);
		bind(AuswertungService.class).to(AuswertungServiceImpl.class);
		bind(MKVBenutzerService.class).to(MKVBenutzerServiceImpl.class);
		bind(OrtskatalogService.class).to(OrtskatalogServiceImpl.class);

		// bind(BVHealthCheck.class);
	}

	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
