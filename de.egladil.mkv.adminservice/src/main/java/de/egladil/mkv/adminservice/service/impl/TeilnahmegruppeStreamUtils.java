//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmegruppe;

/**
 * TeilnahmegruppeStreamUtils
 */
public class TeilnahmegruppeStreamUtils {

	/**
	 *
	 * @param gruppen
	 * @return
	 */
	public static AggregierteTeilnahmegruppenInfo aggregiereInfos(final List<Teilnahmegruppe> gruppen) {
		long anzahlSchulteilnahmen = 0;
		long anzahlKinder = 0;
		String kuerzel = "";

		for (int i = 0; i < gruppen.size(); i++) {
			final Teilnahmegruppe gruppe = gruppen.get(i);
			anzahlSchulteilnahmen += gruppe.getAnzahlAnmeldungen();
			anzahlKinder += gruppe.getAnzahlKinder();
			kuerzel += gruppe.getKuerzel();
			if (i < gruppen.size() - 1) {
				kuerzel += ",";

			}
		}

		return new AggregierteTeilnahmegruppenInfo(anzahlSchulteilnahmen, anzahlKinder, kuerzel);
	}

	/**
	 * Erzeugt einen kommaseparierten String aus allen kuerzel-Attributen der Gruppen.
	 *
	 * @param gruppen List
	 * @return String
	 */
	public static String getAggregierteKuerzel(final List<Teilnahmegruppe> gruppen) {
		final Collector<Teilnahmegruppe, StringJoiner, String> kuerzelCollector = Collector.of(() -> new StringJoiner(","), // supplier
			(j, p) -> j.add(p.getKuerzel()), // accumulator
			(j1, j2) -> j1.merge(j2), // combiner
			StringJoiner::toString); // finisher
		final String kuerzel = gruppen.stream().collect(kuerzelCollector);
		return kuerzel;
	}

	/**
	 * Addiert alle Schulteilnahmen der Gruppen.
	 *
	 * @param gruppen List
	 * @return long
	 */
	public static long sumSchulteilnahmen(final List<Teilnahmegruppe> gruppen) {
		return gruppen.stream().collect(Collectors.summarizingLong(p -> p.getAnzahlAnmeldungen())).getSum();
	}

	/**
	 * Addiert alle Kinder der Gruppen.
	 *
	 * @param gruppen List
	 * @return long
	 */
	public static long sumAnzahlKinder(final List<Teilnahmegruppe> gruppen) {
		return gruppen.stream().collect(Collectors.summarizingLong(p -> p.getAnzahlKinder())).getSum();
	}

	/**
	 * Gibt die Liste aller Teilnahmejahre dieser Gruppen zurück.
	 *
	 * @param gruppen List
	 * @return List
	 */
	public static List<String> collectTeilnahmejahre(final List<Teilnahmegruppe> gruppen) {
		final Set<String> distinctJahre = new HashSet<>();
		gruppen.stream().map(g -> g.getTeilnahmejahre()).map(l -> distinctJahre.addAll(l));
		final List<String> result = new ArrayList<>(distinctJahre.size());
		result.addAll(distinctJahre);
		Collections.sort(result);
		return result;
	}
}
