//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;

/**
 * @author heikew
 *
 */
@Singleton
public class EgladilMKVAdminConfiguration extends AbstractEgladilConfiguration implements IEgladilConfiguration {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilMKVConfiguration
	 */
	@Inject
	public EgladilMKVAdminConfiguration(@Named("configRoot") final String pathConfigRoot) {
		super(pathConfigRoot);
	}

	@Override
	protected String getConfigFileName() {
		return "mkv_admin.properties";
	}
}
