package de.egladil.mkv.adminservice.health;

import javax.ws.rs.core.Response;

import com.codahale.metrics.health.HealthCheck;

import de.egladil.bv.aas.domain.MessageEntity;
import de.egladil.common.validation.validators.UuidStringValidator;
import de.egladil.mkv.adminservice.resources.PingResource;

public class PingHealthCheck extends HealthCheck {

	private final PingResource pingResource;

	private final UuidStringValidator uuidValidator;

	/**
	 * Erzeugt eine Instanz von PingHealthCheck
	 */
	public PingHealthCheck(PingResource pingResource) {
		this.pingResource = pingResource;
		uuidValidator = new UuidStringValidator();
	}

	@Override
	protected Result check() throws Exception {
		Response response = pingResource.getTemporarySession();
		if (response.getStatus() == 200) {
			MessageEntity messageEntity = (MessageEntity) response.getEntity();
			return uuidValidator.isValid(messageEntity.getMessage(), null) ? Result.healthy()
				: Result.unhealthy("Ping result enthält kein reguläres csrf-Token");
		}
		return Result.unhealthy("erwarten Statuscode 200, war aber " + response.getStatus());
	}
}
