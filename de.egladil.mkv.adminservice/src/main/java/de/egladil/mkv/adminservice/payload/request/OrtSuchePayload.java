//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.LandKuerzel;

/**
 * OrtSuchePayload
 */
public class OrtSuchePayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 5)
	@LandKuerzel
	private String landkuerzel;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@Honeypot
	private String kleber;

	/**
	 * Erzeugt eine Instanz von OrtSuchePayload
	 */
	public OrtSuchePayload() {
	}

	/**
	 * Erzeugt eine Instanz von OrtSuchePayload
	 */
	public OrtSuchePayload(final String landkuerzel, final String name) {
		this.landkuerzel = landkuerzel;
		this.name = name;
	}

	/**
	 *
	 * @see de.egladil.common.persistence.ILoggable#toBotLog()
	 */
	@Override
	public String toBotLog() {
		return "OrtSuchePayload [landkuerzel=" + landkuerzel + ", name=" + name + ", kleber=" + kleber + "]";
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

	/**
	 * Liefert die Membervariable landkuerzel
	 *
	 * @return die Membervariable landkuerzel
	 */
	public String getLandkuerzel() {
		return landkuerzel;
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

}
