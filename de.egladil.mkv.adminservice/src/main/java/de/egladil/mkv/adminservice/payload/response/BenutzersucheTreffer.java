//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * BenutzersucheTreffer
 */
public class BenutzersucheTreffer {

	private List<PublicBenutzer> alleBenutzer = new ArrayList<>();

	public void addBenutzer(PublicBenutzer benutzer) {
		if (!alleBenutzer.contains(benutzer)) {
			alleBenutzer.add(benutzer);
		}
	}

	/**
	 *
	 * @return unmodifiable List
	 */
	public List<PublicBenutzer> getAlleBenutzer() {
		return Collections.unmodifiableList(alleBenutzer);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("BenutzersucheTreffer\n");
		for (PublicBenutzer b : alleBenutzer){
			sb.append(b.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
}
