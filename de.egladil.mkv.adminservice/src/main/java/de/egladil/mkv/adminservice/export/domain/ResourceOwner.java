//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.export.domain;

/**
* ResourceOwner
*/
public class ResourceOwner {

	private String uuid;

	private String loginName;

	private String vorname;

	private String nachname;

	private String email;

	private boolean aktiviert;

	private boolean anonym;

	private int anzahlLogins;

	private String roles;

	private Credentials loginSecrets;

	/**
	* Erzeugt eine Instanz von ResourceOwner
	*/
	public ResourceOwner() {
		// TODO Generierter Code

	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(final String loginName) {
		this.loginName = loginName;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(final String nachname) {
		this.nachname = nachname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public boolean isAktiviert() {
		return aktiviert;
	}

	public void setAktiviert(final boolean aktiviert) {
		this.aktiviert = aktiviert;
	}

	public boolean isAnonym() {
		return anonym;
	}

	public void setAnonym(final boolean anonym) {
		this.anonym = anonym;
	}

	public int getAnzahlLogins() {
		return anzahlLogins;
	}

	public void setAnzahlLogins(final int anzahlLogins) {
		this.anzahlLogins = anzahlLogins;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(final String roles) {
		this.roles = roles;
	}

	public Credentials getLoginSecrets() {
		return loginSecrets;
	}

	public void setLoginSecrets(final Credentials loginSecrets) {
		this.loginSecrets = loginSecrets;
	}

	@Override
	public String toString() {
		return "ResourceOwner [email=" + email + ", uuid=" + uuid + "]";
	}

}
