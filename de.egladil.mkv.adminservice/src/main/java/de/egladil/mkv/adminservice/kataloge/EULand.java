//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.kataloge;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * GenLand
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EULand {

	@XmlElement(name = "name")
	private String name;

	@XmlElement(name = "url")
	private String url;

	@XmlElement(name = "ort")
	private List<EUOrt> orte = new ArrayList<>();

	public void addOrt(EUOrt ort) {
		if (!orte.contains(ort)) {
			orte.add(ort);
			ort.setLand(this);
		}
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param name neuer Wert der Membervariablen name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Liefert die Membervariable url
	 *
	 * @return die Membervariable url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param url neuer Wert der Membervariablen url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
