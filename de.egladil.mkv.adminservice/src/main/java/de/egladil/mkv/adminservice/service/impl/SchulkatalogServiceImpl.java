//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.service.SchulkatalogService;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.KatalogDaoProvider;
import de.egladil.mkv.persistence.domain.adv.AdvVereinbarung;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.domain.kataloge.SchuleReadOnly;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.payload.request.GeaenderteSchulePayload;
import de.egladil.mkv.persistence.payload.request.NeueSchulePayload;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;
import de.egladil.mkv.persistence.payload.response.compare.PublicSchuleNameComparator;
import de.egladil.mkv.persistence.service.AdvFacade;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * SchulkatalogServiceImpl
 */
@Singleton
public class SchulkatalogServiceImpl extends BaseAuthorizedService implements SchulkatalogService {

	private static final Logger LOG = LoggerFactory.getLogger(SchulkatalogServiceImpl.class);

	private final KatalogDaoProvider katalogDaoProvider;

	private final AdvFacade advFacade;

	private final TeilnahmenFacade teilnahmenFacade;

	private final AuswertungsgruppenService auswertungsgruppenService;

	private final KuerzelGenerator kuerzelGenerator = new KuerzelGenerator();

	/**
	 * SchulkatalogServiceImpl
	 */
	@Inject
	public SchulkatalogServiceImpl(final KatalogDaoProvider katalogDaoProviderImpl, final AdvFacade advFacade,
		final TeilnahmenFacade teilnahmenFacade, final AuswertungsgruppenService auswertungsgruppenService,
		final IBenutzerService benutzerService) {

		super(benutzerService);
		this.katalogDaoProvider = katalogDaoProviderImpl;
		this.advFacade = advFacade;
		this.teilnahmenFacade = teilnahmenFacade;
		this.auswertungsgruppenService = auswertungsgruppenService;
	}

	@Override
	public List<PublicSchule> findSchulen(final String name, final String ort) {

		final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();
		int maximaleTreffermenge = config.getMaximaleTreffermengeSchulsuche();
		if (StringUtils.isBlank(name)) {
			maximaleTreffermenge = config.getMaximaleTreffermengeSchulenInOrt();
		}
		final int anzahlTreffer = countSchulen(name, ort);

		if (anzahlTreffer > maximaleTreffermenge) {
			LOG.warn("Schulsuche mit [name=" + name + ", ort=" + ort + "]: Anzahl Treffer: {}, erlaubt: {}", anzahlTreffer,
				maximaleTreffermenge);
			throw new PreconditionFailedException("maximal " + maximaleTreffermenge + " Treffer erlaubt. Bitte Suche einschränken");
		}

		final List<SchuleReadOnly> schulen = katalogDaoProvider.getSchuleReadOnlyDao().findSchulen(name, ort);

		final List<PublicSchule> result = new ArrayList<>();

		for (final SchuleReadOnly schule : schulen) {
			final PublicSchule ps = PublicSchule.create(schule);
			final int anzahlTeilnahmen = teilnahmenFacade.getAnzahlSchulteilnahmen(schule.getKuerzel());
			ps.setAnzahlTeilnahmen(anzahlTeilnahmen);
			result.add(ps);
		}

		Collections.sort(result, new PublicSchuleNameComparator());
		return result;
	}

	@Override
	public PublicSchule getSchule(final String kuerzel) {

		final String wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();

		final Optional<SchuleReadOnly> optSchule = katalogDaoProvider.getSchuleReadOnlyDao().findByUniqueKey(SchuleReadOnly.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		if (!optSchule.isPresent()) {
			return null;
		}

		final SchuleReadOnly schule = optSchule.get();

		final PublicSchule ps = PublicSchule.create(schule);
		enhanceSchule(wettbewerbsjahr, ps);
		return ps;
	}

	/**
	 * @param kuerzel
	 * @param wettbewerbsjahr
	 * @param ps
	 */

	private void enhanceSchule(final String wettbewerbsjahr, final PublicSchule ps) {

		final String kuerzel = ps.getKuerzel();
		final Optional<AdvVereinbarung> optAdv = advFacade.findAdvVereinbarungFuerSchule(kuerzel);
		if (optAdv.isPresent()) {
			final HateoasLink link = new HateoasLink("/advvereinbarungen/" + kuerzel, "Vereinbarung ADV", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			ps.addHateoasLink(link);
		}
		final List<SchulteilnahmeReadOnly> teilnahmen = teilnahmenFacade.getSchulteilnahmen(kuerzel);
		ps.setAnzahlTeilnahmen(teilnahmen.size());
		if (!teilnahmen.isEmpty()) {
			final HateoasLink link = new HateoasLink("/schulen/teilnahmen/" + kuerzel, "Teilnahmen", HttpMethod.GET,
				MediaType.APPLICATION_JSON);
			ps.addHateoasLink(link);
		}
		final TeilnahmeIdentifier teilnahmeIdentifier = TeilnahmeIdentifier.create(Teilnahmeart.S, kuerzel, wettbewerbsjahr);
		final Optional<Auswertungsgruppe> optRootgruppe = this.auswertungsgruppenService
			.findRootauswertungsgruppe(teilnahmeIdentifier);

		if (optRootgruppe.isPresent()) {
			final HateoasLink link = new HateoasLink("/auswertungsgruppen/" + optRootgruppe.get().getKuerzel(),
				"Rootauswertungsgruppe", HttpMethod.GET, MediaType.APPLICATION_JSON);
			ps.addHateoasLink(link);
		}
	}

	@Override
	public PublicSchule schuleAnlegen(final NeueSchulePayload payload, final String benutzerUuid) {

		if (payload == null) {
			throw new IllegalArgumentException("payload null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}

		authorize(benutzerUuid);

		final Optional<Land> optLand = katalogDaoProvider.getLandDao().findByUniqueKey(Land.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, payload.getLandkuerzel());
		if (!optLand.isPresent()) {
			throw new ResourceNotFoundException("landkuerzel=" + payload.getLandkuerzel());
		}
		final Land land = optLand.get();
		final Optional<Ort> optOrt = land.findOrt(payload.getOrtkuerzel());
		if (!optOrt.isPresent()) {
			throw new ResourceNotFoundException("ortkuerzel=" + payload.getOrtkuerzel());
		}
		final Ort ort = optOrt.get();
		final String kuerzel = kuerzelGenerator.generateDefaultKuerzel();
		final Schule schule = new Schule();
		schule.setKuerzel(kuerzel);
		schule.setName(payload.getName());
		schule.setUrl(payload.getUrl());
		schule.setStrasse(payload.getStrasse());

		ort.addSchule(schule);

		try {
			katalogDaoProvider.getOrtDao().persist(ort);

			final SchuleLage lage = new SchuleLage(land.getKuerzel(), land.getName(), ort.getKuerzel(), ort.getName());

			return PublicSchule.create(schule, lage);
		} catch (final PersistenceException e) {
			final Optional<EgladilDuplicateEntryException> optEx = PersistenceExceptionMapper.toEgladilDuplicateEntryException(e);
			if (optEx.isPresent()) {
				throw optEx.get();
			}
			throw new EgladilStorageException("Fehler in einer Transaktion: " + e.getMessage(), e);
		}
	}

	@Override
	public PublicSchule schuleAendern(final String kuerzel, final GeaenderteSchulePayload payload, final String benutzerUuid) {

		if (kuerzel == null) {
			throw new IllegalArgumentException("kuerzel null");
		}
		if (payload == null) {
			throw new IllegalArgumentException("payload null");
		}
		if (benutzerUuid == null) {
			throw new IllegalArgumentException("benutzerUuid null");
		}

		authorize(benutzerUuid);

		final Optional<Schule> optSchule = katalogDaoProvider.getSchuleDao().findByUniqueKey(Schule.class,
			MKVConstants.KUERZEL_ATTRIBUTE_NAME, kuerzel);

		if (!optSchule.isPresent()) {
			throw new ResourceNotFoundException("Schule mit Kürzel '" + kuerzel + "'");
		}

		final Schule schule = optSchule.get();
		schule.setName(payload.getName());
		schule.setStrasse(payload.getStrasse());
		schule.setUrl(payload.getUrl());

		final Schule persisted = katalogDaoProvider.getSchuleDao().persist(schule);
		final SchuleLage lage = katalogDaoProvider.getSchuleDao().getSchuleLage(kuerzel);

		final PublicSchule ps = PublicSchule.create(persisted, lage);
		ps.setAnzahlTeilnahmen(teilnahmenFacade.getAnzahlSchulteilnahmen(kuerzel));
		return ps;
	}

	@Override
	public int anzahlSchulenInOrt(final Long ortId) {
		final BigInteger anzahl = katalogDaoProvider.getSchuleReadOnlyDao().anzahlSchulenInOrt(ortId);
		return anzahl.intValue();
	}

	private int countSchulen(final String name, final String ort) {
		final BigInteger anzahl = katalogDaoProvider.getSchuleReadOnlyDao().anzahlTreffer(name, ort);
		return anzahl.intValue();
	}

	@Override
	public List<String> findTeilnahmejahreLand(final String landkuerzel) {
		return null;
	}
}
