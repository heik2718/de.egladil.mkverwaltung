//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import io.dropwizard.auth.Auth;

/**
 * InitDownloadcodesResource
 */
@Singleton
@Produces("application/text")
@Path("/downloads")
public class InitDownloadcodesResource {

	private static final Logger LOG = LoggerFactory.getLogger(InitDownloadcodesResource.class);

	private final ILehrerkontoDao lehrerkontoDao;

	private final IPrivatkontoDao privatkontoDao;

	/**
	 * Erzeugt eine Instanz von InitDownloadcodesResource
	 */
	@Inject
	public InitDownloadcodesResource(final ILehrerkontoDao lehrerkontoDao, final IPrivatkontoDao privatkontoDao) {
		this.lehrerkontoDao = lehrerkontoDao;
		this.privatkontoDao = privatkontoDao;
	}

	@POST
	@Timed
	public Response createDownloadcodes(@Auth final Principal principal) {
		try {
//			updateLehrerkonten();
//			updatePrivatkonten();
			return Response.ok().entity("erfolgreich").build();
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return Response.serverError().entity(e.getMessage()).build();
		}
	}

//	private void updateLehrerkonten() {
//		final List<Lehrerkonto> lehrerkonten = lehrerkontoDao.findAll(Lehrerkonto.class);
//		for (final Lehrerkonto k : lehrerkonten) {
//			if (k.getDownloadcode() == null) {
//				k.setDownloadcode(UUID.randomUUID().toString());
//				lehrerkontoDao.persist(k);
//				pause(150);
//			}
//		}
//	}
//
//	private void updatePrivatkonten() {
//		final List<Privatkonto> privatkonten = privatkontoDao.findAll(Privatkonto.class);
//		for (final Privatkonto k : privatkonten) {
//			if (k.getDownloadcode() == null) {
//				k.setDownloadcode(UUID.randomUUID().toString());
//				privatkontoDao.persist(k);
//				pause(200);
//			}
//		}
//	}

	private void pause(final long millis) {
		try {
			Thread.sleep(millis);
		} catch (final InterruptedException e) {
			//
		}
	}
}
