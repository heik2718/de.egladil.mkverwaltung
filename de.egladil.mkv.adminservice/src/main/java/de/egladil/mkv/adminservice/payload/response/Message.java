//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.egladil.common.webapp.MessageLevel;

/**
 * Message
 */
@XmlRootElement(name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {

	@XmlElement
	private String content;

	@XmlElement
	private MessageLevel level;

	/**
	 * Erzeugt eine Instanz von Message
	 */
	Message() {
	}

	/**
	 * Erzeugt eine Instanz von Message
	 */
	public Message(String content, MessageLevel level) {
		this.content = content;
		this.level = level;
	}
}
