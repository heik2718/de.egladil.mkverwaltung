//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;

/**
 * AuswertungstabelleDateiInfo sind die Daten, die eine Datei aus dem Filesystem mitteilen kann.
 */
public class AuswertungstabelleDateiInfo {

	@JsonProperty
	private String dateiname;

	@JsonProperty
	private String teilnahmekuerzel;

	@JsonProperty
	private Teilnahmeart teilnahmeart;

	@JsonProperty
	private UploadStatus uploadStatus;

	@JsonProperty
	private String groesse;

	public String getDateiname() {
		return dateiname;
	}

	public void setDateiname(final String dateiname) {
		this.dateiname = dateiname;
	}

	public String getTeilnahmekuerzel() {
		return teilnahmekuerzel;
	}

	public void setTeilnahmekuerzel(final String teilnahmekuerzel) {
		this.teilnahmekuerzel = teilnahmekuerzel;
	}

	public Teilnahmeart getTeilnahmeart() {
		return teilnahmeart;
	}

	public void setTeilnahmeart(final Teilnahmeart teilnahmeart) {
		this.teilnahmeart = teilnahmeart;
	}

	public UploadStatus getUploadStatus() {
		return uploadStatus;
	}

	public void setUploadStatus(final UploadStatus uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	public String getGroesse() {
		return groesse;
	}

	public void setGroesse(final String groesse) {
		this.groesse = groesse;
	}

}
