//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.adminservice.payload.response.RootgruppeResponsePayload;
import de.egladil.mkv.persistence.config.MKVConstants;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.teilnahmen.PrivatteilnahmeInformation;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.payload.response.compare.TeilnahmegruppenNameComparator;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Filterart;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmegruppe;
import de.egladil.mkv.persistence.payload.response.teilnahmen.TeilnahmenFilter;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * TeilnahmegruppenService ist ein Delegate um Dinge für Teilnahmegruppen zusammenzutragen.
 */
@Singleton
public class TeilnahmegruppenService {

	private static final Logger LOG = LoggerFactory.getLogger(TeilnahmegruppenService.class);

	private final ILandDao landDao;

	private final TeilnahmenFacade teilnahmenFacade;

	private final AuswertungsgruppenService auswertungsgruppenService;

	private final TeilnehmerFacade teilnehmerFacade;

	private final ISchulteilnahmeReadOnlyDao schulteilnahmeReadOnlyDao;

	/**
	 * TeilnahmegruppenService
	 */
	@Inject
	public TeilnahmegruppenService(final AuswertungsgruppenService auswertungsgruppenService,
		final ISchulteilnahmeReadOnlyDao schulteilnahmeDao, final TeilnehmerFacade teilnehmerFacade, final ILandDao landDao,
		final TeilnahmenFacade teilnahmenFacade) {
		this.auswertungsgruppenService = auswertungsgruppenService;
		this.schulteilnahmeReadOnlyDao = schulteilnahmeDao;
		this.teilnehmerFacade = teilnehmerFacade;
		this.landDao = landDao;
		this.teilnahmenFacade = teilnahmenFacade;
	}

	/**
	 * Gibt die Schulen aus der Tabelle auswertungsgruppen zurück.
	 *
	 * @param jahr int das Jahr.
	 * @return List
	 */
	public List<RootgruppeResponsePayload> getRootauswertungsgruppen(final int jahr) {

		final List<RootgruppeResponsePayload> result = new ArrayList<>();
		final String jahrAsString = "" + jahr;
		final List<Auswertungsgruppe> auswertungsgruppen = auswertungsgruppenService.getRootAuswertungsgruppen(jahrAsString);
		int anzahlGeaendert = 0;

		for (final Auswertungsgruppe g : auswertungsgruppen) {

			final RootgruppeResponsePayload payload = new RootgruppeResponsePayload();
			payload.setRootgruppe(g);

			final Optional<SchulteilnahmeReadOnly> optSchulteilnahme = schulteilnahmeReadOnlyDao
				.findBySchulkuerzelUndJahr(g.getTeilnahmekuerzel(), jahrAsString);

			if (optSchulteilnahme.isPresent()) {
				final SchulteilnahmeReadOnly schulteilnahme = optSchulteilnahme.get();
				payload.setSchulteilnahme(schulteilnahme);
				if (!g.getName().trim().equals(schulteilnahme.getSchule())) {
					payload.setNameDiffersFromKatalog(true);
					LOG.info("Name Auswertungsgruppe='{}', Name Schulkatalog='{}'", g.getName().trim(), schulteilnahme.getSchule());
					anzahlGeaendert++;
				}

				final long anzahlLoesungszettel = teilnehmerFacade.getAnzahlLoesungszettel(schulteilnahme);

				if (anzahlLoesungszettel > 0l) {
					final long anzahlTeilnehmer = teilnehmerFacade.getAnzahlTeilnehmer(schulteilnahme);
					payload.setAuswertungsupload(anzahlTeilnehmer == 0l);
				}
				payload.setAnzahlLoesungszettel(anzahlLoesungszettel);
			} else {
				LOG.warn("Kenne keine Schulteilnahme mit kuerzel {} und jahr {} (Rootauswertungsgruppe = {})",
					g.getTeilnahmekuerzel(), jahrAsString, g.getKuerzel());
			}
			result.add(payload);
		}

		LOG.info("Anzahl geänderte Schulen: {}", anzahlGeaendert);
		Collections.sort(result, new RootgruppeResponsePayloadNameSorter());
		return result;
	}

	/**
	 * Erzeugt die Übersicht über definierte Teilnahmegruppen: Länder, Privatteilnehmer,...
	 *
	 * @param jahr String das gewünschte Jahr.
	 * @return List
	 */
	public List<Teilnahmegruppe> getTeilnahmegruppen(final String jahr) {

		if (StringUtils.isBlank(jahr)) {
			throw new IllegalArgumentException("jahr blank");
		}

		final List<Teilnahmegruppe> result = erzeugeLandGruppen(jahr);
		Collections.sort(result, new TeilnahmegruppenNameComparator());

		// Privatteilnahmen werden hinten angehängt.
		result.addAll(erzeugePrivatteilnahmegruppen(jahr));

		print(jahr, result);

		return result;
	}

	private List<Teilnahmegruppe> erzeugePrivatteilnahmegruppen(final String jahr) {
		final List<Teilnahmegruppe> result = new ArrayList<>(1);
		final List<PrivatteilnahmeInformation> privatteilnahmeInfos = this.teilnahmenFacade.getAllPrivatteilnahmeInfosZuJahr(jahr);

		if (privatteilnahmeInfos.size() > 0) {

			final TeilnahmenFilter filter = new TeilnahmenFilter("Privatteilnahmen", Filterart.PRIVAT);
			final Teilnahmegruppe teilnahmegruppe = new Teilnahmegruppe();
			teilnahmegruppe.setJahr(jahr);
			teilnahmegruppe.setFilter(filter);
			teilnahmegruppe.setKuerzel(MKVConstants.GRUPPE_PRIVATTEILNAHMEN_KUERZEL);
			teilnahmegruppe.setAnzahlAnmeldungen(privatteilnahmeInfos.size());
			final int anzahlLoesungszettel = teilnahmenFacade.getAnzahlLoesungszettelPrivatteilnahmen(jahr, null);
			teilnahmegruppe.setAnzahlKinder(anzahlLoesungszettel);
			final HateoasPayload hateoasPayload = new HateoasPayload(MKVConstants.GRUPPE_PRIVATTEILNAHMEN_KUERZEL,
				"/gruppen/" + MKVConstants.GRUPPE_PRIVATTEILNAHMEN_KUERZEL);
			teilnahmegruppe.setHateoasPayload(hateoasPayload);
			result.add(teilnahmegruppe);
		}

		return result;
	}

	private List<Teilnahmegruppe> erzeugeLandGruppen(final String jahr) {
		final List<Land> laender = landDao.findAll(Land.class);
		final List<Teilnahmegruppe> result = new ArrayList<>(laender.size());

		for (final Land land : laender) {
			final Teilnahmegruppe teilnahmegruppe = this.erzeugeGruppe(jahr, land);
			if (teilnahmegruppe != null) {
				result.add(teilnahmegruppe);
			}
		}

		return result;
	}

	private Teilnahmegruppe erzeugeGruppe(final String jahr, final Land land) {

		final String kuerzel = land.getKuerzel();
		final List<String> landkuerzel = Arrays.asList(new String[] { kuerzel });

		final int anzahlSchulteilnahmen = teilnahmenFacade.getAnzahlSchulteilnahmenLand(landkuerzel, jahr);
		if (anzahlSchulteilnahmen > 0) {

			final TeilnahmenFilter filter = new TeilnahmenFilter(land.getName(), Filterart.LAND);
			filter.addKuerzel(land.getKuerzel());

			final Teilnahmegruppe teilnahmegruppe = new Teilnahmegruppe();
			teilnahmegruppe.setJahr(jahr);
			teilnahmegruppe.setFilter(filter);
			teilnahmegruppe.setAnzahlAnmeldungen(anzahlSchulteilnahmen);
			final int anzahlLoesungszettel = teilnahmenFacade.getAnzahlLoesungszettelSchulteilnahmen(jahr, null, landkuerzel);
			teilnahmegruppe.setAnzahlKinder(anzahlLoesungszettel);

			final List<String> teilnahmejahre = this.teilnahmenFacade.findTeilnahmejahreLand(kuerzel);
			teilnahmegruppe.setTeilnahmejahre(teilnahmejahre);

			HateoasPayload hateoasPayload = new HateoasPayload(kuerzel, "/gruppen/" + kuerzel);
			hateoasPayload = addLinks(hateoasPayload, kuerzel, teilnahmejahre);

			teilnahmegruppe.setHateoasPayload(hateoasPayload);
			return teilnahmegruppe;
		}
		return null;
	}

	private HateoasPayload addLinks(final HateoasPayload hateoasPayload, final String kuerzel, final List<String> teilnahmejahre) {
		for (final String j : teilnahmejahre) {
			final HateoasLink link = new HateoasLink("/auswertungen/" + j + "/laender?l=" + kuerzel, "Statistik " + j,
				HttpMethod.GET, MediaType.APPLICATION_OCTET_STREAM);
			hateoasPayload.addLink(link);
		}
		return hateoasPayload;
	}

	private void print(final String jahr, final List<Teilnahmegruppe> gruppen) {
		// final StringBuffer sb = new StringBuffer();
		// sb.append("\n");
		// sb.append("Jahr: ");
		// sb.append(jahr);
		// sb.append("\n");
		// gruppen.stream().forEach(g -> sb.append(g.toString() + "\n"));
		// LOG.info(sb.toString());

		LOG.info("{}: Anzahl Kinder: {}", jahr, gruppen.stream().mapToLong(g -> g.getAnzahlKinder()).sum());
		LOG.info("{}: Anzahl Anmeldungen: {}", jahr, gruppen.stream().mapToLong(g -> g.getAnzahlAnmeldungen()).sum());
	}
}
