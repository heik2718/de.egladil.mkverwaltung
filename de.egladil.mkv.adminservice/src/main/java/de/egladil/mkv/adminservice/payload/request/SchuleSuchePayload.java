//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.Honeypot;
import de.egladil.common.validation.annotations.Kuerzel;

/**
 * SchuleSuchePayload
 */
public class SchuleSuchePayload implements ILoggable, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = 100)
	@DeutscherName
	private String name;

	@NotNull
	@Size(min = 8, max = 8)
	@Kuerzel
	private String ortkuerzel;

	@Honeypot
	private String kleber;

	/**
	 * Erzeugt eine Instanz von SchuleSuchePayload
	 */
	public SchuleSuchePayload() {
	}

	/**
	 * Erzeugt eine Instanz von SchuleSuchePayload
	 */
	public SchuleSuchePayload(final String ortkuerzel, final String name) {
		this.ortkuerzel = ortkuerzel;
		this.name = name;
	}


	/**
	 *
	 * @see de.egladil.common.persistence.ILoggable#toLog()
	 */
	@Override
	public String toBotLog() {
		return "SchuleSuchePayload [ortkuerzel=" + ortkuerzel + ", name=" + name + ", kleber=" + kleber + "]";
	}

	/**
	 * Liefert die Membervariable kleber
	 *
	 * @return die Membervariable kleber
	 */
	public String getKleber() {
		return kleber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param kleber neuer Wert der Membervariablen kleber
	 */
	public void setKleber(final String kleber) {
		this.kleber = kleber;
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Liefert die Membervariable ortkuerzel
	 *
	 * @return die Membervariable ortkuerzel
	 */
	public String getOrtkuerzel() {
		return ortkuerzel;
	}

}
