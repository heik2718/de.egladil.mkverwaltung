//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.egladil.mkv.persistence.domain.teilnahmen.Upload;

/**
 * AuswertungstabelleInfo enthält die Infos über hochgeladene Auswertungstabellen und den Teilnahmeidentifier.
 */
public class AuswertungstabelleInfo {

	@JsonProperty
	private AuswertungstabelleDateiInfo dateiInfo;

	@JsonProperty
	private Upload upload;

	public AuswertungstabelleDateiInfo getDateiInfo() {
		return dateiInfo;
	}

	public void setDateiInfo(final AuswertungstabelleDateiInfo dateiInfo) {
		this.dateiInfo = dateiInfo;
	}

	public Upload getUpload() {
		return upload;
	}

	public void setUpload(final Upload upload) {
		this.upload = upload;
	}

}
