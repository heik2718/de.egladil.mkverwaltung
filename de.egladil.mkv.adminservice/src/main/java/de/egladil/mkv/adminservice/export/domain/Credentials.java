//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.export.domain;

import java.util.Date;

/**
 * LoginSecrets
 */
public class Credentials {

	private String passwordhash;

	private Date lastLoginAttempt;

	private Salt salt;

	/**
	 * Erzeugt eine Instanz von LoginSecrets
	 */
	public Credentials() {
	}

	public String getPasswordhash() {
		return passwordhash;
	}

	public void setPasswordhash(final String passwordhash) {
		this.passwordhash = passwordhash;
	}

	public Date getLastLoginAttempt() {
		return lastLoginAttempt;
	}

	public void setLastLoginAttempt(final Date lastLoginAttempt) {
		this.lastLoginAttempt = lastLoginAttempt;
	}

	public Salt getSalt() {
		return salt;
	}

	public void setSalt(final Salt salt) {
		this.salt = salt;
	}

}
