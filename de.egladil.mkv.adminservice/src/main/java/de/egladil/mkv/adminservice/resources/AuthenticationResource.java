//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.auth.Credentials;
import de.egladil.bv.aas.auth.IAuthConstants;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.SessionToken;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.mkv.adminservice.session.SessionDelegate;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import io.dropwizard.auth.Auth;

/**
 * AuthenticationResource
 */
@Path("/konten")
@Singleton
@Produces("application/json")
@Consumes("application/json")
public class AuthenticationResource {

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationResource.class);

	private final SessionDelegate sessionDelegate;

	private final IProtokollService protokollService;

	private final ResourceExceptionHandler exceptionHandler;

	private ValidationDelegate validationDelegate;

	/**
	 * Erzeugt eine Instanz von AuthenticationResource
	 */
	@Inject
	public AuthenticationResource(final SessionDelegate sessionDelegate, final IProtokollService protokollService) {
		this.sessionDelegate = sessionDelegate;
		this.protokollService = protokollService;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
		this.validationDelegate = new ValidationDelegate();
	}

	@OPTIONS
	@Path("/admin")
	@Timed
	public Response loginOptions() {
		return Response.ok().build();
	}

	/**
	 * Tja, eben einloggen.
	 *
	 * @param credentials
	 * @param tempCsrfToken String das wird hier benötigt, weil es invalidiert werden muss.
	 * @return
	 */
	@POST
	@Path("/admin")
	@Timed
	public Response login(final Credentials credentials, @HeaderParam("X-XSRF-TOKEN") final String tempCsrfToken) {
		UsernamePasswordToken usernamePasswordToken = null;
		try {
			validationDelegate.check(credentials, Credentials.class);
			usernamePasswordToken = new UsernamePasswordToken(credentials.getUsername(), credentials.getPassword().toCharArray());
			final SessionToken admin = sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken);
			final APIResponsePayload responsePayload = new APIResponsePayload(APIMessage.info(""), admin);
			return Response.ok().entity(responsePayload).build();
		} catch (final EgladilAuthenticationException e) {
			LOG.warn("Admin-Login: IncorrectCredentials!!!");
			return exceptionHandler.mapToMKVApiResponse(e, null, credentials);
		} catch (final DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, credentials);
		} catch (final Throwable e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, credentials);
		} finally {
			if (credentials != null) {
				credentials.clear();
			}
			if (usernamePasswordToken != null) {
				usernamePasswordToken.clear();
			}
		}
	}

	/**
	 * Erzeugt eine neue anonyme Session
	 *
	 * @param principal Principal fürs authentication framework. Die Annotation bewirkt, dass der Parameter durch
	 * io.dropwizard.auth gesetzt wird. Der Request sollte diesen tunlichst nicht enthalten.
	 *
	 * @param bearer String - der Parameter muss hier explizit ausgelesen werden zum Ungüligmachen der Session, da das
	 * AuthenticationToken transparent vom Framework geprüft wird und an dieser Stelle kein Zugang dazu besteht. Die
	 * Session-ID kommt mit vorangestelltem 'Bearer ' an.
	 *
	 * @return Response
	 */
	@POST
	@Timed
	@Path("/logout")
	public Response logout(@Auth final Principal principal, @HeaderParam(IAuthConstants.BEARER_KEY) final String bearer) {
		// @Auth ist eine dropwizard-Abkürzung zum SecurityContext. Auch möglich wäre stattdessen
		// @Context SecurityContext secContext als Parameter und secContext.getPrincipal()...
		try {
			sessionDelegate.logoutQuietly(principal.getName(), bearer);
			final APIResponsePayload entity = sessionDelegate.createAnonymousSession();
			// @formatter:off
			return Response
				.ok()
				.entity(entity)
				.type(MediaType.APPLICATION_JSON)
				.encoding("UTF-8")
				.build();
			// @formatter:on
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return Response.ok().build();
		}
	}

	/**
	 * Setzt zu Testzwecken andere Implementierung.
	 *
	 * @param validationDelegate neuer Wert der Membervariablen validationDelegate
	 */
	void setValidationDelegate(final ValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}
}
