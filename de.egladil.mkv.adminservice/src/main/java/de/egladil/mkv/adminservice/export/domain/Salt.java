//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.export.domain;

/**
 * Salt
 */
public class Salt {

	private String algorithmName;

	private int iterations;

	private String wert;

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(final String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(final int iterations) {
		this.iterations = iterations;
	}

	public String getWert() {
		return wert;
	}

	public void setWert(final String wert) {
		this.wert = wert;
	}

}
