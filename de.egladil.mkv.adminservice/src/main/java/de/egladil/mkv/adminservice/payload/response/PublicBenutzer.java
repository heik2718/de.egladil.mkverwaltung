//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;


import org.hibernate.validator.constraints.Email;

import de.egladil.common.validation.annotations.DeutscherName;
import de.egladil.common.validation.annotations.StringLatin;
import de.egladil.common.validation.annotations.UuidString;
import de.egladil.mkv.persistence.domain.user.AdminBenutzerinfo;

/**
 * PublicBenutzer select vorname, nachname, email, ben_id, aktiviert, gesperrt, rolle, uuid, date_modified, last_login
 * vw_mkvbenutzer<br>
 * <br>
 * deprecated: ersetzen durch MKVBenutzer
 */
@Deprecated
public class PublicBenutzer {

	@UuidString
	private String uuid;

	@StringLatin
	private String nachname;

	@StringLatin
	private String vorname;

	@Email
	private String email;

	private String lastAccess;

	private String datumGeaendert;

	@DeutscherName
	private String rolle;

	private boolean aktiviert;

	private boolean gesperrt;

	public PublicBenutzer() {
	}

	/**
	 *
	 * Erzeugt eine Instanz von PublicBenutzer, indem alle Attribute vom Typ String und boolean aus adminBenutzerinfo
	 * übernommen werden. Die Attrbute lastAccess und datumGeaendert müssen gesetzt werden.
	 */
	public PublicBenutzer(final AdminBenutzerinfo adminBenutzerinfo) {
		this.aktiviert = adminBenutzerinfo.isAktiviert();
		this.gesperrt = adminBenutzerinfo.isGesperrt();
		this.email = adminBenutzerinfo.getEmail();
		this.nachname = adminBenutzerinfo.getNachname();
		this.rolle = adminBenutzerinfo.getRolle().toString();
		this.uuid = adminBenutzerinfo.getUuid();
		this.vorname = adminBenutzerinfo.getVorname();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PublicBenutzer other = (PublicBenutzer) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	public String getUuid() {
		return uuid;
	}

	public String getNachname() {
		return nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public String getEmail() {
		return email;
	}

	public String getLastAccess() {
		return lastAccess;
	}

	public String getRolle() {
		return rolle;
	}

	public boolean isAktiviert() {
		return aktiviert;
	}

	public boolean isGesperrt() {
		return gesperrt;
	}

	public String getDatumGeaendert() {
		return datumGeaendert;
	}

	public void setDatumGeaendert(final String datumeGeandert) {
		this.datumGeaendert = datumeGeandert;
	}

	public void setLastAccess(final String lastAccess) {
		this.lastAccess = lastAccess;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PublicBenutzer [uuid=" + uuid + ", nachname=" + nachname + ", vorname=" + vorname + ", email=" + email
			+ ", lastAccess=" + lastAccess + ", datumGeaendert=" + datumGeaendert + ", rolle=" + rolle + ", aktiviert=" + aktiviert
			+ ", gesperrt=" + gesperrt + "]";
	}

}
