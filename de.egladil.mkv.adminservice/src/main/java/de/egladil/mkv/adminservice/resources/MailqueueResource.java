//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.adminservice.mails.MailqueueDelegate;
import de.egladil.mkv.adminservice.payload.request.NewsletterPayload;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.persistence.domain.user.IMailempfaenger;
import io.dropwizard.auth.Auth;

/**
 * MailqueueResource
 */
@Singleton
@Path("/mails")
@Produces("application/json")
@Consumes("application/json")
public class MailqueueResource {

	private static final Logger LOG = LoggerFactory.getLogger(MailqueueResource.class);

	private int maxAnzMailempfaenger;

	private final MailqueueDelegate mailqueueDelegate;

	private ValidationDelegate validationDelegate;

	private String pathMails;

	/**
	 * Erzeugt eine Instanz von MailqueueResource
	 */
	@Inject
	public MailqueueResource(final MailqueueDelegate mailqueueDelegate) {
		this.mailqueueDelegate = mailqueueDelegate;
		validationDelegate = new ValidationDelegate();
	}

	/**
	 * Stellt Mails in die Mailqueue ein. Die Empfänder und der Inhalt müssen zuvor im Verzeichnis
	 * configuration.verzeichnisNewsletter (=pathMails) als Dateien hinterlegt worden sein.
	 * <ul>
	 * <li>Empfänger: csv-Datei nur mit Mailadressen. Name = NewsletterPayload.dateinameEmfaenger.</li>
	 * <li>Inhalt: Textdatei. Name = NewsletterPayload.dateinameInhalt.</li>
	 * </ul>
	 *
	 * @param principal
	 * @param sourceMailcontents
	 * @param sourceEmpfaenger
	 * @return
	 */
	@POST
	@Path("newsletter")
	public Response queueMails(@Auth final Principal principal, final NewsletterPayload payload) {
		try {
			validationDelegate.check(payload, NewsletterPayload.class);
			final List<IMailempfaenger> empfaenger = mailqueueDelegate.queueMails(payload, pathMails, maxAnzMailempfaenger);
			LOG.info("Mail an {} Empfänger in Mailqueue eingestellt - {}", empfaenger.size(), payload.toString());
			return Response.status(Status.CREATED)
				.entity(
					new APIResponsePayload(APIMessage.info("Mail an " + empfaenger.size() + " Empfänger in Mailqueue eingestellt")))
				.build();
		} catch (final EgladilWebappException e) {
			return e.getResponse();
		} catch (final Exception e) {
			LOG.error("Fehler beim einstellen der Mail {} - {}", new String[] { payload.toString(), e.getMessage() }, e);
			return Response.serverError().entity(new APIResponsePayload(APIMessage.error(e.getMessage()))).build();
		}
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param maxAnzMailempfaenger neuer Wert der Membervariablen maxAnzMailempfaenger
	 */
	public void setMaxAnzMailempfaenger(final int maxAnzMailempfaenger) {
		this.maxAnzMailempfaenger = maxAnzMailempfaenger;
	}

	public void setPathMails(final String pathMails) {
		this.pathMails = pathMails;
	}
}
