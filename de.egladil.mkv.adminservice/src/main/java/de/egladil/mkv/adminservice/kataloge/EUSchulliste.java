//=====================================================
// Projekt: de.egladil.misc
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.kataloge;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * EUSchulliste
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EUSchulliste {

	@XmlElement(name = "schule")
	private List<EUSchule> schulen = new ArrayList<>();

	public void addSchule(EUSchule schule) {
		schulen.add(schule);
	}

	/**
	* Liefert die Membervariable schulen
	* @return die Membervariable  schulen
	*/
	public List<EUSchule> getSchulen() {
		return schulen;
	}

}
