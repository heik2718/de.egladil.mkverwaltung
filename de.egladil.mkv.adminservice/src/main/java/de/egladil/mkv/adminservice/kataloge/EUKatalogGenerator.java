//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.kataloge;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.adminservice.config.MKVAdminModule;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.domain.kataloge.Land;
import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.domain.kataloge.Schule;
import de.egladil.mkv.persistence.utils.KuerzelGenerator;

/**
 * EUKatalogGenerator
 */
public class EUKatalogGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(EUKatalogGenerator.class);

	private static final String PATH_WORK_DIR = "/home/heike/executablejars/crawler/schulliste_eu";

	@Parameter(names = { "-k" }, description = "Kuerzel des zu verarbeitenden Landes", required = true)
	private String laenderkuerzel;

	@Parameter(names = { "-n" }, description = "Name des verarbeitenden Landes", required = true)
	private String landName;

	private ILandDao landDao;

	private JAXBContext jaxbContext;

	private KuerzelGenerator kuerzelGenerator;

	private Validator validator;

	/**
	 * Erzeugt eine Instanz von EUKatalogGenerator
	 *
	 * @throws JAXBException
	 */
	public EUKatalogGenerator(final String pathConfigRoot) throws JAXBException {
		final Injector injector = Guice.createInjector(new MKVAdminModule(pathConfigRoot));
		landDao = injector.getInstance(ILandDao.class);
		jaxbContext = JAXBContext.newInstance(EUSchulliste.class);
		kuerzelGenerator = new KuerzelGenerator();
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	/**
	 * TODO
	 *
	 * @param args
	 */

	public static void main(final String[] args) {
		EUKatalogGenerator application = null;
		try {
			application = new EUKatalogGenerator(OsUtils.getDevConfigRoot());
			new JCommander(application, args);
			LOG.info(application.toString());
			application.start();
			System.exit(0);
		} catch (final JAXBException e) {
			System.err.println("Fehler bei den JAXB-Annotationen: " + e.getMessage());
		} catch (final ParameterException e) {
			System.err.println(e.getMessage());
			if (application != null) {
				application.printUsage();
			}
			System.exit(1);
		} catch (final Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private void start() throws JAXBException {
		laenderkuerzel = laenderkuerzel.toLowerCase();
		final String key = "de-" + laenderkuerzel.toUpperCase();
		Land land = null;
		final Optional<Land> opt = landDao.findByUniqueKey(Land.class, "kuerzel", key);
		if (!opt.isPresent()) {
			land = new Land();
			land.setKuerzel(key);
			land.setName(landName.trim());
		} else {
			land = opt.get();
		}
		final File workDir = new File(PATH_WORK_DIR + File.separator + laenderkuerzel);
		if (!workDir.isDirectory()) {
			LOG.error("Verzeichnis {} existiert nicht", workDir.getAbsolutePath());
			throw new IllegalArgumentException("Voraussetzungen nicht erfüllt");
		}
		final Collection<File> xmlFiles = FileUtils.listFiles(workDir, new String[] { "xml" }, false);
		for (final File xmlFile : xmlFiles) {
			fileVerarbeiten(xmlFile, land);
		}
		LOG.info("Land hat {} orte", land.getOrte().size());
		landDao.persist(land);
		LOG.info("Land fertig: {}", land);
	}

	private void fileVerarbeiten(final File xmlFile, final Land land) throws JAXBException {
		final Object obj = jaxbContext.createUnmarshaller().unmarshal(xmlFile);
		if (!(obj instanceof EUSchulliste)) {
			throw new RuntimeException("Irgendwas anderes als EUSchulliste erzeugt: " + obj.getClass().getName());
		}
		final EUSchulliste schulliste = (EUSchulliste) obj;
		for (final EUSchule schule : schulliste.getSchulen()) {
			final String ortName = schule.getNameOrt();
			final Ort ort = findOrCreateOrt(land, ortName);
			findOrCreateSchule(ort, schule);
		}
	}

	private Ort findOrCreateOrt(final Land land, final String nameOrt) {
		for (final Ort ort : land.getOrte()) {
			if (nameOrt.equals(ort.getName())) {
				return ort;
			}
		}
		final Ort ort = new Ort();
		ort.setKuerzel(kuerzelGenerator.generateDefaultKuerzel());
		ort.setName(nameOrt);
		land.addOrt(ort);
		return ort;
	}

	private void findOrCreateSchule(final Ort ort, final EUSchule euSchule) {
		for (final Schule schule : ort.getSchulen()) {
			if (euSchule.getName().equals(schule.getName())) {
				return;
			}
		}
		final Schule schule = new Schule();
		schule.setKuerzel(kuerzelGenerator.generateDefaultKuerzel());
		schule.setName(euSchule.getName());
		schule.setStrasse(euSchule.getStrasse());
		schule.setUrl(euSchule.getUrl());
		schule.setSchultyp(euSchule.getSchultyp());
		final Set<ConstraintViolation<Schule>> errors = validator.validate(schule);

		if (!errors.isEmpty()) {
			LOG.error("CV bei Schule {} in {}", schule, ort);
			System.err.println(schule.toString());
			final Iterator<ConstraintViolation<Schule>> iter = errors.iterator();
			while (iter.hasNext()) {
				final ConstraintViolation<Schule> cv = iter.next();
				LOG.error(cv.toString());
			}
			return;
		}
		ort.addSchule(schule);
	}

	public void printUsage() {
		final StringBuffer sb = new StringBuffer();
		sb.append("Usage: <main class> [options]\n");
		sb.append("   Options:\n");
		sb.append("     * -k\n");
		sb.append("          kuerzel des zu verarbeitenden Landes ohne de-\n");
		sb.append("     * -n\n");
		sb.append("          name des zu verarbeitenden Landes\n");
		System.out.println(sb.toString());
	}

}
