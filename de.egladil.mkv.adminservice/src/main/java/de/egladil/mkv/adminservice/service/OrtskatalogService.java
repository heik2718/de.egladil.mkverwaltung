//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service;

import java.util.List;

import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.mkv.persistence.payload.response.PublicOrt;

/**
 * OrtskatalogService
 */
public interface OrtskatalogService {

	/**
	 *
	 * @param name String Ortsname oder Teil des Ortsnamens. Darf nicht blank sein.
	 * @param land String Landname (nicht kuerzel!) oder Teil des Landnamens. Darf blank sein
	 * @return List
	 */
	List<PublicOrt> findOrte(String name, String land);

	/**
	 * Gibt den PublicOrt mit allen Schulen (lazy) zurück.
	 *
	 * @param kuerzel String
	 * @return PublicOrt
	 */
	PublicOrt getOrt(String kuerzel);

	/**
	 *
	 * @param kuerzelLand String das Land, zu dem der Ort hinzugefügt wird.
	 * @param name String Name des Ortes
	 * @param benutzerUuid String zur Authorisierun
	 * @return PublicOrt
	 *
	 * @throws EgladilAuthorizationException
	 * @throws EgladilDuplicateEntryException
	 */
	PublicOrt ortAnlegen(String kuerzelLand, String name, String benutzerUuid)
		throws EgladilAuthorizationException, EgladilDuplicateEntryException;

}
