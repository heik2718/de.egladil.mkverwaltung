//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.common.validation.ValidationUtils;
import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;

/**
 * ValidationDelegate für Validierung von Ein- und Ausgabe-Objekten.
 */
public class ValidationDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationDelegate.class);

	private final Validator validator;

	/**
	 * Erzeugt eine Instanz von ValidationDelegate
	 */
	public ValidationDelegate() {
		final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	/**
	 *
	 * @param payload
	 * @param clazz
	 */
	public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
		if (payload == null) {
			LOG.error("Parameter anfrage darf nicht null sein!");
			throw ExceptionFactory.badRequest();
		}
		final Set<ConstraintViolation<T>> errors = validator.validate(payload);
		handleValidationErrorsWithKleber(payload, errors, clazz);
	}

	/**
	 * @param loggableObject
	 * @param errors
	 * @throws IllegalArgumentException
	 * @throws EgladilWebappException
	 */
	private <T extends ILoggable> void handleValidationErrorsWithKleber(final T loggableObject, final Set<ConstraintViolation<T>> errors,
		final Class<T> clazz) throws IllegalArgumentException, EgladilWebappException {
		if (!errors.isEmpty()) {
			final ValidationUtils validationUtils = new ValidationUtils();
			final Set<String> properties = validationUtils.extractPropertyNames(errors);
			if (properties.contains("kleber")) {
				LOG.warn("Possible BOT-Attac: " + loggableObject.toBotLog());
				throw ExceptionFactory.forbidden();
			}
			final String message = "Ungültige Eingaben: " + PrettyStringUtils.collectionToDefaultString(properties);
			if (properties.contains("clientId")) {
				LOG.error(message);
				LOG.error(loggableObject.toString());
			} else {
				LOG.debug(message);
				LOG.debug(loggableObject.toString());
			}

			final ConstraintViolationMessage details = validationUtils.toConstraintViolationMessage(errors, clazz);
			throw ExceptionFactory.validationErrors(details);
		}
	}
}
