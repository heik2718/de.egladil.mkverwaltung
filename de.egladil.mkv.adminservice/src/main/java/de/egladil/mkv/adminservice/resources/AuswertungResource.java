//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.validation.annotations.Kuerzel;
import de.egladil.common.validation.annotations.LandKuerzel;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.adminservice.auswertungen.AuswertungenRunner;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.payload.response.Message;
import de.egladil.mkv.adminservice.service.TeilnahmegruppenService;
import de.egladil.mkv.auswertungen.domain.AufgabeErgebnisItem;
import de.egladil.mkv.auswertungen.domain.Gesamtpunktverteilung;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungDaten;
import de.egladil.mkv.auswertungen.domain.GesamtpunktverteilungTexte;
import de.egladil.mkv.auswertungen.payload.response.PublicGesamtpunktverteilung;
import de.egladil.mkv.auswertungen.statistik.IVerteilungGenerator;
import de.egladil.mkv.auswertungen.urkunden.AuswertungService;
import de.egladil.mkv.persistence.domain.auswertungen.ILoesungszettel;
import de.egladil.mkv.persistence.domain.auswertungen.Loesungszettel;
import de.egladil.mkv.persistence.domain.enums.Aufgabenkategorie;
import de.egladil.mkv.persistence.domain.enums.Klassenstufe;
import de.egladil.mkv.persistence.payload.response.AufgabenkategorieAngemessenheitsintervall;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmegruppe;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmejahr;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import io.dropwizard.auth.Auth;

/**
 * AuswertungResource
 */
@Singleton
@Path("auswertungen")
@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class AuswertungResource {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungResource.class);

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final IVerteilungGenerator verteilungGenerator;

	private final AuswertungService auswertungService;

	private final TeilnahmenFacade teilnahmenFacade;

	private final TeilnahmegruppenService teilnahmegruppenService;

	private final ResourceExceptionHandler exceptionHandler;

	/**
	 * Erzeugt eine Instanz von AuswertungResource
	 */
	@Inject
	public AuswertungResource(final AuswertungenRunner auswertungenRunner, final IVerteilungGenerator verteilungGenerator,
		final TeilnahmenFacade teilnahmenFacade, final AuswertungService auswertungService,
		final TeilnahmegruppenService teilnahmegruppenService) {
		this.verteilungGenerator = verteilungGenerator;
		this.teilnahmenFacade = teilnahmenFacade;
		this.auswertungService = auswertungService;
		this.teilnahmegruppenService = teilnahmegruppenService;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
	}

	/**
	 *
	 * @param jahr int das Wettbewerbsjahr
	 * @param klasseNummer int die Nummer der Klassenstufe.
	 * @return Response: 404, falls es keine Gesamtpunktverteilung zu dem Jahr und der Klassenstufe gibt, 500 bei
	 * unerwarteter Exception, 200 mit XML sonst.
	 */
	@GET
	@Produces(MediaType.APPLICATION_XML + "; charset=utf-8")
	@Path("/gesamtpunktverteilungen/{jahr}/{klasse}")
	@Timed
	public Response generiereGesamtpunktverteilungXml(@PathParam(value = "jahr")
	@Digits(fraction = 0, integer = 4)
	final int jahr, @PathParam(value = "klasse")
	final int klasseNummer) {
		Klassenstufe klassenstufe = null;
		try {

			// FIXME: verlagern nach AuswertungenService.
			klassenstufe = Klassenstufe.valueOfNummer(klasseNummer);
			final List<Loesungszettel> loesungszettel = teilnahmenFacade
				.findLoesungszettelByJahrUndKlassenstufe(String.valueOf(jahr), klassenstufe);

			if (loesungszettel.isEmpty()) {
				LOG.warn("Keine Gesamtauswertung mit Jahr {} und Klassenstufe {} vorhanden", jahr, klasseNummer);
				return Response.status(CustomResponseStatus.NOT_FOUND.getStatusCode()).build();
			}

			final List<ILoesungszettel> alleZettel = new ArrayList<>();
			alleZettel.addAll(loesungszettel);
			final GesamtpunktverteilungDaten daten = verteilungGenerator.berechneGesamtpunktverteilungDaten(alleZettel,
				klassenstufe, String.valueOf(jahr));
			final GesamtpunktverteilungTexte texte = new GesamtpunktverteilungTexte();
			texte.setBasis(
				MessageFormat.format(applicationMessages.getString("statistik.gesamtpunktverteilung.grundlage.description"),
					new Object[] { Integer.toString(alleZettel.size()) }));
			texte.setBewertung(
				MessageFormat.format(applicationMessages.getString("statistik.gesamtpunktverteilung.bewertung.description"),
					new Object[] { klassenstufe.getStartguthaben() }));
			texte.setTitel(MessageFormat.format(applicationMessages.getString("statistik.gesamtpunktverteilung.headline"),
				new Object[] { Integer.toString(jahr), klassenstufe.getLabel() }));
			texte.setSectionEinzelergebnisse(applicationMessages.getString("statistik.gesamtpunktverteilung.sectioneinzel"));
			final Gesamtpunktverteilung result = new Gesamtpunktverteilung(texte, daten);
			return Response.ok(result).build();
		} catch (final IllegalArgumentException e) {
			if (e.getMessage() != null) {
				LOG.warn("IllegalArgumentException beim Erzeugen der Gesamtpunktverteilung: {}", e.getMessage());
			} else {
				LOG.warn("IllegalArgumentException beim Erzeugen der Gesamtpunktverteilung: jahr = {}, klasseNummer = {}", jahr,
					klasseNummer);
				LOG.debug("Details", e);
			}
			return Response.status(CustomResponseStatus.NOT_FOUND.getStatusCode()).build();
		} catch (final Exception e) {
			LOG.error("Fehler beim Erzeugen der Gesamtpunktverteilung: {}", e.getMessage(), e);
			final Message message = new Message(applicationMessages.getString("general.internalServerError"), MessageLevel.ERROR);
			return Response.serverError().entity(message).build();
		}
	}

	/**
	 * Erzeugt die Gesamtpunktverteilung für das gegebene Jahr als JSON-Objekt.
	 *
	 * @param jahr String das Wettbewerbsjahr
	 * @return APIResponsePayload mit PublicGesamtpunktverteilung
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/gesamtpunktverteilungen/{jahr}")
	@Timed
	public Response getGesamtpunktverteilungJson(@Auth
	final Principal principal, @PathParam(value = "jahr")
	@Digits(fraction = 0, integer = 4)
	final String jahr) {
		try {
			final List<GesamtpunktverteilungDaten> daten = this.auswertungService.generiereGesamtpunktverteilung(jahr);

			bewerteAufaben(daten);

			final PublicGesamtpunktverteilung payload = new PublicGesamtpunktverteilung("Gesamtpunktverteilung", jahr, daten);

			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"), payload);
			return Response.ok(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /auswertungen/gesamtpunktverteilungen/{jahr}: {}", e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	private void bewerteAufaben(final List<GesamtpunktverteilungDaten> daten) {

		final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();

		for (final GesamtpunktverteilungDaten datenItem : daten) {
			for (final AufgabeErgebnisItem item : datenItem.getAufgabeErgebnisItemsSorted()) {
				final Aufgabenkategorie aufgabenkategorie = Aufgabenkategorie.getByNummer(item.getNummer());
				final AufgabenkategorieAngemessenheitsintervall intervall = config.getAngemessenheitsintervall(aufgabenkategorie);
				if (intervall != null) {
					final double anteilRichtig = item.getAnteilRichtigGeloest() * 100;
					if (anteilRichtig < intervall.getUntereSchranke()) {
						item.setZuSchwer(true);
					}
					if (anteilRichtig > intervall.getObereSchranke()) {
						item.setZuLeicht(true);
					}
				}
			}
		}
	}

	/**
	 * Gibt die Jahre zurück, für die es Auswertungen gibt.
	 *
	 * @param principal
	 * @return Response mit Teilnahmejahren
	 */
	@GET
	@Path("jahre")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response getJahre(@Auth
	final Principal principal) {
		try {
			final List<Teilnahmejahr> result = this.auswertungService.getAuswertbareJahre();
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"), result);
			return Response.ok(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /auswertungen/jahre: {}", e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			return ExceptionFactory.internalServerErrorException(LOG, e, "Holen der möglichen Auswertungsjahre").getResponse();
		}
	}

	/**
	 * Erzeugt die Übersicht von definierten Gruppen (Länder, aggregierte Länder, Privatteilnahmen) zu einem Jahr in
	 * Form einer Liste von Teilnahmegruppe-Objekten.
	 *
	 * @param principal Principal
	 * @param jahr String
	 * @return Response mit TeilnahmeGruppe-Liste-Payload.
	 */
	@GET
	@Path("{jahr}/gruppen")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response getUebersichtGruppen(@Auth
	final Principal principal, @PathParam("jahr")
	@NotNull
	@Digits(fraction = 0, integer = 4)
	final String jahr) {

		try {
			final List<Teilnahmegruppe> laender = this.teilnahmegruppenService.getTeilnahmegruppen(jahr);

			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"), laender);
			return Response.ok(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /auswertungen/{jahr}/laender: {}", e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Berechnet für die Ländergruppe in kuerzel die PublicGesamtpunktverteilung.
	 *
	 * @param principal
	 * @param jahr
	 * @param filterart Filterart
	 * @param kuerzelliste String die Länderkürzel mit Komma getrennt. Nur bei filterart LAND erforderlich
	 * @return
	 */
	@GET
	@Path("/gesamtpunktverteilungen/{jahr}/gruppen")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response getGesamtpunktverteilungTeilnahmegruppeJson(@Auth
	final Principal principal, @PathParam("jahr")
	@NotNull
	@Digits(fraction = 0, integer = 4)
	final String jahr, @QueryParam("k")
	@LandKuerzel
	@NotBlank
	final String kuerzelliste) {

		try {
			final List<GesamtpunktverteilungDaten> daten = this.auswertungService.generiereGesamtpunktverteilung(jahr,
				kuerzelliste);

			final PublicGesamtpunktverteilung payload = new PublicGesamtpunktverteilung("Gesamtpunktverteilung " + kuerzelliste,
				jahr, daten);

			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"), payload);
			return Response.ok(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /auswertungen/gesamtpunktverteilungen/{jahr}/gruppen: {}", e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	/**
	 * Berechnet für die Ländergruppe in kuerzel die PublicGesamtpunktverteilung.
	 *
	 * @param principal
	 * @param jahr
	 * @param filterart Filterart
	 * @param kuerzelliste String die Länderkürzel mit Komma getrennt. Nur bei filterart LAND erforderlich
	 * @return
	 */
	@GET
	@Path("/schulen/{kuerzel}/{jahr}")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Timed
	public Response getGesamtpunktverteilungSchuleJson(@Auth
	final Principal principal, @PathParam("kuerzel")
	@NotNull
	@Kuerzel
	final String schulkuerzel, @PathParam("jahr")
	@NotNull
	@Digits(fraction = 0, integer = 4)
	final String jahr) {

		try {
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ist noch nicht fertig"), null);
			return Response.ok(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /auswertungen/schulen/{}/{}: {}", schulkuerzel, jahr, e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}
}
