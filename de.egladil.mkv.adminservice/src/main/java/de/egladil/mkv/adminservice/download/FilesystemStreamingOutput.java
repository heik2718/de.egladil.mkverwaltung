//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.download;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.IOUtils;

import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.mkv.persistence.exceptions.MKVException;

/**
 * FileStreamingOutput
 */
public class FilesystemStreamingOutput implements StreamingOutput {

	private final File file;

	/**
	 *
	 * Erzeugt eine Instanz von FileStreamingOutput.
	 *
	 */
	public FilesystemStreamingOutput(final String pathDownloadDir, final String fileName) throws IllegalArgumentException, MKVException {
		if (pathDownloadDir == null) {
			throw new IllegalArgumentException("pathDownloadDir darf nicht null sein.");
		}
		if (fileName == null) {
			throw new IllegalArgumentException("fileName darf nicht null sein.");
		}
		if (!new File(pathDownloadDir).canRead()) {
			throw new MKVException("Keine Leseberechtigung für Verzeichnis [" + pathDownloadDir + "])");
		}
		final String pathname = pathDownloadDir + File.separator + fileName;
		file = new File(pathname);
	}

	/**
	 * @see javax.ws.rs.core.StreamingOutput#write(java.io.OutputStream)
	 */
	@Override
	public void write(final OutputStream output) {
		FileInputStream input = null;
		try {
			input = new FileInputStream(file);
			int bytes;
			while ((bytes = input.read()) != -1) {
				output.write(bytes);
			}
			output.flush();
		} catch (final FileNotFoundException e) {
			throw new ResourceNotFoundException("Datei [" + file.getAbsolutePath() + "] existiert nicht.", e);
		} catch (final IOException e) {
			throw new MKVException(
				"IOException: " + e.getMessage() + " (Keine Leseberechtigung für Datei [" + file.getAbsolutePath() + "])?", e);
		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}
	}
}
