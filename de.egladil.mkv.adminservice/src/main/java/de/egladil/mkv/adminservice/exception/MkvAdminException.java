//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.exception;

/**
 * MkvAdminException
 */
public class MkvAdminException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von MkvAdminException
	 */
	public MkvAdminException(final String arg0) {
		super(arg0);
	}

	/**
	 * Erzeugt eine Instanz von MkvAdminException
	 */
	public MkvAdminException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

}
