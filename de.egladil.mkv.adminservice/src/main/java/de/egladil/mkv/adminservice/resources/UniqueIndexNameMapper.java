//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.UniqueIndexNameMessageProvider;

/**
* UniqueIndexNameMapper
*/
public class UniqueIndexNameMapper implements UniqueIndexNameMessageProvider {

	private static Map<String, String> uniqueIndexMap;

	static {
		uniqueIndexMap = new HashMap<>();
		uniqueIndexMap.put("uk_laender_1", "Ein Land mit diesem KUERZEL gibt es schon.");
		uniqueIndexMap.put("uk_laender_2", "Ein Land mit diesem Namen gibt es schon.");

		uniqueIndexMap.put("uk_orte_1", "Einen Ort mit diesem KUERZEL gibt es schon.");
		uniqueIndexMap.put("uk_orte_2", "Einen Ort mit diesem Namen gibt es in diesem Land schon.");

		uniqueIndexMap.put("uk_schulen_1", "Eine Schule mit diesem KUERZEL gibt es schon.");
		uniqueIndexMap.put("uk_schulen_2", "Eine Schule mit diesem Namen gibt es in diesem Ort schon.");
	}

	/**
	 * Utility-Methode, die alle vorhandenen Unique indexes auf verständliche Meldung mapped.
	 *
	 * @param uniqueIndexName
	 * @return
	 */
	private static Optional<String> uniqueIndexNameToMessage(final String uniqueIndexName) {
		final String message = uniqueIndexMap.get(uniqueIndexName.toLowerCase());
		return message == null ? Optional.empty() : Optional.of(message);
	}

	@Override
	public Optional<String> exceptionToMessage(final EgladilDuplicateEntryException e) {
		final Optional<String> message = uniqueIndexNameToMessage(e.getUniqueIndexName());
		return message;
	}

}
