//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import de.egladil.common.persistence.ILoggable;
import de.egladil.common.validation.annotations.Dateiname;
import de.egladil.common.validation.annotations.DeutscherName;

/**
 * NewsletterPayload
 */
public class NewsletterPayload implements Serializable, ILoggable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@NotBlank
	@Dateiname
	@Size(max = 150)
	private String dateinameInhalt;

	@NotBlank
	@Dateiname
	@Size(max = 150)
	private String dateinameEmfaenger;

	@NotBlank
	@DeutscherName
	@Size(max = 100)
	private String betreff;

	@Override
	public String toBotLog() {
		return "NewsletterPayload [dateinameInhalt=" + dateinameInhalt + ", dateinameEmfaenger=" + dateinameEmfaenger + ", betreff="
			+ betreff + "]";
	}

	public String getDateinameInhalt() {
		return dateinameInhalt;
	}

	public void setDateinameInhalt(final String dateinameInhalt) {
		this.dateinameInhalt = dateinameInhalt;
	}

	public String getDateinameEmfaenger() {
		return dateinameEmfaenger;
	}

	public void setDateinameEmfaenger(final String filenameEmpfaenger) {
		this.dateinameEmfaenger = filenameEmpfaenger;
	}

	public String getBetreff() {
		return betreff;
	}

	public void setBetreff(final String betreff) {
		this.betreff = betreff;
	}

}
