//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * CSPFilter (ContentSecurityPolicy)
 */
public class CSPFilter implements Filter {

	private static final String CONTENT_SECURITY_POLICY = "Content-Security-Policy";

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
		throws IOException, ServletException {

		final HttpServletResponse resp = (HttpServletResponse) response;

		// @formatter:off
		resp.addHeader(CONTENT_SECURITY_POLICY,
			"default-src 'self'; ");
		// @formatter:on

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
