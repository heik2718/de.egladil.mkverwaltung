//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import java.security.Principal;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ByteArrayStreamingOutput;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.mkv.adminservice.payload.response.RootgruppeResponsePayload;
import de.egladil.mkv.adminservice.service.TeilnahmegruppenService;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;
import de.egladil.mkv.auswertungen.statistik.SchulstatistikService;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungDownload;
import de.egladil.mkv.persistence.payload.request.AuswertungsgruppeKuerzel;
import de.egladil.mkv.persistence.payload.request.DownloadcodePayload;
import de.egladil.mkv.persistence.payload.response.DownloadArt;
import de.egladil.mkv.persistence.payload.response.DownloadCode;
import io.dropwizard.auth.Auth;

/**
 * AuswertungsgruppenResource
 */
@Singleton
@Path("/auswertungsgruppen")
@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class AuswertungsgruppenResource {

	private static final Logger LOG = LoggerFactory.getLogger(AuswertungsgruppenResource.class);

	private ResourceBundle applicationMessages;

	private final TeilnahmegruppenService auswertungsruppenService;

	private final SchulstatistikService schulstatistikService;

	private final ResourceExceptionHandler exceptionHandler;

	private final ValidationDelegate validationDelegate = new ValidationDelegate();

	/**
	 * AuswertungsgruppenResource
	 */
	@Inject
	public AuswertungsgruppenResource(final TeilnahmegruppenService auswertungsruppenService,
		final SchulstatistikService schulstatistikService) {
		this.auswertungsruppenService = auswertungsruppenService;
		this.schulstatistikService = schulstatistikService;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
		this.applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);
	}

	/**
	 * Läd alle rootgruppen zum gegebenen Jahr herunter.
	 *
	 * @param principal
	 * @param jahr
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/rootgruppen/{jahr}")
	@Timed
	public Response loadAuswertungsgruppen(@Auth final Principal principal, @PathParam(value = "jahr") final int jahr) {
		try {
			final List<RootgruppeResponsePayload> trefferliste = auswertungsruppenService.getRootauswertungsgruppen(jahr);
			final APIResponsePayload responsePayload = new APIResponsePayload(APIMessage.info(""), trefferliste);
			return Response.ok().entity(responsePayload).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/rootgruppen/{kuerzel}/schulstatistik")
	@Timed
	public Response getUebesichtAuswertungsgruppe(@Auth final Principal principal, @PathParam("kuerzel") final String kuerzel) {

		try {
			validationDelegate.check(new AuswertungsgruppeKuerzel(kuerzel), AuswertungsgruppeKuerzel.class);

			final Optional<String> optDownloadCode = schulstatistikService.generiereStatistikAktuellesJahr(principal.getName(),
				kuerzel);

			if (!optDownloadCode.isPresent()) {
				throw new ResourceNotFoundException("konnte keine Auswertung generieren");
			}

			final String downloadCode = optDownloadCode.get();
			final DownloadCode payload = new DownloadCode(downloadCode, DownloadArt.STATISTIK_SCHULE);
			final APIMessage msg = APIMessage.info(applicationMessages.getString("statistik.schuluebersicht.created"));
			final APIResponsePayload entity = new APIResponsePayload(msg, payload);

			return Response.status(Status.CREATED).entity(entity).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Fehler beim Erzeugen Schulübersicht zur Rootauswertungsgruppe: {}, message: {}", kuerzel, e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

	@OPTIONS
	@Path("/rootgruppen/schuluebersichten/{downloadcode}")
	public Response optionsDownload(@PathParam("downloadcode") final String downloadcode) {
		return Response.ok().build();
	}

	@GET
	@Path("/rootgruppen/schuluebersichten/{downloadcode}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
	@Timed
	public Response downloadFile(@Auth final Principal principal, @PathParam("downloadcode") final String downloadcode) {

		final String benutzerUuid = principal.getName();
		DownloadcodePayload payload = null;
		try {
			payload = new DownloadcodePayload(downloadcode);
			validationDelegate.check(payload, DownloadcodePayload.class);

			final Optional<AuswertungDownload> opt = schulstatistikService.findDownload(downloadcode);
			if (opt.isPresent()) {
				final AuswertungDownload download = opt.get();
				final ByteArrayStreamingOutput result = new ByteArrayStreamingOutput(download.getDaten());
				final ContentDisposition contentDisposition = ContentDisposition.type("attachment")
					.fileName("auswertung_minikaenguru_" + downloadcode + ".pdf").build();
				final Response response = Response.ok(result).header("Content-Type", "application/octet-stream")
					.header("Content-Disposition", contentDisposition).build();

				schulstatistikService.deleteDownloadQietly(benutzerUuid, download);
				return response;
			} else {
				LOG.warn("kein Auswertungsdownload mit downloadcode {} vorhanden", downloadcode);
				final APIResponsePayload entity = new APIResponsePayload(
					APIMessage.error(applicationMessages.getString("statistik.schuluebersicht.notFound")));
				return Response.status(Status.NOT_FOUND).entity(entity).build();
			}
		} catch (final EgladilWebappException e) {
			LOG.debug(e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.error("Auth: {} - {}", e.getClass().getSimpleName(), benutzerUuid);
			return exceptionHandler.mapToMKVApiResponse(e, null, payload);
		} catch (final Throwable e) {
			LOG.error(GlobalConstants.LOG_PREFIX_DOWNLOAD + e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}
}
