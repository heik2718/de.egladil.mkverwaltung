//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

/**
 * IOriginalUploadStrategy
 */
public interface IOriginalUploadStrategy {

	/**
	 * @param idNeuesUpload Long fürs Protokoll.
	 * @return String eine Message für das Protokoll.
	 *
	 */
	String updateOriginalUpload(Long idNeuesUpload);

}
