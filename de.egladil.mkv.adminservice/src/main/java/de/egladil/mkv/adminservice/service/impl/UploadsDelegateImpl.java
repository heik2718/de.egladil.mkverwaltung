//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.adminservice.exception.MkvAdminException;
import de.egladil.mkv.adminservice.payload.response.AuswertungstabelleDateiInfo;
import de.egladil.mkv.adminservice.payload.response.AuswertungstabelleInfo;
import de.egladil.mkv.adminservice.service.UploadsDelegate;
import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.auswertungen.files.UploadDelegate;
import de.egladil.mkv.auswertungen.files.impl.UploadUtils;
import de.egladil.mkv.auswertungen.persistence.IUploadListener;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.domain.IMKVKonto;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.domain.teilnahmen.Upload;
import de.egladil.mkv.persistence.file.VerzeichnisAuflistenCommand;
import de.egladil.mkv.persistence.service.IdentifierMKVKontoProvider;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;
import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * UploadsDelegateImpl
 */
@Singleton
public class UploadsDelegateImpl implements UploadsDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(UploadsDelegate.class);

	private final IUploadDao uploadDao;

	private final IUploadListener uploadListener;

	private final AuswertungstabelleFileWriter fileWriter;

	private final VerzeichnisAuflistenCommand verzeichnisAuflistenCommand;

	private final IProtokollService protokollService;

	private final IPrivatkontoDao privatkontoDao;

	private final ILehrerkontoDao lehrerkontoDao;

	private final TeilnehmerFacade teilnehmerFacade;

	/**
	 * Erzeugt eine Instanz von UploadsDelegateImpl
	 */
	@Inject
	public UploadsDelegateImpl(final IUploadDao uploadDao, final UploadDelegate uploadDelegate,
		final IUploadListener uploadListener, final AuswertungstabelleFileWriter fileWriter,
		final IProtokollService protokollService, final IPrivatkontoDao privatkontoDao, final ILehrerkontoDao lehrerkontoDao,
		final TeilnehmerFacade teilnehmerFacade) {
		super();
		this.uploadDao = uploadDao;
		this.uploadListener = uploadListener;
		this.verzeichnisAuflistenCommand = new VerzeichnisAuflistenCommand();
		this.fileWriter = fileWriter;
		this.protokollService = protokollService;
		this.privatkontoDao = privatkontoDao;
		this.lehrerkontoDao = lehrerkontoDao;
		this.teilnehmerFacade = teilnehmerFacade;
	}

	@Override
	public List<AuswertungstabelleInfo> getContentsUploadDir(final String uploadPath) {

		final List<File> dateien = verzeichnisAuflistenCommand.listFilesAndLogIOException(uploadPath);
		final List<AuswertungstabelleInfo> result = new ArrayList<>();

		for (final File file : dateien) {
			if (file.isFile()) {
				final AuswertungstabelleInfo info = new AuswertungstabelleInfo();

				final float kB = file.length() / 1024;
				final String fileName = file.getName();

				AuswertungstabelleDateiInfo dateiInfo = new AuswertungstabelleDateiInfo();

				dateiInfo.setDateiname(fileName);
				dateiInfo.setGroesse(kB + " kB");

				TeilnahmeIdentifier teilnahmeIdentifier = getTeilnahmeIdentifier(fileName);

				if (teilnahmeIdentifier != null) {
					dateiInfo.setTeilnahmeart(teilnahmeIdentifier.getTeilnahmeart());
					dateiInfo.setTeilnahmekuerzel(teilnahmeIdentifier.getKuerzel());
					dateiInfo.setUploadStatus(getUploadStatus(fileName));

					info.setDateiInfo(dateiInfo);

					String theFilenameInDB = fileName.replace(dateiInfo.getUploadStatus().getFileExtension(), "");
					String checksum = getChecksumme(theFilenameInDB);

					if (checksum != null) {
						Optional<Upload> optUpload = uploadDao.findUpload(teilnahmeIdentifier, checksum);
						if (optUpload.isPresent()) {
							info.setUpload(optUpload.get());
							result.add(info);
						} else {
							LOG.warn("Kein Eintrag in mit {} und checksumme=[{}] in uploads. [{}] wird ignoriert",
								teilnahmeIdentifier, checksum, fileName);
						}
					}
				}
			}
		}

		return result;
	}

	TeilnahmeIdentifier getTeilnahmeIdentifier(final String fileName) {
		String[] parts = StringUtils.split(fileName, '_');

		if (parts.length < 2) {
			LOG.info("Datei [{}] wird ignoriert", fileName);
			return null;
		}

		try {
			Teilnahmeart teilnahmeArt = Teilnahmeart.valueOf(parts[0]);
			String teilnahmekuerzel = parts[1];
			String wettbewerbsjahr = KontextReader.getInstance().getKontext().getWettbewerbsjahr();

			return TeilnahmeIdentifier.create(teilnahmeArt, teilnahmekuerzel, wettbewerbsjahr);

		} catch (IllegalArgumentException e) {
			LOG.info("Datei [{}] wird ignoriert", fileName);
			return null;
		}
	}

	String getChecksumme(final String dateiname) {
		String[] parts = StringUtils.split(dateiname, '_');

		if (parts.length < 2) {
			LOG.info("Datei [{}] wird ignoriert", dateiname);
			return null;
		}

		String withExtension = parts[2];
		if (withExtension.contains(".")) {
			String[] partsWithPunkt = StringUtils.split(withExtension, '.');

			if (partsWithPunkt.length > 0) {
				return partsWithPunkt[0];
			}
		}
		return null;
	}

	UploadStatus getUploadStatus(final String fileName) {
		for (UploadStatus status : UploadStatus.getVerarbeiteteStatus()) {
			if (fileName.endsWith(status.getFileExtension())) {
				return status;
			}
		}
		return UploadStatus.NEU;
	}

	@Override
	public List<AuswertungstabelleInfo> updateUpload(final String benutzerUuid, final byte[] data,
		final TeilnahmeIdentifier teilnahmeIdentifier, final String checksumme, final String pathUploadDir,
		final String pathSandboxDir) throws NullPointerException, IOException, URISyntaxException, EgladilStorageException {

		Optional<Upload> optOriginalUpload = uploadDao.findUpload(teilnahmeIdentifier, checksumme);

		if (optOriginalUpload.isPresent()) {
			Upload originalUpload = optOriginalUpload.get();
			String dateinameOriginalUpload = originalUpload.getDateiname() + originalUpload.getUploadStatus().getFileExtension();

			IOriginalUploadStrategy originalUploadStrategy = new UpdateOriginalUploadStrategy(uploadDao, fileWriter, originalUpload,
				pathUploadDir, dateinameOriginalUpload);

			List<AuswertungstabelleInfo> result = this.mergeUpload(benutzerUuid, data, teilnahmeIdentifier, pathUploadDir,
				pathSandboxDir, originalUploadStrategy);
			return result;
		} else {
			throw new MkvAdminException("Kein upload mit Teilnahmeidentifier=" + teilnahmeIdentifier + " und checksumme=["
				+ checksumme + "] in uploads vorhanden");
		}
	}

	@Override
	public List<AuswertungstabelleInfo> insertUpload(final String benutzerUuid, final byte[] data, final String kontoUuid,
		final String pathUploadDir, final String pathSandboxDir)
		throws NullPointerException, IOException, URISyntaxException, EgladilStorageException {
		if (benutzerUuid == null) {
			throw new NullPointerException("benutzerUuid null");
		}
		if (data == null) {
			throw new NullPointerException("data null");
		}
		if (kontoUuid == null) {
			throw new NullPointerException("kontoUuid null");
		}

		final Optional<IMKVKonto> optKonto = new IdentifierMKVKontoProvider().findMKVBenutzerByBenutzerUuid(kontoUuid,
			lehrerkontoDao, privatkontoDao);
		if (!optKonto.isPresent()) {
			throw new MkvAdminException("kein Lehrerkonto oder Privatkonto mit UUID [" + kontoUuid + "] gefunden");
		}
		final IMKVKonto konto = optKonto.get();

		Optional<TeilnahmeIdentifier> optTeilnahmeIdentifier = konto
			.getTeilnahmeIdentifier(KontextReader.getInstance().getKontext().getWettbewerbsjahr());

		if (!optTeilnahmeIdentifier.isPresent()) {
			throw new MkvAdminException("Benutzer mit UUID [" + kontoUuid + "] nicht zum aktuellen Wettbewerbsjahr angemeldet");
		}
		TeilnahmeIdentifier teilnahmeIdentifier = optTeilnahmeIdentifier.get();

		long anzahlTeilnehmer = this.teilnehmerFacade.getAnzahlTeilnehmer(teilnahmeIdentifier);

		if (anzahlTeilnehmer > 0) {
			throw new MkvAdminException("Für Teilnahme " + teilnahmeIdentifier
				+ " wurde bereits Online-Auswertung begonnen. Konto-UUID=[" + kontoUuid + "]");
		}

		IOriginalUploadStrategy originalUploadStrategy = new IgnoreOriginalUploadStrategy(kontoUuid);
		List<AuswertungstabelleInfo> result = this.mergeUpload(benutzerUuid, data, teilnahmeIdentifier, pathUploadDir,
			pathSandboxDir, originalUploadStrategy);
		return result;
	}

	private List<AuswertungstabelleInfo> mergeUpload(final String benutzerUuid, final byte[] data,
		final TeilnahmeIdentifier teilnahmeIdentifier, final String pathUploadDir, final String pathSandboxDir,
		final IOriginalUploadStrategy originalUploadStrategy) throws NullPointerException, IOException {
		Upload neuesUpload = null;

		try {
			fileWriter.initPaths(pathUploadDir, pathSandboxDir);
			neuesUpload = fileWriter.writeFile(data, teilnahmeIdentifier, benutzerUuid);
			neuesUpload.initWith(teilnahmeIdentifier);
			Optional<Upload> optVorhanden = uploadDao.findUpload(teilnahmeIdentifier, neuesUpload.getChecksumme());

			if (!optVorhanden.isPresent()) {
				Upload persistiert = uploadDao.persist(neuesUpload);
				uploadListener.processUpload(persistiert.getId(), pathUploadDir);
				String msg = originalUploadStrategy.updateOriginalUpload(persistiert.getId());
				final Ereignis ereignis = new Ereignis(Ereignisart.UPLOAD_VERARBEITET.getKuerzel(), benutzerUuid, msg);
				protokollService.protokollieren(ereignis);
			} else {
				String oldFilename = optVorhanden.get().getDateiname();
				fileWriter.renameFileQuietly(pathUploadDir, oldFilename, UploadStatus.IGNORE.getFileExtension());
				LOG.info("Upload {} gibt es schon: wird ignoriert");
			}
			List<AuswertungstabelleInfo> result = getContentsUploadDir(pathUploadDir);
			return result;
		} catch (final ParserSecurityException e) {
			LOG.warn("{} {} hat Datei mit potentiell gefaehrlichem Inhalt hochgeladen: {}", GlobalConstants.LOG_PREFIX_BOT,
				benutzerUuid, e.getMessage());
			return null;
		} catch (final PersistenceException e) {
			if (neuesUpload != null) {
				fileWriter.deleteDateiQuietly(neuesUpload.getMimetype(), neuesUpload.getChecksumme());
			}
			final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optConc.isPresent()) {
				throw new EgladilConcurrentModificationException("Jemand anders hat den Upload vorher geaendert");
			}
			throw new EgladilStorageException("PersistenceException beim Speichern eines Uploads: " + e.getMessage(), e);
		}
	}

	@Override
	public List<AuswertungstabelleInfo> verschiebeDateien(final String[] dateinamen, final String pathUploadDir)
		throws IOException {

		final String pathTargetDir = pathUploadDir + File.separator + "done" + File.separator
			+ KontextReader.getInstance().getKontext().getWettbewerbsjahr();

		new UploadUtils().createDirIfNotExists(pathTargetDir);

		int errors = 0;
		for (String dateiname : dateinamen) {
			try {
				fileWriter.moveFile(pathUploadDir, pathTargetDir, dateiname);
			} catch (IOException e) {
				LOG.error(e.getMessage());
				errors++;
			}
		}

		LOG.info("Dateien wurden verschoben: Anzahl gesamt={}, Anzahl Fehler = {}", dateinamen.length, errors);
		List<AuswertungstabelleInfo> result = getContentsUploadDir(pathUploadDir);
		return result;
	}

}
