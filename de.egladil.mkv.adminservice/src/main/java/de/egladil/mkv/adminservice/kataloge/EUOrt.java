//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.kataloge;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * GenOrt
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EUOrt {

	@XmlElement(name = "name")
	private String name;

	@XmlElement(name = "url")
	private String url;

	@XmlTransient
	private EULand land;

	@XmlElement(name = "schule")
	private List<EUSchule> schulen = new ArrayList<>();

	public void addSchule(EUSchule schule) {
		if (!schulen.contains(schule)) {
			schulen.add(schule);
			schule.setOrt(this);
		}
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EUOrt other = (EUOrt) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param name neuer Wert der Membervariablen name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Liefert die Membervariable url
	 *
	 * @return die Membervariable url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param url neuer Wert der Membervariablen url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Liefert die Membervariable land
	 *
	 * @return die Membervariable land
	 */
	public EULand getLand() {
		return land;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param land neuer Wert der Membervariablen land
	 */
	public void setLand(EULand land) {
		this.land = land;
	}
}
