//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.egladil.mkv.persistence.domain.kataloge.Ort;
import de.egladil.mkv.persistence.payload.response.PublicOrt;

/**
 * OrtsucheTreffer
 */
@Deprecated
public class OrtsucheTreffer implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private List<PublicOrt> orte = new ArrayList<>();

	@JsonIgnore
	public void addOrte(final List<Ort> trefferliste) throws IllegalArgumentException {
		if (trefferliste == null) {
			throw new IllegalArgumentException("trefferliste darf nicht null sein");
		}
		for (final Ort o : trefferliste) {
			final PublicOrt po = PublicOrt.createMitSchulen(o);
			if (!orte.contains(po)) {
				orte.add(po);
			}
		}
	}

	@JsonIgnore
	public int size() {
		return orte.size();
	}

	/**
	 * Liefert die Membervariable orte
	 *
	 * @return die Membervariable orte
	 */
	public List<PublicOrt> getOrte() {
		return Collections.unmodifiableList(orte);
	}

}
