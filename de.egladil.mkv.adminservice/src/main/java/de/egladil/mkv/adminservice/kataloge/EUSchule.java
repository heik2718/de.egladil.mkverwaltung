//=====================================================
// Projekt: de.egladil.misc
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.kataloge;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * EUSchule
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EUSchule {

	@XmlElement(name = "name")
	private String name;

	@XmlElement(name = "typ")
	private String schultyp;

	@XmlElement(name = "strasse")
	private String strasse;

	@XmlElement(name = "ort")
	private String nameOrt;

	@XmlTransient
	private EUOrt ort;

	@XmlElement(name = "url")
	private String url;

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EUSchule [name=" + name + ", schultyp=" + schultyp + ", strasse=" + strasse + ", ort=" + nameOrt + ", url=" + url + "]";
	}

	/**
	 * Liefert die Membervariable name
	 *
	 * @return die Membervariable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param name neuer Wert der Membervariablen name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Liefert die Membervariable schulinfo
	 *
	 * @return die Membervariable schulinfo
	 */
	public String getSchultyp() {
		return schultyp;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param schulinfo neuer Wert der Membervariablen schulinfo
	 */
	public void setSchultyp(String schulinfo) {
		this.schultyp = schulinfo;
	}

	/**
	 * Liefert die Membervariable ort
	 *
	 * @return die Membervariable ort
	 */
	public String getNameOrt() {
		return nameOrt;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param ort neuer Wert der Membervariablen ort
	 */
	public void setNameOrt(String ort) {
		this.nameOrt = ort;
	}

	/**
	 * Liefert die Membervariable strasse
	 *
	 * @return die Membervariable strasse
	 */
	public String getStrasse() {
		return strasse;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param strasse neuer Wert der Membervariablen strasse
	 */
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	/**
	 * Liefert die Membervariable url
	 *
	 * @return die Membervariable url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param url neuer Wert der Membervariablen url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	* Liefert die Membervariable ort
	* @return die Membervariable  ort
	*/
	public EUOrt getOrt() {
		return ort;
	}

	/**
	* Setzt die Membervariable
	* @param ort  neuer Wert der Membervariablen ort
	*/
	public void setOrt(EUOrt ort) {
		this.ort = ort;
	}

	/**
	* @see java.lang.Object#hashCode()
	*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ort == null) ? 0 : ort.hashCode());
		return result;
	}

	/**
	* @see java.lang.Object#equals(java.lang.Object)
	*/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EUSchule other = (EUSchule) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ort == null) {
			if (other.ort != null)
				return false;
		} else if (!ort.equals(other.ort))
			return false;
		return true;
	}
}
