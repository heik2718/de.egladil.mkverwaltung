//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.mkv.adminservice.AbstractGuiceIT;
import de.egladil.mkv.adminservice.payload.request.NewsletterPayload;

/**
 * MailqueueResourceIT
 */
public class MailqueueResourceIT extends AbstractGuiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(MailqueueResourceIT.class);

	@Inject
	private MailqueueResource resource;

	private final Principal principal = new Principal() {

		@Override
		public String getName() {
			return "67289f2c-85a3-4cec-8b97-f42460f0679b";
		}
	};


	@Test
	public void queueMails_wirft_902() throws Exception {
		// Arrange
		resource.setMaxAnzMailempfaenger(2);
		final NewsletterPayload payload = new NewsletterPayload();
		payload.setBetreff("Minikänguru 2017 Unterlagen");
		payload.setDateinameEmfaenger("freischaltung-pri vat-empfaenger");
		payload.setDateinameInhalt("freigabeUnterlagen.txt");

		// Act
		final Response response = resource.queueMails(principal, payload);

		// Assert
		assertEquals(902, response.getStatus());

		final ConstraintViolationMessage msg = (ConstraintViolationMessage) response.getEntity();
		LOG.debug(new ObjectMapper().writeValueAsString(msg));

	}

}
