//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.adminservice.AbstractGuiceIT;
import de.egladil.mkv.adminservice.payload.response.RootgruppeResponsePayload;
import de.egladil.mkv.adminservice.service.TeilnahmegruppenService;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Filterart;
import de.egladil.mkv.persistence.payload.response.teilnahmen.Teilnahmegruppe;

/**
 * TeilnahmegruppenServiceIT
 */
public class TeilnahmegruppenServiceIT extends AbstractGuiceIT {

	@Inject
	private TeilnahmegruppenService service;

	@Test
	void getRootauswertungsgruppenKlappt() {

		final List<RootgruppeResponsePayload> alle = service.getRootauswertungsgruppen(2018);
		assertFalse(alle.isEmpty());

		for (final RootgruppeResponsePayload p : alle) {
			assertNotNull(p.getRootgruppe());

			final SchulteilnahmeReadOnly teilnahme = p.getSchulteilnahme();
			final String message = "Fehler bei " + p.getRootgruppe().getKuerzel();
			assertNotNull(teilnahme, message);
			assertNotNull(teilnahme.getLand(), message);
			assertNotNull(teilnahme.getLandkuerzel(), message);
			assertNotNull(teilnahme.getOrt(), message);
			assertNotNull(teilnahme.getOrtkuerzel(), message);
			assertNotNull(teilnahme.getSchule(), message);
			assertNotNull(teilnahme.getSchulkuerzel(), message);

		}
	}

	@Test
	void getTeilnahmegruppen2018() {
		// Arrange
		final String jahr = "2018";

		// Act
		final List<Teilnahmegruppe> result = service.getTeilnahmegruppen(jahr);

		// Assert
		assertEquals(9, result.size());

		final long anzahlKinder = result.stream().mapToLong(g -> g.getAnzahlKinder()).sum();
		assertEquals(149l, anzahlKinder);

		final long anzahlSchulen = result.stream().filter(g -> g.getFilter().getFilterart() == Filterart.LAND)
			.mapToLong(g -> g.getAnzahlAnmeldungen()).sum();
		assertEquals(18, anzahlSchulen);

		final long anzahlPrivatanmeldungen = result.stream().filter(g -> g.getFilter().getFilterart() == Filterart.PRIVAT)
			.mapToLong(g -> g.getAnzahlAnmeldungen()).sum();
		assertEquals(21, anzahlPrivatanmeldungen);
	}
}
