//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.kataloge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.base.Splitter;

/**
 * ReadThueringenTest
 */
@Ignore
public class ReadThueringenIT {

	private JAXBContext jaxbContext;

	@BeforeEach
	public void setUp() throws Exception {
		jaxbContext = JAXBContext.newInstance(EUSchulliste.class);
	}

	@Test
	public void readThueringen() throws Exception {
		final File file = new File("/home/heike/executablejars/crawler/schulliste_eu/th/grundschulen_thueringen_name_ort_strasse.csv");
		BufferedReader br = null;
		String line = "";
		final Splitter LINE_SPLITTER = Splitter.on(",").trimResults();
		final EUSchulliste schulliste = new EUSchulliste();

		try {
			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				final List<String> strings = LINE_SPLITTER.splitToList(line);
				final EUSchule schule = new EUSchule();
				schule.setName(strings.get(0));
				schule.setNameOrt(strings.get(1));
				schule.setStrasse(strings.get(2));
				schulliste.addSchule(schule);
			}
		} finally {
			IOUtils.closeQuietly(br);
		}

		assertEquals(456, schulliste.getSchulen().size());
		final File target = new File("/home/heike/executablejars/crawler/schulliste_eu/th/th_0.xml");
		writeOutput(schulliste, target);
		assertTrue(target.canRead());
	}

	@Test
	public void berlinSichern() throws Exception {
		InputStream in = null;
		try {
			in = getClass().getResourceAsStream("/fertig_schulen_berlin.xml");
			final EUSchulliste schulliste = (EUSchulliste) jaxbContext.createUnmarshaller().unmarshal(in);

			final File target = new File("/home/heike/executablejars/crawler/schulliste_eu/be/be_0.xml");
			writeOutput(schulliste, target);
			assertTrue(target.canRead());
		} finally {
			IOUtils.closeQuietly(in);
		}

	}

	public void writeOutput(final EUSchulliste katalog, final File file) throws JAXBException {
		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.marshal(katalog, file);
	}
}
