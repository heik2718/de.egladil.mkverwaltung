//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.mkv.adminservice.AbstractGuiceIT;
import de.egladil.mkv.adminservice.TestUtils;
import de.egladil.mkv.adminservice.payload.request.BenutzerSuchmodus;
import de.egladil.mkv.adminservice.payload.response.BenutzersucheTreffer;

/**
 * BenutzerResourceIT
 */
public class BenutzerResourceIT extends AbstractGuiceIT {

	private static final String BENUTZR_UUID = "67289f2c-85a3-4cec-8b97-f42460f0679b";

	private static final Principal PRINCIPAL = new EgladilPrincipal(BENUTZR_UUID);

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerResourceIT.class);

	private ObjectMapper objectMapper = new ObjectMapper();

	@Override
	@BeforeEach
	public void setUp() {
		TestUtils.initMKVApiKontextReader("2017", true);
	}

	@Inject
	private BenutzerResource benutzerResource;

	@Test
	public void findBenutzerWithEmail_klappt() throws Exception {
		// Act
		final Response response = benutzerResource.findBenutzer(PRINCIPAL, BenutzerSuchmodus.EMAIL, "ster");

		// Assert
		assertEquals(200, response.getStatus());
		final BenutzersucheTreffer payload = (BenutzersucheTreffer) response.getEntity();
		final String json = objectMapper.writeValueAsString(payload);
		LOG.debug(json);

		LOG.debug(payload.toString());
	}

	@Test
	public void findBenutzerWithEmail_returns_404() throws Exception {
		// Act
		final Response response = benutzerResource.findBenutzer(PRINCIPAL, BenutzerSuchmodus.EMAIL, "eine-unbekannte@mail.de");

		// Assert
		assertEquals(404, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void findBenutzerWithEmail_returns_400() throws Exception {
		// Act
		final Response response = benutzerResource.findBenutzer(PRINCIPAL, BenutzerSuchmodus.EMAIL, "üüü");

		// Assert
		assertEquals(400, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void findBenutzerWithEmail_returns_400_wenn_leer() throws Exception {
		// Act
		final Response response = benutzerResource.findBenutzer(PRINCIPAL, BenutzerSuchmodus.EMAIL, " ");

		// Assert
		assertEquals(400, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void getBenutzer_klappt() throws Exception {
		// Act
		final Response response = benutzerResource.getBenutzerWithUuid(PRINCIPAL, "fd77d23c");

		// Assert
		assertEquals(200, response.getStatus());
		final Object payload = response.getEntity();
		final String json = objectMapper.writeValueAsString(payload);
		LOG.debug(json);

		LOG.debug(payload.toString());

	}

	@Test
	public void getBenutzer_returns_404() throws Exception {
		// Act
		final Response response = benutzerResource.getBenutzerWithUuid(PRINCIPAL, BENUTZR_UUID);

		// Assert
		assertEquals(404, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void getBenutzer_returns_400_wenn_ungueltig() throws Exception {
		// Act
		final Response response = benutzerResource.getBenutzerWithUuid(PRINCIPAL, "script{evil}");

		// Assert
		assertEquals(400, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void getBenutzer_returns_400_wenn_leer() throws Exception {
		// Act
		final Response response = benutzerResource.getBenutzerWithUuid(PRINCIPAL, "");

		// Assert
		assertEquals(400, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}
}
