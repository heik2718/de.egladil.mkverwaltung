//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.common.config.OsUtils;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.auswertungen.files.AuswertungstabelleFileWriter;
import de.egladil.mkv.auswertungen.files.UploadDelegate;
import de.egladil.mkv.auswertungen.persistence.IUploadListener;
import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.dao.ILehrerkontoDao;
import de.egladil.mkv.persistence.dao.IPrivatkontoDao;
import de.egladil.mkv.persistence.dao.IUploadDao;
import de.egladil.mkv.persistence.domain.enums.UploadStatus;
import de.egladil.mkv.persistence.domain.teilnahmen.TeilnahmeIdentifier;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * UploadsDelegateImplTest
 */
public class UploadsDelegateImplTest {

	private IUploadDao uploadDao;

	private UploadDelegate uploadDelegate;

	private IUploadListener uploadListener;

	private UploadsDelegateImpl delegate;

	private AuswertungstabelleFileWriter fileWriter;

	private IProtokollService protokollService;

	@BeforeEach
	public void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		this.uploadDao = Mockito.mock(IUploadDao.class);
		this.uploadListener = Mockito.mock(IUploadListener.class);
		this.uploadDelegate = Mockito.mock(UploadDelegate.class);
		this.fileWriter = Mockito.mock(AuswertungstabelleFileWriter.class);
		this.protokollService = Mockito.mock(IProtokollService.class);
		this.delegate = new UploadsDelegateImpl(uploadDao, uploadDelegate, uploadListener, fileWriter, protokollService,
			Mockito.mock(IPrivatkontoDao.class), Mockito.mock(ILehrerkontoDao.class), Mockito.mock(TeilnehmerFacade.class));
	}

	@Test
	void getTeilnahmeIdentifierKlappt() {
		// Arrange
		String fileName = "S_093O1J71_bc7a4930df994e8872061fa789200a9e08c7f67aaced80fae744103d44cb9508.ods.done";
		TeilnahmeIdentifier expected = TeilnahmeIdentifier.createSchulteilnahmeIdentifier("093O1J71",
			KontextReader.getInstance().getKontext().getWettbewerbsjahr());

		// Act
		TeilnahmeIdentifier actual = delegate.getTeilnahmeIdentifier(fileName);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void getUploadStatusAlle() {
		// Act + Assert
		assertEquals(UploadStatus.FEHLER, delegate.getUploadStatus("hsaKA_sakgs.ods.error"));
		assertEquals(UploadStatus.FERTIG, delegate.getUploadStatus("hsaKA_sakgs.ods.done"));
		assertEquals(UploadStatus.LEER, delegate.getUploadStatus("hsaKA_sakgs.ods.leer"));
		assertEquals(UploadStatus.NEU, delegate.getUploadStatus("hsaKA_sakgs.ods"));
		assertEquals(UploadStatus.KORRIGIERT, delegate.getUploadStatus("hgasg_fszufdz.ods.korr"));

	}
}
