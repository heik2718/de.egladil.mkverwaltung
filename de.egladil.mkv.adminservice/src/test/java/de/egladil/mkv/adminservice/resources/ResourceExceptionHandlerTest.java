//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.NotImplementedException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.persistence.protokoll.impl.ProtokollService;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.mkv.adminservice.payload.request.LandPayload;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;

/**
 * ResourceExceptionHandlerTest
 */
public class ResourceExceptionHandlerTest {

	private static final Logger LOG = LoggerFactory.getLogger(ResourceExceptionHandlerTest.class);

	private static final String BENUTZER_UUID = "jhafhahanklasl";

	private IProtokollService protokollService;

	private ResourceExceptionHandler exceptionHandler;

	@BeforeEach
	public void setUp() {
		protokollService = Mockito.mock(ProtokollService.class);
		exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
	}

	@Test
	public void exceptionToResponse_reicht_webapp_exception_weiter() {
		// Arrange
		final EgladilWebappException exception = ExceptionFactory.badRequest("schlecht");
		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(400, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_duplicate_entry_generates_900() {
		// Arrange
		final EgladilDuplicateEntryException exception = new EgladilDuplicateEntryException("Hamver schon");
		exception.setUniqueIndexName("uk_laender_2");
		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(900, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_notImplemented_generates_418() {
		// Arrange
		final NotImplementedException exception = new NotImplementedException("geht noch nicht");
		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(418, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_disabled_account_generates_401() {
		// Arrange
		final DisabledAccountException exception = new DisabledAccountException("gesperrt");
		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_authentication_exception_generates_401() {
		final EgladilAuthenticationException exception = new EgladilAuthenticationException("gesperrt");

		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_incorrect_credentials_generates_401() {
		final IncorrectCredentialsException exception = new IncorrectCredentialsException("falsche Credentials");

		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_unknown_account_generates_401() {
		final IncorrectCredentialsException exception = new IncorrectCredentialsException("falscher loginname");

		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_excessive_attempt_generates_401() {
		final ExcessiveAttemptsException exception = new ExcessiveAttemptsException("zu schnell");

		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_checkedException_entry_generates_500() {
		// Arrange
		final Exception exception = new Exception("checked exception");
		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(500, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}

	@Test
	public void exceptionToResponse_uncheckedException_entry_generates_500() {
		// Arrange
		final RuntimeException exception = new RuntimeException("unchecked exception");
		final LandPayload payload = new LandPayload("EG", "Ägypten");

		final String was = payload.toBotLog();
		final Ereignis ereignis = new Ereignis(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
		Mockito.when(protokollService.protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was)).thenReturn(ereignis);

		// Act
		final Response response = exceptionHandler.mapToMKVApiResponse(exception, null, payload);

		// Assert
		assertNotNull(response);
		assertEquals(500, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
		Mockito.verify(protokollService, times(0)).protokollieren(Ereignisart.LAND_ANLEGEN.getKuerzel(), BENUTZER_UUID, was);
	}
}
