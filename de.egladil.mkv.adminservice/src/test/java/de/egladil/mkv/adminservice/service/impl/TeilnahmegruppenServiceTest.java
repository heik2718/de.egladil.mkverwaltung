//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.mkv.adminservice.payload.response.RootgruppeResponsePayload;
import de.egladil.mkv.adminservice.service.TeilnahmegruppenService;
import de.egladil.mkv.persistence.dao.ILandDao;
import de.egladil.mkv.persistence.dao.IPrivatteilnahmeInfoDao;
import de.egladil.mkv.persistence.dao.ISchulteilnahmeReadOnlyDao;
import de.egladil.mkv.persistence.domain.auswertungen.Auswertungsgruppe;
import de.egladil.mkv.persistence.domain.auswertungen.AuswertungsgruppeBuilder;
import de.egladil.mkv.persistence.domain.enums.Teilnahmeart;
import de.egladil.mkv.persistence.domain.teilnahmen.SchulteilnahmeReadOnly;
import de.egladil.mkv.persistence.service.AuswertungsgruppenService;
import de.egladil.mkv.persistence.service.TeilnahmenFacade;
import de.egladil.mkv.persistence.service.TeilnehmerFacade;

/**
 * TeilnahmegruppenServiceTest
 */
public class TeilnahmegruppenServiceTest {

	private static final String JAHR = "2018";

	private AuswertungsgruppenService embeddedService;

	private TeilnehmerFacade teilnehmerFacade;

	private ISchulteilnahmeReadOnlyDao schulteilnahmeReadOnlyDao;

	private IPrivatteilnahmeInfoDao privatteilnahmeInfoDao;

	private TeilnahmegruppenService service;

	@BeforeEach
	void setUp() {
		embeddedService = Mockito.mock(AuswertungsgruppenService.class);
		schulteilnahmeReadOnlyDao = Mockito.mock(ISchulteilnahmeReadOnlyDao.class);
		privatteilnahmeInfoDao = Mockito.mock(IPrivatteilnahmeInfoDao.class);
		teilnehmerFacade = Mockito.mock(TeilnehmerFacade.class);
		service = new TeilnahmegruppenService(embeddedService, schulteilnahmeReadOnlyDao, teilnehmerFacade, Mockito.mock(ILandDao.class),
			Mockito.mock(TeilnahmenFacade.class));
	}

	@Test
	@DisplayName("2 Auswertungsgruppen mit Schulteilnahmen, ein name gleich, einer unterschiedlich")
	void getRootauswertungsgruppenAlleVollstaendig() {

		final String teilnahmekuerzel1 = "ZHGTR54H";

		final String teilnahmekuerzel2 = "U543DRTE";

		final List<Auswertungsgruppe> auswertungsgruppen = new ArrayList<>();
		{
			final String name = "shdh saiohid shio";
			final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(Teilnahmeart.S, teilnahmekuerzel1, JAHR)
				.name(name).checkAndBuild();
			auswertungsgruppen.add(auswertungsgruppe);

			final SchulteilnahmeReadOnly teilnahme = new SchulteilnahmeReadOnly();
			teilnahme.setId(1l);
			teilnahme.setJahr(JAHR);
			teilnahme.setLand("Hessen");
			teilnahme.setLandkuerzel("de-HE");
			teilnahme.setOrt("Wiesbaden");
			teilnahme.setOrtkuerzel("TZUFIG5");
			teilnahme.setSchule(name);
			teilnahme.setSchulkuerzel(teilnahmekuerzel1);
			Mockito.when(schulteilnahmeReadOnlyDao.findBySchulkuerzelUndJahr(teilnahmekuerzel1, JAHR))
				.thenReturn(Optional.of(teilnahme));
			Mockito.when(teilnehmerFacade.getAnzahlLoesungszettel(teilnahme)).thenReturn(0l);

		}
		{
			final String name = "ahsa ashus sahu sa";
			final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(Teilnahmeart.S, teilnahmekuerzel2, JAHR)
				.name(name).checkAndBuild();
			auswertungsgruppen.add(auswertungsgruppe);

			final SchulteilnahmeReadOnly teilnahme = new SchulteilnahmeReadOnly();
			teilnahme.setId(1l);
			teilnahme.setJahr(JAHR);
			teilnahme.setLand("Hessen");
			teilnahme.setLandkuerzel("de-HE");
			teilnahme.setOrt("Wiesbaden");
			teilnahme.setOrtkuerzel("TZUFIG5");
			teilnahme.setSchule("haufogaog gqudgug");
			teilnahme.setSchulkuerzel(teilnahmekuerzel2);
			Mockito.when(schulteilnahmeReadOnlyDao.findBySchulkuerzelUndJahr(teilnahmekuerzel2, JAHR))
				.thenReturn(Optional.of(teilnahme));

			Mockito.when(teilnehmerFacade.getAnzahlLoesungszettel(teilnahme)).thenReturn(34l);

		}

		Mockito.when(embeddedService.getRootAuswertungsgruppen(JAHR)).thenReturn(auswertungsgruppen);

		// Act
		final List<RootgruppeResponsePayload> responsePayload = service.getRootauswertungsgruppen(Integer.valueOf(JAHR));

		// Assert
		assertEquals(2, responsePayload.size());
		for (final RootgruppeResponsePayload p : responsePayload) {
			if (teilnahmekuerzel1.equals(p.getRootgruppe().getTeilnahmekuerzel())) {
				assertEquals(teilnahmekuerzel1, p.getSchulteilnahme().getSchulkuerzel());
				assertFalse(p.isNameDiffersFromKatalog());
				assertEquals(0l, p.getAnzahlLoesungszettel());
			} else {
				assertEquals(teilnahmekuerzel2, p.getSchulteilnahme().getSchulkuerzel());
				assertTrue(p.isNameDiffersFromKatalog());
				assertEquals(34l, p.getAnzahlLoesungszettel());
			}
		}
	}

	@Test
	@DisplayName("2 Auswertungsgruppen mit Schulteilnahmen, eine mit, eine ohne Teilnahme")
	void getRootauswertungsgruppenEineOhneTeilnahme() {

		final String teilnahmekuerzel1 = "ZHGTR54H";

		final String teilnahmekuerzel2 = "U543DRTE";

		final List<Auswertungsgruppe> auswertungsgruppen = new ArrayList<>();
		{
			final String name = "shdh saiohid shio";
			final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(Teilnahmeart.S, teilnahmekuerzel1, JAHR)
				.name(name).checkAndBuild();
			auswertungsgruppen.add(auswertungsgruppe);

			final SchulteilnahmeReadOnly teilnahme = new SchulteilnahmeReadOnly();
			teilnahme.setId(1l);
			teilnahme.setJahr(JAHR);
			teilnahme.setLand("Hessen");
			teilnahme.setLandkuerzel("de-HE");
			teilnahme.setOrt("Wiesbaden");
			teilnahme.setOrtkuerzel("TZUFIG5");
			teilnahme.setSchule(name);
			teilnahme.setSchulkuerzel(teilnahmekuerzel1);
			Mockito.when(schulteilnahmeReadOnlyDao.findBySchulkuerzelUndJahr(teilnahmekuerzel1, JAHR))
				.thenReturn(Optional.of(teilnahme));

		}
		{
			final String name = "ahsa ashus sahu sa";
			final Auswertungsgruppe auswertungsgruppe = new AuswertungsgruppeBuilder(Teilnahmeart.S, teilnahmekuerzel2, JAHR)
				.name(name).checkAndBuild();
			auswertungsgruppen.add(auswertungsgruppe);
			Mockito.when(schulteilnahmeReadOnlyDao.findBySchulkuerzelUndJahr(teilnahmekuerzel2, JAHR)).thenReturn(Optional.empty());

		}

		Mockito.when(embeddedService.getRootAuswertungsgruppen(JAHR)).thenReturn(auswertungsgruppen);

		// Act
		final List<RootgruppeResponsePayload> responsePayload = service.getRootauswertungsgruppen(Integer.valueOf(JAHR));

		// Assert
		assertEquals(2, responsePayload.size());
		for (final RootgruppeResponsePayload p : responsePayload) {
			if (teilnahmekuerzel1.equals(p.getRootgruppe().getTeilnahmekuerzel())) {
				assertEquals(teilnahmekuerzel1, p.getSchulteilnahme().getSchulkuerzel());
				assertFalse(p.isNameDiffersFromKatalog());
			} else {
				assertNull(p.getSchulteilnahme());
			}
		}
	}
}
