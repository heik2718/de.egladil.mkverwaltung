//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.session;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;

import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.domain.Rolle;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.mkv.persistence.domain.protokoll.Ereignisart;

/**
 * SessionDelegateTest
 */
public class SessionDelegateTest {

	private static final Logger LOG = LoggerFactory.getLogger(SessionDelegateTest.class);

	private static final String PASSWORD = "hdget7Z5";

	private static final String CSRF_TOKEN = "dggasdigisuiguasg";

	private static final String BEARER = "hdget-7Z5ret";

	private static final String ACCESS_TOKEN_ID = "abe43-dd5e4-56";

	private static final String BENUTZER_UUID = "hsakhf-hafwoo412g1";

	private static final String LOGINNAME = "gashdgg ud";

	private IAccessTokenDAO accessTokenDAO;

	private IAuthenticationService authenticationService;

	private IProtokollService protokollService;

	private SessionDelegate sessionDelegate;

	private ObjectMapper mapper;

	private UsernamePasswordToken usernamePasswordToken;

	@BeforeEach
	public void setUp() {
		authenticationService = Mockito.mock(IAuthenticationService.class);
		accessTokenDAO = Mockito.mock(IAccessTokenDAO.class);
		protokollService = Mockito.mock(IProtokollService.class);
		usernamePasswordToken = new UsernamePasswordToken(LOGINNAME, PASSWORD.toCharArray());
		sessionDelegate = new SessionDelegate(accessTokenDAO, authenticationService, protokollService);
		mapper = new ObjectMapper();
	}

	@Test
	public void authenticate_wirft_IllegalArgumentException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new IllegalArgumentException("irgendwas"));

		assertThrows(IllegalArgumentException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_wirft_EgladilAuthenticationException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new EgladilAuthenticationException("irgendwas"));

		assertThrows(EgladilAuthenticationException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_wirft_UnknownAccountException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new UnknownAccountException("irgendwas"));

		assertThrows(UnknownAccountException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_wirft_IncorrectCredentialsException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new IncorrectCredentialsException("irgendwas"));

		assertThrows(IncorrectCredentialsException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_wirft_ExcessiveAttemptsException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new ExcessiveAttemptsException("irgendwas"));

		assertThrows(ExcessiveAttemptsException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_wirft_DisabledAccountException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new DisabledAccountException("irgendwas"));

		assertThrows(DisabledAccountException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_ruft_protokollieren_auf() {
		// Arrange
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setAktiviert(true);
		benutzerkonto.setUuid(BENUTZER_UUID);
		benutzerkonto.addRolle(new Rolle(Role.MKV_ADMIN));

		final AccessToken accessToken = new AccessToken(ACCESS_TOKEN_ID);
		accessToken.setCsrfToken(CSRF_TOKEN);
		Mockito.when(accessTokenDAO.generateNewAccessToken(benutzerkonto)).thenReturn(accessToken);

		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzerkonto);
		Mockito.when(protokollService.protokollieren(Ereignisart.LOGIN.getKuerzel(), BENUTZER_UUID, "MKV_ADMIN"))
			.thenReturn(new Ereignis(Ereignisart.LOGIN.getKuerzel(), BENUTZER_UUID, "MKV_ADMIN"));

		// Act
		sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);

		// Assert
		Mockito.verify(protokollService, times(1)).protokollieren(Ereignisart.LOGIN.getKuerzel(), BENUTZER_UUID, "MKV_ADMIN");
	}

	@Test
	public void authenticate_wirft_EgladilStorageException_weiter() {
		// Arrange
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new EgladilStorageException("irgendwas"));

		assertThrows(EgladilStorageException.class, () -> {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		});

	}

	@Test
	public void authenticate_ruft_invalidateTemporaryCsrfTokenQuietly() {
		// Arrange irgenwie will Mockito hier nicht mitspielen :/
		final Benutzerkonto benutzerkonto = new Benutzerkonto();
		benutzerkonto.setUuid(BENUTZER_UUID);

		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV)).thenReturn(benutzerkonto);

		final AccessToken accessToken = new AccessToken(BENUTZER_UUID);
		accessToken.setCsrfToken(CSRF_TOKEN);
		Mockito.when(accessTokenDAO.generateNewAccessToken(benutzerkonto)).thenReturn(accessToken);
		Mockito.when(accessTokenDAO.invalidateTemporaryCsrfToken(CSRF_TOKEN)).thenReturn("");

		// Act
		sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);

		// Assert
		Mockito.verify(accessTokenDAO, times(1)).invalidateTemporaryCsrfToken(CSRF_TOKEN);
	}

	@Test
	public void authenticate_ruft_invalidateTemporaryCsrfTokenQuietly_nur_falls_keine_exception() {
		Mockito.when(authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV))
			.thenThrow(new EgladilStorageException("irgendwas"));

		Mockito.when(accessTokenDAO.invalidateTemporaryCsrfToken(CSRF_TOKEN)).thenReturn("");

		// Act
		try {
			sessionDelegate.authenticate(usernamePasswordToken, CSRF_TOKEN);
		} catch (final Exception e) {

			// Assert
			Mockito.verify(accessTokenDAO, times(0)).invalidateTemporaryCsrfToken(CSRF_TOKEN);
		}
	}

	@Test
	public void logoutQuietly_ruft_invalidateSessionQuietly() {
		// Arrange irgenwie will Mockito hier nicht mitspielen :/
		Mockito.when(accessTokenDAO.invalidateAccessToken(ACCESS_TOKEN_ID)).thenReturn("");

		// Act
		sessionDelegate.logoutQuietly(BENUTZER_UUID, ACCESS_TOKEN_ID);

		// Assert
		Mockito.verify(accessTokenDAO, times(1)).invalidateAccessToken(ACCESS_TOKEN_ID);
	}

	@Test
	public void invalidateSessionQuietly_faengt_runtimeException() {
		// Arrange
		Mockito.when(accessTokenDAO.invalidateAccessToken(ACCESS_TOKEN_ID)).thenThrow(new NullPointerException("hähä"));
		try {
			sessionDelegate.invalidateSessionQuietly(ACCESS_TOKEN_ID);
		} catch (final RuntimeException e) {
			fail("hierher nich");
		}
	}

	@Test
	public void invalidateTemporaryCsrfTokenQuietly_faengt_runtimeException() {
		// Arrange
		Mockito.when(accessTokenDAO.invalidateTemporaryCsrfToken(CSRF_TOKEN)).thenThrow(new NullPointerException("hähä"));
		try {
			sessionDelegate.invalidateTemporaryCsrfTokenQuietly(CSRF_TOKEN);
		} catch (final RuntimeException e) {
			fail("hierher nich");
		}
	}
}
