//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice;

import de.egladil.mkv.persistence.config.KontextReader;
import de.egladil.mkv.persistence.payload.response.Kontext;

/**
 * TestUtils
 */
public final class TestUtils {

	private TestUtils() {
	}

	/**
	 * Gibt dem Pfad zum Dev-Config-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevNewsletterDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/files/mails";
		}
		return "/home/heike/git/konfigurationen/mkverwaltung/files/mkvadmin/mails";
	}

	/**
	 * Gibt dem Pfad zum Dev-Upload-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevUploadDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/upload/mkv";
		}
		return "/home/heike/upload/mkv";
	}

	/**
	 * Gibt dem Pfad zum Dev-Upload-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevSandboxDir() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/upload/mkv";
		}
		return "/home/heike/upload/mkv";
	}

	public static void initMKVApiKontextReader(final String wettbewerbsjahr, final boolean neuanmeldungFreigeschaltet) {
		KontextReader.destroy();
		final Kontext mockKontext = new Kontext();
		mockKontext.setWettbewerbsjahr(wettbewerbsjahr);
		mockKontext.setNeuanmeldungFreigegeben(neuanmeldungFreigeschaltet);
		KontextReader.getInstance().setMockKontextForTest(mockKontext);
	}

}
