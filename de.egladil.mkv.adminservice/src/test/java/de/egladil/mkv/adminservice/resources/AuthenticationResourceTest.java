//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.auth.Credentials;
import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.ExcessiveAttemptsException;
import de.egladil.common.exception.IncorrectCredentialsException;
import de.egladil.common.exception.UnknownAccountException;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.persistence.protokoll.IProtokollService;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.mkv.adminservice.session.SessionDelegate;
import de.egladil.mkv.adminservice.validation.ValidationDelegate;

/**
 * AuthenticationResourceTest
 */
public class AuthenticationResourceTest {

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationResourceTest.class);

	private SessionDelegate sessionDelegate;

	private IProtokollService protokollService;

	private ResourceExceptionHandler resourceExceptionHandler;

	private AuthenticationResource resource;

	private Credentials credentials;

	private UsernamePasswordToken usernamePasswordToken;

	private int anzCheckCalled;

	@BeforeEach
	public void setUp() {
		sessionDelegate = Mockito.mock(SessionDelegate.class);
		protokollService = Mockito.mock(IProtokollService.class);
		resourceExceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());
		resource = new AuthenticationResource(sessionDelegate, protokollService);
		credentials = new Credentials("ruth", "pazzW0rd", null);
		usernamePasswordToken = new UsernamePasswordToken("ruth", "pazzW0rd".toCharArray());
		anzCheckCalled = 0;
	}

	@Test
	public void login_gibt_400_bad_request_zurueck_wenn_credentials_null() {
		// Act
		final Response response = resource.login(null, "TEMPTOKEN");

		// Assert
		assertEquals(400, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_gibt_401_not_authorized_zurueck_bei_DisabledAccountException() {
		// Arrange
		final String tempCsrfToken = "TRFGTR";
		Mockito.when(sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken))
			.thenThrow(new DisabledAccountException("gesperrt"));

		// Act
		final Response response = resource.login(credentials, tempCsrfToken);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.WARN.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_gibt_401_not_authorized_zurueck_bei_EgladilAuthenticationException() {
		// Arrange
		final String tempCsrfToken = "TRFGTR";
		Mockito.when(sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken))
			.thenThrow(new EgladilAuthenticationException("gesperrt"));

		// Act
		final Response response = resource.login(credentials, tempCsrfToken);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_gibt_401_not_authorized_zurueck_bei_ExcessiveAttemptsException() {
		// Arrange
		final String tempCsrfToken = "TRFGTR";
		Mockito.when(sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken))
			.thenThrow(new ExcessiveAttemptsException("zu heftig"));

		// Act
		final Response response = resource.login(credentials, tempCsrfToken);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_gibt_401_not_authorized_zurueck_bei_IncorrectCredentialsException() {
		// Arrange
		final String tempCsrfToken = "TRFGTR";
		Mockito.when(sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken))
			.thenThrow(new IncorrectCredentialsException("falsche credentials"));

		// Act
		final Response response = resource.login(credentials, tempCsrfToken);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_gibt_401_not_authorized_zurueck_bei_UnknownAccountException() {
		// Arrange
		final String tempCsrfToken = "TRFGTR";
		Mockito.when(sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken))
			.thenThrow(new UnknownAccountException("kennmer nich"));

		// Act
		final Response response = resource.login(credentials, tempCsrfToken);

		// Assert
		assertEquals(401, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_gibt_500_internalServerError_zurueck_bei_beliebiger_anderer_runtimeException() {
		// Arrange
		final String tempCsrfToken = "TRFGTR";
		Mockito.when(sessionDelegate.authenticate(usernamePasswordToken, tempCsrfToken))
			.thenThrow(new RuntimeException("kennmer nich"));

		// Act
		final Response response = resource.login(credentials, tempCsrfToken);

		// Assert
		assertEquals(500, response.getStatus());
		final APIResponsePayload pm = (APIResponsePayload) response.getEntity();
		assertEquals(MessageLevel.ERROR.toString(), pm.getApiMessage().getLevel());
		LOG.info(pm.getApiMessage().getMessage());
	}

	@Test
	public void login_ruft_validate_genau_einmal_auf() {
		// Arrange
		final ValidationDelegate validationDelegate = new ValidationDelegate() {

			@Override
			public <T extends ILoggable> void check(final T payload, final Class<T> clazz) {
				anzCheckCalled++;
				super.check(payload, clazz);
			}
		};
		resource.setValidationDelegate(validationDelegate);

		// Act
		final Response response = resource.login(credentials, "XXXX");

		// Assert
		assertEquals(200, response.getStatus());
		assertEquals(1, anzCheckCalled);
	}

	@Test
	public void logout_gibt_200_zurueck_auch_bei_exception() {
		// Arrange
		final String benutzerUuid = "UUID";
		final Principal principal = new EgladilPrincipal(benutzerUuid);
		final String bearer = "Bearer Hjhqihu";
		Mockito.when(sessionDelegate.logoutQuietly(benutzerUuid, bearer))
			.thenThrow(new IllegalArgumentException("eigentlich keine"));

		// Act
		final Response response = resource.logout(principal, bearer);

		// Asset
		assertEquals(200, response.getStatus());
	}
}
