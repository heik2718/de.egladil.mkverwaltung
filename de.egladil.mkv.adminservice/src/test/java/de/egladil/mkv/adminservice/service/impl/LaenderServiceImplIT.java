//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.mkv.adminservice.AbstractGuiceIT;
import de.egladil.mkv.adminservice.service.LaenderService;
import de.egladil.mkv.persistence.payload.response.PublicLand;

/**
 * LaenderServiceImplIT
 */
public class LaenderServiceImplIT extends AbstractGuiceIT {

	@Inject
	private LaenderService service;

	@Test
	void getLaenderKlappt() {

		// Act
		final List<PublicLand> laender = service.getLaender();

		// Assert
		assertEquals(25, laender.size());
		for (final PublicLand land : laender) {
//			System.out.println(land.toString());
			assertNotNull(land.getName());
			assertNotNull(land.getKuerzel());
			if ("AU".equals(land.getKuerzel())) {
				assertEquals(0, land.getAnzahlOrte());
			} else {
				assertTrue(land.getAnzahlOrte() > 0);
			}
		}
	}
}
