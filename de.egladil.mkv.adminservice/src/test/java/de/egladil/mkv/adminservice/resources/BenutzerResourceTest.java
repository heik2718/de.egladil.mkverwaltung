//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.auth.EgladilPrincipal;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.common.persistence.EgladilStorageException;
import de.egladil.mkv.adminservice.TestUtils;
import de.egladil.mkv.adminservice.payload.request.BenutzerSuchmodus;
import de.egladil.mkv.adminservice.service.MKVBenutzerService;
import de.egladil.mkv.persistence.payload.BenutzerPayloadMapper;
import de.egladil.mkv.persistence.service.IAnonymisierungsservice;

/**
 * BenutzerResourceTest
 */
public class BenutzerResourceTest {

	private static final Principal PRINCIPAL = new EgladilPrincipal("agskgdi");

	private static final Logger LOG = LoggerFactory.getLogger(BenutzerResourceTest.class);

	private BenutzerResource benutzerResource;

	private IBenutzerService benutzerService;

	private IAnonymisierungsservice anonymisierungsservice;

	private BenutzerPayloadMapper benutzerPayloadMapper;

	private MKVBenutzerService mkvBenutzerService;

	@BeforeEach
	public void setUp() {
		benutzerService = Mockito.mock(IBenutzerService.class);
		benutzerPayloadMapper = Mockito.mock(BenutzerPayloadMapper.class);
		anonymisierungsservice = Mockito.mock(IAnonymisierungsservice.class);
		mkvBenutzerService = Mockito.mock(MKVBenutzerService.class);
		benutzerResource = new BenutzerResource(benutzerService, benutzerPayloadMapper, anonymisierungsservice, mkvBenutzerService);
		TestUtils.initMKVApiKontextReader("2017", true);
	}

	@Test
	public void findBenutzerWithEmail_returns_500() throws Exception {
		// Arrange
		final String email = "vader";
		Mockito.when(benutzerService.findByEmailLike(email, Anwendung.MKV)).thenThrow(new EgladilStorageException("asnjh"));

		// Act
		final Response response = benutzerResource.findBenutzer(PRINCIPAL, BenutzerSuchmodus.EMAIL, "vader");

		// Assert
		assertEquals(500, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void getBenutzer_returns_500() throws Exception {
		// Arrange
		final String firstChars = "fd77d23c";
		Mockito.when(benutzerService.findBenutzerkontoByUUID(firstChars)).thenThrow(new EgladilStorageException("sazd"));

		// Act
		final Response response = benutzerResource.getBenutzerWithUuid(PRINCIPAL, "fd77d23c");

		// Assert
		assertEquals(500, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

	@Test
	public void getBenutzer_returns_404() throws Exception {
		// Arrange
		final String firstChars = "fd77d23c";
		Mockito.when(benutzerService.findBenutzerkontoByUUID(firstChars)).thenReturn(null);

		// Act
		final Response response = benutzerResource.getBenutzerWithUuid(PRINCIPAL, "fd77d23c");

		// Assert
		assertEquals(404, response.getStatus());
		LOG.debug(response.getEntity().toString());
	}

}
