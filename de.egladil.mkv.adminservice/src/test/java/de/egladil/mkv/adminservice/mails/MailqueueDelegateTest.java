//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.mails;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.mkv.adminservice.TestUtils;
import de.egladil.mkv.adminservice.payload.request.NewsletterPayload;
import de.egladil.mkv.persistence.domain.user.IMailempfaenger;
import de.egladil.mkv.persistence.domain.user.LehrermailEmpfaenger;

/**
 * MailqueueDelegateTest
 */
public class MailqueueDelegateTest {

	private IMailqueueDao mailqueueDao;

	private MailqueueDelegate delegate;

	@BeforeEach
	public void setUp() {
		mailqueueDao = Mockito.mock(IMailqueueDao.class);

		delegate = new MailqueueDelegate(mailqueueDao);
	}

	@Test
	public void mailempfaengerGruppieren_restklasse2_klappt() {
		// Arrange
		final List<LehrermailEmpfaenger> alle = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			final LehrermailEmpfaenger e = new LehrermailEmpfaenger();
			e.setEmail("mail@provider.de");
			e.setId(Long.valueOf(i));
			alle.add(e);
		}

		// Act
		final List<List<LehrermailEmpfaenger>> gruppen = delegate.mailempfaengerGruppieren(alle, 6);

		// Assert
		final Set<Long> ids = new HashSet<>();
		assertEquals(4, gruppen.size());
		for (int i = 0; i < 4; i++) {
			final List<LehrermailEmpfaenger> gruppe = gruppen.get(i);
			for (final LehrermailEmpfaenger e : gruppe) {
				ids.add(e.getId());
			}
			if (i < 3) {
				assertEquals(6, gruppe.size());
			} else {
				assertEquals(2, gruppe.size());
			}
		}
		assertEquals(20, ids.size());
	}

	@Test
	public void mailempfaengerGruppieren_restklasse0_klappt() {
		// Arrange
		final List<LehrermailEmpfaenger> alle = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			final LehrermailEmpfaenger e = new LehrermailEmpfaenger();
			e.setEmail("mail@provider.de");
			e.setId(Long.valueOf(i));
			alle.add(e);
		}

		// Act
		final List<List<LehrermailEmpfaenger>> gruppen = delegate.mailempfaengerGruppieren(alle, 5);

		// Assert
		final Set<Long> ids = new HashSet<>();
		assertEquals(4, gruppen.size());
		for (int i = 0; i < 4; i++) {
			final List<LehrermailEmpfaenger> gruppe = gruppen.get(i);
			for (final LehrermailEmpfaenger e : gruppe) {
				ids.add(e.getId());
			}
			assertEquals(5, gruppe.size());
		}
		assertEquals(20, ids.size());
	}

	@Test
	public void mailempfaengerGruppieren_restklasse_2_laenge_empfaenger_klappt() {
		// Arrange
		delegate.setSizeDBFieldEmpfaenger(127);
		final List<LehrermailEmpfaenger> alle = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			final LehrermailEmpfaenger e = new LehrermailEmpfaenger();
			e.setEmail("mail@provider.de");
			e.setId(Long.valueOf(i));
			alle.add(e);
		}

		// Act
		final List<List<LehrermailEmpfaenger>> gruppen = delegate.mailempfaengerGruppieren(alle, 7);

		// Assert
		final Set<Long> ids = new HashSet<>();
		assertEquals(4, gruppen.size());
		for (int i = 0; i < 4; i++) {
			final List<LehrermailEmpfaenger> gruppe = gruppen.get(i);
			for (final LehrermailEmpfaenger e : gruppe) {
				ids.add(e.getId());
			}
			if (i < 3) {
				assertEquals(6, gruppe.size());
			} else {
				assertEquals(2, gruppe.size());
			}
		}
		assertEquals(20, ids.size());
	}

	@Test
	public void mailempfaengerGruppieren_restklasse_0_laenge_empfaenger_klappt() {
		// Arrange
		delegate.setSizeDBFieldEmpfaenger(127);
		final List<LehrermailEmpfaenger> alle = new ArrayList<>();
		for (int i = 0; i < 18; i++) {
			final LehrermailEmpfaenger e = new LehrermailEmpfaenger();
			e.setEmail("mail@provider.de");
			e.setId(Long.valueOf(i));
			alle.add(e);
		}

		// Act
		final List<List<LehrermailEmpfaenger>> gruppen = delegate.mailempfaengerGruppieren(alle, 7);

		// Assert
		final Set<Long> ids = new HashSet<>();
		assertEquals(3, gruppen.size());
		for (int i = 0; i < 3; i++) {
			final List<LehrermailEmpfaenger> gruppe = gruppen.get(i);
			for (final LehrermailEmpfaenger e : gruppe) {
				ids.add(e.getId());
			}
			assertEquals(6, gruppe.size());
		}
		assertEquals(18, ids.size());
	}

	@Test
	public void queueMails_wirft_IllegalArgumentException_wenn_payload_null() throws Exception {
		// Arrange
		final String path = TestUtils.getDevNewsletterDir();

		// Act + Assert
		assertThrows(IllegalArgumentException.class, () -> {
			delegate.queueMails(null, path, 40);
		});

	}

	@Test
	public void queueMails_wirft_IllegalArgumentException_wenn_pathMails_null() throws Exception {
		// Arrange
		final String path = null;
		final NewsletterPayload payload = new NewsletterPayload();
		payload.setBetreff("Mail aus Unit-Test von DEV");
		payload.setDateinameInhalt("2017-03-13-mailinhalt");
		payload.setDateinameEmfaenger("2017-03-13-empfaenger");

		// Act + Assert
		assertThrows(IllegalArgumentException.class, () -> {
			delegate.queueMails(payload, path, 40);
		});

	}

	@Test
	public void getMailempfaenger_klappt() throws Exception {
		// Arrange
		final String pathMaildir = TestUtils.getDevNewsletterDir();
		final NewsletterPayload payload = new NewsletterPayload();
		payload.setBetreff("Mail aus Unit-Test von DEV");
		payload.setDateinameInhalt("2017-03-13-mailinhalt");
		payload.setDateinameEmfaenger("2017-03-13-empfaenger");

		// Act
		final List<IMailempfaenger> alle = delegate.getMailempfaenger(payload, pathMaildir);

		// Assert
		assertEquals(91, alle.size());
	}
}
