//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice;

import org.junit.jupiter.api.BeforeEach;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.config.MKVAdminModule;
import de.egladil.mkv.persistence.config.KontextReader;

/**
 * @author heikew
 *
 */
public abstract class AbstractGuiceIT {

	@BeforeEach
	public void setUp() {
		KontextReader.getInstance().init(OsUtils.getDevConfigRoot());
		DynamicAdminConfigReader.getInstance().init(OsUtils.getDevConfigRoot());
		final Injector injector = Guice.createInjector(new MKVAdminModule(OsUtils.getDevConfigRoot()));
		injector.injectMembers(this);
	}
}
