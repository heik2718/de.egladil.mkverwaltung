//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;

import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.adminservice.AbstractGuiceIT;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.service.OrtskatalogService;
import de.egladil.mkv.persistence.payload.response.OrtLage;
import de.egladil.mkv.persistence.payload.response.PublicOrt;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;

/**
 * OrtskatalogServiceImplIT
 */
public class OrtskatalogServiceImplIT extends AbstractGuiceIT {

	@Inject
	private OrtskatalogService service;

	private void modifyDynamicContext() {
		final DynamicAdminConfig config = new DynamicAdminConfig();
		config.setMaximaleTreffermengeOrtsuche(400);
		config.setMaximaleTreffermengeOrteInLand(1000);

		DynamicAdminConfigReader.getInstance().setMockKontextForTest(config);
	}

	@Test
	void findOhneLand() {
		// Arrange
		final String name = "bAcH";
		modifyDynamicContext();

		// Act
		final List<PublicOrt> result = service.findOrte(name, null);

		// Assert
		assertEquals(369, result.size());
		int countOrteOhneSchulen = 0;
		int countOrteMitSchulen = 0;

		for (final PublicOrt ort : result) {
			final String kuerzel = ort.getKuerzel();
			assertNotNull(kuerzel);
			assertNotNull(ort.getName());
			final OrtLage lage = ort.getLage();
			assertNotNull(lage);
			assertNotNull(lage.getLand());
			assertNotNull(lage.getLandkuerzel());
			if (ort.getAnzahlSchulen() == 0) {
				countOrteOhneSchulen++;
			} else {
//				System.out.println("Ort " + kuerzel + ", Anzahl Schulen: " + ort.getAnzahlSchulen());
				countOrteMitSchulen++;
			}

			final HateoasPayload hateoasPayload = ort.getHateoasPayload();
			assertEquals("/orte/" + ort.getKuerzel(), hateoasPayload.getUrl());
			final List<HateoasLink> links = hateoasPayload.getLinks();
			assertEquals(2, links.size());
		}
		assertEquals(4, countOrteOhneSchulen);
		assertEquals(365, countOrteMitSchulen);
	}

	@Test
	void getOrt() throws Exception {

		// Arrange
		final String kuerzel = "QJEINQNI";
		final SchuleLage expectedLage = new SchuleLage("DE-BY", null, kuerzel, null);

		// Act
		final PublicOrt po = service.getOrt(kuerzel);

		// Assert
		assertEquals("Ansbach", po.getName());
		final OrtLage lage = po.getLage();
		assertEquals("DE-BY", lage.getLandkuerzel());
		assertEquals("Bayern", lage.getLand());
		assertEquals(10, po.getAnzahlSchulen());

		for (final PublicSchule ps : po.getKinder()) {
			assertEquals(expectedLage, ps.getLage());
			assertNotNull(ps.getKuerzel());
			assertFalse(StringUtils.isBlank(ps.getName()));
		}

		 System.out.println(new ObjectMapper().writeValueAsString(po));
	}

}
