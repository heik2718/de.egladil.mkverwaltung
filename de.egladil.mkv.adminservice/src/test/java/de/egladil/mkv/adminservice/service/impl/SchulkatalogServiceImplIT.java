//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.persistence.HateoasLink;
import de.egladil.common.persistence.HateoasPayload;
import de.egladil.mkv.adminservice.AbstractGuiceIT;
import de.egladil.mkv.adminservice.config.DynamicAdminConfig;
import de.egladil.mkv.adminservice.config.DynamicAdminConfigReader;
import de.egladil.mkv.adminservice.service.SchulkatalogService;
import de.egladil.mkv.persistence.payload.response.PublicSchule;
import de.egladil.mkv.persistence.payload.response.SchuleLage;

/**
 * SchulkatalogServiceImplIT
 */
public class SchulkatalogServiceImplIT extends AbstractGuiceIT {

	@Inject
	private SchulkatalogService service;

	/**
	*
	*/

	private void modifyDynamicContext() {
		final DynamicAdminConfig config = new DynamicAdminConfig();
		config.setMaximaleTreffermengeSchulenInOrt(500);
		config.setMaximaleTreffermengeSchulsuche(1000);

		DynamicAdminConfigReader.getInstance().setMockKontextForTest(config);
	}

	@Test
	void findMitOrt() {
		// Arrange
		final String name = "SteIn";
		modifyDynamicContext();

		// Act
		final List<PublicSchule> result = service.findSchulen(name, name);

		// Assert
		assertEquals(110, result.size());

		for (final PublicSchule schule : result) {
			assertNotNull(schule.getKuerzel());
			assertNotNull(schule.getName());
			final SchuleLage lage = schule.getLage();
			assertNotNull(lage);
			assertNotNull(lage.getLand());
			assertNotNull(lage.getLandkuerzel());
			assertNotNull(lage.getOrt());
			assertNotNull(lage.getOrtkuerzel());

			final HateoasPayload hateoasPayload = schule.getHateoasPayload();
			assertEquals("/schulen/" + schule.getKuerzel(), hateoasPayload.getUrl());
			final List<HateoasLink> links = hateoasPayload.getLinks();
			assertEquals(3, links.size());
		}
	}

	@Test
	void getSchuleKlappt() throws Exception {
		// Assert
		final String kuerzel = "9PDDS51Q";

		// Act
		final PublicSchule ps = service.getSchule(kuerzel);

		// Assert
		assertNotNull(ps);
		assertNotNull(ps.getKuerzel());
		assertNotNull(ps.getName());
		final SchuleLage lage = ps.getLage();
		assertNotNull(lage);
		assertNotNull(lage.getLand());
		assertNotNull(lage.getLandkuerzel());
		assertNotNull(lage.getOrt());
		assertNotNull(lage.getOrtkuerzel());

		final HateoasPayload hateoasPayload = ps.getHateoasPayload();

		assertEquals("/schulen/9PDDS51Q", hateoasPayload.getUrl());
		final List<HateoasLink> links = hateoasPayload.getLinks();

		assertEquals(6, links.size());

		{
			final HateoasLink link = links.get(0);
			assertEquals("/orte/PAMT7UXS", link.getUrl());
			assertEquals("GET", link.getMethod());
			assertEquals("Ort", link.getRel());
		}
		{
			final HateoasLink link = links.get(1);
			assertEquals("/laender/DE-BB", link.getUrl());
			assertEquals("GET", link.getMethod());
			assertEquals("Land", link.getRel());
		}
		{
			final HateoasLink link = links.get(2);
			assertEquals("/schulen/9PDDS51Q", link.getUrl());
			assertEquals("PUT", link.getMethod());
			assertEquals("Schule ändern", link.getRel());
		}
		{
			final HateoasLink link = links.get(3);
			assertEquals("/advvereinbarungen/9PDDS51Q", link.getUrl());
			assertEquals("GET", link.getMethod());
			assertEquals("Vereinbarung ADV", link.getRel());
		}
		{
			final HateoasLink link = links.get(4);
			assertEquals("/schulen/teilnahmen/9PDDS51Q", link.getUrl());
			assertEquals("GET", link.getMethod());
			assertEquals("Teilnahmen", link.getRel());
		}
		{
			final HateoasLink link = links.get(5);
			assertEquals("/auswertungsgruppen/YHHFQEDI20171228142026", link.getUrl());
			assertEquals("GET", link.getMethod());
		}

//		System.out.println(new ObjectMapper().writeValueAsString(ps));

	}
}
