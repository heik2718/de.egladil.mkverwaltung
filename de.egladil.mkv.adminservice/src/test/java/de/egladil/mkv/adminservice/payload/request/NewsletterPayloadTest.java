//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mkv.adminservice.payload.request;


import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * NewsletterPayloadTest
 */
public class NewsletterPayloadTest {

	@Test
	public void serialize() throws Exception {
		// Arrange
		final NewsletterPayload payload = new NewsletterPayload();
		payload.setBetreff("Minikänguru 2017 Unterlagen");
		payload.setDateinameEmfaenger("freischaltung-privat-empfaenger");
		payload.setDateinameInhalt("freigabeUnterlagen.txt");

		System.out.println(new ObjectMapper().writeValueAsString(payload));
	}

}
