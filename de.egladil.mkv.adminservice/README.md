# mkv.adminservice
backend für Administration der Minikänguru-Daten


## Benutzerkonten migrieren
URL ist

	/admin-app/export/benutzer

Im Unterverzeichnis bv_export im user-home muss eine Datei uuid.lst liegen, die die zu exportierenden Benutzer-IDs enthält. Die exportierten JSON-Dateien landen unter user.home/bv_export/export.

Auf dem anderen Server werden die JSON-Dateien über eine Import-URL im authprovider importiert.